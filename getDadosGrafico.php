<?php

session_start();
include_once( "includes/Dao/DaoSistema.class.php" );
include_once( "includes/Dao/RelatorioDao.class.php" );
include_once( "includes/Dao/OcorrenciaDao.class.php" );
include_once( "includes/Status.class.php" );
include_once( "includes/Dao/StatusDao.class.php" );
include_once( "includes/Rota.class.php" );
include_once( "includes/Dao/RotaDao.class.php" );

include_once("DaoGlobal.class.php");
include_once("SistemaDao.class.php");
include_once("GlbSistema.class.php");
include_once("GlbSistemaPerfil.class.php");
include_once("GlbUsuario.class.php");
include_once("GlbColaborador.class.php");
include_once("GlbPessoa.class.php");
include_once("GlbEndereco.class.php");
include_once("GlbTelefone.class.php");
include_once("include_novo/GlbTransportadora.class.php");
include_once("include_novo/GlbContatoTransportador.class.php");
include_once("include_novo/ContatoTransportadorDao.class.php");
include_once("GlbProgramaRotina.class.php");
include_once("GlbModuloPrograma.class.php");
include_once("GlbSistemaModulo.class.php");
include_once("PerfilUsuarioDao.class.php");
include_once("PerfilPermissaoDao.class.php");
include_once("include_novo/GlbCentroDistribuicao.class.php");
include_once("integraSetSistemas.php");

// Estrutura basica do grafico
$grafico = array(
    'dados' => array(
        'cols' => array(
            array('type' => 'string', 'label' => 'Gênero'),
            array('type' => 'number', 'label' => 'Quantidade')
        ),
        'rows' => array()
    ),
    'config' => array(
        'title' => 'Quantidade de alunos por gênero',
        'width' => 400,
        'height' => 300
    )
);

// Consultar dados no BD

$sql = 'SELECT DST_NO_, COUNT(*) as quantidade FROM alunos GROUP BY genero';
$stmt = $pdo->query($sql);
while ($obj = $stmt->fetchObject()) {
    $grafico['dados']['rows'][] = array('c' => array(
        array('v' => $obj->genero),
        array('v' => (int)$obj->quantidade)
    ));
}

// Enviar dados na forma de JSON
header('Content-Type: application/json; charset=UTF-8');
echo json_encode($grafico);
exit(0);