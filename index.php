<!DOCTYPE html>
<?php
session_start();
include_once( "includes/Dao/DaoSistema.class.php" );
include_once( "includes/Dao/RelatorioDao.class.php" );
include_once( "includes/Dao/OcorrenciaDao.class.php" );
include_once( "includes/Status.class.php" );
include_once( "includes/Dao/StatusDao.class.php" );
include_once( "includes/Rota.class.php" );
include_once( "includes/Dao/RotaDao.class.php" );

include_once("DaoGlobal.class.php");
include_once("SistemaDao.class.php");
include_once("GlbSistema.class.php");
include_once("GlbSistemaPerfil.class.php");
include_once("GlbUsuario.class.php");
include_once("GlbColaborador.class.php");
include_once("GlbPessoa.class.php");
include_once("GlbEndereco.class.php");
include_once("GlbTelefone.class.php");
include_once("include_novo/GlbTransportadora.class.php");
include_once("include_novo/GlbContatoTransportador.class.php");
include_once("include_novo/ContatoTransportadorDao.class.php");
include_once("GlbProgramaRotina.class.php");
include_once("GlbModuloPrograma.class.php");
include_once("GlbSistemaModulo.class.php");
include_once("PerfilUsuarioDao.class.php");
include_once("PerfilPermissaoDao.class.php");
include_once("include_novo/GlbCentroDistribuicao.class.php");
include_once("integraSetSistemas.php");
?>
<html lang="en">
<head>
    <title>Devolução</title>
    <?php include("./library/head.php"); ?>

</head>
<body>
<!-- set loading layer -
<div class="dev-page-loading preloader"></div>
<!-- ./set loading layer -->

<!-- page wrapper -->
<div class="dev-page">

    <!-- page header -->
    <?php include("./library/topo.php"); ?>
    <!-- ./page header -->

    <!-- page container -->
    <div class="dev-page-container">

        <!-- page sidebar -->
        <?php include("./library/menu.php"); ?>
        <!-- ./page sidebar -->

        <!-- page content -->
        <div class="dev-page-content">
            <!-- page content container -->
            <div class="container">

                <?php

                $integra = new integraSetSistemas("conf.xml");
                $ObjUsuario = unserialize($integra->getSistemaIntegra());

                $relatorioDao = new RelatorioDao();
                $OcorrenciaDao = new OcorrenciaDao();
                $RotaDao = new RotaDao();
                
                /* FILTRO ROTA */
                if ($ObjUsuario->getUsuarioTipo() == "VEN") {
                    $VendedorUsuario = $ObjUsuario->getObjVendedor();
                    $ObjVendedor = new GlbVendedor();
                    if ($VendedorUsuario[0]) {
                        $ObjVendedor->setCodigo($VendedorUsuario[0]->getCodigo());
                        $ObjVendedor->setTipo($VendedorUsuario[0]->getTipo());
                    }
                    $resultRota = $RotaDao->consultarRotaVendedor($ObjVendedor);
                } else if ($ObjUsuario->getUsuarioTipo() == "CLI") {

                    $resultRota = $OcorrenciaDao->consultaCliente($ObjUsuario->getObjCliente()->getClienteCodigo());
                    //  OCIFetchInto($resultCliente, $rowCliente, OCI_ASSOC);
                } else if($ObjUsuario->getUsuarioTipo() == "TRA"){
                    $resultRota  = $RotaDao->consultaUsuRota($ObjUsuario->getUsuarioCodigo());
                } else {

                    $resultRota = $RotaDao->listar();
                }


                /* FILTRO CEDIS*/
                $GlbCentroDistribuicao = new GlbCentroDistribuicao();
                $GlbCentroDistribuicao = $OcorrenciaDao->filtroCentroDis();

                extract($_REQUEST);

                if ($dataInicial == "" or $dataFinal==""){
                    $dataInicial='01/01/'.date(Y);
                    $dataFinal='31/12/'.date(Y);
                }

                ?>
                <!--
                <div class="row row-condensed">
                    <div class="col-lg-12 col-md-6">
                        <div class="wrapper">
                            <div class="page-subtitle">
                                <h2>Menu/Filtro(s):</h2>
                                <div class="pull-right">
                                    <div class="btn-group">

                                        <button class="btn btn-default btn-rounded btn-icon"><i class="fa fa-calendar pull-left"></i> 01/07/2015</button>
                                        <button class="btn btn-default btn-rounded btn-icon"><i class="fa fa-calendar pull-left"></i> 01/08/2015</button>

                                    </div>
                                </div>
                            </div>
                            <div id="dashboard-chart" class="chart-holder"><svg></svg></div>
                        </div>
                    </div>
                </div>
                -->
                <form name="frmHome" method="POST" action="">
                    <div class="row">
                        <div class="col-lg-12 col-md-6">
                            <div class="wrapper">
                                <div class="page-subtitle">
                                    <h2>Menu/Filtro(s):</h2>
                                </div>
                                <div class="col-md-2 col-sm-3 col-xs-3">
                                    <div class="form-group">
                                        <label>Início</label>
                                        <input type="text" class="form-control datepicker" onKeyPress="fMascara('mesano', event, this)" name="dataInicial" id="dataInicial" value="<?=$dataInicial?>">
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-3 col-xs-3">
                                    <div class="form-group">
                                        <label>Fim</label>
                                        <input type="text" class="form-control datepicker" onKeyPress="fMascara('mesano', event, this)" name="dataFinal" id="dataFinal" value="<?=$dataFinal?>">
                                    </div>
                                </div>
                                <?php
                                if ($ObjUsuario->getUsuarioTipo() == "CLI") {
                                    $ObjUsuario->getObjCliente()->getClienteCodigo();
                                    ?>
                                    <div class="col-md-2 col-sm-3 col-xs-3">
                                        <div class="form-group">
                                            <label>Cód. Cliente</label>
                                            <input type="text" placeholder="Cliente" class="form-control" onkeypress="fMascara('numero', event, this)" name="conCodCliente" id="conCodCliente" value="<?php echo $ObjUsuario->getObjCliente()->getClienteCodigo(); ?>" readonly>
                                        </div>
                                    </div>
                                    <?php
                                } else {
                                    ?>
                                    <div class="col-md-2 col-sm-3 col-xs-3">
                                        <div class="form-group">
                                            <label>Cód. Cliente</label>
                                            <input type="text" placeholder="Cliente" class="form-control" onkeypress="fMascara('numero', event, this)" name="conCodCliente" id="conCodCliente">
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>
                                <?php
                                if ($ObjUsuario->getUsuarioTipo() != "CLI") {
                                    ?>
                                    <?php
                                    if ($ObjUsuario->getUsuarioTipo() != "VEN") {
                                        ?>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>Setor Representante</label>
                                                <input type="text" placeholder="Cód. Representante" class="form-control" size="10" id="representante" name="representante" onKeyPress="fMascara('numero', event, this)" value="<?=$representante?>">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>Gerente Contas</label>
                                                <input type="text" placeholder="Cód. Gerente" class="form-control" size="10" id="gerente" name="gerente" onKeyPress="fMascara('numero', event, this)" value="<?=$gerente?>">
                                            </div>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label>Divisão</label>
                                            <select class="form-control selectpicker" id="divisao" name="divisao">
                                                <option value="">Todos</option>
                                                <option <?=($divisao == 'A'?"selected":"")?> value="A">Alimentar</option>
                                                <option <?=($divisao == 'O'?"selected":"")?> value="O">Dist. Direta</option>
                                                <option <?=($divisao == 'F'?"selected":"")?> value="F">Farma</option>
                                                <option <?=($divisao == 'H'?"selected":"")?> value="H">Hospitalar</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <label>CD</label>
                                            <select class="form-control selectpicker" id="cedis" name="cedis">
                                                <option value="">Todos</option>
                                                <?php
                                                foreach ($GlbCentroDistribuicao as $ObjCedis) {
                                                    ?>
                                                    <option <?=($cedis == $ObjCedis->getCodigo()?"selected":"")?> value="<?php echo $ObjCedis->getCodigo(); ?>"><?php echo $ObjCedis->getNome(); ?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <label>Rota</label>
                                            <select class="form-control selectpicker" id="rota" name="rota">
                                                <option value="">Todos</option>
                                                <?php
                                                while (OCIFetchInto($resultRota, $rowRota, OCI_ASSOC)) {
                                                    ?>
                                                    <option value="<?php echo $rowRota['ROT_CO_NUMERO']; ?>"  <?php
                                                    if ($rowCliente['ROT_CO_NUMERO'] == $rowRota['ROT_CO_NUMERO'] or $rota == $rowRota['ROT_CO_NUMERO']) {
                                                        echo "selected='selected'";
                                                    }
                                                    ?>>
                                                        <?php echo $rowRota['ROT_CO_NUMERO']; ?>
                                                    </option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2 col-sm-6 col-xs-6" style="padding-left: 40px;">
                            <button class="btn btn-primary" onclick="javascript: frmHome.Submit();" id="botaoConsultar">Consultar</button>
                        </div>
                    <div class="row">
                </form>                

                <?php
                if ($ObjUsuario->getUsuarioTipo() != "CLI") {
                    ?>
                    <div class="row">
                        <div class="wrapper">
                            <div class="col-lg-6">
                                <div class="page-subtitle">
                                    <h2>QTD. PROTOCOLOS - AGUARDANDO COLETA</h2>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="page-subtitle">
                                    <h2>Qtd. Protocolos x Valor - Por Motivos Top 10</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="wrapper padding-bottom-0">
                            <div class="dev-table">
                                <div class="dev-col col-md-6">
                                    <?php
                                    $qtdDentro = $relatorioDao->relatorioOcorrenciaCountSum("count", $dataInicial, $dataFinal, $cliente, $rede, $status=5, $rota, $cedis, $motivo, $tipo, $divisao, $representante, $gerente, $coleta="DENTRO", $ObjUsuario);
                                    ?>
                                    <div class="dev-widget dev-widget-transparent">
                                        <h2>DENTRO DO PRAZO</h2>
                                        <!--<p>Total de ocorrências abertas</p> -->
                                        <div class="dev-stat"><span class="counter"><?=$qtdDentro?></span></div>
                                        <div class="progress progress-bar-xs">
                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 79%;"></div>
                                        </div>
                                        <!-- <a href="#" class="dev-drop">Take a closer look <span class="fa fa-angle-right pull-right"></span></a> -->
                                    </div>
                                    <?php
                                    $qtdFora = $relatorioDao->relatorioOcorrenciaCountSum("count", $dataInicial, $dataFinal, $cliente, $rede, $status=5, $rota, $cedis, $motivo, $tipo, $divisao, $representante, $gerente, $coleta="FORA", $ObjUsuario);
                                    ?>
                                    <div class="dev-widget dev-widget-transparent dev-widget-success">
                                        <h2>FORA DO PRAZO</h2>
                                        <!-- <p>Total de ocorrências com situação reprovada</p> -->
                                        <div class="dev-stat"><span class="counter"><?=$qtdFora?></span></div>
                                        <div class="progress progress-bar-xs">
                                            <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 79%;"></div>
                                        </div>
                                        <!-- <a href="#" class="dev-drop">Take a closer look <span class="fa fa-angle-right pull-right"></span></a> -->
                                    </div>
                                </div>
                                <div class="dev-col col-md-6">
                                    <div id="data_grafico3"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                }
                ?>
                <div class="row">
                    <div class="wrapper">
                        <div class="col-lg-6">
                            <div class="page-subtitle">
                                <h2>QTD. PROTOCOLOS - POR STATUS</h2>
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <div class="page-subtitle">
                                <h2>VALOR PROTOCOLOS - POR STATUS</h2>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="wrapper">
                        <div class="col-lg-6">
                            <div id="data_grafico1"></div>

                            <?php

                            $resultQtdStatus = $relatorioDao->relatorioOcorrenciaCountSumRetornoPor("STATUS", $dataInicial, $dataFinal, $cliente, $rede, $status="1,3,5,7,9,11,12,13,18", $rota, $cedis, $motivo, $tipo, $divisao, $representante, $gerente, $ObjUsuario);

                            while (OCIFetchInto($resultQtdStatus, $rowQtdStatus, OCI_ASSOC)) {
                                $vetDescricaoQtdStatus[1] = "AGUARD. APROVACAO";
                                $vetDescricaoQtdStatus[3] = "AGUARD. NFD";
                                $vetDescricaoQtdStatus[5] = "AGUARD. COLETA";
                                $vetDescricaoQtdStatus[7] = "AGUARD. CONFERENCIA FISICA x NF";
                                $vetDescricaoQtdStatus[9] = "AGUARD. LANCAMENTO FISCAL";
                                $vetDescricaoQtdStatus[11] = "FINALIZADO";
                                $vetDescricaoQtdStatus[12] = "REPROVADO";
                                $vetDescricaoQtdStatus[13] = "PENDENTE";
                                $vetDescricaoQtdStatus[18] = "AGUARD. CONFERENCIA RECEBIMENTO";


                                $descricaoQtdStatus = $vetDescricaoQtdStatus[$rowQtdStatus['RETORNO']];

                                $vetItemQtdStatus[] = array("{$descricaoQtdStatus}", $rowQtdStatus['NUM'] );

                            }
                            $qtdTotalQtdStatus = $relatorioDao->relatorioOcorrenciaCountSum("COUNT", $dataInicial, $dataFinal, $cliente, $rede, $status="", $rota, $cedis, $motivo, $tipo, $divisao, $representante, $gerente, $coleta, $ObjUsuario);

                            $vetItemQtdStatus[] = array("PROTOCOLOS REGISTRADOS", $qtdTotalQtdStatus );

                            $_SESSION["data_grafico1"] = $vetItemQtdStatus;



                            ?>

                        </div>
                        <div class="col-lg-6">
                            <?php
                            $resultValorStatus = $relatorioDao->relatorioOcorrenciaCountSumRetornoPor("STATUS", $dataInicial, $dataFinal, $cliente, $rede, $status="1,3,5,7,9,11,12,13,18", $rota, $cedis, $motivo, $tipo, $divisao, $representante, $gerente, $ObjUsuario);
                            while (OCIFetchInto($resultValorStatus, $rowValorStatus, OCI_ASSOC)) {
                                $vetDescricaoValorStatus[1] = "AGUARD. APROVACAO";
                                $vetDescricaoValorStatus[3] = "AGUARD. NFD";
                                $vetDescricaoValorStatus[5] = "AGUARD. COLETA";
                                $vetDescricaoValorStatus[7] = "AGUARD. CONFERENCIA FISICA x NF";
                                $vetDescricaoValorStatus[9] = "AGUARD. LANCAMENTO FISCAL";
                                $vetDescricaoValorStatus[11] = "FINALIZADO";
                                $vetDescricaoValorStatus[12] = "REPROVADO";
                                $vetDescricaoValorStatus[13] = "PENDENTE";
                                $vetDescricaoValorStatus[18] = "AGUARD. CONFERENCIA RECEBIMENTO";

                                $descricaoValorStatus = $vetDescricaoValorStatus[$rowValorStatus['RETORNO']];
                                $vetItemValorStatus[] = array("{$descricaoValorStatus}", str_replace(",",".", $rowValorStatus['VALOR']) );

                            }
                            $qtdTotalValorStatus = $relatorioDao->relatorioOcorrenciaCountSum("VALOR", $dataInicial, $dataFinal, $cliente, $rede, $status="", $rota, $cedis, $motivo, $tipo, $divisao, $representante, $gerente, $coleta, $ObjUsuario);
                            $vetItemValorStatus[] = array("PROTOCOLOS REGISTRADOS", str_replace(",",".", $qtdTotalValorStatus ));

                            $_SESSION["data_grafico2"] = $vetItemValorStatus;

                            ?>
                            <div id="data_grafico2"></div>

                        </div>
                    </div>
                </div>

                <div class="row row-condensed">
                    <div class="col-lg-12 col-md-6">
                        <div class="wrapper">
                            <?php
                            $resultValorMotivo = $relatorioDao->relatorioOcorrenciaCountSumRetornoPor("MOTIVO", $dataInicial, $dataFinal, $cliente, $rede, $status="", $rota, $cedis, $motivo = "6,13,33,37,38,39,82,83,84,85,87", $tipo, $divisao, $representante, $gerente, $ObjUsuario);
                            while (OCIFetchInto($resultValorMotivo, $rowValorMotivo, OCI_ASSOC)) {
                                $vetDescricaoValorMotivo[87] = "DESCONTO";
                                $vetDescricaoValorMotivo[13] = "PRAZO";
                                $vetDescricaoValorMotivo[82] = "NAO PEDIU";
                                $vetDescricaoValorMotivo[39] = "VALIDADE CURTA";
                                $vetDescricaoValorMotivo[84] = "PRECO";
                                $vetDescricaoValorMotivo[85] = "DESISTENCIA DO CLIENTE";
                                $vetDescricaoValorMotivo[38] = "DIGITADO\n APRESENTACAO ERRADA";
                                $vetDescricaoValorMotivo[83] = "DIGITADO\n QUANTIDADE ERRADA";
                                $vetDescricaoValorMotivo[37] = "DUPLICADO";
                                $vetDescricaoValorMotivo[6] = "FALTA UNIDADE";
                                $vetDescricaoValorMotivo[33] = "DANIFICADO";

                                $descricaoValorMotivo = $vetDescricaoValorMotivo[$rowValorMotivo['RETORNO']];

                                $vetItemValorMotivo[] = array("{$descricaoValorMotivo}", str_replace(",",".", $rowValorMotivo['VALOR']) );

                            }
                            $_SESSION["data_grafico3_1"] = $vetItemValorMotivo;

                            $resultQtdMotivo = $relatorioDao->relatorioOcorrenciaCountSumRetornoPor("MOTIVO", $dataInicial, $dataFinal, $cliente, $rede, $status="", $rota, $cedis, $motivo = "6,13,33,37,38,39,82,83,84,85,87", $tipo, $divisao, $representante, $gerente, $ObjUsuario);
                            while (OCIFetchInto($resultQtdMotivo, $rowQtdMotivo, OCI_ASSOC)) {
                                $vetDescricaoQtdMotivo[87] = "DESCONTO";
                                $vetDescricaoQtdMotivo[13] = "PRAZO";
                                $vetDescricaoQtdMotivo[82] = "NAO PEDIU";
                                $vetDescricaoQtdMotivo[39] = "VALIDADE CURTA";
                                $vetDescricaoQtdMotivo[84] = "PRECO";
                                $vetDescricaoQtdMotivo[85] = "DESISTENCIA DO CLIENTE";
                                $vetDescricaoQtdMotivo[38] = "DIGITADO\n APRESENTACAO ERRADA";
                                $vetDescricaoQtdMotivo[83] = "DIGITADO\n QUANTIDADE ERRADA";
                                $vetDescricaoQtdMotivo[37] = "DUPLICADO";
                                $vetDescricaoQtdMotivo[6] = "FALTA UNIDADE";
                                $vetDescricaoQtdMotivo[33] = "DANIFICADO";

                                $descricaoQtdMotivo = $vetDescricaoQtdMotivo[$rowQtdMotivo['RETORNO']];

                                $vetItemQtdMotivo[] = array("{$descricaoQtdMotivo}", $rowQtdMotivo['NUM'] );

                            }
                            $_SESSION["data_grafico3_2"] = $vetItemQtdMotivo;

                            ?>


                            <div id="data_grafico3"></div>

                        </div>
                    </div>

                </div>


                <!-- Copyright -->
                <div class="copyright">
                    <div class="pull-left">
                        &copy; <?php echo date('Y'); ?> <strong> Devolução</strong> - Servimed
                    </div>
                </div>
                <!-- ./Copyright -->
            </div>
            <!-- ./page content container -->

        </div>
        <!-- ./page content -->
    </div>
    <!-- ./page container -->

    <!-- right bar -->
    <div class="dev-page-rightbar">
        <div class="rightbar-chat">

            <div class="rightbar-chat-frame-contacts scroll">
                <div class="rightbar-title">
                    <h3>Messages</h3>
                    <a href="#" class="btn btn-default btn-rounded rightbar-close pull-right"><span class="fa fa-times"></span></a>
                </div>
                <ul class="contacts">
                    <li class="title">online</li>
                    <li>
                        <a href="#" class="status online">
                            <img src="assets/images/users/user_1.jpg" title="Aqvatarius"> John Doe
                        </a>
                    </li>
                    <li>
                        <a href="#" class="status online">
                            <img src="assets/images/users/user_2.jpg" title="Aqvatarius"> Shannon Freeman
                        </a>
                    </li>
                    <li>
                        <a href="#" class="status away">
                            <img src="assets/images/users/user_3.jpg" title="Aqvatarius"> Devin Stephens
                        </a>
                    </li>
                    <li>
                        <a href="#" class="status away">
                            <img src="assets/images/users/user_4.jpg" title="Aqvatarius"> Marissa George
                        </a>
                    </li>
                    <li>
                        <a href="#" class="status dont">
                            <img src="assets/images/users/user_5.jpg" title="Aqvatarius"> Sydney Reeves
                        </a>
                    </li>
                    <li class="title">offline</li>
                    <li>
                        <a href="#" class="status">
                            <img src="assets/images/users/user_6.jpg" title="Aqvatarius"> Kaitlynn Bowen
                        </a>
                    </li>
                    <li>
                        <a href="#" class="status">
                            <img src="assets/images/users/user_7.jpg" title="Aqvatarius"> Karen Spencer
                        </a>
                    </li>
                    <li>
                        <a href="#" class="status">
                            <img src="assets/images/users/user_8.jpg" title="Aqvatarius"> Darrell Wolfe
                        </a>
                    </li>
                </ul>
            </div>

            <div class="rightbar-chat-frame-chat">
                <div class="user">
                    <div class="user-panel">
                        <a href="#" class="btn btn-default btn-rounded rightbar-chat-close"><span class="fa fa-angle-left"></span></a>
                        <a href="#" class="btn btn-default btn-rounded pull-right"><span class="fa fa-user"></span></a>
                    </div>
                    <div class="user-info">
                        <div class="user-info-image status online">
                            <img src="assets/images/users/user_1.jpg">
                        </div>
                        <h5>Devin Stephens</h5>
                        <span>UI/UX Designer</span>
                    </div>
                </div>
                <div class="chat-wrapper scroll">
                    <ul class="chat" id="rightbar_chat">
                        <li class="inbox">
                            Hi, you have a second? Need to ask you something.
                            <span>about 1h ago</span>
                        </li>
                        <li class="sent">
                            Sure i have...
                            <span>59min ago</span>
                        </li>
                        <li class="inbox">
                            It's about latest design you did...
                            <span>14min ago</span>
                        </li>
                        <li class="sent">
                            I will do my best to help you
                            <span>2min ago</span>
                        </li>
                    </ul>
                </div>

                <form class="form" action="#" method="post" id="rightbar_chat_form">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-btn">
                                <button class="btn btn-default"><i class="fa fa-paperclip"></i></button>
                            </div>
                            <input type="text" class="form-control" name="message">
                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-default">Send</button>
                            </div>
                        </div>
                    </div>
                </form>

            </div>


        </div>
    </div>
    <!-- ./right bar -->

    <!-- page footer -->
    <?php include("./library/footer.php"); ?>
    <!-- ./page footer -->

    <!-- page search -->
    <!-- <div class="dev-search">
        <div class="dev-search-container">
            <div class="dev-search-form">
            <form action="index.html" method="post">
                <div class="dev-search-field">
                    <input type="text" placeholder="Search..." value="Nature">
                </div>
            </form>
            </div>
            <div class="dev-search-results"></div>
        </div>
    </div> -->
    <!-- page search -->
</div>
<!-- ./page wrapper -->
<!-- javascript -->
<?php include("./library/rodape.php"); ?>

<script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<!-- ./javascript <script type="text/javascript" src="js/googlecharts.js"></script>-->

<script type="text/javascript">


    google.charts.load('current', {'packages': ['corechart', 'bar']});
    google.charts.setOnLoadCallback(drawChart);
    google.charts.setOnLoadCallback(drawChartGrafico2);
    google.charts.setOnLoadCallback(drawChartGrafico3);

    function drawChart() {

        // Create the data table.

        <?php

        $array_list[]="['Status','Quantidade']"; //setando o titulo do eixo
        foreach ($_SESSION["data_grafico1"] as $dados){
            $array_list[]= "['$dados[0]', $dados[1]]";
        }
        ?>
        var data = google.visualization.arrayToDataTable([
            <?php echo (implode(",", $array_list)); ?> //inserindo o array_list
        ]);

        var options = {
            title: 'Quantidade de protocolos por Status',
            chartArea : {width: '50%'},
            height : 350,
            width : 550,
            colors: ['#e0440e'],
            legend : {position : 'none'},
            bars: 'horizontal',
            backgroundColor : {
                fill: '#fbfbfb',
                fillOpacity: 0.8}
        };

        var chart = new google.visualization.BarChart(document.getElementById('data_grafico1'));

        chart.draw(data, options);
    }

    function drawChartGrafico2(){

        <?php

        $array_listGrafico2[] = "['Status', 'Valor']"; //setando o titulo do eixo
        foreach ($_SESSION["data_grafico2"] as $dados2){
            $array_listGrafico2[] = "['$dados2[0]', $dados2[1]]";
        }
        ?>
        var data2 = google.visualization.arrayToDataTable([
            <?php echo (implode(",", $array_listGrafico2)); ?> //inserindo o array_list
        ]);

        var options2 = {
            title: 'Valor de protocolos por Status',
            chartArea : {width: '30%'},
            height : 350,
            width : 550,
            colors: ['#32CD32'],
            legend : {position : 'none'},
            backgroundColor : {
                fill: '#fbfbfb',
                fillOpacity: 0.8}
        };

        var chart2 = new google.visualization.BarChart(document.getElementById('data_grafico2'));

        chart2.draw(data2, options2);
    }

    function drawChartGrafico3() {

        <?php

        $array_listGrafico3[] = "['Status', 'Valor']"; //setando o titulo do eixo
        foreach ($_SESSION["data_grafico3_2"] as $dados3){
            $array_listGrafico3[] = "['$dados3[0]', $dados3[1]]";
        }
        ?>
        var data3 = google.visualization.arrayToDataTable([
            <?php echo (implode(",", $array_listGrafico3)); ?> //inserindo o array_list
        ]);

        var options3 = {
            title: 'Quantidade de Protocolos x Valor TOP 10',
            chartArea : {width: '50%'},
            height : 350,
            width : 550,
            legend : {position : 'none'},
            colors: ['#8A2BE2'],
            backgroundColor : {
                fill: '#fbfbfb',
                fillOpacity: 0.8}
        };

        var chart3 = new google.visualization.BarChart(document.getElementById('data_grafico3'));

        chart3.draw(data3, options3);

    }

</script>

<!-- ./javascript -->
</body>
</html>