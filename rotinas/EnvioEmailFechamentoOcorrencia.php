<?php
session_start();
include_once( "../includes/Dao/DaoSistema.class.php" );
include_once( "../includes/Dao/OcorrenciaDao.class.php" );
require_once("phpmailer/class.phpmailer.php");

extract($_REQUEST);

$td = "border: 1px solid #dddddd;text-align:left;padding: 8px;";
$th = "border: 1px solid #dddddd;text-align:center;padding: 8px;background-color: #337ab7;color: white;";

$cabecaEmail = "
<style>
  table {
      border-collapse: collapse;
      width: 100%;
  }

  th, td {
      text-align: left;
      padding: 8px;
  }

  tr:nth-child(even){background-color: #f2f2f2}

  th {
      background-color: #337ab7;
      color: white;
  }
</style>
<html>    
  <table style='border-collapse: collapse;width: 100%;'>
    <tr>
      <td>
        <tr>
          <th style='border: 1px solid #dddddd;text-align:left;padding: 8px;background-color: #cc0000;color: white;'>COMUNICADO!</th>
        </tr>
        <tr>
          <td style='border: 1px solid #dddddd;text-align:left;padding: 8px;'>
            Sua Solicitação de Sobra de Mercadoria foi enviado com sucesso.
          </td>
        </tr>
      </td>
    </tr>
  </table>
  <br>
  <table style='border-collapse: collapse;width: 100%;'>
    <tr>
      <th style='".$th."'>Protocolo</th>
      <th style='".$th."'>Nota</th>
      <th style='".$th."'>Motivo Devolução</th>
    </tr>
    <tr style='background-color: #fff;'>  
      <td style='".$td."'>$codOcorrencia</td>
      <td style='".$td."'>$serieNota/$numNota</td>
      <td style='".$td."'>Sobra de Mercadoria Física</td>
    </tr>";

$rodapeEmail = "
  </table>
  <br>
  <p>E-mail enviado automaticamente, respostas ou dúvidas devem ser encaminhadas para fulano@servimed.com.br ou entrar em contato no ramal: 000-0000. <br> TI Servimed.</p>
</html>";

// Variáveis
$tentativasEnvioEmail = 0;

echo $cabecaEmail.$emailEnviado.$rodapeEmail;

    $emailEnviado = ""; //Responsável por verificar se o email deve ser enviado ou não.
    $emailEnvio = "";

    function smtpmailer($para, $de, $de_nome, $assunto, $corpo) {
      global $error;  
      $mail = new PHPMailer();
      $mail->IsSMTP();
      $mail->SMTPDebug = 2;
      $mail->SMTPAuth = true;
      $mail->SMTPSecure = 'ssl';
      $mail->Host = 'smtp.gmail.com';
      $mail->Port = 465;
      $mail->Username = 'USUARIO_EMAIL';
      $mail->Password = 'SENHA';
      $mail->SetFrom($de);
      $mail->Subject = $assunto;
      $mail->Body = $corpo;
      $mail->AddAddress($para);
      $mail->IsHTML(true);   

      ini_set("memory_limit","100M");

      // if (!empty($_FILES['anexarTextos']['name']))	
      //   $mail->AddAttachment($_FILES['anexarTextos']['tmp_name'], $_FILES['anexarTextos']['name']);

      $tentativasEnvioEmail = 0;
      while($tentativasEnvioEmail <= 3)
      {
        try{
          if(!$mail->Send()) {
            $tentativasEnvioEmail++;
            echo "NÃO ENVIOU - Tentativa ".$tentativasEnvioEmail." <br>";
          } else {
            $tentativasEnvioEmail = 4;          
            echo "E-mail enviado! <br>";
          }
        }
        catch (Exception $ex) {
          $tentativasEnvioEmail++;
          echo "Erro - E-mail não enviado! ".$ex;
        }
      }
      $emailEnviado = $arquivo;
      //$emailEnvio = $emailGerente;
      $tentativasEnvioEmail = 0;
    }

    try{
      if (smtpmailer('DESTINATARIO', 'USUARIO_EMAIL', 'Aplicativo', 'Novo Cadastro Efetuado', $cabecaEmail.$emailEnviado.$rodapeEmail)) {
      echo 'Sucesso';
    }
    if (!empty($error)) echo 'Sem sucesso! '.$error;
    }
    catch(Exception $ex){
      echo 'Sem sucesso! '.$ex;
    }
?>