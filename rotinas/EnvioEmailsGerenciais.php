<?php

session_start();
include_once( "../includes/Dao/DaoSistema.class.php" );
include_once( "../includes/Dao/OcorrenciaDao.class.php" );
require_once("phpmailer/class.phpmailer.php");

extract($_REQUEST);

$td = "border: 1px solid #dddddd;text-align:left;padding: 8px;";
$th = "border: 1px solid #dddddd;text-align:center;padding: 8px;background-color: #337ab7;color: white;";

$cabecaEmail = "
<style>
  table {
      border-collapse: collapse;
      width: 100%;
  }

  th, td {
      text-align: left;
      padding: 8px;
  }

  tr:nth-child(even){background-color: #f2f2f2}

  th {
      background-color: #337ab7;
      color: white;
  }
</style>
<html>    
  <table style='border-collapse: collapse;width: 100%;'>
    <tr>
      <td>
        <tr>
          <th style='border: 1px solid #dddddd;text-align:left;padding: 8px;background-color: #cc0000;color: white;'>COMUNICADO IMPORTANTE!</th>
        </tr>
        <tr>
          <td style='border: 1px solid #dddddd;text-align:left;padding: 8px;'>
            Já se conectou no sistema de Devolução hoje?<br>
            Não se esqueça. É importante que realizem as devidas analises e aprovações.<br>
            Lembrando que o maior objetivo da aprovação é negociar junto ao cliente para evitar devoluções.
          </td>
        </tr>
      </td>
    </tr>
  </table>
  <br>
  <table style='border-collapse: collapse;width: 100%;'>
    <tr>
      <th style='".$th."'>Protocolo</th>
      <th style='".$th."'>Código Cliente</th>
      <th style='".$th."'>Razão</th>
      <th style='".$th."'>Nota</th>
      <th style='".$th."'>Valor Total</th>
      <th style='".$th."'>Motivo Devolução</th>
      <th style='".$th."'>Divisão</th>
      <th style='".$th."'>CD</th>
    </tr>";

$rodapeEmail = "
  </table>
  <br>
  <p>E-mail enviado automaticamente em $data_envio, as $hora_envio, respostas ou dúvidas devem ser encaminhadas para fulano@servimed.com.br ou entrar em contato no ramal: 000-0000. <br> TI Servimed.</p>
</html>";

// Variáveis
$OcorrenciaDAO = new OcorrenciaDao();
$data_envio = date('d/m/Y');
$hora_envio = date('H:i:s');
$tentativasEnvioEmail = 0;

$Alertas = $OcorrenciaDAO->ObterOcorrenciasNaoResolvidasParaEnvioEmail();

$emailEnviado = ""; //Responsável por verificar se o email deve ser enviado ou não.
$emailEnvio = "";

echo $Alertas;

while ($dadosEnvio = oci_fetch_object($Alertas)) {

if(oci_num_rows($Alertas) % 2 === 0)
  $arquivo = "<tr style='background-color: #f2f2f2;'>";
else
  $arquivo = "<tr style='background-color: #fff;'>";

  $arquivo = $arquivo."
        <td style='".$td."'>$dadosEnvio->PROTOCOLO</td>
        <td style='".$td."'>$dadosEnvio->CODIGO_CLIENTE</td>
        <td style='".$td."'>$dadosEnvio->RAZAO</td>
        <td style='".$td."'>$dadosEnvio->ROTA</td>
        <td style='".$td."'>$dadosEnvio->VALOR_TOTAL</td>
        <td style='".$td."'>$dadosEnvio->MOTIVO_DEVOLUCAO</td>
        <td style='".$td."'>$dadosEnvio->DIVISAO</td>
        <td style='".$td."'>$dadosEnvio->CD</td>
      </tr>";
    

  $emailGerente = $dadosEnvio->EMAIL_GERENTE;
  if(($emailEnvio === "" && oci_num_rows($Alertas) === 1) ||
      $emailEnvio === $emailGerente){    

    $emailEnviado = $emailEnviado.$arquivo;
    $emailEnvio = $emailGerente;
    if(oci_num_rows($Alertas) != $dadosEnvio->TOTAL)
      continue;
  }
  
  if (strpos($dadosEnvio->EMAIL_GERENTE, '@servimed') === false) {
    $emailGerente = $dadosEnvio->EMAIL_GERENTE."@servimed.com.br";
  }


  function smtpmailer($para, $de, $de_nome, $assunto, $corpo) {
    global $error;  
    $mail = new PHPMailer();
    $mail->IsSMTP();
    $mail->SMTPDebug = false;
    $mail->SMTPAuth = true;
    $mail->SMTPSecure = 'ssl';
    $mail->Host = 'smtp.gmail.com';
    $mail->Port = 465;
    $mail->Username = 'SERVIMED@gmail.com';
    $mail->Password = 'SENHA';
    $mail->SetFrom($de);
    $mail->Subject = $assunto;
    $mail->Body = $corpo;
    $mail->AddAddress($para);
    $mail->IsHTML(true);   

    ini_set("memory_limit","100M");

    // if (!empty($_FILES['anexarTextos']['name']))	
    //   $mail->AddAttachment($_FILES['anexarTextos']['tmp_name'], $_FILES['anexarTextos']['name']);

    $tentativasEnvioEmail = 0;
    while($tentativasEnvioEmail <= 3)
    {
      try{
        if(!$mail->Send()) {
          $tentativasEnvioEmail++;
          echo "NÃO ENVIOU - Tentativa ".$tentativasEnvioEmail." <br>";
        } else {
          $tentativasEnvioEmail = 4;          
          echo "E-mail enviado! <br>";
        }
      }
      catch (Exception $ex) {
        $tentativasEnvioEmail++;
        echo "Erro - E-mail não enviado! ".$ex;
      }
    }
    $emailEnviado = $arquivo;
    $emailEnvio = $emailGerente;
    $tentativasEnvioEmail = 0;
  }

  try{
    if (smtpmailer('josue.junior@hqsplus.com.br', 'josuejunior.automacao@gmail.com', 'Aplicativo', 'Novo Cadastro Efetuado', $cabecaEmail.$emailEnviado.$rodapeEmail)) {
     echo 'Sucesso';
   }
   if (!empty($error)) echo 'Sem sucesso! '.$error;
   }
   catch(Exception $ex){
     echo 'Sem sucesso! '.$ex;
   }
}
?>