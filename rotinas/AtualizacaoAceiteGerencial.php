<?php

session_start();
include_once( "../includes/Dao/DaoSistema.class.php" );
include_once( "../includes/Dao/OcorrenciaDao.class.php" );

extract($_REQUEST);

// Variáveis
$OcorrenciaDAO = new OcorrenciaDao();
echo "Obtendo ocorrências ainda não aprovadas... <hr/><br><br>"; 
$ocorrencias = $OcorrenciaDAO->ObterOcorrenciasNaoAprovadasGerente();

while ($ocorrencia = oci_fetch_object($ocorrencias)) {

    $segundos = strtotime(date("Y-m-d")) - strtotime($ocorrencia->ENTRADA);
    $dias = $segundos / 86400;

    if(intval($dias) >= 2){
        // Atualiza na base de dados as ocorrências ainda não aprovadas
        $OcorrenciaDAO->AprovarPassoGerencial($ocorrencia->OCORRENCIA);
        echo "Ocorrência - <b>".$ocorrencia->OCORRENCIA.'</b> atualizada com sucesso! Estava a <b>'.$dias.'</b> dias sem aprovação!<br>'; 
    }
    else{
        echo "Ocorrência - <b>".$ocorrencia->OCORRENCIA.'</b> ainda não possui 2 dias de diferença! Possui apenas <b>'.$dias.'</b><br>'; 
    }
}
echo "<br><br> <hr/>Fim de leitura <br>"; 

?>