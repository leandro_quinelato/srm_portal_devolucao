<?php 
include_once( "../../../includes/Dao/DaoSistema.class.php" );
include_once( "../../../includes/OcorrenciaStatus.class.php" );
include_once( "../../../includes/Dao/OcorrenciaStatusDao.class.php" );

extract($_REQUEST);

$OcorrenciaStatus = new OcorrenciaStatus();
$OcorrenciaStatus->setCodigoOcorrencia($codOcorrencia);
$OcorrenciaStatusDao = new OcorrenciaStatusDao();
$OcorrenciaStatus = $OcorrenciaStatusDao->consultar($OcorrenciaStatus);
    

?>
<div class="row">
    <div class="col-md-12">
        <div class="table-responsive">
            <table class="table table-bordered table-striped table-hover TableNF">
                <thead>
                    <tr>
                        <th>Cód.</th>
                        <th>Status</th>
                        <th>Responsável</th>
                        <th>Data</th>
                    </tr>

                </thead>                               
                <tbody>
<?php
        foreach ($OcorrenciaStatus as $ObjOcorrenciaStatus) { 
?>
                    <tr>
                        <td><?php echo $ObjOcorrenciaStatus->getCodStatus(); ?></td>
                        <td><?php echo utf8_encode($ObjOcorrenciaStatus->getDescStatus()); ?></td>
                        <td><?php echo $ObjOcorrenciaStatus->getUsuarioNome();?></td>
                        <td><?php echo $ObjOcorrenciaStatus->getDataEntrada();?></td>

                    </tr>
<?php
        }
?>
                                                               
                </tbody>
            </table>
        </div>                                                
    </div>                                                                                   
</div> 