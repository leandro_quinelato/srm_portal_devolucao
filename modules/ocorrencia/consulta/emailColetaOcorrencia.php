<?php
extract($_REQUEST);
?>
<script>
    $(function () {
        $("#emailColeta").focus();
    })
</script>
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Protocolo: <?php echo $codOcorrencia ?></h4>
        </div>
        <div class="modal-body">
            <form id="formEnviarColeta">
                <input type="hidden" name="codOcorrencia" id="codOcorrencia" value="<?php echo $codOcorrencia?>">
                <div id="step-6">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Enviar documento para o e-mail:</label>                                        
                                <input type="text" placeholder="E-mail" class="form-control" maxlength="100" id="emailColeta" name="emailColeta">                                        
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <button data-dismiss="modal" class="btn btn-default" type="button">Cancelar</button>
            <button class="btn btn-primary" type="button" onclick="javascript:enviarEmailColeta();">Enviar</button>
        </div>                
    </div>
</div>