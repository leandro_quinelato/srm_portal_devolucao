<?php
include_once( "../../../includes/Dao/DaoSistema.class.php" );
include_once( "../../../includes/Dao/OcorrenciaDao.class.php" );
include_once( "../../../includes/Ocorrencia.class.php" );
include_once( "../../../includes/CalTipoOcorrencia.class.php" );
include_once( "../../../includes/Dao/TipoOcorrenciaDAO.class.php" );
include_once( "../../../includes/TipoDevolucao.class.php" );
include_once( "../../../includes/Dao/TipoDevolucaoDao.class.php" );
include_once( "../../../includes/Rota.class.php" );
include_once( "../../../includes/Dao/RotaDao.class.php" );


include_once( "../../../includes/Status.class.php" );
include_once( "../../../includes/Dao/StatusDao.class.php" );

include_once( "../../../includes/Passo.class.php" );
include_once( "../../../includes/Dao/PassoDao.class.php" );

include_once("DaoGlobal.class.php");
include_once("SistemaDao.class.php");
include_once("GlbSistema.class.php");
include_once("GlbSistemaPerfil.class.php");
include_once("GlbUsuario.class.php");
include_once("GlbColaborador.class.php");
include_once("GlbPessoa.class.php");
include_once("GlbEndereco.class.php");
include_once("GlbTelefone.class.php");
include_once("include_novo/GlbCentroDistribuicao.class.php");
include_once("include_novo/GlbTransportadora.class.php");
include_once("include_novo/GlbContatoTransportador.class.php");
include_once("include_novo/ContatoTransportadorDao.class.php");
include_once("GlbProgramaRotina.class.php");
include_once("GlbModuloPrograma.class.php");
include_once("GlbSistemaModulo.class.php");
include_once("PerfilUsuarioDao.class.php");
include_once("PerfilPermissaoDao.class.php");
include_once("integraSetSistemas.php");
$integra = new integraSetSistemas("conf.xml");
$ObjUsuario = unserialize($integra->getSistemaIntegra());
$ObjRotinas = $ObjUsuario->getObjSistemaPerfil()->getObjRotina();

include_once("include_novo/Tradutor.class.php");

$Tradutor = new Tradutor();


//echo '<pre>';
//print_r($_REQUEST);
//echo '</pre>';

/*

  [conCodOcorrencia] =>
  [dataInicial] => 04/11/2016
  [dataFinal] => 04/11/2016
  [conCodCliente] =>
  [conSerieDev] =>
  [conNotaDev] =>
  [conSerie] =>
  [conNota] =>
  [conRede] =>
  [ced_co_numero] => 48
  [rot_co_numero] => 2D
  [valorDevInicial] =>
  [valorDevFinal] =>
  [statusOcorrencia] => 3
  [passoOcorrencia] => 1
  [tipoDevolucao] => T
  [acao] => LISOCO

 */

extract($_REQUEST);

if ($acao == "LISOCO") {

    $Ocorrencia = new Ocorrencia();
    $OcorrenciaDao = new OcorrenciaDao();

    $Ocorrencia->setCodigoDevolucao($conCodOcorrencia);
    $Ocorrencia->getGlbCliente()->setClienteCodigo(trim($conCodCliente));
    $Ocorrencia->setSerieNfDevolucao(trim($conSerieDev));
    $Ocorrencia->setNotaNfDevolucao(trim($conNotaDev));
    $Ocorrencia->setCedis($ced_co_numero);
    $Ocorrencia->getObjRota()->setCodigoRota($rot_co_numero);
    $Ocorrencia->setStatusOcorrencia($statusOcorrencia);
    $Ocorrencia->setPassoOcorrencia($passoOcorrencia);
    $Ocorrencia->setTipoOcorrencia($tipoDevolucao);

    if ($valorDevInicial != "" && $valorDevFinal == "") {
        $valorDevFinal = $valorDevInicial;
    } else if ($valorDevFinal != "" && $valorDevInicial == "") {
        $valorDevInicial = $valorDevFinal;
    }
        //echo $valorDevInicial .'   ---   '. $valorDevFinal;
    $retorno = $OcorrenciaDao->consultaOcorrenciaPrincipal($Ocorrencia, $dataInicial, $dataFinal, $conSerie, $conNota, $conRede, $valorDevInicial, $valorDevFinal, $ObjUsuario);
    ?>


    <div class="table-responsive">
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th></th>
                    <th>E-mail coleta</th>
                    <th>Notas Venda</th>            
                    <th>Protocolo</th>
                    <th>Data cadastro</th>
                    <th>Passo</th>
                    <th>Status</th>
                    <th>Cadastrante</th>
                    <th>Usuário</th>
                    <th>CD</th>
					<th>Rota</th>
                    <th>Motivo</th>
                    <th>Tipo</th>
                    <th>Total</th>
                </tr>
            </thead>                               
            <tbody>

                <?php
                while ($dados = oci_fetch_object($retorno)) {
                    ?>
                    <tr>
                        <td><a class="fa fa-edit" style="cursor: pointer" onclick="javascript:detalheOcorrencia(<?php echo $dados->DOC_CO_NUMERO; ?>);" ></a></td>

                        <?php
                        if ($dados->DPA_CO_NUMERO == 2) {
                            ?>
                            <td><button type="button" class="fa fa-envelope"  onclick="javascript:emailColeta(<?php echo $dados->DOC_CO_NUMERO; ?>);"></button></td>
                            <?php
                        } else {
                            ?>      
                            <td><button type="button" class="fa fa-envelope"  onclick="javascript:alert('E-mail de Coleta é enviado somente no passo TRANSPORTADOR');"></button></td>    
                                <?php
                            }
                            ?>
                        <td> <span id="sinalMenos_<?=$dados->DOC_CO_NUMERO?>" onclick="fecharLinha(<?=$dados->DOC_CO_NUMERO?>)">(-)</span> 
                                <span id="sinalMais_<?=$dados->DOC_CO_NUMERO?>" style="display:none;" onclick="abrirLinha(<?=$dados->DOC_CO_NUMERO?>)">(+)</span> </td>
                        <td><?php echo $dados->DOC_CO_NUMERO; ?></td>
                        <td><?php echo $dados->DATA_EXIBICAO; ?></td>
                        <td><?php echo utf8_encode($dados->DPA_NO_DESCRICAO); ?></td>
                        <td><?php echo utf8_encode($dados->DST_NO_DESCRICAO); ?></td>
                        <td><?php echo $dados->DOC_NO_DEPOCONTATO; ?></td>
                        <td><?php echo $dados->USU_NO_USERNAME; ?></td>
                        <td><?php echo $dados->CED_NO_CENTRO; ?></td>
						<td><?php echo $dados->ROT_CO_NUMERO; ?></td>
                        <td><?php echo $dados->TOC_NO_DESCRICAO; ?></td>
                        <td><?php echo utf8_encode($dados->TID_CO_DESCRICAO); ?></td>
                        <td><?php echo $Tradutor->formatar($dados->DOC_VR_VALOR, 'MOEDA'); ?></td>
                    </tr>
                    <tr id="trNotas_<?=$dados->DOC_CO_NUMERO?>">
                        <td colspan="17">
                            <table border="0">
                                <?php
                                $Ocorrencia->setCodigoDevolucao($dados->DOC_CO_NUMERO);
                                $NotasOrigem = $OcorrenciaDao->consultaNotasDevolucao($Ocorrencia);
                                if($NotasOrigem != null){
                                    foreach ($NotasOrigem as $NotaOrigem) {
                                ?>
                                <tr>
                                    <td width="100px"></td>
                                    <td>Série: <?=$NotaOrigem->getSerie();?></td>
                                    <td>Nota: <?=$NotaOrigem->getNota();?></td>
                                    <td>Data: <?=$NotaOrigem->getData();?></td>
                                </tr>
                                <?php
                                    }
                                }
                                ?>
                            </table>				
                        </td>
                    </tr>
                <?php
                }
                ?>
            </tbody>
        </table>
    </div>
    <script language="JavaScript">
        $(function () {
            datatables.init();
        });
    </script>
    
    <script language="JavaScript">
    function abrirLinha(cod){	        
        
        document.getElementById('sinalMais_'+cod).style.display = 'none';
        document.getElementById('sinalMenos_'+cod).style.display = 'block';
        document.getElementById('trNotas_'+cod).style.visibility = "visible";
    }
    function fecharLinha(cod){       
        
        document.getElementById('sinalMais_'+cod).style.display = 'block';
        document.getElementById('sinalMenos_'+cod).style.display = 'none';
        document.getElementById('trNotas_'+cod).style.visibility = "hidden";
    }	
    </script>
    <?php
} else {

    $Ocorrencia = new Ocorrencia();
    $OcorrenciaDao = new OcorrenciaDao();

    $Ocorrencia->setCodigoDevolucao($codOcorrencia);
    $Ocorrencia->getObjRota()->setCodigoRota($rotaAlterar);
    $Ocorrencia->setQntdVolumeDevolucao($volumeAlterar);
    $Ocorrencia->getTipoOcorrencia()->setCodTipoOcorrencia($motivoAlterar);
    $Ocorrencia->setDescricaoDevolucao(trim($descAlterar));

    if (!$OcorrenciaDao->alterar($Ocorrencia)) {

        die("ERRO");
    }
}
?>



