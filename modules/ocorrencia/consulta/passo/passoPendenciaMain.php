<?php

error_reporting(E_ERROR);

include_once( "../../../../includes/Dao/DaoSistema.class.php" );
include_once( "../../../../includes/DevNotaOrigemItem.class.php" );
include_once( "../../../../includes/Dao/DevNotaOrigemItemDao.class.php" );
include_once( "../../../../includes/Dao/OcorrenciaDao.class.php" );
include_once( "../../../../includes/Ocorrencia.class.php" );
include_once( "../../../../includes/CalTipoOcorrencia.class.php" );
include_once( "../../../../includes/Dao/TipoOcorrenciaDAO.class.php" );
include_once( "../../../../includes/TipoDevolucao.class.php" );
include_once( "../../../../includes/Dao/TipoDevolucaoDao.class.php" );
include_once( "../../../../includes/Rota.class.php" );
include_once( "../../../../includes/Dao/RotaDao.class.php" );

include_once( "../../../../includes/Status.class.php" );
include_once( "../../../../includes/Dao/StatusDao.class.php" );

include_once( "../../../../includes/Passo.class.php" );
include_once( "../../../../includes/Dao/PassoDao.class.php" );

include_once( "../../../../includes/OcorrenciaPasso.class.php" );
include_once( "../../../../includes/Dao/OcorrenciaPassoDao.class.php" );

include_once( "../../../../includes/Conferente.class.php" );
include_once( "../../../../includes/Dao/ConferenteDao.class.php" );

include_once( "../../../../includes/LogAcaoPasso.class.php" );
include_once( "../../../../includes/Dao/LogAcaoPassoDao.class.php" );

include_once( "../../../../includes/OcorrenciaStatus.class.php" );
include_once( "../../../../includes/Dao/OcorrenciaStatusDao.class.php" );

include_once("DaoGlobal.class.php");
include_once("SistemaDao.class.php");
include_once("GlbSistema.class.php");
include_once("GlbSistemaPerfil.class.php");
include_once("GlbUsuario.class.php");
include_once("GlbColaborador.class.php");
include_once("GlbPessoa.class.php");
include_once("GlbEndereco.class.php");
include_once("GlbTelefone.class.php");
include_once("include_novo/GlbCentroDistribuicao.class.php");
include_once("include_novo/GlbTransportadora.class.php");
include_once("include_novo/GlbContatoTransportador.class.php");
include_once("include_novo/ContatoTransportadorDao.class.php");
include_once("GlbProgramaRotina.class.php");
include_once("GlbModuloPrograma.class.php");
include_once("GlbSistemaModulo.class.php");
include_once("PerfilUsuarioDao.class.php");
include_once("PerfilPermissaoDao.class.php");
include_once("integraSetSistemas.php");
$integra = new integraSetSistemas("conf.xml");
$ObjUsuario = unserialize($integra->getSistemaIntegra());
$ObjRotinas = $ObjUsuario->getObjSistemaPerfil()->getObjRotina();

extract($_REQUEST);

//echo "<pre>";
//print_r($_REQUEST);
//echo "</pre>";
/*
  [codOcorrenciaPendente] => 16000823230001
  [codPassoPendente] => 2
  [solicitantePendente] => tetsw
  [departamentoPendente] => ti
  [descPendente] => lsdksdvodsjvjsdopjvopsd
 */
$Ocorrencia = new Ocorrencia();
$OcorrenciaDao = new OcorrenciaDao();
$OcorrenciaPasso = new OcorrenciaPasso();
$OcorrenciaPassoDao = new OcorrenciaPassoDao();
$LogAcaoPassoDao = new LogAcaoPassoDao();	
$DevLogAcaoPasso = new LogAcaoPasso();
$OcorrenciaStatusDao = new OcorrenciaStatusDao();
$OcorrenciaStatus = new OcorrenciaStatus();

//status por  passo
$tipoPasso = array();
$tipoPasso[4] = "D";
$tipoPasso[5] = "F";	

$Ocorrencia->setCodigoDevolucao($codOcorrenciaPendente);

if($acao == "abrir"){
	
	$Ocorrencia->setStatusOcorrencia(13);

	if ($OcorrenciaDao->alterar($Ocorrencia)) {
		
		$OcorrenciaPasso->setCodigoOcorrencia($Ocorrencia->getCodigoDevolucao());
		$OcorrenciaPasso->setCodigoPasso($codPassoPendente);
		$OcorrenciaPasso->setUsuario($ObjUsuario->getUsuarioCodigo()); // 
		$OcorrenciaPasso->setIndicadorAprovado(2);
		$OcorrenciaPasso->setObservacao($descPendente);
		$OcorrenciaPassoDao->aprovarPasso($OcorrenciaPasso);		

		$DevLogAcaoPasso->setCodOcorrencia($Ocorrencia->getCodigoDevolucao());
		$DevLogAcaoPasso->setNomeSolicitante($solicitantePendente);
		$DevLogAcaoPasso->setNomeDepartamento($departamentoPendente);
		$DevLogAcaoPasso->setUsuario($ObjUsuario->getUsuarioCodigo());
		$DevLogAcaoPasso->setTipoUsuario('DEV');
		$DevLogAcaoPasso->setAcao('P');
		$DevLogAcaoPasso->setPasso($tipoPasso[$codPassoPendente]);
		$DevLogAcaoPasso->setAcaoDescricao($descPendente);
		$DevLogAcaoPasso->setPassoPortal($codPassoPendente);
		
		$LogAcaoPassoDao->inserir($DevLogAcaoPasso);
		
		/* Registrar o status na tabela de Ocorrencia Status*/
		$OcorrenciaStatus->setCodigoOcorrencia($Ocorrencia->getCodigoDevolucao());
		$OcorrenciaStatus->setUsuario($ObjUsuario->getUsuarioCodigo());
		$OcorrenciaStatus->setCodStatus($Ocorrencia->getStatusOcorrencia());
		$OcorrenciaStatusDao->registrarSatus($OcorrenciaStatus);
?>
	<script>
		detalheOcorrencia('<?php echo $Ocorrencia->getCodigoDevolucao(); ?>');
	</script>
<?php
	}
}else {
	
	//status por  passo
	$statusPasso = array();
	$statusPasso[4] = 8;
	$statusPasso[5] = 9;

	$Ocorrencia->setStatusOcorrencia($statusPasso[$codPassoPendente]); //indica o status da ocorrencia baseado no passo q se encontra , mudança solicitada de ultima hora (pode ser melhorada)
	$Ocorrencia->setFinalizadoDevolucao('NULL');

    if ($OcorrenciaDao->alterar($Ocorrencia)) {
		
		$OcorrenciaPasso->setCodigoOcorrencia($Ocorrencia->getCodigoDevolucao());
		$OcorrenciaPasso->setCodigoPasso($codPassoPendente);
		$OcorrenciaPassoDao->reativarPasso($OcorrenciaPasso);				

        $DevLogAcaoPasso->setCodOcorrencia($Ocorrencia->getCodigoDevolucao());
        $DevLogAcaoPasso->setNomeSolicitante($solicitantePendente);
        $DevLogAcaoPasso->setNomeDepartamento($departamentoPendente);
        $DevLogAcaoPasso->setUsuario($ObjUsuario->getUsuarioCodigo());
        $DevLogAcaoPasso->setTipoUsuario('DEV');
        $DevLogAcaoPasso->setAcao('A');
        $DevLogAcaoPasso->setPasso($tipoPasso[$codPassoPendente]);
        $DevLogAcaoPasso->setAcaoDescricao($descPendente);
        $DevLogAcaoPasso->setPassoPortal($codPassoPendente);
        
        $LogAcaoPassoDao->inserir($DevLogAcaoPasso);
        
        /* Registrar o status na tabela de Ocorrencia Status reativado 16*/
        $OcorrenciaStatus->setCodigoOcorrencia($Ocorrencia->getCodigoDevolucao());
        $OcorrenciaStatus->setUsuario($ObjUsuario->getUsuarioCodigo());
        $OcorrenciaStatus->setCodStatus(16);
        $OcorrenciaStatusDao->registrarSatus($OcorrenciaStatus);
        
        /* Registrar o status na tabela de Ocorrencia Status */ //indica o status da ocorrencia baseado no passo q se encontra , mudança solicitada de ultima hora (pode ser melhorada)
        $OcorrenciaStatus->setCodigoOcorrencia($Ocorrencia->getCodigoDevolucao());
        $OcorrenciaStatus->setUsuario($ObjUsuario->getUsuarioCodigo());
        $OcorrenciaStatus->setCodStatus($Ocorrencia->getStatusOcorrencia());
        $OcorrenciaStatusDao->registrarSatus($OcorrenciaStatus);
        
		?>

		<script>
			detalheOcorrencia('<?php echo $Ocorrencia->getCodigoDevolucao(); ?>');
		</script>
		<?php
        
    }

}
?>