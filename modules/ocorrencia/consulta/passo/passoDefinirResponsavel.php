<?php
include_once( "../../../../includes/Dao/DaoSistema.class.php" );
include_once( "../../../../includes/Dao/OcorrenciaDao.class.php" );
include_once( "../../../../includes/Ocorrencia.class.php" );
include_once( "../../../../includes/CalTipoOcorrencia.class.php" );
include_once( "../../../../includes/Dao/TipoOcorrenciaDAO.class.php" );
include_once( "../../../../includes/TipoDevolucao.class.php" );
include_once( "../../../../includes/Dao/TipoDevolucaoDao.class.php" );
include_once( "../../../../includes/Rota.class.php" );
include_once( "../../../../includes/Dao/RotaDao.class.php" );


include_once( "../../../../includes/Status.class.php" );
include_once( "../../../../includes/Dao/StatusDao.class.php" );

include_once( "../../../../includes/Passo.class.php" );
include_once( "../../../../includes/Dao/PassoDao.class.php" );

include_once( "../../../../includes/DevMotivoResponsabilidade.class.php" );
include_once( "../../../../includes/Dao/MotivoResponsabilidadeDao.class.php" );

include_once("DaoGlobal.class.php");
include_once("SistemaDao.class.php");
include_once("GlbSistema.class.php");
include_once("GlbSistemaPerfil.class.php");
include_once("GlbUsuario.class.php");
include_once("GlbColaborador.class.php");
include_once("GlbPessoa.class.php");
include_once("GlbEndereco.class.php");
include_once("GlbTelefone.class.php");
include_once("include_novo/GlbTransportadora.class.php");
include_once("include_novo/GlbContatoTransportador.class.php");
include_once("include_novo/ContatoTransportadorDao.class.php");
include_once("include_novo/GlbCentroDistribuicao.class.php");
include_once("GlbProgramaRotina.class.php");
include_once("GlbModuloPrograma.class.php");
include_once("GlbSistemaModulo.class.php");
include_once("PerfilUsuarioDao.class.php");
include_once("PerfilPermissaoDao.class.php");
include_once("integraSetSistemas.php");
$integra = new integraSetSistemas("conf.xml");
$ObjUsuario = unserialize($integra->getSistemaIntegra());
$ObjRotinas = $ObjUsuario->getObjSistemaPerfil()->getObjRotina();

include_once("include_novo/Tradutor.class.php");

$Tradutor = new Tradutor();

//echo '<pre>';
//print_r($_REQUEST);
//echo '</pre>';

extract($_REQUEST);

$Ocorrencia = new Ocorrencia();
$OcorrenciaDao = new OcorrenciaDao();
$Ocorrencia->setCodigoDevolucao($doc_co_numero);
$Ocorrencia->setTipoOcorrencia($tipoDevolucao);


$retornoDados = $OcorrenciaDao->consultaOcorrencia($Ocorrencia);
$ocorrenciaDados = oci_fetch_object($retornoDados);
?>
<h3 class="titulo_pagina">Defini&ccedil;&atilde;o de Responsabilidade sobre a devolu&ccedil;&atilde;o</h3>
<form id="frmResponsabilidade" name="frmResponsabilidade">
	<div class="row">
    	<div class="col-md-2 col-sm-3 col-xs-3">
        	<div class="form-group">
                <label>Ocorr&ecirc;ncia :</label><br>
                <div class="col-md-11 col-sm-12 col-xs-12">
                    <?php echo $doc_co_numero ?>
                </div>
            </div>
        </div>
    	<div class="col-md-2 col-sm-3 col-xs-3">
        	<div class="form-group">
                <label>Motivo Devolu&ccedil;&atilde;o:</label><br>
                <div class="col-md-11 col-sm-12 col-xs-12">
                    <?php echo $ocorrenciaDados->TOC_NO_DESCRICAO; ?>
                    <input type="hidden" id="motivoDevolver" name="motivoDevolver" value="<?php echo $ocorrenciaDados->TOC_NO_DESCRICAO; ?>">
                </div>
            </div>
        </div>
        
        <div class="col-md-4 col-sm-4 col-xs-3">
            <div class="form-group">
                <?php
                if ($ocorrenciaDados->DOC_TX_DESCRICAO) {
                    $txdescricao = $ocorrenciaDados->DOC_TX_DESCRICAO->load();
                }
                ?>
                <label>Descrição:</label><br>
                <div class="col-md-11 col-sm-12 col-xs-12">
                    <textarea class="form-control" rows="2" placeholder="Descrição" id="descDevolucao" name="descDevolucao"><?php echo utf8_encode($txdescricao); ?></textarea>
                </div>
            </div>
        </div>
    </div>
    
    <div id="div_combo_notas" class="row">
        <?php
        $notas = new DevNotaOrigem();
        $notas = $OcorrenciaDao->consultaNotasDevolucao($Ocorrencia);

        $cont = 0;
        foreach ($notas as $nota) {
            ?>      
            <div id="div_notas_origens">
                <?php
                $result_detalhe = $OcorrenciaDao->detalhesNotaVenda($nota->getSerie(), $nota->getNota(), $nota->getData(), $nota->getCodCliente());
                while ($detalheNota = oci_fetch_object($result_detalhe)) {

                    $MotivoResponsabilidadeDao = new MotivoResponsabilidadeDao();
                    $DevMotivoResponsabilidade = new DevMotivoResponsabilidade();
                    $DevMotivoResponsabilidade->setStatus("1");
                    $resultMtRespo = $MotivoResponsabilidadeDao->consultar($DevMotivoResponsabilidade);
                    ?>	
                    <div class="col-md-2 col-sm-3 col-xs-3">
                        <div class="form-group">
                            <label>Nota Origem:</label><br>
                            <div class="col-md-11 col-sm-12 col-xs-12">
                                <?php echo $detalheNota->CHDOC; ?>
                            </div>
                        </div>
                    </div>                    
                    <div class="col-md-2 col-sm-3 col-xs-3">
                        <div class="form-group">
                            <label>Origem de Venda:</label><br>
                            <div class="col-md-11 col-sm-12 col-xs-12">
                                <?php echo strtoupper($detalheNota->ORIGEM_VENDA); ?>
                            </div>
                        </div>
                    </div>                    
                    <div class="col-md-3 col-sm-3 col-xs-3">
                        <div class="form-group">
                            <label>Motivo Responsabilidade:</label><br>
                            <div class="col-md-11 col-sm-12 col-xs-12">
                                <select class="form-control selectpicker" name="trp_co_numero_<?php echo $cont; ?>" id="trp_co_numero_<?php echo $cont; ?>" onchange="javascript:industria_show('<?php echo $cont; ?>');verificaMotivo($('#motivoDevolver').val(),$('#trp_co_numero_<?php echo $cont; ?> :selected').text());">
                                <option></option>
                                <?php
                                while (OCIFetchInto($resultMtRespo, $rowMtRespo, OCI_ASSOC)) {
                                    ?>
                                    <option value="<?php echo $rowMtRespo['TRP_CO_NUMERO'] ?>" <?php
                                    if ($rowMtRespo['TRP_CO_NUMERO'] == $nota->getMotivoResponsabilidade()) {
                                        echo 'selected="selected"';
                                    }
                                    ?>  ><?php echo $rowMtRespo['TRP_NO_DESCRICAO'] ?></option>
                                        <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>                    
                    
                    <div id="dadosResponsavel">
                        <?php
                        $dadosResponsavel = $MotivoResponsabilidadeDao->consultar($DevMotivoResponsabilidade);
                        while (OCIFetchInto($dadosResponsavel, $resposavel, OCI_ASSOC)) {
                            ?>
                            <input type="hidden" name="trp_no_responsavel_<?php echo $resposavel['TRP_CO_NUMERO']; ?>_<?php echo $cont; ?>" id="trp_no_responsavel_<?php echo $resposavel['TRP_CO_NUMERO']; ?>_<?php echo $cont; ?>" value="<?php echo $resposavel['TRP_NO_RESPONSAVEL']; ?>">
                        <?php } ?>

                        <input type="hidden" name="tp_origem_venda_<?php echo $cont; ?>" id="tp_origem_venda_<?php echo $cont; ?>" value="<?php echo $detalheNota->TPORIGVENDA; ?>">
                    </div> 
                    <!--<p style="border-bottom:1px solid;margin: 0px"></p>-->

                    <?php
                    if ($nota->getOrigem() == "ADM") {
                        echo "<label>Responsavel: </label>  <label> ADMINISTRATIVO </label>";
                    }
                    ?>
                    <p>		
                        <?php
                        if ($nota->getOrigem() == "CLIE") {
                            echo "<label>Responsavel: </label><br>  <label>CLIENTE: </label>" . $detalheNota->CDCLIENTE . " - " . strtoupper($detalheNota->RAZAO_SOCIAL);
                        }
                        ?>
                        <input type="hidden" name="cli_co_numero_<?php echo $cont; ?>" id="cli_co_numero" value="<?php echo $detalheNota->CDCLIENTE; ?>" onchange="javascript:industria_hidden();">
                    </p>

                    <?php
                    if ($nota->getOrigem() == "DIRT") {
                        echo "<label>Responsavel: </label><br> <label> DIRETORIA </label>";
                    }
                    ?>

                    <?php
                    if ($nota->getOrigem() == "ESTQ") {
                        echo "<label>Responsavel: </label><br> <label> ESTOQUE </label>";
                    }
                    ?>
                    <p>		
                        <input type="hidden" name="gerente_<?php echo $cont; ?>" id="gerente" value="<?php echo $detalheNota->CDEQUIPE; ?>" />
                    </p>
                    <div id="testeIndus">
                        <?php
                        if ($nota->getOrigem() == "INDU") {
                            echo "<label>Responsavel: </label><br> <label> IND&Uacute;STRIA </label>";
                        }
                        ?>

                        <ul id="list-fabricante_<?php echo $cont; ?>"   class=" <?php
                        if ($nota->getOrigem() == "INDU") {
                            echo 'industria_visible';
                        } else {
                            echo 'industria_hidden';
                        }
                        ?>">
                            <label>IND&Uacute;STRIA <strong style="color:red;font-weight: normal;"></strong></label>
                            <li style="margin-left:-25px;_margin-left:0px;list-style: none;">
                                <?php
                                $resultFab = $OcorrenciaDao->industria();
                                ?>
                                <select  name="fab_co_numero_<?php echo $cont; ?>" id="fab_co_numero_<?php echo $cont; ?>">
                                    <option></option>
                                    <?php
                                    while (OCIFetchInto($resultFab, $row_fab, OCI_ASSOC)) {
                                        ?>
                                        <option value="<?php echo $row_fab['FAB_CO_NUMERO'] ?>"  <?php
                                        if ($nota->getResponsavel() == $row_fab['FAB_CO_NUMERO']) {
                                            echo 'selected="selected"';
                                        }
                                        ?> > <?php echo $row_fab['FAB_NO_RAZAO_SOCIAL'] ?></option>
                                                <?php
                                            }
                                            ?>
                                </select>
                            </li>
                        </ul>

                    </div>
                    <?php
                    if ($nota->getOrigem() == "TRAN") {
                        echo "<label>Responsavel: </label><br> <label> TRANSPORTADORA </label>";
                    }
                    ?>

                    <?php
                    if (substr($detalheNota->TPORIGVENDA, 0, 1) == 'T') {
                        $tpu_co_tipo_usuario = 2;
                        $result_operador = $controle_devolucao->consultaOperadorFaturamento($tpu_co_tipo_usuario, $detalheNota->CDOPERSERV);
                        OCIFetchInto($result_operador, $row_operador, OCI_ASSOC);
                        ?>	
                        <p>				
                            <?php
                            if ($row_operador['OPE_NO_OPERADOR'] == "") {
                                //select vendedor
                                $nomeOperador = $controle_devolucao->consultaNomeOperador($detalheNota->CDOPERSERV);
                            } else {
                                $nomeOperador = $row_operador['OPE_NO_OPERADOR'];
                            }



                            if ($nota->getOrigem() == $detalheNota->TPORIGVENDA) {
                                echo "<label>Responsavel: </label><br> <label>ORIGEM: </label> Televendas -" . $nomeOperador;
                            }
                            ?>
                            <input type="hidden" name="responsavel_nome_tele<?php echo $cont; ?>" id="responsavel_nome_tele" value="<?php echo $nomeOperador; /* 02/12/2015 */ ?>" />
                            <input type="hidden" name="responsavel_operador_tele<?php echo $cont; ?>" id="responsavel_operador" value="<?php echo $detalheNota->CDOPERSERV; ?>" />
                        </p>					
                        <?php
                    } elseif ($detalheNota->TPORIGVENDA == 'PI') {
                        $tpu_co_tipo_usuario = 2;
                        $result_operador = $controle_devolucao->consultaOperadorFaturamento($tpu_co_tipo_usuario, $detalheNota->CDOPERSERV);
                        OCIFetchInto($result_operador, $row_operador, OCI_ASSOC);

                        if ($nota->getOrigem() == $detalheNota->TPORIGVENDA) {
                            echo "<label>Responsavel: </label><br> <label>ORIGEM: </label> Televendas PI -" . $row_operador['OPE_NO_OPERADOR'];
                        }
                        ?>	
                        <p>				
                            <input type="hidden" name="nome_operador_pi_<?php echo $cont; ?>" id="nome_operador" value="<?php echo $row_operador['OPE_NO_OPERADOR']; ?>">
                            <input type="hidden" name="responsavel_operador_pi_<?php echo $cont; ?>" id="responsavel_operador" value="<?php echo $row_operador['OPE_CO_OPERADOR']; ?>">
                        </p>					
                        <?php
                    } elseif (substr(strtoupper($detalheNota->ORIGEM_VENDA), 0, 3) == 'EQU') {
                        $tpu_co_tipo_usuario = 3;
                        $result_operador = $controle_devolucao->consultaOperadorFaturamento($tpu_co_tipo_usuario, $detalheNota->CDOPERGLOBAL);
                        OCIFetchInto($result_operador, $row_operador, OCI_ASSOC);
                        ?>
                        <p>
                            <input type="hidden" name="responsavel_operador_global_<?php echo $cont; ?>" id="responsavel_operador" value="<?php echo $row_operador['OPE_CO_OPERADOR']; ?>">
                        </p>
                        <?php
                    } elseif (substr(strtoupper($detalheNota->ORIGEM_VENDA), 0, 4) == 'GLOB') {
                        echo "";
                    } elseif (substr(strtoupper($detalheNota->ORIGEM_VENDA), 0, 4) == 'MESA') {
                        echo "";
                    } elseif (substr(strtoupper($detalheNota->ORIGEM_VENDA), 0, 4) == 'PRIME' || substr(strtoupper($detalheNota->ORIGEM_VENDA), 0, 6) == 'VENDAS') {
                        echo "";
                    } elseif (substr($detalheNota->TPORIGVENDA, 0, 1) == 'O') {
                        $result_operador = $controle_devolucao->consultaOperadorHospitalar($detalheNota->CDOPERSERV, $nota->getData());
                        OCIFetchInto($result_operador, $row_operador, OCI_ASSOC);


                        if ($nota->getOrigem() == $detalheNota->TPORIGVENDA) {
                            echo "<label>Responsavel: </label><br> <label> ORIGEM: </label>CENTRAL HOSPITALAR -" . $row_operador['NOME'];
                        }
                        ?>
                        <p>
                            <input type="hidden" name="nome_operador_hosp_<?php echo $cont; ?>" id="nome_operador_hosp" value="<?php echo $row_operador['NOME']; ?>">
                            <input type="hidden" name="responsavel_operador_hosp_<?php echo $cont; ?>" id="responsavel_operador_hosp" value="<?php echo $row_operador['CDUSUARIO']; ?>">
                        </p>
                        <?php
                    } else {
                        ?>
                        <input type="hidden" name="responsavel_codigo_<?php echo $cont; ?>" id="responsavel_codigo_<?php echo $cont; ?>" value="<?php echo $detalheNota->TPORIGVENDA; ?>"/>
                        <?php
                    }
                    ?>
                    <p>     
                        <?php
                        if ($nota->getOrigem() == "REPR") {
                            echo "<label>Responsavel: </label><br> <label>SETOR: </label>" . $detalheNota->CDSETOR . ' - ' . $detalheNota->NOEQUIPE;
                        }
                        ?>

                        <input type="hidden" name="nome_setor_<?php echo $cont; ?>" id="nome_setor" value="<?php echo 'Setor:' . $detalheNota->CDSETOR . ' - ' . $detalheNota->NOEQUIPE; ?>">
                        <input type="hidden" name="rpr_co_numero_<?php echo $cont; ?>" id="rpr_co_numero" value="<?php echo $detalheNota->CDSETOR; ?>">
                        <input type="hidden" name="nto_nu_serie_<?php echo $cont; ?>" id="nto_nu_serie" value="<?php echo $nota->getSerie(); ?>">
                        <input type="hidden" name="nto_nu_nota_<?php echo $cont; ?>" id="nto_nu_nota" value="<?php echo $nota->getNota(); ?>">							
                    </p>							

                    <div class="col-md-4 col-sm-3 col-xs-3">
                        <div class="form-group">
                            <label>Descri&ccedil;&atilde;o:</label><br>
                            <div class="col-md-11 col-sm-12 col-xs-12">
                                <textarea class="form-control" rows="2" class="texto" placeholder="Descrição" cols="45" name="nto_tx_responsavel_<?php echo $cont; ?>" id="nto_tx_responsavel_<?php echo $cont; ?>"><?php
								if ($nota->getDescricaoResponsavel()) {
									echo $nota->getDescricaoResponsavel()->load();
								}
								?></textarea>
                                

                            </div>
                        </div>
                    </div>
                    <?php
                }
                $cont++;
                ?>
            </div>       
            <?php
        }
        ?>
        <input type="hidden" name="doc_co_numero" id="doc_co_numero" value="<?php echo $doc_co_numero ?>">
        <input type="hidden" name="cont" id="cont" value="<?php echo $cont ?>">    
    </div>
        <div class="row">
            <div class="col-md-2">
                <div class="form-group">
					<?php
                    if ($cont > 0) {
                        ?>
                    <input id="btnDefResp" type="button" value="Definir Responsabilidade" class="ui-state-default ui-corner-all button btn btn-primary" onclick="javascript:definicaoResponsabilidadeAutomatica(<?php echo $cont; ?>);">
						<?php
                    } else {
                        ?>
                        <strong class="msmerro">Desculpe, nota(s) de origem n&atilde;o existe no sistema W2 </strong>
                        <?php
                    }
                    ?>
        <!--<input type="button" value="Pular a defini&ccedil;&atilde;o de responsabilidade" class="ui-state-default ui-corner-all button" onclick="javascript:avancarFiscal()">-->
                </div>
            </div>                                                                                  
        </div>
    </center>
</form>
<div id="retornoDifinirMsg">
    
</div>

