<?php
error_reporting(E_ERROR);


include_once( "../../../../includes/Dao/DaoSistema.class.php" );
include_once( "../../../../includes/DevNotaOrigemItem.class.php" );
include_once( "../../../../includes/Dao/DevNotaOrigemItemDao.class.php" );
include_once( "../../../../includes/Dao/OcorrenciaDao.class.php" );
include_once( "../../../../includes/Ocorrencia.class.php" );
include_once( "../../../../includes/CalTipoOcorrencia.class.php" );
include_once( "../../../../includes/Dao/TipoOcorrenciaDAO.class.php" );
include_once( "../../../../includes/TipoDevolucao.class.php" );
include_once( "../../../../includes/Dao/TipoDevolucaoDao.class.php" );
include_once( "../../../../includes/Rota.class.php" );
include_once( "../../../../includes/Dao/RotaDao.class.php" );

include_once( "../../../../includes/Status.class.php" );
include_once( "../../../../includes/Dao/StatusDao.class.php" );

include_once( "../../../../includes/OcorrenciaStatus.class.php" );
include_once( "../../../../includes/Dao/OcorrenciaStatusDao.class.php" );

include_once( "../../../../includes/Passo.class.php" );
include_once( "../../../../includes/Dao/PassoDao.class.php" );

include_once( "../../../../includes/OcorrenciaPasso.class.php" );
include_once( "../../../../includes/Dao/OcorrenciaPassoDao.class.php" );

include_once( "../../../../includes/Conferente.class.php" );
include_once( "../../../../includes/Dao/ConferenteDao.class.php" );

include_once( "../../../../includes/OcorrenciaArquivos.class.php" );
include_once( "../../../../includes/Dao/OcorrenciaArquivosDao.class.php" );

include_once( "../../../utilitario/email/emailPortalDevolucao.class.php" );

include_once("DaoGlobal.class.php");
include_once("SistemaDao.class.php");
include_once("GlbSistema.class.php");
include_once("GlbSistemaPerfil.class.php");
include_once("GlbUsuario.class.php");
include_once("GlbColaborador.class.php");
include_once("GlbPessoa.class.php");
include_once("GlbEndereco.class.php");
include_once("GlbTelefone.class.php");
include_once("include_novo/GlbCentroDistribuicao.class.php");
include_once("include_novo/GlbTransportadora.class.php");
include_once("include_novo/GlbContatoTransportador.class.php");
include_once("include_novo/ContatoTransportadorDao.class.php");
include_once("GlbProgramaRotina.class.php");
include_once("GlbModuloPrograma.class.php");
include_once("GlbSistemaModulo.class.php");
include_once("PerfilUsuarioDao.class.php");
include_once("PerfilPermissaoDao.class.php");
include_once("integraSetSistemas.php");
$integra = new integraSetSistemas("conf.xml");
$ObjUsuario = unserialize($integra->getSistemaIntegra());
$ObjRotinas = $ObjUsuario->getObjSistemaPerfil()->getObjRotina();

extract($_REQUEST);

//echo "<pre>";
//print_r($_REQUEST);
//echo "</pre>"; //die();

$Passo = new Passo();
$Passo->setCodigo($codPasso);
$PassoDao = new PassoDao();
$Passo = $PassoDao->consultar($Passo);
if ($Passo[0]) {
    $Passo = $Passo[0];
} else {
    $Passo = new Passo();
}

$Ocorrencia = new Ocorrencia();
$OcorrenciaDao = new OcorrenciaDao();
$OcorrenciaPasso = new OcorrenciaPasso();
$OcorrenciaPassoDao = new OcorrenciaPassoDao();
$OcorrenciaStatusDao = new OcorrenciaStatusDao();


$Ocorrencia->setCodigoDevolucao($codOcorrencia);

//echo 'proxima sequencia:' . $proximoPasso = $Passo->getSeqFluxo() + 1;
//die();
switch ($Passo->getCodigo()) {
    case 1:

        $OcorrenciaPasso->setCodigoOcorrencia($Ocorrencia->getCodigoDevolucao());
        $OcorrenciaPasso->setCodigoPasso($Passo->getCodigo()); // 1 - gerente

        $OcorrenciaPasso->setUsuario($ObjUsuario->getUsuarioCodigo()); // 
        $OcorrenciaPasso->setIndicadorAprovado(1);
        $OcorrenciaPasso->setObservacao($descGerente);

        $OcorrenciaPassoDao->aprovarPasso($OcorrenciaPasso);

        //buscar proximo passo no fluxo e encaminhar
        $proximoPasso = $Passo->getSeqFluxo() + 1;

        $NextPasso = new Passo();
        $NextPasso->setSeqFluxo($proximoPasso);
        $NextPasso = $PassoDao->consultar($NextPasso);


        //passa para o proximo passo
        $OcorrenciaPasso->setCodigoOcorrencia($Ocorrencia->getCodigoDevolucao());
        $OcorrenciaPasso->setCodigoPasso($NextPasso[0]->getCodigo()); // 

        $OcorrenciaPassoDao->inserirPasso($OcorrenciaPasso);

        /* informa o passo para a tabela dev_ocorrencia */
        $Ocorrencia->setPassoOcorrencia($OcorrenciaPasso->getCodigoPasso());


        /*         * ********************status*************** */
        /* informa neste ponto que o status da ocorrencia é APROVADO(2) */
        $Ocorrencia->setStatusOcorrencia(2);
        /* Registrar o status na tabela de Ocorrencia Status */
        $OcorrenciaStatus = new OcorrenciaStatus();
        $OcorrenciaStatus->setCodigoOcorrencia($Ocorrencia->getCodigoDevolucao());
        $OcorrenciaStatus->setUsuario($ObjUsuario->getUsuarioCodigo());
        $OcorrenciaStatus->setCodStatus($Ocorrencia->getStatusOcorrencia());
        $OcorrenciaStatusDao->registrarSatus($OcorrenciaStatus);

        /*
          Quando a ocorrencia for do tipo: 3- INTEGRAL ATO DA ENTREGA
          Nao tem necessidade do cliente informar a nota de devolucao
          neste caso segue o processo para aguardando coleta

         */
        if ($tipoDevol == 3) {
            /* informa neste ponto que o status da ocorrencia é aguardando coleta(5) */
            $Ocorrencia->setStatusOcorrencia(5);
            /* Registrar o status na tabela de Ocorrencia Status */
            $OcorrenciaStatus = new OcorrenciaStatus();
            $OcorrenciaStatus->setCodigoOcorrencia($Ocorrencia->getCodigoDevolucao());
            $OcorrenciaStatus->setUsuario($ObjUsuario->getUsuarioCodigo());
            $OcorrenciaStatus->setCodStatus($Ocorrencia->getStatusOcorrencia());
            $OcorrenciaStatusDao->registrarSatus($OcorrenciaStatus);


            //salva arquivo(ordem coleta) na tabela DEV_OCORRENCIA_ARQUIVO
            $nomeOrdemSystem = "ordemColeta_{$Ocorrencia->getCodigoDevolucao()}.pdf";
            $OcorrenciaArquivosDao = new OcorrenciaArquivosDao();
            $OcorrenciaArquivos = new OcorrenciaArquivos();
            $OcorrenciaArquivos->setCodOcorrencia($Ocorrencia->getCodigoDevolucao());
            $OcorrenciaArquivos->setCodigoTipoArquivo(2); // ordem coleta 2
//            $OcorrenciaArquivos->setNomeArquivoCliente($nomeEspelhoSystem);
            $OcorrenciaArquivos->setNomeArquivoSystem($nomeOrdemSystem);
//            $OcorrenciaArquivos->setCodigoUsuario($ObjUsuario->getUsuarioCodigo());
            $OcorrenciaArquivos->setObservacaoArquivo("Ordem coleta para a ocorrência: " . $Ocorrencia->getCodigoDevolucao());
            $OcorrenciaArquivosDao->registrarArquivo($OcorrenciaArquivos);
        } else {

            /* informa neste ponto que o status da ocorrencia é AGUARDANDO NFD(3) */
            $Ocorrencia->setStatusOcorrencia(3);
            /* Registrar o status na tabela de Ocorrencia Status */
            $OcorrenciaStatus = new OcorrenciaStatus();
            $OcorrenciaStatus->setCodigoOcorrencia($Ocorrencia->getCodigoDevolucao());
            $OcorrenciaStatus->setUsuario($ObjUsuario->getUsuarioCodigo());
            $OcorrenciaStatus->setCodStatus($Ocorrencia->getStatusOcorrencia());
            $OcorrenciaStatusDao->registrarSatus($OcorrenciaStatus);
            //nesse ponto gera espelho nota
            //envia o espelho para o cliente E-MAIL
            //salva arquivo(espelho nota) na tabela DEV_OCORRENCIA_ARQUIVO
            $nomeEspelhoSystem = "espelhoNota_{$Ocorrencia->getCodigoDevolucao()}.pdf";
            $OcorrenciaArquivosDao = new OcorrenciaArquivosDao();
            $OcorrenciaArquivos = new OcorrenciaArquivos();
            $OcorrenciaArquivos->setCodOcorrencia($Ocorrencia->getCodigoDevolucao());
            $OcorrenciaArquivos->setCodigoTipoArquivo(3); // espelho nota 3
//            $OcorrenciaArquivos->setNomeArquivoCliente($nomeEspelhoSystem);
            $OcorrenciaArquivos->setNomeArquivoSystem($nomeEspelhoSystem);
//            $OcorrenciaArquivos->setCodigoUsuario($ObjUsuario->getUsuarioCodigo());
            $OcorrenciaArquivos->setObservacaoArquivo("Espelho de nota para a ocorrência: " . $Ocorrencia->getCodigoDevolucao());
            $OcorrenciaArquivosDao->registrarArquivo($OcorrenciaArquivos);
        }


//        $Ocorrencia->setStatusOcorrencia(5);
        
        $emailPortalDevolucao = new emailPortalDevolucao();


        if (!$OcorrenciaDao->alterar($Ocorrencia)) {
            die("<center><strong class='msmerro'>Desculpe, erro na aprova&ccedil;&atilde;o do passo </strong></center>");
        } else {
            if ($tipoDevol == 3) {
                $emailPortalDevolucao->enviaEmailAprovadoSemEspelho($Ocorrencia);
            } else {
                $emailPortalDevolucao->enviaEmailAprovadoComEspelho($Ocorrencia);
            }
        }

        break;

    case 2:

        $OcorrenciaPasso->setCodigoOcorrencia($Ocorrencia->getCodigoDevolucao());
        $OcorrenciaPasso->setCodigoPasso($Passo->getCodigo());
        $OcorrenciaPasso->setUsuario($ObjUsuario->getUsuarioCodigo()); // 
        $OcorrenciaPasso->setIndicadorAprovado(1);
        $OcorrenciaPasso->setObservacao($descTransportador);
        $OcorrenciaPasso->setQtdVolume($qtdVolumeTrans);

        $OcorrenciaPassoDao->aprovarPasso($OcorrenciaPasso);

        //buscar proximo passo no fluxo e encaminhar
        $proximoPasso = $Passo->getSeqFluxo() + 1;

        $NextPasso = new Passo();
        $NextPasso->setSeqFluxo($proximoPasso);
        $NextPasso = $PassoDao->consultar($NextPasso);

        //passa para o proximo passo
        $OcorrenciaPasso->setCodigoOcorrencia($Ocorrencia->getCodigoDevolucao());
        $OcorrenciaPasso->setCodigoPasso($NextPasso[0]->getCodigo()); // 

        $OcorrenciaPassoDao->inserirPasso($OcorrenciaPasso);

        /* informa o passo para a tabela dev_ocorrencia */
        $Ocorrencia->setPassoOcorrencia($OcorrenciaPasso->getCodigoPasso());


        /*         * ********************status*************** */
        /* informa neste ponto que o status da ocorrencia é COLETA REALIZADA(6) */
        $Ocorrencia->setStatusOcorrencia(6);
        /* Registrar o status na tabela de Ocorrencia Status */
        $OcorrenciaStatus = new OcorrenciaStatus();
        $OcorrenciaStatus->setCodigoOcorrencia($Ocorrencia->getCodigoDevolucao());
        $OcorrenciaStatus->setUsuario($ObjUsuario->getUsuarioCodigo());
        $OcorrenciaStatus->setCodStatus($Ocorrencia->getStatusOcorrencia());
        $OcorrenciaStatusDao->registrarSatus($OcorrenciaStatus);

        /*         * ********************status*************** */
        /* informa neste ponto que o status da ocorrencia é AGUARD. CONFERENCIA RECEBIMENTO (18) */
        $Ocorrencia->setStatusOcorrencia(18);
        /* Registrar o status na tabela de Ocorrencia Status */
        $OcorrenciaStatus = new OcorrenciaStatus();
        $OcorrenciaStatus->setCodigoOcorrencia($Ocorrencia->getCodigoDevolucao());
        $OcorrenciaStatus->setUsuario($ObjUsuario->getUsuarioCodigo());
        $OcorrenciaStatus->setCodStatus($Ocorrencia->getStatusOcorrencia());
        $OcorrenciaStatusDao->registrarSatus($OcorrenciaStatus);



        /* informa neste ponto que o status da ocorrencia é coleta realizada(6) ****incluir apos o log de status o status aguard. conf (7) */
//        $Ocorrencia->setStatusOcorrencia(6);

        if (!$OcorrenciaDao->alterar($Ocorrencia)) {
            die("<center><strong class='msmerro'>Desculpe, erro na aprova&ccedil;&atilde;o do passo </strong></center>");
        }

        break;

    case 3:

        $OcorrenciaPasso->setCodigoOcorrencia($Ocorrencia->getCodigoDevolucao());
        $OcorrenciaPasso->setCodigoPasso($Passo->getCodigo());
        $OcorrenciaPasso->setUsuario($ObjUsuario->getUsuarioCodigo()); //
        $OcorrenciaPasso->setIndicadorAprovado(1);
        $OcorrenciaPasso->setObservacao($descRecebimento);
        $OcorrenciaPasso->setQtdVolume($qtdVolumeRecebe);

        $OcorrenciaPassoDao->aprovarPasso($OcorrenciaPasso);

        //buscar proximo passo no fluxo e encaminhar
        $proximoPasso = $Passo->getSeqFluxo() + 1;

        $NextPasso = new Passo();
        $NextPasso->setSeqFluxo($proximoPasso);
        $NextPasso = $PassoDao->consultar($NextPasso);

        //passa para o proximo passo
        $OcorrenciaPasso->setCodigoOcorrencia($Ocorrencia->getCodigoDevolucao());
        $OcorrenciaPasso->setCodigoPasso($NextPasso[0]->getCodigo()); //

        $OcorrenciaPassoDao->inserirPasso($OcorrenciaPasso);

        /* informa o passo para a tabela dev_ocorrencia */
        $Ocorrencia->setPassoOcorrencia($OcorrenciaPasso->getCodigoPasso());

        /*         * ********************status*************** */
        /* informa neste ponto que o status da ocorrencia é COLETA REALIZADA(6)*/
        $Ocorrencia->setStatusOcorrencia(6);
        $OcorrenciaStatus = new OcorrenciaStatus();
        $OcorrenciaStatus->setCodigoOcorrencia($Ocorrencia->getCodigoDevolucao());
        $OcorrenciaStatus->setUsuario($ObjUsuario->getUsuarioCodigo());
        $OcorrenciaStatus->setCodStatus($Ocorrencia->getStatusOcorrencia());
        $OcorrenciaStatusDao->registrarSatus($OcorrenciaStatus);

        /*         * ********************status*************** */
        /* informa neste ponto que o status da ocorrencia é AGUARD CONFERENCIA FISICO x NF(7) */
        $Ocorrencia->setStatusOcorrencia(7);
        /* Registrar o status na tabela de Ocorrencia Status */
        $OcorrenciaStatus = new OcorrenciaStatus();
        $OcorrenciaStatus->setCodigoOcorrencia($Ocorrencia->getCodigoDevolucao());
        $OcorrenciaStatus->setUsuario($ObjUsuario->getUsuarioCodigo());
        $OcorrenciaStatus->setCodStatus($Ocorrencia->getStatusOcorrencia());
        $OcorrenciaStatusDao->registrarSatus($OcorrenciaStatus);

        if (!$OcorrenciaDao->alterar($Ocorrencia)) {
            die("<center><strong class='msmerro'>Desculpe, erro na aprova&ccedil;&atilde;o do passo </strong></center>");
        }

        break;

    case 4:

        $OcorrenciaPasso->setCodigoOcorrencia($Ocorrencia->getCodigoDevolucao());
        $OcorrenciaPasso->setCodigoPasso($Passo->getCodigo());
        $OcorrenciaPasso->setUsuario($ObjUsuario->getUsuarioCodigo()); // 
        $OcorrenciaPasso->setIndicadorAprovado(1);
        $OcorrenciaPasso->setObservacao($descDevolucao);

        $ConferenteDao = new ConferenteDao();

        $totalVolume = 0;
        for ($i = 0; count($volumeDevolucao) > $i; $i++) {
            $Conferente = new Conferente();
            $auxConferente = explode("_", $filtro_conferente[$i]);

            $Conferente->setCodOcorrencia($Ocorrencia->getCodigoDevolucao());
            $Conferente->setEmpresa($auxConferente[0]);
            $Conferente->setEstabelecimento($auxConferente[1]);
            $Conferente->setCracha($auxConferente[2]);
            $Conferente->setVolume($volumeDevolucao[$i]);
            $totalVolume += $volumeDevolucao[$i];
            $ConferenteDao->addConferenteOcorrencia($Conferente);
        }

        $Ocorrencia->setDataConferencia($dtConf);
        $Ocorrencia->setDataDevRecebimento($dtReceb);

        $OcorrenciaPasso->setQtdVolume($totalVolume); /*         * ********************* */

        $OcorrenciaPassoDao->aprovarPasso($OcorrenciaPasso);

        //buscar proximo passo no fluxo e encaminhar
        $proximoPasso = $Passo->getSeqFluxo() + 1;

        $NextPasso = new Passo();
        $NextPasso->setSeqFluxo($proximoPasso);
        $NextPasso = $PassoDao->consultar($NextPasso);

        //passa para o proximo passo
        $OcorrenciaPasso->setCodigoOcorrencia($Ocorrencia->getCodigoDevolucao());
        $OcorrenciaPasso->setCodigoPasso($NextPasso[0]->getCodigo()); // 

        $OcorrenciaPassoDao->inserirPasso($OcorrenciaPasso);

        /* informa o passo para a tabela dev_ocorrencia */
        $Ocorrencia->setPassoOcorrencia($OcorrenciaPasso->getCodigoPasso());
        
        
        /* informa neste ponto que o status da ocorrencia é aguard lancamento fiscal(9) * */
        $Ocorrencia->setStatusOcorrencia(9);
        
        /*         * ********************status*************** */
        /* informa neste ponto que o status da ocorrencia é CONFERENCIA REALIZADA(8) */
        $Ocorrencia->setStatusOcorrencia(8);
        /* Registrar o status na tabela de Ocorrencia Status */
        $OcorrenciaStatus = new OcorrenciaStatus();
        $OcorrenciaStatus->setCodigoOcorrencia($Ocorrencia->getCodigoDevolucao());
        $OcorrenciaStatus->setUsuario($ObjUsuario->getUsuarioCodigo());
        $OcorrenciaStatus->setCodStatus($Ocorrencia->getStatusOcorrencia());
        $OcorrenciaStatusDao->registrarSatus($OcorrenciaStatus);
        
        /*         * ********************status*************** */
        /* informa neste ponto que o status da ocorrencia é AGUARD LANCAMENTO FISCAL(9) 7 */
        $Ocorrencia->setStatusOcorrencia(9);
        /* Registrar o status na tabela de Ocorrencia Status */
        $OcorrenciaStatus = new OcorrenciaStatus();
        $OcorrenciaStatus->setCodigoOcorrencia($Ocorrencia->getCodigoDevolucao());
        $OcorrenciaStatus->setUsuario($ObjUsuario->getUsuarioCodigo());
        $OcorrenciaStatus->setCodStatus($Ocorrencia->getStatusOcorrencia());
        $OcorrenciaStatusDao->registrarSatus($OcorrenciaStatus);
        

        if (!$OcorrenciaDao->alterar($Ocorrencia)) {
            die("<center><strong class='msmerro'>Desculpe, erro na aprova&ccedil;&atilde;o do passo </strong></center>");
        }

        break;

    case 5:

        $OcorrenciaPasso->setCodigoOcorrencia($Ocorrencia->getCodigoDevolucao());
        $OcorrenciaPasso->setCodigoPasso($Passo->getCodigo()); // 1 - gerente

        $OcorrenciaPasso->setUsuario($ObjUsuario->getUsuarioCodigo()); // 
        $OcorrenciaPasso->setIndicadorAprovado(1);
        $OcorrenciaPasso->setObservacao($descFiscal);

        $OcorrenciaPassoDao->aprovarPasso($OcorrenciaPasso);

        //fiscal nao possui um proximo passo 
//        $proximoPasso = $Passo->getSeqFluxo() + 1;
//
//        $NextPasso = new Passo();
//        $NextPasso->setSeqFluxo($proximoPasso);
//        $NextPasso = $PassoDao->consultar($NextPasso);
//
//
//        //passa para o proximo passo
//        $OcorrenciaPasso->setCodigoOcorrencia($Ocorrencia->getCodigoDevolucao());
//        $OcorrenciaPasso->setCodigoPasso($NextPasso[0]->getCodigo()); // 
//
//        $OcorrenciaPassoDao->inserirPasso($OcorrenciaPasso);

        $Ocorrencia->setFinalizadoDevolucao(1);
        $Ocorrencia->setDocInTransmitido('N');
        /* informa neste ponto que o status da ocorrencia é credito liberado e depois finalizado */
//        $Ocorrencia->setStatusOcorrencia(10);
        
        /*         * ********************status*************** */
        /* informa neste ponto que o status da ocorrencia é CREDITO LIBERADO(10) */
        $Ocorrencia->setStatusOcorrencia(10);
        /* Registrar o status na tabela de Ocorrencia Status */
        $OcorrenciaStatus = new OcorrenciaStatus();
        $OcorrenciaStatus->setCodigoOcorrencia($Ocorrencia->getCodigoDevolucao());
        $OcorrenciaStatus->setUsuario($ObjUsuario->getUsuarioCodigo());
        $OcorrenciaStatus->setCodStatus($Ocorrencia->getStatusOcorrencia());
        $OcorrenciaStatusDao->registrarSatus($OcorrenciaStatus);
        
        /*         * ********************status*************** */
        /* informa neste ponto que o status da ocorrencia é FINALIZADO(11) */
        $Ocorrencia->setStatusOcorrencia(11);
        /* Registrar o status na tabela de Ocorrencia Status */
        $OcorrenciaStatus = new OcorrenciaStatus();
        $OcorrenciaStatus->setCodigoOcorrencia($Ocorrencia->getCodigoDevolucao());
        $OcorrenciaStatus->setUsuario($ObjUsuario->getUsuarioCodigo());
        $OcorrenciaStatus->setCodStatus($Ocorrencia->getStatusOcorrencia());
        $OcorrenciaStatusDao->registrarSatus($OcorrenciaStatus);
        
        
        

        if (!$OcorrenciaDao->alterar($Ocorrencia)) {
            die("<center><strong class='msmerro'>Desculpe, erro na aprova&ccedil;&atilde;o do passo </strong></center>");
        }
        
        
        /* enviar  e-mail finalizado*/
        $emailPortalDevolucao = new emailPortalDevolucao();
        $emailPortalDevolucao->enviaEmailOcorrenciaFinalizada($Ocorrencia);
        
        break;
}
/*
  //quando for passo devolucao
  //if($tpu_co_numero==4 && $finalizacao!='yes')
  //{
  //?>
  <script>
  responsavelDevolucao('//<?php echo $oco_co_numero; ?>',<?php echo $cli_co_numero; ?>);
  </script>

  //<?php
  ////die();
  //} */
?>
<script>
    alert('Ocorrência: <?php echo $codOcorrencia; ?>, Aprovada com Sucesso!');
    
    <?php
    if ($Passo->getCodigo() == 4){
    ?>
        responsavelDevolucao(<?php echo $codOcorrencia; ?>,<?php echo $codClienteDev; ?>);
    <?php    
    }else{
    ?>
    $('#tab-fluxo').load('modules/ocorrencia/consulta/detalheFluxo.php?codOcorrencia='+ <?php echo $codOcorrencia ?>);
    
    <?php
    }
    ?>
</script>
<!--<div class="msmsucesso" style="text-decoration: ">
    <center>
        A&ccedil;&atilde;o realizada com Sucesso
        <br />
        <br />
        <br />
        <br />
        <a  class="buttonFinish btn btn-success pull-right btn-xs" style="display: block;" onclick="javascript: aprovarOcorrenciaPasso(<?php echo $codOcorrencia; ?>,<?php echo $codPasso; ?>, 'Recebimento');">voltar</a>
    </center>
</div>-->


