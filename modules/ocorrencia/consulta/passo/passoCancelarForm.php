<?php
session_start();
error_reporting(E_ERROR);
include_once( "../../../../includes/Dao/DaoSistema.class.php" );
include_once( "../../../../includes/DevNotaOrigemItem.class.php" );
include_once( "../../../../includes/Dao/DevNotaOrigemItemDao.class.php" );
include_once( "../../../../includes/Dao/OcorrenciaDao.class.php" );
include_once( "../../../../includes/Ocorrencia.class.php" );
include_once( "../../../../includes/CalTipoOcorrencia.class.php" );
include_once( "../../../../includes/Dao/TipoOcorrenciaDAO.class.php" );
include_once( "../../../../includes/TipoDevolucao.class.php" );
include_once( "../../../../includes/Dao/TipoDevolucaoDao.class.php" );
include_once( "../../../../includes/Rota.class.php" );
include_once( "../../../../includes/Dao/RotaDao.class.php" );


include_once( "../../../../includes/Status.class.php" );
include_once( "../../../../includes/Dao/StatusDao.class.php" );

include_once( "../../../../includes/Passo.class.php" );
include_once( "../../../../includes/Dao/PassoDao.class.php" );

include_once( "../../../../includes/OcorrenciaPasso.class.php" );
include_once( "../../../../includes/Dao/OcorrenciaPassoDao.class.php" );

include_once( "../../../../includes/LogAcaoPasso.class.php" );
include_once( "../../../../includes/Dao/LogAcaoPassoDao.class.php" );

include_once( "../../../../includes/MotivoReprova.class.php" );
include_once( "../../../../includes/Dao/MotivoReprovaDao.class.php" );


$MotivoReprovaDao = new MotivoReprovaDao();
$MotivosReprova = $MotivoReprovaDao->consultar();

//echo "<pre>";
//print_r($_REQUEST);
//echo "</pre>";

extract($_REQUEST);
?>
<script>
//    $(function () {
//        $("#emailColeta").focus();
//    })
</script>
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" aria-label="Close" onclick="javascript: $('#modalCancelarDevolucao').modal('hide');" ><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Protocolo: <?php echo $codOcorrencia ?></h4>
        </div>
        <div class="modal-body">
            <label>Para cancelar a ocorrência, informe os dados abaixo:</label>
            <form id="formCancelar">
                <input type="hidden" name="codOcorrenciaCancelar" id="codOcorrenciaCancelar" value="<?php echo $codOcorrencia ?>">
                <input type="hidden" name="codPassoCancelar" id="codPassoCancelar" value="<?php echo $codPasso ?>">
                <input type="hidden" placeholder="<?php $currentDateTime = date('d/m/Y'); echo $currentDateTime;?>" class="form-control" maxlength="60" id="dataInicioCancelar" name="dataInicioCancelar" readonly>
                <div class="row">

                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Solicitante:</label>                                        
                            <input type="text" placeholder="Solicitante" class="form-control" maxlength="60" id="solicitanteCancelar" name="solicitanteCancelar">                                        
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Departamento:</label>                                        
                            <input type="text" placeholder="Departamento" class="form-control" maxlength="60" id="departamentoCancelar" name="departamentoCancelar">                                        
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Descrição</label>
                            <textarea class="form-control" rows="3" placeholder="Descrição" id="descCancelar" name="descCancelar"></textarea>                                   
                        </div>
                    </div>
                </div>
            </form>
            <div id="retornoCancelar">

            </div>

        </div>
        <div class="modal-footer">
            <button class="btn btn-default" type="button" onclick="javascript: $('#modalCancelarDevolucao').modal('hide');">Cancelar</button>
            <button class="btn btn-primary" type="button" onclick="javascript: cancelarOcorrencia();">Confirmar</button>
        </div>                
    </div>
</div>