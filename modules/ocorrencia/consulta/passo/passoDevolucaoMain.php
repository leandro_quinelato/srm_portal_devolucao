<?php

error_reporting(E_ERROR);

include_once( "../../../../includes/Dao/DaoSistema.class.php" );
include_once( "../../../../includes/DevNotaOrigemItem.class.php" );
include_once( "../../../../includes/Dao/DevNotaOrigemItemDao.class.php" );
include_once( "../../../../includes/Dao/OcorrenciaDao.class.php" );
include_once( "../../../../includes/Ocorrencia.class.php" );
include_once( "../../../../includes/CalTipoOcorrencia.class.php" );
include_once( "../../../../includes/Dao/TipoOcorrenciaDAO.class.php" );
include_once( "../../../../includes/TipoDevolucao.class.php" );
include_once( "../../../../includes/Dao/TipoDevolucaoDao.class.php" );
include_once( "../../../../includes/Rota.class.php" );
include_once( "../../../../includes/Dao/RotaDao.class.php" );

include_once( "../../../../includes/Status.class.php" );
include_once( "../../../../includes/Dao/StatusDao.class.php" );

include_once( "../../../../includes/OcorrenciaStatus.class.php" );
include_once( "../../../../includes/Dao/OcorrenciaStatusDao.class.php" );

include_once( "../../../../includes/Passo.class.php" );
include_once( "../../../../includes/Dao/PassoDao.class.php" );

include_once( "../../../../includes/OcorrenciaPasso.class.php" );
include_once( "../../../../includes/Dao/OcorrenciaPassoDao.class.php" );

include_once( "../../../../includes/Conferente.class.php" );
include_once( "../../../../includes/Dao/ConferenteDao.class.php" );

include_once( "../../../../includes/OcorrenciaArquivos.class.php" );
include_once( "../../../../includes/Dao/OcorrenciaArquivosDao.class.php" );

include_once( "../../../utilitario/email/emailPortalDevolucao.class.php" );

include_once("DaoGlobal.class.php");
include_once("SistemaDao.class.php");
include_once("GlbSistema.class.php");
include_once("GlbSistemaPerfil.class.php");
include_once("GlbUsuario.class.php");
include_once("GlbColaborador.class.php");
include_once("GlbPessoa.class.php");
include_once("GlbEndereco.class.php");
include_once("GlbTelefone.class.php");
include_once("include_novo/GlbCentroDistribuicao.class.php");
include_once("include_novo/GlbTransportadora.class.php");
include_once("include_novo/GlbContatoTransportador.class.php");
include_once("include_novo/ContatoTransportadorDao.class.php");
include_once("GlbProgramaRotina.class.php");
include_once("GlbModuloPrograma.class.php");
include_once("GlbSistemaModulo.class.php");
include_once("PerfilUsuarioDao.class.php");
include_once("PerfilPermissaoDao.class.php");
include_once("integraSetSistemas.php");
$integra = new integraSetSistemas("conf.xml");
$ObjUsuario = unserialize($integra->getSistemaIntegra());
$ObjRotinas = $ObjUsuario->getObjSistemaPerfil()->getObjRotina();

extract($_REQUEST);

//ECHO "<PRE>";
//print_r($_REQUEST);
//ECHO "</PRE>";

$Ocorrencia = new Ocorrencia();
$OcorrenciaDao = new OcorrenciaDao();
$OcorrenciaPasso = new OcorrenciaPasso();
$OcorrenciaPassoDao = new OcorrenciaPassoDao();
$OcorrenciaStatusDao = new OcorrenciaStatusDao();
$ConferenteDao = new ConferenteDao();

$Ocorrencia->setCodigoDevolucao($codOcorrencia);


$OcorrenciaPasso->setCodigoOcorrencia($Ocorrencia->getCodigoDevolucao());
$OcorrenciaPasso->setCodigoPasso(4);
//$OcorrenciaPasso->setObservacao($descDevolucao);

if ($acao == 'excluir'){
	//variaveis necessarias para excluir conferente [$codOcorrencia, $cracha, $volumeExcluir, $acao]
	
	//remove conferente
	$ConferenteDao->excluirConferenteOcorrencia($Ocorrencia->getCodigoDevolucao(), $cracha);
	
	//atuliza o volume
	$vetorObjs = $OcorrenciaPassoDao->consultarOcorrenciaPasso($OcorrenciaPasso);
	$OcorrenciaPasso = $vetorObjs[0];
	$totalVolume = $OcorrenciaPasso->getQtdVolume() - $volumeExcluir; //recalcula o volume.
	$OcorrenciaPasso->setQtdVolume($totalVolume);
	
	if ($OcorrenciaPassoDao->alterarVolume($OcorrenciaPasso)){ //atualiza o volume no passo devolução.
		echo "<script>alert('Conferente Excluido com sucesso!')</script>";
	}
}else{

	$totalVolume = 0;
	for ($i = 0; count($volumeDevolucao) > $i; $i++) {
		$Conferente = new Conferente();
		$auxConferente = explode("_", $filtro_conferente[$i]);

		$Conferente->setCodOcorrencia($Ocorrencia->getCodigoDevolucao());
		$Conferente->setEmpresa($auxConferente[0]);
		$Conferente->setEstabelecimento($auxConferente[1]);
		$Conferente->setCracha($auxConferente[2]);
		$Conferente->setVolume($volumeDevolucao[$i]);
		$totalVolume += $volumeDevolucao[$i];
		$ConferenteDao->addConferenteOcorrencia($Conferente);
	}

	//echo "aquiiiiiiiiiiiiiiiiiii";
	$Ocorrencia->setDataConferencia($dtConf);
	$Ocorrencia->setDataDevRecebimento($dtReceb);

	$OcorrenciaPasso->setQtdVolume($totalVolume); /* * ********************* */

	$OcorrenciaPassoDao->alterarVolume($OcorrenciaPasso);
	if ($OcorrenciaDao->alterar($Ocorrencia)){
		echo "<script>alert('Dados alterados com sucesso!')</script>";
	}
}