<?php
error_reporting(E_ERROR);


include_once( "../../../../includes/Dao/DaoSistema.class.php" );
include_once( "../../../../includes/DevNotaOrigemItem.class.php" );
include_once( "../../../../includes/Dao/DevNotaOrigemItemDao.class.php" );
include_once( "../../../../includes/Dao/OcorrenciaDao.class.php" );
include_once( "../../../../includes/Ocorrencia.class.php" );
include_once( "../../../../includes/CalTipoOcorrencia.class.php" );
include_once( "../../../../includes/Dao/TipoOcorrenciaDAO.class.php" );
include_once( "../../../../includes/TipoDevolucao.class.php" );
include_once( "../../../../includes/Dao/TipoDevolucaoDao.class.php" );
include_once( "../../../../includes/Rota.class.php" );
include_once( "../../../../includes/Dao/RotaDao.class.php" );

include_once( "../../../../includes/Status.class.php" );
include_once( "../../../../includes/Dao/StatusDao.class.php" );

include_once( "../../../../includes/Passo.class.php" );
include_once( "../../../../includes/Dao/PassoDao.class.php" );

include_once( "../../../../includes/OcorrenciaPasso.class.php" );
include_once( "../../../../includes/Dao/OcorrenciaPassoDao.class.php" );

include_once( "../../../../includes/Conferente.class.php" );
include_once( "../../../../includes/Dao/ConferenteDao.class.php" );

include_once( "../../../../includes/LogAcaoPasso.class.php" );
include_once( "../../../../includes/Dao/LogAcaoPassoDao.class.php" );

include_once( "../../../../includes/OcorrenciaStatus.class.php" );
include_once( "../../../../includes/Dao/OcorrenciaStatusDao.class.php" );

include_once("DaoGlobal.class.php");
include_once("SistemaDao.class.php");
include_once("GlbSistema.class.php");
include_once("GlbSistemaPerfil.class.php");
include_once("GlbUsuario.class.php");
include_once("GlbColaborador.class.php");
include_once("GlbPessoa.class.php");
include_once("GlbEndereco.class.php");
include_once("GlbTelefone.class.php");
include_once("include_novo/GlbCentroDistribuicao.class.php");
include_once("include_novo/GlbTransportadora.class.php");
include_once("include_novo/GlbContatoTransportador.class.php");
include_once("include_novo/ContatoTransportadorDao.class.php");
include_once("GlbProgramaRotina.class.php");
include_once("GlbModuloPrograma.class.php");
include_once("GlbSistemaModulo.class.php");
include_once("PerfilUsuarioDao.class.php");
include_once("PerfilPermissaoDao.class.php");
include_once("integraSetSistemas.php");
$integra = new integraSetSistemas("conf.xml");
$ObjUsuario = unserialize($integra->getSistemaIntegra());
$ObjRotinas = $ObjUsuario->getObjSistemaPerfil()->getObjRotina();

extract($_REQUEST);

//echo "<pre>";
//print_r($_REQUEST);
//echo "</pre>";
$OcorrenciaStatusDao = new OcorrenciaStatusDao();

$Passo = new Passo();
$Passo->setCodigo($codPasso);
$PassoDao = new PassoDao();
$Passo = $PassoDao->consultar($Passo);
if ($Passo[0]) {
    $Passo = $Passo[0];
} else {
    $Passo = new Passo();
}

$Ocorrencia = new Ocorrencia();
$OcorrenciaDao = new OcorrenciaDao();
$OcorrenciaPasso = new OcorrenciaPasso();
$OcorrenciaPassoDao = new OcorrenciaPassoDao();


$Ocorrencia->setCodigoDevolucao($codOcorrencia);

//echo 'proxima sequencia:' . $proximoPasso = $Passo->getSeqFluxo() + 1;

/* Criando objetos para gravação dos logs de reprovação */
$DevLogAcaoPasso = new LogAcaoPasso();
$LogAcaoPassoDao = new LogAcaoPassoDao();


//die();
switch ($Passo->getCodigo()) {
    case 1:

        $OcorrenciaPasso->setCodigoOcorrencia($Ocorrencia->getCodigoDevolucao());
        $OcorrenciaPasso->setCodigoPasso($Passo->getCodigo()); // 1 - gerente
        $OcorrenciaPasso->setUsuario($ObjUsuario->getUsuarioCodigo()); // 
        $OcorrenciaPasso->setIndicadorAprovado(0);
        $OcorrenciaPasso->setObservacao($descGerente);

        $OcorrenciaPassoDao->aprovarPasso($OcorrenciaPasso);

        //informar para o log

        $DevLogAcaoPasso->setCodOcorrencia($Ocorrencia->getCodigoDevolucao());
        $DevLogAcaoPasso->setNomeSolicitante($solicitanteGerente);
        $DevLogAcaoPasso->setNomeDepartamento($departamentoGerente);
        $DevLogAcaoPasso->setUsuario($ObjUsuario->getUsuarioCodigo());
        $DevLogAcaoPasso->setTipoUsuario('DEV');
        $DevLogAcaoPasso->setAcao('D');
        $DevLogAcaoPasso->setPasso('G');
        $DevLogAcaoPasso->setPassoPortal($Passo->getCodigo());
        $DevLogAcaoPasso->setAcaoDescricao($descGerente);
        $DevLogAcaoPasso->setCodReprova($motivoReprovaGerente);

        $LogAcaoPassoDao->inserir($DevLogAcaoPasso);


        /* Informar a tabela Ocorrencia que foi reprovada atualizando o campo doc_in_finalizado */
        $Ocorrencia->setFinalizadoDevolucao(0);

        /* informa neste ponto que o status da ocorrencia é coleta Reprovado(12) *** */
                /*         * ********************status*************** */
        $Ocorrencia->setStatusOcorrencia(12);
        /* Registrar o status na tabela de Ocorrencia Status */
        $OcorrenciaStatus = new OcorrenciaStatus();
        $OcorrenciaStatus->setCodigoOcorrencia($Ocorrencia->getCodigoDevolucao());
        $OcorrenciaStatus->setUsuario($ObjUsuario->getUsuarioCodigo());
        $OcorrenciaStatus->setCodStatus($Ocorrencia->getStatusOcorrencia());
        $OcorrenciaStatusDao->registrarSatus($OcorrenciaStatus);

        if (!$OcorrenciaDao->alterar($Ocorrencia)) {
            die("<center><strong class='msmerro'>Desculpe, erro na aprova&ccedil;&atilde;o do passo </strong></center>");
        }

        break;

    case 2:
        /* Reprovar */
        $OcorrenciaPasso->setCodigoOcorrencia($Ocorrencia->getCodigoDevolucao());
        $OcorrenciaPasso->setCodigoPasso($Passo->getCodigo());
        $OcorrenciaPasso->setUsuario($ObjUsuario->getUsuarioCodigo()); // 
        $OcorrenciaPasso->setIndicadorAprovado(0);
        $OcorrenciaPasso->setObservacao($descTransportador);

        $OcorrenciaPassoDao->aprovarPasso($OcorrenciaPasso);

        //informar para o log

        $DevLogAcaoPasso->setCodOcorrencia($Ocorrencia->getCodigoDevolucao());
        $DevLogAcaoPasso->setNomeSolicitante($solicitanteTransp);
        $DevLogAcaoPasso->setNomeDepartamento($departamentoTransp);
        $DevLogAcaoPasso->setUsuario($ObjUsuario->getUsuarioCodigo());
        $DevLogAcaoPasso->setTipoUsuario('DEV');
        $DevLogAcaoPasso->setAcao('D');
        $DevLogAcaoPasso->setPasso('P');
        $DevLogAcaoPasso->setPassoPortal($Passo->getCodigo());
        $DevLogAcaoPasso->setAcaoDescricao($descTransportador);
        $DevLogAcaoPasso->setCodReprova($motivoReprovaTransp);

        $LogAcaoPassoDao->inserir($DevLogAcaoPasso);


        /* Informar a tabela Ocorrencia que foi reprovada atualizando o campo doc_in_finalizado */
        $Ocorrencia->setFinalizadoDevolucao(0);

        /* informa neste ponto que o status da ocorrencia é coleta Reprovado(12) *** */
                /*         * ********************status*************** */
        $Ocorrencia->setStatusOcorrencia(12);
        /* Registrar o status na tabela de Ocorrencia Status */
        $OcorrenciaStatus = new OcorrenciaStatus();
        $OcorrenciaStatus->setCodigoOcorrencia($Ocorrencia->getCodigoDevolucao());
        $OcorrenciaStatus->setUsuario($ObjUsuario->getUsuarioCodigo());
        $OcorrenciaStatus->setCodStatus($Ocorrencia->getStatusOcorrencia());
        $OcorrenciaStatusDao->registrarSatus($OcorrenciaStatus);

        if (!$OcorrenciaDao->alterar($Ocorrencia)) {
            die("<center><strong class='msmerro'>Desculpe, erro na reprova&ccedil;&atilde;o do passo </strong></center>");
        }

        break;

    case 3:

        $OcorrenciaPasso->setCodigoOcorrencia($Ocorrencia->getCodigoDevolucao());
        $OcorrenciaPasso->setCodigoPasso($Passo->getCodigo());
        $OcorrenciaPasso->setUsuario($ObjUsuario->getUsuarioCodigo()); // 
        $OcorrenciaPasso->setIndicadorAprovado(0);
        $OcorrenciaPasso->setObservacao($descRecebimento);

        $OcorrenciaPassoDao->aprovarPasso($OcorrenciaPasso);

        //informar para o log

        $DevLogAcaoPasso->setCodOcorrencia($Ocorrencia->getCodigoDevolucao());
        $DevLogAcaoPasso->setNomeSolicitante($solicitanteRecebimento);
        $DevLogAcaoPasso->setNomeDepartamento($departamentoRecebimento);
        $DevLogAcaoPasso->setUsuario($ObjUsuario->getUsuarioCodigo());
        $DevLogAcaoPasso->setTipoUsuario('DEV');
        $DevLogAcaoPasso->setAcao('D');
        $DevLogAcaoPasso->setPasso('R');
        $DevLogAcaoPasso->setPassoPortal($Passo->getCodigo());
        $DevLogAcaoPasso->setAcaoDescricao($descRecebimento);
        $DevLogAcaoPasso->setCodReprova($motivoReprovaRecebimento);

        $LogAcaoPassoDao->inserir($DevLogAcaoPasso);


        /* Informar a tabela Ocorrencia que foi reprovada atualizando o campo doc_in_finalizado */
        $Ocorrencia->setFinalizadoDevolucao(0);

        /* informa neste ponto que o status da ocorrencia é coleta Reprovado(12) *** */
                /*         * ********************status*************** */
        $Ocorrencia->setStatusOcorrencia(12);
        /* Registrar o status na tabela de Ocorrencia Status */
        $OcorrenciaStatus = new OcorrenciaStatus();
        $OcorrenciaStatus->setCodigoOcorrencia($Ocorrencia->getCodigoDevolucao());
        $OcorrenciaStatus->setUsuario($ObjUsuario->getUsuarioCodigo());
        $OcorrenciaStatus->setCodStatus($Ocorrencia->getStatusOcorrencia());
        $OcorrenciaStatusDao->registrarSatus($OcorrenciaStatus);

        if (!$OcorrenciaDao->alterar($Ocorrencia)) {
            die("<center><strong class='msmerro'>Desculpe, erro na aprova&ccedil;&atilde;o do passo </strong></center>");
        }

        break;

    case 4:

        $OcorrenciaPasso->setCodigoOcorrencia($Ocorrencia->getCodigoDevolucao());
        $OcorrenciaPasso->setCodigoPasso($Passo->getCodigo());
        $OcorrenciaPasso->setUsuario($ObjUsuario->getUsuarioCodigo()); // 
        $OcorrenciaPasso->setIndicadorAprovado(0);
        $OcorrenciaPasso->setObservacao($descDevolucao);


        $OcorrenciaPassoDao->aprovarPasso($OcorrenciaPasso);

        //informar para o log

        $DevLogAcaoPasso->setCodOcorrencia($Ocorrencia->getCodigoDevolucao());
        $DevLogAcaoPasso->setNomeSolicitante($solicitanteDevolucao);
        $DevLogAcaoPasso->setNomeDepartamento($departamentoDevolucao);
        $DevLogAcaoPasso->setUsuario($ObjUsuario->getUsuarioCodigo());
        $DevLogAcaoPasso->setTipoUsuario('DEV');
        $DevLogAcaoPasso->setAcao('D');
        $DevLogAcaoPasso->setPasso('D');
        $DevLogAcaoPasso->setPassoPortal($Passo->getCodigo());
        $DevLogAcaoPasso->setAcaoDescricao($descDevolucao);
        $DevLogAcaoPasso->setCodReprova($motivoReprovaDevolucao);

        $LogAcaoPassoDao->inserir($DevLogAcaoPasso);


        /* Informar a tabela Ocorrencia que foi reprovada atualizando o campo doc_in_finalizado */
        $Ocorrencia->setFinalizadoDevolucao(0);

        /* informa neste ponto que o status da ocorrencia é coleta Reprovado(12) *** */
                /*         * ********************status*************** */
        $Ocorrencia->setStatusOcorrencia(12);
        /* Registrar o status na tabela de Ocorrencia Status */
        $OcorrenciaStatus = new OcorrenciaStatus();
        $OcorrenciaStatus->setCodigoOcorrencia($Ocorrencia->getCodigoDevolucao());
        $OcorrenciaStatus->setUsuario($ObjUsuario->getUsuarioCodigo());
        $OcorrenciaStatus->setCodStatus($Ocorrencia->getStatusOcorrencia());
        $OcorrenciaStatusDao->registrarSatus($OcorrenciaStatus);

        if (!$OcorrenciaDao->alterar($Ocorrencia)) {
            die("<center><strong class='msmerro'>Desculpe, erro na aprova&ccedil;&atilde;o do passo </strong></center>");
        }

        break;

    case 5:

        $OcorrenciaPasso->setCodigoOcorrencia($Ocorrencia->getCodigoDevolucao());
        $OcorrenciaPasso->setCodigoPasso($Passo->getCodigo()); // 1 - gerente

        $OcorrenciaPasso->setUsuario($ObjUsuario->getUsuarioCodigo()); // 
        $OcorrenciaPasso->setIndicadorAprovado(0);
        $OcorrenciaPasso->setObservacao($descFiscal);

        $OcorrenciaPassoDao->aprovarPasso($OcorrenciaPasso);

        
        //informar para o log

        $DevLogAcaoPasso->setCodOcorrencia($Ocorrencia->getCodigoDevolucao());
        $DevLogAcaoPasso->setNomeSolicitante($solicitanteFiscal);
        $DevLogAcaoPasso->setNomeDepartamento($departamentoFiscal);
        $DevLogAcaoPasso->setUsuario($ObjUsuario->getUsuarioCodigo());
        $DevLogAcaoPasso->setTipoUsuario('DEV');
        $DevLogAcaoPasso->setAcao('D');
        $DevLogAcaoPasso->setPasso('F');
        $DevLogAcaoPasso->setPassoPortal($Passo->getCodigo());
        $DevLogAcaoPasso->setAcaoDescricao($descFiscal);
        $DevLogAcaoPasso->setCodReprova($motivoReprovaFiscal);

        $LogAcaoPassoDao->inserir($DevLogAcaoPasso);

        /* Informar a tabela Ocorrencia que foi reprovada atualizando o campo doc_in_finalizado */
        $Ocorrencia->setFinalizadoDevolucao(0);

        /* informa neste ponto que o status da ocorrencia é coleta Reprovado(12) *** */
                /*         * ********************status*************** */
        $Ocorrencia->setStatusOcorrencia(12);
        /* Registrar o status na tabela de Ocorrencia Status */
        $OcorrenciaStatus = new OcorrenciaStatus();
        $OcorrenciaStatus->setCodigoOcorrencia($Ocorrencia->getCodigoDevolucao());
        $OcorrenciaStatus->setUsuario($ObjUsuario->getUsuarioCodigo());
        $OcorrenciaStatus->setCodStatus($Ocorrencia->getStatusOcorrencia());
        $OcorrenciaStatusDao->registrarSatus($OcorrenciaStatus);

        $Ocorrencia->setDocInTransmitido('N');

        if (!$OcorrenciaDao->alterar($Ocorrencia)) {
            die("<center><strong class='msmerro'>Desculpe, erro na aprova&ccedil;&atilde;o do passo </strong></center>");
        }

        break;
}
/*
  //quando for passo devolucao
  //if($tpu_co_numero==4 && $finalizacao!='yes')
  //{
  //?>
  <script>
  responsavelDevolucao('//<?php echo $oco_co_numero; ?>',<?php echo $cli_co_numero; ?>);
  </script>

  //<?php
  ////die();
  //} */
?>
<script>
    alert('Ocorrência: <?php echo $codOcorrencia; ?>, Reprovada com Sucesso!');
    $('#tab-fluxo').load('modules/ocorrencia/consulta/detalheFluxo.php?codOcorrencia='+ <?php echo $codOcorrencia ?>);
</script>
<!--<div class="msmsucesso" style="text-decoration: ">
    <center>
        A&ccedil;&atilde;o realizada com Sucesso
        <br />
        <br />
        <br />
        <br />
        <a  class="buttonFinish btn btn-success pull-right btn-xs" style="display: block;" onclick="javascript: aprovarOcorrenciaPasso(<?php echo $codOcorrencia; ?>,<?php echo $codPasso; ?>, 'Recebimento');">voltar</a>
    </center>
</div>-->


