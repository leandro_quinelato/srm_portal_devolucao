<?php

error_reporting(E_ERROR);

include_once( "../../../../includes/Dao/DaoSistema.class.php" );
include_once( "../../../../includes/DevNotaOrigemItem.class.php" );
include_once( "../../../../includes/Dao/DevNotaOrigemItemDao.class.php" );
include_once( "../../../../includes/Dao/OcorrenciaDao.class.php" );
include_once( "../../../../includes/Ocorrencia.class.php" );
include_once( "../../../../includes/CalTipoOcorrencia.class.php" );
include_once( "../../../../includes/Dao/TipoOcorrenciaDAO.class.php" );
include_once( "../../../../includes/TipoDevolucao.class.php" );
include_once( "../../../../includes/Dao/TipoDevolucaoDao.class.php" );
include_once( "../../../../includes/Rota.class.php" );
include_once( "../../../../includes/Dao/RotaDao.class.php" );

include_once( "../../../../includes/Status.class.php" );
include_once( "../../../../includes/Dao/StatusDao.class.php" );

include_once( "../../../../includes/Passo.class.php" );
include_once( "../../../../includes/Dao/PassoDao.class.php" );

include_once( "../../../../includes/OcorrenciaPasso.class.php" );
include_once( "../../../../includes/Dao/OcorrenciaPassoDao.class.php" );

include_once( "../../../../includes/Conferente.class.php" );
include_once( "../../../../includes/Dao/ConferenteDao.class.php" );

include_once( "../../../../includes/LogAcaoPasso.class.php" );
include_once( "../../../../includes/Dao/LogAcaoPassoDao.class.php" );

include_once( "../../../../includes/OcorrenciaStatus.class.php" );
include_once( "../../../../includes/Dao/OcorrenciaStatusDao.class.php" );

include_once( "../../../../includes/DevParametros.class.php" );
include_once( "../../../../includes/Dao/ParametroDao.class.php" );
include_once( "../../../utilitario/email/emailPortalDevolucao.class.php" );

include_once("DaoGlobal.class.php");
include_once("SistemaDao.class.php");
include_once("GlbSistema.class.php");
include_once("GlbSistemaPerfil.class.php");
include_once("GlbUsuario.class.php");
include_once("GlbColaborador.class.php");
include_once("GlbPessoa.class.php");
include_once("GlbEndereco.class.php");
include_once("GlbTelefone.class.php");
include_once("include_novo/GlbCentroDistribuicao.class.php");
include_once("include_novo/GlbTransportadora.class.php");
include_once("include_novo/GlbContatoTransportador.class.php");
include_once("include_novo/ContatoTransportadorDao.class.php");
include_once("GlbProgramaRotina.class.php");
include_once("GlbModuloPrograma.class.php");
include_once("GlbSistemaModulo.class.php");
include_once("PerfilUsuarioDao.class.php");
include_once("PerfilPermissaoDao.class.php");
include_once("integraSetSistemas.php");
$integra = new integraSetSistemas("conf.xml");
$ObjUsuario = unserialize($integra->getSistemaIntegra());
$ObjRotinas = $ObjUsuario->getObjSistemaPerfil()->getObjRotina();

extract($_REQUEST);

//echo "<pre>";
//print_r($_REQUEST);
//echo "</pre>";
/*
  [codOcorrenciaReentregar] => 16000823230001
  [codPassoReentregar] => 2
  [solicitanteReentregar] => tetsw
  [departamentoReentregar] => ti
  [descReentregar] => lsdksdvodsjvjsdopjvopsd
 */

$Passo = new Passo();
$Passo->setCodigo($codPassoReentregar);
$PassoDao = new PassoDao();
$Passo = $PassoDao->consultar($Passo);
if ($Passo[0]) {
    $Passo = $Passo[0];
} else {
    $Passo = new Passo();
}

$Ocorrencia = new Ocorrencia();
$OcorrenciaDao = new OcorrenciaDao();
$OcorrenciaPasso = new OcorrenciaPasso();
$OcorrenciaPassoDao = new OcorrenciaPassoDao();
$OcorrenciaStatus = new OcorrenciaStatus();
$OcorrenciaStatusDao = new OcorrenciaStatusDao();
$DevLogAcaoPasso = new LogAcaoPasso();
$LogAcaoPassoDao = new LogAcaoPassoDao();


$Ocorrencia->setCodigoDevolucao($codOcorrenciaReentregar);

//echo 'proxima sequencia:' . $proximoPasso = $Passo->getSeqFluxo() + 1;

/* Criando objetos para gravação dos logs de reprovação */
$OcorrenciaPasso->setCodigoOcorrencia($Ocorrencia->getCodigoDevolucao());
$OcorrenciaPasso->setCodigoPasso($Passo->getCodigo()); // 1 - gerente
$OcorrenciaPasso->setUsuario($ObjUsuario->getUsuarioCodigo()); // 
$OcorrenciaPasso->setIndicadorAprovado(0);
$OcorrenciaPasso->setObservacao($descReentregar);

$OcorrenciaPassoDao->aprovarPasso($OcorrenciaPasso);

//informar para o log

$DevLogAcaoPasso->setCodOcorrencia($Ocorrencia->getCodigoDevolucao());
$DevLogAcaoPasso->setNomeSolicitante($solicitanteReentregar);
$DevLogAcaoPasso->setNomeDepartamento($departamentoReentregar);
$DevLogAcaoPasso->setUsuario($ObjUsuario->getUsuarioCodigo());
$DevLogAcaoPasso->setTipoUsuario('DEV');
$DevLogAcaoPasso->setAcao('R');
$DevLogAcaoPasso->setPasso('A');
$DevLogAcaoPasso->setPassoPortal($Passo->getCodigo());
$DevLogAcaoPasso->setAcaoDescricao($descReentregar);

$LogAcaoPassoDao->inserir($DevLogAcaoPasso);


/* Informar a tabela Ocorrencia que foi reprovada atualizando o campo doc_in_finalizado */
$Ocorrencia->setFinalizadoDevolucao(0);

/* informa neste ponto que o status da ocorrencia é coleta Reentrega(17) *** */
		/*         * ********************status*************** */
$Ocorrencia->setStatusOcorrencia(17);
/* Registrar o status na tabela de Ocorrencia Status */
$OcorrenciaStatus->setCodigoOcorrencia($Ocorrencia->getCodigoDevolucao());
$OcorrenciaStatus->setUsuario($ObjUsuario->getUsuarioCodigo());
$OcorrenciaStatus->setCodStatus($Ocorrencia->getStatusOcorrencia());
$OcorrenciaStatusDao->registrarSatus($OcorrenciaStatus);

if (!$OcorrenciaDao->alterar($Ocorrencia)) {
	die("<center><strong class='msmerro'>Desculpe, erro na aprova&ccedil;&atilde;o do passo </strong></center>");
}else{
	$ObjParametro = new DevParametros();
	$ParametroDao = new ParametroDao();
	$ObjParametro = $ParametroDao->consultar();	
	
	$emailPortalDevolucao = new emailPortalDevolucao();
	$emailPortalDevolucao->enviaEmailReentrega($Ocorrencia, $OcorrenciaPasso, $ObjParametro->getEmailReentrega());
}

	
?>
<script>
    alert('Ocorrência: <?php echo $codOcorrencia; ?>, está para Reentrega!');
    $('#tab-fluxo').load('modules/ocorrencia/consulta/detalheFluxo.php?codOcorrencia='+ <?php echo $codOcorrencia ?>);
</script>