<?php
error_reporting(E_ERROR);
session_start();
include_once( "../../../../includes/Dao/DaoSistema.class.php" );
include_once( "../../../../includes/DevNotaOrigemItem.class.php" );
include_once( "../../../../includes/Dao/DevNotaOrigemItemDao.class.php" );
include_once( "../../../../includes/Ocorrencia.class.php" );
include_once( "../../../../includes/Dao/OcorrenciaDao.class.php" );
include_once( "../../../../includes/CalTipoOcorrencia.class.php" );
include_once( "../../../../includes/Dao/TipoOcorrenciaDAO.class.php" );
include_once( "../../../../includes/TipoDevolucao.class.php" );
include_once( "../../../../includes/Dao/TipoDevolucaoDao.class.php" );
include_once( "../../../../includes/Rota.class.php" );
include_once( "../../../../includes/Dao/RotaDao.class.php" );


include_once( "../../../../includes/Status.class.php" );
include_once( "../../../../includes/Dao/StatusDao.class.php" );

include_once( "../../../../includes/Passo.class.php" );
include_once( "../../../../includes/Dao/PassoDao.class.php" );

include_once( "../../../../includes/OcorrenciaPasso.class.php" );
include_once( "../../../../includes/Dao/OcorrenciaPassoDao.class.php" );

include_once( "../../../../includes/LogAcaoPasso.class.php" );
include_once( "../../../../includes/Dao/LogAcaoPassoDao.class.php" );

include_once( "../../../../includes/MotivoReprova.class.php" );
include_once( "../../../../includes/Dao/MotivoReprovaDao.class.php" );

include_once("DaoGlobal.class.php");
include_once("SistemaDao.class.php");
include_once("GlbSistema.class.php");
include_once("GlbSistemaPerfil.class.php");
include_once("GlbUsuario.class.php");
include_once("GlbColaborador.class.php");
include_once("GlbPessoa.class.php");
include_once("GlbEndereco.class.php");
include_once("GlbTelefone.class.php");
include_once("include_novo/GlbTransportadora.class.php");
include_once("include_novo/GlbContatoTransportador.class.php");
include_once("include_novo/ContatoTransportadorDao.class.php");
include_once("GlbProgramaRotina.class.php");
include_once("GlbModuloPrograma.class.php");
include_once("GlbSistemaModulo.class.php");
include_once("PerfilUsuarioDao.class.php");
include_once("PerfilPermissaoDao.class.php");

$ObjUsuario = unserialize($_SESSION['ObjLogin']);

extract($_REQUEST);

$status[0] = "Reprovado";
$status[1] = "Aprovado";
$status[2] = "Pausado";

/* Criando objetos para gravação dos logs de reprovação */
$LogAcaoPasso = new LogAcaoPasso();
$LogAcaoPassoDao = new LogAcaoPassoDao();

$LogAcaoPasso->setPassoPortal($codPasso);


$LogAcaoPasso->setCodOcorrencia($codOcorrencia);

$LogAcaoPasso->setPasso('R');
$qtdLog = $LogAcaoPassoDao->qtdAcaoPasso($LogAcaoPasso);
$resultLog = $LogAcaoPassoDao->consultaAcaoPasso($LogAcaoPasso);


$OcorrenciaPasso = new OcorrenciaPasso();
$OcorrenciaPassoDao = new OcorrenciaPassoDao();

$OcorrenciaPasso->setCodigoOcorrencia($codOcorrencia);
$OcorrenciaPasso->setCodigoPasso($codPasso);
$retornoOcorrenciaPasso = $OcorrenciaPassoDao->consultarOcorrenciaPasso($OcorrenciaPasso);


$OcorrenciaPassoGerado = new OcorrenciaPasso();
$OcorrenciaPassoGeradoDao = new OcorrenciaPassoDao();
$OcorrenciaPassoGerado->setCodigoOcorrencia($codOcorrencia);
$OcorrenciaPassoGerado->setCodigoPasso(1);
$OcorrenciaPassoGerado = $OcorrenciaPassoDao->consultarOcorrenciaPasso($OcorrenciaPassoGerado);


if ($OcorrenciaPassoGerado[0]) {
    $OcorrenciaPassoGerado = $OcorrenciaPassoGerado[0];


} else {
    $OcorrenciaPassoGerado = new OcorrenciaPasso();

}

$OcorrenciaPasso = new OcorrenciaPasso();


$OcorrenciaPasso->setCodigoOcorrencia($codOcorrencia);
$OcorrenciaPasso->setCodigoPasso($codPasso);

$OcorrenciaPasso = $OcorrenciaPassoDao->consultarOcorrenciaPasso($OcorrenciaPasso);


if ($OcorrenciaPasso[0]){
    $OcorrenciaPasso = $OcorrenciaPasso[0];
}else{
    $OcorrenciaPasso = new OcorrenciaPasso();
}

?>
<form id="FormPassoRecebimento">
    <input type="hidden" id="tipoDevol" name="tipoDevol" value="<?php echo $dadoOcorrencia->DOC_IN_TIPO; ?>">

    <div class="form-group">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <span>Entrada:</span>
                <strong><?php echo $OcorrenciaPasso->getDataEntrada(); ?></strong>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <span>Saída:</span>
                <strong><?php echo $OcorrenciaPasso->getDataSaida(); ?></strong>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <span>Status:</span>
                <strong>
                    <?php
                    if (isset($status[$OcorrenciaPasso->getIndicadorAprovado()])) {//retornando false
                        echo $status[$OcorrenciaPasso->getIndicadorAprovado()];
                    } else {
                        echo "";
                    }
                    ?>
                </strong>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <span>Usuário:</span>
                <strong><?php echo $OcorrenciaPasso->getUsuarioNome(); ?></strong>
            </div>
            <div class="col-md-3 col-sm-5 col-xs-12">
                <label>Volume Recebido</label>
                <input type="text" placeholder="Quant" class="form-control" onKeyPress="fMascara('numero', event, this)" id="qtdVolumeRecebe" name="qtdVolumeRecebe" value="<?php echo $OcorrenciaPasso->getQtdVolume(); ?>" <?php echo $OcorrenciaPasso->getIndicadorAprovado() == 1 ? "readonly" : ""; ?>>
            </div>

        </div>
    </div>
    <div class="form-group">
        <?php
        if ($OcorrenciaPasso->getObservacao()) {
            $txdescricao = $OcorrenciaPasso->getObservacao()->load();
        }
        ?>

        <div class="col-md-3 col-sm-12 col-xs-12">
            <label>Descrição</label>
            <textarea class="form-control" rows="3" placeholder="Descrição" id="descRecebimento" name="descRecebimento" <?php echo $OcorrenciaPasso->getIndicadorAprovado() == 1 ? "readonly" : ""; ?> ><?php echo $txdescricao; ?></textarea>
        </div>
    </div>
</form>

<?php
if (($OcorrenciaPasso->getIndicadorAprovado() != 1) && ($OcorrenciaPasso->getIndicadorAprovado() != 0)) {
    if($ObjUsuario->getObjSistemaPerfil() != null){
        if( ($ObjUsuario->getObjSistemaPerfil()->getNomeSistemaPerfil() == 'Administrador') || ($ObjUsuario->getObjSistemaPerfil()->getNomeSistemaPerfil() == 'Devolucao') || ($ObjUsuario->getObjSistemaPerfil()->getNomeSistemaPerfil() == 'Recebimento') ){
            ?>
            <div class="BotAprovacao">
                <a  class="buttonFinish btn btn-success pull-left btn-xs" style="display: block;" onclick="javascript: aprovarOcorrenciaPasso(<?php echo $codOcorrencia; ?>,<?php echo $codPasso; ?>, 'Recebimento');">Aprovar</a>
                <!--<a  class="buttonNext btn btn-info pull-right btn-xs">Pausar</a>-->
                <a  class="buttonPrevious btn btn-danger pull-right btn-xs" onclick="javascript: abrirReprova(<?php echo $codOcorrencia; ?>,<?php echo $codPasso; ?>, 'Recebimento');">Reprovar</a>
            </div>
            <?php
        }
    }
}
?>
<div class="modal fade" id="modalReprovaRecebimento" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

</div>

<?php
if ($qtdLog > 0) {
    ?>

    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover TableNF">
                    <thead>
                    <tr>
                        <th>Solicitante</th>
                        <th>Departamento</th>
                        <th>Usuário</th>
                        <th>Data</th>
                        <th>Ação</th>
                        <th>Descrição</th>
                        <th>Motivo Reprova</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    while (OCIFetchInto($resultLog, $log, OCI_ASSOC)) {
                        ?>

                        <tr>
                            <td><?php echo $log['SOLICITANTE']; ?></td>
                            <td><?php echo $log['DEPARTAMENTO']; ?></td>
                            <td><?php echo $log['USUARIO']; ?></td>
                            <td><?php echo $log['DATA_ACAO']; ?></td>
                            <td><?php echo $log['ACAO']; ?></td>
                            <td><?php
                                if (!is_null($log['DESCRICAO'])) {
                                    echo $log['DESCRICAO']->load();
                                } else {
                                    echo "";
                                }
                                ?></td>
                            <td><?php echo $log['MRE_NO_DESCRICAO']; ?></td>
                        </tr>
                        <?php
                    }
                    ?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <?php
}
?>

