<?php
error_reporting(E_ERROR);
include_once( "../../../../includes/Dao/DaoSistema.class.php" );
include_once( "../../../../includes/DevNotaOrigemItem.class.php" );
include_once( "../../../../includes/Dao/DevNotaOrigemItemDao.class.php" );
include_once( "../../../../includes/Dao/OcorrenciaDao.class.php" );
include_once( "../../../../includes/Ocorrencia.class.php" );
include_once( "../../../../includes/CalTipoOcorrencia.class.php" );
include_once( "../../../../includes/Dao/TipoOcorrenciaDAO.class.php" );
include_once( "../../../../includes/TipoDevolucao.class.php" );
include_once( "../../../../includes/Dao/TipoDevolucaoDao.class.php" );
include_once( "../../../../includes/Rota.class.php" );
include_once( "../../../../includes/Dao/RotaDao.class.php" );


include_once( "../../../../includes/Status.class.php" );
include_once( "../../../../includes/Dao/StatusDao.class.php" );

include_once( "../../../../includes/Passo.class.php" );
include_once( "../../../../includes/Dao/PassoDao.class.php" );

include_once( "../../../../includes/OcorrenciaPasso.class.php" );
include_once( "../../../../includes/Dao/OcorrenciaPassoDao.class.php" );

include_once( "../../../../includes/Conferente.class.php" );
include_once( "../../../../includes/Dao/ConferenteDao.class.php" );

extract($_REQUEST);

$Ocorrencia     = new Ocorrencia();
$OcorrenciaDao  = new OcorrenciaDao();
$Ocorrencia->setCodigoDevolucao($codOcorrencia);    
$Ocorrencia->setTipoOcorrencia($tipoDevolucao);

$retornoDados    = $OcorrenciaDao->consultaOcorrencia($Ocorrencia);
$ocorrenciaDados = oci_fetch_object($retornoDados);
//echo $ocorrenciaDados->CED_CO_NUMERO;
//relaciona centro_distribicao x estabelecimento
$estabelecimento[1] = 150;
$estabelecimento[46] = 304;
$estabelecimento[48] = 160;
$estabelecimento[49] = 354;
$codEstabelecimento = $estabelecimento[$ocorrenciaDados->CED_CO_NUMERO];

$OcorrenciaPasso = new OcorrenciaPasso();
$OcorrenciaPassoDao = new OcorrenciaPassoDao();
$OcorrenciaPasso->setCodigoOcorrencia($codOcorrencia);
$OcorrenciaPasso->setCodigoPasso($codPasso);
$OcorrenciaPasso = $OcorrenciaPassoDao->consultarOcorrenciaPasso($OcorrenciaPasso);
if ($OcorrenciaPasso[0]) {
    $OcorrenciaPasso = $OcorrenciaPasso[0];
} else {
    $OcorrenciaPasso = new OcorrenciaPasso();
}

$conferenteDao = new ConferenteDao();
//$conferentes = $conferenteDao->consultarConferentes(null, null, null, 'false');


if ($acao == "LISTAR") {

    $Conferente = new Conferente();
    $Conferente->setCodOcorrencia($codOcorrencia);
    $Conferente = $conferenteDao->consultarConferenteOcorrencia($Conferente);
    
    if ($Conferente[0]) {
        $Conferente = $Conferente;
        $existeConferente = $Conferente[0]->getCodOcorrencia();
    } else {
        $Conferente = new Conferente();
        $existeConferente = $Conferente->getCodOcorrencia();
    }

    if ($existeConferente) {
        foreach ($Conferente as $ObjConferente) {
            $conferentes = $conferenteDao->consultarConferentes(null, $codEstabelecimento, null, 'false');
            $colaborador = $ObjConferente->getEmpresa() . '_' . $ObjConferente->getEstabelecimento() . '_' . $ObjConferente->getCracha();

            ?>
            <div class="form-group">
                <div class="col-md-3 col-sm-12 col-xs-12">
                    <label>Conferente</label>
                    <select class="form-control selectpicker" name="filtro_conferente[]" >
                        <option value="TOD">Selecionar...</option>
                        <?php
                        while (OCIFetchInto($conferentes, $conf, OCI_ASSOC)) {
                            $conferente = $conf['EMP_CO_NUMERO'] . '_' . $conf['ETB_CO_NUMERO'] . '_' . $conf['COL_CO_CRACHA'];
                            ?>             
                            <option value="<?php echo $conferente; ?>"  <?php echo $conferente == $colaborador ? 'selected' : ''; ?>> <?php echo $conf['COL_NO_COLABORADOR'] .'  (' .$conf['ETB_NO_APELIDO']. ')'  ?> </option>
                            <?php
                        }
                        ?>              					

                    </select>
                </div> 
                <div class="col-md-3 col-sm-12 col-xs-12">
                    <label>Volume Recebido</label>
                    <input type="text" placeholder="Quant" class="form-control" name="volumeDevolucao[]" value="<?php echo $ObjConferente->getVolume();?>">
                </div>
                <div class="col-md-3 col-sm-12 col-xs-12">
					</br></br>
					<a  class="buttonNext btn btn-danger pull-left btn-xs" onclick="javascript: excluirConferenteDevolucao('<?=$codOcorrencia;?>', '<?=$ObjConferente->getCracha();?>', '<?=$ObjConferente->getVolume();?>');">Excluir</a>
                </div>				
            </div>
            <?php
        }
    } else {
        
        $conferentes = $conferenteDao->consultarConferentes(null, $codEstabelecimento, null, 'false');
        ?>    

        <div class = "form-group">
            <div class = "col-md-3 col-sm-12 col-xs-12">
                <label>Conferente</label>
                <select class = "form-control selectpicker" name = "filtro_conferente[]" >
                    <option value = "TOD">Selecionar...</option>
                    <?php
                    while (OCIFetchInto($conferentes, $conf, OCI_ASSOC)) {
                        $conferente = $conf['EMP_CO_NUMERO'] . '_' . $conf['ETB_CO_NUMERO'] . '_' . $conf['COL_CO_CRACHA'];
                        ?>             
                        <option value="<?php echo $conferente; ?>"  <?php echo $conferente == $colaborador ? 'selected' : ''; ?>> <?php echo $conf['COL_NO_COLABORADOR'] .'  (' .$conf['ETB_NO_APELIDO']. ')'  ?> </option>
                        <?php
                    }
                    ?>              					

                </select>
            </div> 
            <div class="col-md-3 col-sm-12 col-xs-12">
                <label>Volume Recebido</label>
                <input type="text" placeholder="Quant" class="form-control" name="volumeDevolucao[]" onKeyPress="fMascara('numero', event, this)">
            </div>
        </div>  
        <?php
    }
} else {
    $conferentes = $conferenteDao->consultarConferentes(null, $codEstabelecimento, null, 'false');
    ?>    

    <div class = "form-group">
        <div class = "col-md-3 col-sm-12 col-xs-12">
            <label>Conferente</label>
            <select class = "form-control selectpicker" name = "filtro_conferente[]" >
                <option value = "TOD">Selecionar...</option>
                <?php
                while (OCIFetchInto($conferentes, $conf, OCI_ASSOC)) {
                    $conferente = $conf['EMP_CO_NUMERO'] . '_' . $conf['ETB_CO_NUMERO'] . '_' . $conf['COL_CO_CRACHA'];
                    ?>             
                    <option value="<?php echo $conferente; ?>"  <?php echo $conferente == $colaborador ? 'selected' : ''; ?>> <?php echo $conf['COL_NO_COLABORADOR'] .'  (' .$conf['ETB_NO_APELIDO']. ')'  ?> </option>
                    <?php
                }
                ?>              					

            </select>
        </div> 
        <div class="col-md-3 col-sm-12 col-xs-12">
            <label>Volume Recebido</label>
            <input type="text" placeholder="Quant" class="form-control" name="volumeDevolucao[]">
        </div>
    </div>  
    <?php
}    