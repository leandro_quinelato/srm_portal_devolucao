<?php
session_start();
error_reporting(E_ERROR);
include_once( "../../../../includes/Dao/DaoSistema.class.php" );
include_once( "../../../../includes/DevNotaOrigemItem.class.php" );
include_once( "../../../../includes/Dao/DevNotaOrigemItemDao.class.php" );
include_once( "../../../../includes/Dao/OcorrenciaDao.class.php" );
include_once( "../../../../includes/Ocorrencia.class.php" );
include_once( "../../../../includes/CalTipoOcorrencia.class.php" );
include_once( "../../../../includes/Dao/TipoOcorrenciaDAO.class.php" );
include_once( "../../../../includes/TipoDevolucao.class.php" );
include_once( "../../../../includes/Dao/TipoDevolucaoDao.class.php" );
include_once( "../../../../includes/Rota.class.php" );
include_once( "../../../../includes/Dao/RotaDao.class.php" );


include_once( "../../../../includes/Status.class.php" );
include_once( "../../../../includes/Dao/StatusDao.class.php" );

include_once( "../../../../includes/Passo.class.php" );
include_once( "../../../../includes/Dao/PassoDao.class.php" );

include_once( "../../../../includes/OcorrenciaPasso.class.php" );
include_once( "../../../../includes/Dao/OcorrenciaPassoDao.class.php" );

include_once( "../../../../includes/LogAcaoPasso.class.php" );
include_once( "../../../../includes/Dao/LogAcaoPassoDao.class.php" );

include_once( "../../../../includes/MotivoReprova.class.php" );
include_once( "../../../../includes/Dao/MotivoReprovaDao.class.php" );


$MotivoReprovaDao = new MotivoReprovaDao();
$MotivosReprova = $MotivoReprovaDao->consultar();

//echo "<pre>";
//print_r($_REQUEST);
//echo "</pre>";

extract($_REQUEST);
?>
<script>
//    $(function () {
//        $("#emailColeta").focus();
//    })
</script>
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" aria-label="Close" onclick="javascript: $('#modalReprova<?php echo $nomePasso; ?>').modal('hide');" ><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Protocolo: <?php echo $codOcorrencia ?></h4>
        </div>
        <div class="modal-body">
            <label>Para reprovação, informe os dados abaixo:</label>
            <form id="formReprova<?php echo $nomePasso; ?>">
                <input type="hidden" name="codOcorrencia<?php echo $nomePasso; ?>" id="codOcorrencia<?php echo $nomePasso; ?>" value="<?php echo $codOcorrencia ?>">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Solicitante:</label>                                        
                            <input type="text" placeholder="Solicitante" class="form-control" maxlength="60" id="solicitante<?php echo $nomePasso; ?>" name="solicitante<?php echo $nomePasso; ?>">                                        
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Departamento:</label>                                        
                            <input type="text" placeholder="Departamento" class="form-control" maxlength="60" id="departamento<?php echo $nomePasso; ?>" name="departamento<?php echo $nomePasso; ?>">                                        
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Motivo Reprova:</label>                                        
                            <select class="form-control selectpicker" id="motivoReprova<?php echo $nomePasso; ?>" name="motivoReprova<?php echo $nomePasso; ?>">
                                <option value="TOD">Selecionar...</option>
                                <?php
                                foreach ($MotivosReprova as $motivoReprova) {
                                    ?>             
                                    <option value="<?php echo $motivoReprova->getCodReprova(); ?>"><?php echo $motivoReprova->getDescricao(); ?></option>
                                    <?php
                                }
                                ?>              					

                            </select>                                      
                        </div>
                    </div>
                </div>
            </form>
            <div id="retornoReprova<?php echo $nomePasso; ?>">
                
            </div>
               
        </div>
        <div class="modal-footer">
            <button class="btn btn-default" type="button" onclick="javascript: $('#modalReprova<?php echo $nomePasso; ?>').modal('hide');">Cancelar</button>
            <button class="btn btn-primary" type="button" onclick="javascript: reprovarOcorrenciaPasso(<?php echo $codOcorrencia; ?>,<?php echo $codPasso; ?>,'<?php echo $nomePasso; ?>');">Enviar</button>
        </div>                
    </div>
</div>