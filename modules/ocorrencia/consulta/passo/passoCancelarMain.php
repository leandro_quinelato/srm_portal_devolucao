<?php

error_reporting(E_ERROR);

include_once( "../../../../includes/Dao/DaoSistema.class.php" );
include_once( "../../../../includes/DevNotaOrigemItem.class.php" );
include_once( "../../../../includes/Dao/DevNotaOrigemItemDao.class.php" );
include_once( "../../../../includes/Dao/OcorrenciaDao.class.php" );
include_once( "../../../../includes/Ocorrencia.class.php" );
include_once( "../../../../includes/CalTipoOcorrencia.class.php" );
include_once( "../../../../includes/Dao/TipoOcorrenciaDAO.class.php" );
include_once( "../../../../includes/TipoDevolucao.class.php" );
include_once( "../../../../includes/Dao/TipoDevolucaoDao.class.php" );
include_once( "../../../../includes/Rota.class.php" );
include_once( "../../../../includes/Dao/RotaDao.class.php" );

include_once( "../../../../includes/Status.class.php" );
include_once( "../../../../includes/Dao/StatusDao.class.php" );

include_once( "../../../../includes/Passo.class.php" );
include_once( "../../../../includes/Dao/PassoDao.class.php" );

include_once( "../../../../includes/OcorrenciaPasso.class.php" );
include_once( "../../../../includes/Dao/OcorrenciaPassoDao.class.php" );

include_once( "../../../../includes/Conferente.class.php" );
include_once( "../../../../includes/Dao/ConferenteDao.class.php" );

include_once( "../../../../includes/LogAcaoPasso.class.php" );
include_once( "../../../../includes/Dao/LogAcaoPassoDao.class.php" );

include_once( "../../../../includes/OcorrenciaStatus.class.php" );
include_once( "../../../../includes/Dao/OcorrenciaStatusDao.class.php" );

include_once("DaoGlobal.class.php");
include_once("SistemaDao.class.php");
include_once("GlbSistema.class.php");
include_once("GlbSistemaPerfil.class.php");
include_once("GlbUsuario.class.php");
include_once("GlbColaborador.class.php");
include_once("GlbPessoa.class.php");
include_once("GlbEndereco.class.php");
include_once("GlbTelefone.class.php");
include_once("include_novo/GlbCentroDistribuicao.class.php");
include_once("include_novo/GlbTransportadora.class.php");
include_once("include_novo/GlbContatoTransportador.class.php");
include_once("include_novo/ContatoTransportadorDao.class.php");
include_once("GlbProgramaRotina.class.php");
include_once("GlbModuloPrograma.class.php");
include_once("GlbSistemaModulo.class.php");
include_once("PerfilUsuarioDao.class.php");
include_once("PerfilPermissaoDao.class.php");
include_once("integraSetSistemas.php");
$integra = new integraSetSistemas("conf.xml");
$ObjUsuario = unserialize($integra->getSistemaIntegra());
$ObjRotinas = $ObjUsuario->getObjSistemaPerfil()->getObjRotina();

extract($_REQUEST);
/*
echo "<pre>";
print_r($_REQUEST);
echo "</pre>";

  [codOcorrenciaCancelar] => 16000823230001
  [codPassoCancelar] => 2
  [solicitanteCancelar] => tetsw
  [departamentoCancelar] => ti
  [descCancelar] => lsdksdvodsjvjsdopjvopsd
 */
$Ocorrencia = new Ocorrencia();
$OcorrenciaDao = new OcorrenciaDao();

$Ocorrencia->setCodigoDevolucao($codOcorrenciaCancelar);
//$Ocorrencia->setDocInTransmitido('N');
$Ocorrencia->setStatusOcorrencia(12);
$Ocorrencia->setFinalizadoDevolucao(0); /* Informar a tabela Ocorrencia que foi reprovada atualizando o campo doc_in_finalizado */

/* Registrar a data de cancelamento na tabela junto com o numero do protocolo */

$Ocorrencia->setDataCancelamento($currentDateTime);
$Ocorrencia->setCodigoOcorrencia($codOcorrencia);


echo $currentDateTime . 'ahuseashuash';

$OcorrenciaDao->inserirDataCancelamento($codOcorrenciaCancelar);


if ($OcorrenciaDao->alterar($Ocorrencia)) {
	//*
	$OcorrenciaPassoDao = new OcorrenciaPassoDao();
	$OcorrenciaPasso = new OcorrenciaPasso();
	$OcorrenciaPasso->setCodigoOcorrencia($Ocorrencia->getCodigoDevolucao());
	$OcorrenciaPasso->setCodigoPasso($codPassoCancelar);
	$OcorrenciaPasso->setUsuario($ObjUsuario->getUsuarioCodigo()); // 
	$OcorrenciaPasso->setIndicadorAprovado(0);
	$OcorrenciaPasso->setObservacao($descCancelar);
	$OcorrenciaPassoDao->aprovarPasso($OcorrenciaPasso);
	//*/
	
	$LogAcaoPassoDao = new LogAcaoPassoDao();	
	$DevLogAcaoPasso = new LogAcaoPasso();
	$DevLogAcaoPasso->setCodOcorrencia($Ocorrencia->getCodigoDevolucao());
	$DevLogAcaoPasso->setNomeSolicitante($solicitanteCancelar);
	$DevLogAcaoPasso->setNomeDepartamento($departamentoCancelar);
	$DevLogAcaoPasso->setUsuario($ObjUsuario->getUsuarioCodigo());
	$DevLogAcaoPasso->setTipoUsuario('DEV');
	$DevLogAcaoPasso->setAcao('D');
	$DevLogAcaoPasso->setPasso('A');
	$DevLogAcaoPasso->setAcaoDescricao($descCancelar);
	$DevLogAcaoPasso->setPassoPortal($codPassoCancelar);
	

	$LogAcaoPassoDao->inserir($DevLogAcaoPasso);
	
	/* Registrar o status na tabela de Ocorrencia Status reprovado 12*/
	$OcorrenciaStatusDao = new OcorrenciaStatusDao();
	$OcorrenciaStatus = new OcorrenciaStatus();
	$OcorrenciaStatus->setCodigoOcorrencia($Ocorrencia->getCodigoDevolucao());
	$OcorrenciaStatus->setUsuario($ObjUsuario->getUsuarioCodigo());
	$OcorrenciaStatus->setCodStatus($Ocorrencia->getStatusOcorrencia());
	$OcorrenciaStatusDao->registrarSatus($OcorrenciaStatus);




?>
<script>
	detalheOcorrencia('<?php echo $Ocorrencia->getCodigoDevolucao(); ?>');
</script>
<?php
}
?>