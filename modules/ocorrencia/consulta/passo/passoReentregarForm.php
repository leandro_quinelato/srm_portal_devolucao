<?php
session_start();
error_reporting(E_ERROR);
include_once( "../../../../includes/Dao/DaoSistema.class.php" );
include_once( "../../../../includes/DevNotaOrigemItem.class.php" );
include_once( "../../../../includes/Dao/DevNotaOrigemItemDao.class.php" );
include_once( "../../../../includes/Dao/OcorrenciaDao.class.php" );
include_once( "../../../../includes/Ocorrencia.class.php" );
include_once( "../../../../includes/CalTipoOcorrencia.class.php" );
include_once( "../../../../includes/Dao/TipoOcorrenciaDAO.class.php" );
include_once( "../../../../includes/TipoDevolucao.class.php" );
include_once( "../../../../includes/Dao/TipoDevolucaoDao.class.php" );
include_once( "../../../../includes/Rota.class.php" );
include_once( "../../../../includes/Dao/RotaDao.class.php" );


include_once( "../../../../includes/Status.class.php" );
include_once( "../../../../includes/Dao/StatusDao.class.php" );

include_once( "../../../../includes/Passo.class.php" );
include_once( "../../../../includes/Dao/PassoDao.class.php" );

include_once( "../../../../includes/OcorrenciaPasso.class.php" );
include_once( "../../../../includes/Dao/OcorrenciaPassoDao.class.php" );

include_once( "../../../../includes/LogAcaoPasso.class.php" );
include_once( "../../../../includes/Dao/LogAcaoPassoDao.class.php" );

include_once( "../../../../includes/MotivoReprova.class.php" );
include_once( "../../../../includes/Dao/MotivoReprovaDao.class.php" );


$MotivoReprovaDao = new MotivoReprovaDao();
$MotivosReprova = $MotivoReprovaDao->consultar();

//echo "<pre>";
//print_r($_REQUEST);
//echo "</pre>";

extract($_REQUEST);
?>
<script>
//    $(function () {
//        $("#emailColeta").focus();
//    })
</script>
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" aria-label="Close" onclick="javascript: $('#modalReentregar').modal('hide');" ><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Protocolo: <?php echo $codOcorrencia ?></h4>
        </div>
        <div class="modal-body">
            <label>Para Reentregar a ocorrência, informe os dados abaixo:</label>
            <form id="formReentregar">
                <input type="hidden" name="codOcorrenciaReentregar" id="codOcorrenciaReentregar" value="<?php echo $codOcorrencia ?>">
                <input type="hidden" name="codPassoReentregar" id="codPassoReentregar" value="<?php echo $codPasso ?>">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Solicitante:</label>                                        
                            <input type="text" placeholder="Solicitante" class="form-control" maxlength="60" id="solicitanteReentregar" name="solicitanteReentregar">                                        
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Departamento:</label>                                        
                            <input type="text" placeholder="Departamento" class="form-control" maxlength="60" id="departamentoReentregar" name="departamentoReentregar">                                        
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Descrição</label>
                            <textarea class="form-control" rows="3" placeholder="Descrição" id="descReentregar" name="descReentregar"></textarea>                                   
                        </div>
                    </div>
                </div>
            </form>
            <div id="retornoReentregar">

            </div>

        </div>
        <div class="modal-footer">
            <button class="btn btn-default" type="button" onclick="javascript: $('#modalReentregar').modal('hide');">Cancelar</button>
            <button class="btn btn-primary" type="button" onclick="javascript: reentregarOcorrencia();">Confirmar</button>
        </div>                
    </div>
</div>