<?php

include_once( "../../../../includes/Dao/DaoSistema.class.php" );
include_once( "../../../../includes/Dao/OcorrenciaDao.class.php" );
include_once( "../../../../includes/Ocorrencia.class.php" );
include_once( "../../../../includes/CalTipoOcorrencia.class.php" );
include_once( "../../../../includes/Dao/TipoOcorrenciaDAO.class.php" );
include_once( "../../../../includes/TipoDevolucao.class.php" );
include_once( "../../../../includes/Dao/TipoDevolucaoDao.class.php" );
include_once( "../../../../includes/Rota.class.php" );
include_once( "../../../../includes/Dao/RotaDao.class.php" );


include_once( "../../../../includes/Status.class.php" );
include_once( "../../../../includes/Dao/StatusDao.class.php" );

include_once( "../../../../includes/Passo.class.php" );
include_once( "../../../../includes/Dao/PassoDao.class.php" );

include_once( "../../../../includes/DevMotivoResponsabilidade.class.php" );
include_once( "../../../../includes/Dao/MotivoResponsabilidadeDao.class.php" );

include_once("DaoGlobal.class.php");
include_once("SistemaDao.class.php");
include_once("GlbSistema.class.php");
include_once("GlbSistemaPerfil.class.php");
include_once("GlbUsuario.class.php");
include_once("GlbColaborador.class.php");
include_once("GlbPessoa.class.php");
include_once("GlbEndereco.class.php");
include_once("GlbTelefone.class.php");
include_once("include_novo/GlbCentroDistribuicao.class.php");
include_once("include_novo/GlbTransportadora.class.php");
include_once("include_novo/GlbContatoTransportador.class.php");
include_once("include_novo/ContatoTransportadorDao.class.php");
include_once("GlbProgramaRotina.class.php");
include_once("GlbModuloPrograma.class.php");
include_once("GlbSistemaModulo.class.php");
include_once("PerfilUsuarioDao.class.php");
include_once("PerfilPermissaoDao.class.php");
include_once("integraSetSistemas.php");
$integra = new integraSetSistemas("conf.xml");
$ObjUsuario = unserialize($integra->getSistemaIntegra());
$ObjRotinas = $ObjUsuario->getObjSistemaPerfil()->getObjRotina();
//
//echo "<pre>";
//print_r($_REQUEST);
//echo "</pre>";

$OcorrenciaDao = new OcorrenciaDao();


for($cot=0;$cot<$_REQUEST['cont'];$cot++){
	$ObjDevolucao = new Ocorrencia(); 
	$ObjDevolucao->setCodigoDevolucao($_REQUEST['doc_co_numero']);
        
        //variavel que servirá para apresentar o responsavel de cada nota
        $apresentaResponsavel = ""; 
	
	$DevMotivoResponsabilidade = new DevMotivoResponsabilidade();
	if($_REQUEST['trp_co_numero_'.$cot]){
		$DevMotivoResponsabilidade->setCodigo($_REQUEST['trp_co_numero_'.$cot]);	
	}else{
		//$DevMotivoResponsabilidade->setCodigo(1);
		die("Escolha o motivo de responsabilidade"); 
	}		 
	
	$DevNotaOrigem = new DevNotaOrigem(); 
	$DevNotaOrigem->setSerie($_REQUEST['nto_nu_serie_'.$cot]); 
	$DevNotaOrigem->setNota($_REQUEST['nto_nu_nota_'.$cot]); 
//	$DevNotaOrigem->setOrigem($_REQUEST['responsavel_'.$cot]);
	$DevNotaOrigem->setMotivoResponsabilidade($DevMotivoResponsabilidade);

        $nomeResponsavel = $_REQUEST['trp_no_responsavel_'.$DevMotivoResponsabilidade->getCodigo(). '_'.$cot];
        $origemVenda     = $_REQUEST['tp_origem_venda_'. $cot];
        
        if ( $nomeResponsavel == 'INDU' ){
	     $DevNotaOrigem->setOrigem($nomeResponsavel);
             $DevNotaOrigem->setResponsavel($_REQUEST['fab_co_numero_'.$cot]);
             $apresentaResponsavel = 'Industria';
        }else if ( $nomeResponsavel == 'ESTQ' ){
	     $DevNotaOrigem->setOrigem($nomeResponsavel);
             $apresentaResponsavel = 'Estoque';
        }else if ( $nomeResponsavel == 'ADM')  {
	     $DevNotaOrigem->setOrigem($nomeResponsavel);
             $apresentaResponsavel = 'Administrativo';
        }else if ( $nomeResponsavel == 'DIRT') {     
                    $DevNotaOrigem->setOrigem('DIRT');
                    $apresentaResponsavel = 'Diretoria';	   
        }else if ( $nomeResponsavel == 'COM' ){
	     //$DevNotaOrigem->setOrigem($nomeResponsavel);
            if ($origemVenda =='EP' || $origemVenda == 'GI' || $origemVenda =='AZ'  || $origemVenda == 'OR' || 
                $origemVenda =='GL' || $origemVenda =='N'   || $origemVenda == 'GD' || $origemVenda == 'GH' || $origemVenda == 'GM'){  //INDUSTRIA
	        $DevNotaOrigem->setOrigem('INDU');
                $DevNotaOrigem->setResponsavel($_REQUEST['fab_co_numero_'.$cot]);
                $apresentaResponsavel = 'Industria';
            }else if ($origemVenda =='B2'  || $origemVenda =='B4' || $origemVenda == 'CF' || $origemVenda =='CV' ||
                      $origemVenda == 'C'  || $origemVenda =='CI'){ //CLIENTE
                
	        $DevNotaOrigem->setOrigem('CLIE');
		$DevNotaOrigem->setResponsavel($_REQUEST['cli_co_numero_'.$cot]);
                $apresentaResponsavel = 'Cliente';
                
            }else if ($origemVenda =='T'  || $origemVenda == 'TR' || $origemVenda =='TZ' || $origemVenda == 'TA') {
                
	        $DevNotaOrigem->setOrigem($origemVenda);
		$DevNotaOrigem->setResponsavel($_REQUEST['responsavel_operador_tele'.$cot]);
                
                $apresentaResponsavel = "Televendas - ".$_REQUEST['responsavel_operador_tele'.$cot]. " - " . $_REQUEST['responsavel_nome_tele'.$cot];
                
            }else if ( $origemVenda =='B3'  || $origemVenda == 'E1' || $origemVenda =='E2' || $origemVenda == 'E3' ||
                       $origemVenda =='E4'  || $origemVenda == 'E5' || $origemVenda =='M'  || $origemVenda == 'MP' ||
                       $origemVenda =='PA'  || $origemVenda == 'PF' || $origemVenda =='V'  || $origemVenda == 'A'){
                
	        $DevNotaOrigem->setOrigem('REPR');
		$DevNotaOrigem->setResponsavel($_REQUEST['rpr_co_numero_'.$cot]); 
                
                $apresentaResponsavel = $_REQUEST['nome_setor_'.$cot];
                
            }else if ( $origemVenda == 'PI'){
                if ($_REQUEST['nome_operador_pi_'.$cot] != '' ){
	            $DevNotaOrigem->setOrigem($origemVenda);
		    $DevNotaOrigem->setResponsavel($_REQUEST['responsavel_operador_pi_'.$cot]);
                    
                    $apresentaResponsavel = 'Televendas PI - '. $_REQUEST['nome_operador_pi_'.$cot];
                    
                }else{
	            $DevNotaOrigem->setOrigem('REPR');
		    $DevNotaOrigem->setResponsavel($_REQUEST['rpr_co_numero_'.$cot]); 
                    
                    $apresentaResponsavel = $_REQUEST['nome_setor_'.$cot];
                }
            }else if ( substr($origemVenda ,0,1) == 'O'){
                if ($_REQUEST['nome_operador_hosp_'.$cot] != '' ){
	            $DevNotaOrigem->setOrigem($origemVenda);
		    $DevNotaOrigem->setResponsavel($_REQUEST['responsavel_operador_hosp_'.$cot]);   
                    
                    $apresentaResponsavel =  'CENTRAL HOSPITALAR - '. $_REQUEST['nome_operador_hosp_'.$cot];
                    
                }else{
	            $DevNotaOrigem->setOrigem('REPR');
		    $DevNotaOrigem->setResponsavel($_REQUEST['rpr_co_numero_'.$cot]);  
                    
                    $apresentaResponsavel = $_REQUEST['nome_setor_'.$cot];
                }
            }else if ( $origemVenda == 'D' ){
                    $DevNotaOrigem->setOrigem('DIRT');
                    
                    $apresentaResponsavel = 'Diretoria';
            }else{
	        $DevNotaOrigem->setOrigem('REPR');
		$DevNotaOrigem->setResponsavel($_REQUEST['rpr_co_numero_'.$cot]);   
                
                $apresentaResponsavel = $_REQUEST['nome_setor_'.$cot];
            }
            
        }    
        

	$DevNotaOrigem->setDescricaoResponsavel($_REQUEST['nto_tx_responsavel_'.$cot]); 
	$ObjDevolucao->setDevNotaOrigem($DevNotaOrigem); 
        

	if(!$OcorrenciaDao->atribuirResponsabilidade($ObjDevolucao)){	
		$msg .=  "<br />
				  <strong class='msmerro'>
						Desculpe, erro ao atribuir a responsabilidade na nota de origem 
						".$_REQUEST['nto_nu_serie_'.$cot]." ".$_REQUEST['nto_nu_nota_'.$cot]."
			  		</strong>			  
			  		<br />";
	}	
	else{
		$msg .= "<br />
				 <strong class='msmsucesso'>
					Foi definido o responsavel:<strong style='color:red;'> {$apresentaResponsavel} </strong> com sucesso 
					para nota ".$_REQUEST['nto_nu_serie_'.$cot]." ".$_REQUEST['nto_nu_nota_'.$cot]."				
				</strong>"; 
	}
}
?>
<div  style="text-decoration: ">
	<center>	
		<?php echo $msg;  ?>
		<br />
		<br />
		<input type="button" value="Voltar" onclick="javascript:atenderOcorrencia(<?php echo $_REQUEST['doc_co_numero']; ?>);" class="ui-state-default ui-corner-all button">
	</center>
</div>