<?php

error_reporting(E_ERROR);

include_once( "../../../../includes/Dao/DaoSistema.class.php" );
include_once( "../../../../includes/DevNotaOrigemItem.class.php" );
include_once( "../../../../includes/Dao/DevNotaOrigemItemDao.class.php" );
include_once( "../../../../includes/Dao/OcorrenciaDao.class.php" );
include_once( "../../../../includes/Ocorrencia.class.php" );
include_once( "../../../../includes/CalTipoOcorrencia.class.php" );
include_once( "../../../../includes/Dao/TipoOcorrenciaDAO.class.php" );
include_once( "../../../../includes/TipoDevolucao.class.php" );
include_once( "../../../../includes/Dao/TipoDevolucaoDao.class.php" );
include_once( "../../../../includes/Rota.class.php" );
include_once( "../../../../includes/Dao/RotaDao.class.php" );

include_once( "../../../../includes/Status.class.php" );
include_once( "../../../../includes/Dao/StatusDao.class.php" );

include_once( "../../../../includes/Passo.class.php" );
include_once( "../../../../includes/Dao/PassoDao.class.php" );

include_once( "../../../../includes/OcorrenciaPasso.class.php" );
include_once( "../../../../includes/Dao/OcorrenciaPassoDao.class.php" );

include_once( "../../../../includes/Conferente.class.php" );
include_once( "../../../../includes/Dao/ConferenteDao.class.php" );

include_once( "../../../../includes/LogAcaoPasso.class.php" );
include_once( "../../../../includes/Dao/LogAcaoPassoDao.class.php" );

include_once( "../../../../includes/OcorrenciaStatus.class.php" );
include_once( "../../../../includes/Dao/OcorrenciaStatusDao.class.php" );

include_once("DaoGlobal.class.php");
include_once("SistemaDao.class.php");
include_once("GlbSistema.class.php");
include_once("GlbSistemaPerfil.class.php");
include_once("GlbUsuario.class.php");
include_once("GlbColaborador.class.php");
include_once("GlbPessoa.class.php");
include_once("GlbEndereco.class.php");
include_once("GlbTelefone.class.php");
include_once("include_novo/GlbCentroDistribuicao.class.php");
include_once("include_novo/GlbTransportadora.class.php");
include_once("include_novo/GlbContatoTransportador.class.php");
include_once("include_novo/ContatoTransportadorDao.class.php");
include_once("GlbProgramaRotina.class.php");
include_once("GlbModuloPrograma.class.php");
include_once("GlbSistemaModulo.class.php");
include_once("PerfilUsuarioDao.class.php");
include_once("PerfilPermissaoDao.class.php");
include_once("integraSetSistemas.php");
$integra = new integraSetSistemas("conf.xml");
$ObjUsuario = unserialize($integra->getSistemaIntegra());
$ObjRotinas = $ObjUsuario->getObjSistemaPerfil()->getObjRotina();

extract($_REQUEST);

//status por  passo
$statusPasso = array();
$statusPasso[1] = 1;
$statusPasso[2] = 5;
$statusPasso[3] = 7;
$statusPasso[4] = 8;
$statusPasso[5] = 9;

//echo "<pre>";
//print_r($_REQUEST);
//echo "</pre>";
/*
  [codOcorrenciaReativar] => 16000823230001
  [codPassoReativar] => 2
  [solicitanteReativar] => tetsw
  [departamentoReativar] => ti
  [descReativar] => lsdksdvodsjvjsdopjvopsd
 */
$Ocorrencia = new Ocorrencia();
$OcorrenciaDao = new OcorrenciaDao();
$OcorrenciaPasso = new OcorrenciaPasso();
$OcorrenciaPassoDao = new OcorrenciaPassoDao();

$Ocorrencia->setCodigoDevolucao($codOcorrenciaReativar);
$Ocorrencia->setDocInTransmitido('N');
$Ocorrencia->setStatusOcorrencia($statusPasso[$codPassoReativar]); //indica o status da ocorrencia baseado no passo q se encontra , mudança solicitada de ultima hora (pode ser melhorada)
$Ocorrencia->setFinalizadoDevolucao('NULL');

/* Registrar a data de cancelamento na tabela junto com o numero do protocolo */

$Ocorrencia->setDataReativar($currentDateTime);

/* Criando objetos para gravação dos logs de reativação */
$DevLogAcaoPasso = new LogAcaoPasso();
$LogAcaoPassoDao = new LogAcaoPassoDao();

$OcorrenciaPasso->setCodigoOcorrencia($codOcorrenciaReativar);
$OcorrenciaPasso->setCodigoPasso($codPassoReativar);
$OcorrenciaPasso->setUsuario($ObjUsuario->getUsuarioCodigo());
$OcorrenciaPasso->setObservacao($descReativar);



$OcorrenciaDao->inserirDataReativar($codOcorrenciaReativar);

if ($OcorrenciaPassoDao->reativarPasso($OcorrenciaPasso)) {


    if ($OcorrenciaDao->alterar($Ocorrencia)) {

        $DevLogAcaoPasso->setCodOcorrencia($Ocorrencia->getCodigoDevolucao());

        $DevLogAcaoPasso->setCodOcorrencia($Ocorrencia->getCodigoDevolucao());
        $DevLogAcaoPasso->setNomeSolicitante($solicitanteReativar);
        $DevLogAcaoPasso->setNomeDepartamento($departamentoReativar);
        $DevLogAcaoPasso->setUsuario($ObjUsuario->getUsuarioCodigo());
        $DevLogAcaoPasso->setTipoUsuario('DEV');
        $DevLogAcaoPasso->setAcao('A');
        $DevLogAcaoPasso->setPasso('A');
        $DevLogAcaoPasso->setAcaoDescricao($descReativar);
        $DevLogAcaoPasso->setPassoPortal($codPassoReativar);
        

        $LogAcaoPassoDao->inserir($DevLogAcaoPasso);
        
        /* Registrar o status na tabela de Ocorrencia Status reativado 16*/
        $OcorrenciaStatusDao = new OcorrenciaStatusDao();
        $OcorrenciaStatus = new OcorrenciaStatus();
        $OcorrenciaStatus->setCodigoOcorrencia($Ocorrencia->getCodigoDevolucao());
        $OcorrenciaStatus->setUsuario($ObjUsuario->getUsuarioCodigo());
        $OcorrenciaStatus->setCodStatus(16);
        $OcorrenciaStatusDao->registrarSatus($OcorrenciaStatus);
        
        /* Registrar o status na tabela de Ocorrencia Status */ //indica o status da ocorrencia baseado no passo q se encontra , mudança solicitada de ultima hora (pode ser melhorada)
        $OcorrenciaStatusDao = new OcorrenciaStatusDao();
        $OcorrenciaStatus = new OcorrenciaStatus();
        $OcorrenciaStatus->setCodigoOcorrencia($Ocorrencia->getCodigoDevolucao());
        $OcorrenciaStatus->setUsuario($ObjUsuario->getUsuarioCodigo());
        $OcorrenciaStatus->setCodStatus($Ocorrencia->getStatusOcorrencia());
        $OcorrenciaStatusDao->registrarSatus($OcorrenciaStatus);



        
?>

<script>
	detalheOcorrencia('<?php echo $Ocorrencia->getCodigoDevolucao(); ?>');
</script>
<?php
        
    }
}

