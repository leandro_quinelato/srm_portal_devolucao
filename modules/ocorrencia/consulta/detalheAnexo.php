<?php
    session_start();
    include_once( "../../../includes/Dao/DaoSistema.class.php" );
    include_once( "../../../includes/OcorrenciaArquivos.class.php" );
    include_once( "../../../includes/Dao/OcorrenciaArquivosDao.class.php" );
    include_once( "../../../includes/TipoArquivoDevolucao.class.php" );
    include_once( "../../../includes/Dao/TipoArquivoDevolucaoDao.class.php" );

    include_once( "../../../includes/CalTipoOcorrencia.class.php" );
    include_once( "../../../includes/Dao/TipoOcorrenciaDAO.class.php" );
    include_once( "../../../includes/Ocorrencia.class.php" );
    include_once( "../../../includes/Dao/OcorrenciaDao.class.php" );

    include_once( "../../../includes/Passo.class.php" );
    include_once( "../../../includes/Dao/PassoDao.class.php" );

    include_once( "../../../includes/OcorrenciaPasso.class.php" );
    include_once( "../../../includes/Dao/OcorrenciaPassoDao.class.php" );

    include_once( "../../../includes/OcorrenciaArquivos.class.php" );
    include_once( "../../../includes/Dao/OcorrenciaArquivosDao.class.php" );
    include_once( "../../../includes/Dao/OcorrenciaStatusDao.class.php" );

    include_once("DaoGlobal.class.php");
    include_once("SistemaDao.class.php");
    include_once("GlbSistema.class.php");
    include_once("GlbSistemaPerfil.class.php");
    include_once("GlbUsuario.class.php");
    include_once("GlbColaborador.class.php");
    include_once("GlbPessoa.class.php");
    include_once("GlbEndereco.class.php");
    include_once("GlbTelefone.class.php");
    include_once("include_novo/GlbCentroDistribuicao.class.php");
    include_once("include_novo/GlbTransportadora.class.php");
    include_once("include_novo/GlbContatoTransportador.class.php");
    include_once("include_novo/ContatoTransportadorDao.class.php");
    include_once("GlbProgramaRotina.class.php");
    include_once("GlbModuloPrograma.class.php");
    include_once("GlbSistemaModulo.class.php");
    include_once("PerfilUsuarioDao.class.php");
    include_once("PerfilPermissaoDao.class.php");
    include_once("integraSetSistemas.php");
    $integra = new integraSetSistemas("conf.xml");
    $ObjUsuario = unserialize($integra->getSistemaIntegra());
    $ObjRotinas = $ObjUsuario->getObjSistemaPerfil()->getObjRotina();

    extract($_REQUEST);


    //echo "<pre>";
    //print_r($_REQUEST);
    //echo "</pre>";

    $Ocorrencia     = new Ocorrencia();
    $OcorrenciaDao  = new OcorrenciaDao();
    $Ocorrencia->setCodigoDevolucao($codOcorrencia);    
    $Ocorrencia->setTipoOcorrencia($tipoDevolucao);

    $OcorrenciaPasso = new OcorrenciaPasso();
    $OcorrenciaPassoDao = new OcorrenciaPassoDao();
    $OcorrenciaStatusDao = new OcorrenciaStatusDao();
    $OcorrenciaPasso->setCodigoOcorrencia($codOcorrencia);
    $OcorrenciaPasso->setCodigoPasso(1);
    $OcorrenciaPasso = $OcorrenciaPassoDao->consultarOcorrenciaPasso($OcorrenciaPasso);
    if ($OcorrenciaPasso[0]) {
        $OcorrenciaPasso = $OcorrenciaPasso[0];
    } else {
        $OcorrenciaPasso = new OcorrenciaPasso();
    }

    //echo "<pre>";
    //print_r($Ocorrencia);
    //echo "</pre>";
    //

    $retornoDados    = $OcorrenciaDao->consultaOcorrencia($Ocorrencia);
    $ocorrenciaDados = oci_fetch_object($retornoDados);
    $tipoDevolucao = $ocorrenciaDados->DOC_IN_TIPO;

    $OcorrenciaArquivos = new OcorrenciaArquivos();
    $OcorrenciaArquivos->setCodOcorrencia($codOcorrencia);
    $OcorrenciaArquivosDao = new OcorrenciaArquivosDao();
    $listaArquivos = $OcorrenciaArquivosDao->consultaArquivos($OcorrenciaArquivos);

    $TiposArquivoDevolucao = new TipoArquivoDevolucao();
    $TipoArquivoDevolucaoDao = new TipoArquivoDevolucaoDao();

    $TiposArquivoDevolucao = $TipoArquivoDevolucaoDao->consultarTipoArquivoAnexo();

    // ARQUIVO NOTA DE DEVOLUÇÃO
    $ArqNotaDev = new TipoArquivoDevolucao();
    $ArqNotaDev->setCodigoTipoArquivo(1);
    $ArqNotaDev = $TipoArquivoDevolucaoDao->consultarArquivo($ArqNotaDev);

    $DIRnotaDev = "http://" . $_SERVER['HTTP_HOST'] . $ArqNotaDev->getDiretorioArquivo() . "/arq";
    $DIRnotaDevTemp = "http://" . $_SERVER['HTTP_HOST'] . $ArqNotaDev->getDiretorioArquivo() . "/temp";

    // ARQUIVO Ordem de Coleta
    $ArqOrdemColeta = new TipoArquivoDevolucao();
    $ArqOrdemColeta->setCodigoTipoArquivo(2);
    $ArqOrdemColeta = $TipoArquivoDevolucaoDao->consultarArquivo($ArqOrdemColeta);

    $DIRordemColeta = "http://" . $_SERVER['HTTP_HOST'] . $ArqOrdemColeta->getDiretorioArquivo() . "/arq";
    $DIRordemColetaTemp = "http://" . $_SERVER['HTTP_HOST'] . $ArqOrdemColeta->getDiretorioArquivo() . "/temp";

    // ARQUIVO Espelho nota
    $ArqEspelho = new TipoArquivoDevolucao();
    $ArqEspelho->setCodigoTipoArquivo(3);
    $ArqEspelho = $TipoArquivoDevolucaoDao->consultarArquivo($ArqEspelho);

    $DIRespelho = "http://" . $_SERVER['HTTP_HOST'] . $ArqEspelho->getDiretorioArquivo() . "/arq";
    $DIRespelhoTemp = "http://" . $_SERVER['HTTP_HOST'] . $ArqEspelho->getDiretorioArquivo() . "/temp";

    $idArquivo = date('dmYhis');
?>
<form id="FormDetalheAnexo">
    <?php                                
        if($OcorrenciaDao->verificarTipoOcorrecia($Ocorrencia) != 1){// 1 = true
    ?>
    <div class="row">    
        <div id="areaInfoNfDev">   
            <div class="col-md-1 col-sm-4 col-xs-6">
                <div class="form-group">
                    <label></br>Série Dev</label>
                    <input type="text" class="form-control" placeholder="Série" id="serieDev" name="serieDev" onKeyPress="fMascara( 'numero', event, this )" >
                </div>
            </div>                                            
            <div class="col-md-1 col-sm-4 col-xs-6">
                <div class="form-group">
                    <label></br>Nota Dev</label>
                    <input type="text" class="form-control" placeholder="Numero" id="numDev" name="numDev" onKeyPress="fMascara( 'numero', event, this )" >
                </div>
            </div>                                            
            <div class="col-md-1 col-sm-4 col-xs-6">
                <div class="form-group">
                    <label>Data Nota Devolução</label>
                    <input type="text" class="form-control" placeholder="Data" id="dataDev" name="dataDev" onKeyPress="fMascara('data', event, this)" 
                            maxlength="10" onblur="javascript: avisoUsuario();">
                </div>
            </div>                                            
            <div class="col-md-1 col-sm-4 col-xs-6">
                <div class="form-group">
                    <label>Qtd Vol(s) à coletar</label>
                    <input type="text" class="form-control" placeholder="Volume" id="volumeDev" name="volumeDev" onKeyPress="fMascara('numero', event, this)" >
                </div>
            </div>
            <?php
                if ( ($ObjUsuario->getObjSistemaPerfil()->getNomeSistemaPerfil() == 'Administrador' || $ObjUsuario->getObjSistemaPerfil()->getNomeSistemaPerfil() == 'Devolucao') && ($ocorrenciaDados->DOC_NU_DEVSERIE != ""  && $ocorrenciaDados->DOC_NU_DEVNOTA != "") ){   
            ?>
                <div class="col-md-1 col-sm-4 col-xs-6">
                    <div class="form-group">
                        </br></br><a class="btn btn-warning BotSalvar" type="button" onclick="javascript: alterarNotaDevolucao(<?php echo $codOcorrencia; ?>);">Gravar Dados</a>
                    </div>
                </div>  
            <?php
                }
            ?>            
        </div>
    </div>  
    <?php
        }                                
    ?>
    <div class="row">  
        <div id="areaInfoAnexo">                                  
            <div class="col-md-3 col-sm-4 col-xs-6">
                <div class="form-group">
                    <label>Tipo documento</label>
                    <select class="form-control selectpicker" onchange="javascript: aprensentaNfDev(this.value);" id="tipoArquivoDev" name="tipoArquivoDev">
                        <?php
                            foreach ($TiposArquivoDevolucao as $TipoArquivoDev) {
                        ?>   
                                <option value="<?php echo $TipoArquivoDev->getCodigoTipoArquivo() ?>"><?php echo utf8_encode($TipoArquivoDev->getDescricaoTipoArquivo()) ?></option>              
                        <?php
                            }
                        ?>              
                    </select>
                </div>    
            </div>
            <div class="col-md-4 col-sm-4 col-xs-6">
                <div class="form-group">
                    <label>Observação</label>
                    <input type="text" class="form-control" placeholder="Observação" id="observacaoDev" name="observacaoDev">
                </div>    
            </div>
        </div>            
    </div>
    <?php   
        if( ($ObjUsuario->getObjSistemaPerfil()->getNomeSistemaPerfil() != 'Fiscal') && ($ObjUsuario->getObjSistemaPerfil()->getNomeSistemaPerfil() != 'Transporte') && ($ObjUsuario->getObjSistemaPerfil()->getNomeSistemaPerfil() != 'Cobranca') && ($ObjUsuario->getObjSistemaPerfil()->getNomeSistemaPerfil() != 'Farmaceutico') ){
            if( ( $tipoDevolucao != 3 &&  $OcorrenciaPasso->getIndicadorAprovado() == 1 ) ){ // permitir se for integral ato da entrega ou se pós recebimento e o passo gerente estiver aprovado
    ?>
                <div id="areaUploadDev">
                    <script>
                        $(document).ready(function () {
                            uploadArquivo('<?php echo $idArquivo; ?>');
                        });
                    </script>
                    <div class="row">
                        <div class="col-md-4 col-sm-6 col-xs-6">
                            <div class="form-group">
                                <label>Arquivo</label>
                                <input type="file" name="importaArquivo" id="importaArquivo" />
                                <div id="msgUploadArquivo"></div>                        
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <a class="btn btn-primary" id="incluirAnexo" onclick="javascript: adicionarAnexoNotaDevolucao(<?php echo $codOcorrencia; ?>);">Adicionar Arquivo</a>
                                <?php                                
                                if($OcorrenciaStatusDao->consultarStatus($codOcorrencia) != 1){
                                ?>
                                    <a class="btn btn-primary" id="incluirNFD" onclick="javascript: adicionarNotaDevolucao(<?php echo $codOcorrencia; ?>);">Adicionar NFD</a>
                                <?php
                                }                                
                                ?>
                            </div>
                        </div>                                                                                  
                    </div>
                    <input type="hidden" id="idArquivo" name="idArquivo" value="<?php echo $idArquivo; ?>"/>
                </div>
    <?php   
            }
        }else{
    ?>    
                <div id="areaUploadDev">
                </div>    
            <?php    
            }   
            ?>
</form>
<div id="retornoDetalheAnexo"></div>
<div class="row">
    <div class="col-md-12">
        <div id="areaListaArquivos">
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover TableNF">
                    <thead>
                        <tr>
                            <th>Tipo de documento</th>
                            <th>Descrição</th>
                            <th>Nome arquivo</th>
                            <th>Usuário</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>                               
                    <tbody>
                    <?php
                     while ($dados = oci_fetch_object($listaArquivos)) {
                    ?>
                        <tr>
                            <td><?php echo utf8_encode($dados->DTA_NO_DESCRICAO); ?></td>
                            <td><?php echo $dados->DOA_TX_OBSERVACAO->load(); ?></td>
                            <td><?php echo $dados->NOMEARQUIVO; ?></td>
                            <td><?php echo $dados->USU_NO_USERNAME == ''? 'SISTEMA' : $dados->USU_NO_USERNAME; ?></td>
                            <td><button class="fa fa-file-pdf-o" type="button" onclick="javascript: downloadArquivos(<?php  echo $dados->DOC_CO_NUMERO; ?>,<?php  echo $dados->DTA_CO_CODIGO; ?>,'<?php  echo $dados->NOMEARQUIVO; ?>');"></button></td>
                            <td>
                                <?php
                                   if( $dados->DTA_CO_CODIGO == 1){
                                ?>
                                    <a class="BotExcluir" onclick="javascript: ExcluirArquivoOcorrencia(<?php  echo $dados->DOC_CO_NUMERO; ?>,<?php  echo $dados->DTA_CO_CODIGO; ?>,'<?php  echo $dados->NOMEARQUIVO; ?>');">X</a>
                                <?php
                                   }
                                ?>                                    
                            </td>
                        </tr>
                    <?php
                     }
                    ?>                                                                                                  
                    </tbody>
                </table>
            </div> 
        </div>     
    </div>                                                                                   
</div> 