<?php
error_reporting(E_ERROR);
session_start();
include_once( "../../../includes/Dao/DaoSistema.class.php" );
include_once( "../../../includes/DevNotaOrigemItem.class.php" );
include_once( "../../../includes/Dao/DevNotaOrigemItemDao.class.php" );
include_once( "../../../includes/Dao/OcorrenciaDao.class.php" );
include_once( "../../../includes/Ocorrencia.class.php" );
include_once( "../../../includes/CalTipoOcorrencia.class.php" );
include_once( "../../../includes/Dao/TipoOcorrenciaDAO.class.php" );
include_once( "../../../includes/TipoDevolucao.class.php" );
include_once( "../../../includes/Dao/TipoDevolucaoDao.class.php" );
include_once( "../../../includes/Rota.class.php" );
include_once( "../../../includes/Dao/RotaDao.class.php" );


include_once( "../../../includes/Status.class.php" );
include_once( "../../../includes/Dao/StatusDao.class.php" );

include_once( "../../../includes/Passo.class.php" );
include_once( "../../../includes/Dao/PassoDao.class.php" );

include_once("DaoGlobal.class.php");
include_once("SistemaDao.class.php");
include_once("GlbSistema.class.php");
include_once("GlbSistemaPerfil.class.php");
include_once("GlbUsuario.class.php");
include_once("GlbColaborador.class.php");
include_once("GlbPessoa.class.php");
include_once("GlbEndereco.class.php");
include_once("GlbTelefone.class.php");
include_once("include_novo/GlbCentroDistribuicao.class.php");
include_once("GlbProgramaRotina.class.php");
include_once("GlbModuloPrograma.class.php");
include_once("GlbSistemaModulo.class.php");
include_once("PerfilUsuarioDao.class.php");
include_once("PerfilPermissaoDao.class.php");
include_once("integraSetSistemas.php");
$integra = new integraSetSistemas("conf.xml");
$ObjUsuario = unserialize($integra->getSistemaIntegra());
$ObjRotinas = $ObjUsuario->getObjSistemaPerfil()->getObjRotina();

include_once("include_novo/Tradutor.class.php");

$Tradutor = new Tradutor();

extract($_REQUEST);

$Ocorrencia     = new Ocorrencia();
$OcorrenciaDao  = new OcorrenciaDao();
$Ocorrencia->setCodigoDevolucao($codOcorrencia);    
$Ocorrencia->setTipoOcorrencia($tipoDevolucao);


$RotaDao = new RotaDao();
$OcorrenciaDao = new OcorrenciaDao();
$resultRota = $RotaDao->listar();

//echo "<pre>";
//print_r($Ocorrencia);
//echo "</pre>";
//
//
//
$retornoDados    = $OcorrenciaDao->consultaOcorrencia($Ocorrencia);
$ocorrenciaDados = oci_fetch_object($retornoDados);

/* FILTRO MOTIVO DEVOLUCAO*/
$TipoOcorrenciaDao = new TipoOcorrenciaDAO();
$TipoOcorrencias = new CalTipoOcorrencia();
$TipoOcorrencias = $TipoOcorrenciaDao->listarTipoOcorrencia($TipoOcorrencias);


?>
<script language="JavaScript">
    $(function() {
		$('ul#tabsList').each(function(){
            var $active, $content, $links = $(this).find('a');
            $active = $($links.filter('[href="'+location.hash+'"]')[0] || $links[0]);
            $active.addClass('active');
            $content = $($active[0].hash);
            $links.not($active).each(function () {
                $(this.hash).hide();
            });
            $(this).on('click', 'a', function(e){
                $active.removeClass('active');
                $content.hide();
                $active = $(this);
                $content = $(this.hash);
                $active.addClass('active');
                $content.show();
                e.preventDefault();
            });
        });
    });
</script>
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Protocolo: <?php echo $codOcorrencia; ?></h4>
        </div>
        <div class="col-md-12 TopModalOcorrrencia">
            <div class="row">
                <div class="col-md-3 col-sm-4 col-xs-4">
                    <div class="form-group"> 
                        <label>Cliente</label>
                        <p><?php echo $ocorrenciaDados->CLI_CO_NUMERO ." - ". $ocorrenciaDados->CLI_NO_FANTASIA; ?></p>
                    </div>
                </div>
                <div class="col-md-2 col-sm-4 col-xs-4">
                    <div class="form-group"> 
                        <label>Data cadastro</label>
                        <p><?php echo $ocorrenciaDados->DATA_EXIBICAO; ?></p>
                    </div>
                </div>
                <div class="col-md-2 col-sm-4 col-xs-4">
                    <div class="form-group">
                        <label>Área Cadastrante</label>
                        <p><?php echo $ocorrenciaDados->DOC_NO_DEPOCONTATO; ?></p>
                    </div>    
                </div> 
                <div class="col-md-3 col-sm-4 col-xs-4">
                    <div class="form-group">
                        <label>Status</label>
                        <p><?php echo utf8_encode($ocorrenciaDados->DST_NO_DESCRICAO); ?></p>
                    </div>    
                </div>                                    
                <div class="col-md-2 col-sm-4 col-xs-4">
                    <div class="form-group">
                        <label>Nota devolução</label>
                        <p>
                            <?php echo $ocorrenciaDados->DOC_NU_DEVSERIE . "  ". $ocorrenciaDados->DOC_NU_DEVNOTA ." - ". $ocorrenciaDados->DOC_DT_DEVNOTA ?>
                        </p>
                    </div>    
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="row">
                <div class="tabs">                            
                    <ul class="nav nav-tabs" role="tablist" id="tabsList">
                        <li class="active"><a href="#tab-det"       role="tab" data-toggle="tab" id="abaDetalhes"  >Detalhes</a></li>
                        <li>               <a href="#tab-anexo"     role="tab" data-toggle="tab" id="abaAnexo"     onclick="javascript:      $('#tab-anexo').load('modules/ocorrencia/consulta/detalheAnexo.php?codOcorrencia='+ <?php echo $codOcorrencia ?>);">NF DEvolução / Anexo</a></li>
                        <?php
                             if ($ObjUsuario->getUsuarioTipo() != "CLI") {
                        ?>
                        <li>               <a href="#tab-fluxo"     role="tab" data-toggle="tab" id="abaFluxo"     onclick="javascript:      $('#tab-fluxo').load('modules/ocorrencia/consulta/detalheFluxo.php?codOcorrencia='+ <?php echo $codOcorrencia ?>);">Fluxo</a></li>
                        <?php
                             }
                        ?>
                        <li>               <a href="#tab-historico" role="tab" data-toggle="tab" id="abaHistorico" onclick="javascript:  $('#tab-historico').load('modules/ocorrencia/consulta/detalheHistorico.php?codOcorrencia='+ <?php echo $codOcorrencia ?>);">Histórico</a></li>
                    </ul>                         
                    <div class="panel-body tab-content">
                        <!--inicio tab detalhes-->
                        <div class="tab-pane active" id="tab-det">
                            <div class="row">                                    
                                <div class="col-md-3 col-sm-6 col-xs-6">
                                    <div class="form-group">
                                        <label>Motivo</label>
                                        <select class="form-control selectpicker" id="motivoAlterar" name="motivoAlterar">
                                            <?php
                                                foreach ($TipoOcorrencias as $TipoOcorrencia) {
                                            ?>
                                                <option value="<?php echo $TipoOcorrencia->getCodTipoOcorrencia(); ?>" <?php echo $TipoOcorrencia->getCodTipoOcorrencia() == $ocorrenciaDados->TOC_CO_NUMERO ? 'selected' : '' ; ?>><?php echo $TipoOcorrencia->getNomTipoOcorrencia(); ?></option>
                                            <?php
                                                }
                                            ?>
                                        </select>
                                    </div>    
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    
<?php
if ($ocorrenciaDados->DOC_TX_DESCRICAO) {
    $txdescricao = $ocorrenciaDados->DOC_TX_DESCRICAO->load();
}
?>
                                    <div class="form-group">
                                        <label>Detalhes</label>
                                        <textarea placeholder="Descrição" rows="1" class="form-control" name="descAlterar" id="descAlterar" <?php if ( ($ObjUsuario->getObjSistemaPerfil()->getNomeSistemaPerfil() != 'Administrador') && ($ObjUsuario->getObjSistemaPerfil()->getNomeSistemaPerfil() != 'Devolucao')){
                                                                    echo "readonly";
                                                                } ?>><?php echo strtoupper(utf8_encode($txdescricao));?></textarea>
                                    </div>
                                </div>                                            
                            </div> 
                            <?php
                                if ($ocorrenciaDados->DST_CO_NUMERO == 12){
                                    if ( ($ObjUsuario->getObjSistemaPerfil()->getNomeSistemaPerfil() == 'Administrador') || ($ObjUsuario->getObjSistemaPerfil()->getNomeSistemaPerfil() == 'Devolucao') ){
                            ?>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <a  class="buttonFinish btn btn-success pull-left btn-xs" style="display: block;" onclick="javascript: abrirReativar(<?php echo $ocorrenciaDados->DOC_CO_NUMERO; ?>,<?php echo $ocorrenciaDados->DPA_CO_NUMERO; ?>);">Reativar ocorrencia</a>
                                            </div>
                                        </div>
                            <?php
                                    }
                                }
                            ?>

                            <?php
                                if ($ocorrenciaDados->DST_CO_NUMERO <> 12){
                                    if ( ($ObjUsuario->getObjSistemaPerfil()->getNomeSistemaPerfil() == 'Administrador') || ($ObjUsuario->getObjSistemaPerfil()->getNomeSistemaPerfil() == 'Devolucao') || ($ObjUsuario->getObjSistemaPerfil()->getNomeSistemaPerfil() == 'Cliente' and $ocorrenciaDados->DST_CO_NUMERO < 4 and $ocorrenciaDados->DOC_CO_CADASTRANTE == $ObjUsuario->getUsuarioCodigo() ) ){
                            ?>	
                                        <div class="row">
                                            <div class="col-md-12">
                                                <a  class="buttonFinish btn btn-success pull-left btn-xs" style="display: block;" onclick="javascript: abrirCancelar(<?php echo $ocorrenciaDados->DOC_CO_NUMERO; ?>,<?php echo $ocorrenciaDados->DPA_CO_NUMERO; ?>);">Cancelar ocorrencia</a>
                                            </div>
                                        </div>
                            <?php
                                    }
                                }
                            ?>							
                            
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="page-subtitle">
                                        <h3>NFs Venda</h3>
                                    </div>
                                </div>
                                <div class="col-md-12">
<?php

    $NotasOrigem = new DevNotaOrigem();
    $Ocorrencia->setCodigoDevolucao($codOcorrencia);
    $NotasOrigem = $OcorrenciaDao->consultaNotasDevolucao($Ocorrencia);
?>
                                    <div class="tabs">                            
                                        <ul class="nav nav-tabs nav-tabs-arrowed TabNFTit" role="tablist" id="tabsListNota">
                                            <?php
                                            foreach ($NotasOrigem as $NotaOrigem) {
                                            ?>
                                            <li><a href="#tabNota_<?php echo $NotaOrigem->getNota() ?>" role="tab" data-toggle="tab" id="abanota_<?php echo $NotaOrigem->getNota() ?>">NF <?php echo $NotaOrigem->getSerie(). " - " . $NotaOrigem->getNota(); ?> </a></li>
                                            <?php
                                            }
                                            ?>
                                        </ul> 
                                        <div class="panel-body tab-content TabNF">
                                            <?php
                                            foreach ($NotasOrigem as $NotaOrigem) {
                                            ?>
                                            <div class="tab-pane " id="tabNota_<?php echo $NotaOrigem->getNota() ?>">
                                                <div class="row">
                                                    <div class="col-md-2 col-sm-4 col-xs-4">
                                                        <div class="form-group"> 
                                                            <label>NF</label>
                                                            <p><?php echo $NotaOrigem->getSerie(). " / ".$NotaOrigem->getNota();  ?> </p>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2 col-sm-4 col-xs-4">
                                                        <div class="form-group"> 
                                                            <label>Emissão</label>
                                                            <p><?php echo $NotaOrigem->getData(); ?></p>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2 col-sm-4 col-xs-4">
                                                        <div class="form-group">
                                                            <label>Tipo</label>
                                                            <p><?php echo utf8_encode($ocorrenciaDados->TID_CO_DESCRICAO); ?></p>
                                                        </div>    
                                                    </div> 
                                                    <div class="col-md-2 col-sm-4 col-xs-4">
                                                        <div class="form-group">
                                                            <label>CD</label>
                                                            <p><?php echo $ocorrenciaDados->CED_NO_CENTRO; ?></p>
                                                        </div>    
                                                    </div>                                    
                                                    <div class="col-md-2 col-sm-4 col-xs-4">
                                                        <div class="form-group">
                                                            <label>Divisão</label>
                                                            <p><?php echo $ocorrenciaDados->DEV_CO_DIVISAO; ?></p>
                                                        </div>    
                                                    </div>                                    
                                                    <div class="col-md-2 col-sm-4 col-xs-4">
                                                        <div class="form-group">
                                                            <label>Rota</label>
                                                            <?php if ( ($ObjUsuario->getObjSistemaPerfil()->getNomeSistemaPerfil() == 'Administrador') || ($ObjUsuario->getObjSistemaPerfil()->getNomeSistemaPerfil() == 'Devolucao')){ ?>
                                                             
                                                            <select class="form-control selectpicker" id="rotaAlterar" name="rotaAlterar">
                                                                <option value=""></option>
                                                                <?php
                                                                while (OCIFetchInto($resultRota, $rowRota, OCI_ASSOC)) {
                                                                    ?>
                                                                    <option value="<?php echo $rowRota['ROT_CO_NUMERO']; ?>"  <?php if ($ocorrenciaDados->ROT_CO_NUMERO == $rowRota['ROT_CO_NUMERO']) {
                                                                    echo "selected='selected'";
                                                                } ?>>
                                                                    <?php echo $rowRota['ROT_CO_NUMERO']; ?> 
                                                                    </option>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </select>
                                                             
                                                                
                                                            <?php }else{ ?>
                                                                    <p><?php echo $ocorrenciaDados->ROT_CO_NUMERO; ?></p>
                                                            
                                                            <?php } ?>
                                                            
                                                           
                                                        </div>    
                                                    </div>
                                                    <div class="col-md-2 col-sm-4 col-xs-4">
                                                        <div class="form-group">
                                                            <label>Qtd. Volume(s)</label>
                                                            <input  type="text" class="form-control" name="volumeAlterar" id="volumeAlterar" maxlength="10" value="<?php echo $ocorrenciaDados->DOC_QT_PRODUTO; ?>" <?php if ( ($ObjUsuario->getObjSistemaPerfil()->getNomeSistemaPerfil() != 'Administrador') && ($ObjUsuario->getObjSistemaPerfil()->getNomeSistemaPerfil() != 'Devolucao')){
                                                                    echo "readonly";
                                                                } ?>>
                                                        </div>    
                                                    </div>
                                                    <div class="col-md-2 col-sm-4 col-xs-4">
                                                        <div class="form-group">
                                                            <label>Total Devolução</label>
                                                            <p><?php echo $Tradutor->formatar($NotaOrigem->getValorTotDevolucao(), 'MOEDA');?></p>
                                                        </div>    
                                                    </div>

                                                </div>                                                            
                                                <div class="table-responsive">
                                                    <table class="table table-bordered table-striped table-hover TableNF">
                                                        <thead>
                                                            <tr>
<!--                                                                <th>
                                                        <div class="checkbox checkbox-inline">
                                                            <input type="checkbox" id="check_1">
                                                            <label for="check_1"></label>
                                                        </div>
                                                        </th>-->
                                                            <th>CÓD SERVIMED</th>
                                                            <th>DESC. PRODUTO</th>
                                                            <th>QTD UNI - NF</th>
                                                            <th>QTD UNI - À DEVOLVER</th>
                                                            <th>PREÇO UNI</th>
                                                            <th>TOTAL BRUTO</th>
                                                            <th>VALOR DESCONTO</th>
                                                            <th>VALOR IMPOSTO</th>
                                                            <th>TOTAL LIQ</th>
                                                        </tr>

                                                        </thead>                               
                                                        <tbody>
                                                        <?php    
                                                            $DevNotaOrigemItem = new DevNotaOrigemItem();
                                                            $DevNotaOrigemItem->setCodigoOcorrencia($ocorrenciaDados->DOC_CO_NUMERO);
                                                            $DevNotaOrigemItem->setSerie($NotaOrigem->getSerie());
                                                            $DevNotaOrigemItem->setNumNota($NotaOrigem->getNota());
                                                            $DevNotaOrigemItemDao = new DevNotaOrigemItemDao();
                                                            $DevNotaOrigemItem = $DevNotaOrigemItemDao->consultarItemNotasDevolucao($DevNotaOrigemItem);
                                                            
                                                            foreach ($DevNotaOrigemItem as $NotaOrigemItem) {
                                                        ?>    
                                                            <tr>
<!--                                                                <td>
                                                                    <div class="checkbox checkbox-inline">
                                                                        <input type="checkbox" id="check_2">
                                                                        <label for="check_2"></label>
                                                                    </div>
                                                                </td>-->
                                                                <td><?php echo $NotaOrigemItem->getCodProduto(); ?></td>
                                                                <td><?php echo $NotaOrigemItem->getNomeProduto();?></td>
                                                                <td><?php echo $NotaOrigemItem->getNotaQuantidade();?></td>
                                                                <td>
                                                                    <div class="form-group">
                                                                        <?php
                                                                        if ($ocorrenciaDados->TID_IN_DEVOLUCAO == "I") {
                                                                            ?>
                                                                        <input type="text" class="form-control" placeholder="Devolver" id="notaItemQtdDev_<?php echo $NotaOrigemItem->getCodProduto(); ?>" name="notaItemQtdDev[]" value="<?php echo $NotaOrigemItem->getDevQuantidade(); ?>" readonly>
                                                                            <?php
                                                                        } else {
                                                                            ?>
                                                                            <input type="text" class="form-control" placeholder="Devolver" id="notaItemQtdDev_<?php echo $NotaOrigemItem->getCodProduto(); ?>" name="notaItemQtdDev[]" onKeyPress="fMascara('numero', event, this)" onchange="javascript:verificaQuantidade(<?php echo $NotaOrigemItem->getCodProduto(); ?>)"  value="<?php echo $NotaOrigemItem->getDevQuantidade(); ?>">
                                                                            <?php
                                                                        }
                                                                        ?>
                                                                    </div>
                                                                </td>
                                                                <td><?php echo $Tradutor->formatar($NotaOrigemItem->getPrecoUnitario(), 'MOEDA');?></td>
                                                                <td><?php echo $Tradutor->formatar($NotaOrigemItem->getTotalBruto(), 'MOEDA');?></td>
                                                                <td><?php echo $Tradutor->formatar($NotaOrigemItem->getTotalDesconto(), 'MOEDA');?></td>
                                                                <td><?php echo $Tradutor->formatar($NotaOrigemItem->getTotalImposto(), 'MOEDA');?></td>
                                                                <td><?php echo $Tradutor->formatar($NotaOrigemItem->getTotalLiquido(), 'MOEDA');?></td>
                                                            </tr>
                                                            <?php
                                                            }
                                                            ?>
                                        
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <?php
                                            }
                                            ?>
                                            <script language="JavaScript">
                                                $(function() {
                                                    $('ul#tabsListNota').each(function(){
                                                        var $active, $content, $links = $(this).find('a');
                                                        $active = $($links.filter('[href="'+location.hash+'"]')[0] || $links[0]);
                                                        $active.addClass('active');
                                                        $active.click();
                                                        $content = $($active[0].hash);
                                                        $links.not($active).each(function () {
                                                            $(this.hash).hide();
                                                        });
                                                        $(this).on('click', 'a', function(e){
                                                            $active.removeClass('active');
                                                            $content.hide();
                                                            $active = $(this);
                                                            $content = $(this.hash);
                                                            $active.addClass('active');
                                                            $content.show();
                                                            e.preventDefault();
                                                        });
                                                    });
                                                });
                                            </script> 
                                            <div class="tab-pane" id="tab1-third">
                                                <p>Vestibulum cursus augue sed leo tempor, at aliquam orci dictum. Sed mattis metus id velit aliquet, et interdum nulla porta. Etiam euismod pellentesque purus, in fermentum eros venenatis ut. Praesent vitae nibh ac augue gravida lacinia non a ipsum. Aenean vestibulum eu turpis eu posuere. Sed eget lacus lacinia, mollis urna et, interdum dui. Donec sed diam ut metus imperdiet malesuada. Maecenas tincidunt ultricies ipsum, lobortis pretium dolor sodales ut. Donec nec fringilla nulla. In mattis sapien lorem, nec tincidunt elit scelerisque tempus.</p>
                                            </div>
                                            <div class="tab-pane" id="tab1-fourth">
                                                <p>In mattis sapien lorem, nec tincidunt elit scelerisque tempus. Quisque nisl nisl, venenatis eget dignissim et, adipiscing eu tellus. Sed nulla massa, luctus id orci sed, elementum consequat est. Proin dictum odio quis diam gravida facilisis. Sed pharetra dolor a tempor tristique. Sed semper sed urna ac dignissim. Aenean fermentum leo at posuere mattis. Etiam vitae quam in magna viverra dictum. Curabitur feugiat ligula in dui luctus, sed aliquet neque posuere.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>                                                                                   
                            </div> 
                            <?php
                                if ( ($ObjUsuario->getObjSistemaPerfil()->getNomeSistemaPerfil() == 'Administrador') || ($ObjUsuario->getObjSistemaPerfil()->getNomeSistemaPerfil() == 'Devolucao')){
                            ?>
                            <a class="btn btn-warning" type="button" onclick="javascript: alterarDetalhesOcorrencia(<?php echo $codOcorrencia; ?>);">Salvar</a>
                            <?php
                                }
                            ?>
                        </div>
                        <!--fim tab detalhes-->
                        
                        <div class="tab-pane" id="tab-anexo">

                        </div>                                    

                        <div class="tab-pane" id="tab-fluxo">
                        
                        </div>
                        
                        <div class="tab-pane" id="tab-historico">

                        </div>  
                        
                        
                    </div>
                </div>                        

            </div>                    
        </div>                    
        <div class="modal-footer">
            <button data-dismiss="modal" class="btn btn-default" type="button">Cancelar</button>
        </div>                                                    
    </div>
</div>
<div class="modal fade" id="modalReativarDevolucao" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

</div> 
<div class="modal fade" id="modalCancelarDevolucao" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

</div> 
<script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/plugins/smartwizard/jquery.smartWizard.js"></script>
<script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/plugins/smartwizard/smartwizard/jquery.smartWizard.min.js"></script>