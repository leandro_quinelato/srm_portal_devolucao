<?php
@session_start();

include_once( "../../../includes/Dao/DaoSistema.class.php" );
include_once( "../../../includes/OcorrenciaArquivos.class.php" );
include_once( "../../../includes/Dao/OcorrenciaArquivosDao.class.php" );
include_once( "../../../includes/TipoArquivoDevolucao.class.php" );
include_once( "../../../includes/Dao/TipoArquivoDevolucaoDao.class.php" );

include_once( "../../../includes/DevNotaOrigemItem.class.php" );
include_once( "../../../includes/Dao/DevNotaOrigemItemDao.class.php" );
include_once( "../../../includes/Dao/OcorrenciaDao.class.php" );
include_once( "../../../includes/Ocorrencia.class.php" );
include_once( "../../../includes/CalTipoOcorrencia.class.php" );
include_once( "../../../includes/Dao/TipoOcorrenciaDAO.class.php" );
include_once( "../../../includes/TipoDevolucao.class.php" );
include_once( "../../../includes/Dao/TipoDevolucaoDao.class.php" );
include_once( "../../../includes/Rota.class.php" );
include_once( "../../../includes/Dao/RotaDao.class.php" );

include_once( "../../../includes/OcorrenciaStatus.class.php" );
include_once( "../../../includes/Dao/OcorrenciaStatusDao.class.php" );

include_once("DaoGlobal.class.php");
include_once("SistemaDao.class.php");
include_once("GlbSistema.class.php");
include_once("GlbSistemaPerfil.class.php");
include_once("GlbUsuario.class.php");
include_once("GlbColaborador.class.php");
include_once("GlbPessoa.class.php");
include_once("GlbEndereco.class.php");
include_once("GlbTelefone.class.php");
include_once("include_novo/GlbTransportadora.class.php");
include_once("include_novo/GlbContatoTransportador.class.php");
include_once("include_novo/ContatoTransportadorDao.class.php");
include_once("GlbProgramaRotina.class.php");
include_once("GlbModuloPrograma.class.php");
include_once("GlbSistemaModulo.class.php");
include_once("PerfilUsuarioDao.class.php");
include_once("PerfilPermissaoDao.class.php");
include_once("integraSetSistemas.php");
include_once("class.phpmailer.php");
include_once("class.smtp.php");

include_once("include_novo/Tradutor.class.php");

$integra = new integraSetSistemas("conf.xml");
$ObjUsuario = unserialize($integra->getSistemaIntegra());

extract($_REQUEST);

if ($acao == 'GRAVAR') {

    $OcorrenciaArquivosDao = new OcorrenciaArquivosDao();

    $TipoArquivoDevolucaoDao = new TipoArquivoDevolucaoDao();
    // ARQUIVO NOTA DE DEVOLUÇÃO
    $ArqNotaDev = new TipoArquivoDevolucao();
    $ArqNotaDev->setCodigoTipoArquivo(1);
    $ArqNotaDev = $TipoArquivoDevolucaoDao->consultarArquivo($ArqNotaDev);

    $DIRnotaDev = "../../../" . $ArqNotaDev->getDiretorioArquivo() . "arq/";
    $DIRnotaDevTemp = "../../.." . $ArqNotaDev->getDiretorioArquivo() . "temp/";

    $OcorrenciaNotaDev = new OcorrenciaArquivos();
    $OcorrenciaNotaDev->setCodOcorrencia($codOcorrencia);
    $OcorrenciaNotaDev->setCodigoTipoArquivo($ArqNotaDev->getCodigoTipoArquivo());
    $existeNotaDev = FALSE;// $OcorrenciaArquivosDao->verificaNotaDev($OcorrenciaNotaDev);
    //
    //echo "<pre>";
    //print_r($_REQUEST);
    //echo "</pre>";
    //die();
    $OcorrenciaStatusDao = new OcorrenciaStatusDao();
    $OcorrenciaDao = new OcorrenciaDao();
    $Ocorrencia = new Ocorrencia();
    $Ocorrencia->setCodigoDevolucao($codOcorrencia);
    $Ocorrencia->setSerieNfDevolucao($serieDev);
    $Ocorrencia->setNotaNfDevolucao($numDev);
    $Ocorrencia->setDataNfDevolucao($dataDev);
    $Ocorrencia->setQntdVolumeDevolucao($volumeDev);

    if ($OcorrenciaDao->alterar($Ocorrencia)) {

        if (!$existeNotaDev) {
            /* informa neste ponto que o status da ocorrencia é:  NFD INFORMADA(4) */
            $Ocorrencia->setStatusOcorrencia(4);
            /* Registrar o status na tabela de Ocorrencia Status */
            $OcorrenciaStatus = new OcorrenciaStatus();
            $OcorrenciaStatus->setCodigoOcorrencia($Ocorrencia->getCodigoDevolucao());
            $OcorrenciaStatus->setUsuario($ObjUsuario->getUsuarioCodigo());
            $OcorrenciaStatus->setCodStatus($Ocorrencia->getStatusOcorrencia());
            $OcorrenciaStatusDao->registrarSatus($OcorrenciaStatus);

            /* informa neste ponto que o status da ocorrencia é:  AGUARDANDO COLETA(5) */
            $Ocorrencia->setStatusOcorrencia(5);
            /* Registrar o status na tabela de Ocorrencia Status */
            $OcorrenciaStatus = new OcorrenciaStatus();
            $OcorrenciaStatus->setCodigoOcorrencia($Ocorrencia->getCodigoDevolucao());
            $OcorrenciaStatus->setUsuario($ObjUsuario->getUsuarioCodigo());
            $OcorrenciaStatus->setCodStatus($Ocorrencia->getStatusOcorrencia());
            $OcorrenciaStatusDao->registrarSatus($OcorrenciaStatus);
            
            //salva arquivo(ordem coleta) na tabela DEV_OCORRENCIA_ARQUIVO
            $nomeOrdemSystem = "ordemColeta_{$Ocorrencia->getCodigoDevolucao()}.pdf";
            $OcorrenciaArquivosDao = new OcorrenciaArquivosDao();
            $OcorrenciaArquivos = new OcorrenciaArquivos();
            $OcorrenciaArquivos->setCodOcorrencia($Ocorrencia->getCodigoDevolucao());
            $OcorrenciaArquivos->setCodigoTipoArquivo(2); // ordem coleta 2
            // $OcorrenciaArquivos->setNomeArquivoCliente($nomeEspelhoSystem);
            $OcorrenciaArquivos->setNomeArquivoSystem($nomeOrdemSystem);
            // $OcorrenciaArquivos->setCodigoUsuario($ObjUsuario->getUsuarioCodigo());
            $OcorrenciaArquivos->setObservacaoArquivo("Ordem coleta para a ocorrência: ".$Ocorrencia->getCodigoDevolucao() );
            $OcorrenciaArquivosDao->registrarArquivo($OcorrenciaArquivos);
            
        }
        if (!$OcorrenciaDao->alterar($Ocorrencia)) {
            echo "ERRO";
        }

        echo "Sucesso!";
    } else {
        echo "ERRO";
    }
} else if ($acao == 'GRAVAR_ARQUIVO') {

    $OcorrenciaArquivosDao = new OcorrenciaArquivosDao();

    $TipoArquivoDevolucaoDao = new TipoArquivoDevolucaoDao();
    // ARQUIVO NOTA DE DEVOLUÇÃO
    $ArqNotaDev = new TipoArquivoDevolucao();
    $ArqNotaDev->setCodigoTipoArquivo(1);
    $ArqNotaDev = $TipoArquivoDevolucaoDao->consultarArquivo($ArqNotaDev);

    $DIRnotaDev = "../../../" . $ArqNotaDev->getDiretorioArquivo() . "arq/";
    $DIRnotaDevTemp = "../../.." . $ArqNotaDev->getDiretorioArquivo() . "temp/";

    $OcorrenciaNotaDev = new OcorrenciaArquivos();
    $OcorrenciaNotaDev->setCodOcorrencia($codOcorrencia);
    $OcorrenciaNotaDev->setCodigoTipoArquivo($ArqNotaDev->getCodigoTipoArquivo());
    $existeNotaDev = FALSE;// $OcorrenciaArquivosDao->verificaNotaDev($OcorrenciaNotaDev);

    $OcorrenciaStatusDao = new OcorrenciaStatusDao();
    $OcorrenciaDao = new OcorrenciaDao();
    $Ocorrencia = new Ocorrencia();
    $Ocorrencia->setCodigoDevolucao($codOcorrencia);
    //$Ocorrencia->setSerieNfDevolucao($serieDev);
    //$Ocorrencia->setNotaNfDevolucao($numDev);
    //$Ocorrencia->setDataNfDevolucao($dataDev);
    //$Ocorrencia->setQntdVolumeDevolucao($volumeDev);

    if ($OcorrenciaDao->alterar($Ocorrencia)) {
        if ($handle = opendir($DIRnotaDevTemp)) {
            $gravaArquivo = false;
            while (false !== ($file = readdir($handle))) {
                $nomeArquivoTemp = explode("_", $file);

                if ($nomeArquivoTemp[1] == $idArquivo) {
                    $gravaArquivo = true;
                    $explode = explode(".", $file);
                    $tipo_arquivo = end($explode);                  
                    $nomeNotaDev = "NotaDevolucao_" . $codOcorrencia . "." . $tipo_arquivo;

                    rename($DIRnotaDevTemp . $file, $DIRnotaDev . $nomeNotaDev);
                }
            }

            if ($gravaArquivo) {
                if (!$existeNotaDev) {
                    $OcorrenciaArquivos = new OcorrenciaArquivos();

                    $OcorrenciaArquivos->setCodOcorrencia($codOcorrencia);
                    $OcorrenciaArquivos->setCodigoTipoArquivo($ArqNotaDev->getCodigoTipoArquivo());
                    $OcorrenciaArquivos->setCodigoUsuario($ObjUsuario->getUsuarioCodigo());
                    $OcorrenciaArquivos->setNomeArquivoCliente($nomeArqCliente);
                    $OcorrenciaArquivos->setNomeArquivoSystem($nomeNotaDev);
                    $OcorrenciaArquivos->setObservacaoArquivo($observacaoDev);
                    $OcorrenciaArquivosDao->registrarArquivo($OcorrenciaArquivos);
                }
            }
        }
        if (!$OcorrenciaDao->alterar($Ocorrencia)) {
            echo "ERRO";
        }
        echo "Sucesso!";
    } else {
        echo "ERRO";
    }
} else if ($acao == 'LISTAR') {

    $OcorrenciaArquivos = new OcorrenciaArquivos();
    $OcorrenciaArquivos->setCodOcorrencia($codOcorrencia);
    $OcorrenciaArquivosDao = new OcorrenciaArquivosDao();
    $listaArquivos = $OcorrenciaArquivosDao->consultaArquivos($OcorrenciaArquivos);
    ?>
        Aguarde...
    <script>
        alert('Operação realizada com sucesso. Aguarde a página recarregar...');
        $('#tab-anexo').load('modules/ocorrencia/consulta/detalheAnexo.php?codOcorrencia='+ <?php echo $codOcorrencia ?>);
    </script>
    
    <?php
        } 
        else if ($acao == 'ALTERAR') {    
        
        $OcorrenciaStatusDao = new OcorrenciaStatusDao();
        $OcorrenciaDao = new OcorrenciaDao();
        $Ocorrencia = new Ocorrencia();
        $Ocorrencia->setCodigoDevolucao($codOcorrencia);
        $Ocorrencia->setSerieNfDevolucao($serieDev);
        $Ocorrencia->setNotaNfDevolucao($numDev);
        $Ocorrencia->setDataNfDevolucao($dataDev);
        $Ocorrencia->setQntdVolumeDevolucao($volumeDev);

        if ($OcorrenciaDao->alterar($Ocorrencia)) {
    ?> 
        
        <script>
            alert('Nota Alterada com sucesso! ');
            $('#tab-anexo').load('modules/ocorrencia/consulta/detalheAnexo.php?codOcorrencia='+ <?php echo $codOcorrencia ?>);
        </script>
        
<?php  
    }
    
    
} else {
    $OcorrenciaArquivosDao = new OcorrenciaArquivosDao();
    $OcorrenciaArquivos = new OcorrenciaArquivos();

    $OcorrenciaArquivos->setCodOcorrencia($codOcorrencia);
    $OcorrenciaArquivos->setCodigoTipoArquivo($codigoTipoArquivo);
    $OcorrenciaArquivos->setNomeArquivoCliente($nomeTipoArquivo);    
    $result = $OcorrenciaArquivosDao->consultaArquivos($OcorrenciaArquivos);
    $dados = oci_fetch_object($result);

    if ($OcorrenciaArquivosDao->ExcluirArquivo($OcorrenciaArquivos)) {
        echo "Arquivo excluido com sucesso!";

        $arquivo = "../../.." . $dados->DTA_NO_PATH . "arq/" . $dados->DOA_NO_ARQSYSTEM;
//        echo $arquivo;
        unlink($arquivo);
    } else {
        echo "Falha ao excluir arquivo!";
    }
}
//1 - gravar a temp para oficial - ok
//2 - salvar a nota de devolucao na tabela DEV_OCORRENCIA - ok
//3 - salvar o arquivo na tabela DEV_OCORRENCIA_ARQUIVO - ok
//4 - alterar status(pois a notadev foi informada)


// 1