<?php
error_reporting(E_ERROR);
include_once( "../../../includes/Dao/DaoSistema.class.php" );
include_once( "../../../includes/DevNotaOrigemItem.class.php" );
include_once( "../../../includes/Dao/DevNotaOrigemItemDao.class.php" );
include_once( "../../../includes/Dao/OcorrenciaDao.class.php" );
include_once( "../../../includes/Ocorrencia.class.php" );
include_once( "../../../includes/CalTipoOcorrencia.class.php" );
include_once( "../../../includes/Dao/TipoOcorrenciaDAO.class.php" );
include_once( "../../../includes/TipoDevolucao.class.php" );
include_once( "../../../includes/Dao/TipoDevolucaoDao.class.php" );
include_once( "../../../includes/Rota.class.php" );
include_once( "../../../includes/Dao/RotaDao.class.php" );


include_once( "../../../includes/Status.class.php" );
include_once( "../../../includes/Dao/StatusDao.class.php" );

include_once( "../../../includes/Passo.class.php" );
include_once( "../../../includes/Dao/PassoDao.class.php" );

include_once( "../../../includes/OcorrenciaPasso.class.php" );
include_once( "../../../includes/Dao/OcorrenciaPassoDao.class.php" );

include_once("DaoGlobal.class.php");
include_once("SistemaDao.class.php");
include_once("GlbSistema.class.php");
include_once("GlbSistemaPerfil.class.php");
include_once("GlbUsuario.class.php");
include_once("GlbColaborador.class.php");
include_once("GlbPessoa.class.php");
include_once("GlbEndereco.class.php");
include_once("GlbTelefone.class.php");
include_once("include_novo/GlbCentroDistribuicao.class.php");
include_once("include_novo/GlbTransportadora.class.php");
include_once("include_novo/GlbContatoTransportador.class.php");
include_once("include_novo/ContatoTransportadorDao.class.php");
include_once("GlbProgramaRotina.class.php");
include_once("GlbModuloPrograma.class.php");
include_once("GlbSistemaModulo.class.php");
include_once("PerfilUsuarioDao.class.php");
include_once("PerfilPermissaoDao.class.php");
include_once("integraSetSistemas.php");
$integra = new integraSetSistemas("conf.xml");
$ObjUsuario = unserialize($integra->getSistemaIntegra());
$ObjRotinas = $ObjUsuario->getObjSistemaPerfil()->getObjRotina();

include_once("include_novo/Tradutor.class.php");

$Tradutor = new Tradutor();

extract($_REQUEST);

$Ocorrencia     = new Ocorrencia();
$OcorrenciaDao  = new OcorrenciaDao();
$Ocorrencia->setCodigoDevolucao($codOcorrencia);    
$Ocorrencia->setTipoOcorrencia($tipoDevolucao);

//echo "<pre>";
//print_r($_REQUEST);
//echo "</pre>";

//echo "<pre>";
//print_r($Ocorrencia);
//echo "</pre>";
//

$retornoDados    = $OcorrenciaDao->consultaOcorrencia($Ocorrencia);
$ocorrenciaDados = oci_fetch_object($retornoDados);

$Passo = new Passo();
$PassoDao = new PassoDao();
$Passo = $PassoDao->consultarFluxo();

$OcorrenciaPassoDao = new OcorrenciaPassoDao();
?>
<div class="">
        <div class="wizard show-submit">                                
            <ul>
<?php
        foreach ($Passo as $ObjPasso) {  
            
            $OcorrenciaPasso = new OcorrenciaPasso();
            $OcorrenciaPasso->setCodigoOcorrencia($codOcorrencia);
            $OcorrenciaPasso->setCodigoPasso($ObjPasso->getCodigo());
            $OcorrenciaPasso = $OcorrenciaPassoDao->consultarOcorrenciaPasso($OcorrenciaPasso);
            if ($OcorrenciaPasso[0]) {
                $OcorrenciaPasso = $OcorrenciaPasso[0];
            } else {
                $OcorrenciaPasso = new OcorrenciaPasso();
            }
            
            if ( $OcorrenciaPasso->getCodigoOcorrencia()){
                if( $OcorrenciaPasso->getIndicadorAprovado() == 9){
                    $eventoFluxo = 'statusYellow';
                }else if( $OcorrenciaPasso->getIndicadorAprovado() == 1){
                    $eventoFluxo = 'StatusGreen';
                }else if( $OcorrenciaPasso->getIndicadorAprovado() == 2){ 
                    $eventoFluxo = 'StatusBlue';
                }else{
                    $eventoFluxo = 'StatusRed';
                }
				
				if( $OcorrenciaPasso->getIndicadorAprovado() == 0 && $ObjPasso->getCodigo()==1 && $ocorrenciaDados->DST_CO_NUMERO == 17 ){
                    $eventoFluxo = 'statusOrange';
				}
				
            }else{
                $eventoFluxo = "";
            }
            
            
?>           
                <li>
                    <a href="#step-<?php echo $ObjPasso->getSeqFluxo()?>" onclick="javascript:      $('#step-<?php echo $ObjPasso->getSeqFluxo();?>').load('modules/ocorrencia/consulta/passo/<?php echo $ObjPasso->getPaginaFluxo();?>?codOcorrencia=<?php echo $codOcorrencia;?>&codPasso=<?php echo $ObjPasso->getCodigo();?>');" class="<?php echo $eventoFluxo; ?>" disabled>
                        <span class="stepNumber"><?php echo $ObjPasso->getSeqFluxo()?></span>
                        <span class="stepDesc"><?php echo utf8_encode($ObjPasso->getDescricao()); ?></span>
                    </a>
                </li>
<?php

        if(($eventoFluxo == "StatusRed") || ($eventoFluxo == "statusYellow") ){
        ?>    
                <script>
                $("#step-<?php echo $ObjPasso->getSeqFluxo()?>").click();
                </script>
                
          <?php  
        }


        }   
?>
            </ul>
<?php
        foreach ($Passo as $ObjPasso) {              
?>         
<div id="step-<?php echo $ObjPasso->getSeqFluxo()?>">
    
</div>    
<?php
        }   
?>         
            <div id="retornoAprovacao"></div>                                                                                         
        </div>


</div>
<script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/plugins/smartwizard/jquery.smartWizard.js"></script>
<script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/plugins/smartwizard/smartwizard/jquery.smartWizard.min.js"></script>