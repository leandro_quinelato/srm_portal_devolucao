<!DOCTYPE html>
<?php
include_once( "../../../includes/Dao/DaoSistema.class.php" );
include_once( "../../../includes/Dao/OcorrenciaDao.class.php" );
include_once( "../../../includes/CalTipoOcorrencia.class.php" );
include_once( "../../../includes/Dao/TipoOcorrenciaDAO.class.php" );
include_once( "../../../includes/TipoDevolucao.class.php" );
include_once( "../../../includes/Dao/TipoDevolucaoDao.class.php" );
include_once( "../../../includes/Rota.class.php" );
include_once( "../../../includes/Dao/RotaDao.class.php" );


include_once( "../../../includes/Status.class.php" );
include_once( "../../../includes/Dao/StatusDao.class.php" );

include_once( "../../../includes/Passo.class.php" );
include_once( "../../../includes/Dao/PassoDao.class.php" );

include_once("DaoGlobal.class.php");
include_once("SistemaDao.class.php");
include_once("GlbSistema.class.php");
include_once("GlbSistemaPerfil.class.php");
include_once("GlbUsuario.class.php");
include_once("GlbColaborador.class.php");
include_once("GlbPessoa.class.php");
include_once("GlbEndereco.class.php");
include_once("GlbTelefone.class.php");
include_once("include_novo/GlbCentroDistribuicao.class.php");
include_once("include_novo/GlbTransportadora.class.php");
include_once("include_novo/GlbContatoTransportador.class.php");
include_once("include_novo/ContatoTransportadorDao.class.php");
include_once("GlbProgramaRotina.class.php");
include_once("GlbModuloPrograma.class.php");
include_once("GlbSistemaModulo.class.php");
include_once("PerfilUsuarioDao.class.php");
include_once("PerfilPermissaoDao.class.php");
include_once("integraSetSistemas.php");
$integra = new integraSetSistemas("conf.xml");
$ObjUsuario = unserialize($integra->getSistemaIntegra());
$ObjRotinas = $ObjUsuario->getObjSistemaPerfil()->getObjRotina();

//echo "<pre>";
//print_r($ObjUsuario);
//echo "</pre>";

$RotaDao = new RotaDao();
$OcorrenciaDao = new OcorrenciaDao();
/* FILTRO ROTA */
if ($ObjUsuario->getUsuarioTipo() == "VEN") {
    $VendedorUsuario = $ObjUsuario->getObjVendedor();
    $ObjVendedor = new GlbVendedor();
    if ($VendedorUsuario[0]) {
        $ObjVendedor->setCodigo($VendedorUsuario[0]->getCodigo());
        $ObjVendedor->setTipo($VendedorUsuario[0]->getTipo());
    }
    $resultRota = $RotaDao->consultarRotaVendedor($ObjVendedor);
} else if ($ObjUsuario->getUsuarioTipo() == "CLI") {

    $resultRota = $OcorrenciaDao->consultaCliente($ObjUsuario->getObjCliente()->getClienteCodigo());
    //  OCIFetchInto($resultCliente, $rowCliente, OCI_ASSOC);
} else if($ObjUsuario->getUsuarioTipo() == "TRA"){
    $resultRota  = $RotaDao->consultaUsuRota($ObjUsuario->getUsuarioCodigo());
}else{

    $resultRota = $RotaDao->listar();
}

/* Gerar codigo da ocorrencia */
//$doc_co_numero = $OcorrenciaDao->gerarCodigoOcorrencia($ObjUsuario->getUsuarioCodigo());

/* FILTRO MOTIVO DEVOLUCAO */
$TipoOcorrenciaDao = new TipoOcorrenciaDAO();
$TipoOcorrencias = new CalTipoOcorrencia();
$TipoOcorrencias = $TipoOcorrenciaDao->listarTipoOcorrencia($TipoOcorrencias);

/* FILTRO TIPO DEVOLUCAO */
$TiposDevolucao = new TipoDevolucao();
$TipoDevolucaoDao = new TipoDevolucaoDao();
$TiposDevolucao->setStatus('A');
$TiposDevolucao = $TipoDevolucaoDao->listarTipoDevolucao($TiposDevolucao);


/* FILTRO CEDIS */
$GlbCentroDistribuicao = new GlbCentroDistribuicao();
$GlbCentroDistribuicao = $OcorrenciaDao->filtroCentroDis();



/* FILTRO STATUS OCORRENCIA */
$Status = new Status();
$StatusDao = new StatusDao();
$Status = $StatusDao->consultar();

/* FILTRO PASSO OCORRENCIA */
$Passo = new Passo();
$PassoDao = new PassoDao();
$Passo = $PassoDao->consultar($Passo);
?>
<html lang="en">
    <head>        
        <!-- meta section -->
        <title>Devolução - Consulta Protocolo</title>
    <?php include("../../../library/head.php"); ?>
        <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/libConsultaOcorrencia.js"></script>

    </head>
    <body>
        <!-- set loading layer -->
        <div class="dev-page-loading preloader"></div>
        <!-- ./set loading layer -->

        <!-- page wrapper -->
        <div class="dev-page">

            <!-- page header -->    
<?php include("../../../library/topo.php"); ?>
            <!-- ./page header -->

            <!-- page container -->
            <div class="dev-page-container">

                <!-- page sidebar -->
            <?php include("../../../library/menu.php"); ?>
                <!-- ./page sidebar -->

                <!-- page content -->
                <div class="dev-page-content">                    
                    <!-- page content container -->
                    <div class="container">

                        <!-- page title -->
                        <div class="page-title">
                            <h1>Consulta Protocolo</h1>

                            <ul class="breadcrumb">
                                <li><a href="#">Devolução</a></li>
                                <li>Consulta Protocolo</li>
                            </ul>
                        </div>                        
                        <!-- ./page title -->



                        <!-- datatables plugin -->
                        <div class="wrapper wrapper-white">
                            <form id="formFiltroConsulta">
                                <div class="row">
                                    <div class="col-md-2 col-sm-3 col-xs-3">
                                        <div class="form-group">
                                            <label>Protocolo</label>
                                            <input type="text" placeholder="Protocolo" class="form-control" onKeyPress="fMascara('numero', event, this);" name="conCodOcorrencia" id="conCodOcorrencia" onkeydown="testeBarra(event);">
                                        </div>
                                    </div>

                                    <div class="col-md-2 col-sm-3 col-xs-3">
                                        <div class="form-group">
                                            <label>Início</label>                            
                                            <input type="text" class="form-control datepicker" onKeyPress="fMascara('mesano', event, this)" name="dataInicial" id="dataInicial">
                                        </div> 
                                    </div>                               
                                    <div class="col-md-2 col-sm-3 col-xs-3">
                                        <div class="form-group">
                                            <label>Fim</label>                            
                                            <input type="text" class="form-control datepicker" onKeyPress="fMascara('mesano', event, this)" name="dataFinal" id="dataFinal">
                                        </div> 
                                    </div>

<?php
if ($ObjUsuario->getUsuarioTipo() == "CLI") {
    $ObjUsuario->getObjCliente()->getClienteCodigo();
?>                       
                                        <div class="col-md-2 col-sm-3 col-xs-3">
                                            <div class="form-group">
                                                <label>Cód. Cliente</label>
                                                <input type="text" placeholder="Cliente" class="form-control" onkeypress="fMascara('numero', event, this)" name="conCodCliente" id="conCodCliente" value="<?php echo $ObjUsuario->getObjCliente()->getClienteCodigo(); ?>" readonly>
                                            </div>
                                        </div>                                                      
<?php
} else {
?>

                                        <div class="col-md-2 col-sm-3 col-xs-3">
                                            <div class="form-group">
                                                <label>Cód. Cliente</label>
                                                <input type="text" placeholder="Cliente" class="form-control" onkeypress="fMascara('numero', event, this)" name="conCodCliente" id="conCodCliente">
                                            </div>
                                        </div>  
<?php
}
?>


                                </div>  
                                <div class="row">

                                    <div class="col-md-3 col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <label>Nota devolução</label><br>
                                            <div class="col-md-4 col-sm-4 col-xs-4">
                                                <input type="text" placeholder="Série" class="form-control" onKeyPress="fMascara('numero', event, this)" name="conSerieDev" id="conSerieDev">
                                            </div>
                                            <div class="col-md-8 col-sm-8 col-xs-8">
                                                <input type="text" placeholder="Nota" class="form-control" onKeyPress="fMascara('numero', event, this)" name="conNotaDev" id="conNotaDev">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <label>Nota de venda</label><br>
                                            <div class="col-md-4 col-sm-4 col-xs-4">
                                                <input type="text" placeholder="Série" class="form-control" onKeyPress="fMascara('numero', event, this)" name="conSerie" id="conSerie" >
                                            </div>
                                            <div class="col-md-8 col-sm-8 col-xs-8">
                                                <input type="text" placeholder="Nota" class="form-control" onKeyPress="fMascara('numero', event, this)" name="conNota" id="conNota">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-6">                        
                                        <div class="form-group">
                                            <label>Rede</label>
                                            <input type="text" placeholder="Rede" class="form-control" onKeyPress="fMascara('numero', event, this)" name="conRede" id="conRede">
                                        </div>                        
                                    </div>  
                                    <div class="col-md-3 col-sm-6 col-xs-6">                        
                                        <div class="form-group">
                                            <label>CD</label>
                                            <select class="form-control selectpicker" id="ced_co_numero" name="ced_co_numero">
                                                <option value="TOD">Todos</option>
<?php
foreach ($GlbCentroDistribuicao as $ObjCedis) {
    ?>
                                                    <option value="<?php echo $ObjCedis->getCodigo(); ?>"><?php echo $ObjCedis->getNome(); ?></option>
                                                    <?php
                                                }
                                                ?>    
                                            </select>
                                        </div>                        
                                    </div>  
                                    <div class="col-md-2 col-sm-6 col-xs-6">                        
                                        <div class="form-group">
                                            <label>Rota</label>
                                            <select class="form-control selectpicker" id="rot_co_numero" name="rot_co_numero">
                                                <option value="TOD">Todos</option>
<?php
while (OCIFetchInto($resultRota, $rowRota, OCI_ASSOC)) {
    ?>
                                                    <option value="<?php echo $rowRota['ROT_CO_NUMERO']; ?>"  <?php
                                                    if ($rowCliente['ROT_CO_NUMERO'] == $rowRota['ROT_CO_NUMERO']) {
                                                        echo "selected='selected'";
                                                    }
                                                    ?>>
                                                                <?php echo $rowRota['ROT_CO_NUMERO']; ?> 
                                                    </option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>                        
                                    </div> 
                                    <div class="col-md-3 col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <label>Valor devolução </label><br>
                                            <div class="col-md-6 col-sm-4 col-xs-4">
                                                <input type="text" placeholder="Valor" class="form-control" name="valorDevInicial" id="valorDevInicial" onkeyup="$(this).priceFormat({prefix: '', centsSeparator: ',', thousandsSeparator: '.', allowNegative: true});" >
                                            </div>
                                            <div class="col-md-6 col-sm-8 col-xs-8">
                                                <input type="text" placeholder="Valor" class="form-control" name="valorDevFinal" id="valorDevFinal" onkeyup="$(this).priceFormat({prefix: '', centsSeparator: ',', thousandsSeparator: '.', allowNegative: true});" >
                                            </div>
                                        </div>
                                    </div>   
                                    <div class="col-md-3 col-sm-6 col-xs-6">                        
                                        <div class="form-group">
                                            <label>Status</label>
                                            <select class="form-control selectpicker" id="statusOcorrencia" name="statusOcorrencia">
                                                <option value="TOD">Todos</option>
                                                <?php
                                                foreach ($Status as $ObjStatus) {
                                                    ?>
                                                    <option value="<?php echo $ObjStatus->getCodigo(); ?>"><?php echo utf8_encode($ObjStatus->getDescricao()); ?></option>
                                                    <?php
                                                }
                                                ?>  
                                            </select>
                                        </div>                        
                                    </div>  
                                    <div class="col-md-3 col-sm-6 col-xs-6">                        
                                        <div class="form-group">
                                            <label>Passo</label>
                                            <select class="form-control selectpicker" id="passoOcorrencia" name="passoOcorrencia">
                                                <option value="TOD">Todos</option>
                                                <?php
                                                foreach ($Passo as $ObjPasso) {
                                                    ?>
                                                    <option value="<?php echo $ObjPasso->getCodigo(); ?>"><?php echo utf8_encode($ObjPasso->getDescricao()); ?></option>
                                                    <?php
                                                }
                                                ?>  
                                            </select>
                                        </div>                        
                                    </div> 
                                </div>
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label>Tipo devolução</label>
                                            <br>
                                            <div class="radio radio-inline">
                                                <input type="radio" name="tipoDevolucao" id="tipoDevolucaoT" value="T" />
                                                <label for="tipoDevolucaoT">TODOS</label>
                                            </div>
                                            <?php
                                            foreach ($TiposDevolucao as $tipoDevolucao) {
                                                ?>
                                                <div class="radio radio-inline">
                                                    <input type="radio" name="tipoDevolucao" id="tipoDevolucao<?php echo $tipoDevolucao->getCodigo(); ?>" value="<?php echo $tipoDevolucao->getCodigo(); ?>" />
                                                    <label for="tipoDevolucao<?php echo $tipoDevolucao->getCodigo(); ?>"><?php echo utf8_encode($tipoDevolucao->getDescricao()); ?></label>
                                                </div>
                                                <?php
                                            }
                                            ?>

                                        </div>                       
                                    </div>                            
                                </div>
                            </form>    
                            <div class="row">
                                <div class="col-md-2">
                                    <button class="btn btn-primary" onclick="javascript: consultarOcorrencia();" id="botaoConsultar">Consultar</button>
                                </div>                                                                                  
                            </div>
                            <div class="form-group">

                            </div>
                            <div id="retornoConsOcorrencia">


                            </div>                        
                            <!-- ./datatables plugin -->


                            <!-- Copyright -->
                            <?php include("../../../library/copyright.php"); ?>
                            <!-- ./Copyright -->

                        </div>
                        <!-- ./page content container -->                                       
                    </div>
                    <!-- ./page content -->                                                
                </div>  
                <!-- ./page container -->


                <!-- page footer -->    
                <?php include("../../../library/footer.php"); ?>
                <!-- ./page footer -->

                <!-- page search -->

                <!-- page search -->            
            </div>
            <!-- ./page wrapper -->


            <!--modal email coleta-->
            <div class="modal fade" id="modal_mailcoleta" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

            </div>   
            <!--fim modal email coleta-->
            <div class="modal fade" id="modal_ocorrencia" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Protocolo: 1600260047010</h4>
                        </div>
                        <div class="col-md-12 TopModalOcorrrencia">
                            <div class="row">
                                <div class="col-md-3 col-sm-4 col-xs-4">
                                    <div class="form-group"> 
                                        <label>Cliente</label>
                                        <p>121221 - Unimed Paulista LTDA</p>
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-4 col-xs-4">
                                    <div class="form-group"> 
                                        <label>Data cadastro</label>
                                        <p>20/02/2016</p>
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-4 col-xs-4">
                                    <div class="form-group">
                                        <label>Área Cadastrante</label>
                                        <p>Central de Soluções</p>
                                    </div>    
                                </div> 
                                <div class="col-md-3 col-sm-4 col-xs-4">
                                    <div class="form-group">
                                        <label>Status</label>
                                        <p>Finalizado e crédito liberado</p>
                                    </div>    
                                </div>                                    
                                <div class="col-md-2 col-sm-4 col-xs-4">
                                    <div class="form-group">
                                        <label>Nota devolução</label>
                                        <p>2121 / 321131321-15/02/16</p>
                                    </div>    
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="tabs">                            
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li class="active"><a href="#tab-det" role="tab" data-toggle="tab">Detalhes</a></li>
                                        <li><a href="#tab-anexo" role="tab" data-toggle="tab">Anexo</a></li>
                                        <li><a href="#tab-fluxo" role="tab" data-toggle="tab">Fluxo</a></li>
                                        <li><a href="#tab-historico" role="tab" data-toggle="tab">Histórico</a></li>
                                    </ul>                         
                                    <div class="panel-body tab-content">
                                        <div class="tab-pane active" id="tab-det">
                                            <div class="row">                                    
                                                <div class="col-md-3 col-sm-6 col-xs-6">
                                                    <div class="form-group">
                                                        <label>Motivo</label>
                                                        <select class="form-control selectpicker">
                                                            <option value="1">Não pediu</option>
                                                            <option value="2">Option 2</option>
                                                        </select>
                                                    </div>    
                                                </div>
                                                <div class="col-md-6 col-sm-6 col-xs-6">
                                                    <div class="form-group">
                                                        <label>Detalhes</label>
                                                        <textarea placeholder="Descrição" rows="1" class="form-control"></textarea>
                                                    </div>
                                                </div>                                            
                                            </div>   

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="page-subtitle">
                                                        <h3>NFs Venda</h3>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="tabs">                            
                                                        <ul class="nav nav-tabs nav-tabs-arrowed TabNFTit" role="tablist">
                                                            <li class="active"><a href="#tab1-NF01" role="tab" data-toggle="tab">NF 1501254</a></li>
                                                            <li><a href="#tab1-NF02" role="tab" data-toggle="tab">NF 545664</a></li>
                                                        </ul>                         
                                                        <div class="panel-body tab-content TabNF">
                                                            <div class="tab-pane active" id="tab1-NF01">
                                                                <div class="row">
                                                                    <div class="col-md-2 col-sm-4 col-xs-4">
                                                                        <div class="form-group"> 
                                                                            <label>NF</label>
                                                                            <p>15235 / 1501254</p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-2 col-sm-4 col-xs-4">
                                                                        <div class="form-group"> 
                                                                            <label>Emissão</label>
                                                                            <p>20/02/2016</p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-2 col-sm-4 col-xs-4">
                                                                        <div class="form-group">
                                                                            <label>Tipo</label>
                                                                            <p>Integral pós-recebimento</p>
                                                                        </div>    
                                                                    </div> 
                                                                    <div class="col-md-2 col-sm-4 col-xs-4">
                                                                        <div class="form-group">
                                                                            <label>CD</label>
                                                                            <p>CD Matriz</p>
                                                                        </div>    
                                                                    </div>                                    
                                                                    <div class="col-md-2 col-sm-4 col-xs-4">
                                                                        <div class="form-group">
                                                                            <label>Divisão</label>
                                                                            <p>Alimentar</p>
                                                                        </div>    
                                                                    </div>
                                                                    <div class="col-md-2 col-sm-4 col-xs-4">
                                                                        <div class="form-group">
                                                                            <label>Rota</label>
                                                                            <p>A3</p>
                                                                        </div>    
                                                                    </div>

                                                                </div>                                                            
                                                                <div class="table-responsive">
                                                                    <table class="table table-bordered table-striped table-hover TableNF">
                                                                        <thead>
                                                                            <tr>
                                                                                <th>
                                                                        <div class="checkbox checkbox-inline">
                                                                            <input type="checkbox" id="check_1">
                                                                            <label for="check_1"></label>
                                                                        </div>
                                                                        </th>
                                                                        <th>Cód</th>
                                                                        <th>Nome produto</th>
                                                                        <th>Lote</th>
                                                                        <th>Quant</th>
                                                                        <th>Quant a devolver</th>
                                                                        <th>Validade</th>
                                                                        <th>Preço</th>
                                                                        <th>Total bruto</th>
                                                                        <th>Valor desc</th>
                                                                        <th>Valor imp</th>
                                                                        <th>Total liq</th>
                                                                        </tr>

                                                                        </thead>                               
                                                                        <tbody>
                                                                            <tr>
                                                                                <td>
                                                                                    <div class="checkbox checkbox-inline">
                                                                                        <input type="checkbox" id="check_2">
                                                                                        <label for="check_2"></label>
                                                                                    </div>
                                                                                </td>
                                                                                <td>5464656</td>
                                                                                <td>Pantoprazol 20mg COMR BLX28</td>
                                                                                <td>464656464</td>
                                                                                <td>152</td>
                                                                                <td>
                                                                                    <div class="form-group">
                                                                                        <input type="text" class="form-control" placeholder="Devolver">
                                                                                    </div>
                                                                                </td>
                                                                                <td>25/02/17</td>
                                                                                <td>258,33</td>
                                                                                <td>260,98</td>
                                                                                <td>1,54</td>
                                                                                <td>5,58</td>
                                                                                <td>250,36</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <div class="checkbox checkbox-inline">
                                                                                        <input type="checkbox" id="check_3">
                                                                                        <label for="check_3"></label>
                                                                                    </div>
                                                                                </td>
                                                                                <td>5464656</td>
                                                                                <td>Pantoprazol 20mg COMR BLX28</td>
                                                                                <td>464656464</td>
                                                                                <td>152</td>
                                                                                <td>
                                                                                    <div class="form-group">
                                                                                        <input type="text" class="form-control" placeholder="Devolver">
                                                                                    </div>
                                                                                </td>

                                                                                <td>25/02/17</td>
                                                                                <td>258,33</td>
                                                                                <td>260,98</td>
                                                                                <td>1,54</td>
                                                                                <td>5,58</td>
                                                                                <td>250,36</td>
                                                                            </tr>  
                                                                            <tr>
                                                                                <td>
                                                                                    <div class="checkbox checkbox-inline">
                                                                                        <input type="checkbox" id="check_4">
                                                                                        <label for="check_4"></label>
                                                                                    </div>
                                                                                </td>
                                                                                <td>5464656</td>
                                                                                <td>Pantoprazol 20mg COMR BLX28</td>
                                                                                <td>464656464</td>
                                                                                <td>152</td>
                                                                                <td>
                                                                                    <div class="form-group">
                                                                                        <input type="text" class="form-control" placeholder="Devolver">
                                                                                    </div>
                                                                                </td>
                                                                                <td>25/02/17</td>
                                                                                <td>258,33</td>
                                                                                <td>260,98</td>
                                                                                <td>1,54</td>
                                                                                <td>5,58</td>
                                                                                <td>250,36</td>
                                                                            </tr>                                        

                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                            <div class="tab-pane active" id="tab1-NF02">
                                                                <div class="row">
                                                                    <div class="col-md-2 col-sm-4 col-xs-4">
                                                                        <div class="form-group"> 
                                                                            <label>NF</label>
                                                                            <p>0000 / 545664</p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-2 col-sm-4 col-xs-4">
                                                                        <div class="form-group"> 
                                                                            <label>Emissão</label>
                                                                            <p>20/02/2016</p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-2 col-sm-4 col-xs-4">
                                                                        <div class="form-group">
                                                                            <label>Tipo</label>
                                                                            <p>Integral pós-recebimento</p>
                                                                        </div>    
                                                                    </div> 
                                                                    <div class="col-md-2 col-sm-4 col-xs-4">
                                                                        <div class="form-group">
                                                                            <label>CD</label>
                                                                            <p>CD Matriz</p>
                                                                        </div>    
                                                                    </div>                                    
                                                                    <div class="col-md-2 col-sm-4 col-xs-4">
                                                                        <div class="form-group">
                                                                            <label>Divisão</label>
                                                                            <p>Alimentar</p>
                                                                        </div>    
                                                                    </div>
                                                                    <div class="col-md-2 col-sm-4 col-xs-4">
                                                                        <div class="form-group">
                                                                            <label>Rota</label>
                                                                            <p>A3</p>
                                                                        </div>    
                                                                    </div>

                                                                </div>                                                            
                                                                <div class="table-responsive">
                                                                    <table class="table table-bordered table-striped table-hover TableNF">
                                                                        <thead>
                                                                            <tr>
                                                                                <th>
                                                                        <div class="checkbox checkbox-inline">
                                                                            <input type="checkbox" id="check_1">
                                                                            <label for="check_1"></label>
                                                                        </div>
                                                                        </th>
                                                                        <th>Cód</th>
                                                                        <th>Nome produto</th>
                                                                        <th>Lote</th>
                                                                        <th>Quant</th>
                                                                        <th>Quant a devolver</th>
                                                                        <th>Validade</th>
                                                                        <th>Preço</th>
                                                                        <th>Total bruto</th>
                                                                        <th>Valor desc</th>
                                                                        <th>Valor imp</th>
                                                                        <th>Total liq</th>
                                                                        </tr>

                                                                        </thead>                               
                                                                        <tbody>
                                                                            <tr>
                                                                                <td>
                                                                                    <div class="checkbox checkbox-inline">
                                                                                        <input type="checkbox" id="check_2">
                                                                                        <label for="check_2"></label>
                                                                                    </div>
                                                                                </td>
                                                                                <td>5464656</td>
                                                                                <td>Pantoprazol 20mg COMR BLX28</td>
                                                                                <td>464656464</td>
                                                                                <td>152</td>
                                                                                <td>
                                                                                    <div class="form-group">
                                                                                        <input type="text" class="form-control" placeholder="Devolver">
                                                                                    </div>
                                                                                </td>
                                                                                <td>25/02/17</td>
                                                                                <td>258,33</td>
                                                                                <td>260,98</td>
                                                                                <td>1,54</td>
                                                                                <td>5,58</td>
                                                                                <td>250,36</td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                            <div class="tab-pane" id="tab1-third">
                                                                <p>Vestibulum cursus augue sed leo tempor, at aliquam orci dictum. Sed mattis metus id velit aliquet, et interdum nulla porta. Etiam euismod pellentesque purus, in fermentum eros venenatis ut. Praesent vitae nibh ac augue gravida lacinia non a ipsum. Aenean vestibulum eu turpis eu posuere. Sed eget lacus lacinia, mollis urna et, interdum dui. Donec sed diam ut metus imperdiet malesuada. Maecenas tincidunt ultricies ipsum, lobortis pretium dolor sodales ut. Donec nec fringilla nulla. In mattis sapien lorem, nec tincidunt elit scelerisque tempus.</p>
                                                            </div>
                                                            <div class="tab-pane" id="tab1-fourth">
                                                                <p>In mattis sapien lorem, nec tincidunt elit scelerisque tempus. Quisque nisl nisl, venenatis eget dignissim et, adipiscing eu tellus. Sed nulla massa, luctus id orci sed, elementum consequat est. Proin dictum odio quis diam gravida facilisis. Sed pharetra dolor a tempor tristique. Sed semper sed urna ac dignissim. Aenean fermentum leo at posuere mattis. Etiam vitae quam in magna viverra dictum. Curabitur feugiat ligula in dui luctus, sed aliquet neque posuere.</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>                                                                                   
                                            </div> 


                                        </div>
                                        <div class="tab-pane active" id="tab-anexo">
                                            <div class="row">                                    
                                                <div class="col-md-3 col-sm-4 col-xs-6">
                                                    <div class="form-group">
                                                        <label>Tipo documento</label>
                                                        <select class="form-control selectpicker">
                                                            <option value="1">Nota fiscal devolução</option>
                                                            <option value="2">Option 2</option>
                                                        </select>
                                                    </div>    
                                                </div>
                                                <div class="col-md-4 col-sm-6 col-xs-6">
                                                    <div class="form-group">
                                                        <label>Descrição</label>
                                                        <input type="text" class="form-control" placeholder="Protocolo">
                                                    </div>
                                                </div>                                            

                                            </div>  
                                            <div class="row">
                                                <div class="col-md-4 col-sm-6 col-xs-6">
                                                    <div class="form-group">
                                                        <label>Arquivo</label>
                                                        <input name="" type="file">
                                                        <input type="text" class="tags" value="nomedoarquivo.pdf,teste.xls,arquivo1.jpg"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <button class="btn btn-primary">Adicionar</button>
                                                    </div>
                                                </div>                                                                                  
                                            </div> 
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="table-responsive">
                                                        <table class="table table-bordered table-striped table-hover TableNF">
                                                            <thead>
                                                                <tr>
                                                                    <th>Tipo de documento</th>
                                                                    <th>Descrição</th>
                                                                    <th>Nome arquivo</th>
                                                                    <th>Usuário</th>
                                                                    <th></th>
                                                                </tr>

                                                            </thead>                               
                                                            <tbody>
                                                                <tr>
                                                                    <td>Nota fiscal devolução</td>
                                                                    <td>NFD</td>
                                                                    <td>nomedoarquivo.pdf</td>
                                                                    <td>alisson.info@servimed.com.br</td>
                                                                    <td><a href="#" class="BotExcluir">X</a></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Nota fiscal devolução</td>
                                                                    <td>NFD</td>
                                                                    <td>nomedoarquivo.pdf</td>
                                                                    <td>alisson.info@servimed.com.br</td>
                                                                    <td><a href="#" class="BotExcluir">X</a></td>
                                                                </tr>                                                                                                    

                                                            </tbody>
                                                        </table>
                                                    </div>                                                
                                                </div>                                                                                   
                                            </div> 
                                        </div>                                    

                                        <div class="tab-pane" id="tab-fluxo">
                                            <div class="modal-body">
                                                <form action="javascript:alert('Submited!');" role="form">
                                                    <div class="wizard show-submit">                                
                                                        <ul>
                                                            <li>
                                                                <a href="#step-1">
                                                                    <span class="stepNumber">1</span>
                                                                    <span class="stepDesc">Gerente</span>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#step-2">
                                                                    <span class="stepNumber">2</span>
                                                                    <span class="stepDesc">Pólo Entrada/Saída</span>
                                                                </a>
                                                            </li> 
                                                            <li>
                                                                <a href="#step-3">
                                                                    <span class="stepNumber">3</span>
                                                                    <span class="stepDesc">Devolução</span>
                                                                </a>
                                                            </li> 
                                                            <li>
                                                                <a href="#step-4">
                                                                    <span class="stepNumber">4</span>
                                                                    <span class="stepDesc">Fiscal</span>
                                                                </a>
                                                            </li> 

                                                        </ul>

                                                        <div id="step-1">                                            
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                                        <span>Entrada:</span>
                                                                        <strong>15/02/16</strong>
                                                                    </div>
                                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                                        <span>Saída:</span>
                                                                        <strong>20/02/16</strong>
                                                                    </div>
                                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                                        <span>Status:</span>
                                                                        <strong>Aprovado</strong>
                                                                    </div> 
                                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                                        <span>E-mail coleta:</span>
                                                                        <strong>polocampinas@servimed.com.br</strong>
                                                                    </div> 
                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <label>Descrição</label>
                                                                        <textarea class="form-control" rows="3" placeholder="Descrição"></textarea>
                                                                    </div>                                   
                                                                </div>                                       
                                                            </div>
                                                        </div>
                                                        <div id="step-2">                                            
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                                        <span>Entrada:</span>
                                                                        <strong>15/02/16</strong>
                                                                    </div>
                                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                                        <span>Saída:</span>
                                                                        <strong>20/02/16</strong>
                                                                    </div>
                                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                                        <span>Status:</span>
                                                                        <strong>Finalizado</strong>
                                                                    </div> 
                                                                    <div class="col-md-3 col-sm-5 col-xs-12">
                                                                        <label>Volume Recebido</label>
                                                                        <input type="text" placeholder="Quant" class="form-control">
                                                                    </div>

                                                                </div>                                       
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                    <label>Descrição</label>
                                                                    <textarea class="form-control" rows="3" placeholder="Descrição"></textarea>
                                                                </div>  
                                                            </div>                                                        
                                                        </div>   
                                                        <div id="step-3">                                            
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                                        <span>Entrada:</span>
                                                                        <strong>15/02/16</strong>
                                                                    </div>
                                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                                        <span>Saída:</span>
                                                                        <strong>20/02/16</strong>
                                                                    </div>
                                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                                        <span>Status:</span>
                                                                        <strong>Finalizado</strong>
                                                                    </div> 
                                                                    <div class="col-md-2 col-sm-3 col-xs-12">
                                                                        <label>Volume Recebido</label>
                                                                        <input type="text" placeholder="Quant" class="form-control">
                                                                    </div>
                                                                    <div class="col-md-2 col-sm-3 col-xs-5">
                                                                        <label>Data recebimento</label>
                                                                        <input type="text" class="form-control datepicker">
                                                                    </div>
                                                                    <div class="col-md-2 col-sm-3 col-xs-5">
                                                                        <label>Data conferência</label>
                                                                        <input type="text" class="form-control datepicker">
                                                                    </div> 
                                                                    <div class="col-md-6 col-sm-12 col-xs-12">
                                                                        <label>Conferente</label>
                                                                        <select class="form-control selectpicker">
                                                                            <option value="1">ANA CLAUDIA DE JESUS RODRIGUES</option>
                                                                            <option value="2">ANDERSON SOBRAL AGOSTINHO</option>
                                                                        </select>
                                                                    </div> 
                                                                </div>                                       
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                    <label>Descrição</label>
                                                                    <textarea class="form-control" rows="3" placeholder="Descrição"></textarea>
                                                                </div>                                 
                                                            </div>
                                                        </div> 
                                                        <div id="step-4">                                            
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                                        <span>Entrada:</span>
                                                                        <strong>15/02/16</strong>
                                                                    </div>
                                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                                        <span>Fim:</span>
                                                                        <strong>20/02/16</strong>
                                                                    </div>
                                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                                        <span>Status:</span>
                                                                        <strong>Finalizado</strong>
                                                                    </div> 
                                                                </div>                                       
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                    <label>Descrição</label>
                                                                    <textarea class="form-control" rows="3" placeholder="Descrição"></textarea>
                                                                </div>
                                                            </div>                                    
                                                        </div>                                                    
                                                    </div>
                                                </form>

                                            </div>                           
                                        </div>
                                        <div class="tab-pane active" id="tab-historico">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="table-responsive">
                                                        <table class="table table-bordered table-striped table-hover TableNF">
                                                            <thead>
                                                                <tr>
                                                                    <th>Data</th>
                                                                    <th>Ação</th>
                                                                    <th>Responsável</th>
                                                                    <th>Área</th>
                                                                    <th>Justificativa</th>
                                                                </tr>

                                                            </thead>                               
                                                            <tbody>
                                                                <tr>
                                                                    <td>12/02/16-08:32</td>
                                                                    <td>Protocolo registrado</td>
                                                                    <td>João da Silva</td>
                                                                    <td>Televendas</td>
                                                                    <td>Motivo da reprova</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>12/02/16-08:32</td>
                                                                    <td>Protocolo registrado</td>
                                                                    <td>João da Silva</td>
                                                                    <td>Televendas</td>
                                                                    <td>Motivo da reprova</td>
                                                                </tr>                                                                
                                                            </tbody>
                                                        </table>
                                                    </div>                                                
                                                </div>                                                                                   
                                            </div> 


                                        </div>                                    
                                    </div>
                                </div>                        

                            </div>                    
                        </div>                    
                        <div class="modal-footer">
                            <button data-dismiss="modal" class="btn btn-default" type="button">Cancelar</button>
                            <button class="btn btn-warning" type="button">Salvar</button>
                        </div>                                                    
                    </div>
                </div>
            </div>        
            <!-- javascripts -->
            <?php include("../../../library/rodape.php"); ?>

            <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js"></script>
            <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/plugins/datatables/jquery.dataTables.min.js"></script>
            <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/plugins/smartwizard/jquery.smartWizard.js"></script>
            <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/plugins/sortable/sortable.min.js"></script>


            <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/plugins/spectrum/spectrum.js"></script>
            <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/plugins/tags-input/jquery.tagsinput.min.js"></script>                

            <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/dev-settings.js"></script>
            <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/demo.js"></script>
        <script type="text/javascript">
            
            function testeBarra(e){
//                    console.log('which: ' + e.which);
//                    console.log('keycode: ' + e.keyCode);
//                    e.preventDefault();
                if ( (e.which === 77) || (e.keyCode === 77) || (e.which === 13) || (e.keyCode === 13)) {
//                    console.log('ALOOOOOOOOOOOOOOOO');
                    $('#botaoConsultar').click();

                }
            }
           
           
            $(function(){
                  $("#conCodOcorrencia").focus();
            });
        </script>
            <!-- ./javascripts -->



    </body>
</html>