<?php
session_start();
ini_set('display_errors', 1);
include_once( "../../../includes/Dao/DaoSistema.class.php" );
include_once( "../../../includes/CalDepartamento.class.php" );
include_once( "../../../includes/CalTipoOcorrencia.class.php" );
include_once( "../../../includes/Dao/TipoOcorrenciaDAO.class.php" );
include_once( "../../../includes/Ocorrencia.class.php" );
include_once( "../../../includes/Dao/OcorrenciaDao.class.php" );
include_once( "../../../includes/TipoDevolucao.class.php" );
include_once( "../../../includes/Dao/TipoDevolucaoDao.class.php" );

include_once( "../../../includes/DevNotaOrigemItem.class.php" );
include_once( "../../../includes/Dao/DevNotaOrigemItemDao.class.php" );

include_once( "../../../includes/DevParametros.class.php" );
include_once( "../../../includes/Dao/ParametroDao.class.php" );

include_once( "../../../includes/Rota.class.php" );
include_once( "../../../includes/Dao/RotaDao.class.php" );

include_once( "../../../includes/OcorrenciaPasso.class.php" );
include_once( "../../../includes/Dao/OcorrenciaPassoDao.class.php" );

include_once( "../../../includes/OcorrenciaStatus.class.php" );
include_once( "../../../includes/Dao/OcorrenciaStatusDao.class.php" );

include_once( "../../../includes/OcorrenciaArquivos.class.php" );
include_once( "../../../includes/Dao/OcorrenciaArquivosDao.class.php" );

include_once( "../../utilitario/email/emailPortalDevolucao.class.php" );
include_once( "../../utilitario/espelhoNotaDevolucao/espelhoNotaDevolucaoSobraMain.php" );

include_once( "../../../includes/Dao/ClienteEspecialNotaDao.class.php" );

include_once( "../../../includes/DevParametrosValorDevolucao.class.php" );
include_once( "../../../includes/Dao/ParametroValorDevolucaoDao.class.php" );

include_once("DaoGlobal.class.php");
include_once("SistemaDao.class.php");
include_once("GlbSistema.class.php");
include_once("GlbSistemaPerfil.class.php");
include_once("GlbUsuario.class.php");
include_once("GlbColaborador.class.php");
include_once("GlbPessoa.class.php");
include_once("GlbEndereco.class.php");
include_once("GlbTelefone.class.php");
include_once("GlbProgramaRotina.class.php");
include_once("GlbModuloPrograma.class.php");
include_once("GlbSistemaModulo.class.php");
include_once("PerfilUsuarioDao.class.php");
include_once("PerfilPermissaoDao.class.php");
include_once("integraSetSistemas.php");
include_once("class.phpmailer.php");
include_once("class.smtp.php");

include_once("include_novo/Tradutor.class.php");

$Tradutor = new Tradutor();
$integra = new integraSetSistemas("conf.xml");
$ObjUsuario = unserialize($integra->getSistemaIntegra());
//$ObjRotinas = $ObjUsuario->getObjSistemaPerfil()->getObjRotina();

$Ocorrencia = new Ocorrencia();
$DevNotaOrigem = new DevNotaOrigem();
$OcorrenciaDao = new OcorrenciaDao();

extract($_REQUEST);
/*
 * verificar se a ocorrencia é integral ou parcial
 */
if ($acao == "ADDNOTA") {
    /*
     * Preencher dados comuns(parcial/integral) da devolucao
     */
    $DevNotaOrigemItemDao = new DevNotaOrigemItemDao();

    $Ocorrencia->setCodigoDevolucao($codOcorrencia);
    $Ocorrencia->getObjRota()->setCodigoRota($rot_co_numero);
    $Ocorrencia->getTipoOcorrencia()->setCodTipoOcorrencia(80);
    $Ocorrencia->setTipoTipoDevolucao(5);
    $Ocorrencia->getGlbCliente()->setClienteCodigo(trim($cli_co_numero));
    $Ocorrencia->getTipoOcorrencia()->setDepartamentoTipoOcorrencia(29);
    $Ocorrencia->setCodigoCadastranteDevolucao($ObjUsuario->getUsuarioCodigo());
    $Ocorrencia->setTipoCadastranteDevolucao($ObjUsuario->getUsuarioTipo());

    if ($ObjUsuario->getUsuarioTipo() == "CLI") {
        $Ocorrencia->setDeptContatoClienteDevolucao(strtoupper("CLIENTE"));
        $Ocorrencia->setEmailCadDevolucao($ObjUsuario->getUsuarioUsername());
    } else if ($ObjUsuario->getUsuarioTipo() == "INT") {        
        if($ObjUsuario->getObjSistemaPerfil()->getNomeSistemaPerfil() == 'CentralSolucao'){
            $Ocorrencia->setTipoCadastranteDevolucao('CSL');
            $Ocorrencia->setDeptContatoClienteDevolucao(strtoupper("CENTRAL SOLUCOES"));       
        }else{
            $Ocorrencia->setDeptContatoClienteDevolucao(strtoupper("DEVOLUCAO"));
        }
        $Ocorrencia->setEmailCadDevolucao($ObjUsuario->getObjColaborador()->getPessoaEmail());
    } else if ($ObjUsuario->getUsuarioTipo() == "VEN") {        
        if($ObjUsuario->getObjSistemaPerfil()->getNomeSistemaPerfil() == 'Gerente'){
            $Ocorrencia->setTipoCadastranteDevolucao('GER');
            $Ocorrencia->setDeptContatoClienteDevolucao(strtoupper("GERENTE"));       
        }else if($ObjUsuario->getObjSistemaPerfil()->getNomeSistemaPerfil() == 'Representante'){
            $Ocorrencia->setTipoCadastranteDevolucao('REP');
            $Ocorrencia->setDeptContatoClienteDevolucao(strtoupper("REPRESENTANTE"));       
        }else if($ObjUsuario->getObjSistemaPerfil()->getNomeSistemaPerfil() == 'Consultor'){
            $Ocorrencia->setTipoCadastranteDevolucao('CON');
            $Ocorrencia->setDeptContatoClienteDevolucao(strtoupper("CONSULTOR"));       
        }                       
        $Ocorrencia->setEmailCadDevolucao($ObjUsuario->getUsuarioUsername());
    } else if ($ObjUsuario->getUsuarioTipo() == "TRA") { //ajustarrr
        $Ocorrencia->setDeptContatoClienteDevolucao(strtoupper("TRANSPORTADORA"));  // AMARAL   
        $Ocorrencia->setEmailCadDevolucao($ObjUsuario->getUsuarioUsername()); //AMARAL
    }

    //$Ocorrencia->setDescricaoDevolucao(trim($doc_tx_descricao));

    //INFORMAR QUE O STATUS NESSE PONTO É NÃO: CADASTRO NAO FINALIZADO (14)
    $Ocorrencia->setStatusOcorrencia(14);
    /* Registrar o status na tabela de Ocorrencia Status */
    $OcorrenciaStatus = new OcorrenciaStatus();
    $OcorrenciaStatus->setCodigoOcorrencia($Ocorrencia->getCodigoDevolucao());
    $OcorrenciaStatus->setUsuario($ObjUsuario->getUsuarioCodigo());
    $OcorrenciaStatus->setCodStatus($Ocorrencia->getStatusOcorrencia());
    $OcorrenciaStatusDao = new OcorrenciaStatusDao();
    $OcorrenciaStatusDao->registrarSatus($OcorrenciaStatus);

    /*         * * carregar a TABELA DEV_OCORRENCIA E DEV_NOTAORIGEM** */
    $dadosNotaVenda = $OcorrenciaDao->consultaNotaVenda($cli_co_numero, $serieNota, $numNota, $dataNota);

    $DevNotaOrigem->setSerie($serieNota);
    $DevNotaOrigem->setNota($numNota);
    $DevNotaOrigem->setData($dataNota);

    $DevNotaOrigem->setCodCliente($cli_co_numero);
    $DevNotaOrigem->setOrigemVenda($dadosNotaVenda->TPORIGVENDA);
    $DevNotaOrigem->setCentroDist($dadosNotaVenda->CD);
    $DevNotaOrigem->setValorTotDevolucao($dadosNotaVenda->VLTOTDOCUM);
    $DevNotaOrigem->setDivisao($dadosNotaVenda->INDICADOR);
    $Ocorrencia->setDevNotaOrigem($DevNotaOrigem);


    $Ocorrencia->setValorDevolucao($dadosNotaVenda->VLTOTDOCUM); // ****** dev_ocorrencia
    $Ocorrencia->setCedis($dadosNotaVenda->CD);
    $Ocorrencia->setDivisao($dadosNotaVenda->INDICADOR);

    if ($OcorrenciaDao->verificaNotacadastrada($codOcorrencia)) {
        if (!$OcorrenciaDao->excluirOcorrencia($Ocorrencia)) {
            die("ERRO OCORRENCIA");
        }else if (!$OcorrenciaDao->inserir($Ocorrencia)) {
            die("ERRO OCORRENCIA");
        }
    } else {//atualiza
        if (!$OcorrenciaDao->alterar($Ocorrencia)) {
            die("ERRO ALTERAR");
        }
    }

    if (!$OcorrenciaDao->inserirNotaOrigem($Ocorrencia)) {            
        die("ERRO INSERIR NOTA");
    }

    $DevNotaOrigemItem = new DevNotaOrigemItem();

    $DevNotaOrigemItem->setCodigoOcorrencia($codOcorrencia);
    $DevNotaOrigemItem->setSerie($serieNota);
    $DevNotaOrigemItem->setNumNota($numNota);
    $DevNotaOrigemItem->setDataNota($dataNota);
    $DevNotaOrigemItem->setCodCliente($cli_co_numero);
    $DevNotaOrigemItem->setCodProduto($codigo);
    $DevNotaOrigemItem->setDevQuantidade($volume);
    $DevNotaOrigemItem->setPrecoUnitario(0);
    $DevNotaOrigemItem->setTotalBruto(0);
    $DevNotaOrigemItem->setTotalDesconto(0);
    $DevNotaOrigemItem->setTotalImposto(0);
    $DevNotaOrigemItem->setTotalLiquido(0);
    $DevNotaOrigemItem->setNotaQuantidade(0);             

    if(!$DevNotaOrigemItemDao->inserir($DevNotaOrigemItem)){
        die("ERRO AO INSERIR ITEM");
    }
    
} else if($acao == "EDITAR"){
    /*
     * Preencher dados comuns(parcial/integral) da devolucao
     */
    $DevNotaOrigemItemDao = new DevNotaOrigemItemDao();

    $Ocorrencia->setCodigoDevolucao($codOcorrencia);
    $Ocorrencia->getObjRota()->setCodigoRota($rot_co_numero);
    $Ocorrencia->getTipoOcorrencia()->setCodTipoOcorrencia(8);
    $Ocorrencia->setTipoTipoDevolucao(null);
    $Ocorrencia->getGlbCliente()->setClienteCodigo(trim($cli_co_numero));
    $Ocorrencia->getTipoOcorrencia()->setDepartamentoTipoOcorrencia(29);
    $Ocorrencia->setCodigoCadastranteDevolucao($ObjUsuario->getUsuarioCodigo());
    $Ocorrencia->setTipoCadastranteDevolucao($ObjUsuario->getUsuarioTipo());

    if ($ObjUsuario->getUsuarioTipo() == "CLI") {
        $Ocorrencia->setDeptContatoClienteDevolucao(strtoupper("CLIENTE"));
        $Ocorrencia->setEmailCadDevolucao($ObjUsuario->getUsuarioUsername());
    } else if ($ObjUsuario->getUsuarioTipo() == "INT") {        
        if($ObjUsuario->getObjSistemaPerfil()->getNomeSistemaPerfil() == 'CentralSolucao'){
            $Ocorrencia->setTipoCadastranteDevolucao('CSL');
            $Ocorrencia->setDeptContatoClienteDevolucao(strtoupper("CENTRAL SOLUCOES"));       
        }else{
            $Ocorrencia->setDeptContatoClienteDevolucao(strtoupper("DEVOLUCAO"));
        }
        $Ocorrencia->setEmailCadDevolucao($ObjUsuario->getObjColaborador()->getPessoaEmail());
    } else if ($ObjUsuario->getUsuarioTipo() == "VEN") {        
        if($ObjUsuario->getObjSistemaPerfil()->getNomeSistemaPerfil() == 'Gerente'){
            $Ocorrencia->setTipoCadastranteDevolucao('GER');
            $Ocorrencia->setDeptContatoClienteDevolucao(strtoupper("GERENTE"));       
        }else if($ObjUsuario->getObjSistemaPerfil()->getNomeSistemaPerfil() == 'Representante'){
            $Ocorrencia->setTipoCadastranteDevolucao('REP');
            $Ocorrencia->setDeptContatoClienteDevolucao(strtoupper("REPRESENTANTE"));       
        }else if($ObjUsuario->getObjSistemaPerfil()->getNomeSistemaPerfil() == 'Consultor'){
            $Ocorrencia->setTipoCadastranteDevolucao('CON');
            $Ocorrencia->setDeptContatoClienteDevolucao(strtoupper("CONSULTOR"));       
        }                       
        $Ocorrencia->setEmailCadDevolucao($ObjUsuario->getUsuarioUsername());
    } else if ($ObjUsuario->getUsuarioTipo() == "TRA") { //ajustarrr
        $Ocorrencia->setDeptContatoClienteDevolucao(strtoupper("TRANSPORTADORA"));  // AMARAL   
        $Ocorrencia->setEmailCadDevolucao($ObjUsuario->getUsuarioUsername()); //AMARAL
    }

    //$Ocorrencia->setDescricaoDevolucao(trim($doc_tx_descricao));

    //INFORMAR QUE O STATUS NESSE PONTO É NÃO: CADASTRO NAO FINALIZADO (14)
    $Ocorrencia->setStatusOcorrencia(14);
    /* Registrar o status na tabela de Ocorrencia Status */
    $OcorrenciaStatus = new OcorrenciaStatus();
    $OcorrenciaStatus->setCodigoOcorrencia($Ocorrencia->getCodigoDevolucao());
    $OcorrenciaStatus->setUsuario($ObjUsuario->getUsuarioCodigo());
    $OcorrenciaStatus->setCodStatus($Ocorrencia->getStatusOcorrencia());
    $OcorrenciaStatusDao = new OcorrenciaStatusDao();
    $OcorrenciaStatusDao->registrarSatus($OcorrenciaStatus);

    /*         * * carregar a TABELA DEV_OCORRENCIA E DEV_NOTAORIGEM** */
    $dadosNotaVenda = $OcorrenciaDao->consultaNotaVenda($cli_co_numero, $serieNota, $numNota, $dataNota);

    $DevNotaOrigem->setSerie($serieNota);
    $DevNotaOrigem->setNota($numNota);
    $DevNotaOrigem->setData($dataNota);

    $DevNotaOrigem->setCodCliente($cli_co_numero);
    $DevNotaOrigem->setOrigemVenda($dadosNotaVenda->TPORIGVENDA);
    $DevNotaOrigem->setCentroDist($dadosNotaVenda->CD);
    $DevNotaOrigem->setValorTotDevolucao($dadosNotaVenda->VLTOTDOCUM);
    $DevNotaOrigem->setDivisao($dadosNotaVenda->INDICADOR);
    $Ocorrencia->setDevNotaOrigem($DevNotaOrigem);


    $Ocorrencia->setValorDevolucao($dadosNotaVenda->VLTOTDOCUM); // ****** dev_ocorrencia
    $Ocorrencia->setCedis($dadosNotaVenda->CD);
    $Ocorrencia->setDivisao($dadosNotaVenda->INDICADOR);

    if (!$OcorrenciaDao->alterar($Ocorrencia)) {
        die("ERRO ALTERAR");
    }
    

    if (!$OcorrenciaDao->alterarNotaOrigem($Ocorrencia)) {            
        die("ERRO ALTERAR NOTA");
    }

    $DevNotaOrigemItem = new DevNotaOrigemItem();

    $DevNotaOrigemItem->setCodigoOcorrencia($codOcorrencia);
    $DevNotaOrigemItem->setSerie($serieNota);
    $DevNotaOrigemItem->setNumNota($numNota);
    $DevNotaOrigemItem->setDataNota($dataNota);
    $DevNotaOrigemItem->setCodCliente($cli_co_numero);
    $DevNotaOrigemItem->setCodProduto($co_produto);
    $DevNotaOrigemItem->setDevQuantidade($volume);
    $DevNotaOrigemItem->setPrecoUnitario(0);
    $DevNotaOrigemItem->setTotalBruto(0);
    $DevNotaOrigemItem->setTotalDesconto(0);
    $DevNotaOrigemItem->setTotalImposto(0);
    $DevNotaOrigemItem->setTotalLiquido(0);
    $DevNotaOrigemItem->setNotaQuantidade(0);             

    if(!$DevNotaOrigemItemDao->alterarItemOcorrencia($DevNotaOrigemItem)){
        die("ERRO AO ALTERAR ITEM");
    }
} else if ($acao == "LISTARNOTAS") {

    $Ocorrencia = new Ocorrencia();
    $itens =  new DevNotaOrigemItem();
    $Ocorrencia->setCodigoDevolucao($codOcorrencia);
    $itens = $OcorrenciaDao->consultaItensDevolucao($Ocorrencia);
    $valorFinalDevolucao = 0;
	
	$ClienteEspecialNotaDao = new ClienteEspecialNotaDao();
	$clienteEspecialNota = $ClienteEspecialNotaDao->verificaClienteEspecialNota($cli_co_numero);

	?>    
    <div class="row" id="areaCadastroPNota">
        <div class="col-md-2">
            <a class="btn btn-primary" onclick="javascript:abrirNotaOrigemItem();">Adicionar item nota</a>
        </div>                                                                                  
    </div>
    </br>
    <?php

	if (count($itens) > 0) {
        ?>
        <div class="table-responsive">
            <table class="table table-bordered table-striped table-hover">
                <thead>
                    <tr>
                        <th></th>
                        <th>Cód. EAN</th>
                        <th>Cód. Produto Servimed</th>
                        <th>Quantidade</th>
                        <th>Produto</th>                         
                    </tr>
                </thead>                               
                <tbody>
                    <?php
                    foreach ($itens as $item) {
                        ?>
                        <tr>
                            <td>
                                <button class="fa fa-minus-square" type="button" onclick="javascript: deletaItemOcorrencia(<?php echo $item->getCodProduto(); ?>, <?php echo $item->getCodigoOcorrencia(); ?>)"></button>
                            </td>                              
                            <td><?php echo $item->getCodEAN(); ?></td>
                            <td><?php echo $item->getCodProduto(); ?></td>
                            <td><?php echo $item->getDevQuantidade(); ?></td>
                            <td><?php echo $item->getNomeProduto(); ?></td>                                                        
                        </tr>
                        <?php                        
                    }
                    ?>                                  
                </tbody>
            </table>
        </div>
        
        <?php
    }
    if (count($itens) > 0) {
        ?>    
        <div class="row">
            <div class="col-md-12">
                <a class="btn btn-primary" onclick="javascript:FinalizaCadastroOcorrencia();">Cadastrar Ocorrência</a>
            </div>                                                                                  
        </div>
        <?php
    }
} else if ($acao == "FINALCAD") {
    
    $Ocorrencia->setCodigoDevolucao($codOcorrencia);        

    EspelhoNotaDevolucaoSobra($codOcorrencia);

    // ABAIXO SOMENTE QUANDO O USUARIO FINALIZAR O CADASTRO DA OCORRENCIA
    
    if (isset($reentregar)) {
        $Ocorrencia->setReentregaGerDevolucao($reentregar);
    }

    if (!isset($agregado)) {
        $controle_rota = new RotaDao();
        $result_rota = $controle_rota->consultaUsuRota(null, $rot_co_numero);
        OCIFetchInto($result_rota, $row_rota, OCI_ASSOC);
        $emailcoleta = $row_rota['USU_NO_EMAIL'];
    }
    if (strlen(trim($emailcoleta)) <= 0) {
        $emailcoleta = $ObjUsuario->getObjColaborador()->getPessoaEmail();
    }
    echo trim($emailcoleta);
    $Ocorrencia->setEmailColetaDevolucao(trim($emailcoleta));


    $TipoOcorrenciaDAO = new TipoOcorrenciaDAO();
    $TipoOcorrencia = new CalTipoOcorrencia();
    $ParametroDao = new ParametroDao();
    $Parametro = new devParametros();

    //$TipoOcorrencia = $TipoOcorrenciaDAO->consultarTipoOcorrencia($Ocorrencia->getTipoOcorrencia());
    $Parametro = $ParametroDao->consultar();


    $OcorrenciaPasso = new OcorrenciaPasso();
    $OcorrenciaPassoDao = new OcorrenciaPassoDao();

    $OcorrenciaStatus = new OcorrenciaStatus();
    $OcorrenciaStatusDao = new OcorrenciaStatusDao();

    
    /* informa neste ponto que o status da ocorrencia é CADASTRADO COM SUCESSO(15) */
    $Ocorrencia->setStatusOcorrencia(15);
    /* Registrar o status na tabela de Ocorrencia Status */

    $OcorrenciaStatus->setCodigoOcorrencia($Ocorrencia->getCodigoDevolucao());
    $OcorrenciaStatus->setUsuario($ObjUsuario->getUsuarioCodigo());
    $OcorrenciaStatus->setCodStatus($Ocorrencia->getStatusOcorrencia());
    $OcorrenciaStatusDao->registrarSatus($OcorrenciaStatus);
    
    /* informa neste ponto que o status da ocorrencia é aguardando aprovacao gerente(1) */
    $Ocorrencia->setStatusOcorrencia(1);
    /* Registrar o status na tabela de Ocorrencia Status */

    $OcorrenciaStatus->setCodigoOcorrencia($Ocorrencia->getCodigoDevolucao());
    $OcorrenciaStatus->setUsuario($ObjUsuario->getUsuarioCodigo());
    $OcorrenciaStatus->setCodStatus($Ocorrencia->getStatusOcorrencia());
    $OcorrenciaStatusDao->registrarSatus($OcorrenciaStatus);
	
	//rotina de valor para aprovação automatica
	$OcorrenciaAux = new Ocorrencia();
	$OcorrenciaAux->setCodigoDevolucao($codOcorrencia);
	$retornoDados = $OcorrenciaDao->consultaOcorrencia($OcorrenciaAux);
	$ocorrenciaDados = oci_fetch_object($retornoDados);
	$codCentroDist = $ocorrenciaDados->CED_CO_NUMERO;
	$divisao = $ocorrenciaDados->DEV_CO_DIVISAO; //('F', 'FARMA', 'H', 'HOSPITALAR', 'A', 'ALIMENTAR', 'O', 'DIST DIRETA')
	
	$ParametroValorDevolucaoDao = new ParametroValorDevolucaoDao();
	$ObjParametroValorDevolucao = $ParametroValorDevolucaoDao->consultar();

	if ($codCentroDist == 1) {
		$valorF = $ObjParametroValorDevolucao->getValor1F();
		$valorO = $ObjParametroValorDevolucao->getValor1O();
		$valorH = $ObjParametroValorDevolucao->getValor1H();
		$valorA = $ObjParametroValorDevolucao->getValor1A();
	}elseif ($codCentroDist == 46) {
		$valorF = $ObjParametroValorDevolucao->getValor46F();
		$valorO = $ObjParametroValorDevolucao->getValor46O();
		$valorH = $ObjParametroValorDevolucao->getValor46H();
		$valorA = $ObjParametroValorDevolucao->getValor46A();
	}elseif ($codCentroDist == 48) {
		$valorF = $ObjParametroValorDevolucao->getValor48F();
		$valorO = $ObjParametroValorDevolucao->getValor48O();
		$valorH = $ObjParametroValorDevolucao->getValor48H();
		$valorA = $ObjParametroValorDevolucao->getValor48A();
	}elseif ($codCentroDist == 49) {
		$valorF = $ObjParametroValorDevolucao->getValor49F();
		$valorO = $ObjParametroValorDevolucao->getValor49O();
		$valorH = $ObjParametroValorDevolucao->getValor49H();
		$valorA = $ObjParametroValorDevolucao->getValor49A();
	}
	
	if($divisao == "FARMA"){
		$ParametroValorAprovacaoAut = $valorF;
	}elseif($divisao == "DIST DIRETA"){
		$ParametroValorAprovacaoAut = $valorO;
	}elseif($divisao == "HOSPITALAR"){
		$ParametroValorAprovacaoAut = $valorH;
	}elseif($divisao == "ALIMENTAR"){
		$ParametroValorAprovacaoAut = $valorA;
	}else{
		$ParametroValorAprovacaoAut = $Parametro->getNumValorAprovacaoAut();
	}
	//FIM - rotina de valor para aprovação automatica

    $aprovacaoAutomatica = TRUE;
    $Ocorrencia->setIndicaAutomatico(2); //cod 2 para aprovaçao automatico de valor
    $OcorrenciaPasso->setCodigoOcorrencia($Ocorrencia->getCodigoDevolucao());
    $OcorrenciaPasso->setCodigoPasso(1); // 1 - gerente

    $OcorrenciaPassoDao->inserirPasso($OcorrenciaPasso);

    $OcorrenciaPasso->setUsuario($ObjUsuario->getUsuarioCodigo()); // 1 - geovanni.info (temporario)
    $OcorrenciaPasso->setIndicadorAprovado(1);
    $OcorrenciaPasso->setObservacao("APROVACAO AUTOMATICA POR VALOR");

    $OcorrenciaPassoDao->aprovarPasso($OcorrenciaPasso);

    //passa para o passo Transportador (2)
    $OcorrenciaPasso->setCodigoOcorrencia($Ocorrencia->getCodigoDevolucao());
    $OcorrenciaPasso->setCodigoPasso(2); // 2 - Trans

    $OcorrenciaPassoDao->inserirPasso($OcorrenciaPasso);

    /* informa o passo para a tabela dev_ocorrencia */
    $Ocorrencia->setPassoOcorrencia($OcorrenciaPasso->getCodigoPasso());

    /* informa neste ponto que o status da ocorrencia é APROVADO(2) */
    $Ocorrencia->setStatusOcorrencia(2);
    /* Registrar o status na tabela de Ocorrencia Status */
    $OcorrenciaStatus = new OcorrenciaStatus();
    $OcorrenciaStatus->setCodigoOcorrencia($Ocorrencia->getCodigoDevolucao());
    $OcorrenciaStatus->setUsuario($ObjUsuario->getUsuarioCodigo());
    $OcorrenciaStatus->setCodStatus($Ocorrencia->getStatusOcorrencia());
    $OcorrenciaStatusDao->registrarSatus($OcorrenciaStatus);

    /*
        Quando a ocorrencia for do tipo: 3- INTEGRAL ATO DA ENTREGA
        Nao tem necessidade do cliente informar a nota de devolucao
        neste caso segue o processo para aguardando coleta
    */

    /* informa neste ponto que o status da ocorrencia é aguardando coleta(5) */
    $Ocorrencia->setStatusOcorrencia(5);
    /* Registrar o status na tabela de Ocorrencia Status */
    $OcorrenciaStatus = new OcorrenciaStatus();
    $OcorrenciaStatus->setCodigoOcorrencia($Ocorrencia->getCodigoDevolucao());
    $OcorrenciaStatus->setUsuario($ObjUsuario->getUsuarioCodigo());
    $OcorrenciaStatus->setCodStatus($Ocorrencia->getStatusOcorrencia());
    $OcorrenciaStatusDao->registrarSatus($OcorrenciaStatus);

    //salva arquivo(ordem coleta) na tabela DEV_OCORRENCIA_ARQUIVO
    $nomeOrdemSystem = "ordemColeta_{$Ocorrencia->getCodigoDevolucao()}.pdf";
    $OcorrenciaArquivosDao = new OcorrenciaArquivosDao();
    $OcorrenciaArquivos = new OcorrenciaArquivos();
    $OcorrenciaArquivos->setCodOcorrencia($Ocorrencia->getCodigoDevolucao());
    $OcorrenciaArquivos->setCodigoTipoArquivo(2); // ordem coleta 2
    // $OcorrenciaArquivos->setNomeArquivoCliente($nomeEspelhoSystem);
    $OcorrenciaArquivos->setNomeArquivoSystem($nomeOrdemSystem);
    // $OcorrenciaArquivos->setCodigoUsuario($ObjUsuario->getUsuarioCodigo());
    $OcorrenciaArquivos->setObservacaoArquivo("Ordem coleta para a ocorrência: " . $Ocorrencia->getCodigoDevolucao());
    $OcorrenciaArquivosDao->registrarArquivo($OcorrenciaArquivos);      

    $nomeEspelhoSystem = "espelhoNota_{$Ocorrencia->getCodigoDevolucao()}.pdf";
    $OcorrenciaArquivosDao = new OcorrenciaArquivosDao();
    $OcorrenciaArquivos = new OcorrenciaArquivos();
    $OcorrenciaArquivos->setCodOcorrencia($Ocorrencia->getCodigoDevolucao());
    $OcorrenciaArquivos->setCodigoTipoArquivo(3); // espelho nota 3
    //$OcorrenciaArquivos->setNomeArquivoCliente($nomeEspelhoSystem);
    $OcorrenciaArquivos->setNomeArquivoSystem($nomeEspelhoSystem);
    //$OcorrenciaArquivos->setCodigoUsuario($ObjUsuario->getUsuarioCodigo());
    $OcorrenciaArquivos->setObservacaoArquivo("Espelho de nota para a ocorrência: " . $Ocorrencia->getCodigoDevolucao());
    $OcorrenciaArquivosDao->registrarArquivo($OcorrenciaArquivos);

    if($volume == ""){
        $Ocorrencia->setQntdVolumeDevolucao(0);
    }else{
        $Ocorrencia->setQntdVolumeDevolucao($volume);
    }

    if (!$OcorrenciaDao->alterar($Ocorrencia)) {
        die("<center><strong class='msmerro'>Desculpe, erro na aprova&ccedil;&atilde;o do passo </strong></center>");
    } else {
        // $emailPortalDevolucao = new emailPortalDevolucao();
        // $emailPortalDevolucao->enviaEmailCadastro($Ocorrencia);

        // if ($aprovacaoAutomatica) {
        //     //if ($tipoDevolucao == 3) {
        //         $emailPortalDevolucao->enviaEmailAprovadoSemEspelho($Ocorrencia);
        //     //} else {
        //     //    $emailPortalDevolucao->enviaEmailAprovadoComEspelho($Ocorrencia);
        //     //}
        // }								
    }
} elseif ($acao == 'DELNOTA') {
    $DevNotaOrigemItemDao = new DevNotaOrigemItemDao();
    
    $cod_produto = $_POST['cod_produto'];
    $cod_ocorrencia = $_POST['cod_ocorrencia'];
    
    $DevNotaOrigemItem = new DevNotaOrigemItem();
    $DevNotaOrigemItem->setCodigoOcorrencia($cod_ocorrencia);
    $DevNotaOrigemItem->setCodProduto($cod_produto);
        
    if (!$DevNotaOrigemItemDao->excluirItemOcorrencia($DevNotaOrigemItem)) {
        die("<script> alert('Erro ao Excluir Item!') </script>");
    } else {
        die("<script> alert('Item Excluido com Sucesso!') </script>");
    }
} elseif ($acao == "EDITARPRODUTO") {
    $DevNotaOrigemItemDao = new DevNotaOrigemItemDao();
    $DevNotaOrigemItem = new DevNotaOrigemItem();

    $DevNotaOrigemItem->setCodigoOcorrencia($codOcorrencia);
    $DevNotaOrigemItem->setSerie($serieNota);
    $DevNotaOrigemItem->setNumNota($numNota);
    $DevNotaOrigemItem->setDataNota($dataNota);
    $DevNotaOrigemItem->setCodCliente($cli_co_numero);
        
    $someArray = json_decode($tblProdutos, true);

    foreach ($someArray as $address)
    {
        $DevNotaOrigemItem->setCodProduto($address["id"]);
        echo $address["quantity"];
        $DevNotaOrigemItem->setDevQuantidade($address["quantity"]);    
        if(!$DevNotaOrigemItemDao->alterarItemOcorrencia($DevNotaOrigemItem)){
            die("ERRO AO ALTERAR ITEM");
            break;
        }                
    };
        
}