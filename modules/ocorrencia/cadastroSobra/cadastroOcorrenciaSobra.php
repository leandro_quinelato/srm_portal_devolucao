<!DOCTYPE html>
<?php
session_start();
include_once( "../../../includes/Dao/DaoSistema.class.php" );
include_once( "../../../includes/Dao/OcorrenciaDao.class.php" );
include_once( "../../../includes/CalTipoOcorrencia.class.php" );
include_once( "../../../includes/Dao/TipoOcorrenciaDAO.class.php" );
include_once( "../../../includes/TipoDevolucao.class.php" );
include_once( "../../../includes/Dao/TipoDevolucaoDao.class.php" );
include_once("DaoGlobal.class.php");
include_once("SistemaDao.class.php");
include_once("GlbSistema.class.php");
include_once("GlbSistemaPerfil.class.php");
include_once("GlbUsuario.class.php");
include_once("GlbColaborador.class.php");
include_once("GlbPessoa.class.php");
include_once("GlbEndereco.class.php");
include_once("GlbTelefone.class.php");
include_once("include_novo/GlbTransportadora.class.php");
include_once("include_novo/GlbContatoTransportador.class.php");
include_once("include_novo/ContatoTransportadorDao.class.php");
include_once("GlbProgramaRotina.class.php");
include_once("GlbModuloPrograma.class.php");
include_once("GlbSistemaModulo.class.php");
include_once("PerfilUsuarioDao.class.php");
include_once("PerfilPermissaoDao.class.php");

$ObjUsuario = unserialize($_SESSION['ObjLogin']);
//$ObjRotinas = $ObjUsuario->getObjSistemaPerfil()->getObjRotina();
//echo "<pre>teste?<br>";
//print_r($ObjUsuario->getObjTransportadora()->getObjContato());
//echo "</pre>";

$OcorrenciaDao = new OcorrenciaDao();
/* Gerar codigo da ocorrencia */
$doc_co_numero = $OcorrenciaDao->gerarCodigoOcorrencia($ObjUsuario->getUsuarioCodigo());

$TipoOcorrenciaDao = new TipoOcorrenciaDAO();
$TipoOcorrencias = new CalTipoOcorrencia();
//$TipoOcorrencias = $TipoOcorrenciaDao->listarTipoOcorrencia($TipoOcorrencias);
$codPerfilUsuario = $ObjUsuario->getObjSistemaPerfil()->getCodigoSistemaPerfil();
$TipoOcorrencias = $TipoOcorrenciaDao->listarTipoOcorrenciaPerfil($TipoOcorrencias, $codPerfilUsuario);

$TiposDevolucao = new TipoDevolucao();
$TipoDevolucaoDao = new TipoDevolucaoDao();

$TiposDevolucao->setStatus('A');
$TiposDevolucao = $TipoDevolucaoDao->listarTipoDevolucao($TiposDevolucao);

if ($ObjUsuario->getUsuarioTipo() == "VEN") {
    $VendedorUsuario = $ObjUsuario->getObjVendedor();
                        $ObjVendedor = new GlbVendedor();
                        if ($VendedorUsuario[0]) {
                            $ObjVendedor->setCodigo($VendedorUsuario[0]->getCodigo());
                            $ObjVendedor->setTipo($VendedorUsuario[0]->getTipo());
                        }
    $resultClienteVen = $OcorrenciaDao->consultarClienteVendedor($ObjVendedor);
}


?>
<html lang="en">
    <head>        
        <!-- meta section -->
        <title>Ocorrências Sobra - Nova</title>
        <?php include("../../../library/head.php"); ?>
        <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/libCadastroOcorrenciaSobra.js"></script>    
        <link rel="stylesheet" href="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/chosen/docsupport/prism.css" >
        <link rel="stylesheet" href="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/chosen/chosen.css" >
    </head>
    <body>
        <!-- set loading layer -->
        <div class="dev-page-loading preloader"></div>
            <!-- ./set loading layer -->

            <!-- page wrapper -->
            <div class="dev-page">

                <!-- page header -->    
                <?php include("../../../library/topo.php"); ?>
                <!-- ./page header -->

                <!-- page container -->
                <div class="dev-page-container">

                    <!-- page sidebar -->
                    <?php include("../../../library/menu.php"); ?>
                    <!-- ./page sidebar -->

                    <!-- page content -->
                    <div class="dev-page-content">                    
                    <!-- page content container -->
                        <div class="container">

                            <!-- page title -->
                            <div class="page-title">
                                <h1>Registro Protocolo Sobra</h1>

                                <ul class="breadcrumb">
                                    <li><a href="#">Devolução</a></li>
                                    <li>Registro Protocolo Sobra</li>
                                </ul>
                            </div>                        
                            <!-- ./page title -->
                            <!-- datatables plugin -->
                            <div class="wrapper wrapper-white">
                                <form id="formCadOcorrencia">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Cód. Ocorrencia</label>
                                                <input type="text" placeholder="Cod Ocorrencia" class="form-control" value="<?php echo $doc_co_numero; ?>" readonly id="codOcorrencia" name="codOcorrencia">
                                            </div>
                                        </div> 
                                           
                                        <?php                                        
                                            if ($ObjUsuario->getUsuarioTipo() == "CLI") {                                        
                                        ?>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Cód. Cliente</label>
                                                <input type="text" placeholder="Cliente" class="form-control" size="10" id="cli_co_numero" name="cli_co_numero" onKeyPress="fMascara('numero', event, this)" onBlur="javascript:apresenta_cliente()" value="<?php echo $ObjUsuario->getUsuarioCodigo() ?>" readonly>
                                            </div>
                                        </div>
                                        <?php
                                            }
                                            else {      
                                        ?>                                       
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label>Cód. Cliente</label>
                                                    <input type="text" placeholder="Cliente" class="form-control" size="10" id="cli_co_numero" name="cli_co_numero" onKeyPress="fMascara('numero', event, this)" onchange="javascript:apresenta_cliente()">
                                                </div>
                                            </div>          
                                        <?php
                                            }     
                                        ?>
                                        <div id="dadosCliente">
                                        </div>                                        
                                        </br>                                                                       
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="form-group">                                                        
                                                <label>Série</label>
                                                <input type="text" placeholder="Série" class="form-control" id="serieNota" name="serieNota"
                                                    onkeyup="proximoCampo(this, 'numNota', '2')" onkeypress="fMascara('numero', event, this)" value="<?php echo $serieNota; ?>">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>Nota</label>
                                                <input type="text" placeholder="Nota" class="form-control" id="numNota" name="numNota"
                                                    onkeyup="proximoCampo(this, 'dataNota', '6')" onkeypress="fMascara('numero', event, this)" value="<?php echo $numNota ?>">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>Data Nota</label>                            
                                                <input type="text" class="form-control datepicker" id="dataNota" name="dataNota" onkeypress="fMascara('data', event, this)" maxlength="10" value="<?php echo $dataNota?>">
                                            </div>
                                        </div> 
                                        <div class="col-md-3">
                                            <div class="form-group">                                                
                                                <label>Qtd. Volume(s) à ser coletado</label>
                                                <input type="number" placeholder="Volume" class="form-control" id="volume" name="volume">
                                            </div>
                                        </div>
                                    </div> 
                                </form>
                                <div id="areaListaNotaOco">
                                    <div class="row" id="areaCadastroPNota">
                                        <div class="col-md-2">
                                            <a class="btn btn-primary" onclick="javascript:abrirNotaOrigemItem();">Adicionar item nota</a>
                                        </div>                                                                                  
                                    </div>
                                </div> 
                            </div>
                            <div id="areaItensNota" >  
                                                                                                                 
                            </div>
                        </div> 
                        <!-- fim Area NOTA ORIGEM -->
                        <!-- ./datatables plugin -->
                        <!-- Copyright -->
                        <?php include("../../../library/copyright.php"); ?>
                        <!-- ./Copyright -->
                    </div>
                    <!-- ./page content container -->                                       
                </div>
                <!-- ./page content -->                                                
            </div>  
            <!-- ./page container -->
            <!-- page footer -->    
            <?php include("../../../library/footer.php"); ?>
            <!-- ./page footer -->
            <!-- page search -->
            <!-- page search -->            
        </div>
        <!-- ./page wrapper -->
        <!-- Inicio Modal NOTA ORIGEM --> 
        <div class="modal fade" id="modal_wizard" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

        </div>        

        <?php include("../../../library/rodape.php"); ?>

        <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js"></script>
        <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/plugins/datatables/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/plugins/smartwizard/jquery.smartWizard.js"></script>
        <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/plugins/sortable/sortable.min.js"></script>
        
        <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/plugins/sortable/sortable.min.js"></script>
        
        <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/chosen/chosen.jquery.js" ></script>
        <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/chosen/docsupport/prism.js" charset="utf-8"></script>
        
        <script type="text/javascript">
            var config = {
            '.chosen-select'           : {},
            '.chosen-select-deselect'  : {allow_single_deselect:true},
            '.chosen-select-no-single' : {disable_search_threshold:10},
            '.chosen-select-no-results': {no_results_text:'Cliente não encontrado!'},
            '.chosen-select-width'     : {width:"95%"}
            }
            for (var selector in config) {
            $(selector).chosen(config[selector]);
            }
            
        </script>
        <!-- ./javascripts -->
        <?php
            if ($ObjUsuario->getUsuarioTipo() == "CLI") {
        ?>    
        <script type="text/javascript">
                apresenta_cliente();
        </script>
        <?php
            }
        ?>                                    
    </body>
</html>

<script type="text/javascript">
    function proximoCampo(atual, proximo, maxLength){
        if(atual.value.length >= maxLength){
            document.getElementById(proximo).focus();
        }
    };

    $('#modal_wizard').on('shown.bs.modal', function () {
        $('#cod_barra').focus();
    })
</script>  
