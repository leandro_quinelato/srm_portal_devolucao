<?php
session_start();

include_once( "../../../includes/Dao/DaoSistema.class.php" );
include_once( "../../../includes/Rota.class.php" );
include_once( "../../../includes/Dao/RotaDao.class.php" );
include_once( "../../../includes/Dao/OcorrenciaDao.class.php" );
include_once( "../../../includes/Dao/ProdutoDao.class.php" );
include_once( "../../../includes/CalDepartamento.class.php" );
include_once( "../../../includes/CalTipoOcorrencia.class.php" );
include_once( "../../../includes/Dao/TipoOcorrenciaDAO.class.php" );
include_once( "../../../includes/Ocorrencia.class.php" );
include_once( "../../../includes/Dao/OcorrenciaDao.class.php" );
include_once( "../../../includes/TipoDevolucao.class.php" );
include_once( "../../../includes/Dao/TipoDevolucaoDao.class.php" );

include_once( "../../../includes/DevNotaOrigemItem.class.php" );
include_once( "../../../includes/Dao/DevNotaOrigemItemDao.class.php" );

include_once( "../../../includes/DevParametros.class.php" );
include_once( "../../../includes/Dao/ParametroDao.class.php" );

include_once( "../../../includes/OcorrenciaPasso.class.php" );

include_once( "../../../includes/OcorrenciaStatus.class.php" );
include_once( "../../../includes/Dao/OcorrenciaStatusDao.class.php" );

include_once( "../../../includes/OcorrenciaArquivos.class.php" );
include_once( "../../../includes/Dao/OcorrenciaArquivosDao.class.php" );

include_once( "../../utilitario/email/emailPortalDevolucao.class.php" );
include_once( "../../utilitario/espelhoNotaDevolucao/espelhoNotaDevolucaoSobraMain.php" );

include_once( "../../../includes/Dao/ClienteEspecialNotaDao.class.php" );

include_once( "../../../includes/DevParametrosValorDevolucao.class.php" );
include_once( "../../../includes/Dao/ParametroValorDevolucaoDao.class.php" );

include_once("DaoGlobal.class.php");
include_once("SistemaDao.class.php");
include_once("GlbSistema.class.php");
include_once("GlbSistemaPerfil.class.php");
include_once("GlbUsuario.class.php");
include_once("GlbColaborador.class.php");
include_once("GlbPessoa.class.php");
include_once("GlbEndereco.class.php");
include_once("GlbTelefone.class.php");
include_once("include_novo/GlbTransportadora.class.php");
include_once("include_novo/GlbContatoTransportador.class.php");
include_once("include_novo/ContatoTransportadorDao.class.php");
include_once("GlbProgramaRotina.class.php");
include_once("GlbModuloPrograma.class.php");
include_once("GlbSistemaModulo.class.php");
include_once("PerfilUsuarioDao.class.php");
include_once("PerfilPermissaoDao.class.php");
include_once("integraSetSistemas.php");
include_once("class.phpmailer.php");
include_once("class.smtp.php");

include_once("include_novo/Tradutor.class.php");


$ObjUsuario = unserialize($_SESSION['ObjLogin']);

//echo "<pre>";
//print_r($ObjUsuario);
//echo "</pre>";


extract($_REQUEST);
$Ocorrencia = new Ocorrencia();
$DevNotaOrigem = new DevNotaOrigem();
$RotaDao = new RotaDao();
$OcorrenciaDao = new OcorrenciaDao();
$ProdutoDao = new ProdutoDao();
$resultRota = $RotaDao->listar();
if (strlen($co_produto) <= 0) {
    die();
}

if ($ObjUsuario->getUsuarioTipo() == "VEN") {
    $VendedorUsuario = $ObjUsuario->getObjVendedor();
                        $ObjVendedor = new GlbVendedor();
                        if ($VendedorUsuario[0]) {
                            $ObjVendedor->setCodigo($VendedorUsuario[0]->getCodigo());
                            $ObjVendedor->setTipo($VendedorUsuario[0]->getTipo());
                        }
    $resultRotaVen = $RotaDao->consultarRotaVendedor($ObjVendedor);
}

if($ObjUsuario->getUsuarioTipo() == "TRA"){
    $resultCliente= $OcorrenciaDao->consultaClienteTransp($co_produto,$ObjUsuario->getUsuarioCodigo() );
    
    $resultRota  = $RotaDao->consultaUsuRota($ObjUsuario->getUsuarioCodigo());
}else{
    
    $resultCliente = $OcorrenciaDao->consultaCliente($co_produto);
}
$resultCliente1= $ProdutoDao->consultaProduto($co_produto,$ObjUsuario->getUsuarioCodigo());

    OCIFetchInto($resultCliente1, $rowCliente, OCI_ASSOC);
    if ($rowCliente['PRO_NO_DESCRICAO']) {
        $DevNotaOrigemItemDao = new DevNotaOrigemItemDao();
        if ($OcorrenciaDao->verificaNotacadastrada($codOcorrencia)) {
            
            $Ocorrencia->setCodigoDevolucao($codOcorrencia);
            $Ocorrencia->getObjRota()->setCodigoRota($rot_co_numero);
            $Ocorrencia->getTipoOcorrencia()->setCodTipoOcorrencia(80);
            $Ocorrencia->setTipoTipoDevolucao(5);
            $Ocorrencia->getGlbCliente()->setClienteCodigo(trim($cli_co_numero));
            $Ocorrencia->getTipoOcorrencia()->setDepartamentoTipoOcorrencia(29);
            $Ocorrencia->setCodigoCadastranteDevolucao($ObjUsuario->getUsuarioCodigo());
            $Ocorrencia->setTipoCadastranteDevolucao($ObjUsuario->getUsuarioTipo());

            if ($ObjUsuario->getUsuarioTipo() == "CLI") {
                $Ocorrencia->setDeptContatoClienteDevolucao(strtoupper("CLIENTE"));
                $Ocorrencia->setEmailCadDevolucao($ObjUsuario->getUsuarioUsername());
            } else if ($ObjUsuario->getUsuarioTipo() == "INT") {        
                if($ObjUsuario->getObjSistemaPerfil()->getNomeSistemaPerfil() == 'CentralSolucao'){
                    $Ocorrencia->setTipoCadastranteDevolucao('CSL');
                    $Ocorrencia->setDeptContatoClienteDevolucao(strtoupper("CENTRAL SOLUCOES"));       
                }else{
                    $Ocorrencia->setDeptContatoClienteDevolucao(strtoupper("DEVOLUCAO"));
                }
                $Ocorrencia->setEmailCadDevolucao($ObjUsuario->getObjColaborador()->getPessoaEmail());
            } else if ($ObjUsuario->getUsuarioTipo() == "VEN") {        
                if($ObjUsuario->getObjSistemaPerfil()->getNomeSistemaPerfil() == 'Gerente'){
                    $Ocorrencia->setTipoCadastranteDevolucao('GER');
                    $Ocorrencia->setDeptContatoClienteDevolucao(strtoupper("GERENTE"));       
                }else if($ObjUsuario->getObjSistemaPerfil()->getNomeSistemaPerfil() == 'Representante'){
                    $Ocorrencia->setTipoCadastranteDevolucao('REP');
                    $Ocorrencia->setDeptContatoClienteDevolucao(strtoupper("REPRESENTANTE"));       
                }else if($ObjUsuario->getObjSistemaPerfil()->getNomeSistemaPerfil() == 'Consultor'){
                    $Ocorrencia->setTipoCadastranteDevolucao('CON');
                    $Ocorrencia->setDeptContatoClienteDevolucao(strtoupper("CONSULTOR"));       
                }                       
                $Ocorrencia->setEmailCadDevolucao($ObjUsuario->getUsuarioUsername());
            } else if ($ObjUsuario->getUsuarioTipo() == "TRA") { //ajustarrr
                $Ocorrencia->setDeptContatoClienteDevolucao(strtoupper("TRANSPORTADORA"));  // AMARAL   
                $Ocorrencia->setEmailCadDevolucao($ObjUsuario->getUsuarioUsername()); //AMARAL
            }

            //$Ocorrencia->setDescricaoDevolucao(trim($doc_tx_descricao));

            //INFORMAR QUE O STATUS NESSE PONTO É NÃO: CADASTRO NAO FINALIZADO (14)
            $Ocorrencia->setStatusOcorrencia(14);
            /* Registrar o status na tabela de Ocorrencia Status */
            $OcorrenciaStatus = new OcorrenciaStatus();
            $OcorrenciaStatus->setCodigoOcorrencia($Ocorrencia->getCodigoDevolucao());
            $OcorrenciaStatus->setUsuario($ObjUsuario->getUsuarioCodigo());
            $OcorrenciaStatus->setCodStatus($Ocorrencia->getStatusOcorrencia());
            $OcorrenciaStatusDao = new OcorrenciaStatusDao();
            $OcorrenciaStatusDao->registrarSatus($OcorrenciaStatus);

            /*         * * carregar a TABELA DEV_OCORRENCIA E DEV_NOTAORIGEM** */
            $dadosNotaVenda = $OcorrenciaDao->consultaNotaVenda($cli_co_numero, $serieNota, $numNota, $dataNota);

            $DevNotaOrigem->setSerie($serieNota);
            $DevNotaOrigem->setNota($numNota);
            $DevNotaOrigem->setData($dataNota);

            $DevNotaOrigem->setCodCliente($cli_co_numero);
            $DevNotaOrigem->setOrigemVenda($dadosNotaVenda->TPORIGVENDA);
            $DevNotaOrigem->setCentroDist($dadosNotaVenda->CD);
            $DevNotaOrigem->setValorTotDevolucao($dadosNotaVenda->VLTOTDOCUM);
            $DevNotaOrigem->setDivisao($dadosNotaVenda->INDICADOR);
            $Ocorrencia->setDevNotaOrigem($DevNotaOrigem);


            $Ocorrencia->setValorDevolucao($dadosNotaVenda->VLTOTDOCUM); // ****** dev_ocorrencia
            $Ocorrencia->setCedis($dadosNotaVenda->CD);
            $Ocorrencia->setDivisao($dadosNotaVenda->INDICADOR);

            if (!$OcorrenciaDao->inserir($Ocorrencia)) {
                die("ERRO OCORRENCIA");
            }

            if (!$OcorrenciaDao->inserirNotaOrigem($Ocorrencia)) {            
                die("ERRO INSERIR NOTA");
            }
        }
    
        $DevNotaOrigemItem = new DevNotaOrigemItem();

        $DevNotaOrigemItem->setCodigoOcorrencia($codOcorrencia);
        $DevNotaOrigemItem->setSerie($serieNota);
        $DevNotaOrigemItem->setNumNota($numNota);
        $DevNotaOrigemItem->setDataNota($dataNota);
        $DevNotaOrigemItem->setCodCliente($cli_co_numero);
        $DevNotaOrigemItem->setCodProduto($rowCliente['PRO_CO_NUMERO']);
        $DevNotaOrigemItem->setDevQuantidade(0);
        $DevNotaOrigemItem->setPrecoUnitario(0);
        $DevNotaOrigemItem->setTotalBruto(0);
        $DevNotaOrigemItem->setTotalDesconto(0);
        $DevNotaOrigemItem->setTotalImposto(0);
        $DevNotaOrigemItem->setTotalLiquido(0);
        $DevNotaOrigemItem->setNotaQuantidade(0);             

        if(!$DevNotaOrigemItemDao->inserir($DevNotaOrigemItem)){
            die("ERRO AO INSERIR ITEM");
        }
        $itens = $OcorrenciaDao->consultaItensDevolucaoCadastro($codOcorrencia);
        if (count($itens) > 0) {
        ?>  
        <div class="table-responsive">
            <table id="tblProdutos" class="table table-bordered table-striped table-hover">
                <thead>
                    <tr>
                        <th>Cód. EAN</th>
                        <th>Cód. Produto Servimed</th>                                            
                        <th>Produto</th>              
                        <th>Quantidade</th>           
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($itens as $item) {
                    ?>
                        <tr>                         
                            <td><?php echo $item->getCodEAN(); ?></td>
                            <td><?php echo $item->getCodProduto(); ?></td>
                            <td><?php echo $item->getNomeProduto(); ?></td>                  
                            <td><input type="number" placeholder="Quantidade Unitária" class="form-control" id="qnt_unitaria" name="qnt_unitaria"></td>                                                                  
                        </tr>
                    <?php                        
                    }
                    ?>                                  
                </tbody>                              
            </table>
        </div>
<?php
        }
    }else{
?>
        <script> 
            alert('Caro usuário!\n'+
                'Caso não consiga realizar a inclusão do item, informando os dados, código do produto Servimed e ou código EAN, favor realizar o contato com a nossa Central de Atendimento, através do número 0800 709 9065.\n'+
                'Obrigado.');                                          
        </script>
<?php
    }
?>