<?php
include_once( "../../../includes/Dao/DaoSistema.class.php" );
include_once( "../../../includes/Dao/OcorrenciaDao.class.php" );
include_once( "../../../includes/CalTipoOcorrencia.class.php" );
include_once( "../../../includes/Dao/TipoOcorrenciaDAO.class.php" );
include_once( "../../../includes/TipoDevolucao.class.php" );
include_once( "../../../includes/Dao/TipoDevolucaoDao.class.php" );
include_once("DaoGlobal.class.php");
include_once("SistemaDao.class.php");
include_once("GlbSistema.class.php");
include_once("GlbSistemaPerfil.class.php");
include_once("GlbUsuario.class.php");
include_once("GlbColaborador.class.php");
include_once("GlbPessoa.class.php");
include_once("GlbEndereco.class.php");
include_once("GlbTelefone.class.php");
include_once("include_novo/GlbTransportadora.class.php");
include_once("include_novo/GlbContatoTransportador.class.php");
include_once("include_novo/ContatoTransportadorDao.class.php");
include_once("GlbProgramaRotina.class.php");
include_once("GlbModuloPrograma.class.php");
include_once("GlbSistemaModulo.class.php");
include_once("PerfilUsuarioDao.class.php");
include_once("PerfilPermissaoDao.class.php");
extract($_REQUEST);

$OcorrenciaDao = new OcorrenciaDao();
?>

<div class="modal-dialog ModalNotaVenda">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" onclick="fecharAguarde();" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Adicionar Itens de Sobra</h4>
        </div>
        <div class="modal-body">
            <div id="step-6">
                <div class="form-group">
                    <div class="wrapper wrapper-white">
                        <form id="formCadItemSobra">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label>FAVOR INFORMAR CÓDIGO DE BARRA (EAN) E/OU CÓDIGO DO PRODUTO SERVIMED NOS CAMPOS INDICADOS E SUAS RESPECTIVAS QUANTIDADES.</label>
                                        <label>CÓDIGO DE BARRAS (EAN) PODERÁ SER INFORMADO ATRAVÉS DE LEITOR</label>
                                    </div>
                                </div>
                            </div>                            
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Cód. Barras (EAN)</label>
                                        <input autofocus type="text" placeholder="Cód. Barra" class="form-control" value="<?php echo $doc_co_numero; ?>" 
                                            id="cod_barra" name="cod_barra" onchange="javascript:apresenta_dados_produto(1)">
                                    </div>
                                </div>                                                                                
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Cód. Produto</label>
                                        <input type="text" required data-errormessage-value-missing="Campo não pode ser deixado em branco" placeholder="Cód. Produto" class="form-control" size="10" id="co_produto" name="co_produto" onKeyPress="fMascara('numero', event, this)" onchange="javascript:apresenta_dados_produto(2)">
                                    </div>
                                </div>                                                                                              
                                </br>                                                                       
                            </div>
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover">
                                    <tbody>
                                    <div id="dadosItemNota">
                                    </div> 
                                    </tbody>                               
                                </table>
                            </div>
                        </form>
                        <div class="modal-footer" id="areaBotaoCadastro" >
                            <button data-dismiss="modal" class="btn btn-default" onclick="fecharAguarde();" type="button">Cancelar</button>       
                            <button class="btn btn-primary" type="button" onclick="javascript: cadastrarOcorrencia();" id="btnSalvarNota">Salvar</button>
                            <div id="msgCadNotaOrigem"></div>
                        </div>  
                    </div>
                </div>                                                                                                                    
            </div>            
        </div>
                      
    </div>
</div>