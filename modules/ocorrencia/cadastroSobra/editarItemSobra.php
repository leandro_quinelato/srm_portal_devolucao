<?php
session_start();

include_once( "../../../includes/Dao/DaoSistema.class.php" );
include_once( "../../../includes/Rota.class.php" );
include_once( "../../../includes/Dao/RotaDao.class.php" );
include_once( "../../../includes/Dao/OcorrenciaDao.class.php" );
include_once( "../../../includes/Dao/ProdutoDao.class.php" );

include_once("DaoGlobal.class.php");
include_once("SistemaDao.class.php");
include_once("GlbSistema.class.php");
include_once("GlbSistemaPerfil.class.php");
include_once("GlbUsuario.class.php");
include_once("GlbColaborador.class.php");
include_once("GlbPessoa.class.php");
include_once("GlbEndereco.class.php");
include_once("GlbTelefone.class.php");
include_once("include_novo/GlbTransportadora.class.php");
include_once("include_novo/GlbContatoTransportador.class.php");
include_once("include_novo/ContatoTransportadorDao.class.php");
include_once("GlbProgramaRotina.class.php");
include_once("GlbModuloPrograma.class.php");
include_once("GlbSistemaModulo.class.php");
include_once("PerfilUsuarioDao.class.php");
include_once("PerfilPermissaoDao.class.php");

$ObjUsuario = unserialize($_SESSION['ObjLogin']);


extract($_REQUEST);
$RotaDao = new RotaDao();
$OcorrenciaDao = new OcorrenciaDao();
$ProdutoDao = new ProdutoDao();
$resultRota = $RotaDao->listar();
if (strlen($co_produto) <= 0) {
    die();
}

if ($ObjUsuario->getUsuarioTipo() == "VEN") {
    $VendedorUsuario = $ObjUsuario->getObjVendedor();
                        $ObjVendedor = new GlbVendedor();
                        if ($VendedorUsuario[0]) {
                            $ObjVendedor->setCodigo($VendedorUsuario[0]->getCodigo());
                            $ObjVendedor->setTipo($VendedorUsuario[0]->getTipo());
                        }
    $resultRotaVen = $RotaDao->consultarRotaVendedor($ObjVendedor);
}

if($ObjUsuario->getUsuarioTipo() == "TRA"){
    $resultCliente= $OcorrenciaDao->consultaClienteTransp($co_produto,$ObjUsuario->getUsuarioCodigo() );
    
    $resultRota  = $RotaDao->consultaUsuRota($ObjUsuario->getUsuarioCodigo());
}else{
    
    $resultCliente = $OcorrenciaDao->consultaCliente($co_produto);
}
$resultCliente1= $ProdutoDao->consultaProduto($co_produto,$ObjUsuario->getUsuarioCodigo() );
//echo "alo?";

    OCIFetchInto($resultCliente1, $rowCliente, OCI_ASSOC);
    if ($rowCliente['PRO_NO_DESCRICAO']) {
?>

<div class="modal-dialog ModalNotaVenda">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Adicionar Itens de Sobra</h4>
        </div>
        <div class="modal-body">
            <div id="step-6">
                <div class="form-group">
                    <div class="wrapper wrapper-white">
                        <form id="formCadItemSobra">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label>FAVOR INFORMAR CÓDIGO DE BARRA (EAN) E/OU CÓDIGO DO PRODUTO SERVIMED NOS CAMPOS INDICADOS E SUAS RESPECTIVAS QUANTIDADES.</label>
                                        <label>CÓDIGO DE BARRAS (EAN) PODERÁ SER INFORMADO ATRAVÉS DE LEITOR</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Cód. Barras (EAN)</label>
                                        <input readonly="true" type="text" placeholder="Cód. Barra" class="form-control" value="<?php echo $rowCliente['PRO_NU_CODIGO_BARRAS']; ?>" id="cod_barra" name="cod_barra" onchange="javascript:apresenta_dados_produto(1)">
                                    </div>
                                </div>                                                                                
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Cód. Produto</label>
                                        <input readonly="true" type="text" required data-errormessage-value-missing="Campo não pode ser deixado em branco" placeholder="Cód. Produto" class="form-control" size="10" id="co_produto" name="co_produto" value="<?php echo $co_produto; ?>" onKeyPress="fMascara('numero', event, this)" onchange="javascript:apresenta_dados_produto(2)">
                                    </div>
                                </div> 
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Informações do Produto</label>
                                        <input type="text" class="form-control" name="pro_no_descricao" id="pro_no_descricao" value="<?php echo $rowCliente['PRO_NO_DESCRICAO']; ?>" readonly="true">
                                    </div>
                                </div>    
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Qtd. Volume a ser coletado</label>
                                        <input type="number" placeholder="Volumme" class="form-control" id="volume" name="volume" value="<?php echo $volume; ?>" onchange="calcularQntUnitaria(this)">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Quantidade Unitária</label>
                                        <input readonly type="text" placeholder="Quantidade Unitária" class="form-control" id="qnt_unitaria" name="qnt_unitaria" value="<?php echo ($rowCliente['PRO_QT_CAIXA_MASTER'] * $volume); ?>">
                                    </div>
                                </div>                                                       
                                <div id="dadosItemNota">
                                </div>                                        
                                </br>                                                                       
                            </div>
                        </form> 
                        <div class="modal-footer" id="areaBotaoCadastro" >
                            <button data-dismiss="modal" class="btn btn-default" onclick="fecharAguarde();" type="button">Cancelar</button>       
                            <button class="btn btn-primary" type="button" onclick="javascript: editarItemOcorrencia();" id="btnSalvarNota">Editar</button>
                            <div id="msgCadNotaOrigem"></div>
                        </div> 
                    </div>
                </div>                                                                                                                    
            </div>             
        </div>                       
    </div>
</div>
<?php
    }
?>
<script type="text/javascript">

    function calcularQntUnitaria(volume){        
        var valorUnitario = <?php echo $rowCliente['PRO_QT_CAIXA_MASTER']; ?>;

        if(volume != null && valorUnitario != null){
            document.getElementById("qnt_unitaria").value = volume.value * valorUnitario;
        }
    }
</script> 