<?php
ini_set('memory_limit', '-1');
include_once( "../../../includes/mpdf60/mpdf.php");

include_once( "../../../includes/Dao/DaoSistema.class.php" );
include_once( "../../../includes/Dao/OcorrenciaDao.class.php" );
include_once( "../../../includes/Ocorrencia.class.php" );
include_once( "../../../includes/CalTipoOcorrencia.class.php" );
include_once( "../../../includes/Dao/TipoOcorrenciaDAO.class.php" );
include_once( "../../../includes/TipoDevolucao.class.php" );
include_once( "../../../includes/Dao/TipoDevolucaoDao.class.php" );
include_once( "../../../includes/Rota.class.php" );
include_once( "../../../includes/Dao/RotaDao.class.php" );
include_once( "../../../includes/Romaneio.class.php" );
include_once( "../../../includes/Dao/RomaneioDao.class.php" );
include_once( "../../../includes/OcorrenciaRomaneio.class.php" );
include_once( "../../../includes/Dao/OcorrenciaRomaneioDao.class.php" );

include_once("DaoGlobal.class.php");
include_once("SistemaDao.class.php");
include_once("GlbSistema.class.php");
include_once("GlbSistemaPerfil.class.php");
include_once("GlbUsuario.class.php");
include_once("GlbColaborador.class.php");
include_once("GlbPessoa.class.php");
include_once("GlbEndereco.class.php");
include_once("GlbTelefone.class.php");
include_once("include_novo/GlbCentroDistribuicao.class.php");
include_once("include_novo/GlbTransportadora.class.php");
include_once("include_novo/GlbContatoTransportador.class.php");
include_once("include_novo/ContatoTransportadorDao.class.php");
include_once("GlbProgramaRotina.class.php");
include_once("GlbModuloPrograma.class.php");
include_once("GlbSistemaModulo.class.php");
include_once("PerfilUsuarioDao.class.php");
include_once("PerfilPermissaoDao.class.php");
include_once("integraSetSistemas.php");
include_once("include_novo/Tradutor.class.php");

$Tradutor = new Tradutor();

$integra = new integraSetSistemas("conf.xml");
$ObjUsuario = unserialize($integra->getSistemaIntegra());

$ObjRotinas = $ObjUsuario->getObjSistemaPerfil()->getObjRotina();

//echo '<pre>';
//print_r($_REQUEST);
//echo '</pre>';

extract($_REQUEST);

// assume $str esteja em UTF-8
$map = array(
	'á' => 'a',
	'à' => 'a',
	'ã' => 'a',
	'â' => 'a',
	'é' => 'e',
	'ê' => 'e',
	'í' => 'i',
	'ó' => 'o',
	'ô' => 'o',
	'õ' => 'o',
	'ú' => 'u',
	'ç' => 'c',
	'Á' => 'A',
	'À' => 'A',
	'Ã' => 'A',
	'Â' => 'A',
	'É' => 'E',
	'Ê' => 'E',
	'Í' => 'I',
	'Ó' => 'O',
	'Ô' => 'O',
	'Õ' => 'O',
	'Ú' => 'U',
	'Ç' => 'C'
);

$emissaoDATA = date("d/m/Y");

$mpdf = new mPDF('utf-8', 'A4-L');

$Ocorrencia = new Ocorrencia();
$OcorrenciaDao = new OcorrenciaDao();

$Ocorrencia->setCedis($ced_co_numero);
//$Ocorrencia->getObjRota()->setCodigoRota($rot_co_numero);
//echo $rot_co_numero;

$Romaneio = new Romaneio();	
$RomaneioDao = new RomaneioDao();

$OcorrenciaRomaneio = new OcorrenciaRomaneio();	
$OcorrenciaRomaneioDao = new OcorrenciaRomaneioDao();

$retorno = $OcorrenciaDao->consultaOcorrenciaRomaneio($Ocorrencia, $dataInicial, $dataFinal, $rot_co_numero);
 
if(count($retorno) > 0)
{
	
	//gerar numero romaneio
	$codRomaneio = $RomaneioDao->gerarCodigoOcorrencia();
	//preenche OBJ
	$Romaneio->setCodigo($codRomaneio);
	$Romaneio->setUsuarioAbertura($ObjUsuario->getUsuarioCodigo());
	//inserir o registro na dev_romaneio
	$resRomaneio = $RomaneioDao->inserir($Romaneio);
	
	if ($resRomaneio)
	{
		$html = '
				<html xmlns="http://www.w3.org/1999/xhtml">
				<head>
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
				<title>Romaneio</title>
				<style>
				body, table {
					font-family: Arial, Verdana, Tahoma, Sans-Serif;
					font-size: 12px;
				}
				</style>
				</head>
					<body>
			<div class="table-responsive">			
				<table border="0" width="95%">
					<tr>
						<th width="200">
							<img src="../../../img/logoServimed.jpg" border="0">
						</th>
						<td align="center">
							<h2>Relatório - Envio de Devolucoes</h2>
							<br/>
							Data Emissão: '.date("d/m/Y").'
						</td>
						<td align="right"> 
							<barcode code="' . $codRomaneio . '" type="C39" size="0.6" height="3.0" />
							<br/>
							Código: '.$codRomaneio.' 
						</td>						
					</tr>					
				</table>			
				<table width="95%" border="1" style="border-collapse: collapse;">
					<thead>
						<tr>
							<th>Dt. Transpotador</th>
							<th>Protocolo</th>
							<th>Qnt. Volume</th>
							<th>Nota Origem</th>
							<th>Nota Devolução</th>
							<th>Cod. Cliente</th>
							<th>CD</th>
							<th>Rota</th>
							<th>Tipo</th>
							<th>Motivo Devolução</th>
						</tr>
					</thead>                               
					<tbody>
					';
					
						while ($dados = oci_fetch_object($retorno)) 
						{
							$qtdNotas++;
							$prot1 = $dados->OCORRENCIA;
							if ($prot1 != $prot2){
								$volume = $dados->VOLUME;
								$prot2 = $prot1;
								$qtdVolume = $qtdVolume + $volume;
							}else{
								$volume = "";
							}					
							
							//busca notas de venda
							$retornoNF = $OcorrenciaDao->consultaOcorrenciaNF($dados->OCORRENCIA);							
							
							//preenche OBJ
							$OcorrenciaRomaneio->setCodigoOcorrencia($dados->OCORRENCIA);
							$OcorrenciaRomaneio->setCodigoRomaneio($codRomaneio);
							//inserir registro na dev_ocorrencia_romaneio
							$resOcoRomaneio = $OcorrenciaRomaneioDao->inserirRomaneio($OcorrenciaRomaneio);
							
							$html .= '<tr>';
								$html .= '<td>'. $dados->DATA_TRANSPORTADOR .'</td>';
								$html .= '<td>'. $dados->OCORRENCIA .'</td>';
								$html .= '<td>'. $volume .'</td>';
								$html .= '<td>';
									while ($dadosNF = oci_fetch_object($retornoNF)) {
										$html .= $dadosNF->NTO_NU_SERIE . '/' . $dadosNF->NTO_NU_NOTA . '<br/>';
									}
								$html .= '</td>';
								$html .= '<td>'. $dados->NOTA_DEVOLUCAO .'</td>';
								$html .= '<td>'. $dados->CODIGO_CLIENTE .'</td>';
								$html .= '<td>'. $dados->CED_NO_CENTRO .'</td>';
								$html .= '<td>'. $dados->ROTA .'</td>';
								$html .= '<td>'. $dados->TID_CO_DESCRICAO .'</td>';
								$html .= '<td>'. utf8_encode(strtr($dados->TOC_NO_DESCRICAO, $map)) .'</td>';
							$html .= '</tr>';
							
							
							
						}
		$html .= '
					</tbody>
				</table>
				<table width="95%" CELLPADDING=5>
					<tr>
						<td>Qtd. Total de Notas: '.$qtdNotas.'</td>
						<td></td>
						<td align="right">Qtd. Total de Volumes: '.$qtdVolume.'</td>
					</tr>					
					<tr>
						<td colspan="3"></td>
					</tr>
					<tr>
						<td colspan="3"></td>
					</tr>					
					<tr>
						<td colspan="3"></td>
					</tr>					
					<tr>
						<td align="right">______________________________________</td>
						<td align="right">______________________________________</td>
						<td align="right">______________________________________</td>
					</tr>
					<tr>
						<td align="right">Resp. Transportador</td>
						<td align="right">Ass. Motorista</td>
						<td align="right">Ass. Recebedor CD</td>
					</tr>
					<tr>
						<td colspan="3"></td>
					</tr>				
					<tr>
						<td colspan="3"></td>
					</tr>					
					<tr>
						<td align="right">______________________________</td>
						<td align="right">______________________________</td>
						<td align="right">______________________________</td>
					</tr>
					<tr>
						<td align="right">Data Envio</td>
						<td align="right">Placa Veiculo</td>
						<td align="right"	>Data Recebimento</td>
					</tr>										
				</table>				
			</div>
			</body>
			</html>';
		//echo $html;
		$mpdf->WriteHTML($html);
		$mpdf->Output("romaneio_{$codRomaneio}_{$emissaoDATA}.pdf", "D");
	
	}
}
	?>
    <script language="JavaScript">
        $(function () {
            datatables.init();
        });
    </script>