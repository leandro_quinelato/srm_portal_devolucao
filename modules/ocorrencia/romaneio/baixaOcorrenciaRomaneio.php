<!DOCTYPE html>
<?php
include_once( "../../../includes/Dao/DaoSistema.class.php" );
include_once( "../../../includes/Dao/OcorrenciaDao.class.php" );
include_once( "../../../includes/CalTipoOcorrencia.class.php" );
include_once( "../../../includes/Dao/TipoOcorrenciaDAO.class.php" );
include_once( "../../../includes/TipoDevolucao.class.php" );
include_once( "../../../includes/Dao/TipoDevolucaoDao.class.php" );
include_once( "../../../includes/Rota.class.php" );
include_once( "../../../includes/Dao/RotaDao.class.php" );

include_once("DaoGlobal.class.php");
include_once("SistemaDao.class.php");
include_once("GlbSistema.class.php");
include_once("GlbSistemaPerfil.class.php");
include_once("GlbUsuario.class.php");
include_once("GlbColaborador.class.php");
include_once("GlbPessoa.class.php");
include_once("GlbEndereco.class.php");
include_once("GlbTelefone.class.php");
include_once("include_novo/GlbCentroDistribuicao.class.php");
include_once("include_novo/GlbTransportadora.class.php");
include_once("include_novo/GlbContatoTransportador.class.php");
include_once("include_novo/ContatoTransportadorDao.class.php");
include_once("GlbProgramaRotina.class.php");
include_once("GlbModuloPrograma.class.php");
include_once("GlbSistemaModulo.class.php");
include_once("PerfilUsuarioDao.class.php");
include_once("PerfilPermissaoDao.class.php");
include_once("integraSetSistemas.php");

$integra = new integraSetSistemas("conf.xml");
$ObjUsuario = unserialize($integra->getSistemaIntegra());

//echo "<pre>";
//print_r($ObjUsuario);
//echo "</pre>";

$OcorrenciaDao = new OcorrenciaDao();

?>
<html lang="en">
    <head>        
        <!-- meta section -->
        <title>Ocorrências - Romaneio</title>
<?php include("../../../library/head.php"); ?>
        <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/libRomaneioOcorrencia.js"></script>

		<script type="text/javascript">
			function marcarTodos(marcar){
				var itens = document.getElementsByName('ocorrencias[]');
				
				var i = 0;
				for(i=0; i<itens.length;i++){
					itens[i].checked = marcar;
				}

			}
		</script>
		
    </head>
    <body>
        <!-- set loading layer -->
        <div class="dev-page-loading preloader"></div>
        <!-- ./set loading layer -->

        <!-- page wrapper -->
        <div class="dev-page">

            <!-- page header -->    
<?php include("../../../library/topo.php"); ?>
            <!-- ./page header -->

            <!-- page container -->
            <div class="dev-page-container">

                <!-- page sidebar -->
<?php include("../../../library/menu.php"); ?>
                <!-- ./page sidebar -->

                <!-- page content -->
                <div class="dev-page-content">                    
                    <!-- page content container -->
                    <div class="container">

                        <!-- page title -->
                        <div class="page-title">
                            <h1>Romaneio Recebimento</h1>

                            <ul class="breadcrumb">
                                <li><a href="#">Ocorrências</a></li>
                                <li>Romaneio Recebimento</li>
                            </ul>
                        </div>                        
                        <!-- ./page title -->

						
                        <!-- datatables plugin -->
                        <div class="wrapper wrapper-white">
                            <form id="formFiltroConsulta">
                                <div class="row">
                                    <div class="col-md-2 col-sm-3 col-xs-3">
                                        <div class="form-group">
                                            <label>Romaneio</label>
                                            <input type="text" class="form-control" name="codRomaneio" id="codRomaneio" onKeyPress="return consultarComEnter(event);">
                                        </div>
                                    </div>
                                </div>
                            </form>    
                            <div class="row">
                                <div class="col-md-2">
                                    <button class="btn btn-primary" onclick="javascript: consultarRomaneio();" id="botaoConsultar">Consultar</button>
                                </div>
                            </div>
                            <div class="form-group">

                            </div>
                            <div id="retornoConsOcorrencia">

                            </div>                        
                            <!-- ./datatables plugin -->


                            <!-- Copyright -->
                            <?php include("../../../library/copyright.php"); ?>
                            <!-- ./Copyright -->

                        </div>
                        <!-- ./page content container -->                                       
                    </div>
                    <!-- ./page content -->                                                
                </div>  
                <!-- ./page container -->


                <!-- page footer -->    
                <?php include("../../../library/footer.php"); ?>
                <!-- ./page footer -->

                <!-- page search -->

                <!-- page search -->            
            </div>
            <!-- ./page wrapper -->
     
            <!-- javascripts -->
            <?php include("../../../library/rodape.php"); ?>

            <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js"></script>
            <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/plugins/datatables/jquery.dataTables.min.js"></script>
            <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/plugins/smartwizard/jquery.smartWizard.js"></script>
            <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/plugins/sortable/sortable.min.js"></script>


            <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/plugins/spectrum/spectrum.js"></script>
            <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/plugins/tags-input/jquery.tagsinput.min.js"></script>                

            <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/dev-settings.js"></script>
            <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/demo.js"></script>
        <script type="text/javascript">
            
            function testeBarra(e){
//                    console.log('which: ' + e.which);
//                    console.log('keycode: ' + e.keyCode);
//                    e.preventDefault();
                if ( (e.which === 77) || (e.keyCode === 77) || (e.which === 13) || (e.keyCode === 13)) {
//                    console.log('ALOOOOOOOOOOOOOOOO');
                    $('#botaoConsultar').click();

                }
            }
           
           
            $(function(){
                  $("#conCodOcorrencia").focus();
            });
			
			
			//seta o fuco no campo para bipar ao abrir a pagina.
			document.getElementById('codRomaneio').focus();
        </script>
            <!-- ./javascripts -->



    </body>
</html>