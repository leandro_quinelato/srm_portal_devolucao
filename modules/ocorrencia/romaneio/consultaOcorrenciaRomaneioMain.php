<?php
include_once( "../../../includes/Dao/DaoSistema.class.php" );
include_once( "../../../includes/Dao/OcorrenciaDao.class.php" );
include_once( "../../../includes/Ocorrencia.class.php" );
include_once( "../../../includes/CalTipoOcorrencia.class.php" );
include_once( "../../../includes/Dao/TipoOcorrenciaDAO.class.php" );
include_once( "../../../includes/TipoDevolucao.class.php" );
include_once( "../../../includes/Dao/TipoDevolucaoDao.class.php" );
include_once( "../../../includes/Rota.class.php" );
include_once( "../../../includes/Dao/RotaDao.class.php" );


include_once( "../../../includes/Status.class.php" );
include_once( "../../../includes/Dao/StatusDao.class.php" );

include_once( "../../../includes/Passo.class.php" );
include_once( "../../../includes/Dao/PassoDao.class.php" );

include_once("DaoGlobal.class.php");
include_once("SistemaDao.class.php");
include_once("GlbSistema.class.php");
include_once("GlbSistemaPerfil.class.php");
include_once("GlbUsuario.class.php");
include_once("GlbColaborador.class.php");
include_once("GlbPessoa.class.php");
include_once("GlbEndereco.class.php");
include_once("GlbTelefone.class.php");
include_once("include_novo/GlbCentroDistribuicao.class.php");
include_once("include_novo/GlbTransportadora.class.php");
include_once("include_novo/GlbContatoTransportador.class.php");
include_once("include_novo/ContatoTransportadorDao.class.php");
include_once("GlbProgramaRotina.class.php");
include_once("GlbModuloPrograma.class.php");
include_once("GlbSistemaModulo.class.php");
include_once("PerfilUsuarioDao.class.php");
include_once("PerfilPermissaoDao.class.php");
include_once("integraSetSistemas.php");
$integra = new integraSetSistemas("conf.xml");
$ObjUsuario = unserialize($integra->getSistemaIntegra());
$ObjRotinas = $ObjUsuario->getObjSistemaPerfil()->getObjRotina();

include_once("include_novo/Tradutor.class.php");

$Tradutor = new Tradutor();


//echo '<pre>';
//print_r($_REQUEST);
//echo '</pre>';

/*

  [conCodOcorrencia] =>
  [dataInicial] => 04/11/2016
  [dataFinal] => 04/11/2016
  [ced_co_numero] => 48
  [rot_co_numero] => 2D
  [acao] => LISOCO

 */

extract($_REQUEST);

if ($acao == "LISOCO") {

    $Ocorrencia = new Ocorrencia();
    $OcorrenciaDao = new OcorrenciaDao();

    $Ocorrencia->setCedis($ced_co_numero);
    //$Ocorrencia->getObjRota()->setCodigoRota($rot_co_numero);
	//echo $rot_co_numero;

    $retorno = $OcorrenciaDao->consultaOcorrenciaRomaneio($Ocorrencia, $dataInicial, $dataFinal, $rot_co_numero);
    ?>

    <div class="table-responsive">
        <table class="table table-bordered table-striped table-sortable">
            <thead>
                <tr>
					<th>Dt. Transpotador</th>
                    <th>Protocolo</th>
					<th>Qnt. Volume</th>
                    <th>Nota Origem</th>
					<th>Nota Devolução</th>
					<th>Cod. Cliente</th>
					<th>CD</th>
					<th>Rota</th>
					<th>Tipo</th>
					<th>Motivo Devolução</th>
                </tr>
            </thead>                               
            <tbody>

                <?php
                while ($dados = oci_fetch_object($retorno)) {
					$prot1 = $dados->OCORRENCIA;
					if ($prot1 != $prot2){
						$volume = $dados->VOLUME;
						$prot2 = $prot1;
					}else{
						$volume = "";
					}
                    ?>
                    <tr>
						<td><?php echo $dados->DATA_TRANSPORTADOR; ?></td>
                        <td><?php echo $dados->OCORRENCIA; ?></td>
						<td><?php echo $volume; ?></td>
                        <td>
						<?php 
						//echo $dados->NOTA_ORIGEM;
						$retornoNF = $OcorrenciaDao->consultaOcorrenciaNF($dados->OCORRENCIA);
						while ($dadosNF = oci_fetch_object($retornoNF)) {
							echo $dadosNF->NTO_NU_SERIE . "/" . $dadosNF->NTO_NU_NOTA . "<br/>";
						}
						?>
						</td>
						<td><?php echo $dados->NOTA_DEVOLUCAO; ?></td>
						<td><?php echo $dados->CODIGO_CLIENTE; ?></td>
						<td><?php echo $dados->CED_NO_CENTRO; ?></td>
						<td><?php echo $dados->ROTA; ?></td>
						<td><?php echo $dados->TID_CO_DESCRICAO; ?></td>
                        <td><?php echo utf8_encode($dados->TOC_NO_DESCRICAO); ?></td>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
    </div>
	
	<div class="row">
		<div class="col-md-2">
			<button class="btn btn-primary" onclick="javascript: gerarDocRomaneio('<?=$dataInicial;?>', '<?=$dataFinal;?>', '<?=$ced_co_numero;?>', '<?=$rot_co_numero;?>');" id="botaoConsultar">Gerar Romaneio</button>
		</div>
	</div>
	
    <script language="JavaScript">
        $(function () {
            datatables.init();
        });
    </script>
    <?php
}
?>