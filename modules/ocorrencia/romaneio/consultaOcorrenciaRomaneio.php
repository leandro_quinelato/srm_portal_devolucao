<!DOCTYPE html>
<?php
include_once( "../../../includes/Dao/DaoSistema.class.php" );
include_once( "../../../includes/Dao/OcorrenciaDao.class.php" );
include_once( "../../../includes/CalTipoOcorrencia.class.php" );
include_once( "../../../includes/Dao/TipoOcorrenciaDAO.class.php" );
include_once( "../../../includes/TipoDevolucao.class.php" );
include_once( "../../../includes/Dao/TipoDevolucaoDao.class.php" );
include_once( "../../../includes/Rota.class.php" );
include_once( "../../../includes/Dao/RotaDao.class.php" );

include_once("DaoGlobal.class.php");
include_once("SistemaDao.class.php");
include_once("GlbSistema.class.php");
include_once("GlbSistemaPerfil.class.php");
include_once("GlbUsuario.class.php");
include_once("GlbColaborador.class.php");
include_once("GlbPessoa.class.php");
include_once("GlbEndereco.class.php");
include_once("GlbTelefone.class.php");
include_once("include_novo/GlbCentroDistribuicao.class.php");
include_once("include_novo/GlbTransportadora.class.php");
include_once("include_novo/GlbContatoTransportador.class.php");
include_once("include_novo/ContatoTransportadorDao.class.php");
include_once("GlbProgramaRotina.class.php");
include_once("GlbModuloPrograma.class.php");
include_once("GlbSistemaModulo.class.php");
include_once("PerfilUsuarioDao.class.php");
include_once("PerfilPermissaoDao.class.php");
include_once("integraSetSistemas.php");
$integra = new integraSetSistemas("conf.xml");
$ObjUsuario = unserialize($integra->getSistemaIntegra());
$ObjRotinas = $ObjUsuario->getObjSistemaPerfil()->getObjRotina();

//echo "<pre>";
//print_r($ObjUsuario);
//echo "</pre>";

$RotaDao = new RotaDao();
$OcorrenciaDao = new OcorrenciaDao();
/* FILTRO ROTA */
if ($ObjUsuario->getUsuarioTipo() == "VEN") {
    $VendedorUsuario = $ObjUsuario->getObjVendedor();
    $ObjVendedor = new GlbVendedor();
    if ($VendedorUsuario[0]) {
        $ObjVendedor->setCodigo($VendedorUsuario[0]->getCodigo());
        $ObjVendedor->setTipo($VendedorUsuario[0]->getTipo());
    }
    $resultRota = $RotaDao->consultarRotaVendedor($ObjVendedor);
} else if ($ObjUsuario->getUsuarioTipo() == "CLI") {
    $resultRota = $OcorrenciaDao->consultaCliente($ObjUsuario->getObjCliente()->getClienteCodigo());
    //  OCIFetchInto($resultCliente, $rowCliente, OCI_ASSOC);
} else if($ObjUsuario->getUsuarioTipo() == "TRA"){
    $resultRota  = $RotaDao->consultaUsuRota($ObjUsuario->getUsuarioCodigo());
}else{
    $resultRota = $RotaDao->listar();
}

/* Gerar codigo da ocorrencia */
//$doc_co_numero = $OcorrenciaDao->gerarCodigoOcorrencia($ObjUsuario->getUsuarioCodigo());

/* FILTRO MOTIVO DEVOLUCAO */
$TipoOcorrenciaDao = new TipoOcorrenciaDAO();
$TipoOcorrencias = new CalTipoOcorrencia();
$TipoOcorrencias = $TipoOcorrenciaDao->listarTipoOcorrencia($TipoOcorrencias);


/* FILTRO CEDIS */
$GlbCentroDistribuicao = new GlbCentroDistribuicao();
$GlbCentroDistribuicao = $OcorrenciaDao->filtroCentroDis();

?>
<html lang="en">
    <head>        
        <!-- meta section -->
        <title>Ocorrências - Romaneio</title>
<?php include("../../../library/head.php"); ?>
        <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/libRomaneioOcorrencia.js"></script>

    </head>
    <body>
        <!-- set loading layer -->
        <div class="dev-page-loading preloader"></div>
        <!-- ./set loading layer -->

        <!-- page wrapper -->
        <div class="dev-page">

            <!-- page header -->    
<?php include("../../../library/topo.php"); ?>
            <!-- ./page header -->

            <!-- page container -->
            <div class="dev-page-container">

                <!-- page sidebar -->
<?php include("../../../library/menu.php"); ?>
                <!-- ./page sidebar -->

                <!-- page content -->
                <div class="dev-page-content">                    
                    <!-- page content container -->
                    <div class="container">

                        <!-- page title -->
                        <div class="page-title">
                            <h1>Romaneio Expedição</h1>

                            <ul class="breadcrumb">
                                <li><a href="#">Ocorrências</a></li>
                                <li>Romaneio Expedição</li>
                            </ul>
                        </div>                        
                        <!-- ./page title -->



                        <!-- datatables plugin -->
                        <div class="wrapper wrapper-white">
                            <form id="formFiltroConsulta">
                                <div class="row">
                                    <div class="col-md-2 col-sm-3 col-xs-3">
                                        <div class="form-group">
                                            <label>Início</label>                            
                                            <input type="text" class="form-control datepicker" onKeyPress="fMascara('mesano', event, this)" name="dataInicial" id="dataInicial">
                                        </div> 
                                    </div>                               
                                    <div class="col-md-2 col-sm-3 col-xs-3">
                                        <div class="form-group">
                                            <label>Fim</label>                            
                                            <input type="text" class="form-control datepicker" onKeyPress="fMascara('mesano', event, this)" name="dataFinal" id="dataFinal">
                                        </div> 
                                    </div>								

                                    <div class="col-md-3 col-sm-6 col-xs-6">                        
                                        <div class="form-group">
                                            <label>CD</label>
                                            <select class="form-control selectpicker" id="ced_co_numero" name="ced_co_numero">
                                                <option value="TOD"></option>
												<?php
												foreach ($GlbCentroDistribuicao as $ObjCedis) {
												?>
                                                    <option value="<?php echo $ObjCedis->getCodigo(); ?>"><?php echo $ObjCedis->getNome(); ?></option>
												<?php
                                                }
                                                ?>    
                                            </select>
                                        </div>  

										<?php
										//trata rotas por perfil usuario
										while (OCIFetchInto($resultRota, $rowRota, OCI_ASSOC)) {
											$vetRotas[] = "|".$rowRota['ROT_CO_NUMERO']."|";
										}
											$txtInRotas = implode(",", $vetRotas);
										?>
										<input type="hidden" name="rot_co_numero" value="<?=$txtInRotas;?>">
										
                                    </div>  									
									
                                </div>
                            </form>    
                            <div class="row">
                                <div class="col-md-2">
                                    <button class="btn btn-primary" onclick="javascript: consultarOcorrenciaRomaneio();" id="botaoConsultar">Consultar</button>
                                </div>
                            </div>
                            <div class="form-group">

                            </div>
                            <div id="retornoConsOcorrencia">


                            </div>                        
                            <!-- ./datatables plugin -->


                            <!-- Copyright -->
                            <?php include("../../../library/copyright.php"); ?>
                            <!-- ./Copyright -->

                        </div>
                        <!-- ./page content container -->                                       
                    </div>
                    <!-- ./page content -->                                                
                </div>  
                <!-- ./page container -->


                <!-- page footer -->    
                <?php include("../../../library/footer.php"); ?>
                <!-- ./page footer -->

                <!-- page search -->

                <!-- page search -->            
            </div>
            <!-- ./page wrapper -->
     
            <!-- javascripts -->
            <?php include("../../../library/rodape.php"); ?>

            <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js"></script>
            <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/plugins/datatables/jquery.dataTables.min.js"></script>
            <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/plugins/smartwizard/jquery.smartWizard.js"></script>
            <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/plugins/sortable/sortable.min.js"></script>


            <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/plugins/spectrum/spectrum.js"></script>
            <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/plugins/tags-input/jquery.tagsinput.min.js"></script>                

            <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/dev-settings.js"></script>
            <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/demo.js"></script>
        <script type="text/javascript">
            
            function testeBarra(e){
//                    console.log('which: ' + e.which);
//                    console.log('keycode: ' + e.keyCode);
//                    e.preventDefault();
                if ( (e.which === 77) || (e.keyCode === 77) || (e.which === 13) || (e.keyCode === 13)) {
//                    console.log('ALOOOOOOOOOOOOOOOO');
                    $('#botaoConsultar').click();

                }
            }
           
           
            $(function(){
                  $("#conCodOcorrencia").focus();
            });
        </script>
            <!-- ./javascripts -->



    </body>
</html>