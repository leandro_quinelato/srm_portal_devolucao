<?php
session_start();
ini_set('display_errors', 1);
include_once( "../../../includes/Dao/DaoSistema.class.php" );
include_once( "../../../includes/CalDepartamento.class.php" );
include_once( "../../../includes/CalTipoOcorrencia.class.php" );
include_once( "../../../includes/Dao/TipoOcorrenciaDAO.class.php" );
include_once( "../../../includes/Ocorrencia.class.php" );
include_once( "../../../includes/Dao/OcorrenciaDao.class.php" );
include_once( "../../../includes/TipoDevolucao.class.php" );
include_once( "../../../includes/Dao/TipoDevolucaoDao.class.php" );

include_once( "../../../includes/DevNotaOrigemItem.class.php" );
include_once( "../../../includes/Dao/DevNotaOrigemItemDao.class.php" );

include_once( "../../../includes/DevParametros.class.php" );
include_once( "../../../includes/Dao/ParametroDao.class.php" );

include_once( "../../../includes/Rota.class.php" );
include_once( "../../../includes/Dao/RotaDao.class.php" );

include_once( "../../../includes/OcorrenciaPasso.class.php" );
include_once( "../../../includes/Dao/OcorrenciaPassoDao.class.php" );

include_once( "../../../includes/OcorrenciaStatus.class.php" );
include_once( "../../../includes/Dao/OcorrenciaStatusDao.class.php" );

include_once( "../../../includes/OcorrenciaArquivos.class.php" );
include_once( "../../../includes/Dao/OcorrenciaArquivosDao.class.php" );

include_once( "../../utilitario/email/emailPortalDevolucao.class.php" );

include_once( "../../../includes/Dao/ClienteEspecialNotaDao.class.php" );

include_once( "../../../includes/DevParametrosValorDevolucao.class.php" );
include_once( "../../../includes/Dao/ParametroValorDevolucaoDao.class.php" );

include_once("DaoGlobal.class.php");
include_once("SistemaDao.class.php");
include_once("GlbSistema.class.php");
include_once("GlbSistemaPerfil.class.php");
include_once("GlbUsuario.class.php");
include_once("GlbColaborador.class.php");
include_once("GlbPessoa.class.php");
include_once("GlbEndereco.class.php");
include_once("GlbTelefone.class.php");
include_once("GlbProgramaRotina.class.php");
include_once("GlbModuloPrograma.class.php");
include_once("GlbSistemaModulo.class.php");
include_once("PerfilUsuarioDao.class.php");
include_once("PerfilPermissaoDao.class.php");
include_once("integraSetSistemas.php");
include_once("class.phpmailer.php");
include_once("class.smtp.php");

include_once("include_novo/Tradutor.class.php");

$Tradutor = new Tradutor();
$integra = new integraSetSistemas("conf.xml");
$ObjUsuario = unserialize($integra->getSistemaIntegra());
//$ObjRotinas = $ObjUsuario->getObjSistemaPerfil()->getObjRotina();

$Ocorrencia = new Ocorrencia();
$DevNotaOrigem = new DevNotaOrigem();
$OcorrenciaDao = new OcorrenciaDao();


//echo "<pre>";
//print_r($Objusuario);
//echo "</pre><br><br><br>";
//
//echo "<pre>";
//print_r($_REQUEST);
//echo "</pre>";
//$tipoDevolucaoInd = $_REQUEST['tipoDevolucaoInd'];
//$codOcorrencia    = $_REQUEST['codOcorrencia'];
//$motivoDevolucao  = $_REQUEST['motivoDevolucao'];
//$cli_co_numero    = $_REQUEST['cli_co_numero'];
//$rot_co_numero    = $_REQUEST['rot_co_numero'];
//$tipoDevolucao    = $_REQUEST['tipoDevolucao'];
//$serieNota        = $_REQUEST['serieNota'];
//$numNota          = $_REQUEST['numNota'];
//$dataNota         = $_REQUEST['dataNota'];
//
//
//$notaCodItem      = $_REQUEST['notaCodItem'];
//die();
extract($_REQUEST);
/*
 * verificar se a ocorrencia é integral ou parcial
 */
if ($acao == "ADDNOTA") {

    /*
     * Preencher dados comuns(parcial/integral) da devolucao
     */

    $DevNotaOrigemItemDao = new DevNotaOrigemItemDao();

    $Ocorrencia->setCodigoDevolucao($codOcorrencia);
    $Ocorrencia->getObjRota()->setCodigoRota($rot_co_numero);
    $Ocorrencia->getTipoOcorrencia()->setCodTipoOcorrencia($motivoDevolucao);
    $Ocorrencia->setTipoTipoDevolucao($tipoDevolucao);
    $Ocorrencia->getGlbCliente()->setClienteCodigo(trim($cli_co_numero));
    $Ocorrencia->getTipoOcorrencia()->setDepartamentoTipoOcorrencia(29);
    $Ocorrencia->setCodigoCadastranteDevolucao($ObjUsuario->getUsuarioCodigo());
    $Ocorrencia->setTipoCadastranteDevolucao($ObjUsuario->getUsuarioTipo()); // JA ESTA OK 23/06/2016 --


    if ($ObjUsuario->getUsuarioTipo() == "CLI") {
        $Ocorrencia->setDeptContatoClienteDevolucao(strtoupper("CLIENTE"));
        $Ocorrencia->setEmailCadDevolucao($ObjUsuario->getUsuarioUsername());
    } else if ($ObjUsuario->getUsuarioTipo() == "INT") {
        
        if($ObjUsuario->getObjSistemaPerfil()->getNomeSistemaPerfil() == 'CentralSolucao'){
            $Ocorrencia->setTipoCadastranteDevolucao('CSL');
            $Ocorrencia->setDeptContatoClienteDevolucao(strtoupper("CENTRAL SOLUCOES"));       
        }else{
            $Ocorrencia->setDeptContatoClienteDevolucao(strtoupper("DEVOLUCAO"));
        }
    //        $Ocorrencia->setDeptContatoClienteDevolucao(strtoupper("INTERNO"));
    //        $ObjColaborador = $ObjUsuario->getObjColaborador(); 
        $Ocorrencia->setEmailCadDevolucao($ObjUsuario->getObjColaborador()->getPessoaEmail());
    } else if ($ObjUsuario->getUsuarioTipo() == "VEN") {
        
        if($ObjUsuario->getObjSistemaPerfil()->getNomeSistemaPerfil() == 'Gerente'){
            $Ocorrencia->setTipoCadastranteDevolucao('GER');
            $Ocorrencia->setDeptContatoClienteDevolucao(strtoupper("GERENTE"));       
        }else if($ObjUsuario->getObjSistemaPerfil()->getNomeSistemaPerfil() == 'Representante'){
            $Ocorrencia->setTipoCadastranteDevolucao('REP');
            $Ocorrencia->setDeptContatoClienteDevolucao(strtoupper("REPRESENTANTE"));       
        }else if($ObjUsuario->getObjSistemaPerfil()->getNomeSistemaPerfil() == 'Consultor'){
            $Ocorrencia->setTipoCadastranteDevolucao('CON');
            $Ocorrencia->setDeptContatoClienteDevolucao(strtoupper("CONSULTOR"));       
        }
        
    //        $Ocorrencia->setDeptContatoClienteDevolucao(strtoupper("VENDEDOR"));
        
        
        $Ocorrencia->setEmailCadDevolucao($ObjUsuario->getUsuarioUsername());
    } else if ($ObjUsuario->getUsuarioTipo() == "TRA") { //ajustarrr
        $Ocorrencia->setDeptContatoClienteDevolucao(strtoupper("TRANSPORTADORA"));  // AMARAL   
        $Ocorrencia->setEmailCadDevolucao($ObjUsuario->getUsuarioUsername()); //AMARAL
    }

    $Ocorrencia->setDescricaoDevolucao(trim($doc_tx_descricao));
    //$DevOcorrencia->setContatoClienteDevolucao(strtoupper($doc_no_contato));    //não esta no processo novo
    //INFORMAR QUE O STATUS NESSE PONTO É NÃO: CADASTRO NAO FINALIZADO (14)
    $Ocorrencia->setStatusOcorrencia(14);
    /* Registrar o status na tabela de Ocorrencia Status */
    $OcorrenciaStatus = new OcorrenciaStatus();
    $OcorrenciaStatus->setCodigoOcorrencia($Ocorrencia->getCodigoDevolucao());
    $OcorrenciaStatus->setUsuario($ObjUsuario->getUsuarioCodigo());
    $OcorrenciaStatus->setCodStatus($Ocorrencia->getStatusOcorrencia());
    $OcorrenciaStatusDao = new OcorrenciaStatusDao();
    $OcorrenciaStatusDao->registrarSatus($OcorrenciaStatus);


    if ($tipoDevolucaoInd == "I") {

        /*         * * carregar a TABELA DEV_OCORRENCIA E DEV_NOTAORIGEM** */
        $dadosNotaVenda = $OcorrenciaDao->consultaNotaVenda($cli_co_numero, $serieNota, $numNota, $dataNota);

        $DevNotaOrigem->setSerie($serieNota);
        $DevNotaOrigem->setNota($numNota);
        $DevNotaOrigem->setData($dataNota);

        $DevNotaOrigem->setCodCliente($cli_co_numero);
        $DevNotaOrigem->setOrigemVenda($dadosNotaVenda->TPORIGVENDA);
        $DevNotaOrigem->setCentroDist($dadosNotaVenda->CD);
        $DevNotaOrigem->setValorTotDevolucao($dadosNotaVenda->VLTOTDOCUM);
        $DevNotaOrigem->setDivisao($dadosNotaVenda->INDICADOR);
        $Ocorrencia->setDevNotaOrigem($DevNotaOrigem);


        $Ocorrencia->setValorDevolucao($dadosNotaVenda->VLTOTDOCUM); // ****** dev_ocorrencia
        $Ocorrencia->setCedis($dadosNotaVenda->CD);
        $Ocorrencia->setDivisao($dadosNotaVenda->INDICADOR);

        if ($OcorrenciaDao->verificaNotacadastrada($codOcorrencia)) {
            if (!$OcorrenciaDao->excluirOcorrencia($Ocorrencia)) {
                die("ERRO OCORRENCIA");
            }else if (!$OcorrenciaDao->inserir($Ocorrencia)) {
                die("ERRO OCORRENCIA");
            }
        } else {//atualiza
            if (!$OcorrenciaDao->alterar($Ocorrencia)) {

                die("ERRO ALTERAR");
            }
        }

        if (!$OcorrenciaDao->inserirNotaOrigem($Ocorrencia)) {
    //        die("<center><strong class='msmerro'>Desculpe, erro ao Inserir Nota </strong></center>");
            die("ERRO INSERIR NOTA");
        }
        $qtdItensDevolucao = 0;
        /*         * * carregar a TABELA DEV_NOTAORIGEM_ITEM** */
        for ($i = 0; count($notaCodItem) > $i; $i++) {

            $DevNotaOrigemItem = new DevNotaOrigemItem();

            $DevNotaOrigemItem->setCodigoOcorrencia($codOcorrencia);
            $DevNotaOrigemItem->setSerie($serieNota);
            $DevNotaOrigemItem->setNumNota($numNota);
            $DevNotaOrigemItem->setDataNota($dataNota);
            $DevNotaOrigemItem->setCodCliente($cli_co_numero);
            $DevNotaOrigemItem->setCodProduto($notaCodItem[$i]);
            $DevNotaOrigemItem->setDevQuantidade($notaItemQtdDev[$i]);
            $DevNotaOrigemItem->setPrecoUnitario($ItemValorUnit[$i]);
            $DevNotaOrigemItem->setTotalBruto($ItemValorTotBruto[$i]);
            $DevNotaOrigemItem->setTotalDesconto($ItemValorDesc[$i]);
            $DevNotaOrigemItem->setTotalImposto($ItemValorImp[$i]);
            $DevNotaOrigemItem->setTotalLiquido($ItemValorTotLiq[$i]);
            $DevNotaOrigemItem->setNotaQuantidade($notaItemQtd[$i]);

            $qtdItensDevolucao += $notaItemQtdDev[$i]; // somar quantidade de itens que serão devolvidos no total

            $DevNotaOrigemItemDao->inserir($DevNotaOrigemItem);
        }

        if($qtdVolume == ""){
            $Ocorrencia->setQntdVolumeDevolucao(0);
        }else{
            $Ocorrencia->setQntdVolumeDevolucao($qtdVolume);
        }
        
        if (!$OcorrenciaDao->alterar($Ocorrencia)) {

            die("ERRO");
        }
    } else if ($tipoDevolucaoInd == "P") {

    //        echo "<pre>";
    //        print_r($_REQUEST);
    //        echo "</pre><br>Geovanni";

        $totalDevolucao = 0;

        for ($i = 0; count($notaCodItem) > $i; $i++) {


            $totalBrutoDevolucao = 0;
            $totalDescontoDevolucao = 0;
            $totalImpostoDevolucao = 0;
            $totalLiqDevolucaoItem = 0;


            if ($notaItemQtdDev[$i] != "") {
    //                echo "produto: " . $notaCodItem[$i] . " - Qtd itens nota: " . $notaItemQtd[$i];


                //obter desconto por item
                if ($ItemValorDesc[$i] > 0) {
                    $descontoUnidade = ($ItemValorDesc[$i] / $notaItemQtd[$i]);
                } else {
                    $descontoUnidade = 0;
                }

                //obter imposto por item
                if ($ItemValorImp[$i] > 0) {
                    $impostoUnidade = ($ItemValorImp[$i] / $notaItemQtd[$i]);
                } else {
                    $impostoUnidade = 0;
                }

                echo "<br>";
                echo "<br>";
                echo "<br>";
                echo 'Valor desconto unidade: ' . $descontoUnidade;
                echo "<br>";
                echo 'Valor imposto unidade: ' . $impostoUnidade;

                $DevNotaOrigemItem = new DevNotaOrigemItem();

                $DevNotaOrigemItem->setCodigoOcorrencia($codOcorrencia);
                $DevNotaOrigemItem->setSerie($serieNota);
                $DevNotaOrigemItem->setNumNota($numNota);
                $DevNotaOrigemItem->setDataNota($dataNota);
                $DevNotaOrigemItem->setCodCliente($cli_co_numero);
                $DevNotaOrigemItem->setCodProduto($notaCodItem[$i]);
                $DevNotaOrigemItem->setDevQuantidade($notaItemQtdDev[$i]);
                $DevNotaOrigemItem->setPrecoUnitario($ItemValorUnit[$i]);
                $DevNotaOrigemItem->setNotaQuantidade($notaItemQtd[$i]);

                $totalBrutoDevolucao = ($ItemValorUnit[$i] * $notaItemQtdDev[$i] );
                echo "<br>total bruto Devolucao: " . $totalBrutoDevolucao . "<br>";
                $DevNotaOrigemItem->setTotalBruto($totalBrutoDevolucao);

                $totalDescontoDevolucao = ( $descontoUnidade * $notaItemQtdDev[$i]);
                echo "<br>total desconto Devolucao: " . $totalDescontoDevolucao . "<br>";
                $DevNotaOrigemItem->setTotalDesconto($totalDescontoDevolucao);

                $totalImpostoDevolucao = ( $impostoUnidade * $notaItemQtdDev[$i]);
                echo "<br>total Imposto Devolucao: " . $totalImpostoDevolucao . "<br><br><br>";
                $DevNotaOrigemItem->setTotalImposto($totalImpostoDevolucao);


                $totalLiqDevolucaoItem = ((( $ItemValorUnit[$i] * $notaItemQtdDev[$i] ) - $totalDescontoDevolucao) + $totalImpostoDevolucao);
                echo "<br>liquido devolucao: " . $totalLiqDevolucaoItem . "<br><br><br>";
                $DevNotaOrigemItem->setTotalLiquido($totalLiqDevolucaoItem);

                $totalDevolucao+= $totalLiqDevolucaoItem;

                $qtdItensDevolucao += $notaItemQtdDev[$i]; // somar quantidade de itens que serão devolvidos no total
    //
                $DevNotaOrigemItemDao->inserir($DevNotaOrigemItem);
            } else {

                $DevNotaOrigemItem = new DevNotaOrigemItem();

                $DevNotaOrigemItem->setCodigoOcorrencia($codOcorrencia);
                $DevNotaOrigemItem->setSerie($serieNota);
                $DevNotaOrigemItem->setNumNota($numNota);
                $DevNotaOrigemItem->setDataNota($dataNota);
                $DevNotaOrigemItem->setCodCliente($cli_co_numero);
                $DevNotaOrigemItem->setCodProduto($notaCodItem[$i]);

                if ($DevNotaOrigemItemDao->verificaProdutoCadastrado($DevNotaOrigemItem)) {

                    $DevNotaOrigemItemDao->excluir($DevNotaOrigemItem);
    //                    echo '<br>DELETADO: '.$notaCodItem[$i] ;
                }
            }
        }

        echo "<br>Total devolucao: " . $totalDevolucao . "<br><br><br>";


        /*         * * carregar a TABELA DEV_OCORRENCIA E DEV_NOTAORIGEM** */
        $dadosNotaVenda = $OcorrenciaDao->consultaNotaVenda($cli_co_numero, $serieNota, $numNota, $dataNota);

        $DevNotaOrigem->setSerie($serieNota);
        $DevNotaOrigem->setNota($numNota);
        $DevNotaOrigem->setData($dataNota);
        $DevNotaOrigem->setCodCliente($cli_co_numero);
        $DevNotaOrigem->setOrigemVenda($dadosNotaVenda->TPORIGVENDA);
        $DevNotaOrigem->setCentroDist($dadosNotaVenda->CD);
        $DevNotaOrigem->setDivisao($dadosNotaVenda->INDICADOR);
        $DevNotaOrigem->setValorTotDevolucao($totalDevolucao);

        $Ocorrencia->setDevNotaOrigem($DevNotaOrigem);


        $Ocorrencia->setValorDevolucao($totalDevolucao); // ****** dev_ocorrencia
        $Ocorrencia->setCedis($dadosNotaVenda->CD);
        $Ocorrencia->setDivisao($dadosNotaVenda->INDICADOR);
        
        if($qtdVolume == ""){
            $Ocorrencia->setQntdVolumeDevolucao(0);
        }else{
            $Ocorrencia->setQntdVolumeDevolucao($qtdVolume);
        }


        if ($OcorrenciaDao->verificaOcocadastrada($codOcorrencia)) {
            if (!$OcorrenciaDao->inserir($Ocorrencia)) {
    //        die("<center><strong class='msmerro'>Desculpe, erro ao Inserir Ocorr&ecirc;ncia </strong></center>");
                die("ERRO");
            }
        } else {//atualiza
            if (!$OcorrenciaDao->alterar($Ocorrencia)) {

                die("ERRO");
            }
        }

        if (!$OcorrenciaDao->inserirNotaOrigem($Ocorrencia)) {
    //        die("<center><strong class='msmerro'>Desculpe, erro ao Inserir Nota </strong></center>");
            die("ERRO");
        }
    }
} else if ($acao == "LISTARNOTAS") {

    //echo "<pre>";
    //print_r($_REQUEST);
    //echo "</pre>";//die();
    $Ocorrencia = new Ocorrencia();
    $notas = new DevNotaOrigem();
    $Ocorrencia->setCodigoDevolucao($codOcorrencia);
    $notas = $OcorrenciaDao->consultaNotasDevolucao($Ocorrencia);
    $valorFinalDevolucao = 0;
	
	$ClienteEspecialNotaDao = new ClienteEspecialNotaDao();
	$clienteEspecialNota = $ClienteEspecialNotaDao->verificaClienteEspecialNota($cli_co_numero);

	//if ($clienteEspecialNota) {
	?>    
        <div class="row" id="areaCadastroPNota">
            <div class="col-md-2">
                <a class="btn btn-primary" onclick="javascript:abrirNotaOrigem();">Adicionar nota</a>
            </div>                                                                                  
        </div>
        </br>
        <?php
    //}
	if (count($notas) > 0) {
        ?>
        <div class="table-responsive">
            <table class="table table-bordered table-striped table-hover">
                <thead>
                    <tr>
                        <th></th>
                        <th></th>
                        <th>Série</th>
                        <th>Nota</th>
                        <th>Data Nota</th>
                        <th>Cliente</th> 
                        <th>Total Devolução</th>                        
                    </tr>

                </thead>                               
                <tbody>
                    <?php
                    foreach ($notas as $nota) {
                        ?>
                        <tr>
                            <td>
                                <button class="fa fa-edit" type="button" onclick="javascript: editarNotaCadastrada(<?php echo $nota->getSerie(); ?>, <?php echo $nota->getNota(); ?>, '<?php echo $nota->getData(); ?>')"></button>
                            </td>
                            <td>
                                <button class="fa fa-minus-square" type="button" onclick="javascript: deletaNotaOcorrencia(<?php echo $nota->getSerie(); ?>, <?php echo $nota->getNota(); ?>, '<?php echo $nota->getData(); ?>')"></button>
                            </td>                            
                            <td><?php echo $nota->getSerie(); ?></td>
                            <td><?php echo $nota->getNota(); ?></td>
                            <td><?php echo $nota->getData(); ?></td>
                            <td><?php echo $nota->getCodCliente(); ?></td>
                            <td><?php echo $Tradutor->formatar($nota->getValorTotDevolucao(), 'MOEDA'); ?></td>
                            
                        </tr>
                        <?php
                        $valorFinalDevolucao += $nota->getValorTotDevolucao();
                    }
                    ?>                                  
                </tbody>
            </table>
        </div>
        <div class="form-group">
            <label>Valor Final Devolução</label>
            <input type="text" class="form-control" placeholder="Total"  value="<?php echo $Tradutor->formatar($valorFinalDevolucao, 'MOEDA'); ?>">
            <input type="hidden" class="form-control" placeholder="Total" name="valorFinalDev" id="valorFinalDev" value="<?php echo $valorFinalDevolucao; ?>">
        </div><br>
        <?php
    }
    if (count($notas) > 0) {
        ?>    
        <div class="row">
            <div class="col-md-3">
                <a class="btn btn-primary" onclick="javascript:FinalizaCadastroOcorrencia();">Cadastrar Ocorrência</a>
            </div>                                                                                  
        </div>
        <?php
    }
} else if ($acao == "FINALCAD") {
    $Ocorrencia->setCodigoDevolucao($codOcorrencia);
    $Ocorrencia->setValorDevolucao($valorFinalDev);
    $Ocorrencia->getTipoOcorrencia()->setCodTipoOcorrencia($motivoDevolucao);
    $Ocorrencia->setQntdVolumeDevolucao($qtdVolume);

//    $emailPortalDevolucao = new emailPortalDevolucao();
//    $emailPortalDevolucao->enviaEmailCadastro($Ocorrencia);
//    echo "<pre>";
//    print_r($_REQUEST);
//    echo "</pre>";
//    die(5);
// ABAIXO SOMENTE QUANDO O USUARIO FINALIZAR O CADASTRO DA OCORRENCIA
    //$Ocorrencia->setQntdVolumeDevolucao($qtdItensDevolucao); QUESTIONAR WILIAN SOBRE VOLUME ***

    if (isset($reentregar)) {
        $Ocorrencia->setReentregaGerDevolucao($reentregar);
    }

    if (!isset($agregado)) {
        $controle_rota = new RotaDao();
        $result_rota = $controle_rota->consultaUsuRota(null, $rot_co_numero);
        OCIFetchInto($result_rota, $row_rota, OCI_ASSOC);
        $emailcoleta = $row_rota['USU_NO_EMAIL'];
    }
    if (strlen(trim($emailcoleta)) <= 0) {
        $emailcoleta = $ObjUsuario->getObjColaborador()->getPessoaEmail();
    }

    $Ocorrencia->setEmailColetaDevolucao(trim($emailcoleta));


    $TipoOcorrenciaDAO = new TipoOcorrenciaDAO();
    $TipoOcorrencia = new CalTipoOcorrencia();
    $ParametroDao = new ParametroDao();
    $Parametro = new devParametros();

    $TipoOcorrencia = $TipoOcorrenciaDAO->consultarTipoOcorrencia($Ocorrencia->getTipoOcorrencia());
    $Parametro = $ParametroDao->consultar();


    $OcorrenciaPasso = new OcorrenciaPasso();
    $OcorrenciaPassoDao = new OcorrenciaPassoDao();

    $OcorrenciaStatus = new OcorrenciaStatus();
    $OcorrenciaStatusDao = new OcorrenciaStatusDao();

    
    /* informa neste ponto que o status da ocorrencia é CADASTRADO COM SUCESSO(15) */
    $Ocorrencia->setStatusOcorrencia(15);
    /* Registrar o status na tabela de Ocorrencia Status */

    $OcorrenciaStatus->setCodigoOcorrencia($Ocorrencia->getCodigoDevolucao());
    $OcorrenciaStatus->setUsuario($ObjUsuario->getUsuarioCodigo());
    $OcorrenciaStatus->setCodStatus($Ocorrencia->getStatusOcorrencia());
    $OcorrenciaStatusDao->registrarSatus($OcorrenciaStatus);
    
    
    /* informa neste ponto que o status da ocorrencia é aguardando aprovacao gerente(1) */
    $Ocorrencia->setStatusOcorrencia(1);
    /* Registrar o status na tabela de Ocorrencia Status */

    $OcorrenciaStatus->setCodigoOcorrencia($Ocorrencia->getCodigoDevolucao());
    $OcorrenciaStatus->setUsuario($ObjUsuario->getUsuarioCodigo());
    $OcorrenciaStatus->setCodStatus($Ocorrencia->getStatusOcorrencia());
    $OcorrenciaStatusDao->registrarSatus($OcorrenciaStatus);

	
	//rotina de valor para aprovação automatica
	$OcorrenciaAux = new Ocorrencia();
	$OcorrenciaAux->setCodigoDevolucao($codOcorrencia);
	$retornoDados    = $OcorrenciaDao->consultaOcorrencia($OcorrenciaAux);
	$ocorrenciaDados = oci_fetch_object($retornoDados);
	$codCentroDist = $ocorrenciaDados->CED_CO_NUMERO;
	$divisao = $ocorrenciaDados->DEV_CO_DIVISAO; //('F', 'FARMA', 'H', 'HOSPITALAR', 'A', 'ALIMENTAR', 'O', 'DIST DIRETA')
	
	$ParametroValorDevolucaoDao = new ParametroValorDevolucaoDao();
	$ObjParametroValorDevolucao = $ParametroValorDevolucaoDao->consultar();

	if ($codCentroDist == 1) {
		$valorF = $ObjParametroValorDevolucao->getValor1F();
		$valorO = $ObjParametroValorDevolucao->getValor1O();
		$valorH = $ObjParametroValorDevolucao->getValor1H();
		$valorA = $ObjParametroValorDevolucao->getValor1A();
	}elseif ($codCentroDist == 46) {
		$valorF = $ObjParametroValorDevolucao->getValor46F();
		$valorO = $ObjParametroValorDevolucao->getValor46O();
		$valorH = $ObjParametroValorDevolucao->getValor46H();
		$valorA = $ObjParametroValorDevolucao->getValor46A();
	}elseif ($codCentroDist == 48) {
		$valorF = $ObjParametroValorDevolucao->getValor48F();
		$valorO = $ObjParametroValorDevolucao->getValor48O();
		$valorH = $ObjParametroValorDevolucao->getValor48H();
		$valorA = $ObjParametroValorDevolucao->getValor48A();
	}elseif ($codCentroDist == 49) {
		$valorF = $ObjParametroValorDevolucao->getValor49F();
		$valorO = $ObjParametroValorDevolucao->getValor49O();
		$valorH = $ObjParametroValorDevolucao->getValor49H();
		$valorA = $ObjParametroValorDevolucao->getValor49A();
	}
	
	if($divisao == "FARMA"){
		$ParametroValorAprovacaoAut = $valorF;
	}elseif($divisao == "DIST DIRETA"){
		$ParametroValorAprovacaoAut = $valorO;
	}elseif($divisao == "HOSPITALAR"){
		$ParametroValorAprovacaoAut = $valorH;
	}elseif($divisao == "ALIMENTAR"){
		$ParametroValorAprovacaoAut = $valorA;
	}else{
		$ParametroValorAprovacaoAut = $Parametro->getNumValorAprovacaoAut();
	}
	//FIM - rotina de valor para aprovação automatica
	
	/*
	echo "<script>alert('".$codCentroDist."');</script>";
	echo "<script>alert('".$divisao."');</script>";
	echo "<script>alert('".$TipoOcorrencia->getRegra()."');</script>";
	echo "<script>alert('".str_replace(',', '.', $Ocorrencia->getValorDevolucao())."');</script>";
	echo "<script>alert('".$ParametroValorAprovacaoAut."');</script>";
	echo "<script>alert('tipoDevolucao:".$tipoDevolucao."');</script>";
	echo "<script>alert('".(int)$OcorrenciaDao->verificaItemGeladeiraOcorrencia($Ocorrencia)."');</script>";	
	*/

    $aprovacaoAutomatica = false; // para indicar se a ocorrencia foi aprovada auto ou não
    /* Definição passo Gerente(1) */
    if ( (($TipoOcorrencia->getRegra() == 'S') && ( str_replace(',', '.', $Ocorrencia->getValorDevolucao()) > $ParametroValorAprovacaoAut ) )
	or ($tipoDevolucao == 3 and ($OcorrenciaDao->verificaItemGeladeiraOcorrencia($Ocorrencia)) ) ) {
        echo "cai para gerente";
        $OcorrenciaPasso->setCodigoOcorrencia($Ocorrencia->getCodigoDevolucao());
        $OcorrenciaPasso->setCodigoPasso(1); // 1 - gerente
        $OcorrenciaPassoDao->inserirPasso($OcorrenciaPasso);
        /* informa o passo para a tabela dev_ocorrencia */
        $Ocorrencia->setPassoOcorrencia($OcorrenciaPasso->getCodigoPasso());

        $aprovacaoAutomatica = FALSE;
    } else {
        $aprovacaoAutomatica = TRUE;
        echo "aprova automatico para gerente";
        $Ocorrencia->setIndicaAutomatico(2); //cod 2 para aprovaçao automatico de valor
        $OcorrenciaPasso->setCodigoOcorrencia($Ocorrencia->getCodigoDevolucao());
        $OcorrenciaPasso->setCodigoPasso(1); // 1 - gerente

        $OcorrenciaPassoDao->inserirPasso($OcorrenciaPasso);

        $OcorrenciaPasso->setUsuario($ObjUsuario->getUsuarioCodigo()); // 1 - geovanni.info (temporario)
        $OcorrenciaPasso->setIndicadorAprovado(1);
        $OcorrenciaPasso->setObservacao("APROVACAO AUTOMATICA POR VALOR");

        $OcorrenciaPassoDao->aprovarPasso($OcorrenciaPasso);


        //passa para o passo Transportador (2)
        $OcorrenciaPasso->setCodigoOcorrencia($Ocorrencia->getCodigoDevolucao());
        $OcorrenciaPasso->setCodigoPasso(2); // 2 - Trans

        $OcorrenciaPassoDao->inserirPasso($OcorrenciaPasso);

        /* informa o passo para a tabela dev_ocorrencia */
        $Ocorrencia->setPassoOcorrencia($OcorrenciaPasso->getCodigoPasso());


        /* informa neste ponto que o status da ocorrencia é APROVADO(2) */
        $Ocorrencia->setStatusOcorrencia(2);
        /* Registrar o status na tabela de Ocorrencia Status */
        $OcorrenciaStatus = new OcorrenciaStatus();
        $OcorrenciaStatus->setCodigoOcorrencia($Ocorrencia->getCodigoDevolucao());
        $OcorrenciaStatus->setUsuario($ObjUsuario->getUsuarioCodigo());
        $OcorrenciaStatus->setCodStatus($Ocorrencia->getStatusOcorrencia());
        $OcorrenciaStatusDao->registrarSatus($OcorrenciaStatus);

        /*
          Quando a ocorrencia for do tipo: 3- INTEGRAL ATO DA ENTREGA
          Nao tem necessidade do cliente informar a nota de devolucao
          neste caso segue o processo para aguardando coleta

         */
        if ($tipoDevolucao == 3) {
            /* informa neste ponto que o status da ocorrencia é aguardando coleta(5) */
            $Ocorrencia->setStatusOcorrencia(5);
            /* Registrar o status na tabela de Ocorrencia Status */
            $OcorrenciaStatus = new OcorrenciaStatus();
            $OcorrenciaStatus->setCodigoOcorrencia($Ocorrencia->getCodigoDevolucao());
            $OcorrenciaStatus->setUsuario($ObjUsuario->getUsuarioCodigo());
            $OcorrenciaStatus->setCodStatus($Ocorrencia->getStatusOcorrencia());
            $OcorrenciaStatusDao->registrarSatus($OcorrenciaStatus);


            //salva arquivo(ordem coleta) na tabela DEV_OCORRENCIA_ARQUIVO
            $nomeOrdemSystem = "ordemColeta_{$Ocorrencia->getCodigoDevolucao()}.pdf";
            $OcorrenciaArquivosDao = new OcorrenciaArquivosDao();
            $OcorrenciaArquivos = new OcorrenciaArquivos();
            $OcorrenciaArquivos->setCodOcorrencia($Ocorrencia->getCodigoDevolucao());
            $OcorrenciaArquivos->setCodigoTipoArquivo(2); // ordem coleta 2
//            $OcorrenciaArquivos->setNomeArquivoCliente($nomeEspelhoSystem);
            $OcorrenciaArquivos->setNomeArquivoSystem($nomeOrdemSystem);
//            $OcorrenciaArquivos->setCodigoUsuario($ObjUsuario->getUsuarioCodigo());
            $OcorrenciaArquivos->setObservacaoArquivo("Ordem coleta para a ocorrência: " . $Ocorrencia->getCodigoDevolucao());
            $OcorrenciaArquivosDao->registrarArquivo($OcorrenciaArquivos);
        } else {

            /* informa neste ponto que o status da ocorrencia é AGUARDANDO NFD(3) */
            $Ocorrencia->setStatusOcorrencia(3);
            /* Registrar o status na tabela de Ocorrencia Status */
            $OcorrenciaStatus = new OcorrenciaStatus();
            $OcorrenciaStatus->setCodigoOcorrencia($Ocorrencia->getCodigoDevolucao());
            $OcorrenciaStatus->setUsuario($ObjUsuario->getUsuarioCodigo());
            $OcorrenciaStatus->setCodStatus($Ocorrencia->getStatusOcorrencia());
            $OcorrenciaStatusDao->registrarSatus($OcorrenciaStatus);
            //nesse ponto gera espelho nota
            //envia o espelho para o cliente E-MAIL
            //salva arquivo(espelho nota) na tabela DEV_OCORRENCIA_ARQUIVO
            $nomeEspelhoSystem = "espelhoNota_{$Ocorrencia->getCodigoDevolucao()}.pdf";
            $OcorrenciaArquivosDao = new OcorrenciaArquivosDao();
            $OcorrenciaArquivos = new OcorrenciaArquivos();
            $OcorrenciaArquivos->setCodOcorrencia($Ocorrencia->getCodigoDevolucao());
            $OcorrenciaArquivos->setCodigoTipoArquivo(3); // espelho nota 3
//            $OcorrenciaArquivos->setNomeArquivoCliente($nomeEspelhoSystem);
            $OcorrenciaArquivos->setNomeArquivoSystem($nomeEspelhoSystem);
//            $OcorrenciaArquivos->setCodigoUsuario($ObjUsuario->getUsuarioCodigo());
            $OcorrenciaArquivos->setObservacaoArquivo("Espelho de nota para a ocorrência: " . $Ocorrencia->getCodigoDevolucao());
            $OcorrenciaArquivosDao->registrarArquivo($OcorrenciaArquivos);
        }
    }

    if (!$OcorrenciaDao->alterar($Ocorrencia)) {
        die("<center><strong class='msmerro'>Desculpe, erro na aprova&ccedil;&atilde;o do passo </strong></center>");
    } else {

//        if ($ObjUsuario->getUsuarioTipo() == "CLI") {
//
//            $emailCadastrante = $ObjUsuario->getUsuarioUsername();
//            
//        } else if ($ObjUsuario->getUsuarioTipo() == "INT") {
//
//            $emailCadastrante = $ObjUsuario->getObjColaborador()->getPessoaEmail();
//            
//        } else if ($ObjUsuario->getUsuarioTipo() == "VEN") {
//
//            $emailCadastrante = $ObjUsuario->getObjVendedor()->getPessoaEmail();
//
//            
//        } else if ($ObjUsuario->getUsuarioTipo() == "TRA") { //ajustarrr 
//            
//            $emailCadastrante = $ObjUsuario->getObjColaborador()->getPessoaEmail(); //AMARAL
//
//        }

        $emailPortalDevolucao = new emailPortalDevolucao();
        $emailPortalDevolucao->enviaEmailCadastro($Ocorrencia);

        if ($aprovacaoAutomatica) {

            if ($tipoDevolucao == 3) {
                $emailPortalDevolucao->enviaEmailAprovadoSemEspelho($Ocorrencia);
            } else {
                $emailPortalDevolucao->enviaEmailAprovadoComEspelho($Ocorrencia);
            }
        }
		
		if($tipoDevolucao == 3 and ($OcorrenciaDao->verificaItemGeladeiraOcorrencia($Ocorrencia)) ){
			$emailPortalDevolucao->enviaEmailAvisoGeladeira($Ocorrencia);
		}		
		
    }
} elseif ($acao == 'DELNOTA') {
    $serieNotaPost        = $_POST['serieNota'];
    $numNotaPost          = $_POST['numNota'];
    $dataNotaPost         = $_POST['dataNota'];

    $Ocorrencia->setCodigoDevolucao($codOcorrencia);
    $DevNotaOrigem->setSerie($serieNotaPost);
    $DevNotaOrigem->setNota($numNotaPost);
    $DevNotaOrigem->setData($dataNotaPost);
    $DevNotaOrigem->setCodCliente($cli_co_numero);
    $Ocorrencia->setDevNotaOrigem($DevNotaOrigem);
    
    if (!$OcorrenciaDao->excluirNotaOrigem($Ocorrencia)) {
        die("<script> alert('Erro ao Excluir Nota!') </script>");
    } else {
        die("<script> alert('Nota Excluida com Sucesso!') </script>");
    }
}