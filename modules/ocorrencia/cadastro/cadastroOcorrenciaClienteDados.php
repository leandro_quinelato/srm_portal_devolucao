<?php
session_start();

include_once( "../../../includes/Dao/DaoSistema.class.php" );
include_once( "../../../includes/Rota.class.php" );
include_once( "../../../includes/Dao/RotaDao.class.php" );
include_once( "../../../includes/Dao/OcorrenciaDao.class.php" );

include_once("DaoGlobal.class.php");
include_once("SistemaDao.class.php");
include_once("GlbSistema.class.php");
include_once("GlbSistemaPerfil.class.php");
include_once("GlbUsuario.class.php");
include_once("GlbColaborador.class.php");
include_once("GlbPessoa.class.php");
include_once("GlbEndereco.class.php");
include_once("GlbTelefone.class.php");
include_once("include_novo/GlbTransportadora.class.php");
include_once("include_novo/GlbContatoTransportador.class.php");
include_once("include_novo/ContatoTransportadorDao.class.php");
include_once("GlbProgramaRotina.class.php");
include_once("GlbModuloPrograma.class.php");
include_once("GlbSistemaModulo.class.php");
include_once("PerfilUsuarioDao.class.php");
include_once("PerfilPermissaoDao.class.php");

$ObjUsuario = unserialize($_SESSION['ObjLogin']);

//echo "<pre>";
//print_r($ObjUsuario);
//echo "</pre>";


extract($_REQUEST);
$RotaDao = new RotaDao();
$OcorrenciaDao = new OcorrenciaDao();
$resultRota = $RotaDao->listar();
if (strlen($cli_co_numero) <= 0) {
    die();
}

if ($ObjUsuario->getUsuarioTipo() == "VEN") {
    $VendedorUsuario = $ObjUsuario->getObjVendedor();
                        $ObjVendedor = new GlbVendedor();
                        if ($VendedorUsuario[0]) {
                            $ObjVendedor->setCodigo($VendedorUsuario[0]->getCodigo());
                            $ObjVendedor->setTipo($VendedorUsuario[0]->getTipo());
                        }
    $resultRotaVen = $RotaDao->consultarRotaVendedor($ObjVendedor);
}

if($ObjUsuario->getUsuarioTipo() == "TRA"){
    $resultCliente= $OcorrenciaDao->consultaClienteTransp($cli_co_numero,$ObjUsuario->getUsuarioCodigo() );
    $resultRota  = $RotaDao->consultaUsuRota($ObjUsuario->getUsuarioCodigo());
}else{
    
    $resultCliente = $OcorrenciaDao->consultaCliente($cli_co_numero);
}
//echo "alo?";

OCIFetchInto($resultCliente, $rowCliente, OCI_ASSOC);
if ($rowCliente['CLI_NO_RAZAO_SOCIAL']) {
    ?>
    <div class="col-md-3">
        <div class="form-group">
            <label>Razão Social</label>
            <input type="text" placeholder="Cliente" class="form-control" name="cli_no_razao_social" id="cli_no_razao_social" value="<?php echo $rowCliente['CLI_NO_RAZAO_SOCIAL']; ?>" readonly="true">
        </div>
    </div>    

<div class="col-md-3">                        
    <div class="form-group">
        <label>Rota Cliente</label>
<?php
if ($ObjUsuario->getUsuarioTipo() == "CLI") {
?>
        
        
        <select class="form-control selectpicker" id="rot_co_numero" name="rot_co_numero">
                <option value="<?php echo $rowCliente['ROT_CO_NUMERO']; ?>" >
                <?php echo $rowCliente['ROT_CO_NUMERO']; ?> 
                </option>
        </select>
        
<?php
}else if ($ObjUsuario->getUsuarioTipo() == "VEN") {
?>    
   
                <select class="form-control selectpicker" id="rot_co_numero" name="rot_co_numero">
            <option value=""></option>
            <?php
            while (OCIFetchInto($resultRotaVen, $rowRota, OCI_ASSOC)) {
                ?>
                <option value="<?php echo $rowRota['ROT_CO_NUMERO']; ?>"  <?php if ($rowCliente['ROT_CO_NUMERO'] == $rowRota['ROT_CO_NUMERO']) {
                echo "selected='selected'";
            } ?>>
                <?php echo $rowRota['ROT_CO_NUMERO']; ?> 
                </option>
                <?php
            }
            ?>
        </select>
        
<?php
}else{
?>    
                <select class="form-control selectpicker" id="rot_co_numero" name="rot_co_numero">
            <option value=""></option>
            <?php
            while (OCIFetchInto($resultRota, $rowRota, OCI_ASSOC)) {
                ?>
                <option value="<?php echo $rowRota['ROT_CO_NUMERO']; ?>"  <?php if ($rowCliente['ROT_CO_NUMERO'] == $rowRota['ROT_CO_NUMERO']) {
                echo "selected='selected'";
            } ?>>
                <?php echo $rowRota['ROT_CO_NUMERO']; ?> 
                </option>
                <?php
            }
            ?>
        </select>

<?php 
}
?>
        
    </div>                        
</div> 
    <?php
}else{
?>
<script> alert('Atenção! Cliente não encontrado ou não liberado para seu usuário.'); </script>
<?php
}
?>


