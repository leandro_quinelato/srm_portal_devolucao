<!DOCTYPE html>
<?php
include_once( "../../../includes/Dao/DaoSistema.class.php" );
include_once( "../../../includes/Dao/OcorrenciaDao.class.php" );
include_once( "../../../includes/CalTipoOcorrencia.class.php" );
include_once( "../../../includes/Dao/TipoOcorrenciaDAO.class.php" );
include_once( "../../../includes/TipoDevolucao.class.php" );
include_once( "../../../includes/Dao/TipoDevolucaoDao.class.php" );

include_once( "../../../includes/DevNotaOrigemItem.class.php" );
include_once( "../../../includes/Dao/DevNotaOrigemItemDao.class.php" );

include_once( "../../../includes/DevNotaOrigem.class.php" );

include_once( "../../../includes/DevLiberacaoNotaOrigem.class.php" );
include_once( "../../../includes/Dao/LiberacaoNotaOrigemDao.class.php" );

include_once( "../../../includes/DevParametros.class.php" );
include_once( "../../../includes/Dao/ParametroDao.class.php" );

include_once( "../../../includes/Dao/ClienteEspecialDao.class.php" );

include_once("DaoGlobal.class.php");
include_once("SistemaDao.class.php");
include_once("GlbSistema.class.php");
include_once("GlbSistemaPerfil.class.php");
include_once("GlbUsuario.class.php");
include_once("GlbColaborador.class.php");
include_once("GlbPessoa.class.php");
include_once("GlbEndereco.class.php");
include_once("GlbTelefone.class.php");
include_once("GlbProgramaRotina.class.php");
include_once("GlbModuloPrograma.class.php");
include_once("GlbSistemaModulo.class.php");
include_once("PerfilUsuarioDao.class.php");
include_once("PerfilPermissaoDao.class.php");
include_once("integraSetSistemas.php");

include_once("include_novo/Tradutor.class.php");

$Tradutor = new Tradutor();

$integra = new integraSetSistemas("conf.xml");
$ObjUsuario = unserialize($integra->getSistemaIntegra());
$ObjRotinas = $ObjUsuario->getObjSistemaPerfil()->getObjRotina();

//echo "<pre>";
//print_r($_REQUEST);
//echo "</pre>";
//echo "<pre>";
//print_r($ObjUsuario);
//echo "</pre>";

$OcorrenciaDao = new OcorrenciaDao();

extract($_REQUEST);
//$codOcorrencia = $_REQUEST['codOcorrencia'];
//$cli_co_numero = $_REQUEST['cli_co_numero'];
//$serie = $_REQUEST['serieNota'];
//$numNota = $_REQUEST['numNota'];
//$dataNota = $_REQUEST['dataNota'];
//$tipoDevolucao = $_REQUEST['tipoDevolucao'];

$LiberacaoNotaOrigem = new DevLiberacaoNotaOrigem();
$LiberacaoNotaOrigem->getDevNotaOrigem()->setSerie($serieNota);
$LiberacaoNotaOrigem->getDevNotaOrigem()->setNota($numNota);
$LiberacaoNotaOrigem->getDevNotaOrigem()->setData($dataNota);
$LiberacaoNotaOrigem->getGlbCliente()->setClienteCodigo($cli_co_numero);

$OcorrenciaDao = new OcorrenciaDao();
$dias_uteis = $OcorrenciaDao->diasUteisNota($dataNota);

/*
 * PASSOS PARA VERIFICAR A NOTA
 *
 * 1- VERIFICA SE A NOTA EXISTE - OK
 * 2- VERIFICA SE A NOTA JA ESTA CADASTRADA EM OUTRA OCORRENCIA - PAUSA
 * 3- VERIFICA SE A NOTA FOI LIBERADA - OK
 * 4- VERIFICA O PRAZO DE CADASTRO DA NOTA
 * 5- VERIFICA SE O CLIENTE É ESPECIAL
 *
 */


if ($acao != "EDITAR") {

    $notaVenda = $OcorrenciaDao->consultaNotaVenda($cli_co_numero, $serieNota, $numNota, $dataNota);
    if ($notaVenda->NUMNOTA) {
        /*
         * Obter prazo de cadastro por divisao
         */
        $ObjParametro = new devParametros();
        $ParametroDao = new ParametroDao();
        $ObjParametro = $ParametroDao->consultar();
        if ($notaVenda->INDICADOR == 'F' || $notaVenda->INDICADOR == 'H' || $notaVenda->INDICADOR == 'O') {
            $prazoMaximo = $ObjParametro->getNumDiasCadFarmaDH();
        } else {
            $prazoMaximo = $ObjParametro->getNumDiasCadalimentar();
        }
        $TipoDevolucao = new TipoDevolucao();
        $TipoDevolucaoDao = new TipoDevolucaoDao();
        $TipoDevolucao->setCodigo($tipoDevolucao);
        $TipoDevolucao = $TipoDevolucaoDao->consultarTipoDevolucao($TipoDevolucao);
        /*
         * *********************************
         */
        /*         * ***
         * Flag que definira se a nota esta dentro das regras para ser cadastrada ou nao
         * *** */
        $cadastroLiberado = FALSE;

        /*
         * VERIFICAR SE A NOTA POSSUI LIBERAÇAO DE CADASTRO PÓS PRAZO
         */

        $LiberacaoNotaOrigemDao = new LiberacaoNotaOrigemDao();
        $qtdNotaLiberada = $LiberacaoNotaOrigemDao->qtdLiberacaoNota($LiberacaoNotaOrigem);
        $resultDup = $OcorrenciaDao->consultaDuplicidadeOcorrencia($cli_co_numero, $serieNota, $numNota, $dataNota, $TipoDevolucao->getTipoDevolucao());
        OCIFetchInto($resultDup, $row, OCI_ASSOC);
        if (isset($row['DOC_CO_NUMERO'])) {
        ?>
            <div class="form-group">
                <label>Aviso! Esta Nota já foi cadastrada na ocorrência: <?php echo $row['DOC_CO_NUMERO']; ?>!</label>
            </div>
        <?php
        } else {
            if (($tipoDevolucao == 4) && ($OcorrenciaDao->verificaItemGeladeiraNota($cli_co_numero, $serieNota, $numNota, $dataNota) ) 
				&& ($ObjUsuario->getObjSistemaPerfil()->getNomeSistemaPerfil() != 'Administrador') ) {
        ?>
                <div class="form-group">
                    <label>Aviso! Esta Nota possui itens de geladeira e não poderá ser devolvida integralmente!</label>
                </div>
        <?php
            } else {
                if ($qtdNotaLiberada >= 1) {
                    $cadastroLiberado = TRUE;
                } else {

                    $ClienteEspecialDao = new ClienteEspecialDao();
                    $clienteEspecial = $ClienteEspecialDao->verificaClienteEspecial($cli_co_numero);
					
					$numDiasLiberacaoNF = $ClienteEspecialDao->consultarDiasClienteEspecial($cli_co_numero);
					if ($numDiasLiberacaoNF > 0){
						$numDiasLiberacaoNF = $numDiasLiberacaoNF + 1;
					}else{
						$numDiasLiberacaoNF = $ObjParametro->getNumDiasLiberacaoNF();
					}
                    if ($clienteEspecial) {

                        $data = split("/", $dataNota);                        
						if ((mktime(0, 0, 0, date('m'), date('d'), date('Y')) - mktime(0, 0, 0, $data[1], $data[0], $data[2])) / (60 * 60 * 24) < $numDiasLiberacaoNF) {
                            $cadastroLiberado = TRUE;
                        } else {
                            $cadastroLiberado = FALSE;
                        }
                    } else {

                        if ($dias_uteis <= $prazoMaximo) {
                            $cadastroLiberado = TRUE;
                        } else {
                            $cadastroLiberado = FALSE;
                        }
                    }
                }
                if ($cadastroLiberado) {
                    $existeParcialCad = $OcorrenciaDao->consultaDuplicidadeOcorrenciaParcial($cli_co_numero, $serieNota, $numNota, $dataNota);
                    if ($OcorrenciaDao->verificaNotacadastrada($codOcorrencia, $cli_co_numero, $serieNota, $numNota)) {
                        if ($existeParcialCad) {
                            $ItensNota = $OcorrenciaDao->carregaNotaItemSaldoDev($cli_co_numero, $serieNota, $numNota, $dataNota);
                        } else {
                            $ItensNota = $OcorrenciaDao->carregaNotaItem($cli_co_numero, $serieNota, $numNota, $dataNota);
                        }
        ?>
                        <input type="hidden" class="form-control" id="tipoDevolucaoInd" name="tipoDevolucaoInd" value="<?php echo $TipoDevolucao->getTipoDevolucao(); ?>">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>Cód</th>
                                        <th>Descrição produto</th>
                                        <th>Qtd Uni - NF</th>
                                        <?php
                                        if ($existeParcialCad) {
                                        ?>
                                            <th>Qtd - Devolvida</th>
                                            <th>Saldo Disp.</th>
                                            <?php
                                        }
                                        ?>
                                        <th>Quant a devolver</th>
                                        <th>Preço</th>
                                        <th>Total bruto</th>
                                        <th>Valor desc</th>
                                        <th>Valor imp</th>
                                        <th>Total liq</th>
                                    </tr>

                                </thead>                               
                                <tbody>
                        <?php
                        while ($dadosItensNota = oci_fetch_object($ItensNota)) {
                            ?>    

                                        <tr>
                            <!--                <td>
                                                <div class="checkbox checkbox-inline">
                                                    <input type="checkbox" id="check_2">
                                                    <label for="check_2"></label>
                                                </div>
                                            </td>-->

                                            <td>
                            <?php echo $dadosItensNota->PRO_CO_NUMERO; ?>
                                                <input type="hidden" class="form-control" id="notaCodItem_<?php echo $dadosItensNota->PRO_CO_NUMERO; ?>" name="notaCodItem[]" value="<?php echo $dadosItensNota->PRO_CO_NUMERO; ?>">
                                            </td>
                                            <td><?php echo $dadosItensNota->PRO_NO_DESCRICAO; ?></td>
                                            <td>
                            <?php echo $dadosItensNota->QTDE; ?>
                                                <input type="hidden" class="form-control" id="notaItemQtd_<?php echo $dadosItensNota->PRO_CO_NUMERO; ?>" name="notaItemQtd[]" value="<?php echo $dadosItensNota->QTDE; ?>">
                                            </td>
                                                <?php
                                                if ($existeParcialCad) {
                                                    ?>
                                                <td><?php echo $dadosItensNota->QTD_ITENS_DEVOLVIDOS; ?></td>
                                                <td><?php echo $dadosItensNota->SALDO_ATUAL_NOTA; ?></td>
                                                <input type="hidden" class="form-control" id="saldoAtualnota_<?php echo $dadosItensNota->PRO_CO_NUMERO; ?>" name="saldoAtualnota[]" value="<?php echo $dadosItensNota->SALDO_ATUAL_NOTA; ?>">
                                                <?php
                                                }
                                                ?>
                                            <td>
                                                <div class="form-group">
                                            <?php
                                            if ($TipoDevolucao->getTipoDevolucao() == "I") {
                                                ?>
                                                        <input type="text" class="form-control notaItemQtdDev" placeholder="Devolver" id="notaItemQtdDev_<?php echo $dadosItensNota->PRO_CO_NUMERO; ?>" name="notaItemQtdDev[]" value="<?php echo $dadosItensNota->QTDE; ?>" readonly>
                                                        <?php
                                                    } else {
                                                        if ($dadosItensNota->PRO_IN_GELADEIRA == "S" && $ObjUsuario->getObjSistemaPerfil()->getNomeSistemaPerfil() != 'Administrador') {
                                                            ?>
                                                                <!--<input type="text" class="form-control notaItemQtdDev" placeholder="Devolver" id="notaItemQtdDev_<?php echo $dadosItensNota->PRO_CO_NUMERO; ?>" name="notaItemQtdDev[]" onKeyPress="fMascara('numero', event, this)" onchange="javascript:verificaQuantidade(<?php echo $dadosItensNota->PRO_CO_NUMERO; ?>)" readonly>-->
                                                            <h5  style="color:red">  SEM DEVOLUCÃO P/ ITEM GELADEIRA</h5>
                                                            <?php
                                                        } else {
                                                            ?>
                                                            <input type="text" class="form-control notaItemQtdDev" placeholder="Devolver" id="notaItemQtdDev_<?php echo $dadosItensNota->PRO_CO_NUMERO; ?>" name="notaItemQtdDev[]" onKeyPress="fMascara('numero', event, this)" onchange="javascript:verificaQuantidade(<?php echo $dadosItensNota->PRO_CO_NUMERO; ?>, <?php echo $existeParcialCad; ?>)">
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </div>
                                            </td>
                                            <td>
                                                    <?php echo $Tradutor->formatar($dadosItensNota->VLUNITNF, 'MOEDA'); ?>
                                                <input type="hidden" class="form-control" id="ItemValorUnit_<?php echo $dadosItensNota->PRO_CO_NUMERO; ?>" name="ItemValorUnit[]" value="<?php echo $dadosItensNota->VLUNITNF; ?>">
                                            </td>
                                            <td>
                                                <?php echo $Tradutor->formatar($dadosItensNota->TOTAL_BRUTO, 'MOEDA'); ?>
                                                <input type="hidden" class="form-control" id="ItemValorTotBruto_<?php echo $dadosItensNota->PRO_CO_NUMERO; ?>" name="ItemValorTotBruto[]" value="<?php echo $dadosItensNota->TOTAL_BRUTO; ?>">
                                            </td>
                                            <td>
                                                <?php echo $Tradutor->formatar($dadosItensNota->VALOR_DESCONTO, 'MOEDA'); ?>
                                                <input type="hidden" class="form-control" id="ItemValorDesc_<?php echo $dadosItensNota->PRO_CO_NUMERO; ?>" name="ItemValorDesc[]" value="<?php echo $dadosItensNota->VALOR_DESCONTO; ?>">
                                            </td>
                                            <td>
                                                <?php echo $Tradutor->formatar($dadosItensNota->VALOR_IMPOSTO, 'MOEDA'); ?>
                                                <input type="hidden" class="form-control" id="ItemValorImp_<?php echo $dadosItensNota->PRO_CO_NUMERO; ?>" name="ItemValorImp[]" value="<?php echo $dadosItensNota->VALOR_IMPOSTO; ?>">
                                            </td>
                                            <td>

                            <?php echo $Tradutor->formatar($dadosItensNota->TOTAL_LIQUIDO, 'MOEDA'); ?>
                                                <input type="hidden" class="form-control" id="ItemValorTotLiq_<?php echo $dadosItensNota->PRO_CO_NUMERO; ?>" name="ItemValorTotLiq[]" value="<?php echo $dadosItensNota->TOTAL_LIQUIDO; ?>">
                                            </td>
                                        </tr>
                                                <?php
                                            }
                                            ?>                                        
                                </tbody>
                            </table>
                        </div>
                                    <?php
                                    if ($TipoDevolucao->getTipoDevolucao() == "I") {
                                        ?>
                            <div class="form-group">
                                <label>Valor Devolução</label>
                                <input type="text" class="form-control" placeholder="Total" name="valorTotalDev" id="valorTotalDev" value="<?php echo $Tradutor->formatar($notaVenda->VLTOTDOCUM, 'MOEDA'); ?>">
                            </div>

                            <?php
                        }
                    } else {
                        ?>
                        <div class="form-group">
                            <label>Aviso! Nota Já esta cadastrada na ocorrência!</label>
                        </div>
                        <?php
                    }
                } else {
                    ?>
                    <div class="form-group">
                        <label>Aviso! Nota informada esta fora do prazo para cadastro.</label>
                    </div>
                    <?php
                }
            }
        }
    } else {
        ?>
        <div class="form-group">
            <label>Aviso! Nota não encontrada na Base!</label>
        </div>
        <?php
    }
} else {

    $DevNotaOrigemItem = new DevNotaOrigemItem();
    $DevNotaOrigemItem->setCodigoOcorrencia($codOcorrencia);
    $DevNotaOrigemItem->setSerie($serieNota);
    $DevNotaOrigemItem->setNumNota($numNota);
    $DevNotaOrigemItemDao = new DevNotaOrigemItemDao();
    $DevNotaOrigemItem = $DevNotaOrigemItemDao->consultarItemNotasDevolucao($DevNotaOrigemItem);
    $qtdDevolucao = array();
    foreach ($DevNotaOrigemItem as $NotaOrigemItem) {
        $qtdDevolucao[$NotaOrigemItem->getCodProduto()] = $NotaOrigemItem->getDevQuantidade();
    }    
    $existeParcialCad = $OcorrenciaDao->consultaDuplicidadeOcorrenciaParcial($cli_co_numero, $serieNota, $numNota, $dataNota);

    if ($existeParcialCad) {
        $ItensNota = $OcorrenciaDao->carregaNotaItemSaldoDev($cli_co_numero, $serieNota, $numNota, $dataNota);
    } else {
        $ItensNota = $OcorrenciaDao->carregaNotaItem($cli_co_numero, $serieNota, $numNota, $dataNota);
    }

    $TipoDevolucao = new TipoDevolucao();
    $TipoDevolucaoDao = new TipoDevolucaoDao();

    $TipoDevolucao->setCodigo($tipoDevolucao);
    $TipoDevolucao = $TipoDevolucaoDao->consultarTipoDevolucao($TipoDevolucao);
    ?>
    <input type="hidden" class="form-control" id="tipoDevolucaoInd" name="tipoDevolucaoInd" value="<?php echo $TipoDevolucao->getTipoDevolucao(); ?>">
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
    <!--                <th>
            <div class="checkbox checkbox-inline">
                <input type="checkbox" id="check_1">
                <label for="check_1"></label>
            </div>
            </th>-->
                                        <th>Cód</th>
                                        <th>Descrição produto</th>
                                        <th>Qtd Uni - NF</th>
                        <?php
                        if ($existeParcialCad) {
                            ?>
                                            <th>Qtd - Devolvida</th>
                                            <th>Saldo Disp.</th>
                                            <?php
                                        }
                                        ?>
                                        <th>Quant a devolver</th>
                                        <th>Preço</th>
                                        <th>Total bruto</th>
                                        <th>Valor desc</th>
                                        <th>Valor imp</th>
                                        <th>Total liq</th>
                </tr>

            </thead>                               
            <tbody>
    <?php
    while ($dadosItensNota = oci_fetch_object($ItensNota)) {
        ?>    

                    <tr>
        <!--                <td>
                            <div class="checkbox checkbox-inline">
                                <input type="checkbox" id="check_2">
                                <label for="check_2"></label>
                            </div>
                        </td>-->

                        <td>
        <?php echo $dadosItensNota->PRO_CO_NUMERO; ?>
                            <input type="hidden" class="form-control" id="notaCodItem_<?php echo $dadosItensNota->PRO_CO_NUMERO; ?>" name="notaCodItem[]" value="<?php echo $dadosItensNota->PRO_CO_NUMERO; ?>">
                        </td>
                        <td><?php echo $dadosItensNota->PRO_NO_DESCRICAO; ?></td>
                        <td>
        <?php echo $dadosItensNota->QTDE; ?>
                            <input type="hidden" class="form-control" id="notaItemQtd_<?php echo $dadosItensNota->PRO_CO_NUMERO; ?>" name="notaItemQtd[]" value="<?php echo $dadosItensNota->QTDE; ?>">
                        </td>
                        
                                                <?php
                                                if ($existeParcialCad) {
                                                    ?>
                                                <td><?php echo $dadosItensNota->QTD_ITENS_DEVOLVIDOS; ?></td>
                                                <td><?php echo $dadosItensNota->SALDO_ATUAL_NOTA; ?></td>
                                                <?php
                                                }
                                                ?>
                        
                        <td>
                            <div class="form-group">
        <?php
        if ($TipoDevolucao->getTipoDevolucao() == "I") {
            ?>
                                    <input type="text" class="form-control notaItemQtdDev" placeholder="Devolver" id="notaItemQtdDev_<?php echo $dadosItensNota->PRO_CO_NUMERO; ?>" name="notaItemQtdDev[]" value="<?php echo $dadosItensNota->QTDE; ?>" readonly>
                                    <?php
                                                    } else {
                                                        if ($dadosItensNota->PRO_IN_GELADEIRA == "S" && $ObjUsuario->getObjSistemaPerfil()->getNomeSistemaPerfil() != 'Administrador') {
                                                            ?>
                                                                <!--<input type="text" class="form-control notaItemQtdDev" placeholder="Devolver" id="notaItemQtdDev_<?php echo $dadosItensNota->PRO_CO_NUMERO; ?>" name="notaItemQtdDev[]" onKeyPress="fMascara('numero', event, this)" onchange="javascript:verificaQuantidade(<?php echo $dadosItensNota->PRO_CO_NUMERO; ?>)" readonly>-->
                                                            <h5  style="color:red">  SEM DEVOLUCÃO P/ ITEM GELADEIRA</h5>
                                                            <?php
                                                        } else {
                                                            ?>
                                                            <input type="text" class="form-control notaItemQtdDev" placeholder="Devolver" id="notaItemQtdDev_<?php echo $dadosItensNota->PRO_CO_NUMERO; ?>" name="notaItemQtdDev[]" onKeyPress="fMascara('numero', event, this)" onchange="javascript:verificaQuantidade(<?php echo $dadosItensNota->PRO_CO_NUMERO; ?>, <?php echo $existeParcialCad; ?>)" value="<?php echo isset($qtdDevolucao[$dadosItensNota->PRO_CO_NUMERO]) ? $qtdDevolucao[$dadosItensNota->PRO_CO_NUMERO] : '' ?>">
                                                            <?php
                                                        }
                                                    }
                                ?>
                            </div>
                        </td>
                        <td>
                                <?php echo $Tradutor->formatar($dadosItensNota->VLUNITNF, 'MOEDA'); ?>
                            <input type="hidden" class="form-control" id="ItemValorUnit_<?php echo $dadosItensNota->PRO_CO_NUMERO; ?>" name="ItemValorUnit[]" value="<?php echo $dadosItensNota->VLUNITNF; ?>">
                        </td>
                        <td>
                            <?php echo $Tradutor->formatar($dadosItensNota->TOTAL_BRUTO, 'MOEDA'); ?>
                            <input type="hidden" class="form-control" id="ItemValorTotBruto_<?php echo $dadosItensNota->PRO_CO_NUMERO; ?>" name="ItemValorTotBruto[]" value="<?php echo $dadosItensNota->TOTAL_BRUTO; ?>">
                        </td>
                        <td>
                            <?php echo $Tradutor->formatar($dadosItensNota->VALOR_DESCONTO, 'MOEDA'); ?>
                            <input type="hidden" class="form-control" id="ItemValorDesc_<?php echo $dadosItensNota->PRO_CO_NUMERO; ?>" name="ItemValorDesc[]" value="<?php echo $dadosItensNota->VALOR_DESCONTO; ?>">
                        </td>
                        <td>
                            <?php echo $Tradutor->formatar($dadosItensNota->VALOR_IMPOSTO, 'MOEDA'); ?>
                            <input type="hidden" class="form-control" id="ItemValorImp_<?php echo $dadosItensNota->PRO_CO_NUMERO; ?>" name="ItemValorImp[]" value="<?php echo $dadosItensNota->VALOR_IMPOSTO; ?>">
                        </td>
                        <td>

        <?php echo $Tradutor->formatar($dadosItensNota->TOTAL_LIQUIDO, 'MOEDA'); ?>
                            <input type="hidden" class="form-control" id="ItemValorTotLiq_<?php echo $dadosItensNota->PRO_CO_NUMERO; ?>" name="ItemValorTotLiq[]" value="<?php echo $dadosItensNota->TOTAL_LIQUIDO; ?>">
                        </td>
                    </tr>
                            <?php
                        }
                        ?>                                        
            </tbody>
        </table>
    </div>
                <?php
                if ($TipoDevolucao->getTipoDevolucao() == "I") {
                    ?>
        <div class="form-group">
            <label>Valor Devolução</label>
            <input type="text" class="form-control" placeholder="Total" name="valorTotalDev" id="valorTotalDev" value="<?php echo $Tradutor->formatar($notaVenda->VLTOTDOCUM, 'MOEDA'); ?>">
        </div>

        <?php
    }
}    