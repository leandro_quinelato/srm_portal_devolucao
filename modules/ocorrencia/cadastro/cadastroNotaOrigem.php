
<?php
include_once( "../../../includes/Dao/DaoSistema.class.php" );
include_once( "../../../includes/Dao/OcorrenciaDao.class.php" );
include_once( "../../../includes/CalTipoOcorrencia.class.php" );
include_once( "../../../includes/Dao/TipoOcorrenciaDAO.class.php" );
include_once( "../../../includes/TipoDevolucao.class.php" );
include_once( "../../../includes/Dao/TipoDevolucaoDao.class.php" );
include_once("DaoGlobal.class.php");
include_once("SistemaDao.class.php");
include_once("GlbSistema.class.php");
include_once("GlbSistemaPerfil.class.php");
include_once("GlbUsuario.class.php");
include_once("GlbColaborador.class.php");
include_once("GlbPessoa.class.php");
include_once("GlbEndereco.class.php");
include_once("GlbTelefone.class.php");
include_once("include_novo/GlbTransportadora.class.php");
include_once("include_novo/GlbContatoTransportador.class.php");
include_once("include_novo/ContatoTransportadorDao.class.php");
include_once("GlbProgramaRotina.class.php");
include_once("GlbModuloPrograma.class.php");
include_once("GlbSistemaModulo.class.php");
include_once("PerfilUsuarioDao.class.php");
include_once("PerfilPermissaoDao.class.php");
include_once("integraSetSistemas.php");
$integra = new integraSetSistemas("conf.xml");
$ObjUsuario = unserialize($integra->getSistemaIntegra());
$ObjRotinas = $ObjUsuario->getObjSistemaPerfil()->getObjRotina();

extract($_REQUEST);

$OcorrenciaDao = new OcorrenciaDao();
?>

<div class="modal-dialog ModalNotaVenda">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Adicionar Nota de venda</h4>
        </div>
        <div class="modal-body">
            <div id="step-6">
                <div class="form-group">
                    <form id="formCadNotas">
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">                                                        
                                    <label>Série</label>
                                    <input type="text" placeholder="Série" class="form-control" id="serieNota" name="serieNota"
                                        onkeyup="proximoCampo(this, 'numNota', '2')" onkeypress="fMascara('numero', event, this)" value="<?php echo $serieNota; ?>">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Nota</label>
                                    <input type="text" placeholder="Nota" class="form-control" id="numNota" name="numNota"
                                        onkeyup="proximoCampo(this, 'dataNota', '6')" onkeypress="fMascara('numero', event, this)" value="<?php echo $numNota ?>">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Data Nota</label>                            
                                    <input type="text" class="form-control datepicker" id="dataNota" name="dataNota" onkeypress="fMascara('data', event, this)" maxlength="10" value="<?php echo $dataNota?>">
                                </div>
                            </div> 
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                            	<div class="form-group">
                                <?php
                                    if (!isset($numNota)){
                                ?>
                                        <a class="btn btn-primary" onclick="javascript:abrirNotaOrigemItem();" id="btnCarregaItem">Carregar Nota</a>
                                <?php
                                    }
                                ?>
                                </div>
                            </div>                                                                                  
                        </div>
                        <!--                        <div class="form-group">
                        
                                                </div>-->
                        <div id="areaCarregaNItens">

                        </div>
                        <div id="testesParcial">

                        </div>

                    </form>
                </div>                                                                                                                    
            </div>
        </div>
        <div class="modal-footer" style="display: none;" id="areaBotaoCadastro" >
            <button data-dismiss="modal" class="btn btn-default" type="button">Cancelar</button>
            <?php
                if (isset($numNota)){
            ?>
                    <button class="btn btn-primary" type="button" onclick="javascript: editarOcorrencia();" id="btnSalvarNota">Editar</button>
            <?php
                }else{
            ?>        
                    <button class="btn btn-primary" type="button" onclick="javascript: cadastrarOcorrencia();" id="btnSalvarNota">Salvar</button>
            <?php        
                }
            ?>
            <div id="msgCadNotaOrigem"></div>
        </div>                
    </div>
</div>

<script type="text/javascript">
    function proximoCampo(atual, proximo, maxLength){
        if(atual.value.length >= maxLength){
            document.getElementById(proximo).focus();
        }
    };
</script>