<?php 
	session_start();
	ini_set('display_errors', 1);	
		
    define("PATH",$_SERVER['DOCUMENT_ROOT']);
	include_once("Dao.class.php");
	include_once("DaoGlobal.class.php");
	include_once("GlbUsuario.class.php");
	include_once("GlbSistemaPerfil.class.php");
	include_once("GlbSistema.class.php");
	include_once("UsuarioDao.class.php");
	include_once("GlbColaborador.class.php");
	include_once("ColaboradorDao.class.php");
	include_once("ContatoDao.class.php");
	include_once("GlbPessoa.class.php");
	include_once("GlbEndereco.class.php");
	include_once("GlbTelefone.class.php");
	include_once("include_novo/GlbTransportadora.class.php");
	include_once("include_novo/GlbContatoTransportador.class.php");
	include_once("include_novo/ContatoTransportadorDao.class.php");
	include_once("PerfilUsuarioDao.class.php" );
	include_once("PerfilPermissaoDao.class.php" );
	include_once("VendedorDao.class.php");
	
	include_once("class.phpmailer.php");
	include_once("class.smtp.php");	
	
	function enviarEmailUsuarioSenha(GlbUsuario $Usuario){	
        $mailHandle = new PHPMailer();

        $mailHandle->IsSMTP(); // seta que o envio � por SMTP
        $mailHandle->Host = "192.168.2.50"; // SMTP server
        $mailHandle->SMTPAuth = false; // Habilita a autentica��o
		$mailHandle->CharSet = "UTF-8";
		$mailHandle->IsHTML(true);

        $mailHandle->Port = 25; // seta a porta de conex�o
        $mailHandle->Username = "devolucao@servimed.com.br"; // usuario de conex�o com o SMTP
        $mailHandle->Password = ""; // senha do usuario conex�o com o SMTP
        //Dados Remetente
        $mailHandle->SetFrom('devolucao@servimed.com.br', 'devolucao@servimed.com.br');		

		//Dados Remetente
		//$mailHandle->AddReplyTo("no-reply@servimed.com.br"); //responder para
		if($Usuario->getUsuarioTipo() == 'INT'){
			$emailDestinatario = $Usuario->getObjColaborador()->getPessoaEmail();
			$nomeDestinatario = $Usuario->getObjColaborador()->getPessoaNome();
		}elseif($Usuario->getUsuarioTipo() == 'CLI'){
			foreach($Usuario->getObjCliente()->getObjContato() as $contato){
				$emailDestinatario = $contato->getPessoaEmail();
				$nomeDestinatario = $contato->getPessoaNome();
			}
		}elseif($Usuario->getUsuarioTipo() == 'VEN'){
/*
 * como a pessoa foi transferida para o representante e esta estrutura nao tem previsao de entrega foi necessaria uma gambitreta 
 * 
*/
			$Vendedor = $Usuario->getObjVendedor();
			$emailDestinatario = $Vendedor[0]->getPseudonimo();
			$nomeDestinatario = $Vendedor[0]->getLabel();
		}elseif($Usuario->getUsuarioTipo() == 'FOR'){
			foreach($Usuario->getObjFabricante()->getDivisao() as $divisao){
				$emailDestinatario = $divisao->getPessoaEmail();
				$nomeDestinatario = $divisao->getPessoaNome();
			}
		}
		$mailHandle->AddAddress($emailDestinatario, $emailDestinatario);
		$mailHandle->AddAddress('bruno.rhassessoria@servimed.com.br', 'bruno.rhassessoria@servimed.com.br');
		$assunto = "Dados de Acesso";
		$texto ="<body bgcolor='#f2f9ff' background='http://integra.servimed.com.br/images/integraBackground.jpg'>
				<center>
					<table border='0' align='center' width='450'>
						<tr>
							<td align='center'>
								<img src='http://integra.servimed.com.br/images/integraLogoMail.png'>
							</td>
						</tr>
						<tr>
							<td height='10'>&nbsp;</td>
						</tr>
						<tr>
							<td><font size='2' color='#666666' face='verdana'>{$nomeDestinatario}, segue os seus dados para acesso ao sistema.</font></td>
						</tr>
						<tr>
							<td><font size='2' color='#666666' face='verdana'>Login: {$Usuario->getUsuarioUsername()}</font></td>
						</tr>
						<tr>
							<td><font size='2' color='#666666' face='verdana'>Senha: {$Usuario->getUsuarioPassword()}</font></td>
						</tr>
						<tr>
							<td><font size='2' color='#666666' face='verdana'><a href='http://portaldevolucao.servimed.com.br'>Sistema de Devolução</a></font></td>
						</tr>					
						<tr>
							<td height='20'>&nbsp;</td>
						</tr>
						<tr>
							<td align='center'>
								<font size='2' color='#c16262' face='verdana'>
									E-mail autom&aacute;tico, n&atilde;o responder!
								</font>
							</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
						</tr>						
						<tr>
							<td align='center'>
								<font size='1' face='verdana' color='#666666'>
									&copy;".date('Y')." Servimed Comercial LTDA
								</font>
							</td>
						</tr>
					</table>
				<center>
			</body>
		"; 
		$mailHandle->Subject = $assunto;
		$mailHandle->MsgHTML($texto); 
		if($mailHandle->Send()){			
			return true;
		}else{
			return false;
		}
	}	
	
	//include_once( "LogDao.class.php" );
	//include_once( "GlbLog.class.php" );
	$txtLogin = $_POST['txtLogin'];
	
	$txtLogin = addslashes($txtLogin);
	
	$UsuarioDao = new UsuarioDao();
	$Usuario = new GlbUsuario();
	
	$Usuario->setUsuarioUsername($txtLogin);
	$ObjUsuario = $UsuarioDao->verificaUsername($Usuario);

	if(!$ObjUsuario){
		die("Erro, ".$UsuarioDao->getMensagem());
	}
	
	if($ObjUsuario->getUsuarioTipo()=='INT'){
		$controle_colab = new ColaboradorDao(); 
		$ObjColaborador = $controle_colab->consultaColaboradorLogin($ObjUsuario);
		$ObjUsuario->setObjColaborador($ObjColaborador); 
		$lac_co_mestre = $ObjColaborador->getColaboradorCracha();
		if($ObjColaborador->getObjLotacao()->getDepartamentoCodigo() == 27 ){
			$VendedorDao = new VendedorDao();
			$VendedorDao->preencherUsuarioVendedor($ObjUsuario);
		}
	}
	
	if($ObjUsuario->getUsuarioTipo()=='CLI'){
		$ContatoDao = new ContatoDao();
		$ContatoDao->preencherUsuarioContato($ObjUsuario);	
	}
        
	if($ObjUsuario->getUsuarioTipo()=='TRA'){
		$ContatoTransportadorDao = new ContatoTransportadorDao();
		$ContatoTransportadorDao->preencherUsuarioContatoTrans($ObjUsuario);
	}

	if($ObjUsuario->getUsuarioTipo()=='VEN') {
		$VendedorDao = new VendedorDao();
		$VendedorDao->preencherUsuarioVendedor($ObjUsuario);
	}	
	
	
	if($ObjUsuario->getUsuarioCodigo() > 0 && $ObjUsuario->getUsuarioAlterarPassword()!="N"){
		$Sistema = new GlbSistema();			
		$Sistema->setCodigoSistema(29);
		
		//Consultando os dados do Sistema selecionado
		$PerfilUsuarioDao = new PerfilUsuarioDao();
		$return = $PerfilUsuarioDao->PerfilPermissaoSistema($ObjUsuario, $Sistema);

		$linhas = OCIFetchStatement( $return, $perfilusuario );
		if ( $linhas>=1 ) {	
			//$ObjSistemaPerfil recebe $ObjLogin e $ObjSistema
			$ObjSistemaPerfilIntegra = new GlbSistemaPerfil();
			
			//Seta o sistema em $ObjSistemaPerfil
			//$ObjSistema recebe os parametros do sistema selecionado	
			$ObjSistemaPerfilIntegra->setCodigoSistemaPerfil( $perfilusuario['PER_CO_NUMERO'][0] );
			$ObjSistemaPerfilIntegra->setNomeSistemaPerfil( $perfilusuario['PER_NO_PERFIL'][0] );  
			$ObjSistemaPerfilIntegra->getObjSistema()->setCodigoSistema( $perfilusuario['SIS_CO_NUMERO'][0] );
			$ObjSistemaPerfilIntegra->getObjSistema()->setNomeSistema( $perfilusuario['SIS_NO_SISTEMA'][0] );
			$ObjSistemaPerfilIntegra->getObjSistema()->setUrlSistema( $perfilusuario['SIS_NO_URL'][0] );
			$ObjSistemaPerfilIntegra->getObjSistema()->setIconeSistema( $perfilusuario['SIS_NO_ICONE'][0] );
					
			$ObjUsuario->setObjSistemaPerfil( $ObjSistemaPerfilIntegra );//Seta o usuÃ¡rio logado em $ObjSistemaPerfil
			
			if($ObjUsuario->getUsuarioTipo()=='CLI'){
				
				//gerar nova senha
				$novaSenha = $UsuarioDao->getNovaSenha();
				$ObjUsuario->setUsuarioPassword($novaSenha);
				
				//Enviar email
				if(enviarEmailUsuarioSenha($ObjUsuario)){
					//gravar nova senha
					$UsuarioDao->alterar($ObjUsuario);
					
					die("Senha alterada e enviada para o email cadastrado!");
				}else{
					die("Erro Envio de email, senha NÃO foi alterada!");
				}
			}else{
				die("Ação permitida apenas aos clientes Servimed!");
			}
		} else {
			$_SESSION['erro']=3;
			?>
			<script>
				location.href='/Login' ;
			</script> 
			<?php 
			die();
		}
		?>
		<script>
			location.href='/Login';
		</script> 
		<?php 
	}else{

		$_SESSION['erro']=1;
		?>
		<script>
			location.href='/Login' ;
		</script> 
		<?php 
	}

?>