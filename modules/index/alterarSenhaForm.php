<?php
    @session_start();
    extract($_REQUEST);
?>

<a class="dev-page-login-block__logo">Devolução</a>
<div class="dev-page-login-block__form">
    <div class="title"><strong>Alteração de Senha,</strong> preencha o formulário.</div>
    <form id="frmCadastrar" name="frmCadastrar" onsubmit="return false;">                        
		
        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                <input type="text" class="form-control" placeholder="Login" name="txtLogin" id="txtLogin" maxlength="80" >
            </div>
        </div>                        
        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                <input type="password" class="form-control" placeholder="Senha" name="txtSenha" id="txtSenha" maxlength="20" >
            </div>
        </div>
        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                <input type="password" class="form-control" placeholder="Nova Senha" name="txtSenhaNova" id="txtSenhaNova" maxlength="20" >
            </div>
        </div>		
        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                <input type="password" class="form-control" placeholder="Senha Confimação" name="txtSenhaConfirmacao" id="txtSenhaConfirmacao" maxlength="20" >
            </div>
        </div>		
        <div id="login-msg" class="form-group ">
        <?php
            if($erro==1){                                
                echo "<p class=\"error\">Senha foi alterada com sucesso!!</p>"; 
            }
            if($erro==2){
                echo "<p class=\"success\">Erro na alterar da senha.</p>"; 
            }		
        ?>
        </div>  
        <div class="form-group no-border margin-top-20">
            <button class="btn btn-success btn-block" onclick="javascript:alterarSenhaConfirma();">Confirmar</button>
        </div>
        		<p><a href="index.php"><< Voltar ao Login</a></p>		
    </form>
</div>
<div class="dev-page-login-block__footer">
    © <?php echo date('Y')?> <strong>Devolução</strong> - Servimed
</div>