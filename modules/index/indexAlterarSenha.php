<?php 
	session_start();
		
    define("PATH",$_SERVER['DOCUMENT_ROOT']);
	include_once("Dao.class.php");
	include_once("DaoGlobal.class.php");
	include_once("GlbUsuario.class.php");
	include_once("GlbSistemaPerfil.class.php");
	include_once("GlbSistema.class.php");
	include_once("UsuarioDao.class.php");
	include_once("GlbColaborador.class.php");
	include_once("ColaboradorDao.class.php");
	include_once("ContatoDao.class.php");
	include_once("GlbPessoa.class.php");
	include_once("GlbEndereco.class.php");
	include_once("GlbTelefone.class.php");
	include_once("include_novo/GlbTransportadora.class.php");
	include_once("include_novo/GlbContatoTransportador.class.php");
	include_once("include_novo/ContatoTransportadorDao.class.php");
	include_once("PerfilUsuarioDao.class.php" );
	include_once("PerfilPermissaoDao.class.php" );
	include_once("VendedorDao.class.php");
	
	
	$txtLogin = $_POST['txtLogin'];
	$txtSenha = md5($_POST['txtSenha']);
	$txtSenhaNova = $_POST['txtSenhaNova'];
	
	$txtLogin = addslashes($txtLogin);
	$txtSenha = addslashes($txtSenha);
	$txtSenhaNova = addslashes($txtSenhaNova);
	
	$UsuarioDao = new UsuarioDao();
	$Usuario = new GlbUsuario();
	
	$Usuario->setUsuarioUsername($txtLogin);
	$Usuario->setUsuarioPassword($txtSenha);
	$ObjUsuario = $UsuarioDao->verifica($Usuario);

	if(!$ObjUsuario){
		die("Erro, ".$UsuarioDao->getMensagem());
	}
	
	if($ObjUsuario->getUsuarioTipo()=='INT'){
		$controle_colab = new ColaboradorDao(); 
		$ObjColaborador = $controle_colab->consultaColaboradorLogin($ObjUsuario);
		$ObjUsuario->setObjColaborador($ObjColaborador); 
		$lac_co_mestre = $ObjColaborador->getColaboradorCracha();
                if($ObjColaborador->getObjLotacao()->getDepartamentoCodigo() == 27 ){
                    $VendedorDao = new VendedorDao();
                    $VendedorDao->preencherUsuarioVendedor($ObjUsuario);
                }
	}
	
	if($ObjUsuario->getUsuarioTipo()=='CLI'){
		$ContatoDao = new ContatoDao();
		$ContatoDao->preencherUsuarioContato($ObjUsuario);	
	}
        
	if($ObjUsuario->getUsuarioTipo()=='TRA'){
            $ContatoTransportadorDao = new ContatoTransportadorDao();
            $ContatoTransportadorDao->preencherUsuarioContatoTrans($ObjUsuario);
            
	
	}

	if($ObjUsuario->getUsuarioTipo()=='VEN') {
            $VendedorDao = new VendedorDao();
            $VendedorDao->preencherUsuarioVendedor($ObjUsuario);
	}	
	
	
	if($ObjUsuario->getUsuarioCodigo() > 0 && $ObjUsuario->getUsuarioAlterarPassword()!="N"){
		$Sistema = new GlbSistema();			
		$Sistema->setCodigoSistema(29);
		
		//Consultando os dados do Sistema selecionado
		$PerfilUsuarioDao = new PerfilUsuarioDao();
		$return = $PerfilUsuarioDao->PerfilPermissaoSistema($ObjUsuario, $Sistema);

		$linhas = OCIFetchStatement( $return, $perfilusuario );
		if ( $linhas>=1 ) {	
			//$ObjSistemaPerfil recebe $ObjLogin e $ObjSistema
			$ObjSistemaPerfilIntegra = new GlbSistemaPerfil();
			
			//Seta o sistema em $ObjSistemaPerfil
			//$ObjSistema recebe os parametros do sistema selecionado	
			$ObjSistemaPerfilIntegra->setCodigoSistemaPerfil( $perfilusuario['PER_CO_NUMERO'][0] );
			$ObjSistemaPerfilIntegra->setNomeSistemaPerfil( $perfilusuario['PER_NO_PERFIL'][0] );  
			$ObjSistemaPerfilIntegra->getObjSistema()->setCodigoSistema( $perfilusuario['SIS_CO_NUMERO'][0] );
			$ObjSistemaPerfilIntegra->getObjSistema()->setNomeSistema( $perfilusuario['SIS_NO_SISTEMA'][0] );
			$ObjSistemaPerfilIntegra->getObjSistema()->setUrlSistema( $perfilusuario['SIS_NO_URL'][0] );
			$ObjSistemaPerfilIntegra->getObjSistema()->setIconeSistema( $perfilusuario['SIS_NO_ICONE'][0] );
					
			$ObjUsuario->setObjSistemaPerfil( $ObjSistemaPerfilIntegra );//Seta o usuÃ¡rio logado em $ObjSistemaPerfil
			
			//gerar nova senha
			$ObjUsuario->setUsuarioPassword($txtSenhaNova);
			
			//gravar nova senha
			$UsuarioDao->alterar($ObjUsuario);
			
			$_SESSION['erro']=1;
			?>
			<script>
				location.href='/alterarSenha.php' ;
			</script> 
			<?php 
			
		} else {
			$_SESSION['erro']=2;
			?>
			<script>
				location.href='/alterarSenha.php' ;
			</script> 
			<?php 
		}
	}else{

		$_SESSION['erro']=2;
		?>
		<script>
			location.href='/alterarSenha.php' ;
		</script> 
		<?php 
	}

?>