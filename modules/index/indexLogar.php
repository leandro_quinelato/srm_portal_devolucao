<?php 
	session_start(); 
		
    define("PATH",$_SERVER['DOCUMENT_ROOT']);
	include_once("Dao.class.php");
	include_once("DaoGlobal.class.php");
	include_once("GlbUsuario.class.php");
	include_once("GlbSistemaPerfil.class.php");
	include_once("GlbSistema.class.php");
	include_once("UsuarioDao.class.php");
	include_once("GlbColaborador.class.php");
	include_once("ColaboradorDao.class.php");
	include_once("ContatoDao.class.php");
	include_once("GlbPessoa.class.php");
	include_once("GlbEndereco.class.php");
	include_once("GlbTelefone.class.php");
	include_once("include_novo/GlbTransportadora.class.php");
	include_once("include_novo/GlbContatoTransportador.class.php");
	include_once("include_novo/ContatoTransportadorDao.class.php");
	include_once( "PerfilUsuarioDao.class.php" );
	include_once( "PerfilPermissaoDao.class.php" );
	include_once("VendedorDao.class.php");

	//include_once( "LogDao.class.php" );
	//include_once( "GlbLog.class.php" );
	$txtLogin = $_POST['txtLogin'];
	$txtSenha = md5($_POST['txtSenha']);
	
	$txtLogin = addslashes($txtLogin);
	$txtSenha = addslashes($txtSenha);
	
	$UsuarioDao = new UsuarioDao();
	
	$Usuario = new GlbUsuario();	
	$Usuario->setUsuarioUsername($txtLogin);
	$Usuario->setUsuarioPassword($txtSenha);
	
	$ObjUsuario = $UsuarioDao->verifica($Usuario);

	if(!$ObjUsuario){
		die("Erro, ".$UsuarioDao->getMensagem());
	}
	
	if($ObjUsuario->getUsuarioTipo()=='INT'){
		$controle_colab = new ColaboradorDao(); 
		$ObjColaborador = $controle_colab->consultaColaboradorLogin($ObjUsuario);
		$ObjUsuario->setObjColaborador($ObjColaborador); 
		$lac_co_mestre = $ObjColaborador->getColaboradorCracha();
                if($ObjColaborador->getObjLotacao()->getDepartamentoCodigo() == 27 ){
                    $VendedorDao = new VendedorDao();
                    $VendedorDao->preencherUsuarioVendedor($ObjUsuario);
                }
	}
	
	if($ObjUsuario->getUsuarioTipo()=='CLI'){
		$ContatoDao = new ContatoDao();
		$ContatoDao->preencherUsuarioContato($ObjUsuario);	
	}
	
	if($ObjUsuario->getUsuarioTipo()=='FOR'){
	
	}
        
	if($ObjUsuario->getUsuarioTipo()=='TRA'){
            $ContatoTransportadorDao = new ContatoTransportadorDao();
            $ContatoTransportadorDao->preencherUsuarioContatoTrans($ObjUsuario);
            
	
	}

	if($ObjUsuario->getUsuarioTipo()=='VEN') {
            $VendedorDao = new VendedorDao();
            $VendedorDao->preencherUsuarioVendedor($ObjUsuario);
	}

// LOG
/*
	$LogDao = new LogDao();

	$Log = new GlbLog();
	$Log->setIp( $_SERVER['REMOTE_ADDR'] );
	$Log->setSessao( session_id() );

	$LogAcao = new GlbLogAcao();
	$LogAcao->setSessao( session_id() );
	$LogAcao->setCodMestre( $lac_co_mestre );
	$LogAcao->setDatAcao( date( "Ymd H:i" ) );
*/

	if($ObjUsuario->getUsuarioCodigo()){
		//$LogAcao->setDesAcao( '[LOGIN]: OK' );
		//$Log->setAcao( $LogAcao );

// seta LOG dentro do USUARIO para vincular log com o usuario retornando o LOG com codigo preenchido
		//$ObjUsuario->setObjLog( $Log );
		//$Log = $LogDao->abrirLog( $ObjUsuario );

// descarrega todas as acoes que estao no LOG e retorna ele sem acao
		//$Log = $LogDao->descarregarAcao( $Log );
		//$ObjUsuario->setObjLog( $Log );
		$Sistema = new GlbSistema();			
		$Sistema->setCodigoSistema( 29 );		
		
		//Retorna para $ObjLogin os dados do usuário logado
		//$ObjLoginIntegra = unserialize( $_SESSION['ObjLogin'] );
		
		//Consultando os dados do Sistema selecionado
		$PerfilUsuarioDao = new PerfilUsuarioDao();
		$return = $PerfilUsuarioDao->PerfilPermissaoSistema( $ObjUsuario,$Sistema );

		$linhas = OCIFetchStatement( $return, $perfilusuario );
		if ( $linhas>=1 ) {	
			//$ObjSistemaPerfil recebe $ObjLogin e $ObjSistema
			$ObjSistemaPerfilIntegra = new GlbSistemaPerfil();
			
			//Seta o sistema em $ObjSistemaPerfil
			//$ObjSistema recebe os parametros do sistema selecionado	
			$ObjSistemaPerfilIntegra->setCodigoSistemaPerfil( $perfilusuario['PER_CO_NUMERO'][0] );
			$ObjSistemaPerfilIntegra->setNomeSistemaPerfil( $perfilusuario['PER_NO_PERFIL'][0] );  
			$ObjSistemaPerfilIntegra->getObjSistema()->setCodigoSistema( $perfilusuario['SIS_CO_NUMERO'][0] );
			$ObjSistemaPerfilIntegra->getObjSistema()->setNomeSistema( $perfilusuario['SIS_NO_SISTEMA'][0] );
			$ObjSistemaPerfilIntegra->getObjSistema()->setDescricaoSistema( $perfilusuario['SIS_TX_DESCRICAO'][0] );
			$ObjSistemaPerfilIntegra->getObjSistema()->setUrlSistema( $perfilusuario['SIS_NO_URL'][0] );
			$ObjSistemaPerfilIntegra->getObjSistema()->setIconeSistema( $perfilusuario['SIS_NO_ICONE'][0] );
					
			$ObjUsuario->setObjSistemaPerfil( $ObjSistemaPerfilIntegra );//Seta o usuÃ¡rio logado em $ObjSistemaPerfil

			$PerfilPermissaoDao = new PerfilPermissaoDao();
			$ObjUsuario = $PerfilPermissaoDao->atribuirPermissao( $ObjUsuario );
			$_SESSION['ObjLogin']=serialize($ObjUsuario);
			
		} else {
			$_SESSION['erro']=3;
			?>
			<script>
				location.href='/Login' ;
			</script> 
			<?php 
			die();
		}

		?>
		<script>
			location.href='/Home';
		</script> 
		<?php 
	}else{
		//$LogAcao->setDesAcao( '[LOGIN]: NEGADO' );
		//$Log->setAcao( $LogAcao );
		//$LogDao->inserirLog( $Log );

		$_SESSION['erro']=1;
		?>
		<script>
			location.href='/Login' ;
		</script> 
		<?php 
	}

?>