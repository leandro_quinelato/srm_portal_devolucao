<?php
    @session_start();
    extract($_REQUEST);
?>

<a class="dev-page-login-block__logo">Devolução</a>
<div class="dev-page-login-block__form">
    <div class="title"><strong>Novo Usuário,</strong> preencha o formulário.</div>
    <form id="frmCadastrar" name="frmCadastrar" onsubmit="return false;">                        
	
        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa"></i></span>
                <input type="text" class="form-control" placeholder="Cnpj" name="txtCnpj" id="txtCnpj" maxlength="80" >
            </div>
        </div>
		
        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa"></i></span>
                <input type="text" class="form-control" placeholder="Código Cliente Servimed" name="txtCodCliente" id="txtCodCliente" maxlength="80" >
            </div>
        </div>		

        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa"></i></span>
                <input type="text" class="form-control" placeholder="Nome" name="txtNome" id="txtNome" maxlength="80" >
            </div>
        </div>
		
        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa"></i></span>
				<select class="form-control" name="txtSexo" id="txtSexo">
					<option  style="color: black;" value="">Qual o Sexo?</option>
					<option  style="color: black;" value="F">Feminino</option>
					<option  style="color: black;" value="M">Masculino</option>
				</select>				
            </div>
        </div>		
		
        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa"></i></span>
                <input type="text" class="form-control" placeholder="Email" name="txtEmail" id="txtEmail" maxlength="80" >
            </div>
        </div>		
		
        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                <input type="text" class="form-control" placeholder="Login" name="txtLogin" id="txtLogin" maxlength="80" >
            </div>
        </div>                        
        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                <input type="password" class="form-control" placeholder="Senha" name="txtSenha" id="txtSenha" maxlength="20" >
            </div>
        </div>
        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                <input type="password" class="form-control" placeholder="Senha Confimação" name="txtSenhaConfirmacao" id="txtSenhaConfirmacao" maxlength="20" >
            </div>
        </div>		
        <div id="login-msg" class="form-group ">
        <?php
            if($erro==1){                                
                echo "<p class=\"error\">Login já exitente!</p>"; 
            }
            if($erro==2){
                echo "<p class=\"success\">Cliente não consta na base.</p>"; 
            }
            if($erro==3){
                echo "<p class=\"error\">Usuário cadastrado!!</p>"; 
            }			
        ?>
        </div>  
        <div class="form-group no-border margin-top-20">
            <button class="btn btn-success btn-block" onclick="javascript:cadastrarUsuario();">Confirmar</button>
        </div>
        		<p><a href="index.php"><< Voltar ao Login</a></p>		
    </form>
</div>
<div class="dev-page-login-block__footer">
    © <?php echo date('Y')?> <strong>Devolução</strong> - Servimed
</div>