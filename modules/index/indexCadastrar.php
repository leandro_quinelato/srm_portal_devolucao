<?php 
	session_start(); 
		
    define("PATH",$_SERVER['DOCUMENT_ROOT']);
	include_once("Dao.class.php");
	include_once("DaoGlobal.class.php");
	include_once("GlbUsuario.class.php");
	include_once("GlbSistemaPerfil.class.php");
	include_once("GlbSistema.class.php");
	include_once("UsuarioDao.class.php");
	include_once("GlbColaborador.class.php");
	include_once("ColaboradorDao.class.php");
	include_once("GlbCliente.class.php");	
	include_once("ClienteDao.class.php");	
	include_once("GlbContato.class.php");
	include_once("ContatoDao.class.php");
	include_once("GlbPessoa.class.php");
	include_once("GlbEndereco.class.php");
	include_once("GlbTelefone.class.php");
	include_once("include_novo/GlbTransportadora.class.php");
	include_once("include_novo/GlbContatoTransportador.class.php");
	include_once("include_novo/ContatoTransportadorDao.class.php");
	include_once("PerfilUsuarioDao.class.php" );
	include_once("PerfilPermissaoDao.class.php" );
	include_once("VendedorDao.class.php");

	
	$txtCnpj  = $_POST['txtCnpj'];
	$txtCodCliente  = $_POST['txtCodCliente'];
	$txtNome = $_POST['txtNome'];
	$txtSexo = $_POST['txtSexo'];
	$txtEmail = $_POST['txtEmail'];
	$txtLogin = $_POST['txtLogin'];
	$txtSenha = $_POST['txtSenha'];
	
	$txtCnpj = addslashes($txtCnpj);
	$txtNome = addslashes($txtNome);
	$txtEmail = addslashes($txtEmail);
	$txtLogin = addslashes($txtLogin);
	$txtSenha = addslashes($txtSenha);
	
	$Usuario = new GlbUsuario();
	$Usuario->setUsuarioUsername($txtLogin);
	
	$UsuarioDao = new UsuarioDao();
	$ObjUsuario = $UsuarioDao->verificaUsername($Usuario);
	
	if($ObjUsuario->getUsuarioCodigo() > 0){ //testar username	
		$_SESSION['erro']=1;
		?>
		<script>location.href='/novoUsuario.php';</script> 
		<?php 
	}else{
		//testar cnpj se exitente
		$ObjCliente = new GlbCliente();
		$ObjCliente->setClienteCnpj($txtCnpj);
		$ObjCliente->setClienteCodigo($txtCodCliente);

		$ClienteDao = new ClienteDao();
		$resCliente = $ClienteDao->consultar($ObjCliente);
		if ($dadosCliente = oci_fetch_object($resCliente)){
			//$codCliente = $dadosCliente->CLI_CO_NUMERO;
			
			//cadastrar contato
			$ObjContato = new GlbContato();
			$ObjContato->setPessoaEmail($txtEmail);
			$ObjContato->setPessoaNome($txtNome);
			$ObjContato->setContatoOrigem('D');
			$ObjContato->setContatoTipo('U');
			$ObjContato->setContatoSexo($txtSexo);
			
			$ObjCliente->setObjContato($ObjContato);
			$contatoDao = new ContatoDao();
			$ObjCliente = $contatoDao->inserirContato($ObjCliente);
				
			//gravar usuario
			$Usuario->setUsuarioPassword($txtSenha);
			$Usuario->setUsuarioTipo('CLI');
			
			$codUser = $UsuarioDao->inserir($Usuario);
			$Usuario->setUsuarioCodigo($codUser);
					
			//atualizar contato com cod_usuario
			$contatoDao->alterarCampo($ObjCliente, "USU_CO_NUMERO", $codUser);
			
			//permissão ao sistema com perfil cliente			
			$ObjSistemaPerfil = new GlbSistemaPerfil();
			$ObjSistemaPerfil->setCodigoSistemaPerfil(84);
			$Usuario->setObjSistemaPerfil($ObjSistemaPerfil);
			
			$perfilUsuarioDao = new PerfilUsuarioDao();
			$perfilUsuarioDao->inserir($Usuario);
			
			//retorno
			$_SESSION['erro']=3;
			?>
			<script>location.href='/novoUsuario.php';</script> 
			<?php 
			
		}else{
			$_SESSION['erro']=2;
			?>
			<script>location.href='/novoUsuario.php';</script> 
			<?php 
		}
	}
?>