<?php
    @session_start();
    extract($_REQUEST);
?>
<script>
    $(function(){
        loginClear();
    });
</script>
<a class="dev-page-login-block__logo">Devolução</a>
<div class="dev-page-login-block__form">
    <div class="title"><strong>Seja Bem Vindo</strong>, insira seu login e senha</div>
    <form id="frmLogin" name="frmLogin" onsubmit="return false;">                        
        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                <input type="text" class="form-control" placeholder="Login" name="txtLogin" id="txtLogin" maxlength="80" >
            </div>
        </div>                        
        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                <input type="password" class="form-control" placeholder="Senha" name="txtSenha" id="txtSenha" maxlength="20" >
            </div>
        </div>
        <div id="login-msg" class="form-group ">
        <?php
            if($erro==1){                                
                echo "<p class=\"error\">Login ou senha inv&aacute;lida</p>"; 
            }
            if($erro==2){
                echo "<p class=\"success\">Sua senha foi alterada e enviada para o seu e-mail.</p>"; 
            }
            if($erro==3){
                echo "<p class=\"error\">Sem permiss&atilde;o ao sistema</p>"; 
            }
        ?>
        </div>  
        <div class="form-group no-border margin-top-20">
            <button class="btn btn-success btn-block" onclick="javascript:login();">Entrar</button>
        </div>
        <p><a href="#" onclick="javascript: recuperarSenha();">Esqueceu sua senha?</a></p>  
		<p><a href="#" onclick="javascript: alterarSenha();">Alterar Senha</a></p>		
		<p><a href="#" onclick="javascript: criarConta();">Criar Conta</a></p>		
    </form>
</div>
<div class="dev-page-login-block__footer">
    © <?php echo date('Y')?> <strong>Devolução</strong> - Servimed
</div>