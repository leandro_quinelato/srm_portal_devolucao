<?php
include_once( "../../../includes/Dao/DaoSistema.class.php" );
include_once( "../../../includes/Dao/RelatorioDao.class.php" );

include_once("include_novo/Tradutor.class.php");

$Tradutor = new Tradutor();

//echo "<pre>";
//print_r($_REQUEST);
//echo "</pre>";

extract($_REQUEST);

$RelatorioDao = new RelatorioDao();

if ($codResponsavel == "TOD") {
    $codResponsavel = null;
}

if ($tresp == "TOD") {
    $tresp = null;
}

if (count($codDivisao) > 0) {
    $auxDivisao = "";
    for ($i = 0; count($codDivisao) > $i; $i++) {
        if ($auxDivisao != "") {
            $auxDivisao .= ", ";
        }
        $auxDivisao .= "'{$codDivisao[$i]}'";
    }
    $codDivisao = $auxDivisao;
}
//die();
//$dataInicial = '01/01/2016';
//$dataFinal = '20/01/2016';


$retorno = $RelatorioDao->relatorioNotaDevolucao($dataInicial, $dataFinal, $codResponsavel, $fab_co_numero, $setor, $codCliente, $codRede, $codDivisao, $tresp);
?>

<div class="table-responsive">
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>Responsável</th>
                <th>Setor</th>
                <th>Vendedor</th>
                <th>Dt Cadastro</th>
                <th>Protocolo</th>
                <th>Cod Cliente</th>
                <th>Cliente</th>
                <th>Motivo</th>
                <th>Serie Origem</th>
                <th>Nota Origem</th>
                <th>Emissão</th>
                <th>Origem da Venda</th>
                <th>Divisão</th>
                <th>CD</th>
                <th>Nota Devolução</th>
                <th>Valor</th>
            </tr>
        </thead>                               
        <tbody>
            <?php
            while (OCIFetchInto($retorno, $row, OCI_ASSOC)) {
                $resposavel[$x] = $row['RESPONSAVEL'];
                if ($row['RESPONSAVEL'] != $resposavel[$x - 1]) {
                    $class = ( $class == "" ? "Zebra" : "" );
                }
                ?>
                <tr class="<?php echo $class; ?>">

                    <?php
                    if ($row['RESPONSAVEL'] != $resposavel[$x - 1]) {
                        ?>
                        <td>
                            <?php
                            echo $array[$row['NTO_IN_ORIGEM_RESPONSAVEL']] . " " . $row['CODIGO'] . " - " . $row['RESPONSAVEL'];
                            ?>
                        </td> 
                        <?php
                    } else {
                        ?>                       
                        <td> </td>
                        <?php
                    }
                    ?>

                    <td><?php echo $row['VEN_CO_NUMERO']; ?></td>
                    <td><?php echo $row['VEN_NO_LABEL']; ?></td>
                    <td><?php echo $row['DOC_DT_CADASTRO']; ?></td>
                    <td><?php echo $row['DOC_CO_NUMERO']; ?></td>
                    <td><?php echo $row['CLI_CO_NUMERO']; ?></td>
                    <td><?php echo $row['CLI_NO_RAZAO_SOCIAL']; ?></td>
                    <td><?php echo $row['TRP_NO_DESCRICAO']; ?></td>
                    <td><?php echo $row['NTO_NU_SERIE']; ?></td>
                    <td><?php echo $row['NTO_NU_NOTA']; ?></td>
                    <td><?php echo $row['NTO_DT_NOTA']; ?></td>
                    <td><?php echo $row['ORI_VENDA']; ?></td>
                    <td><?php echo $row['RPR_IN_DIVISAO']; ?></td>
                    <td><?php echo $row['CED_NO_CENTRO']; ?></td>
                    <td><?php echo $row['DOC_NU_DEVSERIE'] . " " . $row['DOC_NU_DEVNOTA']; ?></td>
                    <td><?php echo $Tradutor->formatar($row['DOC_VR_VALOR'], 'MOEDA');?></td>
                </tr>
                <?php
                $x++;
            }
            ?>
        </tbody>
    </table>
</div> 