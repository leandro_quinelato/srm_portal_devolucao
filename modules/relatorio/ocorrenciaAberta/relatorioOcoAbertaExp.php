<?php
date_default_timezone_set('Brazil/East');
header('Content-type: application/x-msdownload');
header('Content-Disposition: attachment; filename=relatorio_OcorrenciaAberta' .date("d-m-Y-His").'.xls');
header('Pragma: no-cache');
header('Expires: 0');

include_once( "../../../includes/Dao/DaoSistema.class.php" );
include_once( "../../../includes/Dao/RelatorioDao.class.php" );

include_once("include_novo/Tradutor.class.php");

$Tradutor = new Tradutor();

extract($_REQUEST);

$RelatorioDao = new RelatorioDao();

if(count($area) > 0 ){
    $auxArea = "";
    for ($i = 0; count($area) > $i; $i++) {
        if($auxArea != ""){
            $auxArea .= ", ";
        }
        $auxArea .= "'{$area[$i]}'";
    }
    $area = $auxArea;
}
$total = $RelatorioDao->relatorioContadorABertas($dataInicial, $dataFinal, $protocolo, $area);


$retorno = $RelatorioDao->relatorioOcorrenciaAberta($dataInicial, $dataFinal, $protocolo, $area);

?>

<div class="row">
    <div class="col-md-4 col-sm-6 col-xs-6">
        <div class="form-group">
            <div role="alert" class="alert alert-info alert-dismissible">
                Encontradas: <strong><?php echo $total[0]; ?></strong><br>
                Aprovadas Automaticamente - Gerente: <strong><?php echo $total[1]; ?></strong><br>
                Aprovadas Automaticamente - por Valor: <strong><?php echo $total[2]; ?></strong>
            </div>
        </div>                        
    </div>                            
</div>                            
<div class="table-responsive">
    <table class="table table-bordered table-striped table-sortable">
        <thead>
            <tr>
                <th>Dt Cadastro</th>
                <th>Protocolo</th>
                <th>Area</th>
                <th>Usuario</th>
                <th>Motivo Devolucao</th>
                <th>CD</th>
                <th>Valor</th>
            </tr>
        </thead>                               
        <tbody>
<?php
while ($dados = oci_fetch_object($retorno)) {
    ?>
                <tr>
                    <td><?php echo $dados->DATA_CADASTRO;  ?></td>
                    <td><?php echo $dados->DOC_CO_NUMERO;  ?></td>
                    <td><?php echo $dados->AREA_CADASTRANTE ;  ?></td>
                    <td><?php echo $dados->USU_NO_USERNAME ;  ?></td>
                    <td><?php echo $dados->TOC_NO_DESCRICAO ;  ?></td>
                    <td><?php echo $dados->CED_NO_CENTRO ;  ?></td>
                    <td><?php echo $Tradutor->formatar($dados->DOC_VR_VALOR, 'MOEDA') ;  ?></td>
                </tr>
<?php } ?>
        </tbody>
    </table>
</div>
