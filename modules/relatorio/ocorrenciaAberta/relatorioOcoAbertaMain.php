<?php
session_start();
include_once( "../../../includes/Dao/DaoSistema.class.php" );
include_once( "../../../includes/Dao/RelatorioDao.class.php" );

include_once("DaoGlobal.class.php");
include_once("SistemaDao.class.php");
include_once("GlbSistema.class.php");
include_once("GlbSistemaPerfil.class.php");
include_once("GlbUsuario.class.php");
include_once("GlbColaborador.class.php");
include_once("GlbPessoa.class.php");
include_once("GlbEndereco.class.php");
include_once("GlbTelefone.class.php");
include_once("include_novo/GlbTransportadora.class.php");
include_once("include_novo/GlbContatoTransportador.class.php");
include_once("include_novo/ContatoTransportadorDao.class.php");
include_once("GlbProgramaRotina.class.php");
include_once("GlbModuloPrograma.class.php");
include_once("GlbSistemaModulo.class.php");
include_once("PerfilUsuarioDao.class.php");
include_once("PerfilPermissaoDao.class.php");
include_once("integraSetSistemas.php");
include_once("class.phpmailer.php");
include_once("class.smtp.php");

include_once("include_novo/Tradutor.class.php");

$Tradutor = new Tradutor();
$integra = new integraSetSistemas("conf.xml");
$ObjUsuario = unserialize($integra->getSistemaIntegra());

//echo "<pre>";
//print_r($ObjUsuario->getObjSistemaPerfil()->getNomeSistemaPerfil());
//echo "</pre><br><br><br>";


include_once("include_novo/Tradutor.class.php");

$Tradutor = new Tradutor();

extract($_REQUEST);

$RelatorioDao = new RelatorioDao();

if(count($area) > 0 ){
    $auxArea = "";
    for ($i = 0; count($area) > $i; $i++) {
        if($auxArea != ""){
            $auxArea .= ", ";
        }
        $auxArea .= "'{$area[$i]}'";
    }
    $area = $auxArea;
}
$total = $RelatorioDao->relatorioContadorABertas($dataInicial, $dataFinal, $protocolo, $area, $ObjUsuario);


$retorno = $RelatorioDao->relatorioOcorrenciaAberta($dataInicial, $dataFinal, $protocolo, $area , $ObjUsuario);

?>

<div class="row">
    <div class="col-md-4 col-sm-6 col-xs-6">
        <div class="form-group">
            <div role="alert" class="alert alert-info alert-dismissible">
                Encontradas: <strong><?php echo $total[0]; ?></strong><br>
                Aprovadas Automaticamente - Gerente: <strong><?php echo $total[1]; ?></strong><br>
                Aprovadas Automaticamente - por Valor: <strong><?php echo $total[2]; ?></strong>
            </div>
        </div>                        
    </div>                            
</div>                            
<div class="table-responsive">
    <table class="table table-bordered table-striped table-sortable">
        <thead>
            <tr>
                <th>Dt Cadastro</th>
                <th>Protocolo</th>
                <th>Área</th>
                <th>Usuário</th>
                <th>Motivo Devolução</th>
                <th>CD</th>
                <th>Valor</th>
            </tr>
        </thead>                               
        <tbody>
<?php
while ($dados = oci_fetch_object($retorno)) {
    ?>
                <tr>
                    <td><?php echo $dados->DATA_CADASTRO;  ?></td>
                    <td><?php echo $dados->DOC_CO_NUMERO;  ?></td>
                    <td><?php echo $dados->AREA_CADASTRANTE ;  ?></td>
                    <td><?php echo $dados->USU_NO_USERNAME ;  ?></td>
                    <td><?php echo $dados->TOC_NO_DESCRICAO ;  ?></td>
                    <td><?php echo $dados->CED_NO_CENTRO ;  ?></td>
                    <td><?php echo $Tradutor->formatar($dados->DOC_VR_VALOR, 'MOEDA') ;  ?></td>
                </tr>
<?php } ?>
        </tbody>
    </table>
</div>
<script language="JavaScript">
    $(function () {
        datatables.init();
    });
</script>