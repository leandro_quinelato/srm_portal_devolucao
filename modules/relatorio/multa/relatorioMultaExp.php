<?php
date_default_timezone_set('Brazil/East');
header('Content-type: application/x-msdownload');
header('Content-Disposition: attachment; filename=relatorio_multa' .date("d-m-Y-His").'.xls');
header('Pragma: no-cache');
header('Expires: 0');

ini_set('max_execution_time', -1);

include_once( "../../../includes/Dao/DaoSistema.class.php" );
include_once( "../../../includes/Dao/RelatorioDao.class.php" );

include_once("include_novo/Tradutor.class.php");

$Tradutor = new Tradutor();

//echo "<pre>";
//print_r($_REQUEST);
//echo "</pre>";

extract($_REQUEST);

$RelatorioDao = new RelatorioDao();

if($codResponsavel == "TOD"){
    $codResponsavel = null;
}

if(count($codDivisao) > 0 ){
    $auxDivisao = "";
    for ($i = 0; count($codDivisao) > $i; $i++) {
        if($auxDivisao != ""){
            $auxDivisao .= ", ";
        }
        $auxDivisao .= "'{$codDivisao[$i]}'";
    }
    $codDivisao = $auxDivisao;
}

$retorno = $RelatorioDao->relatorioMultaDevolucao($dataInicial, $dataFinal, $codResponsavel, $fab_co_numero, $setor, $codCliente, $codRede, $codDivisao);
?>

<div class="table-responsive">
    <table class="table table-bordered table-striped table-sortable">
        <thead>
            <tr>
                <th>Codigo</th>
                <th>Responsavel</th>
                <th>Qt nota</th>
                <th>Valor Devolução</th>
                <th>Valor Multa</th>

            </tr>
        </thead>                               
        <tbody>
<?php
while (OCIFetchInto($retorno, $row, OCI_ASSOC)) {
    ?>
                <tr>
                    <td><?php echo $row['CODIGO'];  ?></td>
                    <td><?php echo $row['RESPONSAVEL'];  ?></td>
                    <td><?php echo $row['QNTD_NOTA'];  ?></td>
                    <td><?php echo $Tradutor->formatar($row['VALOR'], 'MOEDA'); ?></td>
                    <td><?php echo $Tradutor->formatar($row['VALOR_MULTA'], 'MOEDA'); ?></td>
                </tr>
<?php } ?>
            <tr>
                <td>98789789</td>
                <td>ASSOC AFAM DE ASSISTENCIA FARMACEUTICA</td>
                <td>2</td>
                <td>R$1485,16</td>
                <td>105,00</td>
            </tr>
            <tr>
                <td>546545645</td>
                <td>ADM</td>
                <td>15</td>
                <td>R$3.326,00</td>
                <td>220,00</td>
            </tr>
            <tr>
                <td>98789789</td>
                <td>ASSOC AFAM DE ASSISTENCIA FARMACEUTICA</td>
                <td>2</td>
                <td>R$1485,16</td>
                <td>105,00</td>
            </tr>
            <tr>
                <td>546545645</td>
                <td>ADM</td>
                <td>15</td>
                <td>R$3.326,00</td>
                <td>220,00</td>
            </tr>
            <tr>
                <td>98789789</td>
                <td>ASSOC AFAM DE ASSISTENCIA FARMACEUTICA</td>
                <td>2</td>
                <td>R$1485,16</td>
                <td>105,00</td>
            </tr>

        </tbody>
    </table>
</div>




