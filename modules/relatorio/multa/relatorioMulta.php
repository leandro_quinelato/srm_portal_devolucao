<?php
include_once( "../../../includes/Dao/DaoSistema.class.php" );
include_once( "../../../includes/Dao/RelatorioDao.class.php" );
include_once( "../../../includes/Dao/OcorrenciaDao.class.php" );

$RelatorioDao = new RelatorioDao();
$OcorrenciaDao = new OcorrenciaDao();
$resultFab = $OcorrenciaDao->industria();

$result = $RelatorioDao->listaOrigemVendaNota();
$array = array();
$array['ADM'] = 'Administrativo';
$array['CLIE'] = 'Cliente';
$array['DIRT'] = 'Diretoria';
$array['ESTQ'] = 'Estoque';
$array['INDU'] = 'Industria';
$array['REPR'] = 'Representante';
$array['GLOB'] = 'GlobalPharma';
$array['GER'] = 'Gerente';
$array['CENT'] = 'Central Hospitalar';
$array['TRAN'] = 'Transportadora';
$array['TELE'] = 'Televendas';
$array['MESA'] = 'Mesa de Negocios';

while (OCIFetchInto($result, $row, OCI_ASSOC)) {
    $array[$row['TPORIGVENDA']] = $row['NOME'];
}
natsort($array);
?>


<!DOCTYPE html>
<html lang="en">
    <head>        
        <!-- meta section -->
        <title>Relatórios - Multa</title>
        <?php include("../../../library/head.php"); ?>
        <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/libRelatorio.js"></script>
    </head>
    <body>
        <!-- set loading layer -->
        <!--<div class="dev-page-loading preloader"></div>-->
        <!-- ./set loading layer -->

        <!-- page wrapper -->
        <div class="dev-page">

            <!-- page header -->    
            <?php include("../../../library/topo.php"); ?>
            <!-- ./page header -->

            <!-- page container -->
            <div class="dev-page-container">

                <!-- page sidebar -->
                <?php include("../../../library/menu.php"); ?>
                <!-- ./page sidebar -->

                <!-- page content -->
                <div class="dev-page-content">                    
                    <!-- page content container -->
                    <div class="container">

                        <!-- page title -->
                        <div class="page-title">
                            <h1>Relatório de Multas</h1>

                            <ul class="breadcrumb">
                                <li><a href="#">Relatórios</a></li>
                                <li>Multa</li>
                            </ul>
                        </div>                        
                        <!-- ./page title -->


                        <!-- datatables plugin -->
                        <div class="wrapper wrapper-white">
                            <form id="FormRelatorioMulta">
                                <div class="row">
                                    <div class="col-md-2 col-sm-3 col-xs-3">
                                        <div class="form-group">
                                            <label>Início</label>                            
                                            <input type="text" class="form-control datepicker" id="dataInicial" name="dataInicial">
                                        </div> 
                                    </div>  
                                    <div class="col-md-2 col-sm-3 col-xs-3">
                                        <div class="form-group">
                                            <label>Fim</label>                            
                                            <input type="text" class="form-control datepicker" id="dataFinal" name="dataFinal">
                                        </div> 
                                    </div>   
                                    <div class="col-md-2 col-sm-3 col-xs-3">
                                        <div class="form-group">
                                            <label>Cliente</label>
                                            <input type="text" class="form-control" placeholder="Cliente" onKeyPress="fMascara('numero', event, this)" id="codCliente" name="codCliente">
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-3 col-xs-3">
                                        <div class="form-group">
                                            <label>Rede</label>
                                            <input type="text" class="form-control" placeholder="Rede" onKeyPress="fMascara('numero', event, this)" id="codRede" name="codRede">
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">                        
                                        <div class="form-group">
                                            <label>Responsável</label>
                                            <select class="form-control selectpicker"  id="codResponsavel" name="codResponsavel" onchange="javascript:filtroAdicional(this.value)" >
                                                <option value="TOD">Todos</option>
                                                <?php
                                                foreach ($array as $tipo => $val) {
                                                    ?>
                                                    <option value="<?php echo $tipo; ?>"><?php echo $val; ?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>                        
                                    </div>
                                    <div id="filtroIndustria" style="display: none">
                                        <div class="col-md-3 col-sm-6 col-xs-12">                        
                                            <div class="form-group">
                                                <label>IND&Uacute;STRIA</label>
                                                <select class="form-control selectpicker"  id="fab_co_numero" name="fab_co_numero">
                                                    <option value="TOD">Todas</option>
                                                    <?php
                                                    while (OCIFetchInto($resultFab, $row_fab, OCI_ASSOC)) {
                                                        ?>
                                                        <option value="<?php echo $row_fab['FAB_CO_NUMERO'] ?>"> <?php echo $row_fab['FAB_NO_RAZAO_SOCIAL'] ?></option>
                                                                <?php
                                                            }
                                                            ?>
                                                </select>
                                            </div>                        
                                        </div> 
                                    </div>
                                    <div id="filtroRep" style="display: none">
                                        <div class="col-md-3 col-sm-6 col-xs-12">                        
                                            <div class="form-group">
                                            <label>Setor</label>
                                            <input type="text" class="form-control" placeholder="Setor" onKeyPress="fMascara('numero', event, this)" id="setor" name="setor">
                                            </div>
                                        </div>    
                                    </div>    
                                    <div class="col-md-3 col-sm-6 col-xs-12">                        
                                        <div class="form-group">
                                            <label>Divisão</label>
                                            <select class="form-control selectpicker" multiple id="codDivisao" name="codDivisao[]">
                                                <option value="A">Alimentar</option>
                                                <option value="O">Dist. Direta</option>
                                                <option value="F">Farma</option>
                                                <option value="H">Hospitalar</option>
                                            </select>
                                        </div>                        
                                    </div>  
                                </div>  
                            </form>
                            <div class="row">
                                <div class="col-md-2">
                                    <a class="btn btn-primary" onclick="javascript:relatorioMulta();">Pesquisar</a>
                                </div>                                                                                  
                            </div>
                            <div class="form-group">
                                <a type="button" class="btn btn-success btn-xs" onclick="javascript:relatorioMultaExport();"><i class="glyphicon glyphicon-download-alt"></i> Exportar</a>
                            </div>
                            <div id="areaRelatorio">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped table-sortable">
                                        <thead>
                                            <tr>
                                                <th>Codigo</th>
                                                <th>Responsavel</th>
                                                <th>Qt nota</th>
                                                <th>Valor Devolução</th>
                                                <th>Valor Multa</th>

                                            </tr>
                                        </thead>                               
                                        <tbody>
                                            <tr>
                                                <td>546545645</td>
                                                <td>ADM</td>
                                                <td>15</td>
                                                <td>R$3.326,00</td>
                                                <td>220,00</td>
                                            </tr>
                                            <tr>
                                                <td>98789789</td>
                                                <td>ASSOC AFAM DE ASSISTENCIA FARMACEUTICA</td>
                                                <td>2</td>
                                                <td>R$1485,16</td>
                                                <td>105,00</td>
                                            </tr>
                                            <tr>
                                                <td>546545645</td>
                                                <td>ADM</td>
                                                <td>15</td>
                                                <td>R$3.326,00</td>
                                                <td>220,00</td>
                                            </tr>
                                            <tr>
                                                <td>98789789</td>
                                                <td>ASSOC AFAM DE ASSISTENCIA FARMACEUTICA</td>
                                                <td>2</td>
                                                <td>R$1485,16</td>
                                                <td>105,00</td>
                                            </tr>
                                            <tr>
                                                <td>546545645</td>
                                                <td>ADM</td>
                                                <td>15</td>
                                                <td>R$3.326,00</td>
                                                <td>220,00</td>
                                            </tr>
                                            <tr>
                                                <td>98789789</td>
                                                <td>ASSOC AFAM DE ASSISTENCIA FARMACEUTICA</td>
                                                <td>2</td>
                                                <td>R$1485,16</td>
                                                <td>105,00</td>
                                            </tr>




                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>                        
                        <!-- ./datatables plugin -->


                        <!-- Copyright -->
                        <?php include("../../../library/copyright.php"); ?>
                        <!-- ./Copyright -->

                    </div>
                    <!-- ./page content container -->                                       
                </div>
                <!-- ./page content -->                                                
            </div>  
            <!-- ./page container -->

            <!-- right bar -->

            <!-- ./right bar -->            

            <!-- page footer -->    
            <?php include("../../../library/footer.php"); ?>
            <!-- ./page footer -->

            <!-- page search -->

            <!-- page search -->            
        </div>
        <!-- ./page wrapper -->


        <!-- javascripts -->
        <?php include("../../../library/rodape.php"); ?>

        <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js"></script>
        <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/plugins/datatables/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/plugins/smartwizard/jquery.smartWizard.js"></script>
        <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/plugins/sortable/sortable.min.js"></script>
        <!-- ./javascripts -->


    </body>
</html>