<!DOCTYPE html>
<html lang="en">
    <head>        
        <!-- meta section -->
        <title>Liberação de notas</title>
        <?php include("../../../library/head.php"); ?>
        <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/libRelatorio.js"></script>
    </head>
    <body>
        <!-- set loading layer -->
        <div class="dev-page-loading preloader"></div>
        <!-- ./set loading layer -->

        <!-- page wrapper -->
        <div class="dev-page">

            <!-- page header -->    
            <?php include("../../../library/topo.php"); ?>
            <!-- ./page header -->

            <!-- page container -->
            <div class="dev-page-container">

                <!-- page sidebar -->
                <?php include("../../../library/menu.php"); ?>
                <!-- ./page sidebar -->

                <!-- page content -->
                <div class="dev-page-content">                    
                    <!-- page content container -->
                    <div class="container">

                        <!-- page title -->
                        <div class="page-title">
                            <h1>Liberação de notas</h1>

                            <ul class="breadcrumb">
                                <li><a href="#">Relatório</a></li>
                                <li>Liberação de notas</li>
                            </ul>
                        </div>                        
                        <!-- ./page title -->



                        <!-- datatables plugin -->
                        <div class="wrapper wrapper-white">
                            <form id="FormNotaLiberada">
                                <div class="row">
                                    <div class="col-md-2 col-sm-3 col-xs-3">
                                        <div class="form-group">
                                            <label>Início</label>                            
                                            <input type="text" class="form-control datepicker" id="dataInicial" name="dataInicial">
                                        </div> 
                                    </div>                               
                                    <div class="col-md-2 col-sm-3 col-xs-3">
                                        <div class="form-group">
                                            <label>Fim</label>                            
                                            <input type="text" class="form-control datepicker" id="dataFinal" name="dataFinal">
                                        </div> 
                                    </div>
                                    <div class="col-md-3 col-sm-3 col-xs-6">                        
                                        <div class="form-group">
                                            <label>Setor</label>
                                            <input type="text" placeholder="Setor" class="form-control" id="setor" name="setor">
                                        </div>                       
                                    </div> 

                                    <div class="col-md-5 col-sm-3 col-xs-6">                      
                                        <div class="form-group">
                                            <label>Multa</label>
                                            <select class="form-control selectpicker" id="multa" name="multa">
                                                <option value="TOD">Todas</option>
                                                <option value="0">Não</option>
                                                <option value="1">Sim</option>
                                            </select>
                                        </div>                        
                                    </div> 
                                    
                                    <div class="col-md-5 col-sm-3 col-xs-6">                      
                                        <div class="form-group">
                                            <label>Tipo Relatório</label>
                                            <select class="form-control selectpicker" id="tipoRel" name="tipoRel">
                                                <option value="S">Simples</option>
                                                <option value="D">Detalhado</option>
                                            </select>
                                        </div>                        
                                    </div>  


                                </div>
                            </form>    

                            <div class="row">
                                <div class="col-md-2">
                                    <a class="btn btn-primary" onclick="javascript:relatorioNotaLiberada();">Pesquisar</a>
                                </div>                                                                                  
                            </div>
                            <div class="form-group">
                                <button class="btn btn-success btn-xs" type="button" onclick="javascript: relatorioNotaLiberadaExport();"><i class="glyphicon glyphicon-download-alt"></i> Exportar</button>
                            </div>

                            <div id="areaRelatorio">

                            </div>
                        </div>                        
                        <!-- ./datatables plugin -->


                        <!-- Copyright -->
                        <?php include("../../../library/copyright.php"); ?>
                        <!-- ./Copyright -->

                    </div>
                    <!-- ./page content container -->                                       
                </div>
                <!-- ./page content -->                                                
            </div>  
            <!-- ./page container -->

            <!-- right bar -->

            <!-- ./right bar -->            

            <!-- page footer -->    
            <?php include("../../../library/footer.php"); ?>
            <!-- ./page footer -->

            <!-- page search -->

            <!-- page search -->            
        </div>
        <!-- ./page wrapper -->


        <!-- javascripts -->
        <?php include("../../../library/rodape.php"); ?>

        <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js"></script>
        <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/plugins/datatables/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/plugins/smartwizard/jquery.smartWizard.js"></script>
        <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/plugins/sortable/sortable.min.js"></script>
        <!-- ./javascripts -->


        <script type="text/javascript" src="js/plugins/spectrum/spectrum.js"></script>
        <script type="text/javascript" src="js/plugins/tags-input/jquery.tagsinput.min.js"></script>                

        <script type="text/javascript" src="js/demo.js"></script>



    </body>
</html>