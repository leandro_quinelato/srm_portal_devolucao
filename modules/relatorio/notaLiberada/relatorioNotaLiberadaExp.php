<?php
ini_set('display_errors', 1);
date_default_timezone_set('Brazil/East');
header('Content-type: application/x-msdownload');
header('Content-Disposition: attachment; filename=relatorio_notaLiberada' .date("d-m-Y-His").'.xls');
header('Pragma: no-cache');
header('Expires: 0');

ini_set('max_execution_time', -1);


include_once( "../../../includes/Dao/DaoSistema.class.php" );
include_once( "../../../includes/Dao/RelatorioDao.class.php" );
include_once( "../../../includes/Multa.class.php" );
include_once( "../../../includes/Dao/MultaDao.class.php" );

include_once("include_novo/Tradutor.class.php");


$ObjMulta = new Multa(); 
$MultaDao = new MultaDao(); 
$result = $MultaDao->consultar($ObjMulta);
OCIFetchInto ($result, $parMulta, OCI_ASSOC);



$Tradutor = new Tradutor();
//die('geovanni4');

//echo "<pre>";
//print_r($_REQUEST);
//echo "</pre>";

extract($_REQUEST);

//$dataInicial = '01/04/2016';
//$dataFinal = '20/04/2016';
$RelatorioDao = new RelatorioDao();
//die();
$retorno = $RelatorioDao->relatorioNotaLiberada($dataInicial, $dataFinal, $setor, $multa);


$teste = array();
$cont = 0;
$qtdNotaMulta = 0;
$setorAtual = 0;
$setorAnterior = 0;
$valorMultaDevolucao = $parMulta['MUL_VR_MULTA'];

while (OCIFetchInto($retorno, $row, OCI_ASSOC)) {

    $teste[$row['RPR_CO_NUMERO']] ['responsavel'] = $row['RPR_NO_LABEL'];

    if ($row['LBN_IN_MULTA'] == 1) {
        $teste[$row['RPR_CO_NUMERO']]['notas_multa'][$cont]['cliente'] = $row['CLI_CO_NUMERO'];
        $teste[$row['RPR_CO_NUMERO']]['notas_multa'][$cont]['serie'] = $row['NTO_NU_SERIE'];
        $teste[$row['RPR_CO_NUMERO']]['notas_multa'][$cont]['numero'] = $row['NTO_NU_NOTA'];
        $teste[$row['RPR_CO_NUMERO']]['notas_multa'][$cont]['emissao'] = $row['NTO_DT_NOTA'];
        $teste[$row['RPR_CO_NUMERO']]['notas_multa'][$cont]['dtLibera'] = $row['DATA_LIBERACAO'];
		$teste[$row['RPR_CO_NUMERO']]['notas_multa'][$cont]['responsavel'] = $row['USU_NO_USERNAME'];
		$teste[$row['RPR_CO_NUMERO']]['notas_multa'][$cont]['motivo'] = $row['TOC_NO_DESCRICAO'];

        if (isset($row['LBN_TX_DESCRICAO'])) {
            $descricao = utf8_encode($row['LBN_TX_DESCRICAO']->load());
        } else {
            $descricao = "";
        }

        $teste[$row['RPR_CO_NUMERO']]['notas_multa'][$cont]['descricao'] = $descricao;
    } else {

        $teste[$row['RPR_CO_NUMERO']]['notas'][$cont]['cliente'] = $row['CLI_CO_NUMERO'];
        $teste[$row['RPR_CO_NUMERO']]['notas'][$cont]['serie'] = $row['NTO_NU_SERIE'];
        $teste[$row['RPR_CO_NUMERO']]['notas'][$cont]['numero'] = $row['NTO_NU_NOTA'];
        $teste[$row['RPR_CO_NUMERO']]['notas'][$cont]['emissao'] = $row['NTO_DT_NOTA'];
        $teste[$row['RPR_CO_NUMERO']]['notas'][$cont]['dtLibera'] = $row['DATA_LIBERACAO'];
		$teste[$row['RPR_CO_NUMERO']]['notas'][$cont]['responsavel'] = $row['USU_NO_USERNAME'];
		$teste[$row['RPR_CO_NUMERO']]['notas'][$cont]['motivo'] = $row['TOC_NO_DESCRICAO'];		

        if (isset($row['LBN_TX_DESCRICAO'])) {
            $descricao = utf8_encode($row['LBN_TX_DESCRICAO']->load());
        } else {
            $descricao = "";
        }

        $teste[$row['RPR_CO_NUMERO']]['notas'][$cont]['descricao'] = $descricao;
//    $teste[$row['RPR_CO_NUMERO']] ['notas'][$cont]['multa'] = $row['LBN_IN_MULTA'];
    }
    $cont++;
}
?>

<div class="relatorio_lista">

    <?php
    foreach ($teste as $setor => $valor) {
        $qtd_notaMulta = 0;
        $qtd_notaSemMulta = 0;
        $qtd_notaTotal = 0;
        $vlrmulta = 0;
        $x = 0;

        if (isset($valor['notas_multa']))
            $qtd_notaMulta = count($valor['notas_multa']);
        if (isset($valor['notas']))
            $qtd_notaSemMulta = count($valor['notas']);

        $qtd_notaTotal = ($qtd_notaMulta + $qtd_notaSemMulta);
        $vlrmulta = ($qtd_notaMulta * $valorMultaDevolucao)
        ?>
        
	<div class="table-responsive">
            <table class="table">
            	<thead>
                    <tr>
                        <th>Setor</th>
                        <th>Respons&aacute;vel</th>
                        <th>Qtd. Nota Liberada</th>
                        <th>Valor Multa</th>					
                    </tr>
    			</thead>
                <?php
                $class = ( ( $x % 2 ) == 0 ? "#FFFFF" : "#E8E8E8" );
                //$x++;
                ?>
                <tbody>
                    <tr>
                        <td><?php echo $setor ?></td>
                        <td><?php echo $teste[$setor]['responsavel']; ?></td>
                        <td><?php echo $qtd_notaTotal; ?></td>
                        <td><?php echo 'R$ ' . number_format($vlrmulta, 2, ',', '.'); ?></td>
                    </tr>
    			</tbody>
            </table>
<?php
if ($tipoRel != "S") {
?>
                    
            <table border="<?php echo $border; ?>" width="100%" cellpadding="0" cellspacing="0">
            	<thead>
                    <tr class="titulo_tabela_listagem" bgcolor="#BEBEBE">
                        <th>Cliente</th>
                        <th>S&eacute;rie</th>
                        <th>Nota</th>
                        <th>Data Emiss&atilde;o</th>					
                        <th>Descri&ccedil;&atilde;o</th>	
                        <th>Dt Libera&ccedil;&atilde;o</th>	
                        <th>Respons&aacute;vel Libera&ccedil;&atilde;o</th>	
                        <th>Motivo Devolu&ccedil;&atilde;o</th>													
                        <th>Multa</th>				
                    </tr>
    			</thead>
                <?php
                if ($qtd_notaMulta > 0) {
                    foreach ($valor['notas_multa'] as $informacao) {
                        $class = ( ( $x % 2 ) == 0 ? "#FFFFF" : "#E8E8E8" );
                        $x++;
                        ?>
                        <tr style="background:#e8eb98;">
                            <td ><?php echo $informacao['cliente'] ?></td>
                            <td ><?php echo $informacao['serie']; ?></td>
                            <td ><?php echo $informacao['numero']; ?></td>
                            <td ><?php echo $informacao['emissao']; ?></td>
                            <td><?php echo $informacao['descricao']; ?></td>
                            <td><?php echo $informacao['dtLibera']; ?></td>
                            <td><?php echo $informacao['responsavel']; ?></td>
                            <td><?php echo $informacao['motivo']; ?></td>							
                            <td>Sim</td>
                        </tr>
                        <?php
                    }
                }
                if ($qtd_notaSemMulta > 0) {
                    foreach ($valor['notas'] as $informacao) {
                        $class = ( ( $x % 2 ) == 0 ? "#FFFFF" : "#E8E8E8" );
                        $x++;
                        ?>
                    <tbody>
                        <tr>
                            <td ><?php echo $informacao['cliente'] ?></td>
                            <td ><?php echo $informacao['serie']; ?></td>
                            <td ><?php echo $informacao['numero']; ?></td>
                            <td ><?php echo $informacao['emissao']; ?></td>
                            <td ><?php echo $informacao['descricao']; ?></td>
                            <td ><?php echo $informacao['dtLibera']; ?></td>
                            <td><?php echo $informacao['responsavel']; ?></td>
                            <td><?php echo $informacao['motivo']; ?></td>							
                            <td>---</td>
                        </tr>
                    </tbody>
                        <?php
                    }
                }
                ?>
            </table>
<?php                    
}                    
?>                    
        </div>

        <?php
    }
    ?>

</div>


<?php
die();
if ($tipoRel == "S") {
    ?>    

    <div class="table-responsive">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Setor</th>
                    <th>Responsável</th>	
                    <th>Qtd Nota</th>
                    <th>Valor Multa</th>
                </tr>
            </thead>                               
            <tbody>
                <tr class="Zebra">
                    <td>408</td>
                    <td>EDINALDO-SAO CARLOS-03/10/2006</td>
                    <td>50,00</td>
                    <td>46493</td>
                </tr>
                <tr>
                    <td>808</td>
                    <td>MARILIA - FRANCA - 14/08/2012</td>
                    <td>50,00</td>
                    <td>46493</td>
                </tr>                                          
            </tbody>
        </table>
    </div>

    <?php
} else {
    ?>

    <div class="table-responsive">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Setor</th>
                    <th>Responsável</th>	
                    <th>Valor Multa</th>
                    <th>Cliente</th>
                    <th>Nota</th>
                    <th>Data Emissão</th>	
                    <th>Descrição</th>	
                    <th>Dt Liberação</th>
                </tr>
            </thead>                               
            <tbody>
                <tr class="Zebra">
                    <td>408</td>
                    <td>EDINALDO-SAO CARLOS-03/10/2006</td>
                    <td>50,00</td>
                    <td>46493</td>
                    <td>53 548588</td>
                    <td>01/04/16</td>
                    <td>DESVIO DE QUALIDADE UMIDIFICADOR NS qua 24/02/2016 18:54 WILIAN</td>
                    <td>08/04/16</td>
                </tr>
                <tr class="Zebra">
                    <td></td>
                    <td></td>
                    <td>50,00</td>
                    <td>46493</td>
                    <td>53 548588</td>
                    <td>01/04/16</td>
                    <td>DESVIO DE QUALIDADE UMIDIFICADOR NS qua 24/02/2016 18:54 WILIAN</td>
                    <td>08/04/16</td>
                </tr>
                <tr class="Zebra">
                    <td></td>
                    <td></td>
                    <td>50,00</td>
                    <td>46493</td>
                    <td>53 548588</td>
                    <td>01/04/16</td>
                    <td>DESVIO DE QUALIDADE UMIDIFICADOR NS qua 24/02/2016 18:54 WILIAN</td>
                    <td>08/04/16</td>
                </tr>
                <tr>
                    <td>808</td>
                    <td>MARILIA - FRANCA - 14/08/2012</td>
                    <td>50,00</td>
                    <td>46493</td>
                    <td>53 548588</td>
                    <td>01/04/16</td>
                    <td>DESVIO DE QUALIDADE UMIDIFICADOR NS qua 24/02/2016 18:54 WILIAN</td>
                    <td>08/04/16</td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td>50,00</td>
                    <td>46493</td>
                    <td>53 548588</td>
                    <td>01/04/16</td>
                    <td>DESVIO DE QUALIDADE UMIDIFICADOR NS qua 24/02/2016 18:54 WILIAN</td>
                    <td>08/04/16</td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td>50,00</td>
                    <td>46493</td>
                    <td>53 548588</td>
                    <td>01/04/16</td>
                    <td>DESVIO DE QUALIDADE UMIDIFICADOR NS qua 24/02/2016 18:54 WILIAN</td>
                    <td>08/04/16</td>
                </tr>                                              
                <tr>
                    <td></td>
                    <td></td>
                    <td>50,00</td>
                    <td>46493</td>
                    <td>53 548588</td>
                    <td>01/04/16</td>
                    <td>DESVIO DE QUALIDADE UMIDIFICADOR NS qua 24/02/2016 18:54 WILIAN</td>
                    <td>08/04/16</td>
                </tr>                                              

            </tbody>
        </table>
    </div>
    <?php
}
?>

<script language="JavaScript">
    $(function () {
        datatables.init();
    });
</script>