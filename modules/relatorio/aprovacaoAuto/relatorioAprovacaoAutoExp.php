<?php
date_default_timezone_set('Brazil/East');
header('Content-type: application/x-msdownload');
header('Content-Disposition: attachment; filename=relatorio_AprovacaoAutomatica' .date("d-m-Y-His").'.xls');
header('Pragma: no-cache');
header('Expires: 0');

ini_set('max_execution_time', -1);

include_once( "../../../includes/Dao/DaoSistema.class.php" );
include_once( "../../../includes/Dao/RelatorioDao.class.php" );

include_once("include_novo/Tradutor.class.php");

$Tradutor = new Tradutor();

//echo "<pre>";
//print_r($_REQUEST);
//echo "</pre>";

extract($_REQUEST);


$RelatorioDao = new RelatorioDao();

$retorno = $RelatorioDao->relatorioAprovacaoAutomatica($dataInicial, $dataFinal, $protocolo, $codCliente, $codRede, $ced_co_numero, $tipoAprovacao)

?>


<div class="table-responsive">
    <table class="table table-bordered table-striped table-sortable">
        <thead>
            <tr>
                <th>Protocolo</th>
                <th>Data</th>
                <th>Dt Aprovação</th>
                <th>Tipo Aprovação</th>
                <th>Ger. Contas</th>
                <th>Ger. Regional</th>
                <th>Ger. Divisional</th>
                <th>Motivo</th>
                <th>CD</th>
                <th>Valor</th>
                <th>Rede</th>
                <th>Cod Cliente</th>
                <th>Cliente</th>
            </tr>
        </thead>                               
        <tbody>
<?php
while ($dados = oci_fetch_object($retorno)) {
    ?>
                <tr>
                    <td><?php echo $dados->DOC_CO_NUMERO;  ?></td>
                    <td><?php echo $dados->DATA_CADASTRO;  ?></td>
                    <td><?php echo $dados->DATA_APROVACAO;  ?></td>
                    <td><?php echo $dados->TIPO_APROVACAO;  ?></td>
                    <td><?php echo $dados->EQUIPE ." - ". $dados->GERENTE_CONTAS;  ?></td>
                    <td><?php echo $dados->COD_REGIONAL ." - ". $dados->GERENTE_REGIONAL;  ?></td>
                    <td><?php echo $dados->COD_DIVISIONAL ." - ". $dados->GERENTE_DIVISIONAL;  ?></td>
                    <td><?php echo $dados->TOC_NO_DESCRICAO ;  ?></td>
                    <td><?php echo $dados->CED_NO_CENTRO ;  ?></td>
                    <td><?php echo $Tradutor->formatar($dados->DOC_VR_VALOR, 'MOEDA') ;  ?></td>
                    <td><?php echo $dados->RED_CO_NUMERO ;  ?></td>
                    <td><?php echo $dados->CLI_CO_NUMERO;  ?></td>
                    <td><?php echo $dados->CLI_NO_FANTASIA;  ?></td>
  
                </tr>
<?php } ?>


        </tbody>
    </table>
</div>
