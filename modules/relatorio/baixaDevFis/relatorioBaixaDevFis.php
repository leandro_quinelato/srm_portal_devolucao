<?php

include_once( "../../../includes/Dao/DaoSistema.class.php" );
include_once( "../../../includes/Dao/RelatorioDao.class.php" );
include_once( "../../../includes/Dao/OcorrenciaDao.class.php" );
include_once( "../../../includes/Status.class.php" );
include_once( "../../../includes/Dao/StatusDao.class.php" );
include_once( "../../../includes/Rota.class.php" );
include_once( "../../../includes/Dao/RotaDao.class.php" );

include_once("include_novo/GlbCentroDistribuicao.class.php");

$RelatorioDao = new RelatorioDao();
$OcorrenciaDao = new OcorrenciaDao();


/* FILTRO CEDIS*/
$GlbCentroDistribuicao = new GlbCentroDistribuicao();
$GlbCentroDistribuicao = $OcorrenciaDao->filtroCentroDis();

?>


<!DOCTYPE html>
<html lang="en">
    <head>        
        <!-- meta section -->
        <title>Relatórios - Baixa Devolução/Fiscal</title>
        <?php include("../../../library/head.php"); ?>
        <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/libRelatorio.js"></script>
    </head>
    <body>
        <!-- set loading layer -->
        <!--<div class="dev-page-loading preloader"></div>-->
        <!-- ./set loading layer -->

        <!-- page wrapper -->
        <div class="dev-page">

            <!-- page header -->    
            <?php include("../../../library/topo.php"); ?>
            <!-- ./page header -->

            <!-- page container -->
            <div class="dev-page-container">

                <!-- page sidebar -->
                <?php include("../../../library/menu.php"); ?>
                <!-- ./page sidebar -->

                <!-- page content -->
                <div class="dev-page-content">                    
                    <!-- page content container -->
                    <div class="container">

                        <!-- page title -->
                        <div class="page-title">
                            <h1>Relatório de Baixa Devolução/Fiscal</h1>

                            <ul class="breadcrumb">
                                <li><a href="#">Relatórios</a></li>
                                <li>Baixa Devolução/Fiscal</li>
                            </ul>
                        </div>                        
                        <!-- ./page title -->


                        <!-- datatables plugin -->
                        <div class="wrapper wrapper-white">
                            <form id="FormBaixaDevFis">
                                <div class="row">
                                    <div class="col-md-2 col-sm-3 col-xs-3">
                                        <div class="form-group">
                                            <label>Início</label>                            
                                            <input type="text" class="form-control datepicker" id="dataInicial" name="dataInicial">
                                        </div> 
                                    </div>  
                                    <div class="col-md-2 col-sm-3 col-xs-3">
                                        <div class="form-group">
                                            <label>Fim</label>                            
                                            <input type="text" class="form-control datepicker" id="dataFinal" name="dataFinal">
                                        </div> 
                                    </div> 
                                    <div class="col-md-2 col-sm-3 col-xs-3">
                                        <div class="form-group">
                                            <label>Protocolo</label>
                                            <input type="text" class="form-control" placeholder="Protocolo" onKeyPress="fMascara('numero', event, this)" id="protocolo" name="protocolo">
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-6">                        
                                        <div class="form-group">
                                            <label>Passo</label>
                                            <select class="form-control selectpicker" id="passo" name="passo">    
                                                <option value=""></option>
                                                <option value="4">Devolu&ccedil;&atilde;o</option>
                                                <option value="5">Fiscal </option>
                                            </select>
                                        </div>                        
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-6">                        
                                        <div class="form-group">
                                            <label>CD</label>
                                            <select class="form-control selectpicker" id="ced_co_numero" name="ced_co_numero">
                                                <option value="TOD">Todos</option>
                                                <?php
                                                foreach ($GlbCentroDistribuicao as $ObjCedis) {
                                                    ?>
                                                    <option value="<?php echo $ObjCedis->getCodigo(); ?>"><?php echo $ObjCedis->getNome(); ?></option>
                                                    <?php
                                                }
                                                ?>    
                                            </select>
                                        </div>                        
                                    </div>  
                                </div>
                            </form>
                            <div class="row">
                                <div class="col-md-2">
                                    <a class="btn btn-primary" onclick="javascript:relatorioBaixaDevFis();">Pesquisar</a>
                                </div>                                                                                  
                            </div>
                            <div class="form-group">
                                <a type="button" class="btn btn-success btn-xs" onclick="javascript:relatorioBaixaDevFisExport();"><i class="glyphicon glyphicon-download-alt"></i> Exportar</a>
                            </div>
                            <div id="areaRelatorio">

                            </div>

                        </div>                        
                        <!-- ./datatables plugin -->


                        <!-- Copyright -->
                        <?php include("../../../library/copyright.php"); ?>
                        <!-- ./Copyright -->

                    </div>
                    <!-- ./page content container -->                                       
                </div>
                <!-- ./page content -->                                                
            </div>  
            <!-- ./page container -->

            <!-- right bar -->

            <!-- ./right bar -->            

            <!-- page footer -->    
            <?php include("../../../library/footer.php"); ?>
            <!-- ./page footer -->

            <!-- page search -->

            <!-- page search -->            
        </div>
        <!-- ./page wrapper -->


        <!-- javascripts -->
        <?php include("../../../library/rodape.php"); ?>

        <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js"></script>
        <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/plugins/datatables/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/plugins/smartwizard/jquery.smartWizard.js"></script>
        <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/plugins/sortable/sortable.min.js"></script>
        <!-- ./javascripts -->


    </body>
</html>