<?php
date_default_timezone_set('Brazil/East');
header('Content-type: application/x-msdownload');
header('Content-Disposition: attachment; filename=relatorio_BaixaDevFis' .date("d-m-Y-His").'.xls');
header('Pragma: no-cache');
header('Expires: 0');

include_once( "../../../includes/Dao/DaoSistema.class.php" );
include_once( "../../../includes/Dao/RelatorioDao.class.php" );

include_once("include_novo/Tradutor.class.php");

$Tradutor = new Tradutor();

extract($_REQUEST);


//echo "<pre>";
//print_r($_REQUEST);
//echo "</pre>";

$RelatorioDao = new RelatorioDao();

//if(count($area) > 0 ){
//    $auxArea = "";
//    for ($i = 0; count($area) > $i; $i++) {
//        if($auxArea != ""){
//            $auxArea .= ", ";
//        }
//        $auxArea .= "'{$area[$i]}'";
//    }
//    $area = $auxArea;
//}

$retorno = $RelatorioDao->relatorioBaixaDevolucaoFiscal($dataInicial, $dataFinal, $protocolo, $ced_co_numero, $passo);

?>
                        
<div class="table-responsive">
    <table class="table table-bordered table-striped table-sortable">
        <thead>
            <tr>
                <th>Protocolo</th>
                <th>Passo</th>
                <th>Usuário</th>
                <th>Dt Cadastro</th>
                <th>Dt Baixa</th>
                <th>NF Devolução</th>
                <th>NF Origem</th>
                <th>Volume</th>
                <th>Valor</th>
                <th>CD</th>
            </tr>
        </thead>                               
        <tbody>
<?php
while ($dados = oci_fetch_object($retorno)) {
    ?>
                <tr>
                    <td><?php echo $dados->OCORRENCIA;  ?></td>
                    <td><?php echo $dados->PASSO;  ?></td>
                    <td><?php echo $dados->USUARIO ;  ?></td>
                    <td><?php echo $dados->DATA_CADASTRO ;  ?></td>
                    <td><?php echo $dados->DATA_PASSO ;  ?></td>
                    <td><?php echo $dados->NOTA_DEVOLUCAO ;  ?></td>
                    <td><?php echo $dados->NOTA_ORIGEM ;  ?></td>
                    <td><?php echo $dados->VOLUME ;  ?></td>
                    <td><?php echo $Tradutor->formatar($dados->DOC_VR_VALOR, 'MOEDA') ;  ?></td>
                    <td><?php echo $dados->CED_NO_CENTRO ;  ?></td>
                   
                </tr>
<?php } ?>
        </tbody>
    </table>
</div>
