<?php
date_default_timezone_set('Brazil/East');
header('Content-type: application/x-msdownload');
header('Content-Disposition: attachment; filename=relatorio_ConfColaborador' .date("d-m-Y-His").'.xls');
header('Pragma: no-cache');
header('Expires: 0');

ini_set('max_execution_time', -1);

include_once( "../../../includes/Dao/DaoSistema.class.php" );
include_once( "../../../includes/Dao/RelatorioDao.class.php" );

include_once("include_novo/Tradutor.class.php");

$Tradutor = new Tradutor();

//echo "<pre>";
//print_r($_REQUEST);
//echo "</pre>";

extract($_REQUEST);

//$dataInicial = '01/04/2016';
//$dataFinal = '20/04/2016';
$RelatorioDao = new RelatorioDao();
//die();
$retorno = $RelatorioDao->relatorioConferenciaColaborador($dataInicial, $dataFinal, $protocolo, $conferente, $ced_co_numero);
?>

<div class="table-responsive">
    <table class="table table-bordered table-striped table-sortable">
        <thead>
            <tr>

                <th>Dt Cadastro</th>
                <th>Protocolo</th>
                <th>Dt Confer&ecirc;ncia</th>
                <th>Conferente</th>
                <th>Qtde Notas</th>
                <th>Qtde Volumes</th>
                <th>Motivo Devolu&ccedil;&atilde;o</th>					
                <th>Valor Devolu&ccedil;&atilde;o</th>	
                <th>CD</th>	

            </tr>
        </thead>                               
        <tbody>
<?php
while ($dados = oci_fetch_object($retorno)) {
    ?>
                <tr>
                    <td><?php echo $dados->DATA_EXIBIR; ?></td>
                    <td><?php echo $dados->DOC_CO_NUMERO; ?></td>
                    <td><?php echo $dados->DOC_DT_CONFERENCIA; ?></td>
                    <td><?php echo $dados->COL_NO_COLABORADOR; ?></td>
                    <td><?php echo $dados->QTD_NOTAS; ?></td>
                    <td><?php echo $dados->DOC_QT_VOLUME; ?></td>
                    <td><?php echo $dados->TOC_NO_DESCRICAO; ?></td>
                    <td><?php echo $Tradutor->formatar($dados->DOC_VR_VALOR, 'MOEDA') ;  ?></td>
                    <td><?php echo $dados->CED_NO_CENTRO; ?></td>
                </tr>
<?php } ?>
        </tbody>
    </table>
</div>
