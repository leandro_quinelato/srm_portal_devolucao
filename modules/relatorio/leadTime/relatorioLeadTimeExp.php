<?php
setlocale(LC_ALL, 'pt_BR.utf8');
date_default_timezone_set('Brazil/East');
header('Content-type: application/x-msdownload');
header('Content-Disposition: attachment; filename=relatorio_leadTime' .date("d-m-Y-His").'.xls');
header('Pragma: no-cache');
header('Expires: 0');

ini_set('max_execution_time', -1);


session_start();
include_once( "../../../includes/Dao/DaoSistema.class.php" );
include_once( "../../../includes/Dao/RelatorioDao.class.php" );

include_once("DaoGlobal.class.php");
include_once("SistemaDao.class.php");
include_once("GlbSistema.class.php");
include_once("GlbSistemaPerfil.class.php");
include_once("GlbUsuario.class.php");
include_once("GlbColaborador.class.php");
include_once("GlbPessoa.class.php");
include_once("GlbEndereco.class.php");
include_once("GlbTelefone.class.php");
include_once("include_novo/GlbTransportadora.class.php");
include_once("include_novo/GlbContatoTransportador.class.php");
include_once("include_novo/ContatoTransportadorDao.class.php");
include_once("GlbProgramaRotina.class.php");
include_once("GlbModuloPrograma.class.php");
include_once("GlbSistemaModulo.class.php");
include_once("PerfilUsuarioDao.class.php");
include_once("PerfilPermissaoDao.class.php");
include_once("GlbVendedor.class.php");
include_once("VendedorDao.class.php");
include_once("integraSetSistemas.php");
include_once("class.phpmailer.php");
include_once("class.smtp.php");

include_once("include_novo/Tradutor.class.php");

$Tradutor = new Tradutor();
$integra = new integraSetSistemas("conf.xml");
$ObjUsuario = unserialize($integra->getSistemaIntegra());

include_once("include_novo/Tradutor.class.php");

$Tradutor = new Tradutor();

//echo "<pre>";
//print_r($_REQUEST);
//echo "</pre>";

extract($_REQUEST);

//trata vetor divisao em texto
if($vetDivisao != ""){
$InDivisao = implode(",",$vetDivisao);
//echo $InDivisao;
}

//$dataInicial = '01/04/2016';
//$dataFinal = '20/04/2016';
$RelatorioDao = new RelatorioDao();
$codMotivo = "TOD";
$retorno = $RelatorioDao->relatorioOcorrenciaLeadTime($dataInicial, $dataFinal, $protocolo, $codCliente, $codRede, $codMotivo, $rot_co_numero, $ced_co_numero, $InDivisao, $codPasso, $dataInicialPasso, $dataFinalPasso, $ObjUsuario);

function dataToEn($data){
	if ($data == ""){
		return null;
	}
	$parte = explode("/", $data);
	return $parte[2]."/".$parte[1]."/".$parte[0];
}

function diffDatas($data_inicial, $data_final){
	if ($data_inicial == "" or $data_final == ""){
		return null;
	}
	
	//$data_inicial = '2017-06-30';
	//$data_final = '2017-07-05';
	// Calcula a diferença em segundos entre as datas
	$diferenca = strtotime($data_final) - strtotime($data_inicial);
	//Calcula a diferença em dias
	$dias = floor($diferenca / (60 * 60 * 24));
	if($dias >= 0){
		return $dias;
	}else{
		return null;
	}

}

?>

<div class="table-responsive">
    <table class="table table-bordered table-striped table-sortable">
        <thead>
            <tr>
				<th>Passo Fluxo</th>
                <th>Status</th>
                <th>Protocolo</th>
                <th>Usuario Cadastrante</th>
                <th>Rota</th>
				<th>Setor Terceiro</th>
                <th>Tipo Ocorrencia</th>
				<th>Serie Origem</th>
                <th>NF Origem</th>
                <th>Emissao Origem</th>
				
                <th>Origem Venda</th>
				<th>Serie Devolucao</th>
                <th>NF Devolucao</th>
				<th>Emissao Devolucao</th>
                <th>Valor Ocorrencia</th>
                <th>Qtd. Volume</th>
				<th>Motivo Devolucao</th>
				
                <th>Rede</th>
                <th>Cod Cliente</th>
                <th>Cliente</th>
				<th>Setor</th>
				<th>Representante</th>
				<th>Divisional</th>
				<th>Gerente Divisional</th>
				<th>Regional</th>
				<th>Gerente Regional</th>
				<th>Equipe</th>
				<th>Gerente</th>
				<th>CD</th>
				<th>Divisao</th>
				
				<th>Data Cadastro</th>
				<th>Data Passo Gerente</th>
				<th>Data Inclusao NFD</th>
				<th>Data Passo Transportador</th>
				<th>Data Recebimento CD</th>
				<th>Data Recebimento Devolucao</th>
				<th>Data Conferencia Devolucao</th>
				<th>Data Passo Devolucao</th>
				<th>Data Passo Fiscal</th>
				
				<th>Cadastro a Aprovacao</th>
				<th>Aprovacao Gerente a Inclusao NFD</th>
				<th>Inclusao NFD a Transportador</th>
				<th>Transportador a Recebimento CD</th>
				<th>Recebimento CD a Recebimento Devolucao</th>
				
				<th>Recebimento Devolucao a conferencia Devolucao</th>
				<th>Conferencia Devolucao a Baixa Site Devolucao</th>
				<th>Baixa Site Devolucao a Baixa site Fiscal</th>
				<th>Reprovacao a Reativacao</th>
				<th>Lead Time Total</th>
            </tr>
        </thead>                               
        <tbody>
<?php
$vendedorDao = new VendedorDao();
$ObjVendedor = new GlbVendedor();

while ($dados = oci_fetch_object($retorno)) {
	
		//Pega Representante
		$ObjVendedor->setCodigo($dados->CDSETOR);
		$ObjVendedor = $vendedorDao->preencherVendedor($ObjVendedor);
		$codRepresentante =	$ObjVendedor->getCodigo();
		$nomeRepresentante = $ObjVendedor->getPessoaNome();
		$codGerEquipe = $ObjVendedor->getSupervisor()->getCodigo();
		
		//Pega gerente equipe/contas
		$ObjVendedor->setCodigo($codGerEquipe);
		$ObjVendedor = $vendedorDao->preencherVendedor($ObjVendedor);
		$nomeGerEquipe = $ObjVendedor->getPessoaNome();
		$codGerRegional = $ObjVendedor->getSupervisor()->getCodigo();
		
		//Pega Ger. regional
		$ObjVendedor->setCodigo($codGerRegional);
		$ObjVendedor = $vendedorDao->preencherVendedor($ObjVendedor);
		$nomeGerRegional = $ObjVendedor->getPessoaNome();
		$codGerDivisional = $ObjVendedor->getSupervisor()->getCodigo();
		
		//Pega Ger. divisional
		$ObjVendedor->setCodigo($codGerDivisional);
		$ObjVendedor = $vendedorDao->preencherVendedor($ObjVendedor);
		$nomeGerDivisional = $ObjVendedor->getPessoaNome();
		
		
		//datas Passos
		$res = $RelatorioDao->relatorioOcorrenciaPassoLeadTime($dados->DOC_CO_NUMERO, 1); //gerente
		$registro = oci_fetch_object($res);
		$dataSaidaPassoGer = $registro->DOP_DT_SAIDA;
		
		$res = $RelatorioDao->relatorioOcorrenciaPassoLeadTime($dados->DOC_CO_NUMERO, 2); //transportador
		$registro = oci_fetch_object($res);
		$dataSaidaPassoTra = $registro->DOP_DT_SAIDA;

		$res = $RelatorioDao->relatorioOcorrenciaPassoLeadTime($dados->DOC_CO_NUMERO, 3); //recebimento
		$registro = oci_fetch_object($res);
		$dataSaidaPassoRec = $registro->DOP_DT_SAIDA;
		
		$res = $RelatorioDao->relatorioOcorrenciaPassoLeadTime($dados->DOC_CO_NUMERO, 4); //devolucao
		$registro = oci_fetch_object($res);
		$dataEntradaPassoDev = $registro->DOP_DT_ENTRADA;
		$dataSaidaPassoDev = $registro->DOP_DT_SAIDA;
		
		$res = $RelatorioDao->relatorioOcorrenciaPassoLeadTime($dados->DOC_CO_NUMERO, 5); //fiscal
		$registro = oci_fetch_object($res);
		$dataEntradaPassoFis = $registro->DOP_DT_ENTRADA;
		$dataSaidaPassoFis = $registro->DOP_DT_SAIDA;
		
	
		//calcula leadTime reprova e reativacao total
		$diasTotalReprovacao = null;
		$retornoStatus = $RelatorioDao->relatorioLeadTimeOcorrenciaStatus($dados->DOC_CO_NUMERO, 12); //reprovas
		while ($regStatus = oci_fetch_object($retornoStatus)) {
			$dataReprova = $regStatus->DOS_DT_ENTRADA;
			
			$res = $RelatorioDao->relatorioLeadTimeOcorrenciaStatus($dados->DOC_CO_NUMERO, 16, $dataReprova, null, true); //reativacao seguida da reprova
			$registro = oci_fetch_object($res);
			$dataReativacao = $registro->DOS_DT_ENTRADA;
			
			//echo "dataReprova:". $dataReprova . "dataReativacao:".$dataReativacao;
			$diasTotalReprovacao += diffDatas(dataToEn($dataReprova), dataToEn($dataReativacao));
		}
		
		//calcula leadTime Pendente no passo Devolucao
		$diasTotalPendenteDevolucao = null;
		if($dataEntradaPassoDev !='' and $dataSaidaPassoDev !=''){
			$retornoStatus = $RelatorioDao->relatorioLeadTimeOcorrenciaStatus($dados->DOC_CO_NUMERO, 13, $dataEntradaPassoDev, $dataSaidaPassoDev); //pendentes
			while ($regStatus = oci_fetch_object($retornoStatus)) {
				$dataPendente = $regStatus->DOS_DT_ENTRADA;
				
				$res = $RelatorioDao->relatorioLeadTimeOcorrenciaStatus($dados->DOC_CO_NUMERO, 16, $dataPendente, $dataSaidaPassoDev, true); //reativacao seguida da reprova
				$registro = oci_fetch_object($res);
				$dataReativacao = $registro->DOS_DT_ENTRADA;
				
				//echo "dataPendente:". $dataPendente . "dataReativacao:".$dataReativacao;
				$diasTotalPendenteDevolucao += diffDatas(dataToEn($dataPendente), dataToEn($dataReativacao));
			}
		}
		
		
		//calcula leadTime Pendente no passo Fiscal
		$diasTotalPendenteFical = null;
		if($dataEntradaPassoFis !='' and $dataSaidaPassoFis !=''){
			$retornoStatus = $RelatorioDao->relatorioLeadTimeOcorrenciaStatus($dados->DOC_CO_NUMERO, 13, $dataEntradaPassoFis, $dataSaidaPassoFis); //pendentes
			while ($regStatus = oci_fetch_object($retornoStatus)) {
				$dataPendente = $regStatus->DOS_DT_ENTRADA;
				
				$res = $RelatorioDao->relatorioLeadTimeOcorrenciaStatus($dados->DOC_CO_NUMERO, 16, $dataPendente, $dataSaidaPassoFis, true); //reativacao seguida da reprova
				$registro = oci_fetch_object($res);
				$dataReativacao = $registro->DOS_DT_ENTRADA;
				
				//echo "dataPendente:". $dataPendente . "dataReativacao:".$dataReativacao;
				$diasTotalPendenteFical += diffDatas(dataToEn($dataPendente), dataToEn($dataReativacao));
			}
		}
		
		
		
		
		//Cadastro a Aprovacao **-->
		$diasTotalReprovacaoCadastroAteAprovacao = null;
		if($dados->DATA_CADASTRO !='' and $dataSaidaPassoGer !=''){
			$retornoStatus = $RelatorioDao->relatorioLeadTimeOcorrenciaStatus($dados->DOC_CO_NUMERO, 12, $dados->DATA_CADASTRO, $dataSaidaPassoGer); //reprovados
			while ($regStatus = oci_fetch_object($retornoStatus)) {
				$dataPendente = $regStatus->DOS_DT_ENTRADA;
				
				$res = $RelatorioDao->relatorioLeadTimeOcorrenciaStatus($dados->DOC_CO_NUMERO, 16, $dataPendente, $dataSaidaPassoGer, true); //reativacao seguida da reprova
				$registro = oci_fetch_object($res);
				$dataReativacao = $registro->DOS_DT_ENTRADA;
				
				//echo "dataPendente:". $dataPendente . "dataReativacao:".$dataReativacao;
				$diasTotalReprovacaoCadastroAteAprovacao += diffDatas(dataToEn($dataPendente), dataToEn($dataReativacao));
			}
		}
		
		//Aprovacao Gerente a Inclusao NFD-->
		$diasTotalReprovacaoAprovacaoGerenteAteInclusaoNFD = null;
		if($dataSaidaPassoGer !='' and $dados->DOC_DT_DEVNOTA !=''){
			$retornoStatus = $RelatorioDao->relatorioLeadTimeOcorrenciaStatus($dados->DOC_CO_NUMERO, 12, $dataSaidaPassoGer, $dados->DOC_DT_DEVNOTA); //reprovados
			while ($regStatus = oci_fetch_object($retornoStatus)) {
				$dataPendente = $regStatus->DOS_DT_ENTRADA;
				
				$res = $RelatorioDao->relatorioLeadTimeOcorrenciaStatus($dados->DOC_CO_NUMERO, 16, $dataPendente, $dados->DOC_DT_DEVNOTA, true); //reativacao seguida da reprova
				$registro = oci_fetch_object($res);
				$dataReativacao = $registro->DOS_DT_ENTRADA;
				
				//echo "dataPendente:". $dataPendente . "dataReativacao:".$dataReativacao;
				$diasTotalReprovacaoAprovacaoGerenteAteInclusaoNFD += diffDatas(dataToEn($dataPendente), dataToEn($dataReativacao));
			}
		}
		
		//Inclusao NFD a Transportador-->
		$diasTotalReprovacaoInclusaoNFDAteTransportador = null;
		if($dados->DOC_DT_DEVNOTA !='' and $dataSaidaPassoTra !=''){
			$retornoStatus = $RelatorioDao->relatorioLeadTimeOcorrenciaStatus($dados->DOC_CO_NUMERO, 12, $dados->DOC_DT_DEVNOTA, $dataSaidaPassoTra); //reprovados
			while ($regStatus = oci_fetch_object($retornoStatus)) {
				$dataPendente = $regStatus->DOS_DT_ENTRADA;
				
				$res = $RelatorioDao->relatorioLeadTimeOcorrenciaStatus($dados->DOC_CO_NUMERO, 16, $dataPendente, $dataSaidaPassoTra, true); //reativacao seguida da reprova
				$registro = oci_fetch_object($res);
				$dataReativacao = $registro->DOS_DT_ENTRADA;
				
				//echo "dataPendente:". $dataPendente . "dataReativacao:".$dataReativacao;
				$diasTotalReprovacaoInclusaoNFDAteTransportador += diffDatas(dataToEn($dataPendente), dataToEn($dataReativacao));
			}
		}
		
		//Transportador a Recebimento CD-->
		$diasTotalReprovacaoTransportadorAteRecebimentoCD = null;
		if($dataSaidaPassoTra !='' and $dataSaidaPassoRec !=''){
			$retornoStatus = $RelatorioDao->relatorioLeadTimeOcorrenciaStatus($dados->DOC_CO_NUMERO, 12, $dataSaidaPassoTra, $dataSaidaPassoRec); //reprovados
			while ($regStatus = oci_fetch_object($retornoStatus)) {
				$dataPendente = $regStatus->DOS_DT_ENTRADA;
				
				$res = $RelatorioDao->relatorioLeadTimeOcorrenciaStatus($dados->DOC_CO_NUMERO, 16, $dataPendente, $dataSaidaPassoRec, true); //reativacao seguida da reprova
				$registro = oci_fetch_object($res);
				$dataReativacao = $registro->DOS_DT_ENTRADA;
				
				//echo "dataPendente:". $dataPendente . "dataReativacao:".$dataReativacao;
				$diasTotalReprovacaoTransportadorAteRecebimentoCD += diffDatas(dataToEn($dataPendente), dataToEn($dataReativacao));
			}
		}

		//Recebimento CD a Recebimento Devolucao-->
		$diasTotalReprovacaoRecebimentoCDAteRecebimentoDevolucao = null;
		if($dataSaidaPassoRec !='' and $dados->DOC_DT_DEVRECEBIMENTO !=''){
			$retornoStatus = $RelatorioDao->relatorioLeadTimeOcorrenciaStatus($dados->DOC_CO_NUMERO, 12, $dataSaidaPassoRec, $dados->DOC_DT_DEVRECEBIMENTO); //reprovados
			while ($regStatus = oci_fetch_object($retornoStatus)) {
				$dataPendente = $regStatus->DOS_DT_ENTRADA;
				
				$res = $RelatorioDao->relatorioLeadTimeOcorrenciaStatus($dados->DOC_CO_NUMERO, 16, $dataPendente, $dados->DOC_DT_DEVRECEBIMENTO, true); //reativacao seguida da reprova
				$registro = oci_fetch_object($res);
				$dataReativacao = $registro->DOS_DT_ENTRADA;
				
				//echo "dataPendente:". $dataPendente . "dataReativacao:".$dataReativacao;
				$diasTotalReprovacaoRecebimentoCDAteRecebimentoDevolucao += diffDatas(dataToEn($dataPendente), dataToEn($dataReativacao));
			}
		}

		//Recebimento Devolucao a conferencia Devolucao-->
		$diasTotalReprovacaoRecebimentoDevolucaoAteConferenciaDevolucao = null;
		if($dados->DOC_DT_DEVRECEBIMENTO !='' and $dados->DOC_DT_CONFERENCIA !=''){
			$retornoStatus = $RelatorioDao->relatorioLeadTimeOcorrenciaStatus($dados->DOC_CO_NUMERO, 12, $dados->DOC_DT_DEVRECEBIMENTO, $dados->DOC_DT_CONFERENCIA); //reprovados
			while ($regStatus = oci_fetch_object($retornoStatus)) {
				$dataPendente = $regStatus->DOS_DT_ENTRADA;
				
				$res = $RelatorioDao->relatorioLeadTimeOcorrenciaStatus($dados->DOC_CO_NUMERO, 16, $dataPendente, $dados->DOC_DT_CONFERENCIA, true); //reativacao seguida da reprova
				$registro = oci_fetch_object($res);
				$dataReativacao = $registro->DOS_DT_ENTRADA;
				
				//echo "dataPendente:". $dataPendente . "dataReativacao:".$dataReativacao;
				$diasTotalReprovacaoRecebimentoDevolucaoAteConferenciaDevolucao += diffDatas(dataToEn($dataPendente), dataToEn($dataReativacao));
			}
		}
		
		//Conferencia Devolucao a Baixa Site Devolucao-->
		$diasTotalReprovacaoConferenciaDevolucaoAteBaixaSiteDevolucao = null;
		if($dados->DOC_DT_CONFERENCIA !='' and $dataSaidaPassoDev !=''){
			$retornoStatus = $RelatorioDao->relatorioLeadTimeOcorrenciaStatus($dados->DOC_CO_NUMERO, 12, $dados->DOC_DT_CONFERENCIA, $dataSaidaPassoDev); //reprovados
			while ($regStatus = oci_fetch_object($retornoStatus)) {
				$dataPendente = $regStatus->DOS_DT_ENTRADA;
				
				$res = $RelatorioDao->relatorioLeadTimeOcorrenciaStatus($dados->DOC_CO_NUMERO, 16, $dataPendente, $dataSaidaPassoDev, true); //reativacao seguida da reprova
				$registro = oci_fetch_object($res);
				$dataReativacao = $registro->DOS_DT_ENTRADA;
				
				//echo "dataPendente:". $dataPendente . "dataReativacao:".$dataReativacao;
				$diasTotalReprovacaoConferenciaDevolucaoAteBaixaSiteDevolucao += diffDatas(dataToEn($dataPendente), dataToEn($dataReativacao));
			}
		}

		//Baixa Site Devolucao a Baixa site Fiscal-->
		$diasTotalReprovacaoBaixaSiteDevolucaoAteBaixaSiteFiscal = null;
		if($dataSaidaPassoDev !='' and $dataSaidaPassoFis !=''){
			$retornoStatus = $RelatorioDao->relatorioLeadTimeOcorrenciaStatus($dados->DOC_CO_NUMERO, 12, $dataSaidaPassoDev, $dataSaidaPassoFis); //reprovados
			while ($regStatus = oci_fetch_object($retornoStatus)) {
				$dataPendente = $regStatus->DOS_DT_ENTRADA;
				
				$res = $RelatorioDao->relatorioLeadTimeOcorrenciaStatus($dados->DOC_CO_NUMERO, 16, $dataPendente, $dataSaidaPassoFis, true); //reativacao seguida da reprova
				$registro = oci_fetch_object($res);
				$dataReativacao = $registro->DOS_DT_ENTRADA;
				
				//echo "dataPendente:". $dataPendente . "dataReativacao:".$dataReativacao;
				$diasTotalReprovacaoBaixaSiteDevolucaoAteBaixaSiteFiscal += diffDatas(dataToEn($dataPendente), dataToEn($dataReativacao));
			}
		}	
		
    ?>
                <tr>
					<td><?php echo $dados->DPA_NO_DESCRICAO;  ?></td>
                    <td><?php echo $dados->DST_NO_DESCRICAO;  ?></td>
                    <td><?php echo $dados->DOC_CO_NUMERO;  ?></td>
                    <td><?php echo $dados->USU_NO_USERNAME;  ?></td>
                    <td><?php echo $dados->ROT_CO_NUMERO;  ?></td>
					<td><?php echo $dados->CLI_NU_ROTA_TERCEIRIZADA; ?></td>
                    <td><?php echo $dados->TID_CO_DESCRICAO;  ?></td>
                    <td><?php echo $dados->NTO_NU_SERIE;  ?></td>
					<td><?php echo $dados->NTO_NU_NOTA;  ?></td>
					<td><?php echo $dados->NTO_DT_NOTA;  ?></td>
					
					<td><?php echo $dados->NTO_NU_ORIGEM_VENDA;  ?></td>
					<td><?php echo $dados->DOC_NU_DEVSERIE;  ?></td>
					<td><?php echo $dados->DOC_NU_DEVNOTA;  ?></td>
					<td><?php echo $dados->DOC_DT_DEVNOTA;  ?></td>
					<td><?php echo $Tradutor->formatar($dados->DOC_VR_VALOR, 'MOEDA');  ?></td>
					<td><?php echo $dados->DOC_QT_PRODUTO;  ?></td>
					<td><?php echo $dados->TOC_NO_DESCRICAO; ?></td>
					
                    <td><?php echo $dados->RED_CO_NUMERO;  ?></td>
                    <td><?php echo $dados->CLI_CO_NUMERO;  ?></td>
                    <td><?php echo $dados->CLI_NO_FANTASIA;  ?></td>
					<td><?php echo $codRepresentante;  ?> <!--setor--></td>
					<td><?php echo $nomeRepresentante;  ?> <!--representante--> </td>
					<td><?php echo $codGerDivisional;  ?> <!--divissional--> </td>
					<td><?php echo $nomeGerDivisional;  ?> <!--gerente divissional--> </td>
					<td><?php echo $codGerRegional;  ?> <!--regional--> </td>
					<td><?php echo $nomeGerRegional;  ?> <!--gerente regional--> </td>
					<td><?php echo $codGerEquipe;  ?> <!--equipe--> </td>
					<td><?php echo $nomeGerEquipe;  ?></td>
					<td><?php echo $dados->CED_NO_CENTRO;  ?></td>
					<td><?php echo $dados->DIVISAO;  ?></td>
                    
					<td><?php echo $dados->DATA_CADASTRO;  ?></td>
					<td><?php echo $dataSaidaPassoGer; ?> </td>
					<td><?php echo $dados->DOC_DT_DEVNOTA; ?> <!--data inclusao NFD--> </td>
					<td><?php echo $dataSaidaPassoTra; ?></td>
					<td><?php echo $dataSaidaPassoRec; ?></td>
					<td><?php echo $dados->DOC_DT_DEVRECEBIMENTO; ?></td>
					<td><?php echo $dados->DOC_DT_CONFERENCIA; ?></td>
					<td><?php echo $dataSaidaPassoDev?></td>
					<td><?php echo $dataSaidaPassoFis?></td>
					
					<td><?=diffDatas(dataToEn($dados->DATA_CADASTRO), dataToEn($dataSaidaPassoGer)) - $diasTotalReprovacaoCadastroAteAprovacao;?> <!--Cadastro a Aprovacao **--> </td>
					<td><?=diffDatas(dataToEn($dataSaidaPassoGer), dataToEn($dados->DOC_DT_DEVNOTA)) - $diasTotalReprovacaoAprovacaoGerenteAteInclusaoNFD;?> <!--Aprovacao Gerente a Inclusao NFD--> </td>
					<td><?=diffDatas(dataToEn($dados->DOC_DT_DEVNOTA), dataToEn($dataSaidaPassoTra)) - $diasTotalReprovacaoInclusaoNFDAteTransportador;?> <!--Inclusao NFD a Transportador--> </td>
					<td><?=diffDatas(dataToEn($dataSaidaPassoTra), dataToEn($dataSaidaPassoRec)) - $diasTotalReprovacaoTransportadorAteRecebimentoCD;?> <!--Transportador a Recebimento CD--> </td>
					<td><?=diffDatas(dataToEn($dataSaidaPassoRec), dataToEn($dados->DOC_DT_DEVRECEBIMENTO)) - $diasTotalReprovacaoRecebimentoCDAteRecebimentoDevolucao;?> <!--Recebimento CD a Recebimento Devolucao--> </td>
					
					<td><?=diffDatas(dataToEn($dados->DOC_DT_DEVRECEBIMENTO), dataToEn($dados->DOC_DT_CONFERENCIA)) - $diasTotalReprovacaoRecebimentoDevolucaoAteConferenciaDevolucao;?> <!--Recebimento Devolucao a conferencia Devolucao--> </td>
					<td><?=(diffDatas(dataToEn($dados->DOC_DT_CONFERENCIA), dataToEn($dataSaidaPassoDev)) - $diasTotalPendenteDevolucao) - $diasTotalReprovacaoConferenciaDevolucaoAteBaixaSiteDevolucao;?> <!--Conferencia Devolucao a Baixa Site Devolucao--> </td>
					<td><?=(diffDatas(dataToEn($dataSaidaPassoDev), dataToEn($dataSaidaPassoFis)) - $diasTotalPendenteFical) - $diasTotalReprovacaoBaixaSiteDevolucaoAteBaixaSiteFiscal;?> <!--Baixa Site Devolucao a Baixa site Fiscal--> </td>
					<td><?=$diasTotalReprovacao?> <!--Reprovacao a Reativacao--> </td>
					<td><?=diffDatas(dataToEn($dados->DATA_CADASTRO), dataToEn($dataSaidaPassoFis));?> <!--Lead Time Total--></td>
					
                </tr>
<?php 
} 
?>


        </tbody>
    </table>
</div>