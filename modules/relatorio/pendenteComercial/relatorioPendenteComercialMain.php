<?php

include_once( "../../../includes/Dao/DaoSistema.class.php" );
include_once( "../../../includes/Dao/RelatorioDao.class.php" );

include_once("DaoGlobal.class.php");
include_once("SistemaDao.class.php");
include_once("GlbSistema.class.php");
include_once("GlbSistemaPerfil.class.php");
include_once("GlbUsuario.class.php");
include_once("GlbColaborador.class.php");
include_once("GlbPessoa.class.php");
include_once("GlbEndereco.class.php");
include_once("GlbTelefone.class.php");
include_once("include_novo/GlbTransportadora.class.php");
include_once("include_novo/GlbContatoTransportador.class.php");
include_once("include_novo/ContatoTransportadorDao.class.php");
include_once("GlbProgramaRotina.class.php");
include_once("GlbModuloPrograma.class.php");
include_once("GlbSistemaModulo.class.php");
include_once("PerfilUsuarioDao.class.php");
include_once("PerfilPermissaoDao.class.php");
include_once("integraSetSistemas.php");
include_once("class.phpmailer.php");
include_once("class.smtp.php");

include_once("include_novo/Tradutor.class.php");

$Tradutor = new Tradutor();
$integra = new integraSetSistemas("conf.xml");
$ObjUsuario = unserialize($integra->getSistemaIntegra());

$Tradutor = new Tradutor();

//echo "<pre>";
//print_r($_REQUEST);
//echo "</pre>";

extract($_REQUEST);

//$dataInicial = '01/04/2016';
//$dataFinal = '20/04/2016';
$RelatorioDao = new RelatorioDao();

if(count($codDivisao) > 0 ){
    $auxDivisao = "";
    for ($i = 0; count($codDivisao) > $i; $i++) {
        if($auxDivisao != ""){
            $auxDivisao .= ", ";
        }
        $auxDivisao .= "'{$codDivisao[$i]}'";
    }
    $codDivisao = $auxDivisao;
}


$retorno = $RelatorioDao->relatorioPendenteComercial($dataInicial, $dataFinal, $protocolo, $codCliente, $codRede, $ced_co_numero, $codDivisao, $rot_co_numero, $ObjUsuario);

?>

<div class="table-responsive">
    <table class="table table-bordered table-striped table-sortable">
        <thead>
            <tr>
                <th>Protocolo</th>
                <th>Data</th>
                <th>Usuário</th>
                <th>Cod Cliente</th>
                <th>Cliente</th>
                <th>Rede</th>
                <th>Motivo</th>
                <th>Rota</th>
                <th>Divisão</th>
                <th>CD</th>
                <th>Ger. Contas</th>
                <th>Ger. Regional</th>
                <th>Ger. Divisional</th>
            </tr>
        </thead>                               
        <tbody>
<?php
while ($dados = oci_fetch_object($retorno)) {
    ?>
                <tr>
                    <td><?php echo $dados->DOC_CO_NUMERO;  ?></td>
                    <td><?php echo $dados->DATA_CADASTRO;  ?></td>
                    <td><?php echo $dados->USU_NO_USERNAME;  ?></td>
                    <td><?php echo $dados->CLI_CO_NUMERO;  ?></td>
                    <td><?php echo $dados->CLI_NO_FANTASIA;  ?></td>
                    <td><?php echo $dados->RED_CO_NUMERO ;  ?></td>
                    <td><?php echo $dados->TOC_NO_DESCRICAO ;  ?></td>
                    <td><?php echo $dados->ROT_CO_NUMERO ;  ?></td>
                    <td><?php echo $dados->CED_NO_CENTRO ;  ?></td>
                    <td><?php echo $dados->DIVISAO ;  ?></td>
                    <td><?php echo $dados->EQUIPE ." - ". $dados->GERENTE_CONTAS;  ?></td>
                    <td><?php echo $dados->COD_REGIONAL ." - ". $dados->GERENTE_REGIONAL;  ?></td>
                    <td><?php echo $dados->COD_DIVISIONAL ." - ". $dados->GERENTE_DIVISIONAL;  ?></td>
  
                </tr>
<?php } ?>


        </tbody>
    </table>
</div>
    <script language="JavaScript">
        $(function() {
          datatables.init();
        });
    </script>