<?php
session_start();
include_once( "../../../includes/Dao/DaoSistema.class.php" );
include_once( "../../../includes/Dao/RelatorioDao.class.php" );
include_once( "../../../includes/Dao/OcorrenciaDao.class.php" );
include_once( "../../../includes/Status.class.php" );
include_once( "../../../includes/Dao/StatusDao.class.php" );
include_once( "../../../includes/Rota.class.php" );
include_once( "../../../includes/Dao/RotaDao.class.php" );

include_once("DaoGlobal.class.php");
include_once("SistemaDao.class.php");
include_once("GlbSistema.class.php");
include_once("GlbSistemaPerfil.class.php");
include_once("GlbUsuario.class.php");
include_once("GlbColaborador.class.php");
include_once("GlbPessoa.class.php");
include_once("GlbEndereco.class.php");
include_once("GlbTelefone.class.php");
include_once("include_novo/GlbTransportadora.class.php");
include_once("include_novo/GlbContatoTransportador.class.php");
include_once("include_novo/ContatoTransportadorDao.class.php");
include_once("GlbProgramaRotina.class.php");
include_once("GlbModuloPrograma.class.php");
include_once("GlbSistemaModulo.class.php");
include_once("PerfilUsuarioDao.class.php");
include_once("PerfilPermissaoDao.class.php");

$ObjUsuario = unserialize($_SESSION['ObjLogin']);

include_once("include_novo/GlbCentroDistribuicao.class.php");

$RelatorioDao = new RelatorioDao();
$OcorrenciaDao = new OcorrenciaDao();
$resultFab = $OcorrenciaDao->industria();

$RotaDao = new RotaDao();
/* FILTRO ROTA */
if ($ObjUsuario->getUsuarioTipo() == "VEN") {
    $VendedorUsuario = $ObjUsuario->getObjVendedor();
    $ObjVendedor = new GlbVendedor();
    if ($VendedorUsuario[0]) {
        $ObjVendedor->setCodigo($VendedorUsuario[0]->getCodigo());
        $ObjVendedor->setTipo($VendedorUsuario[0]->getTipo());
    }
    $resultRota = $RotaDao->consultarRotaVendedor($ObjVendedor);
} else if ($ObjUsuario->getUsuarioTipo() == "CLI") {

    $resultRota = $OcorrenciaDao->consultaCliente($ObjUsuario->getObjCliente()->getClienteCodigo());
    //  OCIFetchInto($resultCliente, $rowCliente, OCI_ASSOC);
} else if($ObjUsuario->getUsuarioTipo() == "TRA"){
    $resultRota  = $RotaDao->consultaUsuRota($ObjUsuario->getUsuarioCodigo());
} else {

    $resultRota = $RotaDao->listar();
}


/* FILTRO CEDIS*/
$GlbCentroDistribuicao = new GlbCentroDistribuicao();
$GlbCentroDistribuicao = $OcorrenciaDao->filtroCentroDis();


/* FILTRO STATUS OCORRENCIA*/
$Status = new Status();
$StatusDao = new StatusDao();
$Status = $StatusDao->consultar();

?>


<!DOCTYPE html>
<html lang="en">
    <head>        
        <!-- meta section -->
        <title>Relatórios - Protocolo Pendente Aprovação Comercial</title>
        <?php include("../../../library/head.php"); ?>
        <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/libRelatorio.js"></script>
    </head>
    <body>
        <!-- set loading layer -->
        <!--<div class="dev-page-loading preloader"></div>-->
        <!-- ./set loading layer -->

        <!-- page wrapper -->
        <div class="dev-page">

            <!-- page header -->    
            <?php include("../../../library/topo.php"); ?>
            <!-- ./page header -->

            <!-- page container -->
            <div class="dev-page-container">

                <!-- page sidebar -->
                <?php include("../../../library/menu.php"); ?>
                <!-- ./page sidebar -->

                <!-- page content -->
                <div class="dev-page-content">                    
                    <!-- page content container -->
                    <div class="container">

                        <!-- page title -->
                        <div class="page-title">
                            <h1>Relatório de Protocolo Pendente Aprovação Comercial</h1>

                            <ul class="breadcrumb">
                                <li><a href="#">Relatórios</a></li>
                                <li>Protocolo Pendente Aprovação Comercial</li>
                            </ul>
                        </div>                        
                        <!-- ./page title -->


                        <!-- datatables plugin -->
                        <div class="wrapper wrapper-white">
                            <form id="FormPendenteComercial">
                                <div class="row">
                                    <div class="col-md-2 col-sm-3 col-xs-3">
                                        <div class="form-group">
                                            <label>Início</label>                            
                                            <input type="text" class="form-control datepicker" id="dataInicial" name="dataInicial">
                                        </div> 
                                    </div>  
                                    <div class="col-md-2 col-sm-3 col-xs-3">
                                        <div class="form-group">
                                            <label>Fim</label>                            
                                            <input type="text" class="form-control datepicker" id="dataFinal" name="dataFinal">
                                        </div> 
                                    </div>   
                                    <div class="col-md-2 col-sm-3 col-xs-3">
                                        <div class="form-group">
                                            <label>Protocolo</label>
                                            <input type="text" class="form-control" placeholder="Protocolo" onKeyPress="fMascara('numero', event, this)" id="protocolo" name="protocolo">
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-3 col-xs-3">
                                        <div class="form-group">
                                            <label>Cliente</label>
                                            <input type="text" class="form-control" placeholder="Cliente" onKeyPress="fMascara('numero', event, this)" id="codCliente" name="codCliente">
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-3 col-xs-3">
                                        <div class="form-group">
                                            <label>Rede</label>
                                            <input type="text" class="form-control" placeholder="Rede" onKeyPress="fMascara('numero', event, this)" id="codRede" name="codRede">
                                        </div>
                                    </div>

                                    <div class="col-md-2 col-sm-6 col-xs-6">                        
                                        <div class="form-group">
                                            <label>Rota</label>
                                            <select class="form-control selectpicker" id="rot_co_numero" name="rot_co_numero">
                                                <option value="TOD">Todas</option>
                                                <?php
                                                while (OCIFetchInto($resultRota, $rowRota, OCI_ASSOC)) {
                                                    ?>
                                                    <option value="<?php echo $rowRota['ROT_CO_NUMERO']; ?>"  <?php
                                                    if ($rowCliente['ROT_CO_NUMERO'] == $rowRota['ROT_CO_NUMERO']) {
                                                        echo "selected='selected'";
                                                    }
                                                    ?>>
                                                                <?php echo $rowRota['ROT_CO_NUMERO']; ?> 
                                                    </option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>                        
                                    </div>

                                    <div class="col-md-3 col-sm-6 col-xs-6">                        
                                        <div class="form-group">
                                            <label>CD</label>
                                            <select class="form-control selectpicker" id="ced_co_numero" name="ced_co_numero">
                                                <option value="TOD">Todos</option>
                                                <?php
                                                foreach ($GlbCentroDistribuicao as $ObjCedis) {
                                                    ?>
                                                    <option value="<?php echo $ObjCedis->getCodigo(); ?>"><?php echo $ObjCedis->getNome(); ?></option>
                                                    <?php
                                                }
                                                ?>    
                                            </select>
                                        </div>                        
                                    </div> 
                                    
                                    <div class="col-md-3 col-sm-6 col-xs-12">                        
                                        <div class="form-group">
                                            <label>Divisão</label>
                                            <select class="form-control selectpicker" multiple id="codDivisao" name="codDivisao[]">
                                                <option value="A">Alimentar</option>
                                                <option value="O">Dist. Direta</option>
                                                <option value="F">Farma</option>
                                                <option value="H">Hospitalar</option>
                                            </select>
                                        </div>                        
                                    </div>  
 
                                </div>  
                            </form>
                            <div class="row">
                                <div class="col-md-2">
                                    <a class="btn btn-primary" onclick="javascript:relatorioPendenteComercial();">Pesquisar</a>
                                </div>                                                                                  
                            </div>
                            <div class="form-group">
                                <a type="button" class="btn btn-success btn-xs" onclick="javascript:relatorioPendenteComercialExport();"><i class="glyphicon glyphicon-download-alt"></i> Exportar</a>
                            </div>
                            <div id="areaRelatorio">

                            </div>
                        </div>                        
                        <!-- ./datatables plugin -->


                        <!-- Copyright -->
                        <?php include("../../../library/copyright.php"); ?>
                        <!-- ./Copyright -->

                    </div>
                    <!-- ./page content container -->                                       
                </div>
                <!-- ./page content -->                                                
            </div>  
            <!-- ./page container -->

            <!-- right bar -->

            <!-- ./right bar -->            

            <!-- page footer -->    
            <?php include("../../../library/footer.php"); ?>
            <!-- ./page footer -->

            <!-- page search -->

            <!-- page search -->            
        </div>
        <!-- ./page wrapper -->


        <!-- javascripts -->
        <?php include("../../../library/rodape.php"); ?>

        <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js"></script>
        <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/plugins/datatables/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/plugins/smartwizard/jquery.smartWizard.js"></script>
        <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/plugins/sortable/sortable.min.js"></script>
        <!-- ./javascripts -->


    </body>
</html>