<?php

include_once( "../../../includes/Dao/DaoSistema.class.php" );
include_once( "../../../includes/Dao/RelatorioDao.class.php" );

include_once("include_novo/Tradutor.class.php");

$Tradutor = new Tradutor();

//echo "<pre>";
//print_r($_REQUEST);
//echo "</pre>";

extract($_REQUEST);

//$dataInicial = '01/04/2016';
//$dataFinal = '20/04/2016';
$RelatorioDao = new RelatorioDao();
//die();
$retorno = $RelatorioDao->relatorioClienteEspecial($dataInicial, $dataFinal, $codCliente, $codRede);

?>


<div class="table-responsive">
    <table class="table table-bordered table-striped table-sortable">
        <thead>
            <tr>

                <th>Dt Liberação</th>
                <th>Usuário</th>
                <th>Rede</th>
                <th>Código Cliente</th>
                <th>Razão Social</th>
				<th>CNPJ</th>
				
				<th>Cód. Ger.</th>
                <th>Ger. Contas</th>
				
				<th>Cód. Ger.</th>
                <th>Ger. Regional</th>
				
                <th>Cód. Ger.</th>
				<th>Ger. Divisional</th>
				
				<th>Prazo Liberado (Dias)</th>
            </tr>
        </thead>                               
        <tbody>
<?php
while ($dados = oci_fetch_object($retorno)) {
    ?>
                <tr>
                    <td><?php echo $dados->DT_LIBERACAO;  ?></td>
                    <td><?php echo $dados->USU_NO_USERNAME;  ?></td>
                    <td><?php echo $dados->RED_CO_NUMERO ;  ?></td>
                    <td><?php echo $dados->CLI_CO_NUMERO ;  ?></td>
                    <td><?php echo $dados->CLI_NO_RAZAO_SOCIAL ;  ?></td>
					<td><?php echo $dados->CLI_NU_CNPJ_CPF ;  ?></td>
					
                    <td><?php echo $dados->VEE_NU_EQUIPE;  ?></td>
					<td><?php echo $dados->GERENTE_CONTAS ;  ?></td>
					
                    <td><?php echo $dados->VEE_NU_REGIONAL;  ?></td>
					<td><?php echo $dados->GERENTE_REGIONAL ;  ?></td>
					
                    <td><?php echo $dados->VEE_NU_DIVISIONAL;  ?></td>
					<td><?php echo $dados->GERENTE_DIVISIONAL ;  ?></td>
					
					<td><?php echo $dados->ESP_NU_DIAS_LIBERACAONF ;  ?></td>
                </tr>
<?php } ?>
        </tbody>
    </table>
</div>
<script language="JavaScript">
    $(function () {
        datatables.init();
    });
</script>