<?php
include_once( "../../../includes/Dao/DaoSistema.class.php" );
include_once( "../../../includes/Dao/RelatorioDao.class.php" );
include_once( "../../../includes/Dao/OcorrenciaDao.class.php" );
include_once( "../../../includes/Status.class.php" );
include_once( "../../../includes/Dao/StatusDao.class.php" );
include_once( "../../../includes/Rota.class.php" );
include_once( "../../../includes/Dao/RotaDao.class.php" );

include_once("include_novo/GlbCentroDistribuicao.class.php");

include_once( "../../../includes/Passo.class.php" );
include_once( "../../../includes/Dao/PassoDao.class.php" );

include_once( "../../../includes/MotivoReprova.class.php" );
include_once( "../../../includes/Dao/MotivoReprovaDao.class.php" );

$RelatorioDao = new RelatorioDao();
$OcorrenciaDao = new OcorrenciaDao();

/* FILTRO CEDIS */
$GlbCentroDistribuicao = new GlbCentroDistribuicao();
$GlbCentroDistribuicao = $OcorrenciaDao->filtroCentroDis();

/* FILTRO ROTA */
$RotaDao = new RotaDao();
$resultRota = $RotaDao->listar();

/* FILTRO STATUS OCORRENCIA */
$Status = new Status();
$StatusDao = new StatusDao();
$Status = $StatusDao->consultar();

/* FILTRO PASSO OCORRENCIA */
$Passo = new Passo();
$PassoDao = new PassoDao();
$Passo = $PassoDao->consultar($Passo);

/* FILTRO MOTIVO REPROVA*/
$MotivoReprovaDao = new MotivoReprovaDao();
$MotivosReprova = $MotivoReprovaDao->consultar();

?>


<!DOCTYPE html>
<html lang="en">
    <head>        
        <!-- meta section -->
        <title>Relatórios - Clientes Especiais</title>
        <?php include("../../../library/head.php"); ?>
        <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/libRelatorio.js"></script>
    </head>
    <body>
        <!-- set loading layer -->
        <!--<div class="dev-page-loading preloader"></div>-->
        <!-- ./set loading layer -->

        <!-- page wrapper -->
        <div class="dev-page">

            <!-- page header -->    
            <?php include("../../../library/topo.php"); ?>
            <!-- ./page header -->

            <!-- page container -->
            <div class="dev-page-container">

                <!-- page sidebar -->
                <?php include("../../../library/menu.php"); ?>
                <!-- ./page sidebar -->

                <!-- page content -->
                <div class="dev-page-content">                    
                    <!-- page content container -->
                    <div class="container">

                        <!-- page title -->
                        <div class="page-title">
                            <h1>Relatório de Clientes Especiais</h1>

                            <ul class="breadcrumb">
                                <li><a href="#">Relatórios</a></li>
                                <li>Cliente Especial</li>
                            </ul>
                        </div>                        
                        <!-- ./page title -->


                        <!-- datatables plugin -->
                        <div class="wrapper wrapper-white">
                            <form id="FormClienteEspecial">
                                <div class="row">
                                    <div class="col-md-2 col-sm-3 col-xs-3">
                                        <div class="form-group">
                                            <label>Início</label>                            
                                            <input type="text" class="form-control datepicker" id="dataInicial" name="dataInicial">
                                        </div> 
                                    </div>  
                                    <div class="col-md-2 col-sm-3 col-xs-3">
                                        <div class="form-group">
                                            <label>Fim</label>                            
                                            <input type="text" class="form-control datepicker" id="dataFinal" name="dataFinal">
                                        </div> 
                                    </div>    
                                    <div class="col-md-2 col-sm-3 col-xs-3">
                                        <div class="form-group">
                                            <label>Cliente</label>
                                            <input type="text" class="form-control" placeholder="Cliente" onKeyPress="fMascara('numero', event, this)" id="codCliente" name="codCliente">
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-3 col-xs-3">
                                        <div class="form-group">
                                            <label>Rede</label>
                                            <input type="text" class="form-control" placeholder="Rede" onKeyPress="fMascara('numero', event, this)" id="codRede" name="codRede">
                                        </div>
                                    </div>  

                                </div>  
                            </form>
                            <div class="row">
                                <div class="col-md-2">
                                    <a class="btn btn-primary" onclick="javascript:relatorioClienteEspecial();">Pesquisar</a>
                                </div>                                                                                  
                            </div>
                            <div class="form-group">
                                <a type="button" class="btn btn-success btn-xs" onclick="javascript:relatorioClienteEspecialExport();"><i class="glyphicon glyphicon-download-alt"></i> Exportar</a>
                            </div>
                            <div id="areaRelatorio">

                            </div>
                        </div>                        
                        <!-- ./datatables plugin -->


                        <!-- Copyright -->
                        <?php include("../../../library/copyright.php"); ?>
                        <!-- ./Copyright -->

                    </div>
                    <!-- ./page content container -->                                       
                </div>
                <!-- ./page content -->                                                
            </div>  
            <!-- ./page container -->

            <!-- right bar -->

            <!-- ./right bar -->            

            <!-- page footer -->    
            <?php include("../../../library/footer.php"); ?>
            <!-- ./page footer -->

            <!-- page search -->

            <!-- page search -->            
        </div>
        <!-- ./page wrapper -->


        <!-- javascripts -->
        <?php include("../../../library/rodape.php"); ?>

        <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js"></script>
        <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/plugins/datatables/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/plugins/smartwizard/jquery.smartWizard.js"></script>
        <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/plugins/sortable/sortable.min.js"></script>
        <!-- ./javascripts -->


    </body>
</html>