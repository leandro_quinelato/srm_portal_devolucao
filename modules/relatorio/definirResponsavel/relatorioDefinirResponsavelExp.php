<?php
date_default_timezone_set('Brazil/East');
header('Content-type: application/x-msdownload');
header('Content-Disposition: attachment; filename=relatorio_Pendente_Responsavel' .date("d-m-Y-His").'.xls');
header('Pragma: no-cache');
header('Expires: 0');

ini_set('max_execution_time', -1);

include_once( "../../../includes/Dao/DaoSistema.class.php" );
include_once( "../../../includes/Dao/RelatorioDao.class.php" );

include_once("include_novo/Tradutor.class.php");

$Tradutor = new Tradutor();

//echo "<pre>";
//print_r($_REQUEST);
//echo "</pre>";

extract($_REQUEST);

//$dataInicial = '01/04/2016';
//$dataFinal = '20/04/2016';
$RelatorioDao = new RelatorioDao();

$retorno = $RelatorioDao->relatorioPendenteResponsavel($dataInicial, $dataFinal, $ced_co_numero, $dataInicialPasso, $dataFinalPasso);

?>

<div class="table-responsive">
    <table class="table table-bordered table-striped table-sortable">
        <thead>
            <tr>
                <th>Data</th>
                <th>Protocolo</th>
				<th>Aprovação</br> Devolução</th>
                <th>Série</th>
				<th>NF Venda</th>
				<th>Emissão</th>
				<th>Motivo Devolução</th>
                <th>CD</th>
            </tr>
        </thead>                               
        <tbody>
<?php
while ($dados = oci_fetch_object($retorno)) {
    ?>
			<tr>
				<td><?php echo $dados->DATA_CADASTRO;  ?></td>
				<td><?php echo $dados->DOC_CO_NUMERO;  ?></td>
				<td><?php echo $dados->DOP_DT_SAIDA;  ?></td>
				<td><?php echo $dados->NTO_NU_SERIE; ?></td>
				<td><?php echo $dados->NTO_NU_NOTA; ?></td>
				<td><?php echo $dados->NTO_DT_NOTA ;  ?></td>
				<td><?php echo $dados->TOC_NO_DESCRICAO ;  ?></td>
				<td><?php echo $dados->CED_NO_CENTRO ;  ?></td>
			</tr>
<?php } ?>


        </tbody>
    </table>
</div>
