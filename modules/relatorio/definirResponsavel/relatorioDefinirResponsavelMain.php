<?php
session_start();
include_once( "../../../includes/Dao/DaoSistema.class.php" );
include_once( "../../../includes/Dao/RelatorioDao.class.php" );

include_once("DaoGlobal.class.php");
include_once("SistemaDao.class.php");
include_once("GlbSistema.class.php");
include_once("GlbSistemaPerfil.class.php");
include_once("GlbUsuario.class.php");
include_once("GlbColaborador.class.php");
include_once("GlbPessoa.class.php");
include_once("GlbEndereco.class.php");
include_once("GlbTelefone.class.php");
include_once("include_novo/GlbTransportadora.class.php");
include_once("include_novo/GlbContatoTransportador.class.php");
include_once("include_novo/ContatoTransportadorDao.class.php");
include_once("GlbProgramaRotina.class.php");
include_once("GlbModuloPrograma.class.php");
include_once("GlbSistemaModulo.class.php");
include_once("PerfilUsuarioDao.class.php");
include_once("PerfilPermissaoDao.class.php");
include_once("integraSetSistemas.php");
include_once("class.phpmailer.php");
include_once("class.smtp.php");

include_once("include_novo/Tradutor.class.php");

$Tradutor = new Tradutor();
$integra = new integraSetSistemas("conf.xml");
$ObjUsuario = unserialize($integra->getSistemaIntegra());

include_once("include_novo/Tradutor.class.php");

$Tradutor = new Tradutor();

//echo "<pre>";
//print_r($_REQUEST);
//echo "</pre>";

extract($_REQUEST);

//$dataInicial = '01/04/2016';
//$dataFinal = '20/04/2016';
$RelatorioDao = new RelatorioDao();

$retorno = $RelatorioDao->relatorioPendenteResponsavel($dataInicial, $dataFinal, $ced_co_numero, $dataInicialPasso, $dataFinalPasso);

?>

<div class="table-responsive">
    <table class="table table-bordered table-striped table-sortable">
        <thead>
            <tr>
                <th>Data</th>
                <th>Protocolo</th>
				<th>Aprovação</br> Devolução</th>
                <th>Série</th>
				<th>NF Venda</th>
				<th>Emissão</th>
				<th>Motivo Devolução</th>
                <th>CD</th>
            </tr>
        </thead>                               
        <tbody>
<?php
while ($dados = oci_fetch_object($retorno)) {
    ?>
			<tr>
				<td><?php echo $dados->DATA_CADASTRO;  ?></td>
				<td><?php echo $dados->DOC_CO_NUMERO;  ?></td>
				<td><?php echo $dados->DOP_DT_SAIDA;  ?></td>
				<td><?php echo $dados->NTO_NU_SERIE; ?></td>
				<td><?php echo $dados->NTO_NU_NOTA; ?></td>
				<td><?php echo $dados->NTO_DT_NOTA ;  ?></td>
				<td><?php echo $dados->TOC_NO_DESCRICAO ;  ?></td>
				<td><?php echo $dados->CED_NO_CENTRO ;  ?></td>
			</tr>
<?php } ?>


        </tbody>
    </table>
</div>
    <script language="JavaScript">
        $(function() {
          datatables.init();
        });
    </script>