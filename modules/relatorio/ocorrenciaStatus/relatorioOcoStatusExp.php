<?php
date_default_timezone_set('Brazil/East');
header('Content-type: application/x-msdownload');
header('Content-Disposition: attachment; filename=relatorio_OcorrenciaStatus' .date("d-m-Y-His").'.xls');
header('Pragma: no-cache');
header('Expires: 0');

ini_set('max_execution_time', -1);

include_once( "../../../includes/Dao/DaoSistema.class.php" );
include_once( "../../../includes/Dao/RelatorioDao.class.php" );

include_once("include_novo/Tradutor.class.php");

$Tradutor = new Tradutor();

//echo "<pre>";
//print_r($_REQUEST);
//echo "</pre>";

extract($_REQUEST);

//$dataInicial = '01/04/2016';
//$dataFinal = '20/04/2016';
$RelatorioDao = new RelatorioDao();

$retorno = $RelatorioDao->relatorioOcorrenciaStatus($dataInicial, $dataFinal, $protocolo, $codCliente, $codRede, $statusOcorrencia, $rot_co_numero, $ced_co_numero);

?>

<div class="table-responsive">
    <table class="table table-bordered table-striped table-sortable">
        <thead>
            <tr>
                <th>Status</th>
                <th>Protocolo</th>
                <th>Data</th>
                <th>Usuario</th>
                <th>Rota</th>
                <th>Tipo</th>
                <th>NF Origem</th>
                <th>Divisao</th>
                <th>CD</th>
                <th>NF Devolucao</th>
                <th>Valor</th>
                <th>Qtd. Volume</th>
                <th>Rede</th>
                <th>Cod Cliente</th>
                <th>Cliente</th>
            </tr>
        </thead>                               
        <tbody>
<?php
while ($dados = oci_fetch_object($retorno)) {
    ?>
                <tr>
                    <td><?php echo $dados->DST_NO_DESCRICAO;  ?></td>
                    <td><?php echo $dados->DOC_CO_NUMERO;  ?></td>
                    <td><?php echo $dados->DATA_CADASTRO;  ?></td>
                    <td><?php echo $dados->USU_NO_USERNAME ;  ?></td>
                    <td><?php echo $dados->ROT_CO_NUMERO ;  ?></td>
                    <td><?php echo $dados->TID_CO_DESCRICAO ;  ?></td>
                    <td><?php echo $dados->NTO_NU_SERIE ." - ". $dados->NTO_NU_NOTA;  ?></td>
                    <td><?php echo $dados->DIVISAO ;  ?></td>
                    <td><?php echo $dados->CED_NO_CENTRO ;  ?></td>
                    <td><?php echo $dados->DOC_NU_DEVSERIE ." - ". $dados->DOC_NU_DEVNOTA;  ?></td>
                    <td><?php echo $Tradutor->formatar($dados->DOC_VR_VALOR, 'MOEDA') ;  ?></td>
                    <td><?php echo $dados->DOC_QT_PRODUTO;  ?></td>
                    <td><?php echo $dados->RED_CO_NUMERO ;  ?></td>
                    <td><?php echo $dados->CLI_CO_NUMERO;  ?></td>
                    <td><?php echo $dados->CLI_NO_FANTASIA;  ?></td>
                </tr>
<?php } ?>


        </tbody>
    </table>
</div>
