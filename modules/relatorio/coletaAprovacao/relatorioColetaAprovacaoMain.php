<?php
include_once( "../../../includes/Dao/DaoSistema.class.php" );
include_once( "../../../includes/Dao/RelatorioDao.class.php" );

include_once( "../../../includes/CalTipoOcorrencia.class.php" );
include_once( "../../../includes/Dao/TipoOcorrenciaDAO.class.php" );

include_once( "../../../includes/TipoDevolucao.class.php" );
include_once( "../../../includes/Dao/TipoDevolucaoDao.class.php" );

include_once( "../../../includes/Ocorrencia.class.php" );
include_once( "../../../includes/Dao/OcorrenciaDao.class.php" );
include_once( "../../../includes/Ocorrencia.class.php" );
include_once( "../../../includes/Dao/OcorrenciaDao.class.php" );

include_once( "../../../includes/DevNotaOrigem.class.php" );

include_once( "../../../includes/DevNotaOrigemItem.class.php" );
include_once( "../../../includes/Dao/DevNotaOrigemItemDao.class.php" );

include_once( "../../../includes/DevParametrosValorDevolucao.class.php" );
include_once( "../../../includes/Dao/ParametroValorDevolucaoDao.class.php" );

include_once( "../../../includes/Dao/DaoSistema.class.php" );
include_once( "../../../includes/DevParametros.class.php" );
include_once( "../../../includes/Dao/ParametroDao.class.php" );

include_once( "../../../includes/DevParametroLog.class.php" );
include_once( "../../../includes/Dao/ParametroLogDao.class.php" );

include_once( "../../../includes/Multa.class.php" );
include_once( "../../../includes/Dao/MultaDao.class.php" );

include_once( "../../../includes/DevParametrosValorDevolucao.class.php" );
include_once( "../../../includes/Dao/ParametroValorDevolucaoDao.class.php" );

include_once("DaoGlobal.class.php");
include_once("SistemaDao.class.php");
include_once("GlbSistema.class.php");
include_once("GlbSistemaPerfil.class.php");
include_once("GlbUsuario.class.php");
include_once("GlbColaborador.class.php");
include_once("GlbPessoa.class.php");
include_once("GlbEndereco.class.php");
include_once("GlbTelefone.class.php");
include_once("include_novo/GlbTransportadora.class.php");
include_once("include_novo/GlbContatoTransportador.class.php");
include_once("include_novo/ContatoTransportadorDao.class.php");
include_once("GlbProgramaRotina.class.php");
include_once("GlbModuloPrograma.class.php");
include_once("GlbSistemaModulo.class.php");
include_once("PerfilUsuarioDao.class.php");
include_once("PerfilPermissaoDao.class.php");
include_once("integraSetSistemas.php");
include_once("class.phpmailer.php");
include_once("class.smtp.php");

include_once("include_novo/Tradutor.class.php");


$ObjParametro = new devParametros();
$ParametroDao = new ParametroDao();
$ObjParametro = $ParametroDao->consultar();


$ParametroLogDao = new ParametroLogDao();
$result_logParam = $ParametroLogDao->consultar();
ocifetchinto($result_logParam, $logParam, OCI_ASSOC);

$ObjMulta = new Multa();

$MultaDao = new MultaDao();
$result = $MultaDao->consultar($ObjMulta);
OCIFetchInto ($result, $parMulta, OCI_ASSOC);

$ParametroValorDevolucaoDao = new ParametroValorDevolucaoDao();
$ObjParametroValorDevolucao = $ParametroValorDevolucaoDao->consultar();

$Tradutor = new Tradutor();
$integra = new integraSetSistemas("conf.xml");
$ObjUsuario = unserialize($integra->getSistemaIntegra());

//echo "<pre>";
//print_r($_REQUEST);
//echo "</pre>";
//die();

extract($_REQUEST);

//$dataInicial = '01/04/2016';
//$dataFinal = '20/04/2016';
$RelatorioDao = new RelatorioDao();

if(count($codDivisao) > 0 ){
    $auxDivisao = "";
    for ($i = 0; count($codDivisao) > $i; $i++) {
        if($auxDivisao != ""){
            $auxDivisao .= ", ";
        }
        $auxDivisao .= "'{$codDivisao[$i]}'";
    }
    $codDivisao = $auxDivisao;
}

$retorno = $RelatorioDao->relatorioColetaAprovacao($dataInicial, $dataFinal, $codOcorrencia, $cliente, $rot_co_numero,$procedimento, $tresp, $leadtime, $status, $ced_co_numero, $codDivisao, $ObjUsuario)

?>
<script language="JavaScript">
    function abrirLinha(cod){
        document.getElementById('trNotas_'+cod).style.visibility = "visible";
        document.getElementById('sinalMais_'+cod).style.display = 'none';
        document.getElementById('sinalMenos_'+cod).style.display = 'block';
    }
    function fecharLinha(cod){
        document.getElementById('trNotas_'+cod).style.visibility = "hidden";
        document.getElementById('sinalMais_'+cod).style.display = 'block';
        document.getElementById('sinalMenos_'+cod).style.display = 'none';
    }
    function testaData()
</script>
<form id="frmEmailColeta" name="frmEmailColeta" >
    <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="form-group">
                <label>Insira o email para receber a ordem de coleta</label>
                <div class="input-group">
                    <input type="text" value="" class="form-control" name="emailColeta" id="emailColeta">
                    <div class="input-group-btn"><a class="btn btn-default" onclick="enviarColetaAprovacao();">Enviar</a></div>
                    <div id="retornoEmail"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="form-group">
                <label>Gerar Arquivo de Coleta para Impressão</label>
                <div class="input-group">
                    <div class="input-group-btn"><a class="btn btn-default" onclick="criarColetaAprovacao();">Gerar</a></div>
                    <div id="retornoEmail"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="table-responsive">
        <!-- <table class="table table-bordered table-striped table-sortable"> -->
        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th></th>
                <th>
                    Email
                    <div class="checkbox checkbox-inline">
                        <!--<input type="checkbox" id="check_1">-->
                        <input type="checkbox" name="todos" id="todos" value="todos" onclick="marcardesmarcar();" />
                        <label for="todos"></label>
                    </div>
                </th>
                <th>Notas Venda</th>
                <th>Protocolo</th>
                <th>Data</th>
                <th>Cadastrante</th>
                <th>Cod.</th>
                <th>Cliente</th>
                <th>Motivo Devolução</th>
                <th>Rota</th>
                <th>Setor terc.</th>
                <th>Divisão</th>
                <th>CD</th>
                <th>Aprovação</th>
               <!-- <th>Data Lead</th> -->
                <th>Lead time</th>
                <th>Status</th>
                <th>Dt Envio /Download Ordem Coleta</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $Ocorrencia = new Ocorrencia();
            $OcorrenciaDao = new OcorrenciaDao();
            $NotasOrigem = new DevNotaOrigem();

            while ($dados = oci_fetch_object($retorno)) {
            ?>
            <tr>
                <?php
                if($dados->LEAD_TIME > number_format($ObjParametroValorDevolucao->getValor50F(), 0, ',', '.')){
                    $sinalizar = 'style="background:red;"';
                }else{
                    $sinalizar = '';
                }
                ?>
                <td <?=$sinalizar?> ><button class="fa fa-file-pdf-o" type="button" onclick="javascript: downloadArquivosRel(<?php  echo $dados->DOC_CO_NUMERO; ?>, 2);"></button></td>
                <td>
                    <!-- <div class="checkbox checkbox-inline"> -->
                    <!--<input type="checkbox" id="check_2">-->
                    <input  name="box_dev[]" type="checkbox" value="<?php echo $dados->DOC_CO_NUMERO; ?>" class="selecionar"/>
                    <!-- <label for="box_dev"></label> -->
    </div>
    </td>

    <td> <span id="sinalMenos_<?=$dados->DOC_CO_NUMERO?>" onclick="fecharLinha(<?=$dados->DOC_CO_NUMERO?>)">(-)</span>
        <span id="sinalMais_<?=$dados->DOC_CO_NUMERO?>" style="display:none;" onclick="abrirLinha(<?=$dados->DOC_CO_NUMERO?>)">(+)</span> </td>
    <td><?php echo $dados->DOC_CO_NUMERO;  ?></td>
    <td><?php echo $dados->DOC_DT_CADASTRO;  ?></td>
    <td><?php echo $dados->USU_NO_USERNAME;  ?></td>
    <td><?php echo $dados->CLI_CO_NUMERO;  ?></td>
    <td><?php echo $dados->CLI_NO_RAZAO_SOCIAL;  ?></td>
    <td><?php echo $dados->TOC_NO_DESCRICAO;  ?></td>
    <td><?php echo $dados->ROT_CO_NUMERO;  ?></td>
    <td><?php echo $dados->CLI_NU_ROTA_TERCEIRIZADA;  ?></td>
    <td><?php echo $dados->DEV_CO_DIVISAO;  ?></td>
    <td><?php echo $dados->CED_NO_CENTRO;  ?></td>
    <td><?php echo $dados->DOS_DT_ENTRADA;  ?></td>

    <td><?php echo number_format(($dados->LEAD_TIME - $dados->DATA_LEAD), 0);  ?></td>
    <td><?php echo $dados->STATUS;  ?></td>

    <td><?php echo $dados->DOC_DT_COLETA_ENVIADA;  ?></td>

    </tr>
    <tr id="trNotas_<?=$dados->DOC_CO_NUMERO?>">
        <td colspan="17">
            <table border="0">
                <?php
                $Ocorrencia->setCodigoDevolucao($dados->DOC_CO_NUMERO);
                $NotasOrigem = $OcorrenciaDao->consultaNotasDevolucao($Ocorrencia);
                foreach ($NotasOrigem as $NotaOrigem) {
                    ?>
                    <tr>
                        <td width="100px"></td>
                        <td>Série: <?=$NotaOrigem->getSerie();?></td>
                        <td>Nota: <?=$NotaOrigem->getNota();?></td>
                        <td>Data: <?=$NotaOrigem->getData();?></td>
                    </tr>
                    <?php
                }
                ?>
            </table>
        </td>
    </tr>
    <?php
    }
    ?>
    </tbody>
    </table>
    </div>
</form>

<script language="JavaScript">
    $(function() {
        datatables.init();
    });
</script>