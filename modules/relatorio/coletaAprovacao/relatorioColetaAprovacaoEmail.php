<?php
ini_set('display_errors', 1);
ini_set('memory_limit', '-1');
include_once( "../../../includes/mpdf60/mpdf.php");

include_once( "../../../includes/Dao/DaoSistema.class.php" );
include_once( "../../../includes/Dao/RelatorioDao.class.php" );
include_once( "../../../includes/Ocorrencia.class.php" );
include_once( "../../../includes/CalTipoOcorrencia.class.php" );
include_once( "../../../includes/Dao/TipoOcorrenciaDAO.class.php" );
include_once( "../../../includes/TipoDevolucao.class.php" );
include_once( "../../../includes/Dao/TipoDevolucaoDao.class.php" );

include_once( "../../utilitario/email/emailPortalDevolucao.class.php" );
include_once( "../../utilitario/ordemColeta/ordemColetaDevolucaoMain.php" );

include_once("DaoGlobal.class.php");
include_once("SistemaDao.class.php");
include_once("GlbSistema.class.php");
include_once("GlbSistemaPerfil.class.php");
include_once("GlbUsuario.class.php");
include_once("GlbColaborador.class.php");
include_once("GlbPessoa.class.php");
include_once("GlbEndereco.class.php");
include_once("GlbTelefone.class.php");
include_once("include_novo/GlbTransportadora.class.php");
include_once("include_novo/GlbContatoTransportador.class.php");
include_once("include_novo/ContatoTransportadorDao.class.php");
include_once("GlbProgramaRotina.class.php");
include_once("GlbModuloPrograma.class.php");
include_once("GlbSistemaModulo.class.php");
include_once("PerfilUsuarioDao.class.php");
include_once("PerfilPermissaoDao.class.php");
include_once("integraSetSistemas.php");
include_once("class.phpmailer.php");
include_once("class.smtp.php");

include_once("include_novo/Tradutor.class.php");
extract($_REQUEST);

$Tradutor = new Tradutor();
$integra = new integraSetSistemas("conf.xml");
$ObjUsuario = unserialize($integra->getSistemaIntegra());

//echo "<pre>";
//print_r($_REQUEST);
//echo "</pre><br>";


//die();
$emailPortalDevolucao = new emailPortalDevolucao();

if (trim($emailColeta) != "") {


    if (isset($box_dev)) {
		
		// assume $str esteja em UTF-8
		$map = array(
			'á' => 'a',
			'à' => 'a',
			'ã' => 'a',
			'â' => 'a',
			'é' => 'e',
			'ê' => 'e',
			'í' => 'i',
			'ó' => 'o',
			'ô' => 'o',
			'õ' => 'o',
			'ú' => 'u',
			'ç' => 'c',
			'Á' => 'A',
			'À' => 'A',
			'Ã' => 'A',
			'Â' => 'A',
			'É' => 'E',
			'Ê' => 'E',
			'Í' => 'I',
			'Ó' => 'O',
			'Ô' => 'O',
			'Õ' => 'O',
			'Ú' => 'U',
			'Ç' => 'C'
		);

		$mpdf = new mPDF();
		for ($i = 0; count($box_dev) > $i; $i++) {
			 if ($i > 0){
				$mpdf->AddPage();
			 }
			 
			$protocolo = $box_dev[$i];
			
		$dep = 29;
		$sal = 1;
	//    $protocolo = 1500246040033; //1500216082084;//1500246040033;//1500236076005;

		$emissaoProtocolo = date("d/m/Y");

		$ObjOcorrencia = new Ocorrencia();
		$ObjOcorrencia->setCodigoDevolucao($protocolo);
		
	//    $codigoBarras = 1600244229070;//$protocolo;
	//    new barCodeGenrator($codigoBarras,1,$codigoBarras.'_teste.gif', 160, 70, true);

		$OcorrenciaDao = new OcorrenciaDao();
		$result_devolucao = $OcorrenciaDao->consultaOrdemColeta($ObjOcorrencia);
		OCIFetchInto($result_devolucao, $row_dev, OCI_ASSOC);

		$tipo = $row_dev['TID_CO_DESCRICAO'];
		$tipo = utf8_encode($tipo);



	//    if ($row_dev['DOC_IN_TIPO'] == 1) {
	//        $tipo = "Parcial";
	//    } else {
	//        $tipo = "Integral";
	//    }

		if (isset($row_dev["DOC_NU_DEVSERIE"])) {
			$doc_nu_devserie = $row_dev["DOC_NU_DEVSERIE"];
		} else {
			$doc_nu_devserie = "&nbsp;";
		}


		if (isset($row_dev["DOC_NU_DEVNOTA"])) {
			$doc_nu_devnota = $row_dev["DOC_NU_DEVNOTA"];
		} else {
			$doc_nu_devnota = "&nbsp;";
		}

		if (isset($row_dev["DOC_DT_DEVNOTA"])) {
			$doc_dt_devnota = $row_dev["DOC_DT_DEVNOTA"];
		} else {
			$doc_dt_devnota = "&nbsp;";
		}

		if (isset($row_dev['DOC_TX_DESCRICAO'])) {

	//    $descricao = utf8_encode($row_dev['DOC_TX_DESCRICAO']->load());
			$descricaotst = $row_dev['DOC_TX_DESCRICAO']->load();
			$descricao = $descricaotst;
		}


		$lin = '';
	//echo $descricao[0];die();
		if (strlen($descricao) > 200) {
			$inicio = 0;
			$countCaracteres = 0;
			$lin = null;

			for ($l = 0; $l < 200; $l++) {
				$indice = $l;
				if (isset($descricao[$indice])) {
					$lin = $lin . $descricao[$indice];
				}
			}
	//        $descricao = $lin;
			$descricao = utf8_encode(strtr($lin, $map));
		}
		$descricao = utf8_encode(strtr($descricaotst, $map));


	//    $logoPath = $_SERVER['DOCUMENT_ROOT']. 'devolucao/devolucao_externo/img/logo.jpg'; //testes
		$logoPath = $_SERVER['DOCUMENT_ROOT'] . '/img/logoServimed.jpg'; //testes externo
	//    $logoPath = '/servimed/webpoint/CAL/devolucao/devolucao_externo/img/logoServimed.jpg'; //testes
	// Set some content to print
		$html = '
					<html xmlns="http://www.w3.org/1999/xhtml">
					<head>
					<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
					<title>Ordem de Coleta</title>
					</head>
						<body>
							<div style="border:1px solid #000; margin:0 auto; width:800px;">
									<table cellpadding="0" cellspacing="0" border="0" style="font-family:Tahoma, Geneva, sans-serif; font-size:12px; line-height:16px; width:100%;">
									<tbody>
										<tr>
											<td style="border-bottom:1px solid #000;">&nbsp;</td>
											<td colspan="1" style="border-bottom:1px solid #000;">
												<img src="' . $logoPath . '" style="float:left;" />
											</td>
											<td style="border-bottom:1px solid #000;">
												<span style="font-size:20px; line-height:70px;">SERVIMED COMERCIAL LTDA</span>
											</td>
											<td style="border-bottom:1px solid #000;">
										<barcode code="' . $protocolo . '" type="C39" size="0.6" height="3.0" />
											</td>
										</tr>

										<tr>
											<td style="padding-top:10px;"></td>
											<td style="padding-top:10px;"><strong>Destinatário:</strong> <span>' . $row_dev['CED_NO_DESTINATARIO'] . '</span></td>
											<td style="padding-top:10px;"><strong>CNPJ:</strong> <span>' . $row_dev["CED_NU_CNPJ"] . '</span></td>
											<td style="padding-top:10px;"><strong>CD:</strong> <span>' . $row_dev["CED_NO_CENTRO"] . '</span></td>
										</tr>
										<tr>
											<td></td>
											<td><strong>Rua:</strong> <span>' . utf8_encode($row_dev["CED_NO_ENDERECO"]) . '</span></td>
											<td><strong>Número:</strong> <span>' . utf8_encode($row_dev["CED_ED_NUMERO"]) . '</span></td>
											<td><strong>CEP:</strong> <span>' . $row_dev["CED_NU_CEP"] . '</span></td>
										</tr>
										<tr>
											<td style="padding-bottom:10px; border-bottom:1px solid #000;"></td>
											<td style="padding-bottom:10px; border-bottom:1px solid #000;"><strong>Bairro:</strong> <span>' . utf8_encode($row_dev["CED_NO_BAIRRO"]) . '</span></td>
											<td style="padding-bottom:10px; border-bottom:1px solid #000;"><strong>Cidade:</strong> <span>' . utf8_encode($row_dev["CIDADE_CED"]) . '</span></td>
											<td style="padding-bottom:10px; border-bottom:1px solid #000;"><strong>Estado:</strong> <span>' . $row_dev["ESTADO_CED"] . '</span></td>
										</tr>
										<tr>
											<td colspan="4" style="padding-top:10px; padding-bottom:10px; border-bottom:1px solid #000; text-align:center; font-weight:bold;">ORDEM DE COLETA DE DEVOLUÇÃO</td>
										</tr>
										<tr>
											<td style="padding-top:10px; padding-bottom:10px; border-bottom:1px solid #000;"></td>
											<td style="padding-top:10px; padding-bottom:10px; border-bottom:1px solid #000;"><strong>Protocolo:</strong> <span>' . $row_dev["DOC_CO_NUMERO"] . '</span></td>
											<td colspan="2" style="padding-top:10px; padding-bottom:10px; border-bottom:1px solid #000;"><strong>Data Emissão:</strong> <span> ' . $emissaoProtocolo . '</span></td>
										</tr>
										<tr>
											<td style="padding-top:10px;"></td>
											<td style="padding-top:10px;"><strong>Código Cliente:</strong> <span>' . $row_dev["CLI_CO_NUMERO"] . '</span></td>
											<td style="padding-top:10px;"><strong>Cliente:</strong> <span>' . $row_dev["CLI_NO_RAZAO_SOCIAL"] . '</span></td>
											<td style="padding-top:10px;"><strong>Rota:</strong> <span>' . $row_dev["ROT_CO_NUMERO"] . '</span></td>
										</tr>
										<tr>
											<td></td>
											<td><strong>Setor Terc:</strong> <span>' . $row_dev["CLI_NU_ROTA_TERCEIRIZADA"] . '</span></td>
											<td><strong>Nome Contato:</strong> <span>' . utf8_encode($row_dev["DOC_NO_CONTATO"]) . '</span></td>
											<td><strong>Depart. Contato:</strong> <span>' . utf8_encode($row_dev["DOC_NO_DEPOCONTATO"]) . '</span></td>
										</tr>
										<tr>
											<td></td>
											<td><strong>Rua:</strong> <span>' . utf8_encode($row_dev["CLI_ED_LOGRADOURO"]) . '</span></td>
											<td><strong>Número:</strong> <span>' . $row_dev["CLI_ED_NUMERO"] . '</span></td>
											<td><strong>CEP:</strong> <span>' . $row_dev["CLI_NU_CEP"] . '</span></td>
										</tr>
										<tr>
											<td style="padding-bottom:10px; border-bottom:1px solid #000;"></td>
											<td style="padding-bottom:10px; border-bottom:1px solid #000;"><strong>Bairro:</strong> <span>' . $row_dev["CLI_NO_BAIRRO"] . '</span></td>
											<td style="padding-bottom:10px; border-bottom:1px solid #000;"><strong>Cidade:</strong> <span>' . $row_dev["CIDADE_CLIENTE"] . '</span></td>
											<td style="padding-bottom:10px; border-bottom:1px solid #000;"><strong>Estado:</strong> <span>' . $row_dev["ESTADO_CLIENTE"] . '</span></td>
										</tr>
										<tr>
											<td style="padding-top:10px;"></td>
											<td style="padding-top:10px;"><strong>Qntd. Volume(s) à Coletar:</strong> <span>' . $row_dev["DOC_QT_PRODUTO"] . '</span></td>
											<td style="padding-top:10px;" colspan="2"><strong>Tipo Devolução:</strong> <span>' . $tipo . '</span></td>
										</tr>
										<tr>
											<td></td>
											<td><strong>Motivo da Devolução:</strong> <span>' . utf8_encode($row_dev["TOC_NO_DESCRICAO"]) . '</span></td>
											<td colspan="2"><strong>Descrição Motivo:</strong> <span>' . strtolower($descricao) . '</span></td>
										</tr>
										<tr>
											<td></td>
											<td><strong>Nota Devolução:</strong> <span>' . $doc_nu_devserie . ' ' . $doc_nu_devnota . '</span></td>
											<td colspan="2"><strong>Emissão:</strong> <span>' . $doc_dt_devnota . '</span></td>
										</tr>
										<tr class="Linha">
											<td style="padding-bottom:10px; border-bottom:1px solid #000;"></td>
											<td colspan="3" style="padding-bottom:10px; border-bottom:1px solid #000;">
												';
												$result_nota_origem = $OcorrenciaDao->consultaNotaOrigem($ObjOcorrencia);
												while (OCIFetchInto($result_nota_origem, $row_nota, OCI_ASSOC)) {
												$html .= '
												<strong style="font-size:10px;">NF Orig: <span style="font-weight:normal;"> ' . $row_nota["NTO_NU_SERIE"] . ' ' . $row_nota["NTO_NU_NOTA"] . '/</span></strong>
												<strong style="font-size:10px;">Emissão: <span style="font-weight:normal;"> ' . $row_nota["NTO_DT_NOTA"] . ' #</span></strong>
												';
												}
												$html .= '
											</td>
										</tr>
										<tr>
											<td colspan="4" style="text-align:center; padding-top:10px;"><strong>Data da coleta:  ______/_____/________</strong></td>
										</tr>
										<tr>
											<td style="padding-top:10px;"></td>
											<td><span>Nome do Motorista (legível):<br />____________________________</span></td>
											<td colspan="2"><span>Nome do Cliente (legível):<br />____________________________________________</span></td>	
										</tr>
										<tr class="Linha">
											<td style="padding-bottom:10px; border-bottom:1px solid #000;"></td>
											<td style="padding-bottom:10px; border-bottom:1px solid #000;"><span>Assinatura:<br />____________________________</span></td>
											<td colspan="2" style="padding-bottom:10px; border-bottom:1px solid #000;"><span>Assinatura:<br />____________________________________________</span></td>
										</tr>
										<tr>
											<td colspan="4" style="padding-top:10px; text-align:center;"><strong>Em caso da devolução não estar disponível, realizar o preechimento abaixo</strong></td>
										</tr>
										<tr>
											<td></td>
											<td><strong>1ª Tentativa</strong> - Data:___/___/_____</td>
											<td colspan="2"> <strong>2ª Tentativa</strong> - Data:___/___/_____</td>
										</tr>
										<tr>
											<td></td>
											<td><strong style="font-size:15px; font-weight:normal;">[ ]</strong> <span>Devolucao nao esta pronta</span></td>
											<td colspan="2"><strong style="font-size:15px; font-weight:normal;">[ ]</strong> <span>Devolucao nao esta pronta</span></td>
										</tr>
										<tr>
											<td></td>
											<td><strong style="font-size:15px; font-weight:normal;">[ ]</strong> <span>Devolucao ja foi coletada</span></td>
											<td colspan="2"><strong style="font-size:15px; font-weight:normal;">[ ]</strong> <span>Devolucao ja foi coletada</span></td>
										</tr>
										<tr>
											<td></td>
											<td><strong style="font-size:15px; font-weight:normal;">[ ]</strong> <span>Nao existe devolucao</span></td>
											<td colspan="2"><strong style="font-size:15px; font-weight:normal;">[ ]</strong> <span>Nao existe devolucao</span></td>
										</tr>
										<tr>
											<td></td>
											<td><span>Outros:<br />____________________________</span></td>
											<td colspan="2"><span>Outros:<br />__________________________________</span></td>
										</tr>
										<tr>
											<td></td>
											<td style="padding-bottom:10px;"><span>Nome/Cliente:<br />____________________________</span></td>
											<td style="padding-bottom:10px;" colspan="2">Nome/Cliente:<br />__________________________________</td>
										</tr>

										<tr>
											<td colspan="4" style="border-bottom:1px dashed #000;"></td>
										</tr>
										<tr>
											<td colspan="4" style="padding-top:10px; text-align:center;"><strong>Protocolo Cliente</strong></td>
										</tr>
										<tr>
											<td colspan="4" style="height:40px;">
												<strong>Protocolo Nº:</strong> <span>' . $row_dev["DOC_CO_NUMERO"] . '</span> / 
												<strong>QTD. VOL.:</strong> <span>' . $row_dev["DOC_QT_PRODUTO"] . '</span> / 
												<strong>DATA:</strong> <span>' . $emissaoProtocolo . '</span> / 
												<strong>Rota:</strong> <span>' . $row_dev["ROT_CO_NUMERO"] . '</span> / 
												<strong>Setor Terc:</strong> <span>' . $row_dev["CLI_NU_ROTA_TERCEIRIZADA"] . '</span>
											</td>
										</tr>
										<tr>
											<td style="padding-bottom:10px;"></td>
											<td style="padding-bottom:10px;"><span>Data da Coleta: ____/____/______</span></td>
											<td style="padding-bottom:10px;" colspan="2"><span>__________________________________<br />Assinatura Transportador</span></td>
										</tr>
									</tbody>
								</table>
							</div>
						</body>
					</html>
						'; 
		
		
	//    $html = "Hello Geovanni!";

		$mpdf->WriteHTML($html);
		$OcorrenciaDao->ordemColetaEntregue($protocolo);
		
			if($i==0){
				$protocoloInicio = $protocolo;
			}

		}
		
		$nomeArq = $protocoloInicio ."a". $protocolo;
		
	   // $mpdf->Output("ordemColetaAprovacao_{$emissaoProtocolo}.pdf", "D");
		$mpdf->Output("../../utilitario/arquivos/ordemColeta/arq/ordemColeta_{$nomeArq}.pdf", "F");	
				
		
        //for ($i = 0; count($box_dev) > $i; $i++) {
            //$Ocorrencia = new Ocorrencia();
            //$Ocorrencia->setCodigoDevolucao($box_dev[$i]);
            //DocumentoColeta($Ocorrencia->getCodigoDevolucao(), true, true); 
            $emailPortalDevolucao->enviaEmailOrdemColeta($nomeArq, $emailColeta);

        //}
        
        
    } else {
        echo "<script>alert('Favor selecionar as ocorrencias desejadas!')</script>";
    }
} else {
    echo "<script>alert('Favor informar e-mails para envio!')</script>";
}    