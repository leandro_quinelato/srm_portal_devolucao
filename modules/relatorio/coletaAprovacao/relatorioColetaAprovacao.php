<?php
include_once( "../../../includes/Dao/DaoSistema.class.php" );
include_once( "../../../includes/Dao/RelatorioDao.class.php" );
include_once( "../../../includes/Dao/OcorrenciaDao.class.php" );
include_once( "../../../includes/Status.class.php" );
include_once( "../../../includes/Dao/StatusDao.class.php" );
include_once( "../../../includes/Rota.class.php" );
include_once( "../../../includes/Dao/RotaDao.class.php" );

include_once( "../../../includes/DevMotivoResponsabilidade.class.php" );
include_once( "../../../includes/Dao/MotivoResponsabilidadeDao.class.php" );

include_once("include_novo/GlbCentroDistribuicao.class.php");

include_once("GlbUsuario.class.php");
include_once("integraSetSistemas.php");
$integra = new integraSetSistemas("conf.xml");
$ObjUsuario = unserialize($integra->getSistemaIntegra());

$RelatorioDao = new RelatorioDao();
$OcorrenciaDao = new OcorrenciaDao();
$resultFab = $OcorrenciaDao->industria();


/* FILTRO CEDIS */
$GlbCentroDistribuicao = new GlbCentroDistribuicao();
$GlbCentroDistribuicao = $OcorrenciaDao->filtroCentroDis();

/* FILTRO ROTA */
$RotaDao = new RotaDao();

 if($ObjUsuario->getUsuarioTipo() == "TRA"){
    $resultRota  = $RotaDao->consultaUsuRota($ObjUsuario->getUsuarioCodigo());
}else{
$resultRota = $RotaDao->listar();
}
/* FILTRO STATUS OCORRENCIA */
$Status = new Status();
$StatusDao = new StatusDao();
$Status = $StatusDao->consultar();

/* FILTRO MOTIVO */
$MotivoResponsabilidadeDao = new MotivoResponsabilidadeDao();
$DevMotivoResponsabilidade = new DevMotivoResponsabilidade();
$resultMtRespo = $MotivoResponsabilidadeDao->consultar($DevMotivoResponsabilidade);
?>
<!DOCTYPE html>
<html lang="en">
    <head>        
        <!-- meta section -->
        <title>Relatório - Protocolos Coletas Pendentes</title>
<?php include("../../../library/head.php"); ?>
        <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/libRelatorio.js"></script>
    </head>
    <body>
        <!-- set loading layer -->
        <div class="dev-page-loading preloader"></div>
        <!-- ./set loading layer -->

        <!-- page wrapper -->
        <div class="dev-page">

            <!-- page header -->    
<?php include("../../../library/topo.php"); ?>
            <!-- ./page header -->

            <!-- page container -->
            <div class="dev-page-container">

                <!-- page sidebar -->
<?php include("../../../library/menu.php"); ?>
                <!-- ./page sidebar -->

                <!-- page content -->
                <div class="dev-page-content">                    
                    <!-- page content container -->
                    <div class="container">
                        <!-- page title -->
                        <div class="page-title">
                            <h1>Relatório de Protocolos Coletas Pendentes</h1>

                            <ul class="breadcrumb">
                                <li><a href="#">Relatório</a></li>
                                <li>Protocolos Coletas Pendentes</li>
                            </ul>
                        </div>                        
                        <!-- ./page title -->



                        <!-- datatables plugin -->
                        <div class="wrapper wrapper-white">
                            <form id="FormColetaAprovacao">
                                <div class="row">
                                    <div class="col-md-2 col-sm-3 col-xs-3">
                                        <div class="form-group">
                                            <label>Protocolo</label>
                                            <input type="text" class="form-control" placeholder="Protocolo" id="codOcorrencia" name="codOcorrencia">
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-3 col-xs-3">
                                        <div class="form-group">
                                            <label>Cliente</label>
                                            <input type="text" class="form-control" placeholder="Cliente" id="cliente" name="cliente">
                                        </div>
                                    </div>

                                    <div class="col-md-2 col-sm-3 col-xs-3">
                                        <div class="form-group">
                                            <label>Início</label>                            
                                            <input type="text" class="form-control datepicker" id="dataInicial" name="dataInicial">
                                        </div> 
                                    </div>  
                                    <div class="col-md-2 col-sm-3 col-xs-3">
                                        <div class="form-group">
                                            <label>Fim</label>                            
                                            <input type="text" class="form-control datepicker" id="dataFinal" name="dataFinal">
                                        </div> 
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">                        
                                        <div class="form-group">
                                            <label>Rota</label>
                                            <select class="form-control selectpicker" id="rot_co_numero" name="rot_co_numero">
                                                <option value="TOD">Todas</option>
<?php
while (OCIFetchInto($resultRota, $rowRota, OCI_ASSOC)) {
    ?>
                                                    <option value="<?php echo $rowRota['ROT_CO_NUMERO']; ?>"  <?php
                                                    if ($rowCliente['ROT_CO_NUMERO'] == $rowRota['ROT_CO_NUMERO']) {
                                                        echo "selected='selected'";
                                                    }
                                                    ?>>
                                                    <?php echo $rowRota['ROT_CO_NUMERO']; ?> 
                                                    </option>
                                                            <?php
                                                            }
                                                            ?>
                                            </select>
                                        </div>                        
                                    </div> 
<!--                                    <div class="col-md-3 col-sm-6 col-xs-12">                        
                                        <div class="form-group">
                                            <label>Procedimento</label>
                                            <select class="form-control selectpicker" id="procedimento" name="procedimento">
                                                <option value="TOD">Todos</option>
                                                <option value="GER">Aguardando Aprovação Gerente</option>					
                                                <option value="POL">Transportadora</option>
                                            </select>
                                        </div>                        
                                    </div>                                -->
                                    <div class="col-md-3 col-sm-6 col-xs-12">                        
                                        <div class="form-group">
                                            <label>Motivo</label>
                                            <select class="form-control selectpicker" name="tresp" id="tresp">
                                                <option value="TOD">Todos</option>
<?php
while (OCIFetchInto($resultMtRespo, $rowMtRespo, OCI_ASSOC)) {
    ?>
                                                    <option value="<?php echo $rowMtRespo['TRP_CO_NUMERO'] ?>" > <?php echo $rowMtRespo['TRP_NO_DESCRICAO'] ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>                        
                                    </div>  
                                    <div class="col-md-2 col-sm-3 col-xs-3">
                                        <div class="form-group">
                                            <label>Lead time</label>
                                            <input type="text" class="form-control" placeholder="LeadTime" id="leadtime" name="leadtime">
                                        </div>
                                    </div>                                
                                    <div class="col-md-3 col-sm-6 col-xs-12">                        
                                        <div class="form-group">
                                            <label>Status</label>
                                            <select class="form-control selectpicker" id="status" name="status">
                                                <option value="TOD">Todos</option>
                                                <option value="0">Pendente de envio e ou Download</option>
                                                <option value="1">Ordem de coleta entregue ao transportador</option>
                                            </select>
                                        </div>                        
                                    </div>  
                                    
                                    <div class="col-md-3 col-sm-6 col-xs-6">                        
                                        <div class="form-group">
                                            <label>CD</label>
                                            <select class="form-control selectpicker" id="ced_co_numero" name="ced_co_numero">
                                                <option value="TOD">Todos</option>
                                                <?php
                                                foreach ($GlbCentroDistribuicao as $ObjCedis) {
                                                    ?>
                                                    <option value="<?php echo $ObjCedis->getCodigo(); ?>"><?php echo $ObjCedis->getNome(); ?></option>
                                                    <?php
                                                }
                                                ?>    
                                            </select>
                                        </div>                        
                                    </div> 
                                    
                                    <div class="col-md-3 col-sm-6 col-xs-12">                        
                                        <div class="form-group">
                                            <label>Divisão</label>
                                            <select class="form-control selectpicker" multiple id="codDivisao" name="codDivisao[]">
                                                <option value="A">Alimentar</option>
                                                <option value="O">Dist. Direta</option>
                                                <option value="F">Farma</option>
                                                <option value="H">Hospitalar</option>
                                            </select>
                                        </div>                        
                                    </div> 

                                </div>  
                                <div class="row">
                                    <div class="col-md-2">
                                        <a class="btn btn-primary" onclick="javascript: relatorioColetaAprovacao();">Pesquisar</a>
                                    </div>                                                                                  
                                </div>
                                <div class="form-group">
                                    <a type="button" class="btn btn-success btn-xs" onclick="javascript: relatorioColetaAprovacaoExport();"><i class="glyphicon glyphicon-download-alt"></i> Exportar</a>
                                </div>
                            </form>    
                            <div id="areaRelatorio">

                            </div>
                        </div>                        
                        <!-- ./datatables plugin -->


                        <!-- Copyright -->
<?php include("../../../library/copyright.php"); ?>
                        <!-- ./Copyright -->


                    </div>
                    <!-- ./page content container -->                                       
                </div>
                <!-- ./page content -->                                                
            </div>  
            <!-- ./page container -->

            <!-- right bar -->

            <!-- ./right bar -->            


            <!-- page search -->

            <!-- page search -->            
        </div>
        <!-- ./page wrapper -->


        <!-- javascripts -->
<?php include("../../../library/rodape.php"); ?>

        <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js"></script>
        <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/plugins/datatables/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/plugins/smartwizard/jquery.smartWizard.js"></script>
        <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/plugins/sortable/sortable.min.js"></script>


        <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/plugins/spectrum/spectrum.js"></script>
        <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/plugins/tags-input/jquery.tagsinput.min.js"></script>                

        <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/dev-settings.js"></script>
        <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/demo.js"></script>

        <!-- ./javascripts -->


    </body>
</html>