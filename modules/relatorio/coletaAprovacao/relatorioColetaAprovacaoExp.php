<?php 

date_default_timezone_set('Brazil/East');
header('Content-type: application/x-msdownload');
header('Content-Disposition: attachment; filename=relatorio_coletaAprovacao' .date("d-m-Y-His").'.xls');
header('Pragma: no-cache');
header('Expires: 0');

ini_set('max_execution_time', -1);

include_once( "../../../includes/Dao/DaoSistema.class.php" );
include_once( "../../../includes/Dao/RelatorioDao.class.php" );

include_once( "../../../includes/CalTipoOcorrencia.class.php" );
include_once( "../../../includes/Dao/TipoOcorrenciaDAO.class.php" );

include_once( "../../../includes/TipoDevolucao.class.php" );
include_once( "../../../includes/Dao/TipoDevolucaoDao.class.php" );

include_once( "../../../includes/Ocorrencia.class.php" );
include_once( "../../../includes/Dao/OcorrenciaDao.class.php" );

include_once( "../../../includes/DevNotaOrigem.class.php" );

include_once( "../../../includes/DevNotaOrigemItem.class.php" );
include_once( "../../../includes/Dao/DevNotaOrigemItemDao.class.php" );

include_once("DaoGlobal.class.php");
include_once("SistemaDao.class.php");
include_once("GlbSistema.class.php");
include_once("GlbSistemaPerfil.class.php");
include_once("GlbUsuario.class.php");
include_once("GlbColaborador.class.php");
include_once("GlbPessoa.class.php");
include_once("GlbEndereco.class.php");
include_once("GlbTelefone.class.php");
include_once("include_novo/GlbTransportadora.class.php");
include_once("include_novo/GlbContatoTransportador.class.php");
include_once("include_novo/ContatoTransportadorDao.class.php");
include_once("GlbProgramaRotina.class.php");
include_once("GlbModuloPrograma.class.php");
include_once("GlbSistemaModulo.class.php");
include_once("PerfilUsuarioDao.class.php");
include_once("PerfilPermissaoDao.class.php");
include_once("integraSetSistemas.php");
include_once("class.phpmailer.php");
include_once("class.smtp.php");

include_once("include_novo/Tradutor.class.php");

$Tradutor = new Tradutor();
$integra = new integraSetSistemas("conf.xml");
$ObjUsuario = unserialize($integra->getSistemaIntegra());

$Tradutor = new Tradutor();

//echo "<pre>";
//print_r($_REQUEST);
//echo "</pre>";
//die();

extract($_REQUEST);

//$dataInicial = '01/04/2016';
//$dataFinal = '20/04/2016';
$RelatorioDao = new RelatorioDao();

if(count($codDivisao) > 0 ){
    $auxDivisao = "";
    for ($i = 0; count($codDivisao) > $i; $i++) {
        if($auxDivisao != ""){
            $auxDivisao .= ", ";
        }
        $auxDivisao .= "'{$codDivisao[$i]}'";
    }
    $codDivisao = $auxDivisao;
}

$retorno = $RelatorioDao->relatorioColetaAprovacao($dataInicial, $dataFinal, $codOcorrencia, $cliente, $rot_co_numero,$procedimento, $tresp, $leadtime, $status, $ced_co_numero, $codDivisao, $ObjUsuario)

?>

<div class="table-responsive">
    <table class="table table-bordered table-striped table-sortable">
        <thead>
            <tr>
        <th>Protocolo</th>
        <th>Data</th>
        <th>Cadastrante</th>
        <th>Cod.</th>
        <th>Cliente</th>
        <th>Motivo Devolucao</th>
        <th>Rota</th>
        <th>Setor terc.</th>
        <th>Divisao</th>
        <th>CD</th>
        <th>Aprovacao</th>
        <th>Lead time</th>
        <th>Status</th>
        <th>Dt Envio /Download Ordem Coleta</th>
        </tr>
        </thead>                               
        <tbody>
<?php
		$Ocorrencia = new Ocorrencia();
		$OcorrenciaDao = new OcorrenciaDao();
		$NotasOrigem = new DevNotaOrigem();

        while ($dados = oci_fetch_object($retorno)) {
    ?>
            <tr> 	 	 	 	 	 	 	 		
                <td><?php echo $dados->DOC_CO_NUMERO;  ?></td>	
                <td><?php echo $dados->DOC_DT_CADASTRO;  ?></td>	
                <td><?php echo $dados->USU_NO_USERNAME;  ?></td>	
                <td><?php echo $dados->CLI_CO_NUMERO;  ?></td>	
                <td><?php echo $dados->CLI_NO_RAZAO_SOCIAL;  ?></td>	
                <td><?php echo $dados->TOC_NO_DESCRICAO;  ?></td>	
                <td><?php echo $dados->ROT_CO_NUMERO;  ?></td>	
                <td><?php echo $dados->CLI_NU_ROTA_TERCEIRIZADA;  ?></td>	
                <td><?php echo $dados->DEV_CO_DIVISAO;  ?></td>	
                <td><?php echo $dados->CED_NO_CENTRO;  ?></td>	
                <td><?php echo $dados->DOS_DT_ENTRADA;  ?></td>
                <td><?php echo $dados->LEAD_TIME;  ?></td>	
                <td><?php echo $dados->STATUS;  ?></td>	
                <td><?php echo $dados->DOC_DT_COLETA_ENVIADA;  ?></td>	
            </tr>
			<tr>
				<td colspan="17">
					<table border="0">
						<?
						$Ocorrencia->setCodigoDevolucao($dados->DOC_CO_NUMERO);
						$NotasOrigem = $OcorrenciaDao->consultaNotasDevolucao($Ocorrencia);
						foreach ($NotasOrigem as $NotaOrigem) {
						?>
						<tr>
							<td></td>
							<td>Serie: <?=$NotaOrigem->getSerie();?></td>
							<td>Nota: <?=$NotaOrigem->getNota();?></td>
							<td>Data: <?=$NotaOrigem->getData();?></td>
						</tr>
						<?
						}
						?>
					</table>				
				</td>
			</tr>				
<?php } ?>                                     
        </tbody>
    </table>
</div>

    <script language="JavaScript">
        $(function() {
          datatables.init();
        });
    </script>