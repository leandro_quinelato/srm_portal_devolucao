<?php
session_start();
include_once( "../../../includes/Dao/DaoSistema.class.php" );
include_once( "../../../includes/Dao/RelatorioDao.class.php" );

include_once("DaoGlobal.class.php");
include_once("SistemaDao.class.php");
include_once("GlbSistema.class.php");
include_once("GlbSistemaPerfil.class.php");
include_once("GlbUsuario.class.php");
include_once("GlbColaborador.class.php");
include_once("GlbPessoa.class.php");
include_once("GlbEndereco.class.php");
include_once("GlbTelefone.class.php");
include_once("GlbProgramaRotina.class.php");
include_once("GlbModuloPrograma.class.php");
include_once("GlbSistemaModulo.class.php");
include_once("PerfilUsuarioDao.class.php");
include_once("PerfilPermissaoDao.class.php");
include_once("integraSetSistemas.php");
include_once("class.phpmailer.php");
include_once("class.smtp.php");

include_once("include_novo/Tradutor.class.php");

$Tradutor = new Tradutor();
$integra = new integraSetSistemas("conf.xml");
$ObjUsuario = unserialize($integra->getSistemaIntegra());


//echo "<pre>";
//print_r($_REQUEST);
//echo "</pre>";

extract($_REQUEST);

//$dataInicial = '01/04/2016';
//$dataFinal = '20/04/2016';
$RelatorioDao = new RelatorioDao();
//die();
$retorno = $RelatorioDao->relatorioOcorrenciaReprovada($dataInicial, $dataFinal,$dataInicialRep ,$dataFinalRep , $protocolo, $codCliente, $codRede, $passoOcorrencia, $motivoReprova,$rot_co_numero, $ced_co_numero, $ObjUsuario);

?>


<div class="table-responsive">
    <table class="table table-bordered table-striped table-sortable">
        <thead>
            <tr>
                <th>Protocolo</th>
                <th>Dt Cadastro</th>
                <th>Rota</th>
                <th>Tipo</th>
                <th>NF Origem</th>
                <th>Divisão</th>
                <th>CD</th>
                <th>NF Devolução</th>
                <th>Valor</th>
                <th>Qtd Volumes</th>
                <th>Cod Cliente</th>
                <th>Cliente</th>
                <th>Motivo Reprova</th>
                <th>Dt Reprova</th>
            </tr>
        </thead>                               
        <tbody>
<?php
while ($dados = oci_fetch_object($retorno)) {
    ?>
                <tr>
                    <td><?php echo $dados->OCORRENCIA;  ?></td>
                    <td><?php echo $dados->DATA_CADASTRO;  ?></td>
                    <td><?php echo $dados->ROTA ;  ?></td>
                    <td><?php echo utf8_encode($dados->TID_CO_DESCRICAO) ;  ?></td>
                    <td><?php echo $dados->NOTA_ORIGEM ;  ?></td>
                    <td><?php echo $dados->RPR_IN_DIVISAO ;  ?></td>
                    <td><?php echo $dados->CED_NO_CENTRO ;  ?></td>
                    <td><?php echo $dados->NOTA_DEVOLUCAO ;  ?></td>
                    <td><?php echo $Tradutor->formatar($dados->VALOR, 'MOEDA') ;  ?></td>
                    <td><?php echo $dados->VOLUME ;  ?></td>
                    <td><?php echo $dados->CODIGO_CLIENTE ;  ?></td>
                    <td><?php echo $dados->CLIENTE ;  ?></td>
                    <td><?php echo $dados->MRE_NO_DESCRICAO ;  ?></td>
                    <td><?php echo $dados->LAP_DT_ACAO ;  ?></td>
                </tr>
<?php } ?>
        </tbody>
    </table>
</div>
<script language="JavaScript">
    $(function () {
        datatables.init();
    });
</script>