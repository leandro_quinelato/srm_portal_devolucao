<?php
include_once( "../../../includes/Dao/DaoSistema.class.php" );
include_once( "../../../includes/Dao/RelatorioDao.class.php" );

include_once("DaoGlobal.class.php");
include_once("SistemaDao.class.php");
include_once("GlbSistema.class.php");
include_once("GlbSistemaPerfil.class.php");
include_once("GlbUsuario.class.php");
include_once("GlbColaborador.class.php");
include_once("GlbPessoa.class.php");
include_once("GlbEndereco.class.php");
include_once("GlbTelefone.class.php");
include_once("include_novo/GlbTransportadora.class.php");
include_once("include_novo/GlbContatoTransportador.class.php");
include_once("include_novo/ContatoTransportadorDao.class.php");
include_once("GlbProgramaRotina.class.php");
include_once("GlbModuloPrograma.class.php");
include_once("GlbSistemaModulo.class.php");
include_once("PerfilUsuarioDao.class.php");
include_once("PerfilPermissaoDao.class.php");
include_once("integraSetSistemas.php");
include_once("class.phpmailer.php");
include_once("class.smtp.php");

include_once("include_novo/Tradutor.class.php");

$Tradutor = new Tradutor();
$integra = new integraSetSistemas("conf.xml");
$ObjUsuario = unserialize($integra->getSistemaIntegra());

//echo '<pre>';
//print_r($ObjUsuario);
//echo '</pre>';

extract($_REQUEST);

//print_r($_REQUEST);

//$dataInicial = '01/04/2016';
//$dataFinal = '20/04/2016';
$RelatorioDao = new RelatorioDao();

$retorno = $RelatorioDao->relatorioLiberacaoTransportador($dataInicial, $dataFinal, $ced_co_numero, $ObjUsuario);
?>

<div class="table-responsive">
    <table class="table table-bordered table-striped table-sortable">
        <thead>
            <tr>	 	 	 	 	 	 	 	 	 	
                <th>Dt. Polo Entrada/Saída 	</th> 
                <th>Protocolo 	</th> 
                <th>Nota Origem 	</th> 
                <th>Nota Devolução 	</th> 
                <th>Qnt. Volume 	</th> 
                <th>Cod. Cliente 	</th> 
                <th>Rota 	</th> 
                <th>CD 	</th> 
                <th>Tipo 	</th> 
                <th>Motivo da Devolução</th> 
            </tr>
        </thead>                               
        <tbody>
<?php
        while ($dados = oci_fetch_object($retorno)) {
?>
            <tr>

                <td><?php echo $dados->DOC_DT_POLO_ENTRADA;  ?></td> 
                <td><?php echo $dados->DOC_CO_NUMERO;  ?></td> 
                <td><?php echo $dados->NOTA_ORIGEM;  ?></td> 
                <td><?php echo $dados->NOTA_DEVOLUCAO;  ?></td> 
                <td><?php echo $dados->DOC_QT_POLO_SAIDA_PRODUTO;  ?></td> 
                <td><?php echo $dados->CLI_CO_NUMERO;  ?></td> 
                <td><?php echo $dados->ROT_CO_NUMERO;  ?></td> 
                <td><?php echo $dados->CED_NO_CENTRO;  ?></td> 
                <td><?php echo $dados->DOC_DT_POLO_ENTRADA;  ?></td> 
                <td><?php echo $dados->TOC_NO_DESCRICAO;  ?></td> 
            </tr>
<?php
        }
?>
                                                                           
        </tbody>
    </table>
</div>