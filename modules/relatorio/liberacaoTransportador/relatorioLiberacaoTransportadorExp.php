<?php
include_once( "../../../includes/mpdf60/mpdf.php");
include_once( "../../../includes/Dao/DaoSistema.class.php" );
include_once( "../../../includes/Dao/RelatorioDao.class.php" );

include_once("DaoGlobal.class.php");
include_once("SistemaDao.class.php");
include_once("GlbSistema.class.php");
include_once("GlbSistemaPerfil.class.php");
include_once("GlbUsuario.class.php");
include_once("GlbColaborador.class.php");
include_once("GlbPessoa.class.php");
include_once("GlbEndereco.class.php");
include_once("GlbTelefone.class.php");
include_once("include_novo/GlbTransportadora.class.php");
include_once("include_novo/GlbContatoTransportador.class.php");
include_once("include_novo/ContatoTransportadorDao.class.php");
include_once("GlbProgramaRotina.class.php");
include_once("GlbModuloPrograma.class.php");
include_once("GlbSistemaModulo.class.php");
include_once("PerfilUsuarioDao.class.php");
include_once("PerfilPermissaoDao.class.php");
include_once("integraSetSistemas.php");
include_once("class.phpmailer.php");
include_once("class.smtp.php");

include_once("include_novo/Tradutor.class.php");

$Tradutor = new Tradutor();
$integra = new integraSetSistemas("conf.xml");
$ObjUsuario = unserialize($integra->getSistemaIntegra());

extract($_REQUEST);

//print_r($_REQUEST);

//$dataInicial = '01/04/2016';
//$dataFinal = '20/04/2016';
$RelatorioDao = new RelatorioDao();

$retorno = $RelatorioDao->relatorioLiberacaoTransportador($dataInicial, $dataFinal, $ced_co_numero, $ObjUsuario);

        class PDF extends mPDF {

            //Page header
            function Header() {
                $logoPath = $_SERVER['DOCUMENT_ROOT'] . '/img/logoServimed.jpg'; //testes externo
                //Logo
                $this->Image($logoPath, 10, 8, 33);
                //Arial bold 15
                $this->SetFont('Arial', 'B', 12);
                //Move to the right
                $this->Cell(80);
                //Title
                $this->Cell(100, 10, 'PLANILHA DEVOLUÇÃO ', 0, 0, 'C');
                //Line break
                $this->Ln(20);
            }

            function Footer() {
                //Position at 1.5 cm from bottom
                $this->SetY(-15);
                //Arial italic 8
                $this->SetFont('Arial', 'I', 8);
                //Page number
                $this->Cell(0, 10, 'Servimed' . " " . date("Y"), 0, 0, 'C');
            }

        }

        $pdf = new PDF();
        $pdf->AddPage("L");
        $pdf->SetFont('Arial', 'B', 8);
        $pdf->Cell(30, 5, 'Data de Emissão:', 0);
        $pdf->Cell(100, 5, date('d/m/Y'), 0);
        $pdf->Ln(8);
        $pdf->SetFont('Arial', 'B', 8);
        $pdf->Cell(20, 5, 'Dt. Ent/Saí', 1);
        $pdf->Cell(30, 5, 'Protocolo', 1);
        $pdf->Cell(20, 5, 'Nota Origem', 1);
        $pdf->Cell(30, 5, 'Nota Devolução', 1);
        $pdf->Cell(20, 5, 'Qnt. Volume', 1);
        $pdf->Cell(20, 5, 'Cod. Cliente', 1);
        $pdf->Cell(20, 5, 'Cod. CD', 1);
        $pdf->Cell(12, 5, 'Rota', 1);
        $pdf->Cell(50, 5, 'Tipo', 1);
        $pdf->Cell(50, 5, 'Motivo da Devolução', 1);
        $pdf->Ln();
        $pdf->SetFont('Arial', '', 8);
        while (OCIFetchInto($retorno, $row, OCI_ASSOC)) {
            $contNotas++;
            $contVolume += $row['DOC_QT_POLO_SAIDA_PRODUTO'];
            $pdf->Cell(20, 5, $row['DOC_DT_POLO_ENTRADA'], 1);
            $pdf->Cell(30, 5, $row['DOC_CO_NUMERO'], 1);
            $pdf->Cell(20, 5, $row['NOTA_ORIGEM'], 1);
            $pdf->Cell(30, 5, $row['NOTA_DEVOLUCAO'], 1);
            $pdf->Cell(20, 5, $row['DOC_QT_POLO_SAIDA_PRODUTO'], 1);
            $pdf->Cell(20, 5, $row['CLI_CO_NUMERO'], 1);
            $pdf->Cell(20, 5, $row['CED_NO_CENTRO'], 1);
            $pdf->Cell(12, 5, $row['ROT_CO_NUMERO'], 1);
            $tipo = $row['TID_CO_DESCRICAO'];
            $pdf->Cell(50, 5, $tipo, 1);
            $pdf->Cell(50, 5, $row['TOC_NO_DESCRICAO'], 1);


            $pdf->Ln();
        }

        $pdf->Ln();
        $pdf->Ln();
        $pdf->Ln();
        $pdf->SetFont('Arial', 'B', 8);
        $pdf->cell(30, 5, "Qtd. Total de Notas: ", 0, 0, "C");
        $pdf->cell(30, 5, $contNotas, 0, 0, "L");
        $pdf->cell(30, 5, "Qtd. Total de Volumes: ", 0, 0, "C");
        $pdf->cell(30, 5, $contVolume, 0, 0, "L");
        $pdf->Ln();
        $pdf->Ln();
        $pdf->Ln();
        $pdf->Ln();
        $pdf->cell(110, 5, "__________________________________", 0, 0, "R");
        $pdf->cell(70, 5, "__________________________________", 0, 0, "R");
        $pdf->cell(70, 5, "__________________________________", 0, 0, "R");
        $pdf->Ln();
        $pdf->cell(110, 5, "ASSISTENTE SERVIMED", 0, 0, "R");
        $pdf->cell(70, 5, "ASS. MOTORISTA", 0, 0, "R");
        $pdf->cell(70, 5, "ASS. RECEBEDOR CD", 0, 0, "R");
        $pdf->Ln();
        $pdf->Ln();
        $pdf->cell(110, 5, "___________________", 0, 0, "R");
        $pdf->cell(70, 5, "___________________", 0, 0, "R");
        $pdf->cell(70, 5, "___________________", 0, 0, "R");
        $pdf->Ln();
        $pdf->cell(110, 5, "DATA ENVIO", 0, 0, "R");
        $pdf->cell(70, 5, "PLACA VEICULO", 0, 0, "R");
        $pdf->cell(70, 5, "DATA RECEBIMENTO", 0, 0, "R");

        // $pdf->cell(30,5,"Texto alinhado a direita",0,0,"D");
//	for($i=0; count($box_dev)>$i; $i++){
//		$pdf->Cell(30,5,$doc_co_numero[$box_dev[$i]],1);
//		$pdf->Cell(20,5,$nota_origem[$box_dev[$i]],1);
//		$pdf->Cell(30,5,$nota_devolucao[$box_dev[$i]],1);
//		$pdf->Cell(20,5,$doc_qt_polo_saida_produto[$box_dev[$i]],1);
//		$pdf->Cell(20,5,$cli_co_numero[$box_dev[$i]],1);
//		$pdf->Cell(10,5,$rot_co_numero[$box_dev[$i]],1);
//		if($doc_in_tipo[$box_dev[$i]]==1){ 
//			$tipo = 'Parcial'; 
//		}else{ 
//			$tipo = 'Integral'; 
//		}
//		$pdf->Cell(20,5,$tipo,1);
//		$pdf->Cell(40,5,$toc_no_descricao[$box_dev[$i]],1);
//		$pdf->Ln();			
//	}
        $pdf->Output("PlanilhaDevolucao.pdf", "D");
