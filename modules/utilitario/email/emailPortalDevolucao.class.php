<?php

session_start();

ini_set('display_errors', 1);
//include_once( "../ordemColeta/ordemColetaDevolucaoMain.php" );
include_once( "../../../includes/Dao/DaoSistema.class.php" );
include_once( "../../../includes/CalDepartamento.class.php" );
include_once( "../../../includes/CalTipoOcorrencia.class.php" );
include_once( "../../../includes/Dao/TipoOcorrenciaDAO.class.php" );
include_once( "../../../includes/Ocorrencia.class.php" );
include_once( "../../../includes/Dao/OcorrenciaDao.class.php" );
include_once( "../../../includes/TipoDevolucao.class.php" );
include_once( "../../../includes/Dao/TipoDevolucaoDao.class.php" );

include_once( "../../../includes/DevNotaOrigemItem.class.php" );
include_once( "../../../includes/Dao/DevNotaOrigemItemDao.class.php" );

include_once( "../../../includes/DevParametros.class.php" );
include_once( "../../../includes/Dao/ParametroDao.class.php" );

include_once( "../../../includes/Rota.class.php" );
include_once( "../../../includes/Dao/RotaDao.class.php" );

include_once( "../../../includes/OcorrenciaPasso.class.php" );
include_once( "../../../includes/Dao/OcorrenciaPassoDao.class.php" );

include_once( "../../../includes/OcorrenciaStatus.class.php" );
include_once( "../../../includes/Dao/OcorrenciaStatusDao.class.php" );

include_once( "../../../includes/Status.class.php" );
include_once( "../../../includes/Dao/StatusDao.class.php" );

include_once("DaoGlobal.class.php");
include_once("SistemaDao.class.php");
include_once("GlbSistema.class.php");
include_once("GlbSistemaPerfil.class.php");
include_once("GlbUsuario.class.php");
include_once("GlbColaborador.class.php");
include_once("GlbPessoa.class.php");
include_once("GlbEndereco.class.php");
include_once("GlbTelefone.class.php");
include_once("include_novo/GlbTransportadora.class.php");
include_once("include_novo/GlbContatoTransportador.class.php");
include_once("include_novo/ContatoTransportadorDao.class.php");
include_once("GlbProgramaRotina.class.php");
include_once("GlbModuloPrograma.class.php");
include_once("GlbSistemaModulo.class.php");
include_once("PerfilUsuarioDao.class.php");
include_once("PerfilPermissaoDao.class.php");
include_once("integraSetSistemas.php");
include_once("class.phpmailer.php");
include_once("class.smtp.php");

include_once("include_novo/Tradutor.class.php");

/**
 * Description of emailPortalDevolucao
 *
 * @author geovanni.info
 */
class emailPortalDevolucao {

    public function preencherMailer() {

        $phpMailer = new PHPMailer();

        $phpMailer->IsSMTP(); // seta que o envio � por SMTP
        $phpMailer->Host = "192.168.2.50"; // SMTP server
        $phpMailer->SMTPAuth = false; // Habilita a autentica��o

        $phpMailer->Port = 25; // seta a porta de conex�o
        $phpMailer->Username = "devolucao@servimed.com.br"; // usuario de conex�o com o SMTP
        $phpMailer->Password = ""; // senha do usuario conex�o com o SMTP
        //Dados Remetente
        $phpMailer->SetFrom('devolucao@servimed.com.br', 'devolucao@servimed.com.br');
        $phpMailer->AddReplyTo("devolucao@servimed.com.br"); //responder para 


        return $phpMailer;
    }

    public function enviaEmailCadastro(Ocorrencia $Ocorrencia) {
        
        $Ocorrencia->setTipoOcorrencia('T');
        $phpMailer = emailPortalDevolucao::preencherMailer();
        
        $OcorrenciaDao = new OcorrenciaDao();
        
        $StatusDao = new StatusDao();
        $Status = new Status();
        $Status->setCodigo(1); // DEFININDO O STATUS PARA APRESENTAR NO E-MAIL
        $Status = $StatusDao->consultarStatus($Status);
        $Status = $Status[0];
//        
//        echo 'alouu?<br>    <div class="row">
//        <div class="col-md-3">
//            <a class="btn btn-primary" onclick="javascript:FinalizaCadastroOcorrencia();">Cadastrar Ocorrência</a>
//        </div>
//    </div>	';
        $retorno = $OcorrenciaDao->consultaOcorrencia($Ocorrencia);
        $dados = oci_fetch_object($retorno);

        $NotasOrigem = new DevNotaOrigem();
        $NotasOrigem = $OcorrenciaDao->consultaNotasDevolucao($Ocorrencia);

        /** DADOS DO E-MAIL **/
        
        $assunto = "Cadastro Devolução realizado: {$dados->DOC_CO_NUMERO}";
        $texto = "Servimed - Portal de Devolução";
        $texto .="\r\n";
        $texto .="Foi gerado no Poral de Devolução uma Ocorrência com sucesso!";
        $texto .="Abaixo detalhes da Ocorrência:";
        $texto .="\r\n";
        $texto .="Protocolo : {$dados->DOC_CO_NUMERO}";
        $texto .="\r\n";

        foreach ($NotasOrigem as $NotaOrigem) {
            $texto .="Nota de venda : {$NotaOrigem->getSerie()} {$NotaOrigem->getNota()} - {$NotaOrigem->getData()}";
            $texto .="\r\n";
        }
        $texto .="Motivo Devolução : {$dados->TOC_NO_DESCRICAO}";
        $texto .="\r\n";
        $texto .="Status da Devolução : {$Status->getDescricao()}";
        $texto .="\r\n";

        $texto .="\r\n";
        $texto .="\r\n";
        $texto .="\r\n";
        $texto .="Não responda email automatico - Portal de Devolução";


        $contatoscliente = $OcorrenciaDao->ContatosClienteDevolucao($dados->CLI_CO_NUMERO);
        $qtdContato = 0;
        while ($contatocliente = oci_fetch_object($contatoscliente)) {
            if($qtdContato == 0 ){
//                echo "<br>principal" . $contatocliente->CON_TX_EMAIL . "<br>"; 
//                $phpMailer->AddAddress($contatocliente->CON_TX_EMAIL, $contatocliente->CON_TX_EMAIL);
                $phpMailer->AddAddress('geovanni.info@servimed.com.br', 'geovanni.info@servimed.com.br');
                $phpMailer->AddBCC('wilianfernando@servimed.com.br', 'wilianfernando@servimed.com.br');
            }else{ 
//                echo "<br>Copia" . $contatocliente->CON_TX_EMAIL . "<br>"; 
                $phpMailer->AddBCC('geovanni.info@servimed.com.br', 'geovanni.info@servimed.com.br');
//                $phpMailer->AddBCC($contatocliente->CON_TX_EMAIL, $contatocliente->CON_TX_EMAIL);
            }
            
            $qtdContato++;
        }
        
        $phpMailer->Subject = utf8_decode($assunto); //assunto
        $phpMailer->Body = utf8_decode($texto);
//        
//        echo "<pre>";
//        print_r($texto);
//        echo "</pre>";
        
        if ($phpMailer->Send()) {
            echo "<script>alert('E-mail de Cadastro enviado com sucesso para ocorrência:{$Ocorrencia->getCodigoDevolucao()}');</script>";
        } else {
            echo "<script>alert('{$Ocorrencia->getCodigoDevolucao()} - Falha ao enviar E-mail de cadastro');</script>";
        }
        return true;
    }
    
    
    public function enviaEmailAprovadoComEspelho(Ocorrencia $Ocorrencia) {
        //$Ocorrencia->setTipoOcorrencia('T');
        $ObjOcorrencia = new Ocorrencia();
        $ObjOcorrencia->setCodigoDevolucao($Ocorrencia->getCodigoDevolucao());
        
        $phpMailer = emailPortalDevolucao::preencherMailer();
        
        $OcorrenciaDao = new OcorrenciaDao();
        
        $StatusDao = new StatusDao();
        $Status = new Status();
        $Status->setCodigo(3); // DEFININDO O STATUS PARA APRESENTAR NO E-MAIL
        $Status = $StatusDao->consultarStatus($Status);
        $Status = $Status[0];
        
//        echo 'alouu?<br>    <div class="row">
//        <div class="col-md-3">
//            <a class="btn btn-primary" onclick="javascript:FinalizaCadastroOcorrencia();">Cadastrar Ocorrência</a>
//        </div>
//    </div>	';
        $ObjOcorrencia->setTipoOcorrencia("");
        $retorno = $OcorrenciaDao->consultaOcorrencia($ObjOcorrencia);
        $dados = oci_fetch_object($retorno);
//        echo "GEEEEEEEEEEEEEEEEEEEEE COM 3";//DIE();

        $NotasOrigem = new DevNotaOrigem();
        $NotasOrigem = $OcorrenciaDao->consultaNotasDevolucao($ObjOcorrencia);

        /** DADOS DO E-MAIL **/
        
        $assunto = "Ocorrência de Devolução Aprovada: {$dados->DOC_CO_NUMERO}";
        $texto = "Servimed - Portal de Devolução";
        $texto .="\r\n";
        $texto .="A ocorrencia: {$dados->DOC_CO_NUMERO} foi Aprovada com sucesso. Para dar continuidade ao processo favor acessar o portal pelo link: http://portaldevolucao.servimed.com.br e informe a Nota de Devolução." ;
        $texto .="\r\n";
        $texto .="\r\n";
        $texto .="Segue em Anexo um Modelo de Espelho da nota para que possa auxiliar na geração da mesma.Em caso de duvidas contate ...";
        $texto .="\r\n";
        $texto .="Abaixo detalhes da Ocorrência:";
        $texto .="\r\n";
        $texto .="Protocolo : {$dados->DOC_CO_NUMERO}";
        $texto .="\r\n";

        foreach ($NotasOrigem as $NotaOrigem) {
            $texto .="Nota de venda : {$NotaOrigem->getSerie()} {$NotaOrigem->getNota()} - {$NotaOrigem->getData()}";
            $texto .="\r\n";
        }
        $texto .="Motivo Devolução : {$dados->TOC_NO_DESCRICAO}";
        $texto .="\r\n";
        $texto .="Status da Devolução : {$Status->getDescricao()}";
        $texto .="\r\n";

        $texto .="\r\n";
        $texto .="\r\n";
        $texto .="\r\n";
        $texto .="Não responda email automatico - Portal de Devolução";


        $contatoscliente = $OcorrenciaDao->ContatosClienteDevolucao($dados->CLI_CO_NUMERO);
        $qtdContato = 0;
        while ($contatocliente = oci_fetch_object($contatoscliente)) {
            if($qtdContato == 0 ){
                echo "<br>principal" . $contatocliente->CON_TX_EMAIL . "<br>"; 
//                $phpMailer->AddAddress($contatocliente->CON_TX_EMAIL, $contatocliente->CON_TX_EMAIL);
                $phpMailer->AddAddress('geovanni.info@servimed.com.br', 'geovanni.info@servimed.com.br');
                                $phpMailer->AddBCC('wilianfernando@servimed.com.br', 'wilianfernando@servimed.com.br');
            }else{ 
                echo "<br>Copia" . $contatocliente->CON_TX_EMAIL . "<br>"; 
                $phpMailer->AddBCC('geovanni.info@servimed.com.br', 'geovanni.info@servimed.com.br');
//                $phpMailer->AddBCC($contatocliente->CON_TX_EMAIL, $contatocliente->CON_TX_EMAIL);
            }
            
            $qtdContato++;
        }
        
        /*gerar o espelho enviar*/   
//        $phpMailer->AddAttachment($dir_logErros); // anexo
        
        
        $phpMailer->Subject = utf8_decode($assunto); //assunto
        $phpMailer->Body = utf8_decode($texto);
        
//        echo "<pre>";
//        print_r($texto);
//        echo "</pre>";
//        
        if ($phpMailer->Send()) {
            echo "<script>alert('E-mail de Aprovacao enviado com sucesso: {$Ocorrencia->getCodigoDevolucao()}');</script>";
        } else {
            echo "<script>alert('{$Ocorrencia->getCodigoDevolucao()} - Falha ao enviar E-mail de Aprovacao');</script>";
        }
        return true;
    }
    
    
    
    public function enviaEmailAprovadoSemEspelho(Ocorrencia $Ocorrencia) {
//                echo "GEEEEEEEEEEEEEEEEEEEEE SEM ";
//                                die();
//        $Ocorrencia->setTipoOcorrencia('T');
        $phpMailer = emailPortalDevolucao::preencherMailer();
        
        $ObjOcorrencia = new Ocorrencia();
        $ObjOcorrencia->setCodigoDevolucao($Ocorrencia->getCodigoDevolucao());
        
        $OcorrenciaDao = new OcorrenciaDao();
        
        $StatusDao = new StatusDao();
        $Status = new Status();
        // DEFININDO O STATUS PARA APRESENTAR NO E-MAIL
        $Status->setCodigo(7); // AGUARD CONFERENCIA FISICO x NF
        $Status = $StatusDao->consultarStatus($Status);
        $Status = $Status[0];
        
//        echo 'alouu?<br>    <div class="row">
//        <div class="col-md-3">
//            <a class="btn btn-primary" onclick="javascript:FinalizaCadastroOcorrencia();">Cadastrar Ocorrência</a>
//        </div>
//    </div>	';
        $ObjOcorrencia->setTipoOcorrencia("");
        $retorno = $OcorrenciaDao->consultaOcorrencia($ObjOcorrencia);
        $dados = oci_fetch_object($retorno);

        $NotasOrigem = new DevNotaOrigem();
        $NotasOrigem = $OcorrenciaDao->consultaNotasDevolucao($ObjOcorrencia);

        /** DADOS DO E-MAIL **/
        
        $assunto = "Ocorrência de Devolução Aprovada: {$dados->DOC_CO_NUMERO}";
        $texto = "Servimed - Portal de Devolução";
        $texto .="\r\n";
        $texto .="A ocorrencia: {$dados->DOC_CO_NUMERO} foi Aprovada com sucesso. Para acompanhar ao processo favor acessar o portal pelo link: http://portaldevolucao.servimed.com.br e informe o numero da Ocorrencia." ;
        $texto .="\r\n";
        $texto .="\r\n";
        $texto .="Abaixo detalhes da Ocorrência:";
        $texto .="\r\n";
        $texto .="Protocolo : {$dados->DOC_CO_NUMERO}";
        $texto .="\r\n";

        foreach ($NotasOrigem as $NotaOrigem) {
            $texto .="Nota de venda : {$NotaOrigem->getSerie()} {$NotaOrigem->getNota()} - {$NotaOrigem->getData()}";
            $texto .="\r\n";
        }
        $texto .="Motivo Devolução : {$dados->TOC_NO_DESCRICAO}";
        $texto .="\r\n";
        $texto .="Status da Devolução : {$Status->getDescricao()}";
        $texto .="\r\n";

        $texto .="\r\n";
        $texto .="\r\n";
        $texto .="\r\n";
        $texto .="Não responda email automatico - Portal de Devolução";


        $contatoscliente = $OcorrenciaDao->ContatosClienteDevolucao($dados->CLI_CO_NUMERO);
        $qtdContato = 0;
        while ($contatocliente = oci_fetch_object($contatoscliente)) {
            if($qtdContato == 0 ){
                echo "<br>principal" . $contatocliente->CON_TX_EMAIL . "<br>"; 
//                $phpMailer->AddAddress($contatocliente->CON_TX_EMAIL, $contatocliente->CON_TX_EMAIL);
                $phpMailer->AddAddress('geovanni.info@servimed.com.br', 'geovanni.info@servimed.com.br');
                                $phpMailer->AddBCC('wilianfernando@servimed.com.br', 'wilianfernando@servimed.com.br');
            }else{ 
                echo "<br>Copia" . $contatocliente->CON_TX_EMAIL . "<br>"; 
                $phpMailer->AddBCC('geovanni.info@servimed.com.br', 'geovanni.info@servimed.com.br');
//                $phpMailer->AddBCC($contatocliente->CON_TX_EMAIL, $contatocliente->CON_TX_EMAIL);
            }
            
            $qtdContato++;
        }
 
        $phpMailer->Subject = utf8_decode($assunto); //assunto
        $phpMailer->Body = utf8_decode($texto);
      
        if ($phpMailer->Send()) {
            echo "<script>alert('E-mail de Aprovacao enviado com sucesso: {$Ocorrencia->getCodigoDevolucao()}');</script>";
        } else {
            echo "<script>alert('{$Ocorrencia->getCodigoDevolucao()} - Falha ao enviar E-mail de Aprovacao');</script>";
        }
        return true;
    }
    
    
    public function enviaEmailOcorrenciaFinalizada(Ocorrencia $Ocorrencia) {

//                                die();
//        $Ocorrencia->setTipoOcorrencia('T');
        $phpMailer = emailPortalDevolucao::preencherMailer();
        
        $ObjOcorrencia = new Ocorrencia();
        $ObjOcorrencia->setCodigoDevolucao($Ocorrencia->getCodigoDevolucao());
        
        $OcorrenciaDao = new OcorrenciaDao();
        
        $StatusDao = new StatusDao();
        $Status = new Status();
        // DEFININDO O STATUS PARA APRESENTAR NO E-MAIL
        $Status->setCodigo(10); // AGUARD CONFERENCIA FISICO x NF
        $Status = $StatusDao->consultarStatus($Status);
        $Status = $Status[0];
        

        $ObjOcorrencia->setTipoOcorrencia("");
        $retorno = $OcorrenciaDao->consultaOcorrencia($ObjOcorrencia);
        $dados = oci_fetch_object($retorno);

        $NotasOrigem = new DevNotaOrigem();
        $NotasOrigem = $OcorrenciaDao->consultaNotasDevolucao($ObjOcorrencia);

        /** DADOS DO E-MAIL **/
        
        $assunto = "Ocorrência de Devolução Finalizada: {$dados->DOC_CO_NUMERO}";
        $texto = "Servimed - Portal de Devolução";
        $texto .="\r\n";
        $texto .="A ocorrencia: {$dados->DOC_CO_NUMERO} foi Finalizada com sucesso. Para acompanhar ao processo favor acessar o portal pelo link: http://portaldevolucao.servimed.com.br e informe o numero da Ocorrencia." ;
        $texto .="\r\n";
        $texto .="\r\n";
        $texto .="Abaixo detalhes da Ocorrência:";
        $texto .="\r\n";
        $texto .="Protocolo : {$dados->DOC_CO_NUMERO}";
        $texto .="\r\n";

        foreach ($NotasOrigem as $NotaOrigem) {
            $texto .="Nota de venda : {$NotaOrigem->getSerie()} {$NotaOrigem->getNota()} - {$NotaOrigem->getData()}";
            $texto .="\r\n";
        }
        $texto .="Motivo Devolução : {$dados->TOC_NO_DESCRICAO}";
        $texto .="\r\n";
        $texto .="Status da Devolução : {$Status->getDescricao()}";
        $texto .="\r\n";

        $texto .="\r\n";
        $texto .="\r\n";
        $texto .="\r\n";
        $texto .="Não responda email automatico - Portal de Devolução";


        $contatoscliente = $OcorrenciaDao->ContatosClienteDevolucao($dados->CLI_CO_NUMERO);
        $qtdContato = 0;
        while ($contatocliente = oci_fetch_object($contatoscliente)) {
            if($qtdContato == 0 ){
//                echo "<br>principal" . $contatocliente->CON_TX_EMAIL . "<br>"; 
//                $phpMailer->AddAddress($contatocliente->CON_TX_EMAIL, $contatocliente->CON_TX_EMAIL);
                $phpMailer->AddAddress('geovanni.info@servimed.com.br', 'geovanni.info@servimed.com.br');
                                $phpMailer->AddBCC('wilianfernando@servimed.com.br', 'wilianfernando@servimed.com.br');
            }else{ 
//                echo "<br>Copia" . $contatocliente->CON_TX_EMAIL . "<br>"; 
                $phpMailer->AddBCC('geovanni.info@servimed.com.br', 'geovanni.info@servimed.com.br');
//                $phpMailer->AddBCC($contatocliente->CON_TX_EMAIL, $contatocliente->CON_TX_EMAIL);
            }
            
            $qtdContato++;
        }
 
        $phpMailer->Subject = utf8_decode($assunto); //assunto
        $phpMailer->Body = utf8_decode($texto);
      
        if ($phpMailer->Send()) {
            echo "<script>alert('E-mail de Finalizado enviado com sucesso: {$Ocorrencia->getCodigoDevolucao()}');</script>";
        } else {
            echo "<script>alert('{$Ocorrencia->getCodigoDevolucao()} - Falha ao enviar E-mail de Finalizado');</script>";
        }
        return true;
    }
    
    
    public function enviaEmailOrdemColeta($nomeArq, $emailColeta) {
        
        $phpMailer = emailPortalDevolucao::preencherMailer();
        $phpMailer->CharSet = "UTF-8";
		
		$assunto = "Coleta Devolução Protocolos";				
		$texto  ="Servimed - Coleta Devolução";
		$texto  .="\r\n";
		//$texto  .="Protocolo : {$Ocorrencia->getCodigoDevolucao()}";
		$texto  .="\r\n" ;						
		$texto  .="\r\n";
		$texto  .="\r\n";
		$texto  .="\r\n";
		$texto  .="Nao responda email automatico - Sistema Devolucao";			
			

		//$phpMailer->AddAddress($emailColeta,$emailColeta);
                $emailColeta = explode(";", $emailColeta);
                for ($j = 0; count($emailColeta) > $j; $j++) {
                    $phpMailer->AddBCC($emailColeta[$j], $emailColeta[$j]);
                }
                
		$phpMailer->Subject = $assunto; //assunto
		$phpMailer->Body = $texto;
//		DocumentoColeta($Ocorrencia->getCodigoDevolucao(), true); 			
		$phpMailer->AddAttachment("../../utilitario/arquivos/ordemColeta/arq/ordemColeta_{$nomeArq}.pdf"); // anexo
			
		if( $phpMailer->Send() ){			
			//$OcorrenciaDevDao->emailenviado($_REQUEST['doc_co_numero']);
                    echo "<script>alert('E-mail de coleta enviado com sucesso - {$nomeArq}');</script>";
		}else{
                    echo "<script>alert('{$nomeArq} - E-mail de Coleta ocorreu um erro ao ser enviado');</script>";
		}
        
    }
    
	
    public function enviaEmailReentrega(Ocorrencia $Ocorrencia, OcorrenciaPasso $OcorrenciaPasso, $emailColeta) {
        
        $Ocorrencia->setTipoOcorrencia("");
		
		$OcorrenciaDao = new OcorrenciaDao();
		
        $retorno = $OcorrenciaDao->consultaOcorrencia($Ocorrencia);
        $dados = oci_fetch_object($retorno);

        $NotasOrigem = new DevNotaOrigem();
        $NotasOrigem = $OcorrenciaDao->consultaNotasDevolucao($Ocorrencia);
		
		
        $phpMailer = emailPortalDevolucao::preencherMailer();
		$phpMailer->CharSet = "UTF-8";
		//$phpMailer->IsHTML(true);
        
		$assunto = "SOLICITAÇÃO DE REENTREGA";				
		$texto  ="FAVOR PROCEDER COM A REENTREGA NO CLIENTE";
		$texto  .="\r\n";
		$texto  .="\r\n";
		$texto  .="PROTOCOLO DE DEVOLUÇÃO - {$Ocorrencia->getCodigoDevolucao()}";
		$texto  .="\r\n" ;
		$texto  .="\r\n" ;
		$texto  .="CLIENTE - {$dados->CLI_CO_NUMERO}  {$dados->CLI_NO_RAZAO_SOCIAL}";		
		$texto  .="\r\n" ;
		$texto  .="\r\n" ;
        foreach ($NotasOrigem as $NotaOrigem) {
            $texto .="NF - {$NotaOrigem->getSerie()} {$NotaOrigem->getNota()} - {$NotaOrigem->getData()}   ROTA -     VALOR TOTAL R$ {$NotaOrigem->getValorTotDevolucao()}";
            $texto .="\r\n";
        }
		$texto  .="\r\n" ;
		$texto  .="\r\n" ;
		$texto  .="DESCRIÇÃO: {$OcorrenciaPasso->getObservacao()}" ;
		$texto  .="\r\n";
		$texto  .="\r\n";
		$texto  .="\r\n";
		$texto  .="ATENCIOSAMENTE,";
		$texto  .="\r\n";
		$texto  .="GERENTE DE CONTAS";
		$texto  .="\r\n";
		$texto  .="\r\n";
		$texto  .="Nao responda email automatico - Sistema Devolucao";			
			
			
		$phpMailer->AddAddress('wilianfernando@servimed.com.br', 'wilianfernando@servimed.com.br');
		$listaEmails = explode(";", $emailColeta);
		foreach($listaEmails as $email){
			if($email != ""){
				$phpMailer->AddBCC(trim($email),trim($email));
			}
		}
		
		$phpMailer->Subject = $assunto; //assunto
		$phpMailer->Body = $texto;
//		DocumentoColeta($Ocorrencia->getCodigoDevolucao(), true); 			
			
		if( $phpMailer->Send() ){			
			//$OcorrenciaDevDao->emailenviado($_REQUEST['doc_co_numero']);
			echo "<script>alert('E-mail de reentrega enviado com sucesso para ocorrência:{$Ocorrencia->getCodigoDevolucao()}');</script>";
		}else{
			echo "<script>alert('{$Ocorrencia->getCodigoDevolucao()} - E-mail de reentrega ocorreu um erro ao ser enviado');</script>";
		}
        
    }	
	
	
    public function enviaEmailPendenciasUsuario($codUsuario, $emailUsuario, $codPasso, $dataInicial = null, $dataFinal = null) {
		
		$OcorrenciaDao = new OcorrenciaDao();
        $retorno = $OcorrenciaDao->consultaOcorrenciaPendentes($dataInicial = null, $dataFinal = null, $codUsuario, $codPasso);

        $phpMailer = emailPortalDevolucao::preencherMailer();
		$phpMailer->CharSet = "UTF-8";
		$phpMailer->IsHTML(true);
		
		$assunto = "Lembrete do sistema de Devolução.";				
		
		$texto  ="<html><body>";
		$texto  .="Já conectou no sistema de Devolução hoje?";
		$texto  .="</br>";
		$texto  .="Não se esqueça. É importante que realizem as devidas analises e aprovações.";
		$texto  .="</br>";
		$texto  .="Lembrando que o maior objetivo da aprovação é negociar junto ao cliente para evitar devoluções.";
		$texto  .="</br>";
		$texto  .="</br>";
		$texto  .="<table border='1'>";		
		$texto  .="<tr>";
			$texto  .="<td>Protocolo</td>";
			$texto  .="<td>Cod. Cliente</td>";
			$texto  .="<td>Razão</td>";
			$texto  .="<td>Rota</td>";
			$texto  .="<td>Valor Total</td>";
			$texto  .="<td>Motivo Devolução</td>";
			$texto  .="<td>Divisão</td>";
			$texto  .="<td>CD</td>";
		$texto  .="</tr>";
        while($dados = oci_fetch_object($retorno)) {
			$texto  .="<tr>";
				$texto  .="<td>{$dados->DOC_CO_NUMERO}</td>";
				$texto  .="<td>{$dados->CLI_CO_NUMERO}</td>";
				$texto  .="<td>{$dados->CLI_NO_RAZAO_SOCIAL}</td>";
				$texto  .="<td>{$dados->ROT_CO_NUMERO}</td>";
				$texto  .="<td>{$dados->DOC_VR_VALOR}</td>";
				$texto  .="<td>{$dados->TOC_NO_DESCRICAO}</td>";
				$texto  .="<td>{$dados->DEV_CO_DIVISAO}</td>";
				$texto  .="<td>{$dados->CED_NO_CENTRO}</td>";
			$texto  .="</tr>";
        }
		$texto  .="</table>";
		$texto  .="</br>";
		$texto  .="</br>";
		$texto  .="Nao responda email automatico - Sistema Devolucao";
		$texto  .="</body></html>";
			
		$phpMailer->AddAddress($emailUsuario,$emailUsuario);
        
		$phpMailer->Subject = $assunto; //assunto
		$phpMailer->Body = $texto;
			
		if( $phpMailer->Send() ){
			echo "<script>alert('E-mail de pedencias enviado com sucesso.');</script>";
		}else{
			echo "<script>alert('{$codUsuario} - E-mail de pendencias ocorreu um erro ao ser enviado');</script>";
		}
        
    }

	
    public function enviaEmailAvisoGeladeira(Ocorrencia $Ocorrencia) {
		
        $phpMailer = emailPortalDevolucao::preencherMailer();
		$phpMailer->CharSet = "UTF-8";
		$phpMailer->IsHTML(true);
		
		$assunto = "AVISO IMPORTANTE do sistema de Devolução.";				
		
		$texto  ="<html><body>";
		$texto  .="DEVOLUÇÃO INTEGRAL NO ATO DA ENTREGA";
		$texto  .="</br>";
		$texto  .="CONTÉM PRODUTO (S) DE GELADEIRA. FAVOR NEGOCIAR A REENTREGA COM O CLIENTE.";
		$texto  .="</br>";
		$texto  .="PROTOCOLO – {$Ocorrencia->getCodigoDevolucao()}     ROTA - {$Ocorrencia->getObjRota()}";
		$texto  .="</br>";
		$texto  .="CLIENTE – {$Ocorrencia->getGlbCliente()->getClienteCodigo()} – {$Ocorrencia->getGlbCliente()->getClienteRazaoSocial()}";
		$texto  .="</br>";
		foreach($Ocorrencia->getDevNotaOrigem() as $nota){
			$texto  .="NF {$nota->getSerie()} / {$nota->getNota()}    EMISSÃO {$nota->getData()}     VALOR TOTAL NF R$ {$nota->getValorTotDevolucao()}";
			$texto  .="</br>";
		}
		$texto  .="</br>";
		$texto  .="Nao responda email automatico - Sistema Devolucao";
		$texto  .="</br>";
		$texto  .="SERVIMED COMERCIAL LTDA";
		$texto  .="</body></html>";
			
		$emailUsuario =	'wilianfernando@servimed.com.br';
		$phpMailer->AddAddress($emailUsuario,$emailUsuario);
        
		$phpMailer->Subject = $assunto; //assunto
		$phpMailer->Body = $texto;
			
		if( $phpMailer->Send() ){
			echo "<script>alert('E-mail enviado com sucesso.');</script>";
		}else{
			echo "<script>alert('{$codUsuario} - E-mail ocorreu um erro ao ser enviado');</script>";
		}
        
    }	
	
}
