<?php

ini_set('display_errors', 1);
include_once("../../../includes/mpdf60/mpdf.php");
include_once("../../../includes/barcode/barcode.inc.php");
include_once( "../../../includes/Dao/DaoSistema.class.php" );
include_once( "../../../includes/CalDepartamento.class.php" );
include_once( "../../../includes/CalTipoOcorrencia.class.php" );
include_once( "../../../includes/Dao/TipoOcorrenciaDAO.class.php" );
include_once( "../../../includes/Ocorrencia.class.php" );
include_once( "../../../includes/Dao/OcorrenciaDao.class.php" );
include_once( "../../../includes/TipoDevolucao.class.php" );
include_once( "../../../includes/Dao/TipoDevolucaoDao.class.php" );

include_once( "../../../includes/Passo.class.php" );
include_once( "../../../includes/Dao/PassoDao.class.php" );

include_once( "../../../includes/OcorrenciaPasso.class.php" );
include_once( "../../../includes/Dao/OcorrenciaPassoDao.class.php" );

include_once( "../../../includes/DevNotaOrigemItem.class.php" );
include_once( "../../../includes/Dao/DevNotaOrigemItemDao.class.php" );
include_once("include_novo/Tradutor.class.php");



function EspelhoNotaDevolucaoSobra($protocolo, $download = null, $usuario = null) {
    echo $protocolo;
    $Tradutor = new Tradutor();
    $ObjOcorrencia = new Ocorrencia();
    $ObjOcorrencia->setCodigoDevolucao($protocolo);
    

    $OcorrenciaDao = new OcorrenciaDao();
    $result_devolucao = $OcorrenciaDao->consultaOrdemColeta($ObjOcorrencia);
    $dadosOcorrencia = oci_fetch_object($result_devolucao);
    
    $OcorrenciaPassoGer = new OcorrenciaPasso();
    $OcorrenciaPassoDao = new OcorrenciaPassoDao();
    $OcorrenciaPassoGer->setCodigoOcorrencia($protocolo);
    $OcorrenciaPassoGer->setCodigoPasso(1);
    $OcorrenciaPassoGer = $OcorrenciaPassoDao->consultarOcorrenciaPasso($OcorrenciaPassoGer);
    if ($OcorrenciaPassoGer[0]) {
        $OcorrenciaPassoGer = $OcorrenciaPassoGer[0];
    } else {
        $OcorrenciaPassoGer = new OcorrenciaPasso();
    }
    
    //$result_totaliza = $OcorrenciaDao->consultaTotalEspelhoNota($protocolo);
    //$dadosTotalizar = oci_fetch_object($result_totaliza);
    
    $logoPath = $_SERVER['DOCUMENT_ROOT'] . '/img/logoServimed.jpg'; //testes externo
    
    $html = '
            

<!DOCTYPE html>

<html>

<style media="print">
.dev-invoice { float: left;width: 100%;}
.dev-invoice table.dev-invoice-table{ border:1px solid; font-family:arial; margin-bottom:30px; width: 100%;}
.dev-invoice table.TabelaLista tr th{ font-size:10px;}
.dev-invoice table.TabelaLista tr td{ font-size:10px;}
.dev-invoice table.dev-invoice-table tr td{ border-top: 1px solid #000; border-right: 1px solid #000; padding: 5px;}
.dev-invoice table.dev-invoice-table tr th{ border-right:1px solid; font-size:13px; padding:5px;}
.dev-invoice table.dev-invoice-table tr td{ font-size:12px;}
.dev-invoice table.dev-invoice-table tr td:first-child{ border-left:1px solid #000;}
</style>


    <head>        

        <title>Devolução - Espelho Nota</title>
        
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" >
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" >
        
    </head>
    <body>

        <div class="dev-page">
            <div class="container">
                <div class="wrapper wrapper-white">
					<div class="dev-invoice">
                        <table class="dev-invoice-table" cellpadding="0" cellspacing="0" border="0;">
							<thead>
								<tr>
									<th><img src="' . $logoPath . '"></th>
									<th style="border-right:none;">Servimed Comercial LTDA</th>
								</tr>
							</thead>
                        </table>                	
                   </div>
				
				
                    <div class="dev-invoice">
                        <table class="dev-invoice-table" cellpadding="0" cellspacing="0" border="0">
							<thead>
								<tr>
									<th>Dados emitentes</th>
                                    <th>Dados solicitação</th>
                                    <th style="border-right:none;">Dados NF vendas</th>
								</tr>
							</thead>
						
                            <tbody>
                                <tr>
                                    <td><strong>Razão social:</strong> '. $dadosOcorrencia->CLI_NO_RAZAO_SOCIAL . '</td>
                                    <td><strong>Data solicitação:</strong> '. $dadosOcorrencia->DOC_DT_CADASTRO.'</td>
									<td style="border-bottom:1px solid; border-right:none;"><strong>Nota:</strong> ';	
											$result_nota_origem = $OcorrenciaDao->consultaNotaOrigem($ObjOcorrencia);
											while (OCIFetchInto($result_nota_origem, $row_nota, OCI_ASSOC)) {
											$html .= '
											<strong style="font-size:10px;"><span style="font-weight:normal;"> ' . $row_nota["NTO_NU_SERIE"] . ' ' . $row_nota["NTO_NU_NOTA"] . ' -</span></strong>
											<strong style="font-size:10px;"><span style="font-weight:normal;"> ' . $row_nota["NTO_DT_NOTA"] . ' #</span></strong>
											';
											}
								  
								$html .= '    
									</td>
                                </tr>
                                <tr>
                                    <td><strong>Endereço: </strong> '. $dadosOcorrencia->CLI_ED_NUMERO .'</td>
                                    <td><strong>Data aprovação:</strong> '. $OcorrenciaPassoGer->getDataSaida() .'</td>
                                </tr>
                                <tr>
                                    <td><strong>CEP:</strong> '.$dadosOcorrencia->CLI_NU_CEP.'</td>
                                    <td><strong>Protocolo devolução:</strong> '.$dadosOcorrencia->DOC_CO_NUMERO.'</td>
                                </tr>
                                <tr>
                                    <td><strong>Município/UF:</strong> '.$dadosOcorrencia->CIDADE_CLIENTE.' - '. $dadosOcorrencia->ESTADO_CLIENTE .'</td>
                                    <td style="border-bottom:1px solid;"><strong>Motivo devolução:</strong> SOBRA DE MERCADORIA FÍSICA</td>
                                </tr>
                                <tr>
                                    <td><strong>CNPJ:</strong> '. $dadosOcorrencia->CLI_NU_CNPJ_CPF .'</td>
                                </tr>
                            </tbody>
                        </table>     
						
                        <table class="dev-invoice-table" cellpadding="0" cellspacing="0" border="0">
                            <tbody>
                                <tr>
                                    <td style="border-top:none;"><strong>Natureza da operação</strong></td>
                                    <td style="border-top:none; border-right:none;">Sobra de Mercadoria Física</td>
                                </tr>
                            </tbody>
                        </table> 
                    </div>    
                    <div class="dev-invoice">
                        <table class="dev-invoice-table dev-invoice-table02" cellpadding="0" cellspacing="0" border="0">
                            <tbody>
                                <tr>
                                    <th colspan="2" style="border-right:none;">Dados destinatário</th>
                                </tr>
                                <tr>
                                    <td><strong>Razão social: </strong> '. $dadosOcorrencia->CED_NO_DESTINATARIO .'</td>
                                    <td style="border-right:none;"><strong>CNPJ: </strong>'. $dadosOcorrencia->CED_NU_CNPJ .'</td>
                                </tr>
                                <tr>
                                    <td><strong>Endereço: </strong>'. $dadosOcorrencia->CED_ED_NUMERO .'</td>
                                    <td style="border-right:none;"><strong>CEP: </strong>'. $dadosOcorrencia->CED_NU_CEP .'</td>
                                </tr>
                                <tr>
                                    <td><strong>Municipio: </strong>'. $dadosOcorrencia->CIDADE_CED .'</td>
                                    <td style="border-right:none; border-bottom:1px solid;"><strong>UF: </strong>'. $dadosOcorrencia->ESTADO_CED .'</td>
                                </tr>
                                <tr>
                                    <td><strong>CD: </strong>'. $dadosOcorrencia->CED_NO_CENTRO .'</td>
                                </tr>
                            </tbody>
                            
                        </table>                                                                                 	
                    </div>                                    
                    <div class="dev-invoice">
                        <table class="dev-invoice-table TabelaLista" cellpadding="0" cellspacing="0" border="0">
                            <thead>
								<tr>
									<th>Dados Produtos</th>                                    
								</tr>
							</thead>
                            <thead>                            	
                                <tr>
                                    <th>Cód EAN</th>
                                    <th>Cód produto</th>
                                    <th class="text-center">Quantidade</th>
                                    <th>Produto</th>
                                </tr>
                            </thead>                               
                            <tbody>
    ';
                                                                
        $result_itens = $OcorrenciaDao->consultaItensOcorrencia($ObjOcorrencia);
        	while (OCIFetchInto($result_itens, $row_nota, OCI_ASSOC)) {                                         
                   $result_nota_itens = $OcorrenciaDao->consultaDadosEspelhoNota( $row_nota["CLI_CO_NUMERO"], $row_nota["NTO_NU_SERIE"], $row_nota["NTO_NU_NOTA"], $row_nota["NTO_DT_NOTA"],$ObjOcorrencia->getCodigoDevolucao() );                                                             
                $html .='                                
                    <tr>
                        <td>'. $row_nota["PRO_NU_CODIGO_BARRAS"] .'</td>
                        <td>'. $row_nota["PRO_CO_NUMERO"] .'</td>
                        <td class="text-center">'. $row_nota["NTI_NU_DEVQTD"] .'</td>                        
                        <td>'. $row_nota["PRO_NO_DESCRICAO"] .'</td>
                    </tr>
                    ';                
        }        
            $html .= '                     
                            </tbody>
                        </table>
                    </div>					
                </div>                                                        
            </div>
        </div>
    </body>
</html>
       ';
           
    $mpdf = new mPDF();
    
    $stylesheet = file_get_contents('lightblue.css');
    
    $mpdf->WriteHTML($stylesheet, 1);
    $mpdf->showWatermarkText = true;

    $mpdf->WriteHTML('<watermarktext content="SEM VALOR FISCAL" alpha="0.2" />');
    $mpdf->WriteHTML($html);

    $mpdf->Output("espelhoNota_{$protocolo}.pdf", "D");

    //LINUX
    // $mpdf->Output("../../utilitario/arquivos/ordemColeta/arq/espelhoNota_{$protocolo}.pdf", "F");

    //WINDOWS
    // $filename= "espelhoNota_{$protocolo}.pdf"; 
    // $filelocation = $_SERVER['DOCUMENT_ROOT'];  

    // $fileNL = $filelocation."\\".$filename;
    // $mpdf->Output($fileNL,'F');
    
}
?>
