

<?php

ini_set('display_errors', 1);
include_once("../../../includes/mpdf60/mpdf.php");
include_once("../../../includes/barcode/barcode.inc.php");
include_once( "../../../includes/Dao/DaoSistema.class.php" );
include_once( "../../../includes/CalDepartamento.class.php" );
include_once( "../../../includes/CalTipoOcorrencia.class.php" );
include_once( "../../../includes/Dao/TipoOcorrenciaDAO.class.php" );
include_once( "../../../includes/Ocorrencia.class.php" );
include_once( "../../../includes/Dao/OcorrenciaDao.class.php" );
include_once( "../../../includes/TipoDevolucao.class.php" );
include_once( "../../../includes/Dao/TipoDevolucaoDao.class.php" );

include_once( "../../../includes/Passo.class.php" );
include_once( "../../../includes/Dao/PassoDao.class.php" );

include_once( "../../../includes/OcorrenciaPasso.class.php" );
include_once( "../../../includes/Dao/OcorrenciaPassoDao.class.php" );
include_once("include_novo/Tradutor.class.php");



function EspelhoNotaDevolucao($protocolo, $download = null, $usuario = null) {
    
    $Tradutor = new Tradutor();
    $ObjOcorrencia = new Ocorrencia();
    $ObjOcorrencia->setCodigoDevolucao($protocolo);
    

    $OcorrenciaDao = new OcorrenciaDao();
    $result_devolucao = $OcorrenciaDao->consultaOrdemColeta($ObjOcorrencia);
    $dadosOcorrencia = oci_fetch_object($result_devolucao);
    
    $OcorrenciaPassoGer = new OcorrenciaPasso();
    $OcorrenciaPassoDao = new OcorrenciaPassoDao();
    $OcorrenciaPassoGer->setCodigoOcorrencia($protocolo);
    $OcorrenciaPassoGer->setCodigoPasso(1);
    $OcorrenciaPassoGer = $OcorrenciaPassoDao->consultarOcorrenciaPasso($OcorrenciaPassoGer);
    if ($OcorrenciaPassoGer[0]) {
        $OcorrenciaPassoGer = $OcorrenciaPassoGer[0];
    } else {
        $OcorrenciaPassoGer = new OcorrenciaPasso();
    }
    
    $result_totaliza = $OcorrenciaDao->consultaTotalEspelhoNota($protocolo);
    $dadosTotalizar = oci_fetch_object($result_totaliza);
    
    $logoPath = $_SERVER['DOCUMENT_ROOT'] . '/img/logoServimed.jpg'; //testes externo
    
    $html = '
            

<!DOCTYPE html>

<html>

<style media="print">
.dev-invoice { float: left;width: 100%;}
.dev-invoice table.dev-invoice-table{ border:1px solid; font-family:arial; margin-bottom:30px; width: 100%;}
.dev-invoice table.TabelaLista tr th{ font-size:10px;}
.dev-invoice table.TabelaLista tr td{ font-size:10px;}
.dev-invoice table.dev-invoice-table tr td{ border-top: 1px solid #000; border-right: 1px solid #000; padding: 5px;}
.dev-invoice table.dev-invoice-table tr th{ border-right:1px solid; font-size:13px; padding:5px;}
.dev-invoice table.dev-invoice-table tr td{ font-size:12px;}
.dev-invoice table.dev-invoice-table tr td:first-child{ border-left:1px solid #000;}
</style>


    <head>        

        <title>Devolução - Espelho Nota</title>
        
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" >
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" >
        
    </head>
    <body>

        <div class="dev-page">
            <div class="container">
                <div class="wrapper wrapper-white">
					<div class="dev-invoice">
                        <table class="dev-invoice-table" cellpadding="0" cellspacing="0" border="0;">
							<thead>
								<tr>
									<th><img src="' . $logoPath . '"></th>
									<th style="border-right:none;">Servimed Comercial LTDA</th>
								</tr>
							</thead>
                        </table>                	
                   </div>
				
				
                    <div class="dev-invoice">
                        <table class="dev-invoice-table" cellpadding="0" cellspacing="0" border="0">
							<thead>
								<tr>
									<th>Dados emitentes</th>
                                    <th>Dados solicitação</th>
                                    <th style="border-right:none;">Dados NF vendas</th>
								</tr>
							</thead>
						
                            <tbody>
                                <tr>
                                    <td><strong>Razão social:</strong> '. $dadosOcorrencia->CLI_NO_RAZAO_SOCIAL . '</td>
                                    <td><strong>Data solicitação:</strong> '. $dadosOcorrencia->DOC_DT_CADASTRO.'</td>
									<td style="border-bottom:1px solid; border-right:none;"><strong>Nota:</strong> ';	
											$result_nota_origem = $OcorrenciaDao->consultaNotaOrigem($ObjOcorrencia);
											while (OCIFetchInto($result_nota_origem, $row_nota, OCI_ASSOC)) {
											$html .= '
											<strong style="font-size:10px;"><span style="font-weight:normal;"> ' . $row_nota["NTO_NU_SERIE"] . ' ' . $row_nota["NTO_NU_NOTA"] . ' -</span></strong>
											<strong style="font-size:10px;"><span style="font-weight:normal;"> ' . $row_nota["NTO_DT_NOTA"] . ' #</span></strong>
											';
											}
								  
								$html .= '    
									</td>
                                </tr>
                                <tr>
                                    <td><strong>Endereço: </strong> '. $dadosOcorrencia->CLI_ED_NUMERO .'</td>
                                    <td><strong>Data aprovação:</strong> '. $OcorrenciaPassoGer->getDataSaida() .'</td>
                                </tr>
                                <tr>
                                    <td><strong>CEP:</strong> '.$dadosOcorrencia->CLI_NU_CEP.'</td>
                                    <td><strong>Protocolo devolução:</strong> '.$dadosOcorrencia->DOC_CO_NUMERO.'</td>
                                </tr>
                                <tr>
                                    <td><strong>Município/UF:</strong> '.$dadosOcorrencia->CIDADE_CLIENTE.' - '. $dadosOcorrencia->ESTADO_CLIENTE .'</td>
                                    <td style="border-bottom:1px solid;"><strong>Motivo devolução:</strong> '. $dadosOcorrencia->TOC_NO_DESCRICAO.'</td>
                                </tr>
                                <tr>
                                    <td><strong>CNPJ:</strong> '. $dadosOcorrencia->CLI_NU_CNPJ_CPF .'</td>
                                </tr>
                            </tbody>
                        </table>     
						
                        <table class="dev-invoice-table" cellpadding="0" cellspacing="0" border="0">
                            <tbody>
                                <tr>
                                    <td style="border-top:none;"><strong>Natureza da operação</strong></td>
                                    <td style="border-top:none; border-right:none;">Devolução de compra</td>
                                </tr>
                            </tbody>
                        </table> 
                    </div>    
                    <div class="dev-invoice">
                        <table class="dev-invoice-table dev-invoice-table02" cellpadding="0" cellspacing="0" border="0">
                            <tbody>
                                <tr>
                                    <th colspan="2" style="border-right:none;">Dados destinatário</th>
                                </tr>
                                <tr>
                                    <td><strong>Razão social: </strong> '. $dadosOcorrencia->CED_NO_DESTINATARIO .'</td>
                                    <td style="border-right:none;"><strong>CNPJ: </strong>'. $dadosOcorrencia->CED_NU_CNPJ .'</td>
                                </tr>
                                <tr>
                                    <td><strong>Endereço: </strong>'. $dadosOcorrencia->CED_ED_NUMERO .'</td>
                                    <td style="border-right:none;"><strong>CEP: </strong>'. $dadosOcorrencia->CED_NU_CEP .'</td>
                                </tr>
                                <tr>
                                    <td><strong>Municipio: </strong>'. $dadosOcorrencia->CIDADE_CED .'</td>
                                    <td style="border-right:none; border-bottom:1px solid;"><strong>UF: </strong>'. $dadosOcorrencia->ESTADO_CED .'</td>
                                </tr>
                                <tr>
                                    <td><strong>CD: </strong>'. $dadosOcorrencia->CED_NO_CENTRO .'</td>
                                </tr>
                            </tbody>
                            
                        </table>                                                                                 	
                    </div>   
                    <div class="dev-invoice">
                        <table class="dev-invoice-table dev-invoice-table02 DadosImpostos" cellpadding="0" cellspacing="0" border="0">
                            <tbody>
                                <tr>
                                    <th colspan="3" style="border-right:none;">Dados impostos</th>
                                </tr>
                                <tr>
                                    <td>Base calculo ICMS: <strong>R$ '. $Tradutor->formatar($dadosTotalizar->TOTAL_BASE_CALCULOICMS, 'MOEDA') .'</strong></td>
                                    <td>Valor ICMS: <strong>R$ '. $Tradutor->formatar($dadosTotalizar->TOTAL_VALOR_PROPICMS, 'MOEDA') .'</strong></td>
                                    <td style="border-right:none;">Valor total bruto: <strong>R$ '. $Tradutor->formatar($dadosTotalizar->TOTAL_BRUTO, 'MOEDA') .'</strong></td>
                                </tr>
                                <tr>
                                    <td>Base calculo ICMS ST: <strong>R$ '. $Tradutor->formatar($dadosTotalizar->TOTAL_BASE_CALCULOST, 'MOEDA') .'</strong></td>
                                    <td style="border-bottom:1px solid;">Valor do ICMS do ST: <strong>R$ '. $Tradutor->formatar($dadosTotalizar->TOTAL_VALOR_PROPST, 'MOEDA') .'</strong></td>
                                    <td style="border-bottom:1px solid; border-right:none;">Valor total Liquido: <strong>R$ '. $Tradutor->formatar($dadosTotalizar->TOTAL_LIQUIDO, 'MOEDA') .'</strong></td>
                                </tr>
				<tr>
                                    <td>Valor desconto: <strong>R$ '. $Tradutor->formatar($dadosTotalizar->TOTAL_DESCONTO, 'MOEDA') .'</strong></td>
				<tr>
                            </tbody>
                            
                        </table>                                                                                 	
                    </div>                                 
                    <div class="dev-invoice">
                        <table class="dev-invoice-table TabelaLista" cellpadding="0" cellspacing="0" border="0">
                            <thead>
                            	<tr>
                                    <th colspan="3" style="border-bottom:1px solid;"></th>
                                    <th colspan="2" style="border-bottom:1px solid;">Valor bruto</th>
                                    <th colspan="1" style="border-bottom:1px solid;">Desconto</th>
                                    <th colspan="2" style="border-bottom:1px solid;">Valor liquido</th>
                                    <th colspan="4" style="border-bottom:1px solid; border-right:none;">Impostos</th>
                                </tr>
                                <tr>
                                    <th>Cód produto</th>
                                    <th class="text-center">Qt. unitária</th>
                                    <th>Descrição</th>
                                    <th class="text-center">Unitário</th>
                                    <th class="text-center">Total</th>
                                    <th class="text-center">Valor</th>
                                    <th class="text-center">Unitário</th>
                                    <th class="text-center">Total</th>
                                    <th class="text-center">BC ST</th>
                                    <th class="text-center">Valor ST</th>
                                    <th class="text-center">BC ICMS normal</th>
                                    <th class="text-center" style="border-right:none;">ICMS normal</th>
                                </tr>
                            </thead>                               
                            <tbody>
    ';
                                                                
        $result_nota_origem = $OcorrenciaDao->consultaNotaOrigem($ObjOcorrencia);
	while (OCIFetchInto($result_nota_origem, $row_nota, OCI_ASSOC)) {                                         
                   $result_nota_itens = $OcorrenciaDao->consultaDadosEspelhoNota( $row_nota["CLI_CO_NUMERO"], $row_nota["NTO_NU_SERIE"], $row_nota["NTO_NU_NOTA"], $row_nota["NTO_DT_NOTA"],$ObjOcorrencia->getCodigoDevolucao() );
                    while ($row_itens = oci_fetch_object($result_nota_itens)) {                                                   
    $html .='                                

                                <tr>
                                    <td>'. $row_itens->PRO_CO_NUMERO .'</td>
                                    <td class="text-center">'. $row_itens->NTI_NU_DEVQTD .'</td>
                                    <td>'. $row_itens->PRO_NO_DESCRICAO .'</td>
                                    <td class="text-right">'. $Tradutor->formatar($row_itens->NTI_VL_PRECOUNIT, 'MOEDA').'</td>
                                    <td class="text-right">'. $Tradutor->formatar($row_itens->NTI_VL_TOTBRUTO, 'MOEDA') .'</td>
                                    <td class="text-right">'. $Tradutor->formatar($row_itens->NTI_VL_TOTDESC, 'MOEDA') .'</td>
                                    <td class="text-right">'. $Tradutor->formatar($row_itens->VALOR_UNITLIQ , 'MOEDA').'</td>
                                    <td class="text-right">'. $Tradutor->formatar($row_itens->NTI_VL_TOTLIQ, 'MOEDA').'</td>
                                    <td class="text-right">'. $Tradutor->formatar($row_itens->BASE_CALCULOST, 'MOEDA').'</td>
                                    <td class="text-right">'. $Tradutor->formatar($row_itens->VALOR_PROPST, 'MOEDA').'</td>
                                    <td class="text-right">'. $Tradutor->formatar($row_itens->BASE_CALCULOICMS, 'MOEDA').'</td>
                                    <td class="text-right;" style="border-right:none;">'. $Tradutor->formatar($row_itens->VALOR_PROPICMS, 'MOEDA').'</td>
                                </tr>
    ';
    
                    }
        }
    
    
    $html .= '                     
                            </tbody>
                        </table>
                    </div>
					<div class="dev-invoice">
                        <table class="dev-invoice-table" cellpadding="0" cellspacing="0" border="0;">
                            <thead>
                                <tr><th style="border-right:none;">Dados adicionais</th></tr>
                            </thead>
                            <tbody>
                                <tr><td style="border-right:none;"><strong>Motivo devolução: </strong>(obrigatório)</td></tr>
                                <tr><td style="border-right:none;"><strong>NFs de origem: </strong>(obrigatório)</td></tr>
                                <tr><td style="border-right:none;"><strong>Protocolo de devolução: </strong>(opcional)</td></tr>
                            </tbody>
                        </table>                	
                   </div>
                </div>                        

                
                
            </div>
        </div>

    </body>
</html>
       ';
    
/*    
    $html ='
    
<html >
    <head>        
        <title>Devolução - Espelho Nota</title>


    </head>
    <body>     

       <div class="dev-page">
            <div class="container">
                <div class="wrapper wrapper-white">
                    <div class="dev-invoice">

      <table class="dev-invoice-table">
                            <tbody>
                                <tr>
                                    <th colspan="2">Dados emitentes</th>
                                </tr>
                                <tr>
                                    <td width="10%"><strong>Razão social</strong></td>
                                    <td class="sub-color" width="30%">S E LILLI & CIA TDA EPP</td>
                                </tr>
                                <tr>
                                    <td><strong>Endereço</strong></td>
                                    <td class="sub-color">PCA TIRADENTES - 289</td>
                                </tr>
                                <tr>
                                    <td><strong>CEP</strong></td>
                                    <td class="sub-color">13.800-452</td>
                                </tr>
                                <tr>
                                    <td><strong>Município/UF</strong></td>
                                    <td class="sub-color">Mogi Mirim/SP</td>
                                </tr>
                                <tr>
                                    <td><strong>CNPJ</strong></td>
                                    <td class="sub-color">57.819.708/0001-96</td>
                                </tr>
                            </tbody>
                        </table>   
                        
        </div>
        </div>
        </div>
        </div>

    </body>
</html>
           ';*/

//    echo $html;
//    $html = "oi geovanni!";

    $mpdf = new mPDF();

//    $mpdf->SetDisplayMode('fullpage');

// LOAD a stylesheet
    $stylesheet = file_get_contents('lightblue.css');
    
    $mpdf->WriteHTML($stylesheet, 1);
    $mpdf->showWatermarkText = true;

    $mpdf->WriteHTML('<watermarktext content="SEM VALOR FISCAL" alpha="0.2" />');
    $mpdf->WriteHTML($html);

    $mpdf->Output("espelhoNota_{$protocolo}.pdf", "D");
}
