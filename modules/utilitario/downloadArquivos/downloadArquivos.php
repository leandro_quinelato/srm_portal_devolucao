<?php

@session_start();

include_once( "../../../includes/Dao/DaoSistema.class.php" );
include_once( "../../../includes/OcorrenciaArquivos.class.php" );
include_once( "../../../includes/Dao/OcorrenciaArquivosDao.class.php" );
include_once( "../../../includes/TipoArquivoDevolucao.class.php" );
include_once( "../../../includes/Dao/TipoArquivoDevolucaoDao.class.php" );
include_once( "../ordemColeta/ordemColetaDevolucaoMain.php" );
include_once( "../espelhoNotaDevolucao/espelhoNotaDevolucaoMain.php" );
include_once( "../espelhoNotaDevolucao/espelhoNotaDevolucaoSobraMain.php" );


extract($_REQUEST);
$OcorrenciaArquivosDao = new OcorrenciaArquivosDao();
$OcorrenciaArquivos = new OcorrenciaArquivos();
$OcorrenciaArquivos->setCodOcorrencia($codOcorrencia);
$OcorrenciaArquivos->setCodigoTipoArquivo($codigoTipoArquivo);
$OcorrenciaArquivos->setNomeArquivoCliente($nomeTipoArquivo);
$tipoOcor = $OcorrenciaArquivosDao->verificaTipoOcorrencia($OcorrenciaArquivos);
$result = $OcorrenciaArquivosDao->consultaArquivos($OcorrenciaArquivos);
$dados = oci_fetch_object($result);

if ($codigoTipoArquivo == 1) {
    $arquivo = $_SERVER['DOCUMENT_ROOT'] . $dados->DTA_NO_PATH . "arq/" . $dados->DOA_NO_ARQSYSTEM;


    $tipo_arquivo = explode(".", $dados->DOA_NO_ARQSYSTEM);
    $tipo_arquivo = end($tipo_arquivo);


    switch ($tipo_arquivo) { // verifica a extensão do arquivo para pegar o tipo
        case "pdf": $tipo = "application/pdf";
            break;
        case "exe": $tipo = "application/octet-stream";
            break;
        case "zip": $tipo = "application/zip";
            break;
        case "doc": $tipo = "application/msword";
            break;
        case "xls": $tipo = "application/vnd.ms-excel";
            break;
        case "ppt": $tipo = "application/vnd.ms-powerpoint";
            break;
        case "gif": $tipo = "image/gif";
            break;
        case "png": $tipo = "image/png";
            break;
        case "jpg": $tipo = "image/jpg";
            break;
        case "mp3": $tipo = "audio/mpeg";
            break;
        case "php": // deixar vazio por seurança
        case "htm": // deixar vazio por seurança
        case "html": // deixar vazio por seurança
    }
    header("Content-Type: " . $tipo); // informa o tipo do arquivo ao navegador
    header("Content-Length: " . filesize($arquivo)); // informa o tamanho do arquivo ao navegador
    header("Content-Disposition: attachment; filename=" . basename($arquivo)); // informa ao navegador que é tipo anexo e faz abrir a janela de download, tambem informa o nome do arquivo
    readfile($arquivo); // lê o arquivo
    exit; // aborta pós-ações
}else if ($codigoTipoArquivo == 2) {
    DocumentoColeta($codOcorrencia);
    
}else if ($codigoTipoArquivo == 3) {    
    
    if($tipoOcor)
        EspelhoNotaDevolucaoSobra($codOcorrencia);
    else
        EspelhoNotaDevolucao($codOcorrencia);
    
    
}