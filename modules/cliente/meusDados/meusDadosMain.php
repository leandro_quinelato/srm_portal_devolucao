<?php
include_once( "../../../includes/Dao/DaoSistema.class.php" );
include_once( "../../../includes/ClienteContatoDevolucao.class.php" );
include_once( "../../../includes/Dao/ClienteContatoDevolucaoDao.class.php" );

include_once("DaoGlobal.class.php");
include_once("SistemaDao.class.php");
include_once("GlbSistema.class.php");
include_once("GlbSistemaPerfil.class.php");
include_once("GlbUsuario.class.php");
include_once("GlbColaborador.class.php");
include_once("GlbPessoa.class.php");
include_once("GlbEndereco.class.php");
include_once("GlbTelefone.class.php");
include_once("include_novo/GlbCentroDistribuicao.class.php");
include_once("include_novo/GlbTransportadora.class.php");
include_once("include_novo/GlbContatoTransportador.class.php");
include_once("include_novo/ContatoTransportadorDao.class.php");
include_once("GlbProgramaRotina.class.php");
include_once("GlbModuloPrograma.class.php");
include_once("GlbSistemaModulo.class.php");
include_once("PerfilUsuarioDao.class.php");
include_once("PerfilPermissaoDao.class.php");
include_once("integraSetSistemas.php");
$integra = new integraSetSistemas("conf.xml");
$ObjUsuario = unserialize($integra->getSistemaIntegra());
$ObjRotinas = $ObjUsuario->getObjSistemaPerfil()->getObjRotina();

//echo "<pre>";
//print_r($_REQUEST);
//echo "</pre>";
//die();

extract($_REQUEST);

$ObjContatos = new ClienteContatoDevolucao();
$ContatoDevolucaoDao = new ClienteContatoDevolucaoDao();

if ($acao == "BUSCAR") {

    $resultDadosCLiente = $ContatoDevolucaoDao->consultaDadosCliente($codCliente);
    $DadosCLiente = oci_fetch_object($resultDadosCLiente);

    if ($codCliente != $DadosCLiente->CLI_CO_NUMERO) {
        DIE('ERROR');
    }

    $ObjContatos = $ContatoDevolucaoDao->consultarContatoCliente($DadosCLiente->CLI_CO_NUMERO);

    if ($ObjContatos[0]) {
        $ObjContatos = $ObjContatos;
    } else {
        $ObjContatos = new ClienteContatoDevolucao();
    }
    ?>

    <div class="col-md-5 col-sm-12 col-xs-12">
        <div class="table-responsive">
            <table class="table table-bordered table-striped table-hover">
                <tbody>
                    <tr>
                        <td>CNPJ</td> 	
                        <!--<td><strong>69.061.547/0001-01</strong></td>-->
                        <td><strong><?php echo $DadosCLiente->CLI_NU_CNPJ_CPF; ?></strong></td>
                    </tr>  
                    <tr>
                        <td>Razão Social</td> 	
                        <td><strong><?php echo $DadosCLiente->CLI_NO_RAZAO_SOCIAL; ?></strong></td>
                    </tr>  
                    <tr>
                        <td>Nome Fantasia</td> 	
                        <td><strong><?php echo $DadosCLiente->CLI_NO_FANTASIA; ?></strong></td>
                    </tr>  
                    <tr>
                        <td>Código Servimed</td> 	
                        <td><strong><?php echo $DadosCLiente->CLI_CO_NUMERO; ?></strong></td>
                    </tr>  
                    <tr>
                        <td>CEP</td> 	
                        <!--<td><strong>17032-010</strong></td>-->
                        <td><strong><?php echo $DadosCLiente->CLI_NU_CEP; ?></strong></td>
                    </tr>  
                    <tr>
                        <td>Endereço</td> 	
                        <td><strong><?php echo $DadosCLiente->CLI_ED_LOGRADOURO; ?></strong></td>
                    </tr>  
                    <tr>
                        <td>Nº</td> 	
                        <td><strong><?php echo $DadosCLiente->CLI_ED_NUMERO; ?></strong></td>
                    </tr>  
                    <tr>
                        <td>Complemento</td>
                        <td><?php echo $DadosCLiente->CLI_ED_COMPLEMENTO; ?></td>
                    </tr>  
                    <tr>
                        <td>Bairro</td> 	
                        <td><strong><?php echo $DadosCLiente->CLI_NO_BAIRRO; ?></strong></td>
                    </tr>  
                    <tr>
                        <td>Cidade</td> 	
                        <td><strong><?php echo $DadosCLiente->LOC_NO_CIDADE; ?></strong></td>
                    </tr>  
                    <tr>
                        <td>UF</td> 	
                        <td><strong><?php echo $DadosCLiente->EST_SG_ESTADO; ?></strong> </td>                                    
                    </tr>  
                </tbody>
            </table>
        </div>                                

    </div>
    <div class="col-md-7 col-sm-12 col-xs-12">
        <form id="formCadContato">
            <input type="hidden" id="codClienteContato" name="codClienteContato" value="<?php echo $DadosCLiente->CLI_CO_NUMERO; ?>">
            <input type="hidden" id="usuario" name="usuario" value="<?php echo $ObjUsuario->getUsuarioCodigo(); ?>">
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-6">
                    <div class="form-group">
                        <label>Título do Contato</label><br>
                        <div class="col-md-11 col-sm-12 col-xs-12">
                            <input type="text" placeholder="Ex. Fiscal, Jurídico, Devolução" class="form-control" id="descContato" name="descContato">
                        </div>
                    </div>
                </div>

            </div>
            <div class="row">  
                <div class="col-md-6 col-sm-6 col-xs-6">                        
                    <div class="form-group">
                        <label>Tipo de contato</label>
                        <select class="form-control selectpicker" id="tipoContato" name="tipoContato">
                            <option value="">Selecione o contato</option>
                            <option value="T">Telefone</option>
                            <option value="E">E-Mail</option>
                            <option value="F">Fax</option>
                        </select>
                    </div>                        
                </div>                                    
                <div class="col-md-6 col-sm-6 col-xs-6">
                    <div class="form-group">
                        <label>Contato</label><br>
                        <div class="input-group">                                                                                         
                            <input type="text" value="" class="form-control" id="contato" name="contato">                                
                            <div class="input-group-btn"><a class="btn btn-default" title="Adicionar contato" onclick="javascript: inserirClienteContato();">+</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <div id="areaListaContatos">
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Título do contato</th>
                            <th>Tipo</th>
                            <th></th>
                            <th>Data cadastro</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($ObjContatos as $ObjContato) {
                            ?>  
                            <tr>
                                <td><?php echo $ObjContato->getDescContato(); ?></td>
                                <td><?php echo $ObjContato->getTipoContato(); ?></td>
                                <td><?php echo $ObjContato->getContato(); ?></td>
                                <td><?php echo $ObjContato->getDataCadastro(); ?></td>
                                <td><button type="button" class="fa fa-minus-square" title="Inativar" data-toggle="modal" data-target="excluirRegistro" onclick="javascript: excluirClienteContato(<?php echo $ObjContato->getCodigoContato(); ?>);"></button></td>
                            </tr>
                            <?php
                        }
                        ?>

                    </tbody>
                </table>
            </div>
        </div>

    </div>

    <?php
} else if ($acao == 'ADD') {

    $ObjContato = new ClienteContatoDevolucao();
    $ObjContato->setCodigoCliente($codClienteContato);
    $ObjContato->setTipoContato($tipoContato);
    $ObjContato->setDescContato($descContato);
    $ObjContato->setContato($contato);
    $ObjContato->setUsuario($usuario);

    if (!$ContatoDevolucaoDao->inserirClienteContato($ObjContato)) {
        die('ERROR');
    }
} else if ($acao == 'LISCONTATO') {

    $ObjContatos = $ContatoDevolucaoDao->consultarContatoCliente($codClienteContato);

    if ($ObjContatos[0]) {
        $ObjContatos = $ObjContatos;
    } else {
        $ObjContatos = new ClienteContatoDevolucao();
    }
    ?>

    <div class="table-responsive">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Título do contato</th>
                    <th>Tipo</th>
                    <th></th>
                    <th>Data cadastro</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
    <?php
    foreach ($ObjContatos as $ObjContato) {
        ?>  
                    <tr>
                        <td><?php echo $ObjContato->getDescContato(); ?></td>
                        <td><?php echo $ObjContato->getTipoContato(); ?></td>
                        <td><?php echo $ObjContato->getContato(); ?></td>
                        <td><?php echo $ObjContato->getDataCadastro(); ?></td>
                        <td><button type="button" class="fa fa-minus-square" data-toggle="modal" data-target="excluirRegistro" title="Inativar" onclick="javascript: excluirClienteContato(<?php echo $ObjContato->getCodigoContato(); ?>);"></button></td>
                    </tr>
        <?php
    }
    ?>

            </tbody>
        </table>
    </div>

    <?php
} else if ($acao == 'EXCLUIR') {
echo "</pre>";
    $ObjContato = new ClienteContatoDevolucao();
    $ObjContato->setCodigoCliente($codClienteContato);
    $ObjContato->setCodigoContato($codContato);
    $ObjContato->setUsuario($usuario);

    if (!$ContatoDevolucaoDao->excluirClienteContato($ObjContato)) {
        die('ERROR');
    }
}
?>