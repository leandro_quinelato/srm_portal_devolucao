<?php
include_once( "../../../includes/Dao/DaoSistema.class.php" );
include_once( "../../../includes/ClienteContatoDevolucao.class.php" );
include_once( "../../../includes/Dao/ClienteContatoDevolucaoDao.class.php" );

include_once("DaoGlobal.class.php");
include_once("SistemaDao.class.php");
include_once("GlbSistema.class.php");
include_once("GlbSistemaPerfil.class.php");
include_once("GlbUsuario.class.php");
include_once("GlbColaborador.class.php");
include_once("GlbPessoa.class.php");
include_once("GlbEndereco.class.php");
include_once("GlbTelefone.class.php");
include_once("include_novo/GlbCentroDistribuicao.class.php");
include_once("include_novo/GlbTransportadora.class.php");
include_once("include_novo/GlbContatoTransportador.class.php");
include_once("include_novo/ContatoTransportadorDao.class.php");
include_once("GlbProgramaRotina.class.php");
include_once("GlbModuloPrograma.class.php");
include_once("GlbSistemaModulo.class.php");
include_once("PerfilUsuarioDao.class.php");
include_once("PerfilPermissaoDao.class.php");
include_once("integraSetSistemas.php");
$integra = new integraSetSistemas("conf.xml");
$ObjUsuario = unserialize($integra->getSistemaIntegra());
$ObjRotinas = $ObjUsuario->getObjSistemaPerfil()->getObjRotina();

//echo "<pre>";
//print_r($ObjUsuario);
//echo "</pre>";

$ObjContatos = new ClienteContatoDevolucao();
$ContatoDevolucaoDao = new ClienteContatoDevolucaoDao();

if ($ObjUsuario->getUsuarioTipo() == "CLI") {
    $codcliente = $ObjUsuario->getObjCliente()->getClienteCodigo();
    $resultDadosCLiente = $ContatoDevolucaoDao->consultaDadosCliente($ObjUsuario->getObjCliente()->getClienteCodigo());
    $DadosCLiente = oci_fetch_object($resultDadosCLiente);

    $ObjContatos = $ContatoDevolucaoDao->consultarContatoCliente($ObjUsuario->getObjCliente()->getClienteCodigo());

    if ($ObjContatos[0]) {
        $ObjContatos = $ObjContatos;
    } else {
        $ObjContatos = new ClienteContatoDevolucao();
    }
}
?>

<!DOCTYPE html>
<html lang="en">
    <head>        
        <!-- meta section -->
        <title>Cliente - Meus Dados</title>
        <?php include("../../../library/head.php"); ?>
        <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/libCliente.js"></script>
    </head>
    <body>
        <!-- set loading layer -->
        <div class="dev-page-loading preloader"></div>
        <!-- ./set loading layer -->

        <!-- page wrapper -->
        <div class="dev-page">

            <!-- page header -->    
            <?php include("../../../library/topo.php"); ?>
            <!-- ./page header -->

            <!-- page container -->
            <div class="dev-page-container">

                <!-- page sidebar -->
                <?php include("../../../library/menu.php"); ?>
                <!-- ./page sidebar -->

                <!-- page content -->
                <div class="dev-page-content">                    
                    <!-- page content container -->
                    <div class="container">

                        <!-- page title -->
                        <div class="page-title">
                            <h1>Detalhes do cliente</h1>

                            <ul class="breadcrumb">
                                <li><a href="#">Cliente</a></li>
                                <li>Meus dados</li>
                            </ul>
                        </div>                        
                        <!-- ./page title -->



                        <!-- datatables plugin -->
                        <div class="wrapper wrapper-white">
                            <div class="row">
                                <!--new-->
                                <?php
                                if ($ObjUsuario->getUsuarioTipo() != "CLI") {
                                    ?>                              
                                    <div class="col-md-5 col-sm-12 col-xs-12">

                                        <div class="row">
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <div class="form-group">
                                                    <label>Cód Cliente</label><br>
                                                    <div class="col-md-11 col-sm-12 col-xs-12">
                                                        <input type="text" placeholder="Informe um cliente" class="form-control" name="codCliente" id="codCliente" onkeydown="consultarContatoCli(event, this.value);">
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>
                            </div>
                            <div class="row">
                                <!--fim new-->
                                <div id="divMeusdados">  
                                    <div class="col-md-5 col-sm-12 col-xs-12">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-striped table-hover">
                                                <tbody>
                                                    <tr>
                                                        <td>CNPJ</td> 	
                                                        <!--<td><strong>69.061.547/0001-01</strong></td>-->
                                                        <td><strong><?php echo $DadosCLiente->CLI_NU_CNPJ_CPF; ?></strong></td>
                                                    </tr>  
                                                    <tr>
                                                        <td>Razão Social</td> 	
                                                        <td><strong><?php echo $DadosCLiente->CLI_NO_RAZAO_SOCIAL; ?></strong></td>
                                                    </tr>  
                                                    <tr>
                                                        <td>Nome Fantasia</td> 	
                                                        <td><strong><?php echo $DadosCLiente->CLI_NO_FANTASIA; ?></strong></td>
                                                    </tr>  
                                                    <tr>
                                                        <td>Código Servimed</td> 	
                                                        <td><strong><?php echo $DadosCLiente->CLI_CO_NUMERO; ?></strong></td>
                                                    </tr>  
                                                    <tr>
                                                        <td>CEP</td> 	
                                                        <!--<td><strong>17032-010</strong></td>-->
                                                        <td><strong><?php echo $DadosCLiente->CLI_NU_CEP; ?></strong></td>
                                                    </tr>  
                                                    <tr>
                                                        <td>Endereço</td> 	
                                                        <td><strong><?php echo $DadosCLiente->CLI_ED_LOGRADOURO; ?></strong></td>
                                                    </tr>  
                                                    <tr>
                                                        <td>Nº</td> 	
                                                        <td><strong><?php echo $DadosCLiente->CLI_ED_NUMERO; ?></strong></td>
                                                    </tr>  
                                                    <tr>
                                                        <td>Complemento</td>
                                                        <td><?php echo $DadosCLiente->CLI_ED_COMPLEMENTO; ?></td>
                                                    </tr>  
                                                    <tr>
                                                        <td>Bairro</td> 	
                                                        <td><strong><?php echo $DadosCLiente->CLI_NO_BAIRRO; ?></strong></td>
                                                    </tr>  
                                                    <tr>
                                                        <td>Cidade</td> 	
                                                        <td><strong><?php echo $DadosCLiente->LOC_NO_CIDADE; ?></strong></td>
                                                    </tr>  
                                                    <tr>
                                                        <td>UF</td> 	
                                                        <td><strong><?php echo $DadosCLiente->EST_SG_ESTADO; ?></strong> </td>                                    
                                                    </tr>  
                                                </tbody>
                                            </table>
                                        </div>                                

                                    </div>
                                    <div class="col-md-7 col-sm-12 col-xs-12">

                                        <form id="formCadContato">
                                            <input type="hidden" id="codClienteContato" name="codClienteContato" value="<?php echo $codcliente; ?>">
                                            <input type="hidden" id="usuario" name="usuario" value="<?php echo $ObjUsuario->getUsuarioCodigo(); ?>">
                                            <div class="row">
                                                <div class="col-md-6 col-sm-6 col-xs-6">
                                                    <div class="form-group">
                                                        <label>Título do Contato</label><br>
                                                        <div class="col-md-11 col-sm-12 col-xs-12">
                                                            <input type="text" placeholder="Ex. Fiscal, Jurídico, Devolução" class="form-control" id="descContato" name="descContato">
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="row">  
                                                <div class="col-md-6 col-sm-6 col-xs-6">                        
                                                    <div class="form-group">
                                                        <label>Tipo de contato</label>
                                                        <select class="form-control selectpicker" id="tipoContato" name="tipoContato">
                                                            <option value="">Selecione o contato</option>
                                                            <option value="T">Telefone</option>
                                                            <option value="E">E-Mail</option>
                                                            <option value="F">Fax</option>
                                                        </select>
                                                    </div>                        
                                                </div>                                    
                                                <div class="col-md-6 col-sm-6 col-xs-6">
                                                    <div class="form-group">
                                                        <label>Contato</label><br>
                                                        <div class="input-group">                                                                                         
                                                            <input type="text" value="" class="form-control" id="contato" name="contato">                                
                                                            <div class="input-group-btn"><a class="btn btn-default" title="Adicionar contato" onclick="javascript: inserirClienteContato();">+</a></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                        <div id="areaListaContatos">
                                            <div class="table-responsive">
                                                <table class="table table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th>Título do contato</th>
                                                            <th>Tipo</th>
                                                            <th></th>
                                                            <th>Data cadastro</th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                        foreach ($ObjContatos as $ObjContato) {
                                                            ?>  
                                                            <tr>
                                                                <td><?php echo $ObjContato->getDescContato(); ?></td>
                                                                <td><?php echo $ObjContato->getTipoContato(); ?></td>
                                                                <td><?php echo $ObjContato->getContato(); ?></td>
                                                                <td><?php echo $ObjContato->getDataCadastro(); ?></td>
                                                                <td><button type="button" class="fa fa-minus-square"  title="Inativar" onclick="javascript: excluirClienteContato(<?php echo $ObjContato->getCodigoContato(); ?>);"></button></td>
                                                            </tr>
                                                            <?php
                                                        }
                                                        ?>



                                                    </tbody>
                                                </table>
                                            </div>

                                        </div>
                                    </div>

                                </div>
                            </div>                            
                        </div>                        
                        <!-- ./datatables plugin -->


                        <!-- Copyright -->
                        <?php include("../../../library/copyright.php"); ?>
                        <!-- ./Copyright -->

                    </div>
                    <!-- ./page content container -->                                       
                </div>
                <!-- ./page content -->                                                
            </div>  
            <!-- ./page container -->

            <!-- page search -->

            <!-- page search -->            
        </div>
        <!-- ./page wrapper -->



        <div class="modal fade" id="modal_wizard" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Cadastro / Edição de motivo de devolução</h4>
                    </div>
                    <div class="modal-body">
                        <div id="step-6">
                            <div class="row">
                                <div class="col-md-2 col-sm-4 col-xs-4">
                                    <div class="form-group">
                                        <label>Início</label>                            
                                        <input type="text" class="form-control datepicker">
                                    </div> 
                                </div>  
                                <div class="col-md-2 col-sm-4 col-xs-4">
                                    <div class="form-group">
                                        <label>Fim</label>                            
                                        <input type="text" class="form-control datepicker">
                                    </div> 
                                </div>   
                            </div>  
                            <div class="row">
                                <div class="col-md-6 col-sm-4 col-xs-4">
                                    <div class="form-group">
                                        <label>Origem</label>
                                        <select class="form-control selectpicker" multiple>
                                            <option value="1">Option 1</option>
                                            <option value="2">Option 2</option>
                                            <option value="3">Option 3</option>
                                            <option value="4">Option 4</option>
                                            <option value="5">Option 5</option>
                                        </select>
                                    </div>
                                </div>   
                                <div class="col-md-3 col-sm-4 col-xs-4">
                                    <div class="form-group">
                                        <label>Valor da multa</label>                                        
                                        <input type="text" placeholder="Valor" class="form-control">                                        
                                    </div>
                                </div>


                                <div class="col-md-3 col-sm-4 col-xs-4">
                                    <div class="form-group">
                                        <label>Quant. nota</label>                                        
                                        <input type="text" placeholder="Quant nota" class="form-control">                                        
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-default" type="button">Cancelar</button>
                        <button class="btn btn-primary" type="button">Salvar</button>
                    </div>                
                </div>
            </div>
        </div>        

        <!-- javascripts -->
        <script type="text/javascript" src="js/plugins/jquery/jquery.min.js"></script>       
        <script type="text/javascript" src="js/plugins/bootstrap/bootstrap.min.js"></script>

        <script type="text/javascript" src="js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>
        <script type="text/javascript" src="js/plugins/moment/moment.js"></script>

        <script type="text/javascript" src="js/plugins/knob/jquery.knob.min.js"></script>
        <script type="text/javascript" src="js/plugins/sparkline/jquery.sparkline.min.js"></script>

        <script type="text/javascript" src="js/plugins/bootstrap-select/bootstrap-select.js"></script>
        <script type="text/javascript" src="js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js"></script>
        <script type="text/javascript" src="js/plugins/datatables/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="js/plugins/smartwizard/jquery.smartWizard.js"></script>
        <script type="text/javascript" src="js/plugins/sortable/sortable.min.js"></script>

        <script type="text/javascript" src="js/dev-loaders.js"></script>
        <script type="text/javascript" src="js/dev-layout-default.js"></script>
        <script type="text/javascript" src="js/dev-app.js"></script>
        <!-- ./javascripts -->


    </body>
</html>
