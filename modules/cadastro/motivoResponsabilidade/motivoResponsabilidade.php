<!DOCTYPE html>
<?php
include_once( "../../../includes/Dao/DaoSistema.class.php" );
include_once( "../../../includes/Dao/MotivoResponsabilidadeDao.class.php" );
include_once( "../../../includes/DevMotivoResponsabilidade.class.php" );


$DevMotivoResponsabilidade = new DevMotivoResponsabilidade();

$MotivoResponsabilidadeDao = new MotivoResponsabilidadeDao();
$result = $MotivoResponsabilidadeDao->consultar($DevMotivoResponsabilidade);

//$MotivoReprovaDao = new MotivoReprovaDao();
//$MotivosReprova = $MotivoReprovaDao->consultar();
//    if ($MotivosReprova[0]) {
?>
<html lang="en">
    <head>        
        <!-- meta section -->
        <title>Devolução - Motivo responsabilidade</title>
        <?php include("../../../library/head.php"); ?>
    </head>
    <body>
        <!-- set loading layer -->
        <div class="dev-page-loading preloader"></div>
        <!-- ./set loading layer -->

        <!-- page wrapper -->
        <div class="dev-page">

            <!-- page header -->    
            <?php include("../../../library/topo.php"); ?>
            <!-- ./page header -->

            <!-- page container -->
            <div class="dev-page-container">

                <!-- page sidebar -->
                <?php include("../../../library/menu.php"); ?>
                <!-- ./page sidebar -->

                <!-- page content -->
                <div class="dev-page-content">                    
                    <!-- page content container -->
                    <div class="container">

                        <!-- page title -->
                        <div class="page-title">
                            <h1>Motivo responsabilidade</h1>

                            <ul class="breadcrumb">
                                <li><a href="#">Cadastro</a></li>
                                <li>Motivo responsabilidade/li>
                            </ul>
                        </div>                        
                        <!-- ./page title -->



                        <!-- datatables plugin -->
                        <div class="wrapper wrapper-white">
                            <div class="form-group">
                                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal_wizard">Cadastrar motivo</button>
                            </div>

                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-sortable">
                                    <thead>
                                        <tr>
                                            <th>Descrição</th>
                                            <th>Responsável</th>
                                            <th>Status</th>
                                            <th></th>
                                        </tr>
                                    </thead>                               
                                    <tbody>
                                        <?php
                                        while ($dados = oci_fetch_object($result)) {
                                        ?>  
                                            <tr>
                                                <td><?php echo $dados->TRP_NO_DESCRICAO; ?></td>
                                                <td><?php echo $dados->TRP_NO_RESPONSAVEL; ?></td>
                                                <?php
                                                if ($dados->TRP_IN_STATUS == 'E') {
                                                ?>        
                                                    <td><i class="fa fa-exclamation-triangle" title="Inativo"></i>  Inativo</td>
                                                <?php
                                                } else {
                                                ?>
                                                    <td><i class="fa fa-check-square" title="Ativo"></i>  Ativo</td>
                                               <?php
                                                }
                                                ?>
                                                <td><button data-target="#modal_wizard" data-toggle="modal" class="fa fa-edit" type="button"></button></td>
                                            </tr>
                                        <?php
                                        }
                                        ?>  

                                    </tbody>
                                </table>
                            </div>

                        </div>                        
                        <!-- ./datatables plugin -->


                        <!-- Copyright -->
                        <?php include("../../../library/copyright.php"); ?>
                        <!-- ./Copyright -->

                    </div>
                    <!-- ./page content container -->                                       
                </div>
                <!-- ./page content -->                                                
            </div>  
            <!-- ./page container -->

            <!-- right bar -->

            <!-- ./right bar -->            

            <!-- page footer -->    
            <?php include("../../../library/footer.php"); ?>
            <!-- ./page footer -->

            <!-- page search -->

            <!-- page search -->            
        </div>
        <!-- ./page wrapper -->



        <div class="modal fade" id="modal_wizard" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Cadastro de motivo</h4>
                    </div>
                    <div class="modal-body">
                        <div id="step-6">
                            <div class="row">
                                <div class="col-md-8 col-sm-8 col-xs-8">
                                    <div class="form-group">
                                        <label>Descrição</label>                                        
                                        <input type="text" placeholder="Descrição" class="form-control">                                        
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-4">                        
                                    <div class="form-group">
                                        <label>Responsável</label>
                                        <select class="form-control selectpicker">
                                            <option value="1">Administrativo</option>
                                            <option value="2">Comercial</option>
                                            <option value="3">Diretoria</option>
                                            <option value="4">Indústria</option>
                                            <option value="5">Logística</option>
                                        </select>
                                    </div>                        
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-default" type="button">Cancelar</button>
                        <button class="btn btn-primary" type="button">Salvar</button>
                    </div>                
                </div>
            </div>
        </div>        

        <!-- javascripts -->
        <?php include("../../../library/rodape.php"); ?>

        <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js"></script>
        <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/plugins/datatables/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/plugins/smartwizard/jquery.smartWizard.js"></script>
        <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/plugins/sortable/sortable.min.js"></script>
        <!-- ./javascripts -->


    </body>
</html>