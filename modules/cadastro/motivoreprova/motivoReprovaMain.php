<?php
include_once( "../../../includes/Dao/DaoSistema.class.php" );
include_once( "../../../includes/Dao/MotivoReprovaDao.class.php" );
include_once( "../../../includes/MotivoReprova.class.php" );


if ($_REQUEST['acao'] == 'DEL') {
    $MotivoReprovaDao = new MotivoReprovaDao();
    $MotivoReprova = new MotivoReprova();
    $MotivoReprova->setCodReprova($_REQUEST['codigo']);
    if ($MotivoReprovaDao->excluir($MotivoReprova)) {
        echo "Motivo de Reprova excluido com sucesso!";
    } else {
        echo "Falha ao excluir!";
    }
} else if($_REQUEST['acao'] == 'CAD') {
    $MotivoReprovaDao = new MotivoReprovaDao();
    $MotivoReprova = new MotivoReprova();
    $MotivoReprova->setDescricao($_REQUEST['descricao']);
    if ($MotivoReprovaDao->inserir($MotivoReprova)) {
        echo "Motivo de Reprova cadastrado com sucesso!";
    } else {
        echo "Falha ao cadastrar!";
    }
}