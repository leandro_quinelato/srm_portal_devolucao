<!DOCTYPE html>

<?php
include_once( "../../../includes/Dao/DaoSistema.class.php" );
include_once( "../../../includes/Dao/MotivoReprovaDao.class.php" );
include_once( "../../../includes/MotivoReprova.class.php" );

$MotivoReprovaDao = new MotivoReprovaDao();
$MotivosReprova = $MotivoReprovaDao->consultar();
//    if ($MotivosReprova[0]) {
?>
<html lang="en">
    <head>        
        <!-- meta section -->
        <title>Devolução - Motivo de reprova</title>
        <?php include("../../../library/head.php"); ?>
    </head>
    <body>
        <!-- set loading layer -->
        <div class="dev-page-loading preloader"></div>
        <!-- ./set loading layer -->
        
        <!-- page wrapper -->
        <div class="dev-page">
            
            <!-- page header -->    
            <?php include("../../../library/topo.php"); ?>
            <!-- ./page header -->
            
            <!-- page container -->
            <div class="dev-page-container">

                <!-- page sidebar -->
                <?php include("../../../library/menu.php"); ?>
                <!-- ./page sidebar -->
                
                <!-- page content -->
                <div class="dev-page-content">                    
                    <!-- page content container -->
                    <div class="container">
                        
                        <!-- page title -->
                        <div class="page-title">
                            <h1>Motivo de reprova</h1>
                            
                            <ul class="breadcrumb">
                                <li><a href="#">Cadastro</a></li>
                                <li>Motivo de reprova</li>
                            </ul>
                        </div>                        
                        <!-- ./page title -->
                        

                        
                        <!-- datatables plugin -->
                        <div class="wrapper wrapper-white">
                            <div class="form-group">
                                <!--<button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal_wizard">Cadastrar reprova</button>-->
                                <button type="button" class="btn btn-success" onclick="javascript: carregaMotivoReprova();">Cadastrar reprova</button>
                            </div>
                            <div class="table-responsive">
                            <?php
                            if ($MotivosReprova[0]) {  
                            ?>
                                <table class="table table-bordered table-striped table-sortable">
                                    <thead>
                                        <tr>
                                            <th>Código</th>
                                            <th>Descrição</th>
                                            <th>Data cadastro</th>
                                            <th></th>
                                        </tr>
                                    </thead>                               
                                    <tbody>
                                        <?php
                                        foreach ($MotivosReprova as $motivoReprova) {
                                        ?>    
                                        
                                        <tr>
                                            <td><?php echo $motivoReprova->getCodReprova(); ?></td>
                                            <td><?php echo $motivoReprova->getDescricao(); ?></td>
                                            <td><?php echo $motivoReprova->getDataCadastro(); ?></td>
                                            <td><button title="Inativar" class="fa fa-minus-square" type="button" onclick="excluirMotivoReprova(<?php echo $motivoReprova->getCodReprova(); ?>)"></button></td>
                                        </tr>
                                    <?php
                                        } 
                                    ?>
                                    </tbody>

                                </table>
                            <?php
                            } 
                            ?>
                            </div>
                            
                        </div>                        
                        <!-- ./datatables plugin -->
                        
                        
                        <!-- Copyright -->
                        <?php include("../../../library/copyright.php"); ?>
                        <!-- ./Copyright -->
                        
                    </div>
                    <!-- ./page content container -->                                       
                </div>
                <!-- ./page content -->                                                
            </div>  
            <!-- ./page container -->
            
            <!-- right bar 
            <div class="dev-page-rightbar">
                <div class="rightbar-chat">

                    <div class="rightbar-chat-frame-contacts scroll">
                        <div class="rightbar-title">
                            <h3>Messages</h3>
                            <a href="#" class="btn btn-default btn-rounded rightbar-close pull-right"><span class="fa fa-times"></span></a>
                        </div>
                        <ul class="contacts">
                            <li class="title">online</li>
                            <li>
                                <a href="#" class="status online">
                                    <img src="assets/images/users/user_1.jpg" title="Aqvatarius"> John Doe                                            
                                </a>
                            </li>                                    
                            <li>
                                <a href="#" class="status online">
                                    <img src="assets/images/users/user_2.jpg" title="Aqvatarius"> Shannon Freeman                                            
                                </a>
                            </li>
                            <li>
                                <a href="#" class="status away">
                                    <img src="assets/images/users/user_3.jpg" title="Aqvatarius"> Devin Stephens                                            
                                </a>
                            </li>                                    
                            <li>
                                <a href="#" class="status away">
                                    <img src="assets/images/users/user_4.jpg" title="Aqvatarius"> Marissa George                                           
                                </a>
                            </li>
                            <li>
                                <a href="#" class="status dont">
                                    <img src="assets/images/users/user_5.jpg" title="Aqvatarius"> Sydney Reeves                                           
                                </a>
                            </li>
                            <li class="title">offline</li>
                            <li>
                                <a href="#" class="status">
                                    <img src="assets/images/users/user_6.jpg" title="Aqvatarius"> Kaitlynn Bowen                                           
                                </a>
                            </li>
                            <li>
                                <a href="#" class="status">
                                    <img src="assets/images/users/user_7.jpg" title="Aqvatarius"> Karen Spencer                                            
                                </a>
                            </li>
                            <li>
                                <a href="#" class="status">
                                    <img src="assets/images/users/user_8.jpg" title="Aqvatarius"> Darrell Wolfe                                            
                                </a>
                            </li>                                    
                        </ul>
                    </div>

                    <div class="rightbar-chat-frame-chat">
                        <div class="user">
                            <div class="user-panel">
                                <a href="#" class="btn btn-default btn-rounded rightbar-chat-close"><span class="fa fa-angle-left"></span></a>
                                <a href="#" class="btn btn-default btn-rounded pull-right"><span class="fa fa-user"></span></a>
                            </div>
                            <div class="user-info">
                                <div class="user-info-image status online">
                                    <img src="assets/images/users/user_1.jpg">
                                </div>
                                <h5>Devin Stephens</h5>
                                <span>UI/UX Designer</span>
                            </div>
                        </div>
                        <div class="chat-wrapper scroll">
                            <ul class="chat" id="rightbar_chat">
                                <li class="inbox">
                                    Hi, you have a second? Need to ask you something.
                                    <span>about 1h ago</span>
                                </li>                                    
                                <li class="sent">
                                    Sure i have...
                                    <span>59min ago</span>
                                </li>
                                <li class="inbox">
                                    It's about latest design you did...
                                    <span>14min ago</span>
                                </li>
                                <li class="sent">
                                    I will do my best to help you
                                    <span>2min ago</span>
                                </li>
                            </ul>
                        </div>

                        <form class="form" action="#" method="post" id="rightbar_chat_form">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-btn">
                                        <button class="btn btn-default"><i class="fa fa-paperclip"></i></button>
                                    </div>
                                    <input type="text" class="form-control" name="message">
                                    <div class="input-group-btn">
                                        <button type="submit" class="btn btn-default">Send</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    
                </div>
            </div>
            <!-- ./right bar -->            
            
            <!-- page footer -->    
            <?php include("../../../library/footer.php"); ?>
            <!-- ./page footer -->
            
            <!-- page search -->

            <!-- page search -->            
        </div>
        <!-- ./page wrapper -->
        
        
        <!-- Inicio Modal Editar -->
        <div class="modal fade" id="modal_wizard" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

        </div>        
        <!-- Fim Modal Editar -->
        
        
        <!-- javascripts -->
        <?php include("../../../library/rodape.php"); ?>

        <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js"></script>
        <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/plugins/datatables/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/plugins/smartwizard/jquery.smartWizard.js"></script>
        <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/plugins/sortable/sortable.min.js"></script>
        <!-- ./javascripts -->
        
        
    </body>
</html>