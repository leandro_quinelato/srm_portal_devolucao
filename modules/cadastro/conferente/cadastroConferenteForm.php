<?php
include_once( "../../../includes/Dao/DaoSistema.class.php" );
include_once( "../../../includes/Dao/ConferenteDao.class.php" );
include_once( "../../../includes/Conferente.class.php" );

$ConferenteDao = new ConferenteDao();
$empresa = $ConferenteDao->consultarEmpresa();
$empresaCad = $empresa;
$empresaPadrao = 1;
//    if ($MotivosReprova[0]) {
?>

<script>
    $( function(){
       
        loadEstabelecimento(<?php echo $empresaPadrao; ?>); 
       
    });
</script>
    
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Cadastro de Conferente</h4>
        </div>
        <div class="modal-body">
            <form id="formConferente">

                <div id="step-6">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Empresa</label>                           
                                <!--<input type="checkbox" checked="" id="check_1">-->
                                <select name="empresaCad" id="empresaCad" aria-controls="DataTables_Table_0" class="form-control" onchange="loadEstabelecimento($('#empresaCad').val())">
                                    <?php
                                    while (OCIFetchInto($empresa, $row, OCI_ASSOC)) {
                                        ?>             
                                        <option value="<?php echo $row['EMP_CO_NUMERO']; ?>" <?php echo ($row['EMP_CO_NUMERO'] == $empresaPadrao ) ? 'selected="selected"' : '' ?> > <?php echo $row['EMP_NO_APELIDO'] ?> </option>
                                        <?php
                                    }
                                    ?>  
                                </select>

                                <!--<label >Regra de aprovação</label>-->
                            </div> 
                        </div>
                        <div id="areaEstabelecimento">

                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Crachá</label>                                        
                                <input type="text" placeholder="Crachá" class="form-control" name="cracha" id="cracha" onKeyPress="fMascara( 'numero', event, this )" onChange="buscarNomeColaborador($('#empresaCad').val(), $('#estabConf').val(), $('#cracha').val() )">                                        

                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Colaborador</label>                                        
                                <input type="text" placeholder="Crachá" class="form-control" name="col_no_colaborador" id="col_no_colaborador" readonly>                                        

                            </div>
                        </div>

                    </div>
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <button data-dismiss="modal" class="btn btn-default" type="button">Cancelar</button>
            <button class="btn btn-primary" type="button" onclick="javascript: cadastrarConferente();" >Salvar</button>
        </div>                
    </div>
</div>