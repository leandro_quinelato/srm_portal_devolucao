<?php
include_once( "../../../includes/Dao/DaoSistema.class.php" );
include_once( "../../../includes/Dao/ConferenteDao.class.php" );
include_once( "../../../includes/Conferente.class.php" );

extract($_REQUEST);

$conferenteDao = new ConferenteDao();

if ($acao != 'NOME_COL') {

    $estabelecimento = $conferenteDao->consultarEstab($emp_co_numero)
    ?>

    <div class="col-md-6">
        <div class="form-group">
            <label>Estabelecimento</label>                           
            <!--<input type="checkbox" checked="" id="check_1">-->
            <select name="estabConf" id="estabConf" aria-controls="DataTables_Table_0" class="form-control">
                                                    <option value="TOD">Selecionar</option>
                <?php
                while (OCIFetchInto($estabelecimento, $row, OCI_ASSOC)) {
                    ?>             
                    <option value="<?php echo $row['ETB_CO_NUMERO']; ?>" > <?php echo $row['ETB_CO_NUMERO'] . ' - ' . $row['ETB_NO_APELIDO'] ?> </option>
                    <?php
                }
                ?>  
            </select>

            <!--<label >Regra de aprovação</label>-->
        </div> 
    </div>
    <?php
} else {

    $resultCol = $conferenteDao->consultarColaborador($empresa, $estabelecimento, $cracha);

    OCIFetchInto($resultCol, $col, OCI_ASSOC);
    if ((!is_null($col["COL_NO_COLABORADOR"]) && is_null($col["COL_DT_DEMISSAO"]))) {
        echo $col["COL_NO_COLABORADOR"];
    } else {
        echo "ERRO";
    }
}
