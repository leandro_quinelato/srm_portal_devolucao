<!DOCTYPE html>

<?php
include_once( "../../../includes/Dao/DaoSistema.class.php" );
include_once( "../../../includes/Dao/ConferenteDao.class.php" );
include_once( "../../../includes/Conferente.class.php" );

$ConferenteDao = new ConferenteDao();
$ConferenteResult = $ConferenteDao->consultarConferentes();
//    if ($MotivosReprova[0]) {
?>
<html lang="en">
    <head>        
        <!-- meta section -->
        <title>Devolução - Conferentes</title>
        <?php include("../../../library/head.php"); ?>
    </head>
    <body>
        <!-- set loading layer -->
        <div class="dev-page-loading preloader"></div>
        <!-- ./set loading layer -->

        <!-- page wrapper -->
        <div class="dev-page">

            <!-- page header -->    
            <?php include("../../../library/topo.php"); ?>
            <!-- ./page header -->

            <!-- page container -->
            <div class="dev-page-container">

                <!-- page sidebar -->
                <?php include("../../../library/menu.php"); ?>
                <!-- ./page sidebar -->

                <!-- page content -->
                <div class="dev-page-content">                    
                    <!-- page content container -->
                    <div class="container">

                        <!-- page title -->
                        <div class="page-title">
                            <h1>Conferentes</h1>

                            <ul class="breadcrumb">
                                <li><a href="#">Cadastro</a></li>
                                <li>Conferentes</li>
                            </ul>
                        </div>                        
                        <!-- ./page title -->



                        <!-- datatables plugin -->
                        <div class="wrapper wrapper-white">
                            <div class="form-group">
                                <!--<button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal_wizard">Cadastrar reprova</button>-->
                                <button type="button" class="btn btn-success" onclick="javascript: carregaCadConferente();">Cadastrar Conferente</button>
                            </div>
                            <div id="listaConferentes">
                                <div class="table-responsive">

                                    <table class="table table-bordered table-striped table-sortable">
                                        <thead>
                                            <tr>
                                                <th>Empresa</th>
                                                <th>Estabelecimento</th>
                                                <th>Crach&aacute;</th>
                                                <th>Colaborador</th>
                                                <th>Status</th>	
                                            </tr>
                                        </thead>                               
                                        <tbody>
                                            <?php
                                            while ($dados = oci_fetch_object($ConferenteResult)) {
                                                ?>    

                                                <tr>
                                                    <td><?php echo $dados->EMP_NO_APELIDO; ?></td>
                                                    <td><?php echo $dados->ETB_NO_APELIDO ?></td>
                                                    <td><?php echo $dados->COL_CO_CRACHA ?></td>
                                                    <td><?php echo $dados->COL_NO_COLABORADOR; ?></td>
                                                    <td><?php echo $dados->COL_DT_DEMISSAO ? 'Inativo' : 'Ativo' ?></td>
                                                </tr>
                                                <?php
                                            }
                                            ?>
                                        </tbody>

                                    </table>
                                </div>
                            </div>
                        </div>                        
                        <!-- ./datatables plugin -->


                        <!-- Copyright -->
                        <?php include("../../../library/copyright.php"); ?>
                        <!-- ./Copyright -->

                    </div>
                    <!-- ./page content container -->                                       
                </div>
                <!-- ./page content -->                                                
            </div>  
            <!-- ./page container -->

            <!-- right bar 

            <!-- ./right bar -->            

            <!-- page footer -->    
            <?php include("../../../library/footer.php"); ?>
            <!-- ./page footer -->

            <!-- page search -->

            <!-- page search -->            
        </div>
        <!-- ./page wrapper -->


        <!-- Inicio Modal Editar -->
        <div class="modal fade" id="modal_conferente" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

        </div>        
        <!-- Fim Modal Editar -->


        <!-- javascripts -->
        <?php include("../../../library/rodape.php"); ?>

        <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js"></script>
        <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/plugins/datatables/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/plugins/smartwizard/jquery.smartWizard.js"></script>
        <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/plugins/sortable/sortable.min.js"></script>
        <!-- ./javascripts -->


    </body>
</html>