<?php
include_once( "../../../includes/Dao/DaoSistema.class.php" );
include_once( "../../../includes/Dao/ConferenteDao.class.php" );
include_once( "../../../includes/Conferente.class.php" );

extract($_REQUEST);

//echo "acao=". $acao;

$conferenteDao = new ConferenteDao();

if ($acao == "INCLUIR") {
    $resultConf = $conferenteDao->consultar($empresaCad, $estabConf, $cracha);

    OCIFetchInto($resultConf, $existeConf, OCI_ASSOC);

    if (is_null($existeConf["COL_CO_CRACHA"])) {
        if ($conferenteDao->adicionar($empresaCad, $estabConf, $cracha)) {
            echo "Conferente cadastrado com sucesso!";
        } else {
            echo "Erro ao Cadastrar!";
        }
    } else {
        echo "Conferente já está cadastrado!";
    }
} else if ($acao == "LISTAR") {

    $ConferenteDao = new ConferenteDao();
    $ConferenteResult = $ConferenteDao->consultarConferentes();
    ?>

    <div class="table-responsive">

        <table class="table table-bordered table-striped table-sortable">
            <thead>
                <tr>
                    <th>Empresa</th>
                    <th>Estabelecimento</th>
                    <th>Crach&aacute;</th>
                    <th>Colaborador</th>
                    <th>Status</th>	
                </tr>
            </thead>                               
            <tbody>
                <?php
                while ($dados = oci_fetch_object($ConferenteResult)) {
                    ?>    

                    <tr>
                        <td><?php echo $dados->EMP_NO_APELIDO; ?></td>
                        <td><?php echo $dados->ETB_NO_APELIDO ?></td>
                        <td><?php echo $dados->COL_CO_CRACHA ?></td>
                        <td><?php echo $dados->COL_NO_COLABORADOR; ?></td>
                        <td><?php echo $dados->COL_DT_DEMISSAO ? 'Inativo' : 'Ativo' ?></td>
                    </tr>
                    <?php
                }
                ?>
            </tbody>

        </table>
    </div>
<script language="JavaScript">
    $(function () {
        datatables.init();
    });
</script>

    <?php
}