<!DOCTYPE html>

<?php
include_once( "../../../includes/Dao/DaoSistema.class.php" );
include_once( "../../../includes/Dao/UsuarioTransporteDao.class.php" );

include_once("DaoGlobal.class.php");
include_once("SistemaDao.class.php");
include_once("GlbSistema.class.php");
include_once("GlbSistemaPerfil.class.php");
include_once("GlbUsuario.class.php");
include_once("GlbColaborador.class.php");
include_once("GlbPessoa.class.php");
include_once("GlbEndereco.class.php");
include_once("GlbTelefone.class.php");
include_once("include_novo/GlbCentroDistribuicao.class.php");
include_once("include_novo/GlbTransportadora.class.php");
include_once("include_novo/GlbContatoTransportador.class.php");
include_once("include_novo/ContatoTransportadorDao.class.php");
include_once("GlbProgramaRotina.class.php");
include_once("GlbModuloPrograma.class.php");
include_once("GlbSistemaModulo.class.php");
include_once("PerfilUsuarioDao.class.php");
include_once("PerfilPermissaoDao.class.php");
include_once("integraSetSistemas.php");
$integra = new integraSetSistemas("conf.xml");
$ObjUsuario = unserialize($integra->getSistemaIntegra());
$ObjRotinas = $ObjUsuario->getObjSistemaPerfil()->getObjRotina();

$UsuarioTransporteDao = new UsuarioTransporteDao();
$usuarios = $UsuarioTransporteDao->usuarioTransportador();
//    if ($MotivosReprova[0]) {
?>

<html lang="en">
    <head>        
        <!-- meta section -->
        <title>Cadastro - Usuários Rotas</title>
        <?php include("../../../library/head.php"); ?>
    </head>
    <body>
        <!-- set loading layer -->
        <div class="dev-page-loading preloader"></div>
        <!-- ./set loading layer -->

        <!-- page wrapper -->
        <div class="dev-page">

            <!-- page header -->    
            <?php include("../../../library/topo.php"); ?>
            <!-- ./page header -->

            <!-- page container -->
            <div class="dev-page-container">

                <!-- page sidebar -->
                <?php include("../../../library/menu.php"); ?>
                <!-- ./page sidebar -->

                <!-- page content -->
                <div class="dev-page-content">                    
                    <!-- page content container -->
                    <div class="container">

                        <!-- page title -->
                        <div class="page-title">
                            <h1>Transportadores</h1>

                            <ul class="breadcrumb">
                                <li><a href="#">Cadastro</a></li>
                                <li>Transportadores</li>
                            </ul>
                        </div>                        
                        <!-- ./page title -->



                        <!-- datatables plugin -->
                        <div class="wrapper wrapper-white">
                            <div class="form-group">
							<?php
							    if($ObjUsuario->getUsuarioTipo() != "TRA"){
							?>
                                <button type="button" class="btn btn-success" onclick="javascript: cadastroRotaUsuario('');">Cadastrar Usuário Transportador</button>
							<?php
                            }
                            ?>
                            </div>						
                            <div id="listaUsuarioTrans">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped table-sortable">
                                        <thead>
                                            <tr>
                                                <th>Cód. Usuário</th>
                                                <th>Nome</th>
                                                <th>Rotas</th>
                                                <th>Status</th> 
                                                <th></th>
                                        
                                            </tr>
                                        </thead>                               
                                        <tbody>
                                            <?php
                                            while ($usuario = oci_fetch_object($usuarios)) {
                                                if(($ObjUsuario->getUsuarioTipo() == "TRA" && ($usuario->USU_CO_NUMERO == $ObjUsuario->getUsuarioCodigo()) ) ||  $ObjUsuario->getUsuarioTipo() != "TRA" ) {
                                                ?>
                                                <tr>
                                                    <td><?php echo $usuario->USU_CO_NUMERO; ?></td>
                                                    <td><?php echo $usuario->USU_NO_USERNAME; ?></td>
                                                    
                                                    
                                                    
                                                    <td>
                                                        <?php
                                                            $rotasUsuarios = $UsuarioTransporteDao->consultaUsuRota($usuario->USU_CO_NUMERO);
                                                            $auxRotas = "";
                                                            while ($rotaUsuario = oci_fetch_object($rotasUsuarios)) {
                                                                if($auxRotas){
                                                                    $auxRotas .= " / ". $rotaUsuario->ROT_CO_NUMERO ; 
                                                                }else{
                                                                    $auxRotas = $rotaUsuario->ROT_CO_NUMERO;
                                                                }
                                                                
                                                            }
                                                        ?>
                                                       
                                                        <?php
                                                            echo $auxRotas;
                                                        ?>
                                                    </td>
                                                    

                                                    <?php
                                                    if ($usuario->USU_IN_STATUS) {
                                                        ?>   

                                                        <td><i class="fa fa-exclamation-triangle" title="Inativo"></i></td>
                                                        <?php
                                                    } else {
                                                        ?>
                                                        <td><i class="fa fa-check-square" title="Ativo"></i></td>
                                                        <?php
                                                    }
                                                    ?>
                                                        <td><button class="fa fa-edit" type="button" onclick="javascript: cadastroRotaUsuario(<?php echo $usuario->USU_CO_NUMERO; ?>)"></button></td>
                                                </tr>
                                                <?php
                                                }
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>        


                        </div>                        
                        <!-- ./datatables plugin -->


                        <!-- Copyright -->
                        <?php include("../../../library/copyright.php"); ?>
                        <!-- ./Copyright -->

                    </div>
                    <!-- ./page content container -->                                       
                </div>
                <!-- ./page content -->                                                
            </div>  
            <!-- ./page container -->    

            <!-- page footer -->    
            <?php include("../../../library/footer.php"); ?>
            <!-- ./page footer -->

            <!-- page search -->

            <!-- page search -->            
        </div>
        <!-- ./page wrapper -->



        <div class="modal fade" id="modalRotas" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

        </div>        

        <!-- javascript -->
        <?php include("../../../library/rodape.php"); ?>

        <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js"></script>
        <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/plugins/datatables/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/plugins/smartwizard/jquery.smartWizard.js"></script>
        <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/plugins/sortable/sortable.min.js"></script>
        <!-- ./javascript -->


    </body>
</html>