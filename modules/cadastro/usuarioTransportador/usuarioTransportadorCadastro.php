<!DOCTYPE html>

<?php
include_once( "../../../includes/Dao/DaoSistema.class.php" );
include_once( "../../../includes/Dao/UsuarioTransporteDao.class.php" );

include_once("Dao.class.php");
include_once("DaoGlobal.class.php");
include_once("GlbUsuario.class.php");
include_once("GlbSistemaPerfil.class.php");
include_once("GlbSistema.class.php");
include_once("UsuarioDao.class.php");
include_once("GlbColaborador.class.php");
include_once("ColaboradorDao.class.php");
include_once("GlbCliente.class.php");	
include_once("ClienteDao.class.php");	
include_once("GlbContato.class.php");
include_once("ContatoDao.class.php");
include_once("GlbPessoa.class.php");
include_once("GlbEndereco.class.php");
include_once("GlbTelefone.class.php");
include_once("include_novo/GlbTransportadora.class.php");
include_once("include_novo/GlbContatoTransportador.class.php");
include_once("include_novo/ContatoTransportadorDao.class.php");
include_once("PerfilUsuarioDao.class.php" );
include_once("PerfilPermissaoDao.class.php" );
include_once("VendedorDao.class.php");

include_once("integraSetSistemas.php");
$integra = new integraSetSistemas("conf.xml");
$ObjUsuario = unserialize($integra->getSistemaIntegra());
$ObjRotinas = $ObjUsuario->getObjSistemaPerfil()->getObjRotina();

//echo "<pre>";
//print_r($_REQUEST);
//echo "</pre>";

extract($_REQUEST);

if($codUsuario != ""){
	$Usuario = new GlbUsuario();
	$Usuario->setUsuarioCodigo($codUsuario);
	
	$UsuarioDao = new UsuarioDao();
	$resUsuario = $UsuarioDao->consultar($Usuario);
	if ($dadosUsuario = oci_fetch_object($resUsuario)){
		$Usuario->setUsuarioTipo($dadosUsuario->USU_IN_TIPO);	
		$Usuario->setUsuarioUsername($dadosUsuario->USU_NO_USERNAME);
		$Usuario->setUsuarioStatus($dadosUsuario->USU_IN_STATUS);
		
		$ContatoTransportadorDao = new ContatoTransportadorDao();
		$Usuario = $ContatoTransportadorDao->preencherUsuarioContatoTrans($Usuario);
		
		$cnpj = $Usuario->getObjTransportadora()->getCnpj();
		$ObjContato = $Usuario->getObjTransportadora()->getObjContato();
		$nome = $ObjContato[0]->getPessoaNome();
		$email = $ObjContato[0]->getPessoaEmail();
		$login = $Usuario->getUsuarioUsername();
		$status = $Usuario->getUsuarioStatus();
		
		
	}else{
		echo "Usuário não encontrado.";
	}
	
}else{
	$cnpj = "";
	$nome = "";
	$email = "";
	$login = "";
	$status = "";
}


?>
<div id="retornoTeste"></div>
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Editar usuário</h4>
        </div>
			
        <div class="modal-body">
            <div id="step-6">
			
				<form id="formUsuarioTransportador">
					<input type="hidden" name="codUsuario" id="codUsuario" value="<?php echo $codUsuario; ?>">
					
				<div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>CPNJ Transportadora</label>
							<?if($codUsuario != ""){?>
							</br><label><?=$cnpj?></label>
							<input type="hidden" class="form-control" id="cnpj" name="cnpj" value="<?=$cnpj?>">
							<?}else{?>
							<input type="text" class="form-control" id="cnpj" name="cnpj" value="<?=$cnpj?>">
							<?}?>							
                        </div>
                    </div>
				</div>	
				<div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Nome</label>
							<input type="text" class="form-control" id="nome" name="nome" value="<?=$nome?>">
                        </div>
                    </div>
				</div>				
				<div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Email</label>
							<input type="text" class="form-control" id="email" name="email" value="<?=$email?>">
                        </div>
                    </div>
				</div>
				<div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Login</label>
							<?if($codUsuario != ""){?>
							</br><label><?=$login?></label>
							<input type="hidden" class="form-control" id="login" name="login" value="<?=$login?>">
							<?}else{?>							
							<input type="text" class="form-control" id="login" name="login" value="<?=$login?>">
							<?}?>
                        </div>
                    </div>
				</div>
				<div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Senha</label>
							<input type="password" class="form-control" id="senha" name="senha" value="<?=$senha?>">
                        </div>
                    </div>
				</div>	
				<div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Senha Confirmação</label>
							<input type="password" class="form-control" id="senhaConfirmacao" name="senhaConfirmacao" value="<?=$senha?>">
                        </div>
                    </div>
				</div>					
				<div class="row">
					<div class="col-md-6">
                        <div class="form-group">
                            <label>Status<?=$status?></label>
                            <br>                          
							<select name="status">
								<option value="" <?=($status==''?"selected":"");?> >Ativo</option>
								<option value="E" <?=($status=='E'?"selected":"");?> >Inativo</option>
							</select>
                        </div>
					</div>
				</div>
				<?php
					if($codUsuario != "" and $ObjUsuario->getUsuarioTipo() != "TRA"){
						$UsuarioTransporteDao = new UsuarioTransporteDao();
						$rotasDisponiveis = $UsuarioTransporteDao->consultaRotasCadastro($codUsuario);
						$rotasDisponiveisFiltro = $UsuarioTransporteDao->consultaRotasCadastro($codUsuario);
						$rotasUsuario = $UsuarioTransporteDao->consultaUsuRota($codUsuario);												
				?>
				</form>								
                <div class="row">					
                    <div id="AreaRotas">
						<div class="row">	
							<div class="col-md-2 col-sm-6 col-xs-6">                        
								<div class="form-group">
									<label>Rota Filtro</label>							
									<select class="form-control selectpicker" id="rot_co_numero" name="rot_co_numero">		
										<?php
											while ($rotaDisponivelUsua = oci_fetch_object($rotasDisponiveisFiltro)) {
										?>
											<option value="<?php echo $rotaDisponivelUsua->ROT_CO_NUMERO;  ?>">									
											<?php echo $rotaDisponivelUsua->ROT_CO_NUMERO;  ?>
										</option>
										<?php
											}
										?>
									</select>							
								</div>  											
							</div>		
							<div style="padding-top: 30px;">											
								<button class="btn btn-primary" type="button" onclick="javascript: incluirRotaUsuario(document.getElementById('rot_co_numero').value)">Vincular rota</button>				
							</div>
						</div>
						<div class="col-md-8 col-sm-8 col-xs-8">                        
							<div class="form-group">
								<label>Rotas</label>
								<br>
								<?php
								while ($rotaDisponivel = oci_fetch_object($rotasDisponiveis)) {
								?>
								<button class="btn btn-default BotRotas" title="[+] Vincular rota" onclick="javascript: incluirRotaUsuario('<?php echo $rotaDisponivel->ROT_CO_NUMERO;  ?>')"><?php echo $rotaDisponivel->ROT_CO_NUMERO; ?></button>
								<?php
								}
								?>
							</div>                        
						</div>
						<div class="col-md-4 col-sm-4 col-xs-4">                        
							<div class="form-group">
								<label>Rotas Vinculadas</label>
								<br>      
								<?php
								while ($rotaUsuario = oci_fetch_object($rotasUsuario)) {
								?>
								<button class="btn btn-warning BotRotas" title="[x] Excluir rota" onclick="javascript: excluirRotaUsuario('<?php echo $rotaUsuario->ROT_CO_NUMERO;  ?>')" ><?php echo $rotaUsuario->ROT_CO_NUMERO; ?></button>
								<?php
								}
								?>
							</div>                        
						</div>
					</div>
					<?php
					}
					?>
                </div>
            </div>
        </div>
		
        <div class="modal-footer">
            <button data-dismiss="modal" class="btn btn-default" type="button">Cancelar</button>
            <button class="btn btn-primary" type="button" onclick="javascript: gravarUsuarioTransportador();">Salvar</button>
        </div>                
    </div>
</div>