<?php
include_once( "../../../includes/Dao/DaoSistema.class.php" );
include_once( "../../../includes/Dao/UsuarioTransporteDao.class.php" );

include_once("Dao.class.php");
include_once("DaoGlobal.class.php");
include_once("GlbUsuario.class.php");
include_once("GlbSistemaPerfil.class.php");
include_once("GlbSistema.class.php");
include_once("UsuarioDao.class.php");
include_once("GlbColaborador.class.php");
include_once("ColaboradorDao.class.php");
include_once("GlbCliente.class.php");	
include_once("ClienteDao.class.php");	
include_once("GlbContato.class.php");
include_once("ContatoDao.class.php");
include_once("GlbPessoa.class.php");
include_once("GlbEndereco.class.php");
include_once("GlbTelefone.class.php");
include_once("include_novo/GlbTransportadora.class.php");
include_once("TransportadoraDao.class.php");
include_once("include_novo/GlbContatoTransportador.class.php");
include_once("include_novo/ContatoTransportadorDao.class.php");
include_once("PerfilUsuarioDao.class.php" );
include_once("PerfilPermissaoDao.class.php" );
include_once("VendedorDao.class.php");

//echo "<pre>";
//print_r($_REQUEST);
//echo "</pre>";

extract($_REQUEST);

$UsuarioTransporteDao = new UsuarioTransporteDao();


if ($acao == "INCLUIR") {

    if ($UsuarioTransporteDao->inserirUsuRota($codUsuario, $codRota)) {
        echo "sucesso!";
    } else {
        echo "ERRO";
    }
} else if ($acao == "LISTAR") {

    $rotasDisponiveis = $UsuarioTransporteDao->consultaRotasCadastro($codUsuario);
	$rotasDisponiveisFiltro = $UsuarioTransporteDao->consultaRotasCadastro($codUsuario);
    $rotasUsuario = $UsuarioTransporteDao->consultaUsuRota($codUsuario);
?>    
	<div class="row">	
		<div class="col-md-2 col-sm-6 col-xs-6">                        
			<div class="form-group">
				<label>Rota Filtro</label>							
				<select class="form-control selectpicker" id="rot_co_numero" name="rot_co_numero">		
					<?php
						while ($rotaDisponivelUsua = oci_fetch_object($rotasDisponiveisFiltro)) {
					?>
						<option value="<?php echo $rotaDisponivelUsua->ROT_CO_NUMERO;  ?>">									
						<?php echo $rotaDisponivelUsua->ROT_CO_NUMERO;  ?>
					</option>
					<?php
						}
					?>
				</select>							
			</div>  											
		</div>		
		<div style="padding-top: 30px;">											
			<button class="btn btn-primary" type="button" onclick="javascript: incluirRotaUsuario(document.getElementById('rot_co_numero').value)">Vincular rota</button>				
		</div>
	</div>
    <div class="col-md-8 col-sm-8 col-xs-8">                        
        <div class="form-group">
            <label>Rotas</label>
            <br>
            <?php
            while ($rotaDisponivel = oci_fetch_object($rotasDisponiveis)) {
                ?>
                <button class="btn btn-default BotRotas" title="[+] Vincular rota" onclick="javascript: incluirRotaUsuario('<?php echo $rotaDisponivel->ROT_CO_NUMERO; ?>')"><?php echo $rotaDisponivel->ROT_CO_NUMERO; ?></button>
                <?php
            }
            ?>
        </div>                        
    </div>
    <div class="col-md-4 col-sm-4 col-xs-4">                        
        <div class="form-group">
            <label>Rotas Vinculadas</label>
            <br>      
            <?php
            while ($rotaUsuario = oci_fetch_object($rotasUsuario)) {
                ?>
                <button class="btn btn-warning BotRotas" title="[x] Excluir rota" onclick="javascript: excluirRotaUsuario('<?php echo $rotaUsuario->ROT_CO_NUMERO; ?>')" ><?php echo $rotaUsuario->ROT_CO_NUMERO; ?></button>
                <?php
            }
            ?>
        </div>                        
    </div>


    <?php
} else if ($acao == "EXCLUIR") {
   
    if ($UsuarioTransporteDao->deletUsuarioRota($codUsuario, $codRota)) {
        echo "sucesso!";
    } else {
        echo "ERRO";
    }
    
	
} else if ($acao == "GRAVAR"){
	
	if($codUsuario == ""){
		
		$Usuario = new GlbUsuario();
		$Usuario->setUsuarioUsername($login);
		
		$UsuarioDao = new UsuarioDao();
		$ObjUsuario = $UsuarioDao->verificaUsername($Usuario);
		
		if($ObjUsuario->getUsuarioCodigo() > 0){ //testar username	
			echo "Login já existente!";
			return;
		}else{
			//testar cnpj transportador se exitente
			$ObjTransportadora = new GlbTransportadora();
			$ObjTransportadora->setCnpj($cnpj);
			
			$TransportadoraDao = new TransportadoraDao();
			$resTrans = $TransportadoraDao->consultar($ObjTransportadora);
			if ($dadosTrans = oci_fetch_object($resTrans)){			
				$codTrans = $dadosTrans->TRA_CO_NUMERO;
				$ObjTransportadora->setCodigo($codTrans);
						
				//gravar usuario
				$Usuario->setUsuarioPassword($senha);
				$Usuario->setUsuarioTipo('TRA');
				
				$codUser = $UsuarioDao->inserir($Usuario);
				$Usuario->setUsuarioCodigo($codUser);
				
				//cadastrar contato
				$ObjContatoTransportador = new GlbContatoTransportador();
				$ObjContatoTransportador->setContatoCodigoTransportador($codTrans);
				$ObjContatoTransportador->setPessoaEmail($email);
				$ObjContatoTransportador->setPessoaNome($nome);
				$ObjContatoTransportador->setUsuarioCodigo($codUser);
				$ObjContatoTransportador->setUsuarioTipo('TRA');
				
				$ObjTransportadora->setObjContato($ObjContatoTransportador);
				$ContatoTransportadorDao = new ContatoTransportadorDao();
				$ContatoTransportadorDao->inserirContato($ObjTransportadora);				
				
				
				//permissão ao sistema com perfil cliente			
				$ObjSistemaPerfil = new GlbSistemaPerfil();
				$ObjSistemaPerfil->setCodigoSistemaPerfil(81);
				$Usuario->setObjSistemaPerfil($ObjSistemaPerfil);
				
				$perfilUsuarioDao = new PerfilUsuarioDao();
				$perfilUsuarioDao->inserir($Usuario);
				
				
				//retorno
				echo "Usuário cadastrado com sucesso!";
			}else{
				echo "Transportadora não encontrada.";
			}
			
		}
		
	}else{		
		//alterar
		$Usuario = new GlbUsuario();
		$Usuario->setUsuarioCodigo($codUsuario);
		$Usuario->setUsuarioUsername($login);
		$Usuario->setUsuarioPassword($senha);
		$Usuario->setUsuarioStatus($status);
		
		$UsuarioDao = new UsuarioDao();
		if($UsuarioDao->alterar($Usuario)){
			$UsuarioDao->statusUsuario($Usuario);
			echo "Registro Atualizado!";
		}else{
			echo "ERRO, registro não alterado.";
		}
		
	}

}