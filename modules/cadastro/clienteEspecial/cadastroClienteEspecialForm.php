<?php
include_once( "../../../includes/Dao/DaoSistema.class.php" );
include_once( "../../../includes/Dao/ClienteEspecialDao.class.php" );

//    if ($MotivosReprova[0]) {
?>
    
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Cadastro de Cliente Especial</h4>
        </div>
        <div class="modal-body">
            <form id="formClienteEspecial">

                <div id="step-6">
                    <div class="row">
						<?
						if($_REQUEST["acao"] == "ADD"){
						?>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Cliente</label>                                        
                                <input type="text" placeholder="Código Cliente" class="form-control" name="codCliente" id="codCliente" onKeyPress="fMascara( 'numero', event, this )" >                                        
                            </div>
                        </div>
						<?
						}else if($_REQUEST["acao"] == "ADD_REDE" or $_REQUEST["acao"] == "EXCLUIR_REDE"){
						?>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Rede</label>                                        
                                <input type="text" placeholder="Código Rede" class="form-control" name="codRede" id="codRede" onKeyPress="fMascara( 'numero', event, this )" >
                            </div>
                        </div>
						<?
						}
						if($_REQUEST["acao"] == "ADD" or $_REQUEST["acao"] == "ADD_REDE"){
						?>	
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Dias Liberação de NF</label>
                                <input type="text" placeholder="Dias utéis" class="form-control" name="diasLiberacaoNF" id="diasLiberacaoNF" onKeyPress="fMascara( 'numero', event, this )" >
                            </div>
                        </div>
						<?
						}
						?>
                    </div>
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <button data-dismiss="modal" class="btn btn-default" type="button">Cancelar</button>
			<button class="btn btn-primary" type="button" onclick="javascript: cadastrarClienteEspecial('<?=$_REQUEST["acao"]?>');" ><?=($_REQUEST["acao"]=="EXCLUIR_REDE"?"Excluir":"Salvar");?></button>
        </div>                
    </div>
</div>