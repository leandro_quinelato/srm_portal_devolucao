<?php
include_once( "../../../includes/Dao/DaoSistema.class.php" );
include_once( "../../../includes/Dao/ClienteEspecialDao.class.php" );

include_once("GlbUsuario.class.php");
include_once("integraSetSistemas.php");
$integra = new integraSetSistemas("conf.xml");
$ObjUsuario = unserialize($integra->getSistemaIntegra());


$ClienteEspecialDao = new ClienteEspecialDao();

if ($_REQUEST['acao'] == "ADD_REDE") {
	if ($_REQUEST['codRede'] > 0) {
		if ($ClienteEspecialDao->excluirPorRede($_REQUEST['codRede'])) {
			if($ClienteEspecialDao->adicionarPorRede($_REQUEST['codRede'],$ObjUsuario->getUsuarioCodigo(), $_REQUEST['diasLiberacaoNF'])){
				echo "Clientes Especials cadastrados com sucesso!";
			} else {
				echo "Falha ao Cadastrar!";
			}
		} else {
			echo "Falha ao Excluir antes de Cadastrar!";
		}
	} else {
			echo "Acão não permitida para a Rede Servimed!";
	}	
} else if ($_REQUEST['acao'] == "EXCLUIR_REDE") {
    if ($ClienteEspecialDao->excluirPorRede($_REQUEST['codRede'])) {
        echo "Clientes Especials Excluidos com sucesso!";
    } else {
        echo "Falha ao Excluir!";
    }
} else if ($_REQUEST['acao'] == "ADD") {
    if($ClienteEspecialDao->adicionar($_REQUEST['codCliente'],$ObjUsuario->getUsuarioCodigo(), $_REQUEST['diasLiberacaoNF'])){
        echo "Cliente Especial cadastrado com sucesso!";
    } else {
        echo "Falha ao Cadastrar!";
    }
} else if ($_REQUEST['acao'] == "EXCLUIR") {
    if ($ClienteEspecialDao->excluir($_REQUEST['codCliente'])) {
        echo "Cliente Especial Excluido com sucesso!";
    } else {
        echo "Falha ao Excluir!";
    }
} else if ($_REQUEST['acao'] == "LISTAR") {
    $ClienteEspecialDao = new ClienteEspecialDao();
    $ClienteEspecial = $ClienteEspecialDao->consultarClienteEspecial();
    ?>    

    <div class="table-responsive">
        <table class="table table-bordered table-striped table-sortable">
            <thead>
                <tr>
                    <th>Rede</th>
					<th>Nome Rede</th>   
                    <th>Cód Cliente</th>
                    <th>Razão Social</th>
                    <th>CNPJ</th>
					<th>Data Inclusão</th>
					<th>Usuário</th>
					<th>Prazo Liberado (Dias)</th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>                               
            <tbody>
                <?php
                while ($dados = oci_fetch_object($ClienteEspecial)) {
                    ?>
                    <tr>
                        <td><?php echo $dados->RED_CO_NUMERO; ?></td>
						<td><?php echo $dados->RED_NO_DESCRICAO_REDUZIDO; ?></td>
                        <td><?php echo $dados->CLI_CO_NUMERO; ?></td>
                        <td><?php echo $dados->CLI_NO_RAZAO_SOCIAL; ?></td>
                        <td><?php echo $dados->CLI_NU_CNPJ_CPF; ?></td>
						<td><?php echo $dados->ESP_DT_LIBERACAO; ?></td>
						<td><?php echo $dados->USU_NO_USERNAME; ?></td>
						<td><?php echo $dados->ESP_NU_DIAS_LIBERACAONF; ?></td>
                        <?php
                        if (!is_null($dados->ESP_CO_NUMERO)) {
                            ?>

                            <td><i class="glyphicon glyphicon-star"></i> Especial</td>
                            <td><a class="" onclick="javascript: excluirClienteEspecial(<?php echo $dados->CLI_CO_NUMERO; ?>);">Excluir</a></td>
                            <?php
                        } else {
                            ?>    
                            <td></td>
                            <td><a class="">Ativar</a> </td>
                            <?php
                        }
                        ?>

                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
    </div>
    <script language="JavaScript">
        $(function () {
            datatables.init();
        });
    </script>

    <?php
} 