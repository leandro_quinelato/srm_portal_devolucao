<?php
include_once( "../../../includes/Dao/DaoSistema.class.php" );
include_once( "../../../includes/Dao/LiberacaoNotaOrigemDao.class.php" );
include_once( "../../../includes/DevLiberacaoNotaOrigem.class.php" );

$LiberacaoNotaOrigemDao = new LiberacaoNotaOrigemDao();
$notasResult            = $LiberacaoNotaOrigemDao->consultarLiberacaoNota();


?>

<!DOCTYPE html>
<html lang="en">
    <head>        
        <!-- meta section -->
        <title>Devolução - Liberação de Nota</title>
        <?php include("../../../library/head.php"); ?>
    </head>
    <body>
        <!-- set loading layer -->
        <div class="dev-page-loading preloader"></div>
        <!-- ./set loading layer -->
        
        <!-- page wrapper -->
        <div class="dev-page">
            
            <!-- page header -->    
            <?php include("../../../library/topo.php"); ?>
            <!-- ./page header -->
            
            <!-- page container -->
            <div class="dev-page-container">

                <!-- page sidebar -->
                <?php include("../../../library/menu.php"); ?>
                <!-- ./page sidebar -->
                
                <!-- page content -->
                <div class="dev-page-content">                    
                    <!-- page content container -->
                    <div class="container">
                        
                        <!-- page title -->
                        <div class="page-title">
                            <h1>Liberação de Nota</h1>
                            
                            <ul class="breadcrumb">
                                <li><a href="#">Cadastro</a></li>
                                <li>Liberação de Nota</li>
                            </ul>
                        </div>                        
                        <!-- ./page title -->
                        

                        
                        <!-- datatables plugin -->
                        <div class="wrapper wrapper-white">
                            <div class="form-group">
                                <!--<button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal_wizard">Cadastro de Nota</button>-->
                                <button type="button" class="btn btn-success" onclick="javascript: carregaLiberarNota();">Cadastrar Liberação</button>
                            </div>
                            <div id="areaNotasLiberadas">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-sortable">
                                    <thead>
                                        <tr>
                                            <th>Nota/Série</th>
                                            <th>Data nota</th>
                                            <th>Cliente</th>
                                            <th>Descrição</th>
                                            <th>Data liberação</th>
											<th>Responsável</th>
											<th>Motivo Devolução</th>											
                                            <th></th>
                                        </tr>
                                    </thead>                               
                                    <tbody>
                        <?php
                            while ($dados = oci_fetch_object($notasResult)) {
                                if($dados->LBN_TX_DESCRICAO){
    		                    $descricao = $dados->LBN_TX_DESCRICAO->load();
    	                        }
                        ?>
                                        <tr>
                                            <td><?php echo $dados->NTO_NU_SERIE . ' ' . $dados->NTO_NU_NOTA; ?></td>
                                            <td><?php echo $dados->NTO_DT_NOTA; ?></td>
                                            <td><?php echo $dados->CLI_CO_NUMERO . ' - ' . $dados->CLI_NO_RAZAO_SOCIAL; ?></td>
                                            <td><?php echo $descricao; ?></td>
                                            <td><?php echo $dados->LBN_DT_LIBERACAO; ?></td>
											<td><?php echo $dados->USU_NO_USERNAME; ?></td>
											<td><?php echo $dados->TOC_NO_DESCRICAO; ?></td>											
                                            <td><button type="button" class="fa fa-minus-square" title="Inativo" onclick="javascript: excluirLiberacao(<?php echo $dados->NTO_NU_SERIE; ?>,<?php echo $dados->NTO_NU_NOTA; ?>, <?php echo $dados->CLI_CO_NUMERO; ?>);"></button></td>
                                        </tr>
                        <?php
                            }
                        ?> 
                                    </tbody>
                                </table>
                            </div>
                            </div>
                        </div>                        
                        <!-- ./datatables plugin -->
                        
                        
                        <!-- Copyright -->
                        <?php include("../../../library/copyright.php"); ?>
                        <!-- ./Copyright -->
                        
                    </div>
                    <!-- ./page content container -->                                       
                </div>
                <!-- ./page content -->                                                
            </div>  
            <!-- ./page container -->    
            
            <!-- page footer -->    
            <?php include("../../../library/footer.php"); ?>
            <!-- ./page footer -->
                     
        </div>
        <!-- ./page wrapper -->
        
        
        
        <div class="modal fade" id="modalLiberarNota" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

        </div>        
        
        <!-- javascripts -->
        <?php include("../../../library/rodape.php"); ?>

        <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js"></script>
        <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/plugins/datatables/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/plugins/smartwizard/jquery.smartWizard.js"></script>
        <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/plugins/sortable/sortable.min.js"></script>
        <!-- ./javascripts -->
        
        
    </body>
</html>