<!DOCTYPE html>
<?php
session_start();
include_once( "../../../includes/Dao/DaoSistema.class.php" );
include_once( "../../../includes/Dao/OcorrenciaDao.class.php" );
include_once( "../../../includes/CalTipoOcorrencia.class.php" );
include_once( "../../../includes/Dao/TipoOcorrenciaDAO.class.php" );
include_once( "../../../includes/TipoDevolucao.class.php" );
include_once( "../../../includes/Dao/TipoDevolucaoDao.class.php" );
include_once("DaoGlobal.class.php");
include_once("SistemaDao.class.php");
include_once("GlbSistema.class.php");
include_once("GlbSistemaPerfil.class.php");
include_once("GlbUsuario.class.php");
include_once("GlbColaborador.class.php");
include_once("GlbPessoa.class.php");
include_once("GlbEndereco.class.php");
include_once("GlbTelefone.class.php");
include_once("include_novo/GlbTransportadora.class.php");
include_once("include_novo/GlbContatoTransportador.class.php");
include_once("include_novo/ContatoTransportadorDao.class.php");
include_once("GlbProgramaRotina.class.php");
include_once("GlbModuloPrograma.class.php");
include_once("GlbSistemaModulo.class.php");
include_once("PerfilUsuarioDao.class.php");
include_once("PerfilPermissaoDao.class.php");

$ObjUsuario = unserialize($_SESSION['ObjLogin']);

$TipoOcorrenciaDao = new TipoOcorrenciaDAO();
$TipoOcorrencias = new CalTipoOcorrencia();
$TipoOcorrencias = $TipoOcorrenciaDao->listarTipoOcorrencia($TipoOcorrencias);

?>
<div class="modal-dialog">
    <form id="formLiberarNota">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Cadastro de nota</h4>
            </div>
            <div class="modal-body">
                <div id="step-6">
                    <div class="row">
                        <div class="col-md-12 col-sm-4 col-xs-4">
                            <div class="form-group">
                                <label>Cliente</label>                                        
                                <input type="text" placeholder="Cliente" class="form-control" id="codCliente" name="codCliente" onKeyPress="fMascara( 'numero', event, this )">                                        
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-4 col-xs-4">
                            <div class="form-group">
                                <label>Série</label>                                        
                                <input type="text" placeholder="Série" class="form-control" id="serie" name="serie" onKeyPress="fMascara( 'numero', event, this )">                                        
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-4 col-xs-4">
                            <div class="form-group">
                                <label>Nota</label>                                        
                                <input type="text" placeholder="Nota" class="form-control" id="nota" name="nota" onKeyPress="fMascara( 'numero', event, this )">                                        
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-4 col-xs-4">
                            <div class="form-group">
                                <label>Data</label>                            
                                <input type="text" class="form-control datepicker" id="dataNota" name="dataNota" onKeyPress="fMascara('data', event, this)" maxlength="10">
                            </div> 
                        </div>
                        <div class="col-md-3 col-sm-4 col-xs-4">
                            <div class="form-group">
                                <label>Setor</label>                                        
                                <input type="text" placeholder="Setor" class="form-control" id="setor" name="setor" onKeyPress="fMascara( 'numero', event, this )">                                        
                            </div>
                        </div>                                

                        <div class="col-md-3 col-sm-4 col-xs-4">
                            <div class="form-group">
                                <label>Multa</label>
                                <br>                               
                                <div class="checkbox checkbox-inline">
                                    <select name="multa" id="multa" aria-controls="DataTables_Table_0" class="form-control">
										<?
										if(strtoupper($ObjUsuario->getObjSistemaPerfil()->getNomeSistemaPerfil()) != "FARMACEUTICO" ){
										?>										
                                        <option value="1">Sim</option>
                                        <option value="0">Não</option>
										<?
										}else{
										?>
										<option value="0">Não</option>
										<?
										}
										?>
                                    </select>
                                </div>
                            </div>                        
                        </div>
                        <div class="col-md-3 col-sm-4 col-xs-4">
                            <div class="form-group">
                                <label>Regra de aprovação</label>
                                <br>                               
                                <div class="checkbox checkbox-inline">
                                    <select name="regra" id="regra" aria-controls="DataTables_Table_0" class="form-control">
                                        <option value="1">Sim</option>
                                        <option value="0">Não</option>
                                    </select>
                                </div>
                            </div>                        
                        </div>
                        <div class="col-md-6 col-sm-8 col-xs-8">
                            <div class="form-group">
                                <label>Descrição</label>
                                <textarea placeholder="Descrição" class="form-control" id="descricao" name="descricao"></textarea>                            
                            </div>
                        </div>
						
                        <div class="col-md-6 col-sm-8 col-xs-8">
                            <div class="form-group">
								<label>Motivo devolução</label>
								<select class="form-control selectpicker" id="motivoDevolucao" name="motivoDevolucao" onchange="avisoRecIndustria(this);">
									<option value=""></option>
									<?php
									//"FARMACEUTICO"
									$codMotivoFarmaceutico = 16;
									$desMotivoFarmaceutico = "RECOLHIDO PELA INDUSTRIA";
									
									if(strtoupper($ObjUsuario->getObjSistemaPerfil()->getNomeSistemaPerfil()) != "FARMACEUTICO" ){
										
										foreach ($TipoOcorrencias as $TipoOcorrencia) {
											if($TipoOcorrencia->getIndAtivoTipoOcorrencia() == 1){ // ativo
										?>
										<option value="<?php echo $TipoOcorrencia->getCodTipoOcorrencia(); ?>"><?php echo $TipoOcorrencia->getNomTipoOcorrencia(); ?></option>
										<?php
											}
										}
									}else{
										?>
										<option value="<?php echo $codMotivoFarmaceutico;?>"><?php echo $desMotivoFarmaceutico; ?></option>
										<?php										
									}
									?>
								</select>						
                            </div>
                        </div>						

                    </div>  


                </div>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-default" type="button">Cancelar</button>
                <button class="btn btn-primary" type="button" onclick="javascript: cadastrarLiberacao();">Salvar</button>
            </div>                
        </div>
    </form>
</div>