<?php
include_once( "../../../includes/Dao/DaoSistema.class.php" );
include_once("../../../includes/DevNotaOrigem.class.php");
include_once( "../../../includes/Dao/OcorrenciaDao.class.php" );
include_once( "../../../includes/Dao/LiberacaoNotaOrigemDao.class.php" );
include_once( "../../../includes/DevLiberacaoNotaOrigem.class.php" );
include_once( "../../../includes/DevParametros.class.php" );
include_once( "../../../includes/Dao/ParametroDao.class.php" );
include_once( "../../../includes/CalTipoOcorrencia.class.php" );


include_once("GlbUsuario.class.php");
include_once("integraSetSistemas.php");
$integra = new integraSetSistemas("conf.xml");
$ObjUsuario = unserialize($integra->getSistemaIntegra());


$serie = trim($_REQUEST['serie']);
$nota = trim($_REQUEST['nota']);
$dataNota = trim($_REQUEST['dataNota']);
$cliente = trim($_REQUEST['codCliente']);

$LiberacaoNotaOrigemDao = new LiberacaoNotaOrigemDao();

$DevLiberacaoNotaOrigem = new DevLiberacaoNotaOrigem();
$DevLiberacaoNotaOrigem->getDevNotaOrigem()->setSerie($serie);
$DevLiberacaoNotaOrigem->getDevNotaOrigem()->setNota($nota);
$DevLiberacaoNotaOrigem->getDevNotaOrigem()->setData($dataNota);
$DevLiberacaoNotaOrigem->getGlbCliente()->setClienteCodigo($cliente);
$DevLiberacaoNotaOrigem->setDescricao($_REQUEST['descricao']);
$DevLiberacaoNotaOrigem->setOperador($ObjUsuario->getUsuarioCodigo());

$DevLiberacaoNotaOrigem->getGlbVendedor()->setCodigo($_REQUEST['setor']);
$DevLiberacaoNotaOrigem->setMulta($_REQUEST['multa']);

$ObjTipoOcorrencia = new CalTipoOcorrencia();
$ObjTipoOcorrencia->setCodTipoOcorrencia($_REQUEST['motivoDevolucao']);
$DevLiberacaoNotaOrigem->setObjTipoOcorrencia($ObjTipoOcorrencia);

//echo "<pre>";
//print_r($_REQUEST);
//echo "</pre>";

extract($_REQUEST);

//die();

if ($_REQUEST['acao'] == "EXCLUIR") {
    if ($LiberacaoNotaOrigemDao->excluir($DevLiberacaoNotaOrigem)) {
        echo "Liberação excluida com Sucesso!";
    } else {
        echo "Falha ao Excluir!";
    }
} else if ($_REQUEST['acao'] == "LISTAR") {

    $LiberacaoNotaOrigemDao = new LiberacaoNotaOrigemDao();
    $notasResult = $LiberacaoNotaOrigemDao->consultarLiberacaoNota();
    ?>

    <div class="table-responsive">
        <table class="table table-bordered table-striped table-sortable">
            <thead>
                <tr>
                    <th>Nota/Série</th>
                    <th>Data nota</th>
                    <th>Cliente</th>
                    <th>Descrição</th>
                    <th>Data liberação</th>
					<th>Responsável</th>
					<th>Motivo Devolução</th>
                    <th></th>
                </tr>
            </thead>                               
            <tbody>
                <?php
                while ($dados = oci_fetch_object($notasResult)) {
                    if ($dados->LBN_TX_DESCRICAO) {
                        $descricao = $dados->LBN_TX_DESCRICAO->load();
                    }
                    ?>
                    <tr>
                        <td><?php echo $dados->NTO_NU_SERIE . ' ' . $dados->NTO_NU_NOTA; ?></td>
                        <td><?php echo $dados->NTO_DT_NOTA; ?></td>
                        <td><?php echo $dados->CLI_CO_NUMERO . ' - ' . $dados->CLI_NO_RAZAO_SOCIAL; ?></td>
                        <td><?php echo $descricao; ?></td>
                        <td><?php echo $dados->LBN_DT_LIBERACAO; ?></td>
						<td><?php echo $dados->USU_NO_USERNAME; ?></td>
						<td><?php echo $dados->TOC_NO_DESCRICAO; ?></td>
                        <td><button type="button" class="fa fa-minus-square" title="Inativo" onclick="javascript: excluirLiberacao(<?php echo $dados->NTO_NU_SERIE; ?>,<?php echo $dados->NTO_NU_NOTA; ?>, <?php echo $dados->CLI_CO_NUMERO; ?>);"></button></td>
                    </tr>
                    <?php
                }
                ?> 
            </tbody>
        </table>
    </div>
    <script language="JavaScript">
        $(function () {
            datatables.init();
        });
    </script>


    <?php
} else if ($_REQUEST['acao'] == "ADD") {

    $OcorrenciaDao = new OcorrenciaDao();

    $notaVenda = $OcorrenciaDao->consultaNotaVenda($cliente, $serie, $nota, $dataNota);

    if ($notaVenda->NUMNOTA) {

        $qtd_ocorrencias = $LiberacaoNotaOrigemDao->qtdLiberacaoNota($DevLiberacaoNotaOrigem);
        if ($qtd_ocorrencias == 0) {
            $dias_uteis = $OcorrenciaDao->diasUteisNota($dataNota);
            $ObjParametro = new devParametros();
            $ParametroDao = new ParametroDao();
            $ObjParametro = $ParametroDao->consultar();
             

            if ($dias_uteis <= $ObjParametro->getNumDiasLiberacaoNF()) {
                if ($LiberacaoNotaOrigemDao->inserir($DevLiberacaoNotaOrigem)) {
                    echo "Nota liberada com sucesso!";
                } else {
                    echo "Erro ao liberar a Nota";
                }
            } else {
                if ($_REQUEST['regra'] == 1) {
                    if ($LiberacaoNotaOrigemDao->inserir($DevLiberacaoNotaOrigem)) {
                        echo "Nota liberada com sucesso!";
                    } else {
                        echo "Erro ao liberar a Nota";
                    }
                } else {
                    echo 'Prazo limite para Liberação da Nota expirado!';
                }
            }
        } else {
            echo "A nota informada ja foi liberada!";
        }
    } else {
        echo "Aviso! Nota não encontrada na Base.";
    }
}

