<!DOCTYPE html>
<?php
include_once( "../../../includes/Dao/DaoSistema.class.php" );
include_once( "../../../includes/Dao/ClienteEspecialNotaDao.class.php" );

$ClienteEspecialNotaDao = new ClienteEspecialNotaDao();
$ClienteEspecialNota = $ClienteEspecialNotaDao->consultarClienteEspecialNota();
//    if ($MotivosReprova[0]) {
?>
<html lang="en">
    <head>        
        <!-- meta section -->
        <title>Devolução - Cliente Especial - Liberação de Várias Notas por Devolução</title>
        <?php include("../../../library/head.php"); ?>
    </head>
    <body>
        <!-- set loading layer -->
        <div class="dev-page-loading preloader"></div>
        <!-- ./set loading layer -->

        <!-- page wrapper -->
        <div class="dev-page">

            <!-- page header -->    
            <?php include("../../../library/topo.php"); ?>
            <!-- ./page header -->

            <!-- page container -->
            <div class="dev-page-container">

                <!-- page sidebar -->
                <?php include("../../../library/menu.php"); ?>
                <!-- ./page sidebar -->

                <!-- page content -->
                <div class="dev-page-content">                    
                    <!-- page content container -->
                    <div class="container">

                        <!-- page title -->
                        <div class="page-title">
                            <h1>Cliente especial notas</h1>

                            <ul class="breadcrumb">
                                <li><a href="#">Cadastro</a></li>
                                <li>Cliente especial notas</li>
                            </ul>
                        </div>                        
                        <!-- ./page title -->



                        <!-- datatables plugin -->
                        <div class="wrapper wrapper-white">
                            <div class="form-group">
                                <!--<button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal_wizard">Cadastrar reprova</button>-->
                                <button type="button" class="btn btn-success" onclick="javascript: carregaCadEspecialNota('ADD');">Cadastrar Cliente</button>
                            </div>
                            <div class="form-group">
								<button type="button" class="btn btn-success" onclick="javascript: carregaCadEspecialNota('ADD_REDE');">Cadastrar Rede</button>
								<button type="button" class="btn btn-danger" onclick="javascript: carregaCadEspecialNota('EXCLUIR_REDE');">Excluir Rede</button>								
                            </div>
                            <div id="listaClienteEspecialNota">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped table-sortable">
                                        <thead>
                                            <tr>
												<th>Rede</th>
                                                <th>Nome Rede</th>
                                                <th>Cód Cliente</th>
                                                <th>Razão Social</th>
                                                <th>CNPJ</th>
                                                <th></th>
                                            </tr>
                                        </thead>                               
                                        <tbody>
                                            <?php
                                            while ($dados = oci_fetch_object($ClienteEspecialNota)) {
                                                ?>
                                                <tr>
                                                    <td><?php echo $dados->RED_CO_NUMERO; ?></td>
													<td><?php echo $dados->RED_NO_DESCRICAO_REDUZIDO; ?></td>
                                                    <td><?php echo $dados->CLI_CO_NUMERO; ?></td>
                                                    <td><?php echo $dados->CLI_NO_RAZAO_SOCIAL; ?></td>
                                                    <td><?php echo $dados->CLI_NU_CNPJ_CPF; ?></td>

                                                    <?php
                                                    if (!is_null($dados->CEN_CO_NUMERO)) {
                                                        ?>

                                                        <td><a class="" onclick="javascript: excluirClienteEspecialNota(<?php echo $dados->CLI_CO_NUMERO; ?>);">Excluir</a></td>
                                                        <?php
                                                    } else {
                                                        ?>    
                                                        <td></td>
                                                        <td><a class="">Ativar</a> </td>
                                                        <?php
                                                    }
                                                    ?>

                                                </tr>
                                                <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>                        
                        <!-- ./datatables plugin -->


                        <!-- Copyright -->
                        <?php include("../../../library/copyright.php"); ?>
                        <!-- ./Copyright -->

                    </div>
                    <!-- ./page content container -->                                       
                </div>
                <!-- ./page content -->                                                
            </div>  
            <!-- ./page container -->

            <!-- right bar -->
            <!-- ./right bar -->            

            <!-- page footer -->    
            <?php include("../../../library/footer.php"); ?>
            <!-- ./page footer -->

            <!-- page search -->
            <!-- page search -->            
        </div>
        <!-- ./page wrapper -->

        <!-- Inicio Modal Editar -->
        <div class="modal fade" id="modal_Especial" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

        </div>        
        <!-- Fim Modal Editar -->
        <!-- javascripts -->
        <?php include("../../../library/rodape.php"); ?>

        <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js"></script>
        <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/plugins/datatables/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/plugins/smartwizard/jquery.smartWizard.js"></script>
        <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/plugins/sortable/sortable.min.js"></script>
        <!-- ./javascripts -->


    </body>
</html>