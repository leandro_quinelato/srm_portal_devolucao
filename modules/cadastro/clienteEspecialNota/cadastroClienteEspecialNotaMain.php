<?php
include_once( "../../../includes/Dao/DaoSistema.class.php" );
include_once( "../../../includes/Dao/ClienteEspecialNotaDao.class.php" );

include_once("GlbUsuario.class.php");
include_once("integraSetSistemas.php");
$integra = new integraSetSistemas("conf.xml");
$ObjUsuario = unserialize($integra->getSistemaIntegra());


$ClienteEspecialNotaDao = new ClienteEspecialNotaDao();

if ($_REQUEST['acao'] == "ADD_REDE") {
	if ($_REQUEST['codRede'] > 0) {
		if ($ClienteEspecialNotaDao->excluirPorRede($_REQUEST['codRede'])) {
			if($ClienteEspecialNotaDao->adicionarPorRede($_REQUEST['codRede'],$ObjUsuario->getUsuarioCodigo())){
				echo "Clientes Especials cadastrados com sucesso!";
			} else {
				echo "Falha ao Cadastrar!";
			}
		} else {
			echo "Falha ao Excluir antes de Cadastrar!";
		}
	} else {
			echo "Acão não permitida para a Rede Servimed!";
	}
} else if ($_REQUEST['acao'] == "EXCLUIR_REDE") {
    if ($ClienteEspecialNotaDao->excluirPorRede($_REQUEST['codRede'])) {
        echo "Clientes Especials Excluidos com sucesso!";
    } else {
        echo "Falha ao Excluir!";
    }
} else if ($_REQUEST['acao'] == "ADD") {
    if($ClienteEspecialNotaDao->adicionar($_REQUEST['codCliente'],$ObjUsuario->getUsuarioCodigo())){
        echo "Cliente Especial cadastrado com sucesso!";
    } else {
        echo "Falha ao Cadastrar!";
    }
} else if ($_REQUEST['acao'] == "EXCLUIR") {
    if ($ClienteEspecialNotaDao->excluir($_REQUEST['codCliente'])) {
        echo "Cliente Especial Excluido com sucesso!";
    } else {
        echo "Falha ao Excluir!";
    }
} else if ($_REQUEST['acao'] == "LISTAR") {
    $ClienteEspecialNotaDao = new ClienteEspecialNotaDao();
    $ClienteEspecialNota = $ClienteEspecialNotaDao->consultarClienteEspecialNota();
    ?>    

    <div class="table-responsive">
        <table class="table table-bordered table-striped table-sortable">
            <thead>
                <tr>
                    <th>Rede</th>
					<th>Nome Rede</th>
                    <th>Cód Cliente</th>
                    <th>Razão Social</th>
                    <th>CNPJ</th>
                    <th></th>
                </tr>
            </thead>                               
            <tbody>
                <?php
                while ($dados = oci_fetch_object($ClienteEspecialNota)) {
                    ?>
                    <tr>
                        <td><?php echo $dados->RED_CO_NUMERO; ?></td>
						<td><?php echo $dados->RED_NO_DESCRICAO_REDUZIDO; ?></td>
                        <td><?php echo $dados->CLI_CO_NUMERO; ?></td>
                        <td><?php echo $dados->CLI_NO_RAZAO_SOCIAL; ?></td>
                        <td><?php echo $dados->CLI_NU_CNPJ_CPF; ?></td>

                        <?php
                        if (!is_null($dados->CEN_CO_NUMERO)) {
                            ?>

                            <td><a class="" onclick="javascript: excluirClienteEspecialNota(<?php echo $dados->CLI_CO_NUMERO; ?>);">Excluir</a></td>
                            <?php
                        } else {
                            ?>    
                            <td></td>
                            <td><a class="">Ativar</a> </td>
                            <?php
                        }
                        ?>

                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
    </div>
    <script language="JavaScript">
        $(function () {
            datatables.init();
        });
    </script>

    <?php
} 