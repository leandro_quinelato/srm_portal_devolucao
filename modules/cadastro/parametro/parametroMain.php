<?php

ini_set('display_errors', 1);

include_once( "../../../includes/Dao/DaoSistema.class.php" );
include_once( "../../../includes/DevParametros.class.php" );
include_once( "../../../includes/Dao/ParametroDao.class.php" );

include_once( "../../../includes/DevParametrosValorDevolucao.class.php" );
include_once( "../../../includes/Dao/ParametroValorDevolucaoDao.class.php" );

include_once( "../../../includes/DevParametroLog.class.php" );
include_once( "../../../includes/Dao/ParametroLogDao.class.php" );

include_once( "../../../includes/Multa.class.php" );
include_once( "../../../includes/Dao/MultaDao.class.php" );

include_once("GlbUsuario.class.php");
include_once("integraSetSistemas.php");
$integra = new integraSetSistemas("conf.xml");
$ObjUsuario = unserialize($integra->getSistemaIntegra());

//echo '<pre>????';
//print_r($_REQUEST);
//echo '</pre>';
//die();
extract($_REQUEST);
$ObjParametro = new devParametros();
$ObjMulta = new Multa();
$ObjParametroValorDevolucao = new devParametrosValorDevolucao();

$ObjParametroLog = new DevParametroLog();
$ObjParametroLog->setUsuario($ObjUsuario->getUsuarioCodigo()); // atualizado pegar codigo usuario da GLoBaL
$ObjParametroLog->setTipoUsuario('DEV');

$ObjParametro->setNumDiasCadalimentar($par_nu_dias_cadalimentar);
$ObjParametro->setNumDiasCadFarmaDH($par_nu_dias_cadfarmadh);
$ObjParametro->setNumDiasAprovaGerente($par_nu_dias_aprovagerente);
$ObjParametro->setNumDiasLiberacaoNF($par_nu_dias_liberacaonf);
$ObjParametro->setEmailReentrega($email_reentrega);

$ParametroDao = new ParametroDao();
if ($ParametroDao->alterar($ObjParametro)) {
	
    $ObjMulta->setValorMulta(trim(  str_replace(',','.', str_replace('.', '', $mul_vr_multa)) ));
    $ObjMulta->setQuantidadeMulta(trim($mul_qt_nota));
    $ObjMulta->setOrigemNotaMulta($tporigem);
    $ObjMulta->setInicioMulta(trim($mul_dt_inicio));
    $ObjMulta->setFimMulta(trim($mul_dt_fim));
    $ObjMulta->setStatusMulta(1); 

	
	$ObjParametroValorDevolucao->setValor1F(trim(  str_replace(',','.', str_replace('.', '', $valor_1_f))));
	$ObjParametroValorDevolucao->setValor1O(trim(  str_replace(',','.', str_replace('.', '', $valor_1_o))));
	$ObjParametroValorDevolucao->setValor1H(trim(  str_replace(',','.', str_replace('.', '', $valor_1_h))));
	$ObjParametroValorDevolucao->setValor1A(trim(  str_replace(',','.', str_replace('.', '', $valor_1_a))));
	
	$ObjParametroValorDevolucao->setValor46F(trim(  str_replace(',','.', str_replace('.', '', $valor_46_f))));
	$ObjParametroValorDevolucao->setValor46O(trim(  str_replace(',','.', str_replace('.', '', $valor_46_o))));
	$ObjParametroValorDevolucao->setValor46H(trim(  str_replace(',','.', str_replace('.', '', $valor_46_h))));
	$ObjParametroValorDevolucao->setValor46A(trim(  str_replace(',','.', str_replace('.', '', $valor_46_a))));

	$ObjParametroValorDevolucao->setValor48F(trim(  str_replace(',','.', str_replace('.', '', $valor_48_f))));
	$ObjParametroValorDevolucao->setValor48O(trim(  str_replace(',','.', str_replace('.', '', $valor_48_o))));
	$ObjParametroValorDevolucao->setValor48H(trim(  str_replace(',','.', str_replace('.', '', $valor_48_h))));
	$ObjParametroValorDevolucao->setValor48A(trim(  str_replace(',','.', str_replace('.', '', $valor_48_a))));

	$ObjParametroValorDevolucao->setValor49F(trim(  str_replace(',','.', str_replace('.', '', $valor_49_f))));
	$ObjParametroValorDevolucao->setValor49O(trim(  str_replace(',','.', str_replace('.', '', $valor_49_o))));
	$ObjParametroValorDevolucao->setValor49H(trim(  str_replace(',','.', str_replace('.', '', $valor_49_h))));
	$ObjParametroValorDevolucao->setValor49A(trim(  str_replace(',','.', str_replace('.', '', $valor_49_a))));	

	$ObjParametroValorDevolucao->setValor50F(trim($valor_50_f));	
	
	//var_dump($ObjParametroValorDevolucao);
	
	$ParametroValorDevolucaoDao = new ParametroValorDevolucaoDao();
	if (! $ParametroValorDevolucaoDao->alterar($ObjParametroValorDevolucao)){
		echo "Falha ao atualizar os valores!";
	}
	
	
    $MultaDao = new MultaDao();
    if ($MultaDao->alterar($ObjMulta)) {

        $ParametroLogDao = new ParametroLogDao();
        $ParametroLogDao->gravar($ObjParametroLog);
        echo "Parametros atualizados com sucesso!";
    } else {
        echo "Falha ao atualizar!";
    }
}
else {
    echo "Falha ao atualizar os parâmetros!";
}