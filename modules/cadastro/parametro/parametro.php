<?php
include_once( "../../../includes/Dao/DaoSistema.class.php" );
include_once( "../../../includes/DevParametros.class.php" );
include_once( "../../../includes/Dao/ParametroDao.class.php" );

include_once( "../../../includes/DevParametroLog.class.php" );
include_once( "../../../includes/Dao/ParametroLogDao.class.php" );

include_once( "../../../includes/Multa.class.php" );
include_once( "../../../includes/Dao/MultaDao.class.php" );

include_once( "../../../includes/DevParametrosValorDevolucao.class.php" );
include_once( "../../../includes/Dao/ParametroValorDevolucaoDao.class.php" );


$ObjParametro = new devParametros();
$ParametroDao = new ParametroDao();
$ObjParametro = $ParametroDao->consultar();
 

$ParametroLogDao = new ParametroLogDao();
$result_logParam = $ParametroLogDao->consultar();
ocifetchinto($result_logParam, $logParam, OCI_ASSOC);

$ObjMulta = new Multa(); 

$MultaDao = new MultaDao(); 
$result = $MultaDao->consultar($ObjMulta);
OCIFetchInto ($result, $parMulta, OCI_ASSOC);

$ParametroValorDevolucaoDao = new ParametroValorDevolucaoDao();
$ObjParametroValorDevolucao = $ParametroValorDevolucaoDao->consultar();

?>
<!DOCTYPE html>
<html lang="en">
    <head>        
        <!-- meta section -->
        <title>Devolução - Parâmetros</title>
        <?php include("../../../library/head.php"); ?>
    </head>
    <body>
        <!-- set loading layer -->
        <div class="dev-page-loading preloader"></div>
        <!-- ./set loading layer -->

        <!-- page wrapper -->
        <div class="dev-page">

            <!-- page header -->    
            <?php include("../../../library/topo.php"); ?>
            <!-- ./page header -->

            <!-- page container -->
            <div class="dev-page-container">

                <!-- page sidebar -->
                <?php include("../../../library/menu.php"); ?>
                <!-- ./page sidebar -->

                <!-- page content -->
                <div class="dev-page-content">                    
                    <!-- page content container -->
                    <div class="container">

                        <!-- page title -->
                        <div class="page-title">
                            <h1>Parâmetros</h1>

                            <ul class="breadcrumb">
                                <li><a href="#">Cadastro</a></li>
                                <li>Parâmetros</li>
                            </ul>						
                        </div>                        
                        <!-- ./page title -->
                        <form id="formParametros">

                            <!-- datatables plugin -->
                            <div class="wrapper wrapper-white">
                                <div class="page-subtitle ParametroAtualizado">
                                    <p>Atualizado <?php echo $logParam['DATA_EXIBIR'] . ' Por ' . $logParam['OPR_NO_EXIBICAO']; ?></p>
                                </div>
							</div>

                            <div class="wrapper wrapper-white">
                                <div class="page-subtitle">
                                    <h3>CD Matriz - Valor para aprovação do gerente</h3>
                                </div>
							
                                <div class="row">							
                                    <div class="col-md-2 col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <label>Farma</label>
                                            <input type="text" class="form-control" id="valor_1_f" name="valor_1_f" onkeyup="$(this).priceFormat({prefix: '', centsSeparator: ',', thousandsSeparator: '.', allowNegative: true});" value="<?php echo number_format($ObjParametroValorDevolucao->getValor1F(), 2, ',', '.'); ?>"/>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <label>Dist. Direta</label>
                                            <input type="text" class="form-control" id="valor_1_o" name="valor_1_o" onkeyup="$(this).priceFormat({prefix: '', centsSeparator: ',', thousandsSeparator: '.', allowNegative: true});" value="<?php echo number_format($ObjParametroValorDevolucao->getValor1O(), 2, ',', '.'); ?>"/>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <label>Hospitalar</label>
                                            <input type="text" class="form-control" id="valor_1_h" name="valor_1_h" onkeyup="$(this).priceFormat({prefix: '', centsSeparator: ',', thousandsSeparator: '.', allowNegative: true});" value="<?php echo number_format($ObjParametroValorDevolucao->getValor1H(), 2, ',', '.'); ?>"/>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <label>Alimentar</label>
                                            <input type="text" class="form-control" id="valor_1_a" name="valor_1_a" onkeyup="$(this).priceFormat({prefix: '', centsSeparator: ',', thousandsSeparator: '.', allowNegative: true});" value="<?php echo number_format($ObjParametroValorDevolucao->getValor1A(), 2, ',', '.'); ?>"/>
                                        </div>
                                    </div>
                                </div>
							</div>

                            <div class="wrapper">
                                <div class="page-subtitle">
                                    <h3>CD Rib. Neves - Valor para aprovação do gerente</h3>
                                </div>
                                <div class="row">							
                                    <div class="col-md-2 col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <label>Farma</label>
                                            <input type="text" class="form-control" id="valor_46_f" name="valor_46_f" onkeyup="$(this).priceFormat({prefix: '', centsSeparator: ',', thousandsSeparator: '.', allowNegative: true});" value="<?php echo number_format($ObjParametroValorDevolucao->getValor46F(), 2, ',', '.'); ?>"/>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <label>Dist. Direta</label>
                                            <input type="text" class="form-control" id="valor_46_o" name="valor_46_o" onkeyup="$(this).priceFormat({prefix: '', centsSeparator: ',', thousandsSeparator: '.', allowNegative: true});" value="<?php echo number_format($ObjParametroValorDevolucao->getValor46O(), 2, ',', '.'); ?>"/>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <label>Hospitalar</label>
                                            <input type="text" class="form-control" id="valor_46_h" name="valor_46_h" onkeyup="$(this).priceFormat({prefix: '', centsSeparator: ',', thousandsSeparator: '.', allowNegative: true});" value="<?php echo number_format($ObjParametroValorDevolucao->getValor46H(), 2, ',', '.'); ?>"/>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <label>Alimentar</label>
                                            <input type="text" class="form-control" id="valor_46_a" name="valor_46_a" onkeyup="$(this).priceFormat({prefix: '', centsSeparator: ',', thousandsSeparator: '.', allowNegative: true});" value="<?php echo number_format($ObjParametroValorDevolucao->getValor46A(), 2, ',', '.'); ?>"/>
                                        </div>
                                    </div>
                                </div>   
							</div>

                            <div class="wrapper wrapper-white">
                                <div class="page-subtitle">
                                    <h3>CD HPC - Valor para aprovação do gerente</h3>
                                </div>
                                <div class="row">						
                                    <div class="col-md-2 col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <label>Farma</label>
                                            <input type="text" class="form-control" id="valor_48_f" name="valor_48_f" onkeyup="$(this).priceFormat({prefix: '', centsSeparator: ',', thousandsSeparator: '.', allowNegative: true});" value="<?php echo number_format($ObjParametroValorDevolucao->getValor48F(), 2, ',', '.'); ?>"/>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <label>Dist. Direta</label>
                                            <input type="text" class="form-control" id="valor_48_o" name="valor_48_o" onkeyup="$(this).priceFormat({prefix: '', centsSeparator: ',', thousandsSeparator: '.', allowNegative: true});" value="<?php echo number_format($ObjParametroValorDevolucao->getValor48O(), 2, ',', '.'); ?>"/>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <label>Hospitalar</label>
                                            <input type="text" class="form-control" id="valor_48_h" name="valor_48_h" onkeyup="$(this).priceFormat({prefix: '', centsSeparator: ',', thousandsSeparator: '.', allowNegative: true});" value="<?php echo number_format($ObjParametroValorDevolucao->getValor48H(), 2, ',', '.'); ?>"/>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <label>Alimentar</label>
                                            <input type="text" class="form-control" id="valor_48_a" name="valor_48_a" onkeyup="$(this).priceFormat({prefix: '', centsSeparator: ',', thousandsSeparator: '.', allowNegative: true});" value="<?php echo number_format($ObjParametroValorDevolucao->getValor48A(), 2, ',', '.'); ?>"/>
                                        </div>
                                    </div>
                                </div>
							</div>
							
                            <div class="wrapper">
                                <div class="page-subtitle">
                                    <h3>CD Queimados - Valor para aprovação do gerente</h3>
                                </div>							
								<div class="row">
                                    <div class="col-md-2 col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <label>Farma</label>
                                            <input type="text" class="form-control" id="valor_49_f" name="valor_49_f" onkeyup="$(this).priceFormat({prefix: '', centsSeparator: ',', thousandsSeparator: '.', allowNegative: true});" value="<?php echo number_format($ObjParametroValorDevolucao->getValor49F(), 2, ',', '.'); ?>"/>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <label>Dist. Direta</label>
                                            <input type="text" class="form-control" id="valor_49_o" name="valor_49_o" onkeyup="$(this).priceFormat({prefix: '', centsSeparator: ',', thousandsSeparator: '.', allowNegative: true});" value="<?php echo number_format($ObjParametroValorDevolucao->getValor49O(), 2, ',', '.'); ?>"/>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <label>Hospitalar</label>
                                            <input type="text" class="form-control" id="valor_49_h" name="valor_49_h" onkeyup="$(this).priceFormat({prefix: '', centsSeparator: ',', thousandsSeparator: '.', allowNegative: true});" value="<?php echo number_format($ObjParametroValorDevolucao->getValor49H(), 2, ',', '.'); ?>"/>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <label>Alimentar</label>
                                            <input type="text" class="form-control" id="valor_49_a" name="valor_49_a" onkeyup="$(this).priceFormat({prefix: '', centsSeparator: ',', thousandsSeparator: '.', allowNegative: true});" value="<?php echo number_format($ObjParametroValorDevolucao->getValor49A(), 2, ',', '.'); ?>"/>
                                        </div>
                                    </div>									
                                </div>
							</div>

                            <div class="wrapper wrapper-white">
                                <div class="page-subtitle">
                                    <h3>Prazo de Devolução</h3>
                                </div>

                                <div class="row">
                                    <div class="col-md-2 col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <label>Alimentar - Ocorrência</label>
                                            <input type="text" class="form-control" placeholder="Prazo máximo" id="par_nu_dias_cadalimentar" name="par_nu_dias_cadalimentar" onKeyPress="fMascara('numero', event, this)" value="<?php echo $ObjParametro->getNumDiasCadalimentar(); ?>"/>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-4 col-xs-4">
                                        <div class="form-group">
                                            <label>Farma, DD e Hosp - Ocorrência</label>
                                            <input type="text" class="form-control" placeholder="Prazo máximo" id="par_nu_dias_cadfarmadh" name="par_nu_dias_cadfarmadh" onKeyPress="fMascara('numero', event, this)" value="<?php echo $ObjParametro->getNumDiasCadFarmaDH(); ?>"/>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <label>Aprovação Gerente</label>
                                            <input type="text" class="form-control" placeholder="Prazo máximo" id="par_nu_dias_aprovagerente" name="par_nu_dias_aprovagerente" onKeyPress="fMascara('numero', event, this)" value="<?php echo $ObjParametro->getNumDiasAprovaGerente(); ?>"/>
                                        </div>
                                    </div>                                
                                    <div class="col-md-2 col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <label>Liberação de NF</label>
                                            <input type="text" class="form-control" placeholder="Prazo máximo" id="par_nu_dias_liberacaonf" name="par_nu_dias_liberacaonf" onKeyPress="fMascara('numero', event, this)" value="<?php echo $ObjParametro->getNumDiasLiberacaoNF(); ?>" />
                                        </div>
                                    </div>                                
                                </div>
                            </div> 

                            <div class="wrapper">                
                                <div class="page-subtitle">
                                    <h3>Devolução - Multa</h3>
                                </div>

                                <div class="row">
                                    <div class="col-md-2 col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <label>Início</label>                            
                                            <input type="text" class="form-control datepicker" id="mul_dt_inicio" name="mul_dt_inicio" onKeyPress="fMascara('data', event, this)" maxlength="10" value="<?php echo $parMulta['MUL_DT_INICIO'] ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <label>Fim</label>                            
                                            <input type="text" class="form-control datepicker" id="mul_dt_fim" name="mul_dt_fim" onKeyPress="fMascara('data', event, this)" maxlength="10" value="<?php echo $parMulta['MUL_DT_FIM'] ?>" >
                                        </div>
                                    </div>

                                    <div class="col-md-2 col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <label>Origem</label>
                                            <select class="form-control selectpicker" id="tporigem" name="tporigem" >
                                                <option value="NULL">PADRÃO</option>
                                                <option value="CLIE">CLIENTE</option>
                                                <option value="DIRT">DIRETORIA</option>
                                                <option value="ESTQ">ESTOQUE</option>
                                                <option value="INDU">INDUSTRIA</option>
                                                <option value="REPR">REPRESENTANTE</option>
                                                <option value="TRAN">TRANSPORTADORA</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-3 col-sm-4 col-xs-4">
                                        <div class="form-group">
                                            <label>Valor da multa</label>                                        
                                            <input type="text" placeholder="Valor" class="form-control" id="mul_vr_multa" name="mul_vr_multa" onKeyPress="fMascara('numero', event, this)" onkeyup="$(this).priceFormat({prefix: '', centsSeparator: ',', thousandsSeparator: '.', allowNegative: true});"  value="<?php echo number_format($parMulta['MUL_VR_MULTA'],2,',','.'); ?> ">                                        
                                        </div>
                                    </div>

                                    <div class="col-md-3 col-sm-4 col-xs-4">
                                        <div class="form-group">
                                            <label>Quant. nota</label>                                        
                                            <input type="text" placeholder="Quant nota" class="form-control" id="mul_qt_nota" name="mul_qt_nota" onKeyPress="fMascara('numero', event, this)"  value="<?php echo $parMulta['MUL_QT_NOTA'] ?>" >                                        
                                        </div>
                                    </div>

                                </div>
                            </div> 

                            <div class="wrapper wrapper-white">
                                <div class="page-subtitle">
                                    <h3>Prazo de Coleta do Transportador</h3>
                                </div>

                                <div class="row">
                                    <div class="col-md-2 col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <label>Quantidade de Dias</label>
                                            <input type="text" class="form-control" id="valor_50_f" name="valor_50_f" value="<?php echo number_format($ObjParametroValorDevolucao->getValor50F(), 0, ',', '.'); ?>"/>
                                        </div>
                                    </div>                             
                                </div>
                            </div>
							
                            <div class="wrapper wrapper-white">
                                <div class="page-subtitle">
                                    <h3>Email de reentrega</h3>
                                </div>
                                <div class="row">						
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label>Destinatários</label>
                                            <div class="row">
                                            <div class="col-md-5 col-sm-6 col-xs-6">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" id="email" placeholder="E-mail"/>   
                                                </div>
                                                </div>
                                                <div class="col-md-2 col-sm-6 col-xs-6">
                                                    <div class="form-group">
                                                        <input type="button" class="btn btn-primary add-row" value="Adicionar E-mail">   
                                                    </div>
                                                </div>
                                            </div>
                                            </br></br>
                                            <table class="table table-bordered table-striped table-sortable">
                                                <thead>
                                                    <tr>
                                                        <th></th>
                                                        <th>E-mail</th>
                                                    </tr>
                                                </thead>                               
                                                <tbody>    
                                                    <?php
                                                        foreach ($ObjParametro->getListaEmailReentrega() as $Valores) {
                                                    ?>                                                                                                           
                                                    <tr>
                                                        <td><input type="checkbox" id=<?php echo $Valores; ?> name="record"></td>
                                                        <td><?php echo $Valores; ?></td>                                                        
                                                    </tr>
                                                    <?php
                                                        } 
                                                    ?>                                                
                                                </tbody>
                                            </table>
                                            <input type="button" class="btn btn-primary delete-row" value="Excluir E-mail">                                            
                                            <input type="hidden" class="form-control" id="email_reentrega" name="email_reentrega" value="<?php echo $ObjParametro->getEmailReentrega(); ?>"/>
                                        </div>
                                    </div>
                                </div>
							</div>							


                            <div class="wrapper">
                                <div class="row">
                                    <div class="col-md-2">
                                        <a class="btn btn-primary" onclick="javascript: atualizarParametro();">Atualizar</a>
                                        <!--<button class="btn btn-primary" onclick="javascript: atualizarParametro();">Atualizar</button>-->
                                    </div> 
                                </div>
                            </div>                             

                            <div id="msgParametros"></div>
                        </form>
                        <!-- ./datatables plugin -->


                        <!-- Copyright -->
                        <?php include("../../../library/copyright.php"); ?>
                        <!-- ./Copyright -->

                    </div>
                    <!-- ./page content container -->                                       
                </div>
                <!-- ./page content -->                                                
            </div>  
            <!-- ./page container -->      

            <!-- page footer -->    
            <?php include("../../../library/footer.php"); ?>
            <!-- ./page footer -->

            <!-- page search -->

            <!-- page search -->            
        </div>
        <!-- ./page wrapper -->


        <!-- javascript -->

        <?php include("../../../library/rodape.php"); ?>

        <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js"></script>
        <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/plugins/datatables/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/plugins/smartwizard/jquery.smartWizard.js"></script>
        <script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/plugins/sortable/sortable.min.js"></script>
        <!-- ./javascript -->


    </body>
</html>

<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $(".add-row").click(function(){           
            var email = $("#email").val();
            var markup = "<tr><td><input type='checkbox' id="+email+" name='record'></td><td>" + email + "</td></tr>";
            var text = $('#email_reentrega');
            if(text.val().match(email)){
                alert("Esse e-mail ja foi cadastrado");
            }else{
                $("table tbody").append(markup);          
                text.val(text.val() + ';' + email);  
            }

            
        });
        
        // Find and remove selected table rows
        $(".delete-row").click(function(){
            var email = '';
            $("table tbody").find('input[name="record"]').each(function(){
            	if($(this).is(":checked")){
                    email = this.id;
                    $(this).parents("tr").remove();
                }
            });
            var text = $('#email_reentrega');
            if(text.val().match(email)){
                text.val(text.val().replace(email,''));  
            }
            text.val(text.val().replace(';;',';'));  

        });
    });    
</script>