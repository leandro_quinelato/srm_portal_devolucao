<?php

include_once( "../../../includes/Dao/DaoSistema.class.php" );
include_once( "../../../includes/CalTipoOcorrencia.class.php" );
include_once( "../../../includes/Dao/TipoOcorrenciaDAO.class.php" );
include_once( "../../../includes/Dao/TipoOcorrenciaPerfilDao.class.php" );

$TipoOcorrenciaDao = new TipoOcorrenciaDAO();
$TipoOcorrencia    = new CalTipoOcorrencia();
$TipoOcorrenciaPerfilDao = new TipoOcorrenciaPerfilDao();

if($_REQUEST['codMotivoDevolucao'] != ""){

$TipoOcorrencia->setCodTipoOcorrencia($_REQUEST['codMotivoDevolucao']);
$TipoOcorrencia = $TipoOcorrenciaDao->listarTipoOcorrencia($TipoOcorrencia);
$TipoOcorrencia = $TipoOcorrencia[0];

}
?>


<div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Cadastro / Edição de motivo de devolução</h4>
                    </div>
                    <div class="modal-body">
                        <div id="step-6">
                            <div class="form-group">
                                <form id="formMotivoDevolucao">
                                <div class="row">
                                    <div class="col-md-12 col-sm-6 col-xs-6">
                                    	<div class="form-group"> 
                                            <label>Descrição</label>
                                            <input type="text" class="form-control" placeholder="Descrição" id="descMotivoDevolucao"  name="descMotivoDevolucao" value="<?php echo $TipoOcorrencia->getNomTipoOcorrencia();  ?>"/>
                                        </div>
                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-6">
                                    	<div class="form-group">
	                                        <label>Regra</label>
                                        	<br>                               
                                        	<div class="checkbox checkbox-inline">
                                                <!--<input type="checkbox" checked="" id="check_1">-->
                                                    <select name="toc_in_regra" id="toc_in_regra" aria-controls="DataTables_Table_0" class="form-control">
                                                        <option value="S" <?php echo  $TipoOcorrencia->getRegra() == 'S'? "selected=selected" : ''; ?>>Sim</option>
                                                        <option value="N" <?php echo  $TipoOcorrencia->getRegra() == 'N'? "selected=selected" : ''; ?>>Não</option>
                                                    </select>
                                                
                                                <!--<label >Regra de aprovação</label>-->
                                            </div>
                                        </div> 
                                    </div>
                                    <div class="col-md-4 col-sm-6 col-xs-6">
                                    	<div class="form-group">
	                                        <label>Status</label>
                                        	<br>                               
                                        	<div class="checkbox checkbox-inline">
                                                    <input type="checkbox" id="checkAtivar" name="checkAtivar" value="1" <?php echo  $TipoOcorrencia->getIndAtivoTipoOcorrencia() == 1 ? "checked='true'" : ""; ?>>                                             
                                                <label for="checkAtivar">Ativar/Desativar</label>
                                            </div>
                                        </div> 
                                    </div> 
									<!-- perfil de sistema - inicio -->
									<?php 
									if($TipoOcorrencia->getCodTipoOcorrencia()){
									?>
                                    <div class="col-md-4 col-sm-6 col-xs-6">
                                    	<div class="form-group">
	                                        <label>Perfil Cadastrante</label>
                                        	<br>                               
                                        	<div class="checkbox checkbox-inline">
											<?php
												$result = $TipoOcorrenciaPerfilDao->ListaPerfilSistema($sistema = 29);
												while ($rsTipoOcorrencia = oci_fetch_object($result)) {
													$cod_perfil = $rsTipoOcorrencia->PER_CO_NUMERO;
													$des_perfil = $rsTipoOcorrencia->PER_NO_PERFIL;
													
													//verifica existencia
													$flagPerfil = $TipoOcorrenciaPerfilDao->verificaTipoOcorrenciaPerfil($TipoOcorrencia->getCodTipoOcorrencia(), $cod_perfil);
											?>
                                                <input type="checkbox" id="checkPerfil<?=$cod_perfil?>" name="checkPerfil[]" value="<?=$cod_perfil?>" <?php echo  $flagPerfil == 1 ? "checked='true'" : ""; ?>>
                                                <label for="checkPerfil<?=$cod_perfil?>"><?=$des_perfil?></label>
											<?php
												}
											?>
                                            </div>			
                                        </div> 
                                    </div> 		
									<?php
									}
									?>
									<!-- perfil de sistema - fim -->
                                </div>
                                </form>
                            </div>                                                                                                                    
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-default" type="button">Cancelar</button>
                        <?php
                            if($TipoOcorrencia->getCodTipoOcorrencia()){
                        ?>
                        <button class="btn btn-primary" type="button" onclick="javascript: atualizarMotivoDevolucao(<?php echo $TipoOcorrencia->getCodTipoOcorrencia(); ?>);" >Atualizar</button>
                        <?php
                            }else{
                        ?>
                                <button class="btn btn-primary" type="button" onclick="javascript: gravarMotivoDevolucao();" >Salvar</button>
                        <?php
                            }
                        ?>
                                <div id="msgMotivoDevolucao"></div>
                    </div>                
                </div>
            </div>
