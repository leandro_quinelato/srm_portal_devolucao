<?php
include_once( "../../../includes/Dao/DaoSistema.class.php" );
include_once( "../../../includes/CalTipoOcorrencia.class.php" );
include_once( "../../../includes/Dao/TipoOcorrenciaDAO.class.php" );
include_once( "../../../includes/Dao/TipoOcorrenciaPerfilDao.class.php" );

$TipoOcorrenciaDao = new TipoOcorrenciaDAO();
$TipoOcorrencia = new CalTipoOcorrencia();

//echo "<pre>";
//print_r($_REQUEST);
//echo "</pre>";

$codMotivoDevolucao = $_REQUEST["codMotivoDevolucao"];
$acao = $_REQUEST["acao"];

if (isset($_REQUEST['checkAtivar'])) {
    $statusMotivo = $_REQUEST['checkAtivar'];
} else {
    $statusMotivo = 0;
}

function atualizaVetorPerfil($codMotivoDevolucao, $vetorPerfil){
	$TipoOcorrenciaPerfilDao = new TipoOcorrenciaPerfilDao();
	$TipoOcorrenciaPerfilDao->excluir($codMotivoDevolucao);
	FOREACH($vetorPerfil as $codPerfil){
		//echo $codPerfil."</br>";	
		$TipoOcorrenciaPerfilDao->adicionar($codMotivoDevolucao, $codPerfil);
	}
}

if ($acao == 'ATUALIZAR') {
    $TipoOcorrencia->setCodTipoOcorrencia($codMotivoDevolucao);
    $TipoOcorrencia->setNomTipoOcorrencia($_REQUEST['descMotivoDevolucao']);
    $TipoOcorrencia->setRegra($_REQUEST['toc_in_regra']);
    $TipoOcorrencia->setIndAtivoTipoOcorrencia($statusMotivo);
    $TipoOcorrenciaDao->alterarTipoOcorrencia($TipoOcorrencia);
	atualizaVetorPerfil($codMotivoDevolucao, $_REQUEST["checkPerfil"]);
    
} else if ($acao == 'LISTAR') {

    $TipoOcorrencias = new CalTipoOcorrencia();
    $TipoOcorrencias = $TipoOcorrenciaDao->listarTipoOcorrencia($TipoOcorrencias);
    ?>    

    <table class="table table-bordered table-striped table-sortable">
        <thead>
            <tr>
                <th>Descrição</th>
                <th>Status</th>
                <th>Regra Aprovação</th>
                <th>Editar</th>
            </tr>
        </thead>                               
        <tbody>
            <?php
            foreach ($TipoOcorrencias as $TipoOcorrencia) {
                ?>
                <tr>
                    <td><?php echo $TipoOcorrencia->getNomTipoOcorrencia(); ?></td>
                    <td><?php echo $TipoOcorrencia->getIndAtivoTipoOcorrencia() == 1 ? 'ATIVO' : 'INATIVO'; ?></td>
                    <td><?php echo $TipoOcorrencia->getRegra() == 'S' ? 'SIM' : 'NÃO'; ?></td>
                    <!--<td><button type="button" class="fa fa-edit" data-toggle="modal" data-target="#modal_wizard" href="motivoDevolucaoEditar.php" onclick="javascript: carregaMotivoCadastro(<?php echo $TipoOcorrencia->getCodTipoOcorrencia(); ?>);"></button></td>-->
                    <td><button type="button" class="fa fa-edit" onclick="javascript: carregaMotivoCadastro(<?php echo $TipoOcorrencia->getCodTipoOcorrencia(); ?>);"></button></td>
                </tr>
                <?php
            }
            ?>          
        </tbody>
    </table>    

    <?php
} else if ($acao == 'GRAVAR') {
   
    $TipoOcorrencia->setNomTipoOcorrencia($_REQUEST['descMotivoDevolucao']);
    $TipoOcorrencia->setRegra($_REQUEST['toc_in_regra']);
    $TipoOcorrencia->setIndAtivoTipoOcorrencia($statusMotivo);
    $TipoOcorrenciaDao->incluirTipoOcorrencia($TipoOcorrencia);
	atualizaVetorPerfil($codMotivoDevolucao, $_REQUEST["checkPerfil"]);
    
 
}



