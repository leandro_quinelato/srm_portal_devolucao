function abrirCadastroItensNota() {

    if ($("#cli_no_razao_social").val()) {
        if ($("#serieNota").val()) {
            if ($("#numNota").val()) {
                if ($("#dataNota").val()) {
                    if ($("#volume").val()) {
                        $.ajax({
                            url: '/modules/ocorrencia/cadastroSobra/cadastroNotaSobra.php',
                            type: 'POST',
                            cache: false,
                            //data: $("#formCadOcorrencia").serialize(),
                            dataType: 'html',
                            error: function () {
                                alert('Erro ao Tentar acao');
                            },
                            success: function (texto) {
                                $("#modal_wizard").html(texto);                            
                                $("#modal_wizard").modal("show");                                    
                            }
                        });
                    } else {
                        $("#areaItensNota").html("");
                        alert("Favor informar a Qtd. Volume(s) à ser coletado");
                    }
                } else {
                    $("#areaItensNota").html("");
                    alert("Favor informar a Data da Nota!");
                }
            } else {
                $("#areaItensNota").html("");
                alert("Favor informar o Número da Nota!");
            }
        } else {
            $("#areaItensNota").html("");
            alert("Favor informar a Série da Nota!");
        }				
    } else {
        $("#areaItensNota").html("");
        alert("Favor preencher o campo cliente");
    }    
}

function enviarSoliciacao(){
    var r = confirm("Enviar Solicitação? \n\n" +
                    "IMPORTANTE!!! PARA SOLICITAÇÕES DE 'SOBRA DE MERCADORIA FÍSICA' NÃO SE DEVE EMITIR NOTA FISCAL DE DEVOLUÇÃO\n\n" +
                    "OBRIGATÓRIO!!! DEIXAR A SOBRA PRONTA PARA A COLETA, REALIZAR O ENVIO DOS SEGUINTES DOCUMENTOS JUNTO AO VOLUME FÍSICO: FORMULÁRIO DE SOBRA, NF DE VENDA(CÓPIA), E ORDEM DE COLETA DEVIDAMENTE PREENCHIDA.");
    if (r == false) {
        return false;
    }
    else{
        return true;
    }
}

function abrirNotaOrigemItem() {    
    $("#areaItensNota").html("Aguarde..."); 
    abrirCadastroItensNota();
    // if ($("#cli_no_razao_social").val()) {
    //     if (($("#serieNota").val() != "") && ($("#numNota").val() != "") && ($("#dataNota").val() != "")) {
            // $.ajax({
            //     url: 'modules/ocorrencia/cadastro/cadastroNotaOrigemItem.php',
            //     type: 'POST',
            //     cache: false,
            //     data: $("#formCadOcorrencia").serialize() + "&" + $("#formCadNotas").serialize(),
            //     dataType: 'html',
            //     error: function () {
            //         alert('Erro ao Tentar acao');
            //     },
            //     success: function (texto) {
            //         //if (texto.search(/Aviso/) != -1) {
            //         //    $("#areaItensNota").html(texto);                    
            //         //} else {
            //         //    $("#areaItensNota").html(""); 
            //             abrirCadastroItensNota();
            //         //}
            //     }
            // });
    //     } else {
    //         $("#areaItensNota").html("");
    //         alert("Favor preencher todos os dados da nota!");
    //     }
    // } else {
    //     $("#areaItensNota").html(""); 
    //     alert("Favor preencher o campo cliente");
    // } 
}

function fecharAguarde(){
    $("#areaItensNota").html(""); 
}

function apresenta_cliente(){
    //  $(function () {
    $.ajax({
        url: 'modules/ocorrencia/cadastro/cadastroOcorrenciaClienteDados.php',
        type: 'POST',
        cache: false,
        data: "cli_co_numero=" + $("#cli_co_numero").val(),
        dataType: 'html',
        error: function () {
            alert('Erro ao tentar acao');
        }
        ,
        success: function (texto) {
            $("#dadosCliente").html(texto);
        }
        ,
        beforeSend: function () {
            $("#dadosCliente").html("Carregando informa&ccedil;&otilde;es... ");
        }
    });
    // });
}

function apresenta_dados_produto(origin){ 
    var codigo = "";
    if($("#cod_barra").val() != null && $("#cod_barra").val() != "" && origin == 1){
        codigo = $("#cod_barra").val();
        document.getElementById("co_produto").value = "";   
    }   
    else if($("#co_produto").val() != null && $("#co_produto").val() != "" && origin == 2){
        codigo = $("#co_produto").val();
        document.getElementById("cod_barra").value = "";                
    }  
    
    $.ajax({
        url: 'modules/ocorrencia/cadastroSobra/cadastroItemSobra.php',
        type: 'POST',
        cache: false,
        //data: "co_produto=" + $("#co_produto").val(),
        data: $("#formCadOcorrencia").serialize() + "&" + "co_produto=" + codigo,
        dataType: 'html',
        error: function () {
            alert('Erro ao tentar acao');
        }
        ,
        success: function (texto) {
            $("#dadosItemNota").html(texto);
            document.getElementById("cod_barra").value = ""; 
            document.getElementById("co_produto").value = "";            
        }
        ,
        beforeSend: function () {
            $("#dadosItemNota").html("Carregando informa&ccedil;&otilde;es... ");
        }
    });

}

function cadastrarOcorrencia() {
    var tabela = document.getElementById('tblProdutos');    
    var vazio = false;
    for (i=0; i < tabela.rows.length; i++){
        var colunas = tabela.rows[i].childNodes;        
        //Coluna que tem o valor da quantidade
        var elementos = colunas[7].childNodes[0].value; 
        if(elementos == "" || elementos == "..."){
            vazio = true;
            break;
        }                
    }	   
    if(vazio == true){
        alert("Existe produto sem quantidade de devolução definida.");
    }
    else{     
        var arr = new Array();
        var employees = [];
        
        for (i = 1; i < tabela.rows.length; i++){
            var colunas = tabela.rows[i].childNodes;        
            //Coluna que tem o valor da quantidade
            employees.push({id:colunas[3].innerText,quantity:colunas[7].childNodes[0].value});                      
        }	
        var JSONString = JSON.stringify(employees);
        $.ajax({
            url: 'modules/ocorrencia/cadastroSobra/cadastroOcorrenciaSobraMain.php',
            type: 'POST',
            cache: false,
            data: $("#formCadOcorrencia").serialize() + "&" + $("#formCadItemSobra").serialize() + "&" + "tblProdutos=" + JSONString + "&acao=EDITARPRODUTO" ,
            dataType: 'html',
            error: function () {
                $("#areaItensNota").html("");
                alert('Erro ao Tentar acao');
            },
            success: function (texto) {
                if (texto.search(/ERRO OCORRENCIA/) != -1) {
                    $("#areaItensNota").html("");
                    alert("Ocorreu um erro ao inserir a ocorrência!");
                } else if (texto.search(/ERRO ALTERAR/) != -1) {
                    $("#areaItensNota").html("");
                    alert("Ocorreu um erro ao alterar o registro!");
                }
                else if (texto.search(/ERRO INSERIR NOTA/) != -1) {
                    $("#areaItensNota").html("");
                    alert("Ocorreu um erro ao inserir a nota!");
                }
                else if (texto.search(/ERRO AO INSERIR ITEM/) != -1) {
                    $("#areaItensNota").html("");
                    alert("Ocorreu um erro ao inserir o item!");
                }
                else {
                    alert("Cadastro realizado com sucesso.");                    
                    $("#modal_wizard").modal('hide');
                    ListarNotaOcorrencia();
                }

            }
        });
    }       
}

function ListarNotaOcorrencia() {

    $.ajax({
        url: 'modules/ocorrencia/cadastroSobra/cadastroOcorrenciaSobraMain.php',
        type: 'POST',
        cache: false,
        data: $("#formCadOcorrencia").serialize() + "&acao=LISTARNOTAS",
        dataType: 'html',
        error: function () {
            alert('Erro ao Tentar acao');
        },
        success: function (texto) {

            if (texto.search(/ERRO/) != -1) {
                $("#areaItensNota").html("");
                $("#areaListaNotaOco").html(texto);//comenta
            } else {            
                $("#areaListaNotaOco").html(texto);
                $("#areaItensNota").html("");
            }

        }
    });
}

function EnvioEmail(){
    $.ajax({
        url: 'rotinas/EnvioEmailFechamentoOcorrencia.php',
        type: 'POST',
        cache: false,
        data: $("#formCadOcorrencia").serialize(),
        dataType: 'html',
        error: function () {
            alert('Erro ao Tentar acao');
        },
        success: function (texto) {
            if (texto.search(/ERRO/) != -1) {
                $("#areaListaNotaOco").html(texto);
                $("#areaItensNota").html(""); 
                alert("Ocorreu um erro ao enviar o email!");
                //comenta
            } else {
                $("#areaItensNota").html("");                 
                alert("Ocorrencia gerada com sucesso!");          
                location.reload();
            }
        }
    });
}

function FinalizaCadastroOcorrencia() {
    var result = enviarSoliciacao();    
    if(result == true){
        $("#areaItensNota").html("Aguarde...");     
        if ($("#cli_no_razao_social").val()) {
            if ($("#serieNota").val()) {
                if ($("#numNota").val()) {
                    if ($("#dataNota").val()) {
                        $.ajax({
                            url: 'modules/ocorrencia/cadastroSobra/cadastroOcorrenciaSobraMain.php',
                            type: 'POST',
                            cache: false,
                            data: $("#formCadOcorrencia").serialize() + "&acao=FINALCAD",
                            dataType: 'html',
                            error: function () {
                                alert('Erro ao Tentar acao');
                            },
                            success: function (texto) {
                                if (texto.search(/ERRO/) != -1) {
                                    $("#areaItensNota").html(""); 
                                    alert("Ocorreu um erro ao Finalizar o cadastro!");
                                    $("#areaListaNotaOco").html(texto);//comenta
                                } else {                                    
                                    //$("#areaListaNotaOco").html(texto);     
                                    //$("#areaItensNota").html("");                 
                                    //alert("Ocorrencia gerada com sucesso!");          
                                    //location.reload();                               
                                    EnvioEmail();                
                                    // location.reload();
                                }
                            }
                        });
                    } else {
                        $("#areaItensNota").html("");
                        alert("Favor informar a Data da Nota!");
                    }
                } else {
                    $("#areaItensNota").html("");
                    alert("Favor informar o Número da Nota!");
                }
            } else {
                $("#areaItensNota").html("");
                alert("Favor informar a Série da Nota!");
            }				
        } else {
            $("#areaItensNota").html("");
            alert("Favor preencher o campo cliente");
        } 
    }
}

function deletaItemOcorrencia(produto, ocorrencia) {
    $("#areaItensNota").html("Aguarde..."); 
    if (confirm("Deseja realmente excluir o produto da Ocorrência?")) {
        $.ajax({
            url: 'modules/ocorrencia/cadastroSobra/cadastroOcorrenciaSobraMain.php',
            type: 'POST',
            cache: false,
            data: $("#formCadOcorrencia").serialize() + "&cod_produto=" + produto + "&cod_ocorrencia=" + ocorrencia + "&acao=DELNOTA",            
            error: function () {
                $("#areaItensNota").html("");
                alert('Erro ao Tentar acao');
            },
            success: function (texto) {
                $("#areaItensNota").html("");
                $("#areaListaNotaOco").html(texto);
                ListarNotaOcorrencia();
            }
        });
    }
}

function editarItemCadastrado(produto, ocorrencia, volume) {
    $("#areaItensNota").html("Aguarde..."); 
    if ($("#cli_no_razao_social").val()) {
        if ($("#serieNota").val()) {
            if ($("#numNota").val()) {
                if ($("#dataNota").val()) {
                    $.ajax({
                        url: '/modules/ocorrencia/cadastroSobra/editarItemSobra.php',
                        type: 'POST',
                        cache: false,
                        data: $("#formCadOcorrencia").serialize() + "&co_produto=" + produto + "&cod_ocorrencia=" + ocorrencia + "&volume=" + volume,
                        dataType: 'html',
                        error: function () {
                            $("#areaItensNota").html("");
                            alert('Erro ao Tentar acao');
                        },
                        success: function (texto) {
                            $("#modal_wizard").html(texto);
                            $("#modal_wizard").modal("show");
                        }
                    });
                } else {
                    $("#areaItensNota").html("");
                    alert("Favor informar a Data da Nota!");
                }
            } else {
                $("#areaItensNota").html("");
                alert("Favor informar o Número da Nota!");
            }
        } else {
            $("#areaItensNota").html("");
            alert("Favor informar a Série da Nota!");
        }				
    } else {
        $("#areaItensNota").html("");
        alert("Favor preencher o campo cliente");
    }    
}

function editarItemOcorrencia() {
    
    if (($("#co_produto").val() != "") || ($("#cod_barra").val() != "")){
        if (($("#pro_no_descricao").val() != "") && ($("#volume").val() != "") && ($("#qnt_unitaria").val() != "")) {
            $.ajax({
                url: 'modules/ocorrencia/cadastroSobra/cadastroOcorrenciaSobraMain.php',
                type: 'POST',
                cache: false,
                data: $("#formCadOcorrencia").serialize() + "&" + $("#formCadItemSobra").serialize() + "&acao=EDITAR",
                dataType: 'html',
                error: function () {
                    $("#areaItensNota").html("");                    
                    alert('Erro ao Tentar acao');
                },
                success: function (texto) {

                    if (texto.search(/ERRO ALTERAR/) != -1) {
                        $("#areaItensNota").html("");
                        alert("Ocorreu um erro ao alterar a ocorrência!");
                    } else if (texto.search(/ERRO ALTERAR/) != -1) {
                        $("#areaItensNota").html("");
                        alert("Ocorreu um erro ao alterar o registro!");
                    }
                    else if (texto.search(/ERRO ALTERAR NOTA/) != -1) {
                        $("#areaItensNota").html("");
                        alert("Ocorreu um erro ao alterar a nota!");
                    }
                    else if (texto.search(/ERRO AO ALTERAR ITEM/) != -1) {
                        $("#areaItensNota").html("");
                        alert("Ocorreu um erro ao alterar o item!");
                    }
                    else {
                        alert("Cadastro realizado com sucesso.");                    
                        $("#modal_wizard").modal('hide');
                        ListarNotaOcorrencia();
                    }

                }
            });
        } else {
            alert("Favor preencher todos os dados da nota!");
        }    
    } else {
        alert("É obrigatório o preenchimento do Cód. de Barra ou Cód. do Produto.");
    }
}