
function consultarOcorrencia() {


    if ( $("#conCodOcorrencia").val() != "" ||
         $("#conCodCliente").val() != ""    || 
         $("#conNotaDev").val() != ""       || 
         $("#conNota").val() != ""          || 
         $("#conRede").val() != ""          || 
         $("#ced_co_numero").val() != "TOD" || 
         $("#rot_co_numero").val() != "TOD" || 
         $("#statusOcorrencia").val() != "TOD" || 
         $("#passoOcorrencia").val() != "TOD" || 
         
         ($("#dataInicial").val() != "" && $("#dataFinal").val() != "")

       ) {
        //    if ($("#dataInicial").val() != "" && $("#dataFinal").val() != "") {
        $.ajax({
            url: 'modules/ocorrencia/consulta/consultaOcorrenciaMain.php',
            type: 'POST',
            cache: false,
            data: $("#formFiltroConsulta").serialize() + "&acao=LISOCO",
            dataType: 'html',
            error: function () {
                alert('Erro ao Tentar acao');
            },
            success: function (texto) {

                if (texto.search(/ERRO/) != -1) {
                    $("#retornoConsOcorrencia").html(texto);
                } else {
                    $("#retornoConsOcorrencia").html(texto);
                }

            }
        });
    } else {
        alert("Atenção! Favor preencher dados para facilitar a consulta.");
    }
        //    } else {
        //        alert("Atenção! Favor preencher a data inicial e final.");
        //    }
}

function    detalheOcorrencia(codOcorrencia) {

    $.ajax({
        url: 'modules/ocorrencia/consulta/detalheOcorrencia.php',
        type: 'POST',
        cache: false,
        data: $("#formFiltroConsulta").serialize() + "&codOcorrencia=" + codOcorrencia,
        dataType: 'html',
        error: function () {
            alert('Erro ao Tentar acao');
        },
        success: function (texto) {
            $("#modal_ocorrencia").html(texto);
            $("#modal_ocorrencia").modal("show");
        }
    });

}

function    emailColeta(codOcorrencia) {

    $.ajax({
        url: 'modules/ocorrencia/consulta/emailColetaOcorrencia.php',
        type: 'POST',
        cache: false,
        data: $("#formFiltroConsulta").serialize() + "&codOcorrencia=" + codOcorrencia,
        dataType: 'html',
        error: function () {
            alert('Erro ao Tentar acao');
        },
        success: function (texto) {
            $("#modal_mailcoleta").html(texto);
            $("#modal_mailcoleta").modal("show");
        }
    });

}

function    enviarEmailColeta() {

    $.ajax({
        url: 'modules/utilitario/ordemColeta/enviarOrdemColeta.php',
        type: 'POST',
        cache: false,
        data: $("#formEnviarColeta").serialize(),
        dataType: 'html',
        error: function () {
            alert('Erro ao Tentar acao');
        },
        success: function (texto) {
            $("#modal_mailcoleta").html(texto);
            $("#modal_mailcoleta").modal("hide");
        }
    });

}

function confereDataPassoDevolucao() {
    var dataInicial = $('#dtReceb').val();
    var dataFinal = $('#dtConf').val();
    var data1;
    var data2;


    data1 = parseInt(dataInicial.split("/")[2].toString() + dataInicial.split("/")[1].toString() + dataInicial.split("/")[0].toString());
    data2 = parseInt(dataFinal.split("/")[2].toString() + dataFinal.split("/")[1].toString() + dataFinal.split("/")[0].toString());

    if (data1 > data2) {
        return false;
    } else {
        return true;
    }
}

function aprovarOcorrenciaPasso(codOcorrencia, codPasso, nomeForm) {

    if (codPasso == 1 && $('#descGerente').val() == "") {
        alert("Atenção! Favor preencher o campo descrição!");
        $('#descGerente').focus();
        $("#descGerente").css("background-color", "yellow");

    } else if (codPasso == 2 && $('#qtdVolumeTrans').val() == "") {
        alert("Atenção! Favor preencher o campo volume!");
        $('#qtdVolumeTrans').focus();
        $("#qtdVolumeTrans").css("background-color", "yellow");

    } else if (codPasso == 2 && $('#descTransportador').val() == "") {
        alert("Atenção! Favor preencher o campo Descrição!");
        $('#descTransportador').focus();
        $("#descTransportador").css("background-color", "yellow");

    } else if (codPasso == 3 && $('#qtdVolumeRecebe').val() == "") {
        alert("Atenção! Favor preencher o campo volume!");
        $('#qtdVolumeTrans').focus();
        $("#qtdVolumeTrans").css("background-color", "yellow");

    } else if (codPasso == 3 && $('#descRecebimento').val() == "") {
        alert("Atenção! Favor preencher o campo Descrição!");
        $('#descTransportador').focus();
        $("#descTransportador").css("background-color", "yellow");
    }
    else if (codPasso == 4 && $('#descDevolucao').val() == "") {
        alert("Atenção! Favor preencher o campo Descrição!");
        $('#descDevolucao').focus();
        $("#descDevolucao").css("background-color", "yellow");
    }
    else if (codPasso == 4 && $('#dtConf').val() == "") {
        alert("Digite a data de conferencia");
        $('#dtConf').focus();
        $("#dtConf").css("background-color", "yellow");
    }
    else if (codPasso == 4 && $('#dtReceb').val() == "") {
        alert("Digite a data de Recebimento");
        $('#dtReceb').focus();
        $("#dtReceb").css("background-color", "yellow");
    } else if (codPasso == 4 && (!confereDataPassoDevolucao())) {
        alert("ATENÇÃO!A Data de Recebimento não pode ser maior que a de Conferencia.");

    } else if (codPasso == 2 && $('#descFiscal').val() == "") {
        alert("Atenção! Favor preencher o campo Descrição!");
        $('#descFiscal').focus();
        $("#descFiscal").css("background-color", "yellow");

    }


    else {

        $.ajax({
            url: 'modules/ocorrencia/consulta/passo/passoAprovacaoMain.php',
            type: 'POST',
            cache: false,
            data: $("#FormPasso" + nomeForm).serialize() + '&codOcorrencia=' + codOcorrencia + '&codPasso=' + codPasso,
            dataType: 'html',
            error: function () {
                alert('Erro ao Tentar acao');
            },
            success: function (texto) {
                $("#retornoAprovacao").html(texto);

            },
            beforeSend: function () {
                $("#retornoAprovacao").html("Aguarde...");
            }
        });


    }

}


function aprovarPassoGerente(codOcorrencia, codPasso, nomeForm) {

    if (codPasso == 1 && $('#descGerente').val() == "") {
        alert("Atenção! Favor preencher o campo descrição!");
        $('#descGerente').focus();
        $("#descGerente").css("background-color", "yellow");
    }
    else {

        var valorDevolucao = $('#FormatValDev').val();

        if (confirm("Atenção! Ocorrência: " + codOcorrencia + " possui valor de R$ " + valorDevolucao + ". Deseja Realmente Aprovar?")) {

            $.ajax({
                url: 'modules/ocorrencia/consulta/passo/passoAprovacaoMain.php',
                type: 'POST',
                cache: false,
                data: $("#FormPasso" + nomeForm).serialize() + '&codOcorrencia=' + codOcorrencia + '&codPasso=' + codPasso,
                dataType: 'html',
                error: function () {
                    alert('Erro ao Tentar acao');
                },
                success: function (texto) {
                    $("#retornoAprovacao").html(texto);

                },
                beforeSend: function () {
                    $("#retornoAprovacao").html("Aguarde...");
                }
            });

        }
    }

}

function    areaConferente(acao) {

    $.ajax({
        url: 'modules/ocorrencia/consulta/passo/carregaAreaConferente.php',
        type: 'POST',
        cache: false,
        data: $("#FormPassoDevolucao").serialize() + "&acao=" + acao,
        dataType: 'html',
        error: function () {
            alert('Erro ao Tentar acao');
        },
        success: function (texto) {

            $("#areaConferente").append(texto);
        }
    });

}

function responsavelDevolucao(doc_co_numero, cli_co_numero)
{
    $(function () {
        $("#step-4").load("modules/ocorrencia/consulta/passo/passoDefinirResponsavel.php?doc_co_numero=" + doc_co_numero + "&cli_co_numero=" + cli_co_numero);
    });
}

function industria_show(index) {

    var codResponsavel = $('#trp_co_numero_' + index).val();
    var responsavel = $('#trp_no_responsavel_' + codResponsavel + '_' + index).val();
    var tpOrigemVenda = $('#tp_origem_venda_' + index).val();
    //        alert (codResponsavel);
    //        alert (responsavel);
    //        alert (tpOrigemVenda);
    //        
    if (responsavel == 'INDU' || ((responsavel == 'COM') && (tpOrigemVenda == 'EP' || tpOrigemVenda == 'GI' || tpOrigemVenda == 'AZ' || tpOrigemVenda == 'OR' || tpOrigemVenda == 'GL' || tpOrigemVenda == 'N' || tpOrigemVenda == 'GD' || tpOrigemVenda == 'GH' || tpOrigemVenda == 'GM'))) {
        $('#list-fabricante_' + index).removeClass('industria_hidden').addClass('industria_visible');
    } else {
        $('#list-fabricante_' + index).removeClass('industria_visible').addClass('industria_hidden');
    }

}

function industria_hidden(index) {
    $(function () {
        $('#list-fabricante_' + index).removeClass('industria_visible').addClass('industria_hidden');
    });
}


function definicaoResponsabilidadeAutomatica(qntd) {
    var cont;
    var validate = true;
    var msg = "";
    var resp = 0;

    var codResponsavel = 0;//$('#trp_co_numero_'+index).val();
    var responsavel = 0;//$('#trp_no_responsavel_'+codResponsavel+'_'+index).val();
    var tpOrigemVenda = 0;

    for (i = 0; i < qntd; i++) {

        tpOrigemVenda = $('#tp_origem_venda_' + i).val();
        codResponsavel = $('#trp_co_numero_' + i).val();
        responsavel = $('#trp_no_responsavel_' + codResponsavel + '_' + i).val();
        ContTexto = document.forms['frmResponsabilidade'].elements['nto_tx_responsavel_' + i].value.length;
        cont = document.forms['frmResponsabilidade'].elements['tp_origem_venda_' + i].length;

        if (responsavel == 'INDU' || ((responsavel == 'COM') && (tpOrigemVenda == 'EP' || tpOrigemVenda == 'GI' || tpOrigemVenda == 'AZ' || tpOrigemVenda == 'OR' || tpOrigemVenda == 'GL' || tpOrigemVenda == 'N' || tpOrigemVenda == 'GD' || tpOrigemVenda == 'GH' || tpOrigemVenda == 'GM'))) {
            if (document.forms['frmResponsabilidade'].elements['fab_co_numero_' + i].value == "") {
                msg += "\t >> Selecione um fabricante \n";
                validate = false;
            }
        }

        if (codResponsavel == "") {
            msg += "\t >> Selecione o Motivo \n";
            validate = false;
        }
        if (ContTexto == 0) {
            msg += "\t >> Digite a descricao \n";
            validate = false;
        }
    }

    if (validate) {
        $(function () {
            $.ajax({
                url: 'modules/ocorrencia/consulta/passo/passoDefinirResponsavelMain.php',
                type: 'POST',
                cache: false,
                data: $("#frmResponsabilidade").serialize(),
                dataType: 'html',
                error: function () {
                    alert('Erro ao Tentar acao');
                },
                success: function (texto) {
                    $("#retornoDifinirMsg").html(texto);
                },
                beforeSend: function () {
                    $("#retornoDifinirMsg").html("Aguarde...");
                }
            });
        });
    //                alert('ok!');
    } else {
        alert(msg);
    }
}

function avancarFiscal() {

}

function    abrirReprova(codOcorrencia, codPasso, nomePasso) {

    $.ajax({
        url: 'modules/ocorrencia/consulta/passo/passoReprovaForm.php',
        type: 'POST',
        cache: false,
        data: "&codOcorrencia=" + codOcorrencia + "&codPasso=" + codPasso + "&nomePasso=" + nomePasso,
        dataType: 'html',
        error: function () {
            alert('Erro ao Tentar acao');
        },
        success: function (texto) {
            $("#modalReprova" + nomePasso).html(texto);
            $("#modalReprova" + nomePasso).modal("show");
        }
    });

}

function reprovarOcorrenciaPasso(codOcorrencia, codPasso, nomePasso) {

    if (codPasso == 1 && $('#descGerente').val() == "") {
        alert("Atenção! Favor preencher o campo descrição!");
        $('#descGerente').focus();
        $("#descGerente").css("background-color", "yellow");

    } else if (codPasso == 2 && $('#descTransportador').val() == "") {
        alert("Atenção! Favor preencher o campo Descrição!");
        $('#descTransportador').focus();
        $("#descTransportador").css("background-color", "yellow");

    } else if (codPasso == 3 && $('#descRecebimento').val() == "") {
        alert("Atenção! Favor preencher o campo Descrição!");
        $('#descTransportador').focus();
        $("#descTransportador").css("background-color", "yellow");
    }
    else if (codPasso == 4 && $('#descDevolucao').val() == "") {
        alert("Atenção! Favor preencher o campo Descrição!");
        $('#descDevolucao').focus();
        $("#descDevolucao").css("background-color", "yellow");
    } else if (codPasso == 5 && $('#descFiscal').val() == "") {
        alert("Atenção! Favor preencher o campo Descrição!");
        $('#descFiscal').focus();
        $("#descFiscal").css("background-color", "yellow");
    }


    else {
	
		if ($("#solicitante"+nomePasso).val() != "") {
			if ($("#departamento"+nomePasso).val() != "") {
				if ($("#motivoReprova"+nomePasso).val() != "TOD") {
	
					$.ajax({
						url: 'modules/ocorrencia/consulta/passo/passoReprovacaoMain.php',
						type: 'POST',
						cache: false,
						data: $("#FormPasso" + nomePasso).serialize() + "&" + $("#formReprova" + nomePasso).serialize() + '&codOcorrencia=' + codOcorrencia + '&codPasso=' + codPasso,
						dataType: 'html',
						error: function () {
							alert('Erro ao Tentar acao');
						},
						success: function (texto) {
							$("#retornoReprova" + nomePasso).html(texto);
							$('#modalReprova' + nomePasso).modal('hide');

						},
						beforeSend: function () {
							$("#retornoReprova" + nomePasso).html("Aguarde...");
						}
					});

				}else{
					alert("Favor Preencher o Motivo Reprova");
				}
			}else{
				alert("Favor Preencher o Departamento");
			}
		}else{
			alert("Favor Preencher o Solicitante");
		}
    }

}


function chamaFormPendencia(codOcorrencia, codPasso, nomePasso, acao) {

    $.ajax({
        url: 'modules/ocorrencia/consulta/passo/passoPendenciaForm.php',
        type: 'POST',
        cache: false,
        data: "&codOcorrencia=" + codOcorrencia + "&codPasso=" + codPasso + "&nomePasso=" + nomePasso + "&acao=" + acao,
        dataType: 'html',
        error: function () {
            alert('Erro ao Tentar acao');
        },
        success: function (texto) {
            $("#modalReprova" + nomePasso).html(texto);
            $("#modalReprova" + nomePasso).modal("show");
        }
    });

}

function tratarPendencia(codOcorrencia, codPasso, nomePasso, acao) {

	if ($("#solicitantePendente").val() != "") {
		if ($("#departamentoPendente").val() != "") {
			if ($("#descPendente").val() != "") {
				$.ajax({
					url: 'modules/ocorrencia/consulta/passo/passoPendenciaMain.php',
					type: 'POST',
					cache: false,
					data: $("#FormPasso" + nomePasso).serialize() + "&" + $("#formPendente").serialize() + '&codOcorrencia=' + codOcorrencia + '&codPasso=' + codPasso + '&acao=' + acao,
					dataType: 'html',
					error: function () {
						alert('Erro ao Tentar acao');
					},
					success: function (texto) {
						$("#retornoPendente").html(texto);
						$('#modalReprova' + nomePasso).modal('hide');

					},
					beforeSend: function () {
						$("#retornoPendente").html("Aguarde...");
					}
				});
			}else{
				alert("Favor Preencher a Descrição");
			}
		}else{
			alert("Favor Preencher o Departamento");
		}
	}else{
		alert("Favor Preencher o Solicitante");
	}
}


function aprensentaNfDev(tipoDevolucao) {

    // if (tipoDevolucao == 1) {
         $('#areaInfoNfDev').show();
         $('#areaUploadDev').show();
    // } else {
    //     $('#areaInfoNfDev').hide();
    //     $('#areaUploadDev').hide();
    // }

}

function uploadArquivo(idArquivo) {

    new AjaxUpload($('#importaArquivo'),
            {
                action: 'modules/ocorrencia/consulta/teste.php',
                name: 'uploadfile',
                onSubmit: function (file, ext)
                {
                    if (!(ext && /^(jpg|gif|png|txt|pdf)$/.test(ext.toLowerCase())))
                    {
                        alert("Tipo de arquivo invalido!");
                        return false;
                    }

                    this.setData({
                        idArquivoTemp: idArquivo
                    });

                    $('#msgUploadArquivo').html('<p id="msg">Carregando...</p>');
                },
                onComplete: function (file, response)
                {
                    $('#msgUploadArquivo').html(response);
    //                        $('#excluirImgOficial').val('N');
                }
            });
}

function excluirArquivoNotaDev(dirImagem, tipoImagem, imagemTemp) {


    if (confirm("Deseja realmente excluir este Arquivo?")) {
        $(function () {
            $.ajax({
                //    url: 'modules/ocorrencia/consulta/uploadNotadevolucao.php',
                url: 'modules/ocorrencia/consulta/teste.php',
                type: 'POST',
                cache: false,
                data: "dirImagem=" + dirImagem
                        + "&tipoImagem=" + tipoImagem
                        + "&imagemTemp=" + imagemTemp
                        + "&acao=EXCLUIR",
                dataType: 'html',
                error: function () {
                    alert('Erro!');
                },
                success: function (texto) {
                    $('#msgUploadArquivo').html(texto);

    //                    if (tipoImagem == "OFICIAL"){
    //                        $('#excluirImgOficial').val('S');
    //                    }
                }
            });
        });
    }
}

function adicionarNotaDevolucao(codOcorrencia) {
    if ($('#serieDev').val() != "" && $('#numDev').val() != "" && $('#dataDev').val() != "") {
        $(function () {
            $.ajax({
                url: 'modules/ocorrencia/consulta/detalheAnexoMain.php',
                type: 'POST',
                cache: false,
                data: $("#FormDetalheAnexo").serialize() + "&codOcorrencia=" + codOcorrencia + "&acao=GRAVAR",
                dataType: 'html',
                error: function () {
                    alert('Erro ao Tentar acao');
                },
                success: function (texto) {
                    if (texto.search(/ERRO/) == -1) {
                        $('#incluirNFD').hide();
                        alert('Nota Devolução Cadastrada com sucesso!');
						alert('OBRIGATÓRIO!!! DEIXAR A DEVOLUÇÃO PRONTA PARA A COLETA, REALIZAR O ENVIO DOS SEGUINTES DOCUMENTOS JUNTO AO VOLUME FÍSICO: NF DEVOLUÇÃO e ou CARTA (CLIENTES HOSPITALARES), NF DE VENDA (CÓPIA), E ORDEM DE COLETA DEVIDAMENTE PREENCHIDA.');
                        $("#retornoDetalheAnexo").html(texto);                        
                        //$('#areaInfoNfDev').hide();
                        //$('#areaUploadDev').hide();
                        //listarArquivosOcorrencia(codOcorrencia);
                    } else {
                        alert('Erro ao Cadastrar nota!');
                    }

                },
                beforeSend: function () {
                    $("#retornoDetalheAnexo").html("Aguarde...");
                }
            });
        });
    } else {
        alert("Favor informar todos os dados para enviar a Nota!");
    }
}

function adicionarAnexoNotaDevolucao(codOcorrencia) {
    if ($('#observacaoDev').val() != "") {
        $(function () {
            $.ajax({
                url: 'modules/ocorrencia/consulta/detalheAnexoMain.php',
                type: 'POST',
                cache: false,
                data: $("#FormDetalheAnexo").serialize() + "&codOcorrencia=" + codOcorrencia + "&acao=GRAVAR_ARQUIVO",
                dataType: 'html',
                error: function () {
                    alert('Erro ao Tentar acao');
                },
                success: function (texto) {
                    if (texto.search(/ERRO/) == -1) {
                        alert('Anexo Cadastrado com sucesso!');
						//alert('OBRIGATÓRIO!!! DEIXAR A DEVOLUÇÃO PRONTA PARA A COLETA, REALIZAR O ENVIO DOS SEGUINTES DOCUMENTOS JUNTO AO VOLUME FÍSICO: NF DEVOLUÇÃO e ou CARTA (CLIENTES HOSPITALARES), NF DE VENDA (CÓPIA), E ORDEM DE COLETA DEVIDAMENTE PREENCHIDA.');
                        $("#retornoDetalheAnexo").html(texto);                        
                        listarArquivosOcorrencia(codOcorrencia);
                    } else {
                        alert('Erro ao Cadastrar nota!');
                    }
                },
                beforeSend: function () {
                    $("#retornoDetalheAnexo").html("Aguarde...");
                }
            });
        });
    } else {
        alert("Favor informar a observação para anexar um arquivo!");
    }
}

function listarArquivosOcorrencia(codOcorrencia) {

    $(function () {
        $.ajax({
            url: 'modules/ocorrencia/consulta/detalheAnexoMain.php',
            type: 'POST',
            cache: false,
            data: $("#FormDetalheAnexo").serialize() + "&codOcorrencia=" + codOcorrencia + "&acao=LISTAR",
            dataType: 'html',
            error: function () {
                alert('Erro ao Tentar acao');
            },
            success: function (texto) {

                $("#areaListaArquivos").html(texto);


            }
        });
    });

}

function ExcluirArquivoOcorrencia(codOcorrencia, codigoTipoArquivo, nomeTipoArquivo) {

    if (confirm("Deseja Realmente Excluir este Arquivo?")) {
        $(function () {
            $.ajax({
                url: 'modules/ocorrencia/consulta/detalheAnexoMain.php',
                type: 'POST',
                cache: false,
                data: $("#FormDetalheAnexo").serialize() + "&codOcorrencia=" + codOcorrencia + "&codigoTipoArquivo=" + codigoTipoArquivo + "&nomeTipoArquivo=" + nomeTipoArquivo,
                dataType: 'html',
                error: function () {
                    alert('Erro ao Tentar acao');
                },
                success: function (texto) {
                    alert(texto);
                    listarArquivosOcorrencia(codOcorrencia);

                }
            });
        });
    }
}


function  verificaMotivo(motivoDevolver, respDevolver) {
    alert("teste: " + motivoDevolver + " ... res: " + respDevolver);


    if (motivoDevolver.trim() != respDevolver.trim()) {
        alert("Atenção! Motivo informado esta diferente do motivo da devolução, favor corrigir");
    }

}

function downloadArquivos(codOcorrencia, codigoTipoArquivo, nomeTipoArquivo) {

    jn = window.open("../modules/utilitario/downloadArquivos/downloadArquivos.php?codOcorrencia=" + codOcorrencia + "&codigoTipoArquivo=" + codigoTipoArquivo + "&nomeTipoArquivo=" + nomeTipoArquivo, "Relatorio", "height = 300, width = 300,scrollbars = no,location = no,toolbar = no,menubar=yes");
//    jn = window.open("/servimed/webpoint/servimed.com.br/portaldevolqa/public/modules/utilitario/downloadArquivos/downloadArquivos.php?codOcorrencia=" + codOcorrencia + "&codigoTipoArquivo=" + codigoTipoArquivo, "Relatorio", "height = 300, width = 300,scrollbars = no,location = no,toolbar = no,menubar=yes");

}

function abrirReativar(codOcorrencia, codPasso) {

    $.ajax({
        url: 'modules/ocorrencia/consulta/passo/passoReativarForm.php',
        type: 'POST',
        cache: false,
        data: "&codOcorrencia=" + codOcorrencia + "&codPasso=" + codPasso,
        dataType: 'html',
        error: function () {
            alert('Erro ao Tentar acao');
        },
        success: function (texto) {
            $("#modalReativarDevolucao").html(texto);
            $("#modalReativarDevolucao").modal("show");
        }
    });

}

function reativarOcorrencia() {

//    alert("reativar ocorrencia!");

    $.ajax({
        url: 'modules/ocorrencia/consulta/passo/passoReativarMain.php',
        type: 'POST',
        cache: false,
        data: $("#formReativar").serialize(),
        dataType: 'html',
        error: function () {
            alert('Erro ao Tentar acao');
        },
        success: function (texto) {
            $('#modalReativarDevolucao').modal('hide');
            alert("Ocorrência reativada com sucesso");
            $("#retornoReativar").html(texto);

        },
        beforeSend: function () {
            $("#retornoReativar").html("Aguarde...");
        }
    });
}

function    abrirCancelar(codOcorrencia, codPasso) {

    $.ajax({
        url: 'modules/ocorrencia/consulta/passo/passoCancelarForm.php',
        type: 'POST',
        cache: false,
        data: "&codOcorrencia=" + codOcorrencia + "&codPasso=" + codPasso,
        dataType: 'html',
        error: function () {
            alert('Erro ao Tentar acao');
        },
        success: function (texto) {
            $("#modalCancelarDevolucao").html(texto);
            $("#modalCancelarDevolucao").modal("show");
        }
    });

}

function cancelarOcorrencia() {

    if ($("#solicitanteCancelar").val() != "") {
        if ($("#departamentoCancelar").val() != "") {
            if ($("#descCancelar").val() != "") {
				
				$.ajax({
					url: 'modules/ocorrencia/consulta/passo/passoCancelarMain.php',
					type: 'POST',
					cache: false,
					data: $("#formCancelar").serialize(),
					dataType: 'html',
					error: function () {
						alert('Erro ao Tentar acao');
					},
					success: function (texto) {
						$('#modalCancelarDevolucao').modal('hide');
						alert("Ocorrência cancelada com sucesso");
						$("#retornoCancelar").html(texto);

					},
					beforeSend: function () {
						$("#retornoCancelar").html("Aguarde...");
					}
				});
				
            } else {
                alert("Favor Preencher a Descrição!");
            }
        } else {
            alert("Favor Preencher o Departamento!");
        }
    } else {
        alert("Favor Preencher o Solicitante!");
    }
}


function abrirReentrega(codOcorrencia, codPasso) {
	
    $.ajax({
        url: 'modules/ocorrencia/consulta/passo/passoReentregarForm.php',
        type: 'POST',
        cache: false,
        data: "&codOcorrencia=" + codOcorrencia + "&codPasso=" + codPasso,
        dataType: 'html',
        error: function () {
            alert('Erro ao Tentar acao');
        },
        success: function (texto) {
            $("#modalReentregar").html(texto);
            $("#modalReentregar").modal("show");
        }
    });

}
function reentregarOcorrencia() {

    if ($("#solicitanteReentregar").val() != "") {
        if ($("#departamentoReentregar").val() != "") {
            if ($("#descReentregar").val() != "") {

				$.ajax({
					url: 'modules/ocorrencia/consulta/passo/passoReentregarMain.php',
					type: 'POST',
					cache: false,
					data: $("#formReentregar").serialize(),
					dataType: 'html',
					error: function () {
						alert('Erro ao Tentar acao');
					},
					success: function (texto) {
						$('#modalReentregar').modal('hide');
						alert("Ocorrência enviada para Reentrega");
						$("#retornoReentregar").html(texto);

					},
					beforeSend: function () {
						$("#retornoReentregar").html("Aguarde...");
					}
				});
	
            } else {
                alert("Favor Preencher a Descrição!");
            }
        } else {
            alert("Favor Preencher o Departamento!");
        }
    } else {
        alert("Favor Preencher o Solicitante!");
    }	
	
}

function alterarDetalhesOcorrencia(codOcorrencia) {


    if ($("#motivoAlterar").val() != "") {
        if ($("#descAlterar").val() != "") {
            if ($("#rotaAlterar").val() != "") {
                if ($("#volumeAlterar").val() != "") {
                    $.ajax({
                        url: 'modules/ocorrencia/consulta/consultaOcorrenciaMain.php',
                        type: 'POST',
                        cache: false,
                        data: "&codOcorrencia=" + codOcorrencia + "&motivoAlterar=" + $("#motivoAlterar").val() + "&descAlterar=" + $("#descAlterar").val() + "&rotaAlterar=" + $("#rotaAlterar").val() + "&volumeAlterar=" + $("#volumeAlterar").val(),
                        dataType: 'html',
                        error: function () {
                            alert('Erro ao Tentar acao');
                        },
                        success: function (texto) {
                            alert("Ocorrência alterada com sucesso");
                            detalheOcorrencia(codOcorrencia)

                        }
                    });

                } else {
                    alert("Favor Preencher o Volume!");
                }
            } else {
                alert("Favor Preencher a Rota!");
            }
        } else {
            alert("Favor Preencher a Descrição!");
        }
    } else {
        alert("Favor Preencher o Motivo!");
    }

}

function alterarNotaDevolucao(codOcorrencia) {


    if ($('#serieDev').val() != "" && $('#numDev').val() != "" && $('#dataDev').val() != "") {
        $(function () {
            $.ajax({
                url: 'modules/ocorrencia/consulta/detalheAnexoMain.php',
                type: 'POST',
                cache: false,
                data: $("#FormDetalheAnexo").serialize() + "&codOcorrencia=" + codOcorrencia + "&acao=ALTERAR",
                dataType: 'html',
                error: function () {
                    alert('Erro ao Tentar acao');
                },
                success: function (texto) {

                        $("#retornoDetalheAnexo").html(texto);
                },
                beforeSend: function () {
                    $("#retornoDetalheAnexo").html("Aguarde...");
                }
            });
        });
    } else {
        alert("Favor informar todos os dados para alterar a Nota!");
    }
}

function alterarPassoDevolucao(codOcorrencia) {

    if ($('#descDevolucao').val() == "") {
        alert("Atenção! Favor preencher o campo Descrição!");
        $('#descDevolucao').focus();
        $("#descDevolucao").css("background-color", "yellow");
    }
    else if ( $('#dtConf').val() == "") {
        alert("Digite a data de conferencia");
        $('#dtConf').focus();
        $("#dtConf").css("background-color", "yellow");
    }
    else if ($('#dtReceb').val() == "") {
        alert("Digite a data de Recebimento");
        $('#dtReceb').focus();
        $("#dtReceb").css("background-color", "yellow");
    } else if ((!confereDataPassoDevolucao())) {
        alert("ATENÇÃO!A Data de Recebimento não pode ser maior que a de Conferencia.");

    } 
    
    else {

        $.ajax({
            url: 'modules/ocorrencia/consulta/passo/passoDevolucaoMain.php',
            type: 'POST',
            cache: false,
            data: $("#FormPassoDevolucao").serialize() + '&codOcorrencia=' + codOcorrencia ,
            dataType: 'html',
            error: function () {
                alert('Erro ao Tentar acao');
            },
            success: function (texto) {
                $("#retornoAprovacao").html(texto);

            },
            beforeSend: function () {
                $("#retornoAprovacao").html("Aguarde...");
            }
        });


    }

}


function excluirConferenteDevolucao(codOcorrencia, cracha, volumeExcluir) {

	$.ajax({
		url: 'modules/ocorrencia/consulta/passo/passoDevolucaoMain.php',
		type: 'POST',
		cache: false,
		data: $("#FormPassoDevolucao").serialize() + '&codOcorrencia=' + codOcorrencia + '&cracha=' + cracha + '&volumeExcluir=' + volumeExcluir + '&acao=excluir' ,
		dataType: 'html',
		error: function () {
			alert('Erro ao Tentar acao');
		},
		success: function (texto) {
			$("#retornoAprovacao").html(texto);
			$('#step-4').load('modules/ocorrencia/consulta/passo/passoDevolucao.php?codOcorrencia='+codOcorrencia+'&codPasso=4');
		},
		beforeSend: function () {
			$("#retornoAprovacao").html("Aguarde...");
		}
	});
	
}


function avisoUsuario() {
    alert('IMPORTANTE - FAVOR INSERIR PDF DA NOTA DE DEVOLUÇÃO PARA QUE A COLETA OCORRA COM MAIOR AGILIDADE');

}



