function    relatorioMulta() {


    if ($("#dataInicial").val() != "" && $("#dataFinal").val() != "")
    {
        $.ajax({
            url: 'modules/relatorio/multa/relatorioMultaMain.php',
            type: 'POST',
            cache: false,
            data: $("#FormRelatorioMulta").serialize(),
            dataType: 'html',
            error: function () {
                alert('Erro ao Tentar acao');
            },
            success: function (texto) {

                $("#areaRelatorio").html(texto);
            },
            beforeSend: function () {
                $("#areaRelatorio").html("Aguarde... ");
            }
        });

    } else {
        alert("Digite a data inicial e final para executar a pesquisa")
    }


}

function relatorioMultaExport() {
    if ($("#dataInicial").val() != "" && $("#dataFinal").val() != "")
    {
        window.open("modules/relatorio/multa/relatorioMultaExp.php?" + $("#FormRelatorioMulta").serialize(), "Produto", "location=no,toolbar=no,status=no,resizable=no,width=25,height=25");
    } else {
        alert("Selecione o periodo!");
    }
}

function filtroAdicional(tporigem) {


    if (tporigem == "REPR") {
        $("#filtroRep").show();
        $("#filtroIndustria").hide();
    }
    else {
        if (tporigem == "INDU") {
            $("#filtroIndustria").show();
            $("#filtroRep").hide();
        } else {
            $("#filtroIndustria").hide();
            $("#filtroRep").hide();
        }
    }
}

function relatorioNotaDevolucao() {


    if ($("#dataInicial").val() != "" && $("#dataFinal").val() != "")
    {
        $.ajax({
            url: 'modules/relatorio/notaDevolucao/relatorioNotaDevolucaoMain.php',
            type: 'POST',
            cache: false,
            data: $("#FormRelatorioNotaDevolucao").serialize(),
            dataType: 'html',
            error: function () {
                alert('Erro ao Tentar acao');
            },
            success: function (texto) {

                $("#areaRelatorio").html(texto);
            },
            beforeSend: function () {
                $("#areaRelatorio").html("Aguarde... ");
            }
        });

    } else {
        alert("Digite a data inicial e final para executar a pesquisa")
    }


}

function relatorioNotaDevolucaoExport() {
    if ($("#dataInicial").val() != "" && $("#dataFinal").val() != "")
    {
        window.open("modules/relatorio/notaDevolucao/relatorioNotaDevolucaoExp.php?" + $("#FormRelatorioNotaDevolucao").serialize(), "Produto", "location=no,toolbar=no,status=no,resizable=no,width=25,height=25");
    } else {
        alert("Selecione o periodo!");
    }
}

function relatorioOcorrenciaStatus() {


    if ($("#dataInicial").val() != "" && $("#dataFinal").val() != "")
    {
        $.ajax({
            url: 'modules/relatorio/ocorrenciaStatus/relatorioOcoStatusMain.php',
            type: 'POST',
            cache: false,
            data: $("#FormOcoStatus").serialize(),
            dataType: 'html',
            error: function () {
                alert('Erro ao Tentar acao');
            },
            success: function (texto) {

                $("#areaRelatorio").html(texto);
            },
            beforeSend: function () {
                $("#areaRelatorio").html("Aguarde... ");
            }
        });

    } else {
        alert("Digite a data inicial e final para executar a pesquisa")
    }


}

function relatorioOcorrenciaStatusExport() {
    if ($("#dataInicial").val() != "" && $("#dataFinal").val() != "")
    {
        window.open("modules/relatorio/ocorrenciaStatus/relatorioOcoStatusExp.php?" + $("#FormOcoStatus").serialize(), "Produto", "location=no,toolbar=no,status=no,resizable=no,width=25,height=25");
    } else {
        alert("Selecione o periodo!");
    }
}


function relatorioDefinirResponsavel() {


    if ( ($("#dataInicial").val() != "" && $("#dataFinal").val() != "") || ($("#dataInicialPasso").val() != "" && $("#dataFinalPasso").val() != "") )
    {
        $.ajax({
            url: 'modules/relatorio/definirResponsavel/relatorioDefinirResponsavelMain.php',
            type: 'POST',
            cache: false,
            data: $("#FormDefResponsavel").serialize(),
            dataType: 'html',
            error: function () {
                alert('Erro ao Tentar acao');
            },
            success: function (texto) {

                $("#areaRelatorio").html(texto);
            },
            beforeSend: function () {
                $("#areaRelatorio").html("Aguarde... ");
            }
        });

    } else {
        alert("Digite a data inicial e final para executar a pesquisa")
    }


}

function relatorioDefinirResponsavelExport() {
    if ( ($("#dataInicial").val() != "" && $("#dataFinal").val() != "") || ($("#dataInicialPasso").val() != "" && $("#dataFinalPasso").val() != "")  )
    {
        window.open("modules/relatorio/definirResponsavel/relatorioDefinirResponsavelExp.php?" + $("#FormDefResponsavel").serialize(), "Produto", "location=no,toolbar=no,status=no,resizable=no,width=25,height=25");
    } else {
        alert("Selecione o periodo!");
    }
}

function relatorioLeadTime() {


    if ( ($("#dataInicial").val() != "" && $("#dataFinal").val() != "") || ($("#codPasso").val() != "" && $("#dataInicialPasso").val() != "" && $("#dataFinalPasso").val() != ""))
    {
        $.ajax({
            url: 'modules/relatorio/leadTime/relatorioLeadTimeMain.php',
            type: 'POST',
            cache: false,
            data: $("#FormLeadTime").serialize(),
            dataType: 'html',
            error: function () {
                alert('Erro ao Tentar acao');
            },
            success: function (texto) {

                $("#areaRelatorio").html(texto);
            },
            beforeSend: function () {
                $("#areaRelatorio").html("Aguarde... ");
            }
        });

    } else {
        alert("Digite a data início e fim referente ao cadastro do protocolo, ou informe o passo e a data início e fim da baixa no passo.")
    }


}

function relatorioLeadTimeExport() {

	window.open("modules/relatorio/leadTime/relatorioLeadTimeExp.php?" + $("#FormLeadTime").serialize(), "Produto", "location=no,toolbar=no,status=no,resizable=no,width=25,height=25");

}


function relatorioAprovaAuto() {


    if ($("#dataInicial").val() != "" && $("#dataFinal").val() != "")
    {
        $.ajax({
            url: 'modules/relatorio/aprovacaoAuto/relatorioAprovacaoAutoMain.php',
            type: 'POST',
            cache: false,
            data: $("#FormAprovaAuto").serialize(),
            dataType: 'html',
            error: function () {
                alert('Erro ao Tentar acao');
            },
            success: function (texto) {

                $("#areaRelatorio").html(texto);
            },
            beforeSend: function () {
                $("#areaRelatorio").html("Aguarde... ");
            }
        });

    } else {
        alert("Digite a data inicial e final para executar a pesquisa")
    }


}

function relatorioAprovaAutoExport() {
    if ($("#dataInicial").val() != "" && $("#dataFinal").val() != "")
    {
        window.open("modules/relatorio/aprovacaoAuto/relatorioAprovacaoAutoExp.php?" + $("#FormAprovaAuto").serialize(), "Produto", "location=no,toolbar=no,status=no,resizable=no,width=25,height=25");
    } else {
        alert("Selecione o periodo!");
    }
}

function relatorioAprovaAuto() {


    if ($("#dataInicial").val() != "" && $("#dataFinal").val() != "")
    {
        $.ajax({
            url: 'modules/relatorio/aprovacaoAuto/relatorioAprovacaoAutoMain.php',
            type: 'POST',
            cache: false,
            data: $("#FormAprovaAuto").serialize(),
            dataType: 'html',
            error: function () {
                alert('Erro ao Tentar acao');
            },
            success: function (texto) {

                $("#areaRelatorio").html(texto);
            },
            beforeSend: function () {
                $("#areaRelatorio").html("Aguarde... ");
            }
        });

    } else {
        alert("Digite a data inicial e final para executar a pesquisa")
    }


}

function relatorioAprovaAutoExport() {
    if ($("#dataInicial").val() != "" && $("#dataFinal").val() != "")
    {
        window.open("modules/relatorio/aprovacaoAuto/relatorioAprovacaoAutoExp.php?" + $("#FormAprovaAuto").serialize(), "Produto", "location=no,toolbar=no,status=no,resizable=no,width=25,height=25");
    } else {
        alert("Selecione o periodo!");
    }
}

function relatorioPendenteComercial() {


    if ($("#dataInicial").val() != "" && $("#dataFinal").val() != "")
    {
        $.ajax({
            url: 'modules/relatorio/pendenteComercial/relatorioPendenteComercialMain.php',
            type: 'POST',
            cache: false,
            data: $("#FormPendenteComercial").serialize(),
            dataType: 'html',
            error: function () {
                alert('Erro ao Tentar acao');
            },
            success: function (texto) {

                $("#areaRelatorio").html(texto);
            },
            beforeSend: function () {
                $("#areaRelatorio").html("Aguarde... ");
            }
        });

    } else {
        alert("Digite a data inicial e final para executar a pesquisa")
    }


}

function relatorioPendenteComercialExport() {
    if ($("#dataInicial").val() != "" && $("#dataFinal").val() != "")
    {
        window.open("modules/relatorio/pendenteComercial/relatorioPendenteComercialExp.php?" + $("#FormPendenteComercial").serialize(), "Produto", "location=no,toolbar=no,status=no,resizable=no,width=25,height=25");
    } else {
        alert("Selecione o periodo!");
    }
}

function relatorioOcorrenciaAberta() {


    if ($("#dataInicial").val() != "" && $("#dataFinal").val() != "")
    {
        $.ajax({
            url: 'modules/relatorio/ocorrenciaAberta/relatorioOcoAbertaMain.php',
            type: 'POST',
            cache: false,
            data: $("#FormOcoAberta").serialize(),
            dataType: 'html',
            error: function () {
                alert('Erro ao Tentar acao');
            },
            success: function (texto) {

                $("#areaRelatorio").html(texto);
            },
            beforeSend: function () {
                $("#areaRelatorio").html("Aguarde... ");
            }
        });

    } else {
        alert("Digite a data inicial e final para executar a pesquisa")
    }


}

function relatorioOcorrenciaAbertaExport() {
    if ($("#dataInicial").val() != "" && $("#dataFinal").val() != "")
    {
        window.open("modules/relatorio/ocorrenciaAberta/relatorioOcoAbertaExp.php?" + $("#FormOcoAberta").serialize(), "Produto", "location=no,toolbar=no,status=no,resizable=no,width=25,height=25");
    } else {
        alert("Selecione o periodo!");
    }
}

function relatorioBaixaDevFis() {



        $.ajax({
            url: 'modules/relatorio/baixaDevFis/relatorioBaixaDevFisMain.php',
            type: 'POST',
            cache: false,
            data: $("#FormBaixaDevFis").serialize(),
            dataType: 'html',
            error: function () {
                alert('Erro ao Tentar acao');
            },
            success: function (texto) {

                $("#areaRelatorio").html(texto);
            },
            beforeSend: function () {
                $("#areaRelatorio").html("Aguarde... ");
            }
        });


}

function relatorioBaixaDevFisExport() {

        window.open("modules/relatorio/baixaDevFis/relatorioBaixaDevFisExp.php?" + $("#FormBaixaDevFis").serialize(), "Produto", "location=no,toolbar=no,status=no,resizable=no,width=25,height=25");

}

function relatorioOcoReprovada() {


        $.ajax({
            url: 'modules/relatorio/ocorrenciaReprovada/relatorioOcoReprovadaMain.php',
            type: 'POST',
            cache: false,
            data: $("#FormOcoReprovada").serialize(),
            dataType: 'html',
            error: function () {
                alert('Erro ao Tentar acao');
            },
            success: function (texto) {

                $("#areaRelatorio").html(texto);
            },
            beforeSend: function () {
                $("#areaRelatorio").html("Aguarde... ");
            }
        });


}

function relatorioOcoReprovadaExport() {

        window.open("modules/relatorio/ocorrenciaReprovada/relatorioOcoReprovadaExp.php?" + $("#FormOcoReprovada").serialize(), "Produto", "location=no,toolbar=no,status=no,resizable=no,width=25,height=25");

}

function relatorioClienteEspecial() {


        $.ajax({
            url: 'modules/relatorio/clienteEspecial/relatorioClienteEspecialMain.php',
            type: 'POST',
            cache: false,
            data: $("#FormClienteEspecial").serialize(),
            dataType: 'html',
            error: function () {
                alert('Erro ao Tentar acao');
            },
            success: function (texto) {

                $("#areaRelatorio").html(texto);
            },
            beforeSend: function () {
                $("#areaRelatorio").html("Aguarde... ");
            }
        });


}

function relatorioClienteEspecialExport() {

        window.open("modules/relatorio/clienteEspecial/relatorioClienteEspecialExp.php?" + $("#FormClienteEspecial").serialize(), "Produto", "location=no,toolbar=no,status=no,resizable=no,width=25,height=25");

}

function relatorioConferenciaColaborador() {


        $.ajax({
            url: 'modules/relatorio/conferenciaColaborador/relatorioConfColaboradorMain.php',
            type: 'POST',
            cache: false,
            data: $("#FormConfColaborador").serialize(),
            dataType: 'html',
            error: function () {
                alert('Erro ao Tentar acao');
            },
            success: function (texto) {

                $("#areaRelatorio").html(texto);
            },
            beforeSend: function () {
                $("#areaRelatorio").html("Aguarde... ");
            }
        });


}

function relatorioConferenciaColaboradorExport() {

        window.open("modules/relatorio/conferenciaColaborador/relatorioConfColaboradorExp.php?" + $("#FormConfColaborador").serialize(), "Produto", "location=no,toolbar=no,status=no,resizable=no,width=25,height=25");

}

function relatorioNotaLiberada() {


        $.ajax({
            url: 'modules/relatorio/notaLiberada/relatorioNotaLiberadaMain.php',
            type: 'POST',
            cache: false,
            data: $("#FormNotaLiberada").serialize(),
            dataType: 'html',
            error: function () {
                alert('Erro ao Tentar acao');
            },
            success: function (texto) {

                $("#areaRelatorio").html(texto);
            },
            beforeSend: function () {
                $("#areaRelatorio").html("Aguarde... ");
            }
        });


}

function relatorioNotaLiberadaExport() {

        window.open("modules/relatorio/notaLiberada/relatorioNotaLiberadaExp.php?" + $("#FormNotaLiberada").serialize(), "Produto", "location=no,toolbar=no,status=no,resizable=no,width=25,height=25");

}

function relatorioColetaAprovacao() {


    if ($("#dataInicial").val() != "" && $("#dataFinal").val() != "")
    {
        $.ajax({
            url: 'modules/relatorio/coletaAprovacao/relatorioColetaAprovacaoMain.php',
            type: 'POST',
            cache: false,
            data: $("#FormColetaAprovacao").serialize(),
            dataType: 'html',
            error: function () {
                alert('Erro ao Tentar acao');
            },
            success: function (texto) {

                $("#areaRelatorio").html(texto);
            },
            beforeSend: function () {
                $("#areaRelatorio").html("Aguarde... ");
            }
        });

    } else {
        alert("Digite a data inicial e final para executar a pesquisa")
    }
}

function relatorioColetaAprovacaoExport() {

        window.open("modules/relatorio/coletaAprovacao/relatorioColetaAprovacaoExp.php?" + $("#FormColetaAprovacao").serialize(), "Produto", "location=no,toolbar=no,status=no,resizable=no,width=25,height=25");

}


function downloadArquivosRel(codOcorrencia, codigoTipoArquivo) {

    jn = window.open("../modules/utilitario/downloadArquivos/downloadArquivos.php?codOcorrencia=" + codOcorrencia + "&codigoTipoArquivo=" + codigoTipoArquivo, "Relatorio", "height = 300, width = 300,scrollbars = no,location = no,toolbar = no,menubar=yes");
//    jn = window.open("/servimed/webpoint/servimed.com.br/portaldevolqa/public/modules/utilitario/downloadArquivos/downloadArquivos.php?codOcorrencia=" + codOcorrencia + "&codigoTipoArquivo=" + codigoTipoArquivo, "Relatorio", "height = 300, width = 300,scrollbars = no,location = no,toolbar = no,menubar=yes");

}

function marcardesmarcar() {
   
//   alert('teste: ' + $("#todos").is(':checked'));
    if ($("#todos").is(':checked')) {
        $('.selecionar').each(
                function () {
//                    $(this).attr("checked", true);
                    $(this).prop("checked", true);
                }
        );
//        $("#marcaTodos").val('TOD');
    } else {
        $('.selecionar').each(
                function () {
//                    $(this).attr("checked", false);
                    $(this).prop("checked", false);
                }
        );
//        $("#marcaTodos").val('');
    }
}

function enviarColetaAprovacao() {

    $(function () {
        $.ajax({
            url: 'modules/relatorio/coletaAprovacao/relatorioColetaAprovacaoEmail.php',
            type: 'POST',
            cache: false,
            data: $("#frmEmailColeta").serialize(),
            dataType: 'html',
            error: function () {
                alert('Erro ao Tentar acao');
            },
            success: function (texto) {
                
            if (texto.search(/sucesso/) != -1) {
                alert('E-mail de Coleta Enviado com sucesso!');
            }else{
                $("#retornoEmail").html(texto);  
            }

            }, beforeSend: function () {
                $("#retornoEmail").html(" ");
            }
        });
    });
}

function criarColetaAprovacaoteste() {

    $(function () {
        $.ajax({
            url: 'modules/relatorio/coletaAprovacao/relatorioColetaAprovacaoImp.php',
            type: 'POST',
            cache: false,
            data: $("#frmEmailColeta").serialize(),
            dataType: 'html',
            error: function () {
                alert('Erro ao Tentar acao');
            },
            success: function (texto) {
                

                $("#retornoEmail").html(texto);  


            }, beforeSend: function () {
                $("#retornoEmail").html(" ");
            }
        });
    });
}

function criarColetaAprovacao() {

    jn = window.open("../modules/relatorio/coletaAprovacao/relatorioColetaAprovacaoImp.php?"+ $("#frmEmailColeta").serialize(), "Relatorio", "height = 300, width = 300,scrollbars = no,location = no,toolbar = no,menubar=yes");
//    jn = window.open("/servimed/webpoint/servimed.com.br/portaldevolqa/public/modules/utilitario/downloadArquivos/downloadArquivos.php?codOcorrencia=" + codOcorrencia + "&codigoTipoArquivo=" + codigoTipoArquivo, "Relatorio", "height = 300, width = 300,scrollbars = no,location = no,toolbar = no,menubar=yes");

}

function relatorioLiberacaoTransportador() {


    if ($("#dataInicial").val() != "" && $("#dataFinal").val() != "")
    {
        $.ajax({
            url: 'modules/relatorio/liberacaoTransportador/relatorioLiberacaoTransportadorMain.php',
            type: 'POST',
            cache: false,
            data: $("#FormLiberacaoTransportador").serialize(),
            dataType: 'html',
            error: function () {
                alert('Erro ao Tentar acao');
            },
            success: function (texto) {

                $("#areaRelatorio").html(texto);
            },
            beforeSend: function () {
                $("#areaRelatorio").html("Aguarde... ");
            }
        });

    } else {
        alert("Digite a data inicial e final para executar a pesquisa")
    }
}


function relatorioLiberacaoTransportadorExp() {

    jn = window.open("../modules/relatorio/liberacaoTransportador/relatorioLiberacaoTransportadorExp.php?"+ $("#FormLiberacaoTransportador").serialize(), "Relatorio", "height = 300, width = 300,scrollbars = no,location = no,toolbar = no,menubar=yes");
//    jn = window.open("/servimed/webpoint/servimed.com.br/portaldevolqa/public/modules/utilitario/downloadArquivos/downloadArquivos.php?codOcorrencia=" + codOcorrencia + "&codigoTipoArquivo=" + codigoTipoArquivo, "Relatorio", "height = 300, width = 300,scrollbars = no,location = no,toolbar = no,menubar=yes");

}