"use strict";

var demo_dashboard = {
    init: function(){
        
        /* dashboard chart */        
        var myColors = ["#34495e","#85d6de","#82b440","#F3BC65","#E74E40","#3B5998",
                        "#80CDC2","#A6D969","#D9EF8B","#FFFF99","#F7EC37","#F46D43",
                        "#E08215","#D73026","#A12235","#8C510A","#14514B","#4D9220",
                        "#542688", "#4575B4", "#74ACD1", "#B8E1DE", "#FEE0B6","#FDB863",                                                
                        "#C51B7D","#DE77AE","#EDD3F2"];

        d3.scale.myColors = function() {
            return d3.scale.ordinal().range(myColors);
        };
        
        var data = [
            {
                key: "Numero",
                values: [
                    { x: "Janeiro", y: 1200 },
                    { x: "Fevereiro", y: 1000 },
                    { x: "Março", y: 1200 },
                    { x: "Abril", y: 1000 },
                    { x: "Maio", y: 1000 },
                    { x: "Junho", y: 1500 },
                    { x: "Julho", y: 1200 },
                    { x: "Agosto", y: 1000 },
                    { x: "Setembro", y: 1200 },
                    { x: "Outubro", y: 1100 },
                    { x: "Novembro", y: 1050 },
                    { x: "Dezembro", y: 1500 }
                ]
            }
        ];
        
        nv.addGraph({
            generate: function() {                

                var chart = nv.models.multiBarChart()                    
                    .stacked(true)
                    .color(d3.scale.myColors().range())
					.rotateLabels(0)
					.showControls(false)
					.groupSpacing(0.20)
                    .margin({top: 0, right: 0, bottom: 40, left: 50});
					
				chart.yAxis.tickFormat(d3.format(',.0f'));	

                var svg = d3.select('#dashboard-chart svg').datum(data);
				
                svg.transition().duration(0).call(chart);

                return chart;
            }
        });                
        /* ./dashboard chart */
        
    }
};

$(function(){    
    demo_dashboard.init();
});

