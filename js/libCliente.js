function consultarContatoCli(e, codCliente) {

    if ((e.which === 77) || (e.keyCode === 77) || (e.which === 13) || (e.keyCode === 13)) {
        if (codCliente != "") {
            $.ajax({
                url: 'modules/cliente/meusDados/meusDadosMain.php',
                type: 'POST',
                cache: false,
                data: "codCliente=" + codCliente + "&acao=BUSCAR",
                dataType: 'html',
                error: function () {
                    alert('Erro ao Tentar acao');
                },
                success: function (texto) {

                    if (texto.search(/ERROR/) != -1) {
                        alert("Cliente não encontrado!");
                    } else {
                        $("#divMeusdados").html(texto);
                    }

                }
            });

        } else {
            alert("Favor Informar Cliente!");
        }


    }
}


function inserirClienteContato() {

    if (($("#descContato").val() != "") && ($("#tipoContato").val() != "") && ($("#contato").val() != "") && ($("#codClienteContato").val() != "") ) {
        $.ajax({
            url: 'modules/cliente/meusDados/meusDadosMain.php',
            type: 'POST',
            cache: false,
            data: $("#formCadContato").serialize() + "&acao=ADD",
            dataType: 'html',
            error: function () {
                alert('Erro ao Tentar acao');
            },
            success: function (texto) {

                if (texto.search(/ERRO/) != -1) {
                    alert('erro ao salvar contato');
                } else {
//                $("#areaListaContatos").html(texto);
                    alert('Contato cadastrado com sucesso!');
                    listarContatoCliente();
                }
            }
        });
    } else {
        alert("Atenção! Favor preencher todos os dados para continuar.")
    }
}

function listarContatoCliente() {


    $.ajax({
        url: 'modules/cliente/meusDados/meusDadosMain.php',
        type: 'POST',
        cache: false,
        data: $("#formCadContato").serialize() + "&acao=LISCONTATO",
        dataType: 'html',
        error: function () {
            alert('Erro ao Tentar acao');
        },
        success: function (texto) {

            if (texto.search(/ERRO/) != -1) {
                alert('erro ao salvar contato')
            } else {
                $("#areaListaContatos").html(texto);
//                listarContatoCliente();
            }

        }
    });

}

function excluirClienteContato(codContato) {


    var r = confirm("Tem certeza que deseja excluir esse contato? ");

    if (r == true ){

        $.ajax({
            url: 'modules/cliente/meusDados/meusDadosMain.php',
            type: 'POST',
            cache: false,
            data: $("#formCadContato").serialize() + "&acao=EXCLUIR&codContato="+codContato,
            dataType: 'html',
            error: function () {
                alert('Erro ao Tentar acao');
            },
            success: function (texto) {

                if (texto.search(/ERRO/) != -1) {
                    alert('erro ao Excluir contato');
                } else {
//                $("#areaListaContatos").html(texto);
                        alert('Contato excluido com sucesso!');
                        listarContatoCliente();
                }
            }
        });

    }
}


