
function carregaMotivoCadastro(codMotivo) {

    $.ajax({
        url: 'modules/cadastro/motivodevolucao/motivoDevolucaoEditar.php',
        type: 'POST',
        cache: false,
        data: 'codMotivoDevolucao=' + codMotivo,
        dataType: 'html',
        error: function () {
            alert('Erro ao Tentar acao');
        },
        success: function (texto) {
            $("#modal_wizard").html(texto);
            $("#modal_wizard").modal("show");
        }
    });

}


function atualizarMotivoDevolucao(codMotivo) {

    if ($("#toc_in_regra").val().trim() != '') {
        $.ajax({
            url: 'modules/cadastro/motivodevolucao/motivoDevolucaoMain.php',
            type: 'POST',
            cache: false,
            data: $("#formMotivoDevolucao").serialize() + '&codMotivoDevolucao=' + codMotivo + '&acao=ATUALIZAR',
            dataType: 'html',
            error: function () {
                alert('Erro ao Tentar acao');
            },
            success: function (texto) {
                $("#msgMotivoDevolucao").html(texto);
                $("#modal_wizard").modal("hide");
                listarMotivoDevolucao();

            }
        });
    } else {
        alert('Favor Preencher a Descrição para continuar!');
    }
}

function listarMotivoDevolucao() {


    $.ajax({
        url: 'modules/cadastro/motivodevolucao/motivoDevolucaoMain.php',
        type: 'POST',
        cache: false,
        data: $("#formMotivoDevolucao").serialize() + '&acao=LISTAR',
        dataType: 'html',
        error: function () {
            alert('Erro ao Tentar acao');
        },
        success: function (texto) {
            $("#lisMotivoDevolucao").html(texto);

        }
    });
}

function gravarMotivoDevolucao() {

    if ($("#toc_in_regra").val().trim() != '') {
        $.ajax({
            url: 'modules/cadastro/motivodevolucao/motivoDevolucaoMain.php',
            type: 'POST',
            cache: false,
            data: $("#formMotivoDevolucao").serialize() + '&acao=GRAVAR',
            dataType: 'html',
            error: function () {
                alert('Erro ao Tentar acao');
            },
            success: function (texto) {
                $("#msgMotivoDevolucao").html(texto);
                $("#modal_wizard").modal("hide");
                listarMotivoDevolucao();

            }
        });
    } else {
        alert('Favor Preencher a Descrição para continuar!');
    }
}


/*******************************************************************************
 *  CADASTRO DE PARAMETROS
 *******************************************************************************/


function atualizarParametro() {

    if (confirm("Deseja realmente altualizar os parametros?")) {
        $.ajax({
            url: 'modules/cadastro/parametro/parametroMain.php',
            type: 'POST',
            cache: false,
            data: $("#formParametros").serialize(),
            dataType: 'html',
            error: function () {
                alert('Erro ao Tentar acao');
            },
            success: function (texto) {
                $("#msgParametros").html(texto);
                if (texto.search(/sucesso/) != -1) {
                    alert(texto);
                    //atualizar pagina
                } else {
                    alert(texto);

                }

            }
        });
    }
}

/*******************************************************************************
 *  CADASTRO MOTIVOS DE REPROVAS
 *******************************************************************************/
function excluirMotivoReprova(codigo) {

    if (confirm("Deseja Realmente excluir?")) {
        $(function () {
            $.ajax({
                url: 'modules/cadastro/motivoreprova/motivoReprovaMain.php'
                , type: 'POST'
                , cache: false
                , data: "acao=DEL&codigo=" + codigo
                , dataType: 'html'
                , error: function () {
                    alert('Erro ao tentar acao');
                }
                , success: function (texto) {
                    alert(texto);
                    //listarMotivoReprova();
                    location.reload();
                }
            });
        });

    }
}

function listarMotivoReprova() {

    $(function () {
        $.ajax({
            url: 'modules/cadastro/motivoreprova/motivoReprovaMain.php'
            , type: 'POST'
            , cache: false
            , data: "acao=LIS"
            , dataType: 'html'
            , error: function () {
                alert('Erro ao tentar acao');
            }
            , success: function (texto) {
                location.reload();
                // $("#areaListarReprova").html(texto);
            }
        });
    });
}

function gravarMotivoReprova() {

    if ($("#descricao").val() != "") {
        $(function () {
            $.ajax({
                url: 'modules/cadastro/motivoreprova/motivoReprovaMain.php'
                , type: 'POST'
                , cache: false
                , data: $("#formMotivoReprova").serialize() + "&acao=CAD"
                , dataType: 'html'
                , error: function () {
                    alert('Erro ao tentar acao');
                }
                , success: function (texto) {
                    alert(texto);
                    $("#modal_wizard").modal("hide");
                    location.reload();
                }
            });
        });
    } else {
        alert('Favor preencher o campo descrição!');
    }
}

function carregaMotivoReprova() {

    $.ajax({
        url: 'modules/cadastro/motivoreprova/motivoReprovaCadastro.php',
        type: 'POST',
        cache: false,
        dataType: 'html',
        error: function () {
            alert('Erro ao Tentar acao');
        },
        success: function (texto) {
            $("#modal_wizard").html(texto);
            $("#modal_wizard").modal("show");
        }
    });

}

function carregaCadConferente() {

    $.ajax({
        url: 'modules/cadastro/conferente/cadastroConferenteForm.php',
        type: 'POST',
        cache: false,
        dataType: 'html',
        error: function () {
            alert('Erro ao Tentar acao');
        },
        success: function (texto) {
            $("#modal_conferente").html(texto);
            $("#modal_conferente").modal("show");
        }
    });

}

function loadEstabelecimento(emp_co_numero) {

    $(function () {
        $.ajax({
            url: 'modules/cadastro/conferente/conferenteCombo.php?emp_co_numero=' + emp_co_numero,
            type: 'POST',
            cache: false,
            data: $("#formConferente").serialize(),
            dataType: 'html',
            error: function () {
                alert('Erro ao Tentar acao');
            },
            success: function (texto) {
                $("#areaEstabelecimento").html(texto);
            }
        });
    });
}

function buscarNomeColaborador(empresa, estabelecimento, cracha) {

    $(function () {
        $.ajax({
            url: "modules/cadastro/conferente/conferenteCombo.php?acao=NOME_COL&empresa=" + empresa + "&estabelecimento=" + estabelecimento + "&cracha=" + cracha + "&liberarExcluido=" + 1,
            type: 'POST',
            cache: false,
            dataType: 'html',
            error: function () {
                alert('Erro ao Tentar acao');
            },
            success: function (texto) {

                if (texto.search(/ERRO/) != -1) {
                    alert('Colaborador não encontrado!');
                    $("#col_no_colaborador").val('');
                } else {
                    $("#col_no_colaborador").val(texto);
                }
            }
        });
    });
}

function listarConferentes() {

    $(function () {
        $.ajax({
            url: 'modules/cadastro/conferente/cadastroConferenteMain.php',
            type: 'POST',
            cache: false,
            data: 'acao=LISTAR',
            dataType: 'html',
            error: function () {
                alert('Erro ao Tentar acao');
            },
            success: function (texto) {
                $("#listaConferentes").html(texto);
            }
        });
    });
}

function cadastrarConferente() {

    $(function () {
        $.ajax({
            url: 'modules/cadastro/conferente/cadastroConferenteMain.php',
            type: 'POST',
            cache: false,
            data: $("#formConferente").serialize() + "&acao=INCLUIR",
            dataType: 'html',
            error: function () {
                alert('Erro ao Tentar acao');
            },
            success: function (texto) {
                $("#modal_conferente").modal("hide");
                alert(texto);
                listarConferentes();
            }
        });
    });
}


function excluirClienteEspecial(codCliente) {

    if (confirm("Deseja Realmente excluir?")) {
        $(function () {
            $.ajax({
                url: 'modules/cadastro/clienteEspecial/cadastroClienteEspecialMain.php',
                type: 'POST',
                cache: false,
                data: "&acao=EXCLUIR&codCliente=" + codCliente,
                dataType: 'html',
                error: function () {
                    alert('Erro ao Tentar acao');
                },
                success: function (texto) {
                    alert(texto);
                    listarClienteEspecial();
                }
            });
        });

    }
}

function listarClienteEspecial() {

    $(function () {
        $.ajax({
            url: 'modules/cadastro/clienteEspecial/cadastroClienteEspecialMain.php',
            type: 'POST',
            cache: false,
            data: "&acao=LISTAR",
            dataType: 'html',
            error: function () {
                alert('Erro ao Tentar acao');
            },
            success: function (texto) {
                $("#listaClienteEspecial").html(texto);
            }
        });
    });

}

function cadastrarClienteEspecial(acao) {
	
	if (acao == "ADD_REDE") {
		if (! confirm("Deseja Realmente ADD todos da Rede?")) {
			return;
		}
	}
	if (acao == "EXCLUIR_REDE") {
		if (! confirm("Deseja Realmente EXCLUIR todos da Rede?")) {
			return;
		}
	}	

    $(function () {
        $.ajax({
            url: 'modules/cadastro/clienteEspecial/cadastroClienteEspecialMain.php',
            type: 'POST',
            cache: false,
            data: $("#formClienteEspecial").serialize() + "&acao="+acao,
            dataType: 'html',
            error: function () {
                alert('Erro ao Tentar acao');
            },
            success: function (texto) {
                $("#modal_Especial").modal("hide");
                alert(texto);
                listarClienteEspecial();
            }
        });
    });
}


function carregaCadEspecial(acao) {

    $.ajax({
        url: 'modules/cadastro/clienteEspecial/cadastroClienteEspecialForm.php?acao='+acao,
        type: 'POST',
        cache: false,
        dataType: 'html',
        error: function () {
            alert('Erro ao Tentar acao');
        },
        success: function (texto) {
            $("#modal_Especial").html(texto);
            $("#modal_Especial").modal("show");
        }
    });

}

function excluirClienteEspecialNota(codCliente) {

    if (confirm("Deseja Realmente excluir?")) {
        $(function () {
            $.ajax({
                url: 'modules/cadastro/clienteEspecialNota/cadastroClienteEspecialNotaMain.php',
                type: 'POST',
                cache: false,
                data: "&acao=EXCLUIR&codCliente=" + codCliente,
                dataType: 'html',
                error: function () {
                    alert('Erro ao Tentar acao');
                },
                success: function (texto) {
                    alert(texto);
                    listarClienteEspecialNota();
                }
            });
        });

    }
}

function listarClienteEspecialNota() {

    $(function () {
        $.ajax({
            url: 'modules/cadastro/clienteEspecialNota/cadastroClienteEspecialNotaMain.php',
            type: 'POST',
            cache: false,
            data: "&acao=LISTAR",
            dataType: 'html',
            error: function () {
                alert('Erro ao Tentar acao');
            },
            success: function (texto) {
                $("#listaClienteEspecialNota").html(texto);
            }
        });
    });

}

function cadastrarClienteEspecialNota(acao) {

	if (acao == "ADD_REDE") {
		if (! confirm("Deseja Realmente ADD todos da Rede?")) {
			return;
		}
	}
	if (acao == "EXCLUIR_REDE") {
		if (! confirm("Deseja Realmente EXCLUIR todos da Rede?")) {
			return;
		}
	}	

    $(function () {
        $.ajax({
            url: 'modules/cadastro/clienteEspecialNota/cadastroClienteEspecialNotaMain.php',
            type: 'POST',
            cache: false,
            data: $("#formClienteEspecialNota").serialize() + "&acao="+acao,
            dataType: 'html',
            error: function () {
                alert('Erro ao Tentar acao');
            },
            success: function (texto) {
                $("#modal_Especial").modal("hide");
                alert(texto);
                listarClienteEspecialNota();
            }
        });
    });
}


function carregaCadEspecialNota(acao) {

    $.ajax({
        url: 'modules/cadastro/clienteEspecialNota/cadastroClienteEspecialNotaForm.php?acao='+acao,
        type: 'POST',
        cache: false,
        dataType: 'html',
        error: function () {
            alert('Erro ao Tentar acao');
        },
        success: function (texto) {
            $("#modal_Especial").html(texto);
            $("#modal_Especial").modal("show");
        }
    });

}

function cadastroRotaUsuario(codUsuario) {

    $.ajax({
        url: 'modules/cadastro/usuarioTransportador/usuarioTransportadorCadastro.php',
        type: 'POST',
        cache: false,
        data: "&codUsuario=" + codUsuario,
        dataType: 'html',
        error: function () {
            alert('Erro ao Tentar acao');
        },
        success: function (texto) {
            $("#modalRotas").html(texto);
            $("#modalRotas").modal("show");
        }
    });

}

function incluirRotaUsuario(codRota) {

    var codUsuario = $("#codUsuario").val();

    $.ajax({
        url: 'modules/cadastro/usuarioTransportador/usuarioTransportadorMain.php',
        type: 'POST',
        cache: false,
        data: "&codUsuario=" + codUsuario + "&acao=INCLUIR&codRota=" + codRota,
        dataType: 'html',
        error: function () {
            alert('Erro ao Tentar acao');
        },
        success: function (texto) {
            $("#retornoTeste").html(texto);

            if (texto.search(/ERRO/) != -1) {
                alert('Erro ao incluir Rota!');
            } else {
                alert('Rota Vinculada com Sucesso!');
                listarAreaRota(codUsuario, codRota);
//                $("#col_no_colaborador").val(texto);
            }
//            $("#modalRotas").html(texto);
        }
    });

}

function listarAreaRota(codUsuario, codRota) {

    $(function () {
        $.ajax({
            url: 'modules/cadastro/usuarioTransportador/usuarioTransportadorMain.php',
            type: 'POST',
            cache: false,
            data: "&codUsuario=" + codUsuario + "&acao=LISTAR&codRota=" + codRota,
            dataType: 'html',
            error: function () {
                alert('Erro ao Tentar acao');
            },
            success: function (texto) {
                $("#AreaRotas").html(texto);
            }
        });
    });

}

function excluirRotaUsuario(codRota) {


    alert(' EXCLUIR Rota: ' + codRota);

    var codUsuario = $("#codUsuario").val();

    $.ajax({
        url: 'modules/cadastro/usuarioTransportador/usuarioTransportadorMain.php',
        type: 'POST',
        cache: false,
        data: "&codUsuario=" + codUsuario + "&acao=EXCLUIR&codRota=" + codRota,
        dataType: 'html',
        error: function () {
            alert('Erro ao Tentar acao');
        },
        success: function (texto) {
            $("#retornoTeste").html(texto);

            if (texto.search(/ERRO/) != -1) {
                alert('Erro ao Excluir Rota!');
            } else {
                alert('Rota Excluida com Sucesso!');
                listarAreaRota(codUsuario, codRota);
//                $("#col_no_colaborador").val(texto);
            }
//            $("#modalRotas").html(texto);
        }
    });

}

function gravarUsuarioTransportador() {

	if($("#senha").val() == $("#senhaConfirmacao").val()){
		if ($("#cpnj").val() != "" && $("#email").val() != "" && $("#login").val() != "" && ($("#senha").val() != "" || $("#codUsuario").val() > 0) ) {
			$(function () {
				$.ajax({
					url: 'modules/cadastro/usuarioTransportador/usuarioTransportadorMain.php'
					, type: 'POST'
					, cache: false
					, data: $("#formUsuarioTransportador").serialize() + "&acao=GRAVAR"
					, dataType: 'html'
					, error: function () {
						alert('Erro ao tentar acao');
					}
					, success: function (texto) {
						alert(texto);
						$("#modalRotas").modal("hide");
						location.reload();
					}
				});
			});
		} else {
			alert('Favor preencher os campos!');
		}
	}else{
		alert('Senha não confere!');
	}	
	
}


/*******************************************************************************
 *  CADASTRO LIBERAR NOTA
 *******************************************************************************/
function excluirLiberacao(serie, nota, codCliente) {

    if (confirm("Deseja Realmente excluir?")) {
        $(function () {
            $.ajax({
                url: 'modules/cadastro/liberarNota/liberarNotaMain.php',
                type: 'POST',
                cache: false,
                data: "&acao=EXCLUIR&codCliente=" + codCliente + "&serie="+serie + "&nota=" + nota,
                dataType: 'html',
                error: function () {
                    alert('Erro ao Tentar acao');
                },
                success: function (texto) {
                    alert(texto);
                    listarNotasLiberadas();
                }
            });
        });

    }
}

function listarNotasLiberadas(){

    $(function () {
        $.ajax({
            url: 'modules/cadastro/liberarNota/liberarNotaMain.php',
            type: 'POST',
            cache: false,
            data: "&acao=LISTAR",
            dataType: 'html',
            error: function () {
                alert('Erro ao Tentar acao');
            },
            success: function (texto) {
                $("#areaNotasLiberadas").html(texto);
            }
        });
    });

}

function carregaLiberarNota(){

    $.ajax({
        url: 'modules/cadastro/liberarNota/liberarNotaCadastro.php',
        type: 'POST',
        cache: false,
        dataType: 'html',
        error: function () {
            alert('Erro ao Tentar acao');
        },
        success: function (texto) {
            $("#modalLiberarNota").html(texto);
            $("#modalLiberarNota").modal("show");
        }
    });
    
}

function avisoRecIndustria(param){
	//alert(param.value);
	if(param.value == 16){
		alert("FAVOR INFORMAR PRODUTO E QUANTIDADE NO CAMPO DESCRIÇÃO!!!");
		$("#descricao").focus();
	}
}

function cadastrarLiberacao() {

	if (($("#codCliente").val() != "") && ($("#serie").val() != "") && ($("#nota").val() != "") && ($("#descricao").val() != "") && ($("#motivoDevolucao").val() != "")) {
		//motivo - “RECOLHIDO PELA INDÚSTRIA” => codigo = 16 
		if (($("#descricao").val() == "") && ($("#motivoDevolucao").val() == 16)) {    
			alert("FAVOR INFORMAR PRODUTO E QUANTIDADE NO CAMPO DESCRIÇÃO!!!");
			$("#descricao").focus();
		}else{
			$(function () {
				$.ajax({
					url: 'modules/cadastro/liberarNota/liberarNotaMain.php',
					type: 'POST',
					cache: false,
					data: $("#formLiberarNota").serialize() + "&acao=ADD",
					dataType: 'html',
					error: function () {
						alert('Erro ao Tentar acao');
					},
					success: function (texto) {
						$("#modalLiberarNota").modal("hide");
						alert(texto);
						listarNotasLiberadas();
					}
				});
			});
		}
	} else {
		alert("Favor preencher todos os dados!");
	}	
	
}

