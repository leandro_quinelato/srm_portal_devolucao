function apresenta_cliente()
{
    //  $(function () {
    $.ajax({
        url: 'modules/ocorrencia/cadastro/cadastroOcorrenciaClienteDados.php',
        type: 'POST',
        cache: false,
        data: "cli_co_numero=" + $("#cli_co_numero").val(),
        dataType: 'html',
        error: function () {
            alert('Erro ao tentar acao');
        }
        ,
        success: function (texto) {
            $("#dadosCliente").html(texto);
        }
        ,
        beforeSend: function () {
            $("#dadosCliente").html("Carregando informa&ccedil;&otilde;es... ");
        }
    });
    // });
}


function abrirNotaOrigem() {

    var tipoDevolucao = $("input[name='tipoDevolucao']:checked").val();
    if ($("#motivoDevolucao").val() != "") {
        if ($("#cli_no_razao_social").val()) {
			if ($("#qtdVolume").val()) {
				if (tipoDevolucao) {
					if ($("#doc_tx_descricao").val() != "") {
						$.ajax({
							url: 'modules/ocorrencia/cadastro/cadastroNotaOrigem.php',
							type: 'POST',
							cache: false,
							data: $("#formCadOcorrencia").serialize(),
							dataType: 'html',
							error: function () {
								alert('Erro ao Tentar acao');
							},
							success: function (texto) {
								$("#modal_wizard").html(texto);
								$("#modal_wizard").modal("show");

							}
						});
					} else {
						alert("Favor informar os detalhes!");

					}
				} else {
					alert("Favor informar o tipo da Devolução!");
				}
			} else {
				alert("Favor informar a Qtd. Volume!");
			}				
        } else {
            alert("Favor preencher o campo cliente");
        }
    } else {
        alert("Favor preencher o motivo da devolução");
    }
}


function abrirNotaOrigemItem() {


    if (($("#serieNota").val() != "") && ($("#numNota").val() != "") && ($("#dataNota").val() != "")) {
        $.ajax({
            url: 'modules/ocorrencia/cadastro/cadastroNotaOrigemItem.php',
            type: 'POST',
            cache: false,
            data: $("#formCadOcorrencia").serialize() + "&" + $("#formCadNotas").serialize(),
            dataType: 'html',
            error: function () {
                alert('Erro ao Tentar acao');
            },
            success: function (texto) {
                if (texto.search(/Aviso/) != -1) {
    //                    $("#areaItensNota").html(texto);
                    $("#areaCarregaNItens").html(texto);
                    $("#btnSalvarNota").hide();
                } else {
                    $("#areaCarregaNItens").html(texto);
                    $("#areaBotaoCadastro").show();
                    $("#btnSalvarNota").show();

                }

            }
        });
    } else {
        alert("Favor preencher todos os dados da nota!");
    }
}

function verificaQuantidade(codProduto , existeParcialCad) {


    if(existeParcialCad){ 
        var quatidadeNota = parseInt($('#saldoAtualnota_' + codProduto).val());
    }else{
        var quatidadeNota = parseInt($('#notaItemQtd_' + codProduto).val());
    }
    
    var quatidadeDev = parseInt($('#notaItemQtdDev_' + codProduto).val());

    //    alert("quatidadeNota: "+ quatidadeNota + "  quatidadeDev: "+ quatidadeDev );

    if (quatidadeDev > quatidadeNota) {
        alert("A quantidade a ser devolvida não pode ser maior que a quantidade do Produto!");
        $('#notaItemQtdDev_' + codProduto).val("");
        $('#notaItemQtdDev_' + codProduto).focus();
    }

}

function cadastrarOcorrencia() {

    var itensInformados = false;

    $(".notaItemQtdDev").each(function () {

        if (($(this).val() != "") && ($(this).val() != 0)) {
            itensInformados = true;
        }
    });


    if (itensInformados) {
        $.ajax({
            url: 'modules/ocorrencia/cadastro/cadastroOcorrenciaMain.php',
            type: 'POST',
            cache: false,
            data: $("#formCadOcorrencia").serialize() + "&" + $("#formCadNotas").serialize() + "&acao=ADDNOTA",
            dataType: 'html',
            error: function () {
                alert('Erro ao Tentar acao');
            },
            success: function (texto) {
                alert("Informações inseridas com sucesso.");

                if (texto.search(/ERRO OCORRENCIA/) != -1) {
                    
//                    $("#areaItensNota").html(texto);
                    alert("Ocorreu um erro ao inserir a ocorrência!");
                    //$("#areaCarregaNItens").html(texto);//comenta
                    //$("#btnSalvarNota").hide();
                } else if (texto.search(/ERRO ALTERAR/) != -1) {
                    
//                    $("#areaItensNota").html(texto);
                    alert("Ocorreu um erro ao alterar o registro!");
                    //$("#areaCarregaNItens").html(texto);//comenta
                    //$("#btnSalvarNota").hide();
                } 
                else if (texto.search(/ERRO INSERIR NOTA/) != -1) {

//                    $("#areaItensNota").html(texto);
                    alert("Ocorreu um erro ao inserir a nota!");
                    //$("#areaCarregaNItens").html(texto);//comenta
                    //$("#btnSalvarNota").hide();
                } 
                else {
                    //$("#areaCarregaNItens").html(texto);

                    //alert("Nota cadastrada com sucesso!");
                    alert("Cadastro realizado com sucesso.");
                    $("#modal_wizard").modal('hide');
                    ListarNotaOcorrencia();
                }

            }
        });
    } else {
        alert('Favor informar pelo menos 1 item para prosseguir com o cadastro!');
    }
}

function ListarNotaOcorrencia() {
    
    $.ajax({
        url: 'modules/ocorrencia/cadastro/cadastroOcorrenciaMain.php',
        type: 'POST',
        cache: false,
        data: $("#formCadOcorrencia").serialize() + "&acao=LISTARNOTAS",
        dataType: 'html',
        error: function () {
            alert('Erro ao Tentar acao');
        },
        success: function (texto) {

            if (texto.search(/ERRO/) != -1) {
                $("#areaListaNotaOco").html(texto);//comenta
            } else {
                $("#areaListaNotaOco").html(texto);
            }

        }
    });
}

function FinalizaCadastroOcorrencia() {

    $.ajax({
        url: 'modules/ocorrencia/cadastro/cadastroOcorrenciaMain.php',
        type: 'POST',
        cache: false,
        data: $("#formCadOcorrencia").serialize() + "&acao=FINALCAD&valorFinalDev=" + $("#valorFinalDev").val(),
        dataType: 'html',
        error: function () {
            alert('Erro ao Tentar acao');
        },
        success: function (texto) {


            if (texto.search(/ERRO/) != -1) {
//                    $("#areaItensNota").html(texto);
                alert("Ocorreu um erro ao Finalizar o cadastro!");
                $("#areaListaNotaOco").html(texto);//comenta
            } else {
                $("#areaListaNotaOco").html(texto);
                alert("Ocorrencia gerada com sucesso!");
                //ListarNotaOcorrencia();
                location.reload();
            }

        }
    });
}

function    editarNotaCadastrada(serie, nota, dataNota) {

    var tipoDevolucao = $("input[name='tipoDevolucao']:checked").val();
    if ($("#motivoDevolucao").val() != "") {
        if ($("#cli_no_razao_social").val()) {
            if (tipoDevolucao) {
                $.ajax({
                    url: 'modules/ocorrencia/cadastro/cadastroNotaOrigem.php',
                    type: 'POST',
                    cache: false,
                    data: $("#formCadOcorrencia").serialize() + "&serieNota=" + serie + "&numNota=" + nota + "&dataNota=" + dataNota,
                    dataType: 'html',
                    error: function () {
                        alert('Erro ao Tentar acao');
                    },
                    success: function (texto) {
//                    $("#areaItensNota").html(texto);
                        $("#modal_wizard").html(texto);
                        $("#modal_wizard").modal("show");

                    }
                });
            } else {
                alert("Favor informar o tipo da Devolução!");
            }
        } else {
            alert("Favor preencher o campo cliente");
        }
    } else {
        alert("Favor preencher o motivo da devolução");
    }
}

function    editarNotaOrigemItem() {


    if (($("#serieNota").val() != "") && ($("#numNota").val() != "") && ($("#dataNota").val() != "")) {
        $.ajax({
            url: 'modules/ocorrencia/cadastro/cadastroNotaOrigemItem.php',
            type: 'POST',
            cache: false,
            data: $("#formCadOcorrencia").serialize() + "&" + $("#formCadNotas").serialize() + "&acao=EDITAR",
            dataType: 'html',
            error: function () {
                alert('Erro ao Tentar acao');
            },
            success: function (texto) {


                if (texto.search(/Aviso/) != -1) {
//                    $("#areaItensNota").html(texto);
                    $("#areaCarregaNItens").html(texto);
                    $("#btnSalvarNota").hide();
                } else {
                    $("#areaCarregaNItens").html(texto);
                    $("#areaBotaoCadastro").show();
                    $("#btnSalvarNota").show();

                }

            }
        });
    } else {
        alert("Favor preencher todos os dados da nota!");
    }
}


function editarOcorrencia() {

    var itensInformados = false;

    $(".notaItemQtdDev").each(function () {

        if (($(this).val() != "") && ($(this).val() != 0)) {
            itensInformados = true;
        }
    });


    if (itensInformados) {
        $.ajax({
            url: 'modules/ocorrencia/cadastro/cadastroOcorrenciaMain.php',
            type: 'POST',
            cache: false,
            data: $("#formCadOcorrencia").serialize() + "&" + $("#formCadNotas").serialize() + "&acao=ADDNOTA",
            dataType: 'html',
            error: function () {
                alert('Erro ao Tentar acao');
            },
            success: function (texto) {


                if (texto.search(/ERRO/) != -1) {
//                    $("#testesParcial").html(texto);
                    alert("Ocorreu um erro ao inserir a nota!");
                    $("#areaCarregaNItens").html(texto);//comenta
                    $("#btnSalvarNota").hide();
                } else {
                    $("#areaCarregaNItens").html(texto);
                    $("#modal_wizard").modal("hide");
                    alert("Nota cadastrada com sucesso!");
                    ListarNotaOcorrencia();
                }

            }
        });
    } else {
        alert('Favor informar pelo menos 1 item para prosseguir com o cadastro!');
    }
}

function deletaNotaOcorrencia(serie, nota, dataNota) {

    if (confirm("Deseja realmente excluir a nota da Ocorrência?")) {
        $.ajax({
            url: 'modules/ocorrencia/cadastro/cadastroOcorrenciaMain.php',
            type: 'POST',
            cache: false,
            data: $("#formCadOcorrencia").serialize() + "&serieNota=" + serie + "&numNota=" + nota + "&dataNota=" + dataNota + "&acao=DELNOTA",            
            error: function () {
                alert('Erro ao Tentar acao');
            },
            success: function (texto) {

                $("#areaListaNotaOco").html(texto);
                ListarNotaOcorrencia();


            }
        });
    }
}
