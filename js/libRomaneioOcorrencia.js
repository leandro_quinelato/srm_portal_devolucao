function consultarOcorrenciaRomaneio() {

  if( $("#ced_co_numero").val() != "TOD"){
    if($("#rot_co_numero").val() != "TOD" ||  ($("#dataInicial").val() != "" && $("#dataFinal").val() != "") ) 
	{
        $.ajax({
            url: 'modules/ocorrencia/romaneio/consultaOcorrenciaRomaneioMain.php',
            type: 'POST',
            cache: false,
            data: $("#formFiltroConsulta").serialize() + "&acao=LISOCO",
            dataType: 'html',
            error: function () {
                alert('Erro ao Tentar acao');
            },
            success: function (texto) {

                if (texto.search(/ERRO/) != -1) {
                    $("#retornoConsOcorrencia").html(texto);
                } else {
                    $("#retornoConsOcorrencia").html(texto);
                }

            }
        });
    } else {
        alert("Atenção! Favor preencher dados para facilitar a consulta.");
    }
	
  }else {
    alert("Atenção! Favor preencher dados para facilitar a consulta.");
  }
	  
}


function consultarRomaneio() {

    if( $("#codRomaneio").val() != "" ) 
	{
        $.ajax({
            url: 'modules/ocorrencia/romaneio/baixaOcorrenciaRomaneioMain.php',
            type: 'POST',
            cache: false,
            data: $("#formFiltroConsulta").serialize() + "&acao=LISOCO",
            dataType: 'html',
            error: function () {
                alert('Erro ao Tentar acao');
            },
            success: function (texto) {

                if (texto.search(/ERRO/) != -1) {
                    $("#retornoConsOcorrencia").html(texto);
                } else {
                    $("#retornoConsOcorrencia").html(texto);
                }

            }
        });
    } else {
        alert("Atenção! Favor preencher dados para facilitar a consulta.");
    }
}


function BaixarRomaneio() {

	if (confirm("Deseja confirmar a Baixa do Romaneio?")) { 
		if( $("#codRomaneio").val() != "" ) 
		{
			$.ajax({
				url: 'modules/ocorrencia/romaneio/baixaOcorrenciaRomaneioMain.php',
				type: 'POST',
				cache: false,
				data: $("#formListaRomaneio").serialize() + "&acao=BAIXAR",
				dataType: 'html',
				error: function () {
					alert('Erro ao Tentar acao');
				},
				success: function (texto) {

					if (texto.search(/ERRO/) != -1) {
						$("#retornoConsOcorrencia").html(texto);
					} else {
						$("#retornoConsOcorrencia").html(texto);
					}

				}
			});
		} else {
			alert("Atenção! Favor preencher dados para facilitar a consulta.");
		}
	}
}

function gerarDocRomaneio(dataInicial, dataFinal, ced_co_numero, rot_co_numero) {
	alert("O romaneio será gerado para download e impressão.");
    jn = window.open("modules/ocorrencia/romaneio/consultaOcorrenciaRomaneioImp.php?dataInicial="+ dataInicial +"&dataFinal="+ dataFinal +"&ced_co_numero="+ ced_co_numero +"&rot_co_numero="+rot_co_numero, "Romaneio", "height = 300, width = 300,scrollbars = no,location = no,toolbar = no,menubar=yes");
}


function consultarComEnter(e) {
    if (e.keyCode == 13) {
		//alert("teste");
		consultarRomaneio();
        return false;
    }
}