<?php

/**
 * Description of DevNotaOrigemItem
 *
 * @author geovanni.info
 * 
 * 
 * DOC_CO_NUMERO	NUMBER(30,0)
 * NTI_NU_SERIE	        NUMBER(4,0)
 * NTI_NU_NOTA	        NUMBER(10,0)
 * NTI_DT_NOTA	        DATE
 * CLI_CO_NUMERO	NUMBER(8,0)
 * PRO_CO_NUMERO	NUMBER(15,0)
 * NTI_NU_DEVQTD	NUMBER(6,0)
 * NTI_VL_PRECOUNIT	NUMBER(17,2)
 * NTI_VL_TOTBRUTO	NUMBER(17,2)
 * NTI_VL_TOTDESC	NUMBER(17,2)
 * NTI_VL_TOTIMP	NUMBER(17,2)
 * NTI_VL_TOTLIQ	NUMBER(17,2)
 * 
 */
class DevNotaOrigemItem {

    private $codigoOcorrencia;
    private $serie;
    private $numNota;
    private $dataNota;
    private $codCliente;
    private $codProduto;
    private $devQuantidade;
    private $precoUnitario;
    private $totalBruto;
    private $totalDesconto;
    private $totalImposto;
    private $totalLiquido;
    private $notaQuantidade;
    private $nomeProduto;
    private $codEAN;

    public function getCodigoOcorrencia() {
        return $this->codigoOcorrencia;
    }

    public function setCodigoOcorrencia($codigoOcorrencia) {
        $this->codigoOcorrencia = $codigoOcorrencia;
    }
    
    public function getSerie() {
        return $this->serie;
    }
    
    public function setSerie($serie) {
        $this->serie = $serie;
    }
    
    public function getNumNota() {
        return $this->numNota;
    }
    
    public function setNumNota($numNota) {
        $this->numNota = $numNota;
    }
    
    public function getDataNota() {
        return $this->dataNota;
    }
    
    public function setDataNota($dataNota) {
        $this->dataNota = $dataNota;
    }
    
    public function getCodCliente() {
        return $this->codCliente;
    }
    
    public function setCodCliente($codCliente) {
        $this->codCliente = $codCliente;
    }
    
    public function getCodProduto() {
        return $this->codProduto;
    }
    
    public function setCodProduto($codProduto) {
        $this->codProduto = $codProduto;
    }

    public function getCodEAN() {
        return $this->codEAN;
    }
    
    public function setCodEAN($codEAN) {
        $this->codEAN = $codEAN;
    }
    
    public function getDevQuantidade() {
        return $this->devQuantidade;
    }
    
    public function setDevQuantidade($devQuantidade) {
        $this->devQuantidade = $devQuantidade;
    }
    
    public function getPrecoUnitario() {
        return $this->precoUnitario;
    }
    
    public function setPrecoUnitario($precoUnitario) {
        $this->precoUnitario = $precoUnitario;
    }
    
    public function getTotalBruto() {
        return $this->totalBruto;
    }
    
    public function setTotalBruto($totalBruto) {
        $this->totalBruto = $totalBruto;
    }
    
    public function getTotalDesconto() {
        return $this->totalDesconto;
    }
    
    public function setTotalDesconto($totalDesconto) {
        $this->totalDesconto = $totalDesconto;
    }
    
    public function getTotalImposto() {
        return $this->totalImposto;
    }
    
    public function setTotalImposto($totalImposto) {
        $this->totalImposto = $totalImposto;
    }
    
    public function getTotalLiquido() {
        return $this->totalLiquido;
    }
    
    public function setTotalLiquido($totalLiquido) {
        $this->totalLiquido = $totalLiquido;
    }
    
    public function getNotaQuantidade() {
        return $this->notaQuantidade;
    }
    
    public function setNotaQuantidade($notaQuantidade) {
        $this->notaQuantidade = $notaQuantidade;
    }
    
    public function getNomeProduto() {
        return $this->nomeProduto;
    }
    
    public function setNomeProduto($nomeProduto) {
        $this->nomeProduto = $nomeProduto;
    }
    
    
    
    
    
    

}
