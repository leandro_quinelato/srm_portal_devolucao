<?php 
Class devParametrosValorDevolucao{
    private $valor1F;
	private $valor1O;
	private $valor1H;
	private $valor1A;
	
    private $valor46F;
	private $valor46O;
	private $valor46H;
	private $valor46A;

    private $valor48F;
	private $valor48O;
	private $valor48H;
	private $valor48A;

    private $valor49F;
	private $valor49O;
	private $valor49H;
	private $valor49A;

    private $valor50F;


    public function getValor1F() {
        return $this->valor1F;
    }
    public function setValor1F($valor) {
        $this->valor1F = $valor;
    }
	
    public function getValor1O() {
        return $this->valor1O;
    }
    public function setValor1O($valor) {
        $this->valor1O = $valor;
    }

    public function getValor1H() {
        return $this->valor1H;
    }
    public function setValor1H($valor) {
        $this->valor1H = $valor;
    }
	
    public function getValor1A() {
        return $this->valor1A;
    }
    public function setValor1A($valor) {
        $this->valor1A = $valor;
    }


    public function getValor46F() {
        return $this->valor46F;
    }
    public function setValor46F($valor) {
        $this->valor46F = $valor;
    }
	
    public function getValor46O() {
        return $this->valor46O;
    }
    public function setValor46O($valor) {
        $this->valor46O = $valor;
    }

    public function getValor46H() {
        return $this->valor46H;
    }
    public function setValor46H($valor) {
        $this->valor46H = $valor;
    }
	
    public function getValor46A() {
        return $this->valor46A;
    }
    public function setValor46A($valor) {
        $this->valor46A = $valor;
    }	

	
    public function getValor48F() {
        return $this->valor48F;
    }
    public function setValor48F($valor) {
        $this->valor48F = $valor;
    }
	
    public function getValor48O() {
        return $this->valor48O;
    }
    public function setValor48O($valor) {
        $this->valor48O = $valor;
    }

    public function getValor48H() {
        return $this->valor48H;
    }
    public function setValor48H($valor) {
        $this->valor48H = $valor;
    }
	
    public function getValor48A() {
        return $this->valor48A;
    }
    public function setValor48A($valor) {
        $this->valor48A = $valor;
    }
	
	
    public function getValor49F() {
        return $this->valor49F;
    }
    public function setValor49F($valor) {
        $this->valor49F = $valor;
    }
	
    public function getValor49O() {
        return $this->valor49O;
    }
    public function setValor49O($valor) {
        $this->valor49O = $valor;
    }

    public function getValor49H() {
        return $this->valor49H;
    }
    public function setValor49H($valor) {
        $this->valor49H = $valor;
    }
	
    public function getValor49A() {
        return $this->valor49A;
    }
    public function setValor49A($valor) {
        $this->valor49A = $valor;
    }

    public function getValor50F() {
        return $this->valor50F;
    }

 public function setValor50F($valor) {
    $this->valor50F = $valor;
}

}
