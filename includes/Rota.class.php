<?php 
Class Rota
{
	private $codigoRota;
	private $descricaoRota; 
	private $ativaRota; 

	public function getCodigoRota()
	{
		return $this->codigoRota; 
	}
	public function setCodigoRota($codigoRota)
	{
		$this->codigoRota = $codigoRota; 
	}
	
	public function getDescricaoRota()
	{
		return $this->descricaoRota; 
	}
	public function setDescricaoRota($descricaoRota)
	{
		$this->descricaoRota = $descricaoRota; 
	}
	
	public function getAtivaRota()
	{
		return $this->ativaRota; 
	}
	public function setAtivaRota ($ativaRota)
	{
		$this->ativaRota = $ativaRota; 
	}

}
