<?php
/*
 * @author: Anderson Felix;
 * @date: 25-03-2010 
 * @version: 0.0.0.1
 */ 
Class devVoltaPasso{
	private $codigoVoltaPasso; 
	private $descricaoVoltaPasso; 
	private $passoEntradaVoltaPasso; 
	private $passoVoltaVoltaPasso;
	private $usuarioVoltaPasso; 

	public function getCodigoVoltaPasso() {
	    return $this->codigoVoltaPasso;
	}
	
	public function setCodigoVoltaPasso($codigoVoltaPasso) {
	    $this->codigoVoltaPasso = $codigoVoltaPasso;
	}
	
	public function getDescricaoVoltaPasso() {
	    return $this->descricaoVoltaPasso;
	}
	
	public function setDescricaoVoltaPasso($descricaoVoltaPasso) {
	    $this->descricaoVoltaPasso = $descricaoVoltaPasso;
	}
	
	public function getPassoEntradaVoltaPasso() {
	    return $this->passoEntradaVoltaPasso;
	}
	
	public function setPassoEntradaVoltaPasso($passoEntradaVoltaPasso) {
	    $this->passoEntradaVoltaPasso = $passoEntradaVoltaPasso;
	}
	
	public function getPassoVoltaVoltaPasso() {
	    return $this->passoVoltaVoltaPasso;
	}
	
	public function setPassoVoltaVoltaPasso($passoVoltaVoltaPasso) {
	    $this->passoVoltaVoltaPasso = $passoVoltaVoltaPasso;
	}
	
	public function getUsuarioVoltaPasso() {
	    return $this->usuarioVoltaPasso;
	}
	
	public function setUsuarioVoltaPasso($usuarioVoltaPasso) {
	    $this->usuarioVoltaPasso = $usuarioVoltaPasso;
	}

}

