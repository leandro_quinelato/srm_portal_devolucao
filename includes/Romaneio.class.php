<?php
/**
 * Description of Romaneio
 *
 * @author bruno.andrade
 * 
 * 
 * TABELA: DEV_PASSO
 * DRO_CO_NUMERO	NUMBER(30,0)
 * DRO_DT_ABERTURA	DATE
 * USU_CO_ABERTURA	NUMBER(5,0)
 * DRO_DT_BAIXA	DATE
 * USU_CO_BAIXA	NUMBER(5,0)
 * 
 */


class Romaneio {
	private $codigo;
	private $dataAbertura;
	private $usuarioAbertura;
	private $dataBaixa;
	private $usuarioBaixa;

	public function getCodigo()
	{
		return $this->codigo; 
	}
	public function setCodigo($codigo)
	{
		$this->codigo = $codigo; 
	}
               
	public function getDataAbertura()
	{
		return $this->dataAbertura; 
	}
	public function setDataAbertura($dataAbertura)
	{
		$this->dataAbertura = $dataAbertura; 
	}

	public function getUsuarioAbertura()
	{
		return $this->usuarioAbertura; 
	}
	public function setUsuarioAbertura($usuarioAbertura)
	{
		$this->usuarioAbertura = $usuarioAbertura; 
	}

	public function getDataBaixa()
	{
		return $this->dataBaixa; 
	}
	public function setDataBaixa($dataBaixa)
	{
		$this->dataBaixa = $dataBaixa; 
	}

	public function getUsuarioBaixa()
	{
		return $this->usuarioBaixa; 
	}
	public function setUsuarioBaixa($usuarioBaixa)
	{
		$this->usuarioBaixa = $usuarioBaixa; 
	}

	
}
