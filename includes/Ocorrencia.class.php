<?php

include_once("GlbCliente.class.php");
include_once("GlbTelefone.class.php");  
include_once("Rota.class.php");
include_once("DevNotaOrigem.class.php");
include_once("DevVoltaPasso.class.php");


Class Ocorrencia
{

    private $codigoDevolucao;
    private $GlbCliente;
    private $dataDevolucao;
    private $serieNfDevolucao;
    private $notaNfDevolucao;
    private $valorDevolucao;
    private $qntdVolumeDevolucao;
    private $finalizadoDevolucao;
    private $ObjRota;
    private $transferidoDevolucao;
    private $transferidoDtDevolucao;
    private $TipoOcorrencia;

    private $usuarioGerDevolucao;
    private $dataGerDevolucao;
    private $aprovacaoGerDevolucao;
    private $reentregaGerDevolucao;
    private $descricaoGerDevolucao;
    private $usuarioIntGerDevolucao;

    private $usuarioPolVolDevolucao;
    private $dataPolVolDevolucao;
    private $descricaoPolVolDevolucao;
    private $usuarioIntPolVolDevolucao;

    private $usuarioPolEntDevolucao;
    private $dataPolEntDevolucao;
    private $aprovacaoPolEntDevolucao;
    private $descricaoPolEntDevolucao;
    private $qntdVolumePolEntDevolucao;
    private $usuarioIntPolEntDevolucao;

    private $usuarioPolSaiDevolucao;
    private $dataPolSaiDevolucao;
    private $aprovacaoPolSaiDevolucao;
    private $descricaoPolSaiDevolucao;
    private $qntdVolumePolSaiDevolucao;
    private $usuarioIntPolSaiDevolucao;

    private $usuarioIntDevolucao;
    private $dataIntDevolucao;
    private $aprovacaoIntDevolucao;
    private $descricaoIntDevolucao;
    private $qntdVolumeIntDevolucao;
    private $descricaoIntAguardandoDevolucao;

    private $usuarioFisDevolucao;
    private $dataFisDevolucao;
    private $aprovacaoFisDevolucao;
    private $descricaoFisDevolucao;
    private $descricaoDevolucao;
    private $usuarioIntFisDevolucao;
    private $descricaoFisAguardandoDevolucao;

    private $tipoTipoDevolucao;
    private $emailColetaDevolucao;
    private $dataNfDevolucao;
    private $codigoOcorrencia;
    private $emailCadDevolucao;

    private $codigoCadastranteDevolucao;
    private $contatoClienteDevolucao;
    private $deptContatoClienteDevolucao;
    private $ObjVoltaPasso;
    private $tipoCadastranteDevolucao;
    private $DevNotaOrigem;

    // add - 22/08/2014 - chamado: 22663
    private $empresa;
    private $estabelecimento;
    private $cracha;
    private $dataConferencia;

    //add - 05/09/2014
    private $docInTransmitido;

    //add - 15/10/2014
    private $numChamadoCsl;

    //add - 13/04/2015
    private $docInColetaEnviada;

    //add - 28/07/2015
    private $dataDevRecebimento;

    //ADD - 06/04/2016
    private $statusOcorrencia;
    private $passoOcorrencia;

    //ADD - 11/04/2016
    private $cedis;

    //ADD - 30/04/2016
    private $indicaAutomatico;

    //ADD - 30/04/2016
    private $divisao;

    //ADD - 31/10/2017
    private $dataCancelamento;
    private $dataReativar;


    function __construct()
    {
        $this->ObjRota = new Rota();
        $this->TipoOcorrencia = new CalTipoOcorrencia();
        $this->GlbCliente = new GlbCliente();
    }

    public function getCodigoDevolucao()
    {
        return $this->codigoDevolucao;
    }

    public function setCodigoDevolucao($codigoDevolucao)
    {
        $this->codigoDevolucao = $codigoDevolucao;
    }


    public function getGlbCliente()
    {
        return $this->GlbCliente;
    }

    public function setGlbCliente($GlbCliente)
    {
        $this->GlbCliente = $GlbCliente;
    }

    public function getDataDevolucao()
    {
        return $this->dataDevolucao;
    }

    public function setDataDevolucao($dataDevolucao)
    {
        $this->dataDevolucao = $dataDevolucao;
    }

    public function getSerieNfDevolucao()
    {
        return $this->serieNfDevolucao;
    }

    public function setSerieNfDevolucao($serieNfDevolucao)
    {
        if ($serieNfDevolucao == "") {
            $serieNfDevolucao = NULL;
        }
        $this->serieNfDevolucao = $serieNfDevolucao;
    }

    public function getNotaNfDevolucao()
    {
        return $this->notaNfDevolucao;
    }

    public function setNotaNfDevolucao($notaNfDevolucao)
    {
        if ($notaNfDevolucao == "") {
            $notaNfDevolucao = NULL;
        }
        $this->notaNfDevolucao = $notaNfDevolucao;
    }

    public function getDataNfDevolucao()
    {
        return $this->dataNfDevolucao;
    }

    public function setDataNfDevolucao($dataNfDevolucao)
    {
        if ($dataNfDevolucao == "") {
            $dataNfDevolucao = NULL;
        }
        $this->dataNfDevolucao = $dataNfDevolucao;
    }

    public function getValorDevolucao()
    {
        return $this->valorDevolucao;
    }

    public function setValorDevolucao($valorDevolucao)
    {
        $valorDevolucao = ereg_replace("([$]?[A-za-z_/-])", "", $valorDevolucao);
        $valorDevolucao = str_replace("$", "", $valorDevolucao);
        $valorDevolucao = str_replace(",", ".", $valorDevolucao);
        if (!filter_var($valorDevolucao, FILTER_VALIDATE_FLOAT, FILTER_FLAG_ALLOW_FRACTION)) {
            $valorDevolucao = 0;
        }
        if (substr_count($valorDevolucao, ".") >= 2) {
            $cent = substr($valorDevolucao, strlen($valorDevolucao) - 2);
            $real = str_replace(".", "", substr($valorDevolucao, 0, strlen($valorDevolucao) - 2));
            $valorDevolucao = $real . "." . $cent;
        }
        $this->valorDevolucao = $valorDevolucao;
    }

    public function getQntdVolumeDevolucao()
    {
        return $this->qntdVolumeDevolucao;
    }

    public function setQntdVolumeDevolucao($qntdVolumeDevolucao)
    {
        $this->qntdVolumeDevolucao = $qntdVolumeDevolucao;
    }

    public function getFinalizadoDevolucao()
    {
        return $this->finalizadoDevolucao;
    }

    public function setFinalizadoDevolucao($finalizadoDevolucao)
    {
        $this->finalizadoDevolucao = $finalizadoDevolucao;
    }

    public function getObjRota()
    {
        return $this->ObjRota;
    }

    public function setObjRota($ObjRota)
    {
        $this->ObjRota = $ObjRota;
    }

    public function getTransferidoDevolucao()
    {
        return $this->transferidoDevolucao;
    }

    public function setTransferidoDevolucao($transferidoDevolucao)
    {
        $this->transferidoDevolucao = $transferidoDevolucao;
    }

    public function getTransferidoDtDevolucao()
    {
        return $this->transferidoDtDevolucao;
    }

    public function setTransferidoDtDevolucao($transferidoDtDevolucao)
    {
        $this->transferidoDtDevolucao = $transferidoDtDevolucao;
    }

    public function getTipoOcorrencia()
    {
        return $this->TipoOcorrencia;
    }

    public function setTipoOcorrencia($TipoOcorrencia)
    {
        $this->TipoOcorrencia = $TipoOcorrencia;
    }

    public function getTipoTipoDevolucao()
    {
        return $this->tipoTipoDevolucao;
    }

    public function setTipoTipoDevolucao($tipoTipoDevolucao)
    {
        $this->tipoTipoDevolucao = $tipoTipoDevolucao;
    }


    /* Passo Fluxo Gerente */
    public function getUsuarioGerDevolucao()
    {
        return $this->usuarioGerDevolucao;
    }

    public function setUsuarioGerDevolucao($usuarioGerDevolucao)
    {
        $this->usuarioGerDevolucao = $usuarioGerDevolucao;
    }

    public function getDataGerDevolucao()
    {
        return $this->dataGerDevolucao;
    }

    public function setDataGerDevolucao($dataGerDevolucao)
    {
        $this->dataGerDevolucao = $dataGerDevolucao;
    }

    public function getAprovacaoGerDevolucao()
    {
        return $this->aprovacaoGerDevolucao;
    }

    public function setAprovacaoGerDevolucao($aprovacaoGerDevolucao)
    {
        $this->aprovacaoGerDevolucao = $aprovacaoGerDevolucao;
    }

    public function getReentregaGerDevolucao()
    {
        return $this->reentregaGerDevolucao;
    }

    public function setReentregaGerDevolucao($reentregaGerDevolucao)
    {
        $this->reentregaGerDevolucao = $reentregaGerDevolucao;
    }

    public function getDescricaoGerDevolucao()
    {
        return $this->descricaoGerDevolucao;
    }

    public function setDescricaoGerDevolucao($descricaoGerDevolucao)
    {
        $this->descricaoGerDevolucao = addslashes($descricaoGerDevolucao);
    }

    public function getEmailColetaDevolucao()
    {
        return $this->emailColetaDevolucao;
    }

    public function setEmailColetaDevolucao($emailColetaDevolucao)
    {
        $this->emailColetaDevolucao = $emailColetaDevolucao;
    }

    public function getUsuarioIntGerDevolucao()
    {
        return $this->usuarioIntGerDevolucao;
    }

    public function setUsuarioIntGerDevolucao($usuarioIntGerDevolucao)
    {
        $this->usuarioIntGerDevolucao = $usuarioIntGerDevolucao;
    }

    /* Passo Fluxo Polo Entrada */
    public function getUsuarioPolEntDevolucao()
    {
        return $this->usuarioPolEntDevolucao;
    }

    public function setUsuarioPolEntDevolucao($usuarioPolEntDevolucao)
    {
        $this->usuarioPolEntDevolucao = $usuarioPolEntDevolucao;
    }

    public function getDataPolEntDevolucao()
    {
        return $this->dataPolEntDevolucao;
    }

    public function setDataPolEntDevolucao($dataPolEntDevolucao)
    {
        $this->dataPolEntDevolucao = $dataPolEntDevolucao;
    }

    public function getAprovacaoPolEntDevolucao()
    {
        return $this->aprovacaoPolEntDevolucao;
    }

    public function setAprovacaoPolEntDevolucao($aprovacaoPolEntDevolucao)
    {
        $this->aprovacaoPolEntDevolucao = $aprovacaoPolEntDevolucao;
    }

    public function getDescricaoPolEntDevolucao()
    {
        return $this->descricaoPolEntDevolucao;
    }

    public function setDescricaoPolEntDevolucao($descricaoPolEntDevolucao)
    {
        $this->descricaoPolEntDevolucao = addslashes($descricaoPolEntDevolucao);
    }

    public function getQntdVolumePolEntDevolucao()
    {
        return $this->qntdVolumePolEntDevolucao;
    }

    public function setQntdVolumePolEntDevolucao($qntdVolumePolEntDevolucao)
    {
        $this->qntdVolumePolEntDevolucao = $qntdVolumePolEntDevolucao;
    }

    public function getUsuarioIntPolEntDevolucao()
    {
        return $this->usuarioIntPolEntDevolucao;
    }

    public function setUsuarioIntPolEntDevolucao($usuarioIntPolEntDevolucao)
    {
        $this->usuarioIntPolEntDevolucao = $usuarioIntPolEntDevolucao;
    }

    /* Passo Fluxo Polo Saida */

    public function getUsuarioPolSaiDevolucao()
    {
        return $this->usuarioPolSaiDevolucao;
    }

    public function setUsuarioPolSaiDevolucao($usuarioPolSaiDevolucao)
    {
        $this->usuarioPolSaiDevolucao = $usuarioPolSaiDevolucao;
    }

    public function getDataPolSaiDevolucao()
    {
        return $this->dataPolSaiDevolucao;
    }

    public function setDataPolSaiDevolucao($dataPolSaiDevolucao)
    {
        $this->dataPolSaiDevolucao = $dataPolSaiDevolucao;
    }

    public function getAprovacaoPolSaiDevolucao()
    {
        return $this->aprovacaoPolSaiDevolucao;
    }

    public function setAprovacaoPolSaiDevolucao($aprovacaoPolSaiDevolucao)
    {
        $this->aprovacaoPolSaiDevolucao = $aprovacaoPolSaiDevolucao;
    }

    public function getDescricaoPolSaiDevolucao()
    {
        return $this->descricaoPolSaiDevolucao;
    }

    public function setDescricaoPolSaiDevolucao($descricaoPolSaiDevolucao)
    {
        $this->descricaoPolSaiDevolucao = addslashes($descricaoPolSaiDevolucao);
    }

    public function getQntdVolumePolSaiDevolucao()
    {
        return $this->qntdVolumePolSaiDevolucao;
    }

    public function setQntdVolumePolSaiDevolucao($qntdVolumePolSaiDevolucao)
    {
        $this->qntdVolumePolSaiDevolucao = $qntdVolumePolSaiDevolucao;
    }

    public function getUsuarioIntPolSaiDevolucao()
    {
        return $this->usuarioIntPolSaiDevolucao;
    }

    public function setUsuarioIntPolSaiDevolucao($usuarioIntPolSaiDevolucao)
    {
        $this->usuarioIntPolSaiDevolucao = $usuarioIntPolSaiDevolucao;
    }

    /* Passo Fluxo Devolu��o */
    public function getUsuarioIntDevolucao()
    {
        return $this->usuarioIntDevolucao;
    }

    public function setUsuarioIntDevolucao($usuarioIntDevolucao)
    {
        $this->usuarioIntDevolucao = $usuarioIntDevolucao;
    }

    public function getDataIntDevolucao()
    {
        return $this->dataIntDevolucao;
    }

    public function setDataIntDevolucao($dataIntDevolucao)
    {
        $this->dataIntDevolucao = $dataIntDevolucao;
    }

    public function getAprovacaoIntDevolucao()
    {
        return $this->aprovacaoIntDevolucao;
    }

    public function setAprovacaoIntDevolucao($aprovacaoIntDevolucao)
    {
        $this->aprovacaoIntDevolucao = $aprovacaoIntDevolucao;
    }

    public function getDescricaoIntDevolucao()
    {
        return $this->descricaoIntDevolucao;
    }

    public function setDescricaoIntDevolucao($descricaoIntDevolucao)
    {
        $this->descricaoIntDevolucao = addslashes($descricaoIntDevolucao);
    }

    public function getQntdVolumeIntDevolucao()
    {
        return $this->qntdVolumeIntDevolucao;
    }

    public function setQntdVolumeIntDevolucao($qntdVolumeIntDevolucao)
    {
        $this->qntdVolumeIntDevolucao = $qntdVolumeIntDevolucao;
    }

    //anderson
    public function getDescricaoIntAguardandoDevolucao()
    {
        return $this->descricaoIntAguardandoDevolucao;
    }

    public function setDescricaoIntAguardandoDevolucao($descricaoIntAguardandoDevolucao)
    {
        $this->descricaoIntAguardandoDevolucao = addslashes($descricaoIntAguardandoDevolucao);
    }

    /* Passo Fluxo Fiscal */
    public function getUsuarioFisDevolucao()
    {
        return $this->usuarioFisDevolucao;
    }

    public function setUsuarioFisDevolucao($usuarioFisDevolucao)
    {
        $this->usuarioFisDevolucao = $usuarioFisDevolucao;
    }

    public function getDataFisDevolucao()
    {
        return $this->dataFisDevolucao;
    }

    public function setDataFisDevolucao($dataFisDevolucao)
    {
        $this->dataFisDevolucao = $dataFisDevolucao;
    }

    public function getAprovacaoFisDevolucao()
    {
        return $this->aprovacaoFisDevolucao;
    }

    public function setAprovacaoFisDevolucao($aprovacaoFisDevolucao)
    {
        $this->aprovacaoFisDevolucao = $aprovacaoFisDevolucao;
    }

    public function getDescricaoFisDevolucao()
    {
        return $this->descricaoFisDevolucao;
    }

    public function setDescricaoFisDevolucao($descricaoFisDevolucao)
    {
        $this->descricaoFisDevolucao = addslashes($descricaoFisDevolucao);
    }


    public function getUsuarioIntFisDevolucao()
    {
        return $this->usuarioIntFisDevolucao;
    }

    public function setUsuarioIntFisDevolucao($usuarioIntFisDevolucao)
    {
        $this->usuarioIntFisDevolucao = $usuarioIntFisDevolucao;
    }

    public function getDescricaoFisAguardandoDevolucao()
    {
        return $this->descricaoFisAguardandoDevolucao;
    }

    public function setDescricaoFisAguardandoDevolucao($descricaoFisAguardandoDevolucao)
    {
        $this->descricaoFisAguardandoDevolucao = addslashes($descricaoFisAguardandoDevolucao);
    }

    public function getDescricaoDevolucao()
    {
        return $this->descricaoDevolucao;
    }

    public function setDescricaoDevolucao($descricaoDevolucao)
    {
        $descricaoDevolucao = str_replace("'", "", $descricaoDevolucao);
        $this->descricaoDevolucao = addslashes($descricaoDevolucao);
    }

    public function getNotaNFEntregaDevolucao()
    {
        return $this->notaNFEntregaDevolucao;
    }

    public function setNotaNFEntregaDevolucao($notaNFEntregaDevolucao)
    {
        $this->notaNFEntregaDevolucao[] = $notaNFEntregaDevolucao;
    }


    public function getCodigoOcorrencia()
    {
        return $this->codigoOcorrencia;
    }

    public function setCodigoOcorrencia($codigoOcorrencia)
    {
        $this->codigoOcorrencia[] = $codigoOcorrencia;
    }

    public function getEmailCadDevolucao()
    {
        return $this->emailCadDevolucao;
    }

    public function setEmailCadDevolucao($emailCadDevolucao)
    {
        $this->emailCadDevolucao = $emailCadDevolucao;
    }


    public function getCodigoCadastranteDevolucao()
    {
        return $this->codigoCadastranteDevolucao;
    }

    public function setCodigoCadastranteDevolucao($codigoCadastranteDevolucao)
    {
        $this->codigoCadastranteDevolucao = $codigoCadastranteDevolucao;
    }

    public function getContatoClienteDevolucao()
    {
        return $this->contatoClienteDevolucao;
    }

    public function setContatoClienteDevolucao($contatoClienteDevolucao)
    {
        $this->contatoClienteDevolucao = $contatoClienteDevolucao;
    }

    public function getDeptContatoClienteDevolucao()
    {
        return $this->deptContatoClienteDevolucao;
    }

    public function setDeptContatoClienteDevolucao($deptContatoClienteDevolucao)
    {
        $this->deptContatoClienteDevolucao = $deptContatoClienteDevolucao;
    }

    public function getObjVoltaPasso()
    {
        return $this->ObjVoltaPasso;
    }

    public function setObjVoltaPasso($ObjVoltaPasso)
    {
        $this->ObjVoltaPasso = $ObjVoltaPasso;
    }

    public function getTipoCadastranteDevolucao()
    {
        return $this->tipoCadastranteDevolucao;
    }

    public function setTipoCadastranteDevolucao($tipoCadastranteDevolucao)
    {
        $this->tipoCadastranteDevolucao = $tipoCadastranteDevolucao;
    }

    public function getDevNotaOrigem()
    {
        return $this->DevNotaOrigem;
    }

    public function setDevNotaOrigem($DevNotaOrigem)
    {
        $this->DevNotaOrigem[] = $DevNotaOrigem;
    }

// add - 22/08/2014
    public function getEmpresa()
    {
        return $this->empresa;
    }

    public function setEmpresa($empresa)
    {
        $this->empresa = $empresa;
    }

    public function getEstabelecimento()
    {
        return $this->estabelecimento;
    }

    public function setEstabelecimento($estabelecimento)
    {
        $this->estabelecimento = $estabelecimento;
    }

    public function getCracha()
    {
        return $this->cracha;
    }

    public function setCracha($cracha)
    {
        $this->cracha = $cracha;
    }

    public function getDataConferencia()
    {
        return $this->dataConferencia;
    }

    public function setDataConferencia($dataConferencia)
    {
        $this->dataConferencia = $dataConferencia;
    }

    public function getDocInTransmitido()
    {
        return $this->docInTransmitido;
    }

    public function setDocInTransmitido($docInTransmitido)
    {
        $this->docInTransmitido = $docInTransmitido;
    }

    public function getNumChamadoCsl()
    {
        return $this->numChamadoCsl;
    }

    public function setNumChamadoCsl($numChamadoCsl)
    {
        $this->numChamadoCsl = $numChamadoCsl;
    }

    public function getDocInColetaEnviada()
    {
        return $this->docInColetaEnviada;
    }

    public function setDocInColetaEnviada($docInColetaEnviada)
    {
        $this->docInColetaEnviada = $docInColetaEnviada;
    }

    public function getDataDevRecebimento()
    {
        return $this->dataDevRecebimento;
    }

    public function setDataDevRecebimento($dataDevRecebimento)
    {
        $this->dataDevRecebimento = $dataDevRecebimento;
    }

    public function getStatusOcorrencia()
    {
        return $this->statusOcorrencia;
    }

    public function setStatusOcorrencia($statusOcorrencia)
    {
        $this->statusOcorrencia = $statusOcorrencia;
    }

    public function getPassoOcorrencia()
    {
        return $this->passoOcorrencia;
    }

    public function setPassoOcorrencia($passoOcorrencia)
    {
        $this->passoOcorrencia = $passoOcorrencia;
    }

    public function getCedis()
    {
        return $this->cedis;
    }

    public function setCedis($cedis)
    {
        $this->cedis = $cedis;
    }

    public function getIndicaAutomatico()
    {
        return $this->indicaAutomatico;
    }

    public function setIndicaAutomatico($indicaAutomatico)
    {
        $this->indicaAutomatico = $indicaAutomatico;
    }

    public function getDivisao()
    {
        return $this->divisao;
    }

    public function setDivisao($divisao)
    {
        $this->divisao = $divisao;
    }

    public function getDataCancelamento()
    {
        return $this->dataCancelamento;
    }


    public function setDataCancelamento($dataCancelamento)
    {
        $this->dataCancelamento = $dataCancelamento;
    }


    public function getDataReativar()
    {
        return $this->dataReativar;
    }

    public function setDataReativar($dataReativar)
    {
        $this->dataReativar = $dataReativar;
    }

}


?>