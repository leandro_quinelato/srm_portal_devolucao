<?php

/**
 * Description of Passo
 *
 * @author geovanni.info
 * 
 * 
 * TABELA: DEV_PASSO
 * DPA_CO_NUMERO	NUMBER(3,0)
 * DPA_NO_DESCRICAO	VARCHAR2(60 BYTE)
 * DPA_DT_CRIACAO	DATE
 * DPA_IN_STATUS	CHAR(1 BYTE)
 * DPA_NU_SEQFLUXO	NUMBER(2,0)
 * 
 */



class Passo {
	private $codigo;
	private $descricao; 
        private $dataCriacao;
	private $status;
        private $seqFluxo;
        private $paginaFluxo;

	public function getCodigo()
	{
		return $this->codigo; 
	}
	public function setCodigo($codigo)
	{
		$this->codigo = $codigo; 
	}
        
	public function getDescricao()
	{
		return $this->descricao; 
	}
	public function setDescricao($descricao)
	{
		$this->descricao = $descricao; 
	}
        
	public function getDataCriacao()
	{
		return $this->dataCriacao; 
	}
	public function setDataCriacao($dataCriacao)
	{
		$this->dataCriacao = $dataCriacao; 
	}
        
	public function getStatus()
	{
		return $this->status; 
	}
	public function setStatus($status)
	{
		$this->status = $status; 
	}
        
	public function getSeqFluxo()
	{
		return $this->seqFluxo; 
	}
	public function setSeqFluxo($seqFluxo)
	{
		$this->seqFluxo = $seqFluxo; 
	}
        
	public function getPaginaFluxo()
	{
		return $this->paginaFluxo; 
	}
	public function setPaginaFluxo($paginaFluxo)
	{
		$this->paginaFluxo = $paginaFluxo; 
	}
        
}
