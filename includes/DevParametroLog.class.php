<?php

/*
 * Gravar logs de alteração na tabela de parametros do sistema
 */

/**
 * Description of DevParametroLog
 *
 * @author geovanni.info
 */
class DevParametroLog {
    private $usuario;
    private $tipoUsuario;
    private $dataAcao;
    
    public function getUsuario() {
        return $this->usuario;
    }

    public function setUsuario($usuario) {
        $this->usuario = $usuario;
    }
    
    public function getTipoUsuario(){
        return $this->tipoUsuario;
    }
    
    public function setTipoUsuario($tipoUsuario) {
        $this->tipoUsuario = $tipoUsuario;
    }
    
    public function getDataAcao(){
        return $this->dataAcao;
    }
    
    public function setDataAcao($dataAcao) {
        $this->dataAcao = $dataAcao;
    }
    
}
