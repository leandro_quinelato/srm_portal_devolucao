<?php

/**
 * Description of TipoArquivoDevolucao
 *
 * @author geovanni.info
 * 
 * DTA_CO_CODIGO
 * DTA_NO_DESCRICAO
 * DTA_NO_PATH
 * 
 */
class TipoArquivoDevolucao {
    
    
    private $CodigoTipoArquivo;
    private $DescricaoTipoArquivo;
    private $DiretorioArquivo;
    
    public function getCodigoTipoArquivo() {
        return $this->CodigoTipoArquivo;
    }

    public function setCodigoTipoArquivo($CodigoTipoArquivo) {
        $this->CodigoTipoArquivo = $CodigoTipoArquivo;
    }
    
    public function getDescricaoTipoArquivo() {
        return $this->DescricaoTipoArquivo;
    }

    public function setDescricaoTipoArquivo($DescricaoTipoArquivo) {
        $this->DescricaoTipoArquivo = $DescricaoTipoArquivo;
    }
    
    public function getDiretorioArquivo() {
        return $this->DiretorioArquivo;
    }

    public function setDiretorioArquivo($DiretorioArquivo) {
        $this->DiretorioArquivo = $DiretorioArquivo;
    }
}
