<?php

/**
 * Description of OcorrenciaRomaneio
 *
 * @author bruno.andrade
 * 
 * TABELA: DEV_OCORRENCIA_Romaneio
 * DOC_CO_NUMERO	NUMBER(30,0)
 * DRO_CO_NUMERO	NUMBER(30,0)
 * DOR_IN_BAIXA		CHAR(1)
 * 
 */
class OcorrenciaRomaneio {

	private $codigoOcorrencia;
	private $codigoRomaneio; 
    private $indicadorBaixa;
        
        
	public function getCodigoOcorrencia()
	{
		return $this->codigoOcorrencia; 
	}
	public function setCodigoOcorrencia($codigoOcorrencia)
	{
		$this->codigoOcorrencia = $codigoOcorrencia; 
	}
        
	public function getCodigoRomaneio()
	{
		return $this->codigoRomaneio; 
	}
	public function setCodigoRomaneio($codigoRomaneio)
	{
		$this->codigoRomaneio = $codigoRomaneio; 
	}
         
	public function getIndicadorBaixa()
	{
		return $this->indicadorBaixa; 
	}
	public function setIndicadorBaixa($indicadorBaixa)
	{
		$this->indicadorBaixa = $indicadorBaixa; 
	}
    
}
