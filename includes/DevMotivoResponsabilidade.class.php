<?php

Class DevMotivoResponsabilidade {

    private $codigo;
    private $descricao;
    private $status;
    private $responsavel;

    public function getCodigo() {
        return $this->codigo;
    }

    public function setCodigo($codigo) {
        $this->codigo = $codigo;
    }

    public function getDescricao() {
        return $this->descricao;
    }

    public function setDescricao($descricao) {
        $this->descricao = $descricao;
    }

    public function getStatus() {
        return $this->status;
    }

    public function setStatus($status) {
        $this->status = $status;
    }

    public function getResponsavel() {
        return $this->responsavel;
    }
    
    public function setResponsavel($responsavel) {
        $this->responsavel = $responsavel;
    }

}
