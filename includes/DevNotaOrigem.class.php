<?php 

Class DevNotaOrigem{
        private $codOcorrencia;
	private $serie; 
	private $nota; 
	private $data; 
	public  $responsavel; 
	private $origem;
	private $descricaoResponsavel;
	private $MotivoResponsabilidade;
        private $codCliente;
        private $valorTotDevolucao;
        private $origemVenda;
        private $centroDist;
        private $divisao;
	
        
	public function setCodOcorrencia($codOcorrencia){
		$this->codOcorrencia = $codOcorrencia;		
	}
	public function getCodOcorrencia(){
		return $this->codOcorrencia; 
	}
        
	public function setSerie($serie){
		$this->serie = $serie;		
	}
	public function getSerie(){
		return $this->serie; 
	}

	public function setNota($nota){
		$this->nota = $nota;		
	}
	public function getNota(){
		return $this->nota; 
	}
	
	public function setData($data){
		$this->data = $data;		
	}
	public function getData(){
		return $this->data; 
	}
	
	public function setResponsavel($responsavel){
		$this->responsavel = $responsavel;		
	}
	public function getResponsavel(){
		return $this->responsavel; 
	}
	
	public function setOrigem($origem){
		$this->origem = $origem;		
	}
	public function getOrigem(){
		return $this->origem; 
	}
	
    public function setDescricaoResponsavel($descricaoResponsavel) {
        $this->descricaoResponsavel = $descricaoResponsavel;
    }
	public function getDescricaoResponsavel() {
        return $this->descricaoResponsavel;
    }
    
	public function setMotivoResponsabilidade($MotivoResponsabilidade){
		$this->MotivoResponsabilidade = $MotivoResponsabilidade;		
	}
	public function getMotivoResponsabilidade(){
		return $this->MotivoResponsabilidade; 
	}
        
/*
CLI_CO_NUMERO	NUMBER(8,0)
NTO_VL_DEVOLUCAO	NUMBER(17,2)
NTO_NU_ORIGEM_VENDA	VARCHAR2(2 BYTE)
NTO_CO_CDIST	NUMBER(2,0)
 */ 

        public function setCodCliente($codCliente){
		$this->codCliente = $codCliente;		
	}
	public function getCodCliente(){
		return $this->codCliente; 
	}

        public function setValorTotDevolucao($valorTotDevolucao){
		$this->valorTotDevolucao = $valorTotDevolucao;		
	}
	public function getValorTotDevolucao(){
		return $this->valorTotDevolucao; 
	}

        public function setOrigemVenda($origemVenda){
		$this->origemVenda = $origemVenda;		
	}
	public function getOrigemVenda(){
		return $this->origemVenda; 
	}

        public function setCentroDist($centroDist){
		$this->centroDist = $centroDist;		
	}
	public function getCentroDist(){
		return $this->centroDist; 
	}

        public function setDivisao($divisao){
		$this->divisao = $divisao;		
	}
	public function getDivisao(){
		return $this->divisao; 
	}
        
        
        
        
}
