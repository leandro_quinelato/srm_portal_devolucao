<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MotivoReprova
 *
 * @author geovanni.info
 */
class MotivoReprova {

    private $codReprova;
    private $descricao;
    private $dataCadastro;
    private $dataExclusao;

    
    public function getCodReprova() {
        return $this->codReprova;
    }

    public function setCodReprova($codReprova) {
        $this->codReprova = $codReprova;
    }
    
    public function getDescricao() {
        return $this->descricao;
    }

    public function setDescricao($descricao) {
        $this->descricao = $descricao;
    }
    
    
    public function getDataCadastro() {
        return $this->dataCadastro;
    }

    public function setDataCadastro($dataCadastro) {
        $this->dataCadastro = $dataCadastro;
    }
    
    public function getDataExclusao() {
        return $this->dataExclusao;
    }

    public function setDataExclusao($dataExclusao) {
        $this->dataExclusao = $dataExclusao;
    }

}
