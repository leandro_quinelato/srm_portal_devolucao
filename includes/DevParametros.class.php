<?php 
Class devParametros{
    private $emailDivAlimentar;
    private $emailDivFarma;
    private $emailDivHosp;
    private $emailDiretoria;
    private $emailTransporte;
    private $emailDevolucao;
    private $emailGerCdBauru;
    private $emailGerLogistica;
    private $emailCooFiscal;
    private $emailGerAdminis;
    private $numeroDiasPasso;
    private $numeroVoltaPasso;
    private $numValorAprovacaoAut;
    
    //chamado 69898 - 12/08/2015
    private $numDiasCadalimentar;
    private $numDiasCadFarmaDH;
    private $numDiasAprovaGerente;
    private $numDiasLiberacaoNF;
	
	private $emailReentrega;

    private $listaEmailReentrega;
	

    public function getEmailDivAlimentar() {
        return $this->emailDivAlimentar;
    }

    public function setEmailDivAlimentar($emailDivAlimentar) {
        $this->emailDivAlimentar = $emailDivAlimentar;
    }

    public function getEmailDivFarma() {
        return $this->emailDivFarma;
    }

    public function setEmailDivFarma($emailDivFarma) {
        $this->emailDivFarma = $emailDivFarma;
    }

    public function getEmailDivHosp() {
        return $this->emailDivHosp;
    }

    public function setEmailDivHosp($emailDivHosp) {
        $this->emailDivHosp = $emailDivHosp;
    }

    public function getEmailDiretoria() {
        return $this->emailDiretoria;
    }

    public function setEmailDiretoria($emailDiretoria) {
        $this->emailDiretoria = $emailDiretoria;
    }

    public function getEmailTransporte() {
        return $this->emailTransporte;
    }

    public function setEmailTransporte($emailTransporte) {
        $this->emailTransporte = $emailTransporte;
    }

    public function getEmailDevolucao() {
        return $this->emailDevolucao;
    }

    public function setEmailDevolucao($emailDevolucao) {
        $this->emailDevolucao = $emailDevolucao;
    }

    public function getEmailGerCdBauru() {
        return $this->emailGerCdBauru;
    }

    public function setEmailGerCdBauru($emailGerCdBauru) {
        $this->emailGerCdBauru = $emailGerCdBauru;
    }

    public function getEmailGerLogistica() {
        return $this->emailGerLogistica;
    }

    public function setEmailGerLogistica($emailGerLogistica) {
        $this->emailGerLogistica = $emailGerLogistica;
    }

    public function getEmailCooFiscal() {
        return $this->emailCooFiscal;
    }

    public function setEmailCooFiscal($emailCooFiscal) {
        $this->emailCooFiscal = $emailCooFiscal;
    }

    public function getEmailGerAdminis() {
        return $this->emailGerAdminis;
    }

    public function setEmailGerAdminis($emailGerAdminis) {
        $this->emailGerAdminis = $emailGerAdminis;
    }

    public function getNumeroDiasPasso() {
        return $this->numeroDiasPasso;
    }

    public function setNumeroDiasPasso($numeroDiasPasso) {
        $this->numeroDiasPasso = $numeroDiasPasso;
    }
    
    public function getNumeroVoltaPasso() {
        return $this->numeroDiasPasso;
    }

    public function setNumeroVoltaPasso($numeroVoltaPasso) {
        $this->numeroVoltaPasso = $numeroVoltaPasso;
    }
    
    public function getNumValorAprovacaoAut() {
        return $this->numValorAprovacaoAut;
    }

    public function setNumValorAprovacaoAut($numValorAprovacaoAut) {
        $this->numValorAprovacaoAut = $numValorAprovacaoAut;
    }
      
    public function getNumDiasCadalimentar() {
        return $this->numDiasCadalimentar;
    }

    public function setNumDiasCadalimentar($numDiasCadalimentar) {
        $this->numDiasCadalimentar = $numDiasCadalimentar;
    }
    
    public function getNumDiasCadFarmaDH() {
        return $this->numDiasCadFarmaDH;
    }

    public function setNumDiasCadFarmaDH($numDiasCadFarmaDH) {
        $this->numDiasCadFarmaDH = $numDiasCadFarmaDH;
    }
    
    public function getNumDiasAprovaGerente() {
        return $this->numDiasAprovaGerente;
    }

    public function setNumDiasAprovaGerente($numDiasAprovaGerente) {
        $this->numDiasAprovaGerente = $numDiasAprovaGerente;
    }
    
    public function getNumDiasLiberacaoNF() {
        return $this->numDiasLiberacaoNF;
    }

    public function setNumDiasLiberacaoNF($numDiasLiberacaoNF) {
        $this->numDiasLiberacaoNF = $numDiasLiberacaoNF;
    }
    
    public function getEmailReentrega() {
        return $this->emailReentrega;
    }

    public function setEmailReentrega($emailReentrega) {
        $this->emailReentrega = $emailReentrega;
    } 

    public function getListaEmailReentrega() {
        return $this->listaEmailReentrega;
    }

    public function setListaEmailReentrega($listaEmailReentrega) {
        $this->listaEmailReentrega = $listaEmailReentrega;
    }   
        
}
