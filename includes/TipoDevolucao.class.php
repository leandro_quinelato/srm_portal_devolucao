<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TipoDevolução
 *
 * @author geovanni.info
 * 
 * TABELA: DEV_TIPO_DEVOLUCAO
 * TID_CO_NUMERO
 * TID_CO_DESCRICAO
 * TID_IN_STATUS
 * TID_IN_DEVOLUCAO  -- P - PARCIAL , I - INTEGRAL
 * 
 */
class TipoDevolucao {

	private $codigo;
	private $descricao; 
	private $status;
        private $tipoDevolucao;

	public function getCodigo()
	{
		return $this->codigo; 
	}
	public function setCodigo($codigo)
	{
		$this->codigo = $codigo; 
	}
        
	public function getDescricao()
	{
		return $this->descricao; 
	}
	public function setDescricao($descricao)
	{
		$this->descricao = $descricao; 
	}
        
	public function getStatus()
	{
		return $this->status; 
	}
	public function setStatus($status)
	{
		$this->status = $status; 
	}
        
	public function getTipoDevolucao()
	{
		return $this->tipoDevolucao; 
	}
	public function setTipoDevolucao($tipoDevolucao)
	{
		$this->tipoDevolucao = $tipoDevolucao; 
	}
        
    
    
}
