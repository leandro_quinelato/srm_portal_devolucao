<?php


/**
 * Description of OcorrenciaArquivos
 *
 * @author geovanni.info
 * 
 * DOC_CO_NUMERO
 * DTA_CO_CODIGO
 * DOA_NO_ARQCLIENTE
 * DOA_NO_ARQSYSTEM
 * USU_CO_NUMERO
 * DOA_TX_OBSERVACAO
 * DOA_DT_CRIACAO
 * 
 */
class OcorrenciaArquivos {
    private $codOcorrencia;
    private $codigoTipoArquivo;
    private $nomeArquivoCliente;
    private $nomeArquivoSystem;
    private $codigoUsuario;
    private $ObservacaoArquivo;
    private $dataCriacaoArquivo;
    
    public function getCodOcorrencia() {
        return $this->codOcorrencia;
    }

    public function setCodOcorrencia($codOcorrencia) {
        $this->codOcorrencia = $codOcorrencia;
    }
    
    
    public function getCodigoTipoArquivo() {
        return $this->codigoTipoArquivo;
    }

    public function setCodigoTipoArquivo($codigoTipoArquivo) {
        $this->codigoTipoArquivo = $codigoTipoArquivo;
    }
    
    
    public function getNomeArquivoCliente() {
        return $this->nomeArquivoCliente;
    }

    public function setNomeArquivoCliente($nomeArquivoCliente) {
        $this->nomeArquivoCliente = $nomeArquivoCliente;
    }
    
    
    public function getNomeArquivoSystem() {
        return $this->nomeArquivoSystem;
    }

    public function setNomeArquivoSystem($nomeArquivoSystem) {
        $this->nomeArquivoSystem = $nomeArquivoSystem;
    }
    
    
    public function getCodigoUsuario() {
        return $this->codigoUsuario;
    }

    public function setCodigoUsuario($codigoUsuario) {
        $this->codigoUsuario = $codigoUsuario;
    }
    
    
    public function getObservacaoArquivo() {
        return $this->ObservacaoArquivo;
    }

    public function setObservacaoArquivo($ObservacaoArquivo) {
        $this->ObservacaoArquivo = $ObservacaoArquivo;
    }
    
    
    public function getDataCriacaoArquivo() {
        return $this->dataCriacaoArquivo;
    }

    public function setDataCriacaoArquivo($dataCriacaoArquivo) {
        $this->dataCriacaoArquivo = $dataCriacaoArquivo;
    } 
    
    
}
