<?php

/**
 * Description of OcorrenciaStatus
 *
 * @author geovanni.info
 * 
 * TABELA: DEV_OCORRENCIA_STATUS
 * DOC_CO_NUMERO
 * DST_CO_NUMERO
 * USU_CO_NUMERO
 * DOS_DT_ENTRADA
 * DOS_DT_SAIDA
 * 
 */
class OcorrenciaStatus {

    private $codigoOcorrencia;
    private $codStatus;
    private $descStatus;
    private $usuario;
    private $dataEntrada;
    private $dataSaida;

    public function getCodigoOcorrencia() {
        return $this->codigoOcorrencia;
    }

    public function setCodigoOcorrencia($codigoOcorrencia) {
        $this->codigoOcorrencia = $codigoOcorrencia;
    }

    public function getCodStatus() {
        return $this->codStatus;
    }

    public function setCodStatus($codStatus) {
        $this->codStatus = $codStatus;
    }

    public function getUsuario() {
        return $this->usuario;
    }

    public function setUsuario($usuario) {
        $this->usuario = $usuario;
    }

    public function getUsuarioNome() {
        return $this->usuarioNome;
    }

    public function setUsuarioNome($usuarioNome) {
        $this->usuarioNome = $usuarioNome;
    }

    public function getDataEntrada() {
        return $this->dataEntrada;
    }

    public function setDataEntrada($dataEntrada) {
        $this->dataEntrada = $dataEntrada;
    }

    public function getDataSaida() {
        return $this->dataSaida;
    }

    public function setDataSaida($dataSaida) {
        $this->dataSaida = $dataSaida;
    }
    
    public function getDescStatus() {
        return $this->descStatus;
    }

    public function setDescStatus($descStatus) {
        $this->descStatus = $descStatus;
    }

}
