<?php


/**
 * Description of Conferente
 *
 * @author geovanni.info
 * 
 * DOC_CO_NUMERO
 * EMP_CO_NUMERO
 * ETB_CO_NUMERO
 * COL_CO_CRACHA
 * DOC_QT_VOLUME
 */
class Conferente {

    private $codOcorrencia;
    private $empresa;
    private $estabelecimento;
    private $cracha;
    private $volume;
    
    public function getCodOcorrencia() {
        return $this->codOcorrencia;
    }

    public function setCodOcorrencia($codOcorrencia) {
        $this->codOcorrencia = $codOcorrencia;
    }
    
    public function getEmpresa() {
        return $this->empresa;
    }

    public function setEmpresa($empresa) {
        $this->empresa = $empresa;
    }
    
    public function getEstabelecimento() {
        return $this->estabelecimento;
    }

    public function setEstabelecimento($estabelecimento) {
        $this->estabelecimento = $estabelecimento;
    }
    
    public function getCracha() {
        return $this->cracha;
    }

    public function setCracha($cracha) {
        $this->cracha = $cracha;
    }
    
    public function getVolume() {
        return $this->volume;
    }

    public function setVolume($volume) {
        $this->volume = $volume;
    }
    
    
    
}
