<?php
/**
* @author : Rogerio rkyKO
* @version 0.0.1 
* @date : 12/01/2010
*
* Classe POJO para departamento
*/

Class CalDepartamento
{
	private $codDepartamento;

	
	public function setCodDepartamento( $codigo )
	{
		$this->codDepartamento = $codigo;
	}

	public function getCodDepartamento()
	{
		return $this->codDepartamento;
	}

}
?>