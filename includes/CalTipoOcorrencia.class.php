<?php
/**
 * @author : Rogerio rkyKO
 * @version 0.0.1
 * @date : 15/12/2009
 *
 * Classe POJO para o tipo de ocorrencia
 */

Class CalTipoOcorrencia
{

    private $Departamento;
    private $codTipoOcorrencia;
    private $nomTipoOcorrencia;
    private $numTempoAtendimento;
    private $numTempoFechamento;
    private $indAtivo;
    
    private $regra; /* Chamado 59343 - 30/06/2015 */
    
    
    public function setDepartamentoTipoOcorrencia(  $departamento )
    {
        $this->Departamento = $departamento;
    }
    
    public function getDepartamentoTipoOcorrencia()
    {
        return $this->Departamento;
    }
	
    public function setCodTipoOcorrencia( $codTipoOcorrencia )
    {
        $this->codTipoOcorrencia = $codTipoOcorrencia;
    }

    public function getCodTipoOcorrencia()
    {
        return $this->codTipoOcorrencia;
    }

    public function setNomTipoOcorrencia( $nomTipoOcorrencia )
    {
        $this->nomTipoOcorrencia = $nomTipoOcorrencia;
    }

    public function getNomTipoOcorrencia()
    {
        return $this->nomTipoOcorrencia;
    }

    public function setNumTempoAtendimento( $numTempoAtendimento )
    {
        $this->numTempoAtendimento = $numTempoAtendimento;
    }

    public function getNumTempoAtendimento()
    {
        return $this->numTempoAtendimento;
    }

    public function setNumTempoFechamento( $numTempoFechamento )
    {
        $this->numTempoFechamento = $numTempoFechamento;
    }

    public function getNumTempoFechamento()
    {
        return $this->numTempoFechamento;
    }

    public function setIndAtivoTipoOcorrencia( $indAtivo )
    {
        $this->indAtivo = $indAtivo;
    }

    public function getIndAtivoTipoOcorrencia()
    {
        return $this->indAtivo;
    }
    
    public function setRegra( $regra )
    {
        $this->regra = $regra;
    }

    public function getRegra()
    {
        return $this->regra;
    }
}
?>