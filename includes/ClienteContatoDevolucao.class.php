<?php

/*
CLI_CO_NUMERO      NUMBER(8,0)
DCC_IN_CONTATO     VARCHAR2(1 BYTE)
DCC_NO_DESCONTATO  VARCHAR2(80 BYTE)
DCC_NO_CONTATO	   VARCHAR2(200 BYTE)
DCC_DT_CADASTRO	   DATE
DCC_DT_EXCLUSAO	   DATE
 */

/**
 * Description of ClienteContatoDevolucao
 *
 * @author geovanni.info
 */
class ClienteContatoDevolucao {
    
    private $codigoContato;
    private $codigoCliente;
    private $tipoContato;
    private $DescContato;
    private $contato;
    private $dataCadastro;
    private $dataExclusao;
    private $usuario;


    public function getCodigoContato() {
        return $this->codigoContato;
    }

    public function setCodigoContato($codigoContato) {
        $this->codigoContato = $codigoContato;
    }

    public function getCodigoCliente() {
        return $this->codigoCliente;
    }

    public function setCodigoCliente($codigoCliente) {
        $this->codigoCliente = $codigoCliente;
    }
    
    public function getTipoContato() {
        return $this->tipoContato;
    }

    public function setTipoContato($tipoContato) {
        $this->tipoContato = $tipoContato;
    }
    
    public function getDescContato() {
        return $this->DescContato;
    }

    public function setDescContato($DescContato) {
        $this->DescContato = $DescContato;
    }
    
    public function getContato() {
        return $this->contato;
    }

    public function setContato($Contato) {
        $this->contato = $Contato;
    }
    
    public function getDataCadastro() {
        return $this->dataCadastro;
    }

    public function setDataCadastro($dataCadastro) {
        $this->dataCadastro = $dataCadastro;
    }
    
    
    public function getDataExclusao() {
        return $this->dataExclusao;
    }

    public function setDataExclusao($dataExclusao) {
        $this->dataExclusao = $dataExclusao;
    }
    
    public function getUsuario() {
        return $this->usuario;
    }

    public function setUsuario($usuario) {
        $this->usuario = $usuario;
    }
    
    
    
}
