<?php

/**
 * Description of Status
 *
 * @author geovanni.info
 * 
 * 
 * TABELA: DEV_STATUS
 * DST_CO_NUMERO	NUMBER(4,0)
 * DST_NO_DESCRICAO	VARCHAR2(80 BYTE)
 * DST_DT_CRIACAO	DATE
 * DST_IN_STATUS	CHAR(1 BYTE)
 * 
 */
class Status {

	private $codigo;
	private $descricao; 
        private $dataCriacao;
	private $status;

	public function getCodigo()
	{
		return $this->codigo; 
	}
	public function setCodigo($codigo)
	{
		$this->codigo = $codigo; 
	}
        
	public function getDescricao()
	{
		return $this->descricao; 
	}
	public function setDescricao($descricao)
	{
		$this->descricao = $descricao; 
	}
        
	public function getDataCriacao()
	{
		return $this->dataCriacao; 
	}
	public function setDataCriacao($dataCriacao)
	{
		$this->dataCriacao = $dataCriacao; 
	}
        
	public function getStatus()
	{
		return $this->status; 
	}
	public function setStatus($status)
	{
		$this->status = $status; 
	}
        
    
}
