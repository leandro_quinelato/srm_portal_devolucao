<?php

/*
 * @author: Bruno Andrade;
 * @date: 25-05-2017
 * @version: 0.0.0.1
 */

//include_once("../DevParametros.class.php");

Class ParametroValorDevolucaoDao {

    private $conn;

    function __construct() {
        $this->conn = new DaoSistema();
        $this->conn->conectar();
    }

    public function alterar(devParametrosValorDevolucao $ObjParametro) {
        
        $sql = "UPDATE DEV_PARAMETROS_VALOR_DEVOLUCAO SET 
                                 PVD_VL_1_F = {$ObjParametro->getValor1F()}
                                ,PVD_VL_1_O = {$ObjParametro->getValor1O()}
								,PVD_VL_1_H = {$ObjParametro->getValor1H()}
								,PVD_VL_1_A = {$ObjParametro->getValor1A()}
								,PVD_VL_46_F = {$ObjParametro->getValor46F()}
                                ,PVD_VL_46_O = {$ObjParametro->getValor46O()}
								,PVD_VL_46_H = {$ObjParametro->getValor46H()}
								,PVD_VL_46_A = {$ObjParametro->getValor46A()}
								,PVD_VL_48_F = {$ObjParametro->getValor48F()}
                                ,PVD_VL_48_O = {$ObjParametro->getValor48O()}
								,PVD_VL_48_H = {$ObjParametro->getValor48H()}
								,PVD_VL_48_A = {$ObjParametro->getValor48A()}
								,PVD_VL_49_F = {$ObjParametro->getValor49F()}
                                ,PVD_VL_49_O = {$ObjParametro->getValor49O()}
								,PVD_VL_49_H = {$ObjParametro->getValor49H()}
								,PVD_VL_49_A = {$ObjParametro->getValor49A()}
                                ,PVD_VL_50_F = {$ObjParametro->getValor50F()}								
		";

        //echo $sql;
        if ($this->conn->execSql($sql)) {
            return true;
        }
        return false;
    }

    public function consultar() {
        $sql = "
			SELECT * FROM dev_parametros_valor_devolucao
		";

        $result = $this->conn->execSql($sql);
        $array = oci_fetch_object($result);
        $ObjParametro = new devParametrosValorDevolucao();
        $ObjParametro->setValor1F($array->PVD_VL_1_F);
		$ObjParametro->setValor1O($array->PVD_VL_1_O);
		$ObjParametro->setValor1H($array->PVD_VL_1_H);
		$ObjParametro->setValor1A($array->PVD_VL_1_A);
        $ObjParametro->setValor46F($array->PVD_VL_46_F);
		$ObjParametro->setValor46O($array->PVD_VL_46_O);
		$ObjParametro->setValor46H($array->PVD_VL_46_H);
		$ObjParametro->setValor46A($array->PVD_VL_46_A);
        $ObjParametro->setValor48F($array->PVD_VL_48_F);
		$ObjParametro->setValor48O($array->PVD_VL_48_O);
		$ObjParametro->setValor48H($array->PVD_VL_48_H);
		$ObjParametro->setValor48A($array->PVD_VL_48_A);
        $ObjParametro->setValor49F($array->PVD_VL_49_F);
		$ObjParametro->setValor49O($array->PVD_VL_49_O);
		$ObjParametro->setValor49H($array->PVD_VL_49_H);
		$ObjParametro->setValor49A($array->PVD_VL_49_A);
        $ObjParametro->setValor50F($array->PVD_VL_50_F);		

        return $ObjParametro;
    }

}
