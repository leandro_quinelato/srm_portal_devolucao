<?php

//include_once( "../TipoDevolucao.class.php" );

/**
 * Description of TipoDevolucaoDao
 *
 * @author geovanni.info
 * 
 * TABELA: DEV_TIPO_DEVOLUCAO
 * TID_CO_NUMERO
 * TID_CO_DESCRICAO
 * TID_IN_STATUS
 * TID_IN_DEVOLUCAO  -- P - PARCIAL , I - INTEGRAL
 *  
 */
class TipoDevolucaoDao {

    private $conn;

    function __construct() {
        $this->conn = new DaoSistema();
        $this->conn->conectar();
    }

    public function listarTipoDevolucao(TipoDevolucao $TipoDevolucao) {

        try {
            $sql = "
                        SELECT *
                        FROM DEV_TIPO_DEVOLUCAO
                        WHERE 1=1
            ";

            if ($TipoDevolucao->getStatus() == "I") {
                $sql .= "    AND   TID_IN_STATUS = " . $TipoDevolucao->getStatus();
            } else if ($TipoDevolucao->getStatus() == "A") {
                $sql .= "    AND   TID_IN_STATUS IS NULL";
            }

            $sql .= "
                    ORDER BY TID_CO_NUMERO
            ";

            $result = $this->conn->execSql($sql);
            while ($rsTipoDevolucao = oci_fetch_object($result)) {
                $TipoDevolucao = new TipoDevolucao();
                $TipoDevolucao->setCodigo($rsTipoDevolucao->TID_CO_NUMERO);
                $TipoDevolucao->setDescricao($rsTipoDevolucao->TID_CO_DESCRICAO);
                $TipoDevolucao->setStatus($rsTipoDevolucao->TID_IN_STATUS);
                $TipoDevolucao->setTipoDevolucao($rsTipoDevolucao->TID_IN_DEVOLUCAO);

                $ObjTiposDevolucao[] = $TipoDevolucao;
            }
            return $ObjTiposDevolucao;
        } catch (Exception $e) {
            print( "[ ERRO NO METODO consultarTipoOcorrencia ]: " . $e->getMessage());
            return null;
        }
    }

    public function consultarTipoDevolucao(TipoDevolucao $TipoDevolucao) {

        try {
            $sql = "
                        SELECT *
                        FROM DEV_TIPO_DEVOLUCAO
                        WHERE TID_CO_NUMERO = {$TipoDevolucao->getCodigo()}
            ";

            $result = $this->conn->execSql($sql);
            $rsTipoDevolucao = oci_fetch_object($result);
            $TipoDevolucao = new TipoDevolucao();
            $TipoDevolucao->setCodigo($rsTipoDevolucao->TID_CO_NUMERO);
            $TipoDevolucao->setDescricao($rsTipoDevolucao->TID_CO_DESCRICAO);
            $TipoDevolucao->setStatus($rsTipoDevolucao->TID_IN_STATUS);
            $TipoDevolucao->setTipoDevolucao($rsTipoDevolucao->TID_IN_DEVOLUCAO);


            return $TipoDevolucao;
        } catch (Exception $e) {
            print( "[ ERRO NO METODO consultarTipoOcorrencia ]: " . $e->getMessage());
            return null;
        }
    }

}
