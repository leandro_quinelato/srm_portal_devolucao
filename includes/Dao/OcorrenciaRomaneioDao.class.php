<?php

//include_once("../OcorrenciaRomaneio.class.php");

/**
 * Description of OcorrenciaRomaneioDao
 *
 * @author bruno.andrade
 */
class OcorrenciaRomaneioDao {

    private $conn;

    function __construct() {
        $this->conn = new DaoSistema();
        $this->conn->conectar();
    }

    public function inserirRomaneio(OcorrenciaRomaneio $OcorrenciaRomaneio) {

        $sql = "
            INSERT
            INTO DEV_OCORRENCIA_ROMANEIO
              (
                DOC_CO_NUMERO,
                DRO_CO_NUMERO
              )
              VALUES
              (
                {$OcorrenciaRomaneio->getCodigoOcorrencia()},
                {$OcorrenciaRomaneio->getCodigoRomaneio()}
              )
            ";

		//echo $sql;//die();
        if ($this->conn->execSql($sql)) {
            return true;
        }
        return false;
    }

    public function consultarOcorrenciaRomaneio(OcorrenciaRomaneio $OcorrenciaRomaneio) {

        $sql = "
				SELECT DOC.DOC_CO_NUMERO,
					   DRO.DRO_CO_NUMERO,
					   DOR.DOR_IN_BAIXA
				FROM  DEV_OCORRENCIA_ROMANEIO DOR
				WHERE DOR.DRO_CO_NUMERO = {$OcorrenciaRomaneio->getCodigoRomaneio()}		
		";

        if ($OcorrenciaRomaneio->getCodigoOcorrencia()) {
            $sql .= "
                       AND   DOC.DOC_CO_NUMERO  = {$OcorrenciaRomaneio->getCodigoOcorrencia()}
                    ";
        }    
        
		//echo $sql;
        $result = $this->conn->execSql($sql);
        while ($RRomaneio = oci_fetch_object($result)) {
            $OcorrenciaRomaneio = new OcorrenciaRomaneio();
            $OcorrenciaRomaneio->setCodigoOcorrencia($RRomaneio->DOC_CO_NUMERO);
            $OcorrenciaRomaneio->setCodigoRomaneio($RRomaneio->DRO_CO_NUMERO);
            $OcorrenciaRomaneio->setIndicadorBaixa($RRomaneio->DOR_IN_BAIXA);

            $ObjOcorrenciaRomaneio[] = $OcorrenciaRomaneio;
        }
        return $ObjOcorrenciaRomaneio;
    }
    
    
    public function baixarOcorrenciaRomaneio(OcorrenciaRomaneio $OcorrenciaRomaneio) {
        
        $sql = "UPDATE DEV_OCORRENCIA_ROMANEIO SET 
					DOR_IN_BAIXA = 1
                WHERE DOC_CO_NUMERO   = {$OcorrenciaRomaneio->getCodigoOcorrencia()}
                AND DRO_CO_NUMERO     = {$OcorrenciaRomaneio->getCodigoRomaneio()}   
               ";
                
		//echo $sql;//die();
        if ($this->conn->execSql($sql)) {
            return true;
        }
        return false;
   
    }
    

}