<?php

//include_once("../Conferente.class.php");

/**
 * Description of ConferenteDao
 *
 * @author geovanni.info
 */
class ConferenteDao {

    private $conn;

    function __construct() {
        $this->conn = new DaoSistema();
        $this->conn->conectar();
    }

    public function consultarEmpresa($emp_co_numero = NULL) {

        $sql = "
                           SELECT * 
                           FROM GLOBAL.GLB_EMPRESA 
                           WHERE 1=1
                       ";

        if ($emp_co_numero) {
            $sql .= "AND EMP_CO_NUMERO = {$emp_co_numero}";
        }

        $sql .= "ORDER BY EMP_NO_APELIDO";

        if ($result = $this->conn->execSql($sql)) {
            return $result;
        }

        return false;
    }

    public function consultarEstab($emp_co_numero) {

        $sql = "
                           SELECT * 
                           FROM GLOBAL.GLB_ESTABELECIMENTO
                           WHERE EMP_CO_NUMERO = {$emp_co_numero}
                       ";


        $sql .= "ORDER BY ETB_NO_APELIDO";

        if ($result = $this->conn->execSql($sql)) {
            return $result;
        }

        return false;
    }

    public function consultarColaborador($empresa, $estabelecimento, $cracha) {
        $sql = "
                     SELECT *
                     FROM   GLOBAL.GLB_COLABORADOR
                     WHERE  EMP_CO_NUMERO = {$empresa}
                     AND    ETB_CO_NUMERO = {$estabelecimento}
                     AND    COL_CO_CRACHA = {$cracha}
                   ";

//                     
//           echo "<pre>";
//                print_r($sql);
//           echo "</pre>";     

        if ($result = $this->conn->execSql($sql)) {
            return $result;
        }

        return false;
    }

    public function consultar($empresa = null, $estabelecimento = null, $cracha = null) {
        $sql = "
                     SELECT *
                     FROM   DEV_CONFERENTE
                     WHERE  1 = 1
               ";
        
        if ( $empresa ){
            $sql .= "     AND    EMP_CO_NUMERO = {$empresa}"; 
        }
        
        if ( $estabelecimento ){
            $sql .= "     AND    ETB_CO_NUMERO = {$estabelecimento}"; 
        }
        
        if ( $cracha ){
            $sql .= "     AND    COL_CO_CRACHA = {$cracha}"; 
        }


        if ($result = $this->conn->execSql($sql)) {
            return $result;
        }

        return false;
    }

    public function adicionar($empresa, $estabelecimento, $cracha) {
        $sql = "
                            INSERT
                            INTO DEV_CONFERENTE
                            (
                              EMP_CO_NUMERO,
                              ETB_CO_NUMERO,
                              COL_CO_CRACHA
                            )
                            VALUES
                            (
                              {$empresa},
                              {$estabelecimento},
                              {$cracha}
                            )  
                       ";

        if ($this->conn->execSql($sql)) {
            return true;
        }

        return false;
    }
    
    public function excluir( $empresa, $estabelecimento, $cracha ){
        
        $sql = "
                 DELETE FROM DEV_CONFERENTE
                 WHERE EMP_CO_NUMERO = {$empresa}
                 AND   ETB_CO_NUMERO = {$estabelecimento}
                 AND   COL_CO_CRACHA = {$cracha}
               ";

        if ($this->conn->execSql($sql)) {
            return true;
        }

        return false;
                 
                 
                 
    }
    
    
    public function consultarConferentes($empresa = null, $estab = null, $cracha = null, $verif_ativos = null ) {

        $sql = "                 SELECT COF.EMP_CO_NUMERO,
                                        EMP.EMP_NO_APELIDO,
                                        COF.ETB_CO_NUMERO,
                                        ETB.ETB_NO_APELIDO,
                                        COF.COL_CO_CRACHA,
                                        COL.COL_NO_COLABORADOR,
                                        COL.COL_DT_DEMISSAO
                                FROM DEV_CONFERENTE COF
                                INNER JOIN GLOBAL.GLB_COLABORADOR COL
                                    ON COL.EMP_CO_NUMERO  = COF.EMP_CO_NUMERO
                                    AND COL.ETB_CO_NUMERO = COF.ETB_CO_NUMERO
                                    AND COL.COL_CO_CRACHA = COF.COL_CO_CRACHA
                                INNER JOIN GLOBAL.GLB_EMPRESA EMP 
                                    ON EMP.EMP_CO_NUMERO = COF.EMP_CO_NUMERO 
                                INNER JOIN GLOBAL.GLB_ESTABELECIMENTO ETB 
                                    ON  ETB.EMP_CO_NUMERO = COF.EMP_CO_NUMERO 
                                    AND ETB.ETB_CO_NUMERO = COF.ETB_CO_NUMERO
                                WHERE 1 = 1 ";

        if ($empresa) {
            $sql.= "  AND COF.EMP_CO_NUMERO = {$empresa}";
        }

        if ($estab) {
            $sql.= "  AND COF.ETB_CO_NUMERO = {$estab}";
        }

        if ($cracha) {
            $sql.= "  AND COF.COL_CO_CRACHA = {$cracha}";
        }
        
        if ($verif_ativos == "false"){
            $sql.= "  AND COL.COL_DT_DEMISSAO IS NULL";
        }


        $sql.= "
                     ORDER BY COF.ETB_CO_NUMERO, COF.EMP_CO_NUMERO, COL.COL_NO_COLABORADOR
                  ";

//          echo "<pre>".$sql."</pre>"; die();// geovanni testes

        if ($result = $this->conn->execSql($sql)) {
            return $result;
        }
        return false;
    }

    
    public function pesquisarConferentes($empresa = null, $estab = null, $cracha = null, $verif_ativos = null, $listagem = 10, $pagina = 1) {

        $limit = ($listagem * $pagina);
        $start = (($limit - $listagem) + 1);

        $sql = "        
                    SELECT * FROM 
                        (
                          SELECT EMP_CO_NUMERO,
                                EMP_NO_APELIDO,
                                ETB_CO_NUMERO,
                                ETB_NO_APELIDO,
                                COL_CO_CRACHA,
                                COL_NO_COLABORADOR,
                                COL_DT_DEMISSAO,
                                ROWNUM AS LINHA
                                FROM
                                (SELECT COF.EMP_CO_NUMERO,
                                        EMP.EMP_NO_APELIDO,
                                        COF.ETB_CO_NUMERO,
                                        ETB.ETB_NO_APELIDO,
                                        COF.COL_CO_CRACHA,
                                        COL.COL_NO_COLABORADOR,
                                        COL.COL_DT_DEMISSAO
                                FROM DEV_CONFERENTE COF
                                INNER JOIN GLOBAL.GLB_COLABORADOR COL
                                    ON COL.EMP_CO_NUMERO  = COF.EMP_CO_NUMERO
                                    AND COL.ETB_CO_NUMERO = COF.ETB_CO_NUMERO
                                    AND COL.COL_CO_CRACHA = COF.COL_CO_CRACHA
                                INNER JOIN GLOBAL.GLB_EMPRESA EMP 
                                    ON EMP.EMP_CO_NUMERO = COF.EMP_CO_NUMERO 
                                INNER JOIN GLOBAL.GLB_ESTABELECIMENTO ETB 
                                    ON  ETB.EMP_CO_NUMERO = COF.EMP_CO_NUMERO 
                                    AND ETB.ETB_CO_NUMERO = COF.ETB_CO_NUMERO
                                WHERE 1 = 1 ";

        if ($empresa) {
            $sql.= "  AND COF.EMP_CO_NUMERO = {$empresa}";
        }

        if ($estab) {
            $sql.= "  AND COF.ETB_CO_NUMERO = {$estab}";
        }

        if ($cracha) {
            $sql.= "  AND COF.COL_CO_CRACHA = {$cracha}";
        }
        
        if (!$verif_ativos){
            $sql.= "  AND COL.COL_DT_DEMISSAO IS NULL";
        }


        $sql.= "
                                 ORDER BY COF.ETB_CO_NUMERO, COF.EMP_CO_NUMERO, COL.COL_NO_COLABORADOR
                              )
                          WHERE ROWNUM <=  {$limit}
                    ) WHERE LINHA >= {$start}
                  ";

//          echo "<pre>".$sql."</pre>"; die();// geovanni testes

        if ($result = $this->conn->execSql($sql)) {
            return $result;
        }
        return false;
    }
    
    
    public function qtdConferenciaColaborador($empresa, $estab, $cracha){
        
        $sql = "
                 SELECT COUNT(*) AS QTD_CONF
                 FROM   DEV_OCORRENCIA
                 WHERE  EMP_CO_NUMERO = {$empresa}
                 AND    ETB_CO_NUMERO = {$estab}
                 AND    COL_CO_CRACHA = {$cracha}
                 AND    DOC_DT_CONFERENCIA IS NOT NULL

               ";

        if ($result = $this->conn->execSql($sql)) {
            OCIFetchInto($result, $consulta, OCI_ASSOC);
            return $consulta["QTD_CONF"];
        }

        return false; 
    }
    
    public function consultarConferenteOcorrencia( Conferente $Conferente ) {
        $sql = "
		    SELECT DOC_CO_NUMERO,
                           EMP_CO_NUMERO,
                           ETB_CO_NUMERO,
                           COL_CO_CRACHA,
                           DOC_QT_VOLUME
                    FROM DEV_OCORRENCIA_CONFERENTE
                    WHERE DOC_CO_NUMERO = {$Conferente->getCodOcorrencia()}
		";

//        echo $sql;
        $result = $this->conn->execSql($sql);
        while ($RConferente= oci_fetch_object($result)) {
            $Conferente= new Conferente();
            $Conferente->setCodOcorrencia($RConferente->DOC_CO_NUMERO);
            $Conferente->setEmpresa($RConferente->EMP_CO_NUMERO);
            $Conferente->setEstabelecimento($RConferente->ETB_CO_NUMERO);
            $Conferente->setCracha($RConferente->COL_CO_CRACHA);
            $Conferente->setVolume($RConferente->DOC_QT_VOLUME);


            $ObjConferente[] = $Conferente;
        }
        return $ObjConferente;
    }
    
    public function addConferenteOcorrencia(Conferente $Conferente) {
        $sql = "
            
                MERGE INTO DEV_OCORRENCIA_CONFERENTE DEV USING(
                        SELECT  
                            {$Conferente->getCodOcorrencia()}                  W_DOC_CO_NUMERO
                            ,{$Conferente->getEmpresa()}                        W_EMP_CO_NUMERO
                            ,{$Conferente->getEstabelecimento()}                W_ETB_CO_NUMERO
                            ,{$Conferente->getCracha()}                         W_COL_CO_CRACHA
                            ,{$Conferente->getVolume()}                         W_DOC_QT_VOLUME

                        FROM DUAL
                ) ON (          DEV.DOC_CO_NUMERO  = W_DOC_CO_NUMERO
                          AND   DEV.EMP_CO_NUMERO  = W_EMP_CO_NUMERO
                          AND   DEV.ETB_CO_NUMERO  = W_ETB_CO_NUMERO
                          AND   DEV.COL_CO_CRACHA  = W_COL_CO_CRACHA

                    )
                WHEN MATCHED THEN
                        UPDATE SET

                         DOC_QT_VOLUME    = W_DOC_QT_VOLUME
                WHEN NOT MATCHED THEN
                        INSERT

                        (
                          DOC_CO_NUMERO,
                          EMP_CO_NUMERO,
                          ETB_CO_NUMERO,
                          COL_CO_CRACHA,
                          DOC_QT_VOLUME
                        )
                        VALUES
                        (
                          W_DOC_CO_NUMERO,
                          W_EMP_CO_NUMERO,
                          W_ETB_CO_NUMERO,
                          W_COL_CO_CRACHA,
                          W_DOC_QT_VOLUME
                        )
                       ";
//                            echo $sql;

        if ($this->conn->execSql($sql)) {
            return true;
        }

        return false;
    }
    
    
    public function excluirConferenteOcorrencia($protocolo, $cracha){
        
        $sql = "
                 DELETE FROM DEV_OCORRENCIA_CONFERENTE
                 WHERE DOC_CO_NUMERO = {$protocolo}
                 AND   COL_CO_CRACHA = {$cracha}
               ";

        if ($this->conn->execSql($sql)) {
            return true;
        }

        return false;
    }
       
}














?>
