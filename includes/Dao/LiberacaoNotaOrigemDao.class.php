<?php 
Class LiberacaoNotaOrigemDao
{
	private $conn;

	function __construct(){
		$this->conn = new DaoSistema();
		$this->conn->conectar();
	}
	
	public function inserir(DevLiberacaoNotaOrigem $DevLiberacaoNotaOrigem){
		$sql = "INSERT INTO dev_liberacaonota
				  (
				    nto_nu_serie,
					nto_nu_nota,
					nto_dt_nota,
					cli_co_numero,				    
					lbn_tx_descricao,
					usu_co_numero,
					rpr_co_numero,
					lbn_in_multa,
					toc_co_numero
				  )
				  VALUES
				  (
				    '{$DevLiberacaoNotaOrigem->getDevNotaOrigem()->getSerie()}',
				    '{$DevLiberacaoNotaOrigem->getDevNotaOrigem()->getNota()}',
				    TO_DATE('{$DevLiberacaoNotaOrigem->getDevNotaOrigem()->getData()}','DD/MM/YYYY'),
				    {$DevLiberacaoNotaOrigem->getGlbCliente()->getClienteCodigo()},				    
				    '{$DevLiberacaoNotaOrigem->getDescricao()}',
				    {$DevLiberacaoNotaOrigem->getOperador()},
					{$DevLiberacaoNotaOrigem->getGlbVendedor()->getCodigo()},
					{$DevLiberacaoNotaOrigem->getMulta()},
					{$DevLiberacaoNotaOrigem->getObjTipoOcorrencia()->getCodTipoOcorrencia()} 
				  )"; 
//		echo $sql;
		if($this->conn->execSql($sql)){
			return true; 
		}				 
		return false;    
	}
	public function consultar(DevLiberacaoNotaOrigem $DevLiberacaoNotaOrigem){
		$sql = "SELECT 
				  lib.nto_nu_serie,
				  lib.nto_nu_nota, 
				  to_char(lib.nto_dt_nota,'DD/MM/YYYY')  nto_dt_nota, 
				  lib.lbn_tx_descricao,
				  to_char(lib.lbn_dt_liberacao,'DD/MM/YYYY') pln_dt_liberacao,
				  cli.cli_no_razao_social,
				  cli.cli_co_numero
				FROM dev_liberacaonota lib 
				INNER JOIN global.glb_cliente cli ON cli.cli_co_numero = lib.cli_co_numero 
				"; 
		if($result = $this->conn->execSql($sql)){
			return $result; 
		}				 
		return false;    
	}
	
	public function consultarPag(DevLiberacaoNotaOrigem $DevLiberacaoNotaOrigem,$pginicial = 0, $pgfinal = 20){
		$sql = "SELECT *
				  FROM ( SELECT /*+ FIRST_ROWS(n) */
				  topn.*, ROWNUM rnum
				      FROM (  SELECT 
				                    lib.nto_nu_serie,
				                    lib.nto_nu_nota, 
				                    to_char(lib.nto_dt_nota,'DD/MM/YYYY')  nto_dt_nota, 
				                    lib.lbn_tx_descricao,
				                    to_char(lib.lbn_dt_liberacao,'DD/MM/YYYY') pln_dt_liberacao,
				                    cli.cli_no_razao_social,
				                    cli.cli_co_numero
				                FROM dev_liberacaonota lib 
				                INNER JOIN GLOBAL.glb_cliente cli ON cli.cli_co_numero = lib.cli_co_numero
				                WHERE 1=1";
						if($DevLiberacaoNotaOrigem->getDevNotaOrigem()->getSerie()){ 
							$sql .=" AND lib.nto_nu_serie = '{$DevLiberacaoNotaOrigem->getDevNotaOrigem()->getSerie()}'"; 
		               	}
						if($DevLiberacaoNotaOrigem->getDevNotaOrigem()->getNota()){
		               		$sql .=" AND lib.nto_nu_nota = '{$DevLiberacaoNotaOrigem->getDevNotaOrigem()->getNota()}'";
						}
						if($DevLiberacaoNotaOrigem->getGlbCliente()->getClienteCodigo()){
							$sql .=" AND lib.cli_co_numero = {$DevLiberacaoNotaOrigem->getGlbCliente()->getClienteCodigo()}";
						} 
						$sql .=" AND lbn_in_status is null ";				             
			$sql .="ORDER BY pln_dt_liberacao DESC, lib.ROWID ) topn
				      WHERE ROWNUM <= $pgfinal )
				WHERE rnum  > $pginicial"; 
			echo $sql;
		if($result = $this->conn->execSql($sql)){
			return $result; 
		}				 
		return false;    
	}

	public function excluir(DevLiberacaoNotaOrigem $DevLiberacaoNotaOrigem){
		$sql = " UPDATE dev_liberacaonota SET lbn_in_status = 'E' WHERE 
					1=1  
				 	AND (
				 		  (	nto_nu_serie = '{$DevLiberacaoNotaOrigem->getDevNotaOrigem()->getSerie()}'
	  					  	AND nto_nu_nota = '{$DevLiberacaoNotaOrigem->getDevNotaOrigem()->getNota()}'
	  	        		  	AND cli_co_numero = '{$DevLiberacaoNotaOrigem->getGlbCliente()->getClienteCodigo()}')
	  	          		OR( cli_co_numero = '{$DevLiberacaoNotaOrigem->getGlbCliente()->getClienteCodigo()}'
	  	          			AND nto_nu_serie is null 
	  	          		    AND nto_nu_nota is null			  	          		
	  	          		   )			  	          
	  	          	  	)		
		";
		if($this->conn->execSql($sql)){
			return true; 
		}				 
		return false;   
	} 
	
	public function verificarNotaLiberada(DevLiberacaoNotaOrigem $DevLiberacaoNotaOrigem){
		//$sql = " update dev_liberacaonota set lbn_in_status = 'F' where sysdate  - lbn_dt_liberacao >= 15 and lbn_in_status is null "; 	
		//$this->conn->execSql($sql);  
		$sql = "SELECT 
				  	COUNT(1) total,decode(nto_nu_nota ,null,'cliente','nota') tipo
				FROM dev_liberacaonota 
				WHERE lbn_in_status IS NULL 
				";  
				if($DevLiberacaoNotaOrigem->getDevNotaOrigem()->getSerie() && $DevLiberacaoNotaOrigem->getDevNotaOrigem()->getNota() && $DevLiberacaoNotaOrigem->getGlbCliente()->getClienteCodigo()){		
					$sql .= " 
						 					
						AND (
								(nto_nu_serie = '{$DevLiberacaoNotaOrigem->getDevNotaOrigem()->getSerie()}'
				  					  AND nto_nu_nota = '{$DevLiberacaoNotaOrigem->getDevNotaOrigem()->getNota()}'
				  	        		  AND cli_co_numero = '{$DevLiberacaoNotaOrigem->getGlbCliente()->getClienteCodigo()}'
				  	        	)
				  	          	OR (  cli_co_numero = '{$DevLiberacaoNotaOrigem->getGlbCliente()->getClienteCodigo()}'
				  	          			  AND nto_nu_serie is null 
				  	          		      AND nto_nu_nota is null		
				  	          		)				  	          
				  	         )
				  	         
						";
				}
		$sql .=" GROUP BY decode(nto_nu_nota ,null,'cliente','nota') ";				
		//echo "[LiberacaoNotaOrigeDao:151] liberação ==> " . $sql; 	 			
		if($result = $this->conn->execSql($sql)){
			OCIFetchInto ($result, $row, OCI_ASSOC);			 
			$return['total'] =  $row['TOTAL'];
			$return['tipo'] =  $row['TIPO'];
			return $return;  
		}				 
		return false;
	}
        
        
        public function consultarLiberacaoNota() {
            
            
                        $sql = "    SELECT  LIB.NTO_NU_SERIE,
                                            LIB.NTO_NU_NOTA,
                                            TO_CHAR(LIB.NTO_DT_NOTA,'DD/MM/YYYY') NTO_DT_NOTA,
                                            LIB.LBN_TX_DESCRICAO,
                                            TO_CHAR(LIB.LBN_DT_LIBERACAO,'DD/MM/YYYY') LBN_DT_LIBERACAO,
                                            LIB.LBN_DT_LIBERACAO ODR_DT_LIBERACAO,
                                            CLI.CLI_NO_RAZAO_SOCIAL,
                                            CLI.CLI_CO_NUMERO,
											U.USU_NO_USERNAME,
											TOC.TOC_NO_DESCRICAO
                                    FROM DEV_LIBERACAONOTA LIB
                                    INNER JOIN GLOBAL.GLB_CLIENTE CLI ON CLI.CLI_CO_NUMERO = LIB.CLI_CO_NUMERO
                                    INNER JOIN GLOBAL.GLB_USUARIO U ON U.USU_CO_NUMERO = LIB.USU_CO_NUMERO
                                    LEFT JOIN CAL_TIPO_OCORRENCIA TOC ON TOC.TOC_CO_NUMERO = LIB.TOC_CO_NUMERO AND TOC.DEP_CO_NUMERO = 29
                                    WHERE  LBN_IN_STATUS   IS NULL
                                    ORDER BY ODR_DT_LIBERACAO DESC
									";

		if($result = $this->conn->execSql($sql)){
			return $result; 
		}				 
		return false;   

        }
        
        
        public function qtdLiberacaoNota(DevLiberacaoNotaOrigem $DevLiberacaoNotaOrigem) {
            
                        $sql = "               
                                                SELECT COUNT(*) AS QTD_NOTAS
                                                FROM dev_liberacaonota lib
                                                INNER JOIN GLOBAL.glb_cliente cli
                                                ON cli.cli_co_numero = lib.cli_co_numero
                                                WHERE 1              =1";
						if($DevLiberacaoNotaOrigem->getDevNotaOrigem()->getSerie()){ 
							$sql .=" AND lib.nto_nu_serie = '{$DevLiberacaoNotaOrigem->getDevNotaOrigem()->getSerie()}'"; 
		               	}
						if($DevLiberacaoNotaOrigem->getDevNotaOrigem()->getNota()){
		               		$sql .=" AND lib.nto_nu_nota = '{$DevLiberacaoNotaOrigem->getDevNotaOrigem()->getNota()}'";
						}
						if($DevLiberacaoNotaOrigem->getGlbCliente()->getClienteCodigo()){
							$sql .=" AND lib.cli_co_numero = {$DevLiberacaoNotaOrigem->getGlbCliente()->getClienteCodigo()}";
						} 
						$sql .=" AND lbn_in_status is null ";	
                                                
//			echo $sql;
		if($result=$this->conn->execSql($sql)){
			OCIFetchInto($result,$consulta,OCI_ASSOC);
			return $consulta["QTD_NOTAS"];
                }		 
		return false;   

        }
	
}
