<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of LogAcaoPassoDao
 *
 * @author geovanni.info
 */
//include_once("DevLogAcaoPasso.class.php"); 
class LogAcaoPassoDao {
    
    private $conn;

	function __construct()
	{
		$this->conn = new DaoSistema();
		$this->conn->conectar();
	}

        
	public function inserir(LogAcaoPasso $DevLogAcaoPasso)
	{
		$sql = "
                    INSERT
                        INTO DEV_LOG_ACAO_PASSO
                        (
                            DOC_CO_NUMERO,
                            LAP_NO_SOLICITANTE,
                            LAP_NO_DEPARTAMENTO,
                            LAP_CO_USUARIO,
                            LAP_IN_USUARIO,
                            LAP_IN_ACAO,
                            LAP_DT_ACAO,
                            LAP_IN_PASSO,
                            LAP_TX_DESCRICAO,
                            DPA_CO_NUMERO
                            
                ";
                
                if($DevLogAcaoPasso->getCodReprova()){
                        $sql .="    ,MRE_CO_NUMERO";
                }
                
                $sql .="
                        )
                        VALUES
                        (
                             {$DevLogAcaoPasso->getCodOcorrencia()},
                            '{$DevLogAcaoPasso->getNomeSolicitante()}',
                            '{$DevLogAcaoPasso->getNomeDepartamento()}',
                             {$DevLogAcaoPasso->getUsuario()},
                            '{$DevLogAcaoPasso->getTipoUsuario()}',
                            '{$DevLogAcaoPasso->getAcao()}',
                            SYSDATE,
                            '{$DevLogAcaoPasso->getPasso()}',
                            '{$DevLogAcaoPasso->getAcaoDescricao()}',
                             {$DevLogAcaoPasso->getPassoPortal()}
                ";
                            
                if($DevLogAcaoPasso->getCodReprova()){
                        $sql .=" ,{$DevLogAcaoPasso->getCodReprova()}";
                }         

                $sql .="
                        )		 
		";
                                        
//             echo "<pre>".$sql."</pre>"; // testes geovanni           
//	die();
                if($this->conn->execSql( $sql ))
                {
                    return true; 
		}
                return false;
                
                
	}
        
        
    public function consultaAcaoPasso(LogAcaoPasso $LogAcaoPasso) {
        
        $sql = "
                    SELECT LAP_NO_SOLICITANTE SOLICITANTE,
                           LAP_NO_DEPARTAMENTO DEPARTAMENTO,
                           DECODE(LAP_IN_USUARIO,
                                   'REP',(SELECT NOME FROM USERVIMED.USUARIO WHERE CDUSUARIO = LAP_CO_USUARIO),
                                   'POL',(SELECT USU_NO_COMPLETO FROM USUARIO WHERE  USU_CO_NUMERO = LAP_CO_USUARIO),      
                                   'GER',(SELECT NOME FROM USERVIMED.USUARIO WHERE CDUSUARIO = LAP_CO_USUARIO), 
                                   'DEV',(SELECT USU_NO_USERNAME FROM GLOBAL.GLB_USUARIO WHERE USU_CO_NUMERO = LAP_CO_USUARIO)
                                  ) USUARIO,
                            TO_CHAR(LAP_DT_ACAO, 'DD/MM/YYYY HH24:MI:SS') DATA_ACAO,
                            LAP_DT_ACAO DATACAO,
                            DECODE(LAP_IN_ACAO, 'A', 'REATIVADO', 'D', 'REPROVADO', 'R', 'REENTREGUE', 'P', 'PENDENTE' ) ACAO,
                            LAP_IN_PASSO PASSO,
                            LAP_TX_DESCRICAO DESCRICAO,
                            MRE.MRE_CO_NUMERO,
                            MRE.MRE_NO_DESCRICAO
                            FROM   DEV_LOG_ACAO_PASSO    LAP
                            LEFT JOIN DEV_MOTIVO_REPROVA MRE ON MRE.MRE_CO_NUMERO = LAP.MRE_CO_NUMERO
                            WHERE  DOC_CO_NUMERO = {$LogAcaoPasso->getCodOcorrencia()}
                            AND    ((LAP.LAP_IN_PASSO IN ('A', '{$LogAcaoPasso->getPasso()}')) OR (LAP.DPA_CO_NUMERO = {$LogAcaoPasso->getPassoPortal()} ))
                            ORDER BY DATACAO";
//        echo "<pre>".$sql."</pre>"; // testes geovanni 
        $result = $this->conn->execSql($sql);
        return $result; 
    }
    
    public function qtdAcaoPasso(LogAcaoPasso $DevLogAcaoPasso) {
        
        $sql = "
                    SELECT COUNT(*) QTDOCO
                            FROM   DEV_LOG_ACAO_PASSO
                            WHERE  DOC_CO_NUMERO = {$DevLogAcaoPasso->getCodOcorrencia()}
                            AND    ((LAP_IN_PASSO IN ('A', '{$DevLogAcaoPasso->getPasso()}')) OR (DPA_CO_NUMERO = {$DevLogAcaoPasso->getPassoPortal()} ))
               ";
//          echo "<pre>".$sql."</pre>";           
        $result = $this->conn->execSql($sql);
        OCIFetchInto($result, $row, OCI_ASSOC);
        return $row['QTDOCO'];
    }
        
}
