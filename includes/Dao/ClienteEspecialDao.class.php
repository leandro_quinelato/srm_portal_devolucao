<?php 
Class ClienteEspecialDao
{
	private $conn;

	function __construct() {
		$this->conn = new DaoSistema();
		$this->conn->conectar();
	}
	
	public function adicionar($codigoCliente, $codUsuario, $diasLiberacaoNF)
	{
		$sql = "INSERT INTO dev_cliente_especial
                        VALUES ({$codigoCliente},{$codUsuario}, SYSDATE, {$diasLiberacaoNF} )";

		if($this->conn->execSql($sql)){
			return true; 
		}				 

		return false;
	}

	public function excluir($codigoCliente)
	{
		$sql = "DELETE FROM dev_cliente_especial
				WHERE esp_co_numero = '".$codigoCliente."'";
				
		if($this->conn->execSql($sql)){
			return true;
		}				 

		return false;
	}

	public function adicionarPorRede($codigoRede, $codUsuario, $diasLiberacaoNF)
	{
		$sql = "INSERT INTO dev_cliente_especial
               (SELECT CLI_CO_NUMERO, {$codUsuario}, SYSDATE, {$diasLiberacaoNF} FROM GLOBAL.GLB_CLIENTE where red_co_numero = {$codigoRede})";

		if($this->conn->execSql($sql)){
			return true; 
		}				 

		return false;
	}		
	
	public function excluirPorRede($codigoRede)
	{
		$sql = "DELETE FROM dev_cliente_especial WHERE ESP_CO_NUMERO IN (SELECT CLI_CO_NUMERO FROM GLOBAL.GLB_CLIENTE where red_co_numero = {$codigoRede})";
				
		if($this->conn->execSql($sql)){
			return true;
		}				 

		return false;
	}	
	
	public function consultarClienteEspecial() {

		$sql = "SELECT *
                        FROM GLOBAL.GLB_CLIENTE C
                        INNER JOIN DEV_CLIENTE_ESPECIAL ESP
                        ON C.CLI_CO_NUMERO = ESP.ESP_CO_NUMERO
						LEFT JOIN GLOBAL.GLB_REDE_FARMACIA R
						ON R.RED_CO_NUMERO = C.RED_CO_NUMERO	
						INNER JOIN GLOBAL.GLB_USUARIO U 
						ON U.USU_CO_NUMERO = ESP.USU_CO_NUMERO						
                        WHERE C.CLI_IN_STATUS IS NULL
                        ORDER BY C.CLI_CO_NUMERO ASC
                       "; 

		if($result = $this->conn->execSql($sql)){
			return $result; 
		}				 

		return false;
	}


	public function verificaClienteEspecial($codigoCliente) {

		$sql = "SELECT esp_co_numero 
					FROM dev_cliente_especial
					WHERE esp_co_numero = ".$codigoCliente;

		if($result = $this->conn->execSql($sql)){
			OCIFetchInto ($result, $linha, OCI_ASSOC);
			if(!is_null($linha['ESP_CO_NUMERO']))
				return true;
		}				 
		return false;
	}
	
	public function consultarDiasClienteEspecial($codigoCliente) {

		$sql = "SELECT ESP_NU_DIAS_LIBERACAONF
					FROM dev_cliente_especial
					WHERE esp_co_numero = ".$codigoCliente;

		if($result = $this->conn->execSql($sql)){
			OCIFetchInto ($result, $linha, OCI_ASSOC);
			if(is_null($linha['ESP_NU_DIAS_LIBERACAONF']))
				return 0;
		}				 
		return $linha['ESP_NU_DIAS_LIBERACAONF'];
	}	
	
}