<?php
//include_once("../Romaneio.class.php"); 

class RomaneioDao {
    
    private $conn;

    function __construct() {

        $this->conn = new DaoSistema();
        $this->conn->conectar();
    }
    
    public function gerarCodigoOcorrencia() {
        try {
            $sql = "SELECT NVL( MAX( dro_co_numero ), 0) + 1 AS DRO_CO_NUMERO
                    FROM dev_romaneio
            ";

            $result = $this->conn->execSql($sql);
            OCIFetchInto($result, $row, OCI_ASSOC);

            return $row['DRO_CO_NUMERO'];
        } catch (Exception $e) {
            print( "[ ERRO NO METODO gerarCodigoOcorrencia ]: " . $e->getMessage());
            return null;
        }
    }
	
    function inserir(Romaneio $Romaneio) {
        $sql = "
            INSERT INTO dev_romaneio
            (
                dro_co_numero,
                dro_dt_abertura,             
                usu_co_abertura
            )
            VALUES
            (	
                {$Romaneio->getCodigo()}
                ,SYSDATE 
                ,{$Romaneio->getUsuarioAbertura()}
            )";
//          echo $sql; 
        if ($this->conn->execSql($sql)) {
            return true;
        }
        return false;
    }
	
    public function consultar(Romaneio $Romaneio) {
        $sql = "
		    SELECT *
				FROM DEV_ROMANEIO
				WHERE 1=1
        ";
        
        if($Romaneio->getCodigo()){
            $sql .=" AND DRO_CO_NUMERO = {$Romaneio->getCodigo()}";
        }
        
        if($Romaneio->getUsuarioAbertura()){
            $sql .=" AND USU_CO_NUMERO = {$Romaneio->getUsuarioAbertura()}";
        }

        $sql .=" ORDER BY DRO_CO_NUMERO";

        $result = $this->conn->execSql($sql);
        while ($resRomaneio = oci_fetch_object($result)) {
            $Romaneio = new Romaneio();
            $Romaneio->setCodigo($resRomaneio->DRO_CO_NUMERO);
            $Romaneio->setDataAbertura($resRomaneio->DRO_DT_ABERTURA);
			$Romaneio->setUsuarioAbertura($resRomaneio->USU_CO_ABERTURA);
			$Romaneio->setDataBaixa($resRomaneio->DRO_DT_BAIXA);
			$Romaneio->setUsuarioBaixa($resRomaneio->USU_CO_BAIXA);
			
            $ObjRomaneio[] = $Romaneio;
        }
        return $ObjRomaneio;
    }
	
    public function baixarRomaneio($codRomaneio, $codUsuario) {
        
        $sql = "UPDATE DEV_ROMANEIO SET 
					DRO_DT_BAIXA = SYSDATE,
					USU_CO_BAIXA = {$codUsuario}
                WHERE DRO_CO_NUMERO     = {$codRomaneio}   
               ";
                
		//echo $sql;//die();
        if ($this->conn->execSql($sql)) {
            return true;
        }
        return false;
   
    }	
    
    
}