<?php

//include_once("../TipoArquivoDevolucao.class.php");

/**
 * Description of TipoArquivoDevolucaoDao
 *
 * @author geovanni.info
 */
class TipoArquivoDevolucaoDao {

    private $conn;

    function __construct() {

        $this->conn = new DaoSistema();
        $this->conn->conectar();
    }

    public function consultar() {
        $sql = "
		    SELECT DTA_CO_CODIGO,
                           DTA_NO_DESCRICAO,
                           DTA_NO_PATH
                    FROM DEV_TIPOARQUIVO
                    ORDER BY DTA_CO_CODIGO		
		";

        $result = $this->conn->execSql($sql);
        while ($RsArquivo = oci_fetch_object($result)) {
            $tipoArquivo = new TipoArquivoDevolucao();

            $tipoArquivo->setCodigoTipoArquivo($RsArquivo->DTA_CO_CODIGO);
            $tipoArquivo->setDescricaoTipoArquivo($RsArquivo->DTA_NO_DESCRICAO);
            $tipoArquivo->setDiretorioArquivo($RsArquivo->DTA_NO_PATH);

            $ObjArquivo[] = $tipoArquivo;
        }
        return $ObjArquivo;
    }

    public function consultarTipoArquivoAnexo() {
        $sql = "
		    SELECT DTA_CO_CODIGO,
                    DTA_NO_DESCRICAO,
                    DTA_NO_PATH
                FROM DEV_TIPOARQUIVO
                WHERE DTA_NO_DESCRICAO LIKE 'NOTA DEVOLU%'
                ORDER BY DTA_CO_CODIGO		
		";

        $result = $this->conn->execSql($sql);
        while ($RsArquivo = oci_fetch_object($result)) {
            $tipoArquivo = new TipoArquivoDevolucao();

            $tipoArquivo->setCodigoTipoArquivo($RsArquivo->DTA_CO_CODIGO);
            $tipoArquivo->setDescricaoTipoArquivo($RsArquivo->DTA_NO_DESCRICAO);
            $tipoArquivo->setDiretorioArquivo($RsArquivo->DTA_NO_PATH);

            $ObjArquivo[] = $tipoArquivo;
        }
        return $ObjArquivo;
    }

    public function consultarArquivo(TipoArquivoDevolucao $TipoArquivoDevolucao) {
        $sql = "
		    SELECT DTA_CO_CODIGO,
                           DTA_NO_DESCRICAO,
                           DTA_NO_PATH
                    FROM DEV_TIPOARQUIVO		
            ";

        if ($TipoArquivoDevolucao->getCodigoTipoArquivo()) {
            $sql .= " WHERE DTA_CO_CODIGO = {$TipoArquivoDevolucao->getCodigoTipoArquivo()}";
        }


        $sql .= " 
                    ORDER BY DTA_CO_CODIGO		
		";

        $result = $this->conn->execSql($sql);
        $RsArquivo = oci_fetch_object($result);
        $ObjArquivo = new TipoArquivoDevolucao();

        $ObjArquivo->setCodigoTipoArquivo($RsArquivo->DTA_CO_CODIGO);
        $ObjArquivo->setDescricaoTipoArquivo($RsArquivo->DTA_NO_DESCRICAO);
        $ObjArquivo->setDiretorioArquivo($RsArquivo->DTA_NO_PATH);
        
        return $ObjArquivo;
    }

}
