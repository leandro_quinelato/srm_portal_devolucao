<?php

/**
 * Description of UsuarioTransporteDao
 *
 * @author geovanni.info
 */
class UsuarioTransporteDao {

    private $conn;

    function __construct() {

        $this->conn = new DaoSistema();
        $this->conn->conectar();
    }

    public function usuarioTransportador() {
        $codPermissao = 81;
        $sistemaDevol = 29;

        $sql = "
                    SELECT  USU.USU_CO_NUMERO
                           ,PES.PER_CO_NUMERO
                           ,PES.SIS_CO_NUMERO
                           ,PES.PER_NO_PERFIL
                           ,USU.USU_IN_TIPO
                           ,USU.USU_NO_USERNAME
                           ,USU.USU_IN_STATUS
                    FROM        GLOBAL.GLB_PERFIL_USUARIO PEU
                    INNER JOIN  GLOBAL.GLB_PERFIL_SISTEMA PES ON PES.PER_CO_NUMERO = PEU.PER_CO_NUMERO AND PES.SIS_CO_NUMERO = {$sistemaDevol}
                    INNER JOIN  GLOBAL.GLB_USUARIO        USU ON USU.USU_CO_NUMERO = PEU.USU_CO_NUMERO
                    WHERE PEU.PER_CO_NUMERO = {$codPermissao}
        ";
//        echo $sql; 
        return $this->conn->execSql($sql);
    }

    /* Consultar usuario rota  */

    public function consultaUsuRota($usu_co_numero, $rot_co_numero = NULL) {
        $sql = " SELECT *
                 FROM USUARIO_ROTA usr
		 INNER JOIN global.glb_rota    rot ON ROT.ROT_CO_NUMERO = USR.ROT_CO_NUMERO  
                 INNER JOIN global.GLB_USUARIO USU ON USU.USU_CO_NUMERO = USR.USU_CO_NUMERO             
                 WHERE 1=1      
				";
        if (!is_null($usu_co_numero)) {
            $sql .= " AND usr.USU_CO_NUMERO = {$usu_co_numero}";
        }
        if ($rot_co_numero) {
            $sql .= " AND  rot.ROT_CO_NUMERO = '{$rot_co_numero}'";
        }

        $result = $this->conn->execSql($sql);
        return $result;
    }

    public function consultaRotasCadastro($usu_co_numero) {

        $sql = " SELECT * 
                 FROM   GLOBAL.GLB_ROTA
                 WHERE ROT_CO_NUMERO NOT IN ( SELECT ROT_CO_NUMERO FROM USUARIO_ROTA WHERE USU_CO_NUMERO = {$usu_co_numero} )
                 ORDER BY ROT_CO_NUMERO
		";

        $result = $this->conn->execSql($sql);
        return $result;
    }

    public function inserirUsuRota($usu_co_numero, $rot_co_numero) {
        $sql = "INSERT INTO USUARIO_ROTA(
					USU_CO_NUMERO
					,ROT_CO_NUMERO					
					)
				VALUES(
					{$usu_co_numero}, 
					'{$rot_co_numero}'										
				)";
        //echo $sql; 		 			
        if ($this->conn->execSql($sql)) {
            return true;
        }
        return false;
    }

    public function deletUsuarioRota($usu_co_numero, $rot_co_numero) {


        $sql = " DELETE FROM USUARIO_ROTA 
					WHERE 
						USU_CO_NUMERO = {$usu_co_numero} 
						AND ROT_CO_NUMERO = '{$rot_co_numero}'";
        //echo $sql;
        if ($result = $this->conn->execSql($sql)) {
            return true;
        }
        return false;
    }

}
