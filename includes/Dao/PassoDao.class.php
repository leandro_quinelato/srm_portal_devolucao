<?php
//include_once("../Passo.class.php"); 

/**
 * Description of StatusDao
 *
 * @author geovanni.info
 */
class PassoDao {
    
    private $conn;

    function __construct() {

        $this->conn = new DaoSistema();
        $this->conn->conectar();
    }
    
    public function consultar(Passo $Passo ) {
        $sql = "
		    SELECT *
                    FROM DEV_PASSO
                    WHERE 1=1
        ";
        
        if($Passo->getCodigo()){
            $sql .=" AND DPA_CO_NUMERO = {$Passo->getCodigo()}";
        }
        
        if($Passo->getSeqFluxo()){
            $sql .=" AND DPA_NU_SEQFLUXO = {$Passo->getSeqFluxo()}";
        }

        $sql .="
                    ORDER BY DPA_CO_NUMERO 		
		";

        $result = $this->conn->execSql($sql);
        while ($Rpasso = oci_fetch_object($result)) {
            $Passo = new Passo();
            $Passo->setCodigo($Rpasso->DPA_CO_NUMERO);
            $Passo->setDescricao($Rpasso->DPA_NO_DESCRICAO);
            $Passo->setDataCriacao($Rpasso->DPA_DT_CRIACAO);
            $Passo->setStatus($Rpasso->DPA_IN_STATUS);
            $Passo->setSeqFluxo($Rpasso->DPA_NU_SEQFLUXO);
            $Passo->setPaginaFluxo($Rpasso->DPA_NO_URL);


            $ObjPasso[] = $Passo;
        }
        return $ObjPasso;
    }
    
    public function consultarFluxo() {
        $sql = "
		    SELECT *
                    FROM DEV_PASSO
                    WHERE DPA_IN_STATUS IS NULL
                    ORDER BY DPA_NU_SEQFLUXO 		
		";

        $result = $this->conn->execSql($sql);
        while ($Rpasso = oci_fetch_object($result)) {
            $Passo = new Passo();
            $Passo->setCodigo($Rpasso->DPA_CO_NUMERO);
            $Passo->setDescricao($Rpasso->DPA_NO_DESCRICAO);
            $Passo->setDataCriacao($Rpasso->DPA_DT_CRIACAO);
            $Passo->setStatus($Rpasso->DPA_IN_STATUS);
            $Passo->setSeqFluxo($Rpasso->DPA_NU_SEQFLUXO);
            $Passo->setPaginaFluxo($Rpasso->DPA_NO_URL);


            $ObjPasso[] = $Passo;
        }
        return $ObjPasso;
    }
    
    
}
//DPA_CO_NUMERO
//DPA_NO_DESCRICAO
//DPA_DT_CRIACAO
//DPA_IN_STATUS