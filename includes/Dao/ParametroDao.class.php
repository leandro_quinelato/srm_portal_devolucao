<?php

/*
 * @author: Anderson Felix;
 * @date: 25-03-2010 
 * @version: 0.0.0.1
 */

//include_once("../DevParametros.class.php");

Class ParametroDao {

    private $conn;

    function __construct() {
        $this->conn = new DaoSistema();
        $this->conn->conectar();
    }

    public function alterar(devParametros $ObjParametro) {
        $sql = "UPDATE DEV_PARAMETROS SET 
                                 PAR_NU_DIAS_CADALIMENTAR  = {$ObjParametro->getNumDiasCadalimentar()}
                                ,PAR_NU_DIAS_CADFARMADH    = {$ObjParametro->getNumDiasCadFarmaDH()}
                                ,PAR_NU_DIAS_APROVAGERENTE = {$ObjParametro->getNumDiasAprovaGerente()}
                                ,PAR_NU_DIAS_LIBERACAONF   = {$ObjParametro->getNumDiasLiberacaoNF()}    
								,PAR_NO_EMAIL_REENTREGA    = '{$ObjParametro->getEmailReentrega()}'
		";
        //echo $sql;
        if ($this->conn->execSql($sql)) {
            return true;
        }
        return false;
    }

    public function consultar() {
        $sql = "
			SELECT * FROM dev_parametros 		
		";

        $result = $this->conn->execSql($sql);
        $parametro = oci_fetch_object($result);
        $Parametro = new devParametros();
        $Parametro->setNumeroDiasPasso($parametro->PAR_NU_DIASPASSO);
        $Parametro->setNumeroVoltaPasso($parametro->PAR_NU_VOLTAPASSO);
        $Parametro->setNumValorAprovacaoAut($parametro->PAR_NU_VALOR_APROVACAO_AUT);
        $Parametro->setNumDiasCadalimentar($parametro->PAR_NU_DIAS_CADALIMENTAR);
        $Parametro->setNumDiasCadFarmaDH($parametro->PAR_NU_DIAS_CADFARMADH);
        $Parametro->setNumDiasAprovaGerente($parametro->PAR_NU_DIAS_APROVAGERENTE);
        $Parametro->setNumDiasLiberacaoNF($parametro->PAR_NU_DIAS_LIBERACAONF);
		$Parametro->setEmailReentrega($parametro->PAR_NO_EMAIL_REENTREGA);

        if($parametro->PAR_NO_EMAIL_REENTREGA != null){
            $arr = explode(';', $parametro->PAR_NO_EMAIL_REENTREGA);

            $arrN = array();
            $count = 1;
            foreach($arr as $item){
                
                $arrN[$count] = $item;
                $count = $count + 1;
            }
            $Parametro->setListaEmailReentrega($arrN);
        }

//        $ObjParametro[] = $Parametro;

        return $Parametro;
    }

}
