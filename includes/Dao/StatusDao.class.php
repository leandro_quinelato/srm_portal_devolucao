<?php
//include_once("../Status.class.php"); 

/**
 * Description of StatusDao
 *
 * @author geovanni.info
 */
class StatusDao {
    
    private $conn;

    function __construct() {

        $this->conn = new DaoSistema();
        $this->conn->conectar();
    }
    
    public function consultar() {
        $sql = "
		    SELECT * 
                    FROM DEV_STATUS
                    ORDER BY SEQ_FLUXO 		
		";

        $result = $this->conn->execSql($sql);
        while ($Rstatus = oci_fetch_object($result)) {
            $Status = new Status();
            $Status->setCodigo($Rstatus->DST_CO_NUMERO);
            $Status->setDescricao($Rstatus->DST_NO_DESCRICAO);
            $Status->setDataCriacao($Rstatus->DST_DT_CRIACAO);
            $Status->setStatus($Rstatus->DST_IN_STATUS);


            $ObjStatus[] = $Status;
        }
        return $ObjStatus;
    }
    
    public function consultarStatus( Status $Status) {
        $sql = "
		    SELECT * 
                    FROM DEV_STATUS";
        
        if ($Status->getCodigo()) {
            $sql .=" WHERE DST_CO_NUMERO = {$Status->getCodigo()}";
        }

        $sql .=  "           
                    ORDER BY DST_CO_NUMERO 		
		";

        $result = $this->conn->execSql($sql);
        while ($Rstatus = oci_fetch_object($result)) {
            $Status = new Status();
            $Status->setCodigo($Rstatus->DST_CO_NUMERO);
            $Status->setDescricao($Rstatus->DST_NO_DESCRICAO);
            $Status->setDataCriacao($Rstatus->DST_DT_CRIACAO);
            $Status->setStatus($Rstatus->DST_IN_STATUS);

            $ObjStatus[] = $Status;
        }
        return $ObjStatus;
    }
    
    
}
//DST_CO_NUMERO
//DST_NO_DESCRICAO
//DST_DT_CRIACAO
//DST_IN_STATUS