<?php  

//include_once("../Multa.class.php"); 
Class MultaDao
{
	private $conn;

	function __construct()
	{
		$this->conn = new DaoSistema();
		$this->conn->conectar();
	}
	
	public function inserir(devMulta $ObjMulta)
	{
		$sql = "INSERT INTO dev_multa
				(mul_co_numero
				,mul_vr_multa
				,mul_qt_nota
				,nto_in_origem_responsavel
				,mul_dt_inicio
				,mul_dt_fim
				)
				VALUES
				(
					(SELECT NVL(MAX(mul_co_numero),0)+1 FROM dev_multa )
					,{$ObjMulta->getValorMulta()}
					,{$ObjMulta->getQuantidadeMulta()}
					,'{$ObjMulta->getOrigemNotaMulta()}'
					,to_date('{$ObjMulta->getInicioMulta()}','DD/MM/YYYY')
					,to_date('{$ObjMulta->getFimMulta()}','DD/MM/YYYY')
				)
				 
		";		 
		if($this->conn->execSql( $sql ))
		{
			return true; 
		}
		return false;
	
	}
	public function alterar(Multa $ObjMulta)
	{
		$sql = "UPDATE dev_multa SET 
					mul_vr_multa = {$ObjMulta->getValorMulta()}
					,mul_qt_nota = {$ObjMulta->getQuantidadeMulta()}
					,nto_in_origem_responsavel = '{$ObjMulta->getOrigemNotaMulta()}'
                ";
                                        
                if  ($ObjMulta->getInicioMulta()){
                    $sql .= ",mul_dt_inicio = to_date('{$ObjMulta->getInicioMulta()}','DD/MM/YYYY')";
                }
                
                if  ($ObjMulta->getFimMulta()){
                    $sql .= ",mul_dt_fim = to_date('{$ObjMulta->getFimMulta()}','DD/MM/YYYY')";
                }
					
                $sql .= " ,mul_in_status = {$ObjMulta->getStatusMulta()} ";	
                
//                echo $sql;die();
		if($this->conn->execSql( $sql ))
		{
			return true; 
		}
		return false;	
	}
	
	public function excluir(devMulta $ObjMulta){}
	
	public function consultar(Multa $ObjMulta)
	{		
		$sql = "SELECT mul_co_numero
					,mul_vr_multa
					,mul_qt_nota
					,
					DECODE(nto_in_origem_responsavel
							,'CLIE','CLIENTE'
							,'REPR','REPRESENTATE'
							,'DIRT','DIRETORIA'
							,'ESTQ','ESTOQUE'
							,'INDU','INDUSTRIA'
							,'TRAN','TRANSPORTADORA'
							,(SELECT NOME 
	              				FROM uservimed.tp_origemvenda 
	              				WHERE TPORIGVENDA = nto_in_origem_responsavel
	              			 )          
	        		) AS nto_in_origem_responsavel
	        		, nto_in_origem_responsavel as TPORIGVENDA 
					,to_char(mul_dt_inicio,'DD/MM/YYYY') AS mul_dt_inicio
					,to_char(mul_dt_fim ,'DD/MM/YYYY') AS mul_dt_fim
					,mul_in_status
					FROM dev_multa
				WHERE 1=1 
				";
		if($ObjMulta->getCodigoMulta())
		{
			$sql .= " AND mul_co_numero = {$ObjMulta->getCodigoMulta()} ";
		}		
		if($ObjMulta->getOrigemNotaMulta())
		{
			$sql .= " AND nto_in_origem_responsavel = '{$ObjMulta->getOrigemNotaMulta()}'";
		}		
		if($ObjMulta->getInicioMulta() && $ObjMulta->getFimMulta())
		{
			$sql .="
					AND MUL_DT_INICIO >= to_date('{$ObjMulta->getInicioMulta()}','DD/MM/YYYY')
    				AND MUL_DT_FIM <= to_date('{$ObjMulta->getFimMulta()}','DD/MM/YYYY')
					";
		}		
		  //echo $sql; 
		if($result=$this->conn->execSql( $sql ))
		{
			return $result; 
		}
		return false;
	}
	
	public function listar(){}
	
	public function consultaValida(devMulta $ObjMulta)
	{
		$sql = "SELECT count(*) TOTAL FROM dev_multa
				WHERE 
				nto_in_origem_responsavel = '{$ObjMulta->getOrigemNotaMulta()}'
				AND MUL_DT_INICIO >= to_date('{$ObjMulta->getInicioMulta()}','DD/MM/YYYY')
    			AND MUL_DT_FIM <= to_date('{$ObjMulta->getFimMulta()}','DD/MM/YYYY')
    			AND mul_in_status = 1  
				"; 
		if($result=$this->conn->execSql( $sql ))
		{
			return $result; 
		}
		return false;
	}
	
	public function buscaMultaAtribui(devMulta $ObjMulta)
	{
		$sql = "SELECT mul_co_numero
					  ,mul_vr_multa 
					  ,mul_qt_nota
					FROM dev_multa
				WHERE mul_in_status = 1					 
				";
		if($ObjMulta->getInicioMulta() )
		{
			$sql .=" AND to_date('{$ObjMulta->getInicioMulta()}','DD/MM/YYYY') 
							BETWEEN MUL_DT_INICIO AND MUL_DT_FIM ";
		}
		if($ObjMulta->getOrigemNotaMulta())
		{
			$sql .=" AND nto_in_origem_responsavel = '{$ObjMulta->getOrigemNotaMulta()}' "; 
		}
		if(is_null($ObjMulta->getOrigemNotaMulta()))
		{
			$sql .=" AND nto_in_origem_responsavel IS NULL "; 
		}
		
		if($result=$this->conn->execSql( $sql ))
		{
			return $result; 
		}
		return false;
	}
}
?>