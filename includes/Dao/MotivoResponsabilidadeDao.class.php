<?php 
Class MotivoResponsabilidadeDao {
	private $conn;

	function __construct(){
		$this->conn = new DaoSistema();
		$this->conn->conectar();
	}
	function inserir(DevMotivoResponsabilidade $DevMotivoResponsabilidade){
		$sql = "INSERT INTO
			  dev_tipo_responsabilidade
			  (
			    trp_co_numero,			    
                            trp_no_descricao,
                            trp_no_responsavel
			  )
			  VALUES
			  (
			    (SELECT NVL(MAX(trp_co_numero),0)+1 FROM dev_tipo_responsabilidade ),
                            '{$DevMotivoResponsabilidade->getDescricao()}',
                            '{$DevMotivoResponsabilidade->getResponsavel()}'    
			  )
			";
		if($result = $this->conn->execSql($sql)){
			return true; 
		}
		return false;
	}
	
	function consultar(DevMotivoResponsabilidade $DevMotivoResponsabilidade){
		$sql = "SELECT * FROM dev_tipo_responsabilidade 
				WHERE 1=1
		";
		if($DevMotivoResponsabilidade->getCodigo()){
			$sql.= " AND trp_co_numero = {$DevMotivoResponsabilidade->getCodigo()}"; 
		}
		if($DevMotivoResponsabilidade->getDescricao()){
			$sql.= " AND trp_no_descricao like '%{$DevMotivoResponsabilidade->getDescricao()}%'"; 
		}
                if($DevMotivoResponsabilidade->getStatus()){
			$sql.= " AND trp_in_status is null"; 
		}
		$sql .= " ORDER BY trp_no_descricao ";
		//echo $sql;
                if($result=$this->conn->execSql( $sql )){
			return $result; 
		}
		return false; 
		
	}
	function alterar(DevMotivoResponsabilidade $DevMotivoResponsabilidade){
		$sql = "UPDATE dev_tipo_responsabilidade SET 
					trp_no_descricao   = '{$DevMotivoResponsabilidade->getDescricao()}',
					trp_in_status      = '{$DevMotivoResponsabilidade->getStatus()}',
                                        trp_no_responsavel = '{$DevMotivoResponsabilidade->getResponsavel()}'
				WHERE trp_co_numero = {$DevMotivoResponsabilidade->getCodigo()}			
		";
		if($result = $this->conn->execSql($sql)){
			return true; 
		}
		return false;
	}
}