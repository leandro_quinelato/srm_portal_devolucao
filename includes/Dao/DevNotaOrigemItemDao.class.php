<?php

//include_once("DevNotaOrigemItem.class.php");

/**
 * Description of DevNotaOrigemItemDao
 *
 * @author geovanni.info
 */
class DevNotaOrigemItemDao {

    private $conn;
    private $OracleError;

    function __construct() {
        $this->conn = new DaoSistema();
        $this->OracleError = new OracleError();
        $this->conn->conectar();
    }

    public function inserir(DevNotaOrigemItem $DevNotaOrigemItem) {

        $sql = "
                MERGE INTO DEV_NOTAORIGEM_ITEM NTI USING(
                        SELECT  
                             {$DevNotaOrigemItem->getCodigoOcorrencia()}       W_DOC_CO_NUMERO
                            ,{$DevNotaOrigemItem->getSerie()}                  W_NTI_NU_SERIE
                            ,{$DevNotaOrigemItem->getNumNota()}                W_NTI_NU_NOTA
                            ,TO_DATE('{$DevNotaOrigemItem->getDataNota()}','DD/MM/YYYY')             W_NTI_DT_NOTA
                            ,{$DevNotaOrigemItem->getCodCliente()}             W_CLI_CO_NUMERO
                            ,{$DevNotaOrigemItem->getCodProduto()}             W_PRO_CO_NUMERO
                            ,{$DevNotaOrigemItem->getDevQuantidade()}          W_NTI_NU_DEVQTD
                            ,{$DevNotaOrigemItem->getPrecoUnitario()}          W_NTI_VL_PRECOUNIT
                            ,{$DevNotaOrigemItem->getTotalBruto()}             W_NTI_VL_TOTBRUTO
                            ,{$DevNotaOrigemItem->getTotalDesconto()}          W_NTI_VL_TOTDESC
                            ,{$DevNotaOrigemItem->getTotalImposto()}           W_NTI_VL_TOTIMP
                            ,{$DevNotaOrigemItem->getTotalLiquido()}           W_NTI_VL_TOTLIQ
                            ,{$DevNotaOrigemItem->getNotaQuantidade()}         W_NTI_NU_QTDNOTA

                        FROM DUAL
                ) ON (          NTI.DOC_CO_NUMERO = W_DOC_CO_NUMERO
                          AND   NTI.NTI_NU_SERIE  = W_NTI_NU_SERIE
                          AND   NTI.NTI_NU_NOTA   = W_NTI_NU_NOTA
                          AND   NTI.CLI_CO_NUMERO = W_CLI_CO_NUMERO
                          AND   NTI.PRO_CO_NUMERO = W_PRO_CO_NUMERO

                    )
                WHEN MATCHED THEN
                        UPDATE SET
                            NTI.NTI_NU_DEVQTD = W_NTI_NU_DEVQTD ,
                            NTI.NTI_VL_PRECOUNIT = W_NTI_VL_PRECOUNIT ,
                            NTI.NTI_VL_TOTBRUTO = W_NTI_VL_TOTBRUTO ,
                            NTI.NTI_VL_TOTDESC  = W_NTI_VL_TOTDESC,
                            NTI.NTI_VL_TOTIMP  =  W_NTI_VL_TOTIMP,
                            NTI.NTI_VL_TOTLIQ  =  W_NTI_VL_TOTLIQ,
                            NTI.NTI_NU_QTDNOTA =  W_NTI_NU_QTDNOTA
                WHEN NOT MATCHED THEN
                        INSERT (
                        
                             NTI.DOC_CO_NUMERO
                            ,NTI.NTI_NU_SERIE
                            ,NTI.NTI_NU_NOTA
                            ,NTI.NTI_DT_NOTA
                            ,NTI.CLI_CO_NUMERO
                            ,NTI.PRO_CO_NUMERO
                            ,NTI.NTI_NU_DEVQTD
                            ,NTI.NTI_VL_PRECOUNIT
                            ,NTI.NTI_VL_TOTBRUTO
                            ,NTI.NTI_VL_TOTDESC
                            ,NTI.NTI_VL_TOTIMP
                            ,NTI.NTI_VL_TOTLIQ
                            ,NTI_NU_QTDNOTA

                        )  VALUES (
                             W_DOC_CO_NUMERO
                            ,W_NTI_NU_SERIE
                            ,W_NTI_NU_NOTA
                            ,W_NTI_DT_NOTA
                            ,W_CLI_CO_NUMERO
                            ,W_PRO_CO_NUMERO
                            ,W_NTI_NU_DEVQTD
                            ,W_NTI_VL_PRECOUNIT
                            ,W_NTI_VL_TOTBRUTO
                            ,W_NTI_VL_TOTDESC
                            ,W_NTI_VL_TOTIMP
                            ,W_NTI_VL_TOTLIQ
                            ,W_NTI_NU_QTDNOTA
                        ) 
                    ";

//        echo "<pre>".print_r($sql)."</pre>"  ;//die();
        //return $this->conn->execSql($sql);
        //echo $sql;
        if ($this->conn->execSql($sql)) {
            return true;
        }
        $this->OracleError->getMensagem($this->conn->getError());
        return false;
    }

    public function consultar() {
        $sql = "
			SELECT count(*) TESTE FROM DEV_TIPO_DEVOLUCAO 		
		";

        $result = $this->conn->execSql($sql);
        $parametro = oci_fetch_object($result);
        return $parametro->TESTE;
    }

    public function consultarItemNotasDevolucao(DevNotaOrigemItem $DevNotaOrigemItem) {

        $sql = "
                SELECT 
                     NTI.DOC_CO_NUMERO
                    ,NTI.NTI_NU_SERIE
                    ,NTI.NTI_NU_NOTA
                    ,NTI.NTI_DT_NOTA
                    ,NTI.CLI_CO_NUMERO
                    ,NTI.PRO_CO_NUMERO
                    ,NTI.NTI_NU_DEVQTD
                    ,NTI.NTI_VL_PRECOUNIT
                    ,NTI.NTI_VL_TOTBRUTO
                    ,NTI.NTI_VL_TOTDESC
                    ,NTI.NTI_VL_TOTIMP
                    ,NTI.NTI_VL_TOTLIQ
                    ,NTI.NTI_NU_QTDNOTA
                    ,PRO.PRO_NO_DESCRICAO
                FROM DEV_NOTAORIGEM_ITEM NTI
                INNER JOIN GLOBAL.GLB_PRODUTO PRO ON PRO.PRO_CO_NUMERO = NTI.PRO_CO_NUMERO
                WHERE NTI.DOC_CO_NUMERO = {$DevNotaOrigemItem->getCodigoOcorrencia()}";


        if ($DevNotaOrigemItem->getSerie()) {
            $sql .= "
                AND   NTI.NTI_NU_SERIE  = {$DevNotaOrigemItem->getSerie()}
            ";
        }

        if ($DevNotaOrigemItem->getNumNota()) {
            $sql .= "
                AND   NTI.NTI_NU_NOTA  = {$DevNotaOrigemItem->getNumNota()} 
            ";
        }

        if ($DevNotaOrigemItem->getDataNota()) {
            $sql .= "
                AND   NTI.NTI_DT_NOTA  = TO_DATE('{$DevNotaOrigemItem->getDataNota()} 00:00:00','DD/MM/YYYY HH24:MI:SS')
            ";
        }

        $sql .="
                ORDER BY PRO.PRO_NO_DESCRICAO
            ";

//          echo "<pre>".$sql."</pre>"; die();	 // geovanni testes

        $result = $this->conn->execSql($sql);
        while ($itens = oci_fetch_object($result)) {
            $DevNotaOrigemItem = new DevNotaOrigemItem();
            $DevNotaOrigemItem->setCodigoOcorrencia($itens->DOC_CO_NUMERO);
            $DevNotaOrigemItem->setSerie($itens->NTI_NU_SERIE);
            $DevNotaOrigemItem->setNumNota($itens->NTI_NU_NOTA);
            $DevNotaOrigemItem->setDataNota($itens->NTI_DT_NOTA);
            $DevNotaOrigemItem->setCodCliente($itens->CLI_CO_NUMERO);
            $DevNotaOrigemItem->setCodProduto($itens->PRO_CO_NUMERO);
            $DevNotaOrigemItem->setDevQuantidade($itens->NTI_NU_DEVQTD);
            $DevNotaOrigemItem->setPrecoUnitario($itens->NTI_VL_PRECOUNIT);
            $DevNotaOrigemItem->setTotalBruto($itens->NTI_VL_TOTBRUTO);
            $DevNotaOrigemItem->setTotalDesconto($itens->NTI_VL_TOTDESC);
            $DevNotaOrigemItem->setTotalImposto($itens->NTI_VL_TOTIMP);
            $DevNotaOrigemItem->setTotalLiquido($itens->NTI_VL_TOTLIQ);
            $DevNotaOrigemItem->setNotaQuantidade($itens->NTI_NU_QTDNOTA);
            $DevNotaOrigemItem->setNomeProduto($itens->PRO_NO_DESCRICAO);

            $ObjDevNotaOrigemItem[] = $DevNotaOrigemItem;
        }
        return $ObjDevNotaOrigemItem;
    }
    
    public function verificaProdutoCadastrado( DevNotaOrigemItem $DevNotaOrigemItem ) {
        $sql = "
                    SELECT COUNT(*) TESTE
                    FROM DEV_NOTAORIGEM_ITEM
                    WHERE DOC_CO_NUMERO = {$DevNotaOrigemItem->getCodigoOcorrencia()}
                    AND   NTI_NU_SERIE  = {$DevNotaOrigemItem->getSerie()}
                    AND   NTI_NU_NOTA   = {$DevNotaOrigemItem->getNumNota()}
                    AND   NTI_DT_NOTA   = TO_DATE('{$DevNotaOrigemItem->getDataNota()}' ,'DD/MM/YYYY')
                    AND   CLI_CO_NUMERO = {$DevNotaOrigemItem->getCodCliente()}
                    AND   PRO_CO_NUMERO = {$DevNotaOrigemItem->getCodProduto()}
		";

        $result = $this->conn->execSql($sql);
        $parametro = oci_fetch_object($result);
        if($parametro->TESTE > 0){
            return TRUE;
        }else{
            return FALSE;
        }
        
    }
    
    public function excluir(DevNotaOrigemItem $DevNotaOrigemItem) {

        $sql = "
                    DELETE
                    FROM DEV_NOTAORIGEM_ITEM
                    WHERE DOC_CO_NUMERO = {$DevNotaOrigemItem->getCodigoOcorrencia()}
                    AND   NTI_NU_SERIE  = {$DevNotaOrigemItem->getSerie()}
                    AND   NTI_NU_NOTA   = {$DevNotaOrigemItem->getNumNota()}
                    AND   NTI_DT_NOTA   = TO_DATE('{$DevNotaOrigemItem->getDataNota()}' ,'DD/MM/YYYY')
                    AND   CLI_CO_NUMERO = {$DevNotaOrigemItem->getCodCliente()}
		";

//        echo "<pre>".print_r($sql)."</pre>"  ;//die();
        //return $this->conn->execSql($sql);

        if ($this->conn->execSql($sql)) {
            return true;
        }
        $this->OracleError->getMensagem($this->conn->getError());
        return false;
    }
    
    public function excluirItemOcorrencia(DevNotaOrigemItem $DevNotaOrigemItem) {

        $sql = "
                    DELETE
                    FROM DEV_NOTAORIGEM_ITEM
                    WHERE DOC_CO_NUMERO = {$DevNotaOrigemItem->getCodigoOcorrencia()}
                    AND   PRO_CO_NUMERO = {$DevNotaOrigemItem->getCodProduto()}
		";

        //echo $sql;
        if ($this->conn->execSql($sql)) {
            return true;
        }
        $this->OracleError->getMensagem($this->conn->getError());
        return false;
    }

    public function alterarItemOcorrencia(DevNotaOrigemItem $DevNotaOrigemItem) {

        $sql = "
                    UPDATE DEV_NOTAORIGEM_ITEM
                        SET NTI_NU_DEVQTD = {$DevNotaOrigemItem->getDevQuantidade()}
                    WHERE DOC_CO_NUMERO = {$DevNotaOrigemItem->getCodigoOcorrencia()}
                    AND   PRO_CO_NUMERO = {$DevNotaOrigemItem->getCodProduto()}
		";

        //echo $sql;
        if ($this->conn->execSql($sql)) {
            return true;
        }
        $this->OracleError->getMensagem($this->conn->getError());
        return false;
    }
    

}
