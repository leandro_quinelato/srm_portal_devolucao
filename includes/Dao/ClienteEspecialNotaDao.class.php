<?php 
Class ClienteEspecialNotaDao
{
	private $conn;

	function __construct() {
		$this->conn = new DaoSistema();
		$this->conn->conectar();
	}
	
	public function adicionar($codigoCliente, $codUsuario)
	{
		$sql = "INSERT INTO dev_cliente_especial_nota
                        VALUES ({$codigoCliente},{$codUsuario}, SYSDATE )";

		if($this->conn->execSql($sql)){
			return true; 
		}				 

		return false;
	}
	
	public function excluir($codigoCliente)
	{
		$sql = "DELETE FROM dev_cliente_especial_nota
				WHERE cen_co_numero = '".$codigoCliente."'";
				
		if($this->conn->execSql($sql)){
			return true;
		}				 

		return false;
	}
	
	public function adicionarPorRede($codigoRede, $codUsuario)
	{
		$sql = "INSERT INTO dev_cliente_especial_nota
               (SELECT CLI_CO_NUMERO, {$codUsuario}, SYSDATE FROM GLOBAL.GLB_CLIENTE where red_co_numero = {$codigoRede})";

		if($this->conn->execSql($sql)){
			return true; 
		}				 

		return false;
	}		
	
	public function excluirPorRede($codigoRede)
	{
		$sql = "DELETE FROM dev_cliente_especial_nota WHERE CEN_CO_NUMERO IN (SELECT CLI_CO_NUMERO FROM GLOBAL.GLB_CLIENTE where red_co_numero = {$codigoRede})";
				
		if($this->conn->execSql($sql)){
			return true;
		}				 

		return false;
	}	

	public function consultarClienteEspecialNota() {

		$sql = "SELECT *
					FROM GLOBAL.GLB_CLIENTE C
					INNER JOIN DEV_CLIENTE_ESPECIAL_NOTA CEN
					ON C.CLI_CO_NUMERO = CEN.CEN_CO_NUMERO
					LEFT JOIN GLOBAL.GLB_REDE_FARMACIA R
					ON R.RED_CO_NUMERO = C.RED_CO_NUMERO
					WHERE C.CLI_IN_STATUS IS NULL
					ORDER BY C.CLI_CO_NUMERO ASC
                       "; 

		if($result = $this->conn->execSql($sql)){
			return $result; 
		}				 

		return false;
	}


	public function verificaClienteEspecialNota($codigoCliente) {

		$sql = "SELECT cen_co_numero 
					FROM dev_cliente_especial_nota
					WHERE cen_co_numero = ".$codigoCliente;

		if($result = $this->conn->execSql($sql)){
			OCIFetchInto ($result, $linha, OCI_ASSOC);
			if(!is_null($linha['CEN_CO_NUMERO']))
				return true;
		}				 
		return false;
	}
	
}