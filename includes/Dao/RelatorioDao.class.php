<?php

/**
 * Description of RelatorioDao
 *
 * @author geovanni.info
 */
class RelatorioDao {

    private $conn;

    function __construct() {

        $this->conn = new DaoSistema();
        $this->conn->conectar();
    }

    public function relatorioMultaDevolucao($data_inicio, $data_final, $tporigem = NULL, $fornecedor = null, $setor = null, $cliente = null, $rede = null, $divisao = null) {
        $sql = "SELECT SUM(TOTAL) QNTD_NOTA, 
				       SUM(VALOR) VALOR , 
				       nto_in_origem_responsavel,
				       RESPONSAVEL,
				       CODIGO,
				       CASE WHEN  SUM(TOTAL) > ( SELECT mul_qt_nota FROM dev_multa ) THEN  (SUM(TOTAL) - (SELECT mul_qt_nota FROM dev_multa)) *( SELECT mul_vr_multa FROM dev_multa  )
				       ELSE 
				           0
				       END VALOR_MULTA 
				FROM 
				    (
					    SELECT COUNT(distinct dev.doc_co_numero) total, 
					            sum(dev.doc_vr_valor) valor,       
					            dev.nto_in_origem_responsavel ,
					            dev.responsavel, 
					            dev.codigo, 
					            TO_CHAR(dev.doc_dt_cadastro,'DD/MM/YYYY') doc_dt_cadastro, 
			                    dev.cli_co_numero ,
			                    dev.doc_dt_interno
					    FROM view_devolucao_teste dev
                                            INNER JOIN USERVIMED.NOTAVENDA NTV ON  DEV.NTO_NU_SERIE = NTV.SERIE AND DEV.NTO_NU_NOTA = NTV.NUMNOTA AND DEV.NTO_DT_NOTA = NTV.DTMOV AND DEV.CLI_CO_NUMERO = NTV.CDCLIENTE                                            
					    WHERE dev.doc_in_tipo = 1
					    AND dev.nto_in_origem_responsavel is not null  
					    ";
        if ($divisao) {
            $sql .=" AND ntv.indicador in ({$divisao})";
        }
        if ($tporigem) {
            //$sql .=" AND nto_in_origem_responsavel = '$tporigem'";
            if ($tporigem == "GLOB") {
                $sql .=" AND nto_in_origem_responsavel IN ('GC','DM','EP','GS','GF','GI','OR','GM','GR','GL','GV','GT','G','N')";
            } elseif ($tporigem == "TELE") {
                $sql .=" AND nto_in_origem_responsavel IN ('T','TR','TY','TA','TZ')";
            } elseif ($tporigem == "CENT") {
                $sql .=" AND nto_in_origem_responsavel IN ('OC','CH','O')";
            } elseif ($tporigem == "MESA") {
                $sql .=" AND nto_in_origem_responsavel IN ('M','MF','MP')";
            } elseif ($tporigem == "REPR") {
                $sql .=" AND nto_in_origem_responsavel = '{$tporigem}'";
                if ($setor) {
                    $sql .=" AND codigo = '$setor'";
                }
            } elseif ($tporigem == "INDU") {
                $sql .=" AND nto_in_origem_responsavel = '{$tporigem}'";
                if ($fornecedor) {
                    $sql .= " AND codigo = '$fornecedor'";
                }
            } else {
                $sql .=" AND nto_in_origem_responsavel = '{$tporigem}' ";
            }
        }
        $sql.=" GROUP BY dev.nto_in_origem_responsavel, dev.responsavel, dev.codigo, to_char(dev.doc_dt_cadastro,'DD/MM/YYYY'), dev.cli_co_numero, dev.doc_dt_interno
					    UNION ALL 
						    SELECT  count(1) total , 
						            sum(dev.doc_vr_valor) valor,        
						            dev.nto_in_origem_responsavel , 
						            dev.responsavel, 
						            dev.codigo, 
						            to_char(dev.doc_dt_cadastro,'DD/MM/YYYY') doc_dt_cadastro, 
		                    		dev.cli_co_numero,	                    		 
				                    dev.doc_dt_interno
						    FROM view_devolucao_teste dev
                                                    INNER JOIN USERVIMED.NOTAVENDA NTV ON  DEV.NTO_NU_SERIE = NTV.SERIE AND DEV.NTO_NU_NOTA = NTV.NUMNOTA AND DEV.NTO_DT_NOTA = NTV.DTMOV AND DEV.CLI_CO_NUMERO = NTV.CDCLIENTE
							WHERE dev.doc_in_tipo = 2 
							  AND dev.nto_in_origem_responsavel is not null							  
					    	";
        if ($divisao) {
            $sql .=" AND ntv.indicador in ({$divisao})";
        }
        if ($tporigem) {
            //$sql .=" AND nto_in_origem_responsavel = '$tporigem'";
            if ($tporigem == "GLOB") {
                $sql .=" AND nto_in_origem_responsavel IN ('GC','DM','EP','GS','GF','GI','OR','GM','GR','GL','GV','GT','G','N')";
            } elseif ($tporigem == "TELE") {
                $sql .=" AND nto_in_origem_responsavel IN ('T','TR','TY','TA','TZ')";
            } elseif ($tporigem == "CENT") {
                $sql .=" AND nto_in_origem_responsavel IN ('OC','CH','O')";
            } elseif ($tporigem == "MESA") {
                $sql .=" AND nto_in_origem_responsavel IN ('M','MF','MP')";
            } elseif ($tporigem == "REPR") {
                $sql .=" AND nto_in_origem_responsavel = '{$tporigem}'";
                if ($setor) {
                    $sql .=" AND codigo = '$setor'";
                }
            } elseif ($tporigem == "INDU") {
                $sql .=" AND nto_in_origem_responsavel = '{$tporigem}'";
                if ($fornecedor) {
                    $sql .= " AND codigo = '$fornecedor'";
                }
            } else {
                $sql .=" AND nto_in_origem_responsavel = '{$tporigem}' ";
            }
        }

        $sql .= "GROUP BY dev.nto_in_origem_responsavel, dev.responsavel, dev.codigo, to_char(dev.doc_dt_cadastro,'DD/MM/YYYY'), dev.cli_co_numero, dev.doc_dt_interno
					    UNION ALL 
						    SELECT  count(1) total , 
						            sum(dev.doc_vr_valor) valor,        
						            dev.nto_in_origem_responsavel , 
						            dev.responsavel, 
						            dev.codigo, 
						            to_char(dev.doc_dt_cadastro,'DD/MM/YYYY') doc_dt_cadastro, 
		                    		dev.cli_co_numero,	                    		 
				                    dev.doc_dt_interno
						    FROM view_devolucao_teste dev
                                                    INNER JOIN USERVIMED.NOTAVENDA NTV ON  DEV.NTO_NU_SERIE = NTV.SERIE AND DEV.NTO_NU_NOTA = NTV.NUMNOTA AND DEV.NTO_DT_NOTA = NTV.DTMOV AND DEV.CLI_CO_NUMERO = NTV.CDCLIENTE
							WHERE dev.doc_in_tipo = 3 
							  AND dev.nto_in_origem_responsavel is not null							  
					    	";
        if ($divisao) {
            $sql .=" AND ntv.indicador in ({$divisao})";
        }
        if ($tporigem) {
            //$sql .=" AND nto_in_origem_responsavel = '$tporigem'";
            if ($tporigem == "GLOB") {
                $sql .=" AND nto_in_origem_responsavel IN ('GC','DM','EP','GS','GF','GI','OR','GM','GR','GL','GV','GT','G','N')";
            } elseif ($tporigem == "TELE") {
                $sql .=" AND nto_in_origem_responsavel IN ('T','TR','TY','TA','TZ')";
            } elseif ($tporigem == "CENT") {
                $sql .=" AND nto_in_origem_responsavel IN ('OC','CH','O')";
            } elseif ($tporigem == "MESA") {
                $sql .=" AND nto_in_origem_responsavel IN ('M','MF','MP')";
            } elseif ($tporigem == "REPR") {
                $sql .=" AND nto_in_origem_responsavel = '{$tporigem}'";
                if ($setor) {
                    $sql .=" AND codigo = '$setor'";
                }
            } elseif ($tporigem == "INDU") {
                $sql .=" AND nto_in_origem_responsavel = '{$tporigem}'";
                if ($fornecedor) {
                    $sql .= " AND codigo = '$fornecedor'";
                }
            } else {
                $sql .=" AND nto_in_origem_responsavel = '{$tporigem}' ";
            }
        }

        $sql .= "GROUP BY dev.nto_in_origem_responsavel, dev.responsavel, dev.codigo, to_char(dev.doc_dt_cadastro,'DD/MM/YYYY'), dev.cli_co_numero, dev.doc_dt_interno
					    UNION ALL 
						    SELECT  count(1) total , 
						            sum(dev.doc_vr_valor) valor,        
						            dev.nto_in_origem_responsavel , 
						            dev.responsavel, 
						            dev.codigo, 
						            to_char(dev.doc_dt_cadastro,'DD/MM/YYYY') doc_dt_cadastro, 
		                    		dev.cli_co_numero,	                    		 
				                    dev.doc_dt_interno
						    FROM view_devolucao_teste dev
                                                    INNER JOIN USERVIMED.NOTAVENDA NTV ON  DEV.NTO_NU_SERIE = NTV.SERIE AND DEV.NTO_NU_NOTA = NTV.NUMNOTA AND DEV.NTO_DT_NOTA = NTV.DTMOV AND DEV.CLI_CO_NUMERO = NTV.CDCLIENTE
							WHERE dev.doc_in_tipo = 4 
							  AND dev.nto_in_origem_responsavel is not null							  
					    	";
        if ($divisao) {
            $sql .=" AND ntv.indicador in ({$divisao})";
        }
        if ($tporigem) {
            //$sql .=" AND nto_in_origem_responsavel = '$tporigem'";
            if ($tporigem == "GLOB") {
                $sql .=" AND nto_in_origem_responsavel IN ('GC','DM','EP','GS','GF','GI','OR','GM','GR','GL','GV','GT','G','N')";
            } elseif ($tporigem == "TELE") {
                $sql .=" AND nto_in_origem_responsavel IN ('T','TR','TY','TA','TZ')";
            } elseif ($tporigem == "CENT") {
                $sql .=" AND nto_in_origem_responsavel IN ('OC','CH','O')";
            } elseif ($tporigem == "MESA") {
                $sql .=" AND nto_in_origem_responsavel IN ('M','MF','MP')";
            } elseif ($tporigem == "REPR") {
                $sql .=" AND nto_in_origem_responsavel = '{$tporigem}'";
                if ($setor) {
                    $sql .=" AND codigo = '$setor'";
                }
            } elseif ($tporigem == "INDU") {
                $sql .=" AND nto_in_origem_responsavel = '{$tporigem}'";
                if ($fornecedor) {
                    $sql .= " AND codigo = '$fornecedor'";
                }
            } else {
                $sql .=" AND nto_in_origem_responsavel = '{$tporigem}' ";
            }
        }


        $sql .= "GROUP BY dev.nto_in_origem_responsavel, dev.responsavel, dev.codigo, to_char(dev.doc_dt_cadastro,'DD/MM/YYYY'), dev.cli_co_numero, dev.doc_dt_interno
					    UNION ALL 
						    SELECT COUNT(distinct dev.doc_co_numero) total, 
					            sum(dev.doc_vr_valor) valor,       
					            dev.nto_in_origem_responsavel ,
					            dev.responsavel, 
					            dev.codigo, 
					            TO_CHAR(dev.doc_dt_cadastro,'DD/MM/YYYY') doc_dt_cadastro, 
			                    dev.cli_co_numero ,
			                    dev.doc_dt_interno
					    FROM view_devolucao_teste dev
                                            INNER JOIN USERVIMED.NOTAVENDA NTV ON  DEV.NTO_NU_SERIE = NTV.SERIE AND DEV.NTO_NU_NOTA = NTV.NUMNOTA AND DEV.NTO_DT_NOTA = NTV.DTMOV AND DEV.CLI_CO_NUMERO = NTV.CDCLIENTE                                            
					    WHERE dev.doc_in_tipo = 5
					    AND dev.nto_in_origem_responsavel is not null 						  
					    	";
        if ($divisao) {
            $sql .=" AND ntv.indicador in ({$divisao})";
        }
        if ($tporigem) {
            //$sql .=" AND nto_in_origem_responsavel = '$tporigem'";
            if ($tporigem == "GLOB") {
                $sql .=" AND nto_in_origem_responsavel IN ('GC','DM','EP','GS','GF','GI','OR','GM','GR','GL','GV','GT','G','N')";
            } elseif ($tporigem == "TELE") {
                $sql .=" AND nto_in_origem_responsavel IN ('T','TR','TY','TA','TZ')";
            } elseif ($tporigem == "CENT") {
                $sql .=" AND nto_in_origem_responsavel IN ('OC','CH','O')";
            } elseif ($tporigem == "MESA") {
                $sql .=" AND nto_in_origem_responsavel IN ('M','MF','MP')";
            } elseif ($tporigem == "REPR") {
                $sql .=" AND nto_in_origem_responsavel = '{$tporigem}'";
                if ($setor) {
                    $sql .=" AND codigo = '$setor'";
                }
            } elseif ($tporigem == "INDU") {
                $sql .=" AND nto_in_origem_responsavel = '{$tporigem}'";
                if ($fornecedor) {
                    $sql .= " AND codigo = '$fornecedor'";
                }
            } else {
                $sql .=" AND nto_in_origem_responsavel = '{$tporigem}' ";
            }
        }

        $sql .= "   GROUP BY dev.nto_in_origem_responsavel, dev.responsavel, dev.codigo, to_char(dev.doc_dt_cadastro,'DD/MM/YYYY'), dev.cli_co_numero, dev.doc_dt_interno    
            
                            )
				WHERE 
				doc_dt_interno BETWEEN TO_DATE('$data_inicio','DD/MM/YYYY') AND TO_DATE('$data_final','DD/MM/YYYY')";

        if ($cliente) {
            $sql .=" AND cli_co_numero = $cliente";
        }
        if ($rede) {
            $sql .=" AND cli_co_numero IN (
                  			SELECT cli.cli_co_numero FROM GLOBAL.glb_cliente cli 
                  			INNER JOIN GLOBAL.glb_rede_farmacia red ON cli.red_co_numero = cli.red_co_numero 
                  			WHERE cli.red_co_numero = $rede 
                	)";
        }
        $sql .= " GROUP BY nto_in_origem_responsavel, RESPONSAVEL, CODIGO
				ORDER BY  nto_in_origem_responsavel";
        //        echo "<pre>" . $sql . "</pre>";
        if ($result = $this->conn->execSql($sql)) {
            return $result;
        }
        return false;
    }

    public function listaOrigemVendaNota() {
        $sql = " SELECT tporigvenda,INITCAP(nome)nome  
                 FROM uservimed.tp_origemvenda 
                 WHERE UPPER(NOME) NOT LIKE 'GLOBALPH%'
                      AND UPPER(NOME) NOT LIKE 'CENTRAL HOSPITALAR%'
                      AND UPPER(NOME) NOT LIKE 'MESA%'
                      AND UPPER(NOME) NOT LIKE 'TELEVENDAS%'                     
                      AND UPPER(NOME) NOT LIKE 'DIRETORIA'
                ORDER BY NOME  ";
        return $this->conn->execSql($sql);
    }

    public function relatorioNotaDevolucao($data_inicio, $data_final, $tporigem = NULL, $fornecedor = null, $setor = null, $cliente = null, $rede = null, $divisao = null, $tresp = null) {
        $sql = "SELECT
                                    dev.codigo ,
				    dev.responsavel,
				    TO_CHAR(dev.doc_dt_cadastro,'DD/MM/YYYY') doc_dt_cadastro,
				    dev.nto_in_origem_responsavel,
				    TO_CHAR(dev.nto_dt_nota,'DD/MM/YYYY') nto_dt_nota,
				    dev.nto_nu_serie,
				    dev.nto_nu_nota ,
				    dev.doc_vr_valor, 
				    cli.cli_co_numero,
				    cli.cli_no_razao_social,       
				    dev.doc_nu_devserie, 
				    dev.doc_nu_devnota,
                                    DEV.DOC_CO_NUMERO,";

        $sql .="              tresp.trp_co_numero , 
                                      tresp.trp_no_descricao, ";

        $sql .="              UPPER(tpo.nome) ORI_VENDA 
                                     ,DECODE(NTV.indicador, 'F', 'FARMA', 'H', 'HOSPITALAR', 'A', 'ALIMENTAR', 'O', 'DIST DIRETA', NTV.indicador) RPR_IN_DIVISAO
                                       ,VEN.VEN_CO_NUMERO
                                       ,VEN.VEN_NO_LABEL
                                       ,CED.CED_NO_CENTRO
				FROM
				  view_devolucao_teste dev 
				    INNER JOIN GLOBAL.glb_cliente cli ON cli.cli_co_numero = dev.cli_co_numero 
  				  INNER JOIN GLOBAL.glb_rede_farmacia red ON cli.red_co_numero = red.red_co_numero	
  				  INNER JOIN USERVIMED.NOTAVENDA NTV ON DEV.NTO_NU_SERIE   = NTV.SERIE AND DEV.NTO_NU_NOTA   = NTV.NUMNOTA
                                      AND ntv.cdcliente = dev.cli_co_numero 
                                      AND ntv.dtmov = dev.nto_dt_nota
          		  INNER JOIN USERVIMED.tp_origemvenda tpo ON ntv.tporigvenda = tpo.tporigvenda";

        $sql .="  INNER JOIN dev_notaorigem notaorigem ON dev.doc_co_numero = notaorigem.doc_co_numero
                          INNER JOIN dev_tipo_responsabilidade tresp ON notaorigem.trp_co_numero = tresp.trp_co_numero";

        $sql .="  INNER JOIN dev_notaorigem origem ON dev.doc_co_numero = origem.doc_co_numero
                  INNER JOIN GLOBAL.GLB_VENDEDOR VEN ON VEN.VEN_CO_NUMERO = NTV.CDSETOR
                  INNER JOIN GLOBAL.GLB_CENTRO_DISTRIBUICAO CED ON CED.CED_CO_NUMERO = NTV.CD
				WHERE";

        $sql .="           dev.nto_in_origem_responsavel is not null         				  
				   AND DEV.DOC_DT_CADASTRO BETWEEN to_date ('$data_inicio','DD/MM/YYYY') AND to_date('$data_final','DD/MM/YYYY')";

        if ($divisao) {
            $sql .=" AND ntv.indicador in ({$divisao})";
        }

        if ($tporigem) {

            if ($tporigem == "GLOB") {
                $sql .=" AND dev.nto_in_origem_responsavel IN ('GC','DM','EP','GS','GF','GI','OR','GM','GR','GL','GV','GT','G','N')";
            } elseif ($tporigem == "TELE") {
                $sql .=" AND dev.nto_in_origem_responsavel IN ('T','TR','TY','TA','TZ')";
            } elseif ($tporigem == "CENT") {
                $sql .=" AND dev.nto_in_origem_responsavel IN ('OC','CH','O')";
            } elseif ($tporigem == "MESA") {
                $sql .=" AND dev.nto_in_origem_responsavel IN ('M','MF','MP')";
            } elseif ($tporigem == "REPR") {
                $sql .=" AND dev.nto_in_origem_responsavel = '{$tporigem}'";
                if ($setor) {
                    $sql .=" AND dev.codigo = '$setor'";
                }
            } elseif ($tporigem == "INDU") {
                $sql .=" AND dev.nto_in_origem_responsavel = '{$tporigem}'";
                if ($fornecedor) {
                    $sql .= " AND dev.codigo = '$fornecedor'";
                }
            } else {
                $sql .=" AND dev.nto_in_origem_responsavel = '{$tporigem}' ";
            }
        }
        if ($cliente) {
            $sql .=" AND cli.cli_co_numero = $cliente";
        }
        if ($rede) {
            $sql .=" AND cli.red_co_numero = $rede";
        }
        if ($tresp) {
            $sql .=" AND TRESP.TRP_CO_NUMERO = $tresp";
        }

        $sql.=" ORDER BY responsavel,cli_no_razao_social";

        //		echo "<pre>".$sql."</pre>"; //die();// teste geovanni

        if ($result = $this->conn->execSql($sql)) {
            return $result;
        }
        return false;
    }

    public function relatorioOcorrenciaStatus($dataInicial, $dataFinal, $protocolo = NULL, $cliente = NULL, $rede = NULL, $status = NULL, $rota = NULL, $cedis = NULL, GlbUsuario $ObjUsuario) {
        $sql = "SELECT 
                        DOC.DST_CO_NUMERO,
                        DST.DST_NO_DESCRICAO,
                        DOC.DOC_CO_NUMERO,
                        TO_CHAR(DOC.DOC_DT_CADASTRO, 'DD/MM/YYYY') DATA_CADASTRO,
                        DOC.DOC_DT_CADASTRO,
                        DOC.DOC_CO_CADASTRANTE,
                        USU.USU_NO_USERNAME,
                        DOC.ROT_CO_NUMERO,
                        DOC.DOC_IN_TIPO,
                        TID.TID_CO_DESCRICAO,
                        NTO.NTO_NU_SERIE,
                        NTO.NTO_NU_NOTA,
                        DECODE(NTO.NTO_CO_DIVISAO, 'F', 'FARMA', 'H', 'HOSPITALAR', 'A', 'ALIMENTAR', 'O', 'DIST DIRETA', NTO.NTO_CO_DIVISAO) DIVISAO,
                        DOC.CED_CO_NUMERO CD,
                        CED.CED_NO_CENTRO,
                        DOC.DOC_NU_DEVSERIE,
                        DOC.DOC_NU_DEVNOTA,
                        DOC.DOC_VR_VALOR,
                        DOC.DOC_QT_PRODUTO,
                        CLI.CLI_CO_NUMERO,
                        CLI.CLI_NO_FANTASIA,
                        CLI.RED_CO_NUMERO

                FROM DEV_OCORRENCIA DOC
                INNER JOIN DEV_NOTAORIGEM                 NTO ON NTO.DOC_CO_NUMERO = DOC.DOC_CO_NUMERO
                INNER JOIN GLOBAL.GLB_CLIENTE             CLI ON CLI.CLI_CO_NUMERO = DOC.CLI_CO_NUMERO
                LEFT  JOIN GLOBAL.GLB_CENTRO_DISTRIBUICAO CED ON CED.CED_CO_NUMERO = DOC.CED_CO_NUMERO
                LEFT  JOIN DEV_STATUS                     DST ON DST.DST_CO_NUMERO = DOC.DST_CO_NUMERO
                INNER JOIN GLOBAL.GLB_USUARIO             USU ON USU.USU_CO_NUMERO = DOC.DOC_CO_CADASTRANTE
                INNER JOIN DEV_TIPO_DEVOLUCAO             TID ON TID.TID_CO_NUMERO = DOC.DOC_IN_TIPO
                WHERE DOC.DOC_DT_CADASTRO BETWEEN TO_DATE ('{$dataInicial} 00:00:00', 'DD/MM/YYYY HH24:MI:SS') AND  TO_DATE ('{$dataFinal} 23:59:59', 'DD/MM/YYYY HH24:MI:SS')";

        if (($status) && ($status != "TOD")) {
            $sql .=" AND DST.DST_CO_NUMERO = {$status}";
        }

        if ($protocolo) {
            $sql .=" AND DOC.DOC_CO_NUMERO = $protocolo";
        }

        if ($cliente) {
            $sql .=" AND cli.cli_co_numero = $cliente";
        }
        if ($rede) {
            $sql .=" AND cli.red_co_numero = $rede";
        }

        if (($rota) && ($rota != "TOD")) {
            $sql .=" AND DOC.ROT_CO_NUMERO = $rota";
        }

        if (($cedis) && ($cedis != "TOD")) {
            $sql .= " AND DOC.CED_CO_NUMERO = {$cedis}";
        }
        
        if ($ObjUsuario->getUsuarioTipo() == "CLI") {

            $sql .=" AND DOC.CLI_CO_NUMERO = {$ObjUsuario->getObjCliente()->getClienteCodigo()}";
        } else if ($ObjUsuario->getUsuarioTipo() == "VEN") {
            
            $VendedorUsuario = $ObjUsuario->getObjVendedor();

            $sql .= " AND DOC.CLI_CO_NUMERO IN (
                        SELECT DISTINCT CLI.CLI_CO_NUMERO
                         FROM GLOBAL.GLB_CLIENTE_VENDEDOR CVEN 
                         INNER JOIN GLOBAL.GLB_VENDEDOR VEN ON VEN.VEN_CO_NUMERO = CVEN.VEN_CO_NUMERO
                         INNER JOIN GLOBAL.GLB_CLIENTE  CLI ON CLI.CLI_CO_NUMERO = CVEN.CLI_CO_NUMERO
                         WHERE 
                               CVEN.CLV_IN_STATUS IS NULL ";

            if (($VendedorUsuario[0]->getTipo() == "V") || ($VendedorUsuario[0]->getTipo() == "N")) {
                $sql .= " AND VEN.VEN_CO_SUPERVISOR = {$VendedorUsuario[0]->getCodigo()} ";
            } else {
                $sql .= " AND VEN.VEN_CO_NUMERO = {$VendedorUsuario[0]->getCodigo()} ";
            }
            
            $sql .= ")";
        } else if ($ObjUsuario->getUsuarioTipo() == "TRA") {
            
            $sql .= " AND DOC.ROT_CO_NUMERO IN (
                        SELECT DISTINCT ROT_CO_NUMERO FROM USUARIO_ROTA WHERE USU_CO_NUMERO = {$ObjUsuario->getUsuarioCodigo()}
                        )";
            
        }

        $sql.=" ORDER BY DOC_DT_CADASTRO";

        //		echo "<pre>".$sql."</pre>"; //die();// teste geovanni

        if ($result = $this->conn->execSql($sql)) {
            return $result;
        }
        return false;
    }

    public function relatorioAprovacaoAutomatica($dataInicial, $dataFinal, $protocolo = NULL, $cliente = NULL, $rede = NULL, $cedis = NULL, $tipoAprovacao = NULL) {
        $sql = "
                    SELECT  OCO.DOC_CO_NUMERO
                           ,TO_CHAR(OCO.DOC_DT_CADASTRO, 'DD/MM/YYYY') DATA_CADASTRO
                           ,OCO.DOC_DT_CADASTRO
                           ,TO_CHAR(DECODE( DOP.DOP_DT_SAIDA, NULL, OCO.DOC_DT_GERENTE, DOP.DOP_DT_SAIDA  ), 'DD/MM/YYYY') DATA_APROVACAO
                           ,DECODE(OCO.DEV_IN_AUTOMATICO, 1 , 'AUTOMATICA', 2 , 'VALOR', 'GERENTE') TIPO_APROVACAO
                           ,WNT.CDEQUIPE EQUIPE 
                           ,EQP.VEE_NO_EQUIPE GERENTE_CONTAS
                           ,EQP.VEE_NU_REGIONAL COD_REGIONAL
                           ,EQP.VEE_NO_REGIONAL GERENTE_REGIONAL
                           ,EQP.VEE_NU_DIVISIONAL COD_DIVISIONAL
                           ,EQP.VEE_NO_DIVISIONAL GERENTE_DIVISIONAL
                           ,OCO.TOC_CO_NUMERO
                           ,TOC.TOC_NO_DESCRICAO  
                           ,OCO.CED_CO_NUMERO
                           ,CED.CED_NO_CENTRO
                           ,OCO.DOC_VR_VALOR
                           ,CLI.CLI_CO_NUMERO
                           ,CLI.CLI_NO_FANTASIA
                           ,CLI.RED_CO_NUMERO
                           ,OCO.DEV_IN_AUTOMATICO

                    FROM      DEV_OCORRENCIA       OCO
                    INNER JOIN DEV_NOTAORIGEM NTO ON NTO.DOC_CO_NUMERO = OCO.DOC_CO_NUMERO
                    INNER JOIN CAL_TIPO_OCORRENCIA TOC ON TOC.TOC_CO_NUMERO = OCO.TOC_CO_NUMERO AND TOC.DEP_CO_NUMERO = 29 AND TOC.SLA_CO_NUMERO = 1
                    LEFT  JOIN GLOBAL.GLB_CENTRO_DISTRIBUICAO CED ON CED.CED_CO_NUMERO = OCO.CED_CO_NUMERO
                    LEFT  JOIN DEV_OCORRENCIA_PASSO DOP ON DOP.DOC_CO_NUMERO = OCO.DOC_CO_NUMERO AND DOP.DPA_CO_NUMERO = 1 AND DOP.DOP_IN_APROVADO = 1
                    INNER JOIN GLOBAL.GLB_CLIENTE   CLI ON CLI.CLI_CO_NUMERO = OCO.CLI_CO_NUMERO
                    INNER JOIN USERVIMED.NOTAVENDA WNT   ON NTO.CHDOC = WNT.CHDOC
                            AND WNT.CDCLIENTE = OCO.CLI_CO_NUMERO 
                            AND WNT.DTMOV     = NTO.NTO_DT_NOTA
                    INNER JOIN USERVIMED.VIEW_ESTRUTURA_EQUIPE EQP ON WNT.CDEQUIPE = EQP.VEE_NU_EQUIPE 
                            AND WNT.CDSETOR = EQP.VEE_NU_SETOR 
                            AND TO_CHAR(WNT.DTMOV,'MM') = EQP.VEE_NU_MES 
                            AND TO_CHAR(WNT.DTMOV,'YYYY') = EQP.VEE_NU_ANO
                    WHERE /*OCO.DEV_IN_AUTOMATICO IS NOT NULL
                    AND*/  OCO.DOC_DT_CADASTRO BETWEEN TO_DATE ('{$dataInicial} 00:00:00', 'DD/MM/YYYY HH24:MI:SS') AND  TO_DATE ('{$dataFinal} 23:59:59', 'DD/MM/YYYY HH24:MI:SS')";

        if ($protocolo) {
            $sql .=" AND OCO.DOC_CO_NUMERO = $protocolo";
        }

        if ($cliente) {
            $sql .=" AND cli.cli_co_numero = $cliente";
        }
        if ($rede) {
            $sql .=" AND cli.red_co_numero = $rede";
        }

        if (($cedis) && ($cedis != "TOD")) {
            $sql .= " AND OCO.CED_CO_NUMERO = {$cedis}";
        }

        if (($tipoAprovacao) && ($tipoAprovacao != "TOD")) {
            
            if ( $tipoAprovacao == 3){    
                $sql .= " AND OCO.DEV_IN_AUTOMATICO is NULL";
            }else{
                $sql .= " AND OCO.DEV_IN_AUTOMATICO = {$tipoAprovacao}";
            }
        }


        $sql.=" ORDER BY OCO.DOC_DT_CADASTRO";

        //		echo "<pre>".$sql."</pre>"; //die();// teste geovanni

        if ($result = $this->conn->execSql($sql)) {
            return $result;
        }
        return false;
    }

    public function relatorioPendenteComercial($dataInicial, $dataFinal, $protocolo = NULL, $cliente = NULL, $rede = NULL, $cedis = NULL, $divisao = NULL, $rota = NULL, GlbUsuario $ObjUsuario) {

        $status = 1; //codigo do status - 1	AGUARDANDO APROVACAO

        $sql = "
                    SELECT  OCO.DOC_CO_NUMERO
                           ,TO_CHAR(OCO.DOC_DT_CADASTRO, 'DD/MM/YYYY') DATA_CADASTRO
                           ,OCO.DOC_DT_CADASTRO
                           ,OCO.DOC_CO_CADASTRANTE
                           ,USU.USU_NO_USERNAME
                           ,WNT.CDEQUIPE EQUIPE 
                           ,EQP.VEE_NO_EQUIPE GERENTE_CONTAS
                           ,EQP.VEE_NU_REGIONAL COD_REGIONAL
                           ,EQP.VEE_NO_REGIONAL GERENTE_REGIONAL
                           ,EQP.VEE_NU_DIVISIONAL COD_DIVISIONAL
                           ,EQP.VEE_NO_DIVISIONAL GERENTE_DIVISIONAL
                           ,OCO.TOC_CO_NUMERO
                           ,TOC.TOC_NO_DESCRICAO  
                           ,OCO.CED_CO_NUMERO
                           ,CED.CED_NO_CENTRO
                           ,OCO.DOC_VR_VALOR
                           ,CLI.CLI_CO_NUMERO
                           ,CLI.CLI_NO_FANTASIA
                           ,CLI.RED_CO_NUMERO
                           ,OCO.DEV_IN_AUTOMATICO
                           ,OCO.ROT_CO_NUMERO
                           ,DECODE(WNT.indicador, 'F', 'FARMA', 'H', 'HOSPITALAR', 'A', 'ALIMENTAR', 'O', 'DIST DIRETA', WNT.indicador) DIVISAO

                    FROM      DEV_OCORRENCIA       OCO
                    INNER JOIN DEV_NOTAORIGEM NTO ON NTO.DOC_CO_NUMERO = OCO.DOC_CO_NUMERO
                    INNER JOIN CAL_TIPO_OCORRENCIA TOC ON TOC.TOC_CO_NUMERO = OCO.TOC_CO_NUMERO AND TOC.DEP_CO_NUMERO = 29 AND TOC.SLA_CO_NUMERO = 1
                    INNER JOIN GLOBAL.GLB_USUARIO USU  ON USU.USU_CO_NUMERO = OCO.DOC_CO_CADASTRANTE
                    LEFT  JOIN GLOBAL.GLB_CENTRO_DISTRIBUICAO CED ON CED.CED_CO_NUMERO = OCO.CED_CO_NUMERO
                    LEFT  JOIN DEV_OCORRENCIA_PASSO DOP ON DOP.DOC_CO_NUMERO = OCO.DOC_CO_NUMERO AND DOP.DPA_CO_NUMERO = 1 AND DOP.DOP_IN_APROVADO = 1
                    INNER JOIN GLOBAL.GLB_CLIENTE   CLI ON CLI.CLI_CO_NUMERO = OCO.CLI_CO_NUMERO
                    INNER JOIN USERVIMED.NOTAVENDA WNT   ON NTO.CHDOC = WNT.CHDOC
                            AND WNT.CDCLIENTE = OCO.CLI_CO_NUMERO 
                            AND WNT.DTMOV     = NTO.NTO_DT_NOTA
                    INNER JOIN USERVIMED.VIEW_ESTRUTURA_EQUIPE EQP ON WNT.CDEQUIPE = EQP.VEE_NU_EQUIPE 
                            AND WNT.CDSETOR = EQP.VEE_NU_SETOR 
                            AND TO_CHAR(WNT.DTMOV,'MM') = EQP.VEE_NU_MES 
                            AND TO_CHAR(WNT.DTMOV,'YYYY') = EQP.VEE_NU_ANO
                    WHERE OCO.DST_CO_NUMERO = {$status}
                    AND  OCO.DOC_DT_CADASTRO BETWEEN TO_DATE ('{$dataInicial} 00:00:00', 'DD/MM/YYYY HH24:MI:SS') AND  TO_DATE ('{$dataFinal} 23:59:59', 'DD/MM/YYYY HH24:MI:SS')";

        if ($protocolo) {
            $sql .=" AND OCO.DOC_CO_NUMERO = $protocolo";
        }

        if ($cliente) {
            $sql .=" AND cli.cli_co_numero = $cliente";
        }
        if ($rede) {
            $sql .=" AND cli.red_co_numero = $rede";
        }

        if (($cedis) && ($cedis != "TOD")) {
            $sql .= " AND OCO.CED_CO_NUMERO = {$cedis}";
        }

        if ($divisao) {
            $sql .=" AND WNT.indicador in ({$divisao})";
        }

        if (($rota) && ($rota != "TOD")) {
            $sql .=" AND OCO.ROT_CO_NUMERO = $rota";
        }
        
        if ($ObjUsuario->getUsuarioTipo() == "CLI") {

            $sql .=" AND OCO.CLI_CO_NUMERO = {$ObjUsuario->getObjCliente()->getClienteCodigo()}";
        } else if ($ObjUsuario->getUsuarioTipo() == "VEN") {
            
            $VendedorUsuario = $ObjUsuario->getObjVendedor();

            $sql .= " AND OCO.CLI_CO_NUMERO IN (
                        SELECT DISTINCT CLI.CLI_CO_NUMERO
                         FROM GLOBAL.GLB_CLIENTE_VENDEDOR CVEN 
                         INNER JOIN GLOBAL.GLB_VENDEDOR VEN ON VEN.VEN_CO_NUMERO = CVEN.VEN_CO_NUMERO
                         INNER JOIN GLOBAL.GLB_CLIENTE  CLI ON CLI.CLI_CO_NUMERO = CVEN.CLI_CO_NUMERO
                         WHERE 
                               CVEN.CLV_IN_STATUS IS NULL ";

            if (($VendedorUsuario[0]->getTipo() == "V") || ($VendedorUsuario[0]->getTipo() == "N")) {
                $sql .= " AND VEN.VEN_CO_SUPERVISOR = {$VendedorUsuario[0]->getCodigo()} ";
            } else {
                $sql .= " AND VEN.VEN_CO_NUMERO = {$VendedorUsuario[0]->getCodigo()} ";
            }
            
            $sql .= ")";
        } else if ($ObjUsuario->getUsuarioTipo() == "TRA") {
            
            $sql .= " AND OCO.ROT_CO_NUMERO IN (
                        SELECT DISTINCT ROT_CO_NUMERO FROM USUARIO_ROTA WHERE USU_CO_NUMERO = {$ObjUsuario->getUsuarioCodigo()}
                        )";
            
        }

        $sql.=" ORDER BY OCO.DOC_DT_CADASTRO";

        //		echo "<pre>".$sql."</pre>"; //die();// teste geovanni

        if ($result = $this->conn->execSql($sql)) {
            return $result;
        }
        return false;
    }

    public function relatorioOcorrenciaAberta($dataInicial, $dataFinal, $protocolo = NULL, $area = NULL, GlbUsuario $ObjUsuario) {

        $sql = "SELECT                         TO_CHAR(DOC.DOC_DT_CADASTRO,'DD/MM/YYYY') DATA_CADASTRO,
                                               DOC.DOC_DT_CADASTRO,
                                               DOC.DOC_CO_NUMERO,
                                               USU.USU_NO_USERNAME,
                                               DOC_CO_CADASTRANTE,
                                               DECODE( DOC_IN_CADASTRANTE, 
                                                           'CSL', 'Central de Solucoes',
                                                           'INT', 'Devolucao',
                                                           'DEV', 'Devolucao',
                                                           'GER', 'Gerente',
                                                           'TRA', 'Transportador',
                                                           'POL', 'Transportador',
                                                           'REP', 'Representante',
                                                           'CLI', 'Cliente',
                                                           'CON', 'Consultor',
                                                           DOC_IN_CADASTRANTE
                                               ) AREA_CADASTRANTE,
                                               TOC.TOC_NO_DESCRICAO,
                                               CED.CED_NO_CENTRO,
                                               NVL(DOC.DOC_VR_VALOR, 0) DOC_VR_VALOR
                                      FROM  DEV_OCORRENCIA DOC
                                      INNER JOIN CAL_TIPO_OCORRENCIA TOC ON TOC.TOC_CO_NUMERO = DOC.TOC_CO_NUMERO AND TOC.DEP_CO_NUMERO = 29 AND TOC.SLA_CO_NUMERO = 1
                                      LEFT  JOIN GLOBAL.GLB_CENTRO_DISTRIBUICAO CED ON CED.CED_CO_NUMERO = DOC.CED_CO_NUMERO
                                      LEFT JOIN GLOBAL.GLB_USUARIO USU ON DOC.DOC_CO_CADASTRANTE = USU.USU_CO_NUMERO 
                                      WHERE DOC.DOC_DT_CADASTRO BETWEEN TO_DATE ('{$dataInicial} 00:00:00', 'DD/MM/YYYY HH24:MI:SS') AND  TO_DATE ('{$dataFinal} 23:59:59', 'DD/MM/YYYY HH24:MI:SS')";

        if ($protocolo) {
            $sql .=" AND DOC.DOC_CO_NUMERO = {$protocolo}";
        }

        if ($area) {
            $sql .=" AND DOC.DOC_IN_CADASTRANTE in ({$area})";
        }

        if ($ObjUsuario->getUsuarioTipo() == "CLI") {

            $sql .=" AND DOC.CLI_CO_NUMERO = {$ObjUsuario->getObjCliente()->getClienteCodigo()}";
        } else if ($ObjUsuario->getUsuarioTipo() == "VEN") {
            
            $VendedorUsuario = $ObjUsuario->getObjVendedor();

            $sql .= " AND DOC.CLI_CO_NUMERO IN (
                        SELECT DISTINCT CLI.CLI_CO_NUMERO
                         FROM GLOBAL.GLB_CLIENTE_VENDEDOR CVEN 
                         INNER JOIN GLOBAL.GLB_VENDEDOR VEN ON VEN.VEN_CO_NUMERO = CVEN.VEN_CO_NUMERO
                         INNER JOIN GLOBAL.GLB_CLIENTE  CLI ON CLI.CLI_CO_NUMERO = CVEN.CLI_CO_NUMERO
                         WHERE 
                               CVEN.CLV_IN_STATUS IS NULL ";

            if (($VendedorUsuario[0]->getTipo() == "V") || ($VendedorUsuario[0]->getTipo() == "N")) {
                $sql .= " AND VEN.VEN_CO_SUPERVISOR = {$VendedorUsuario[0]->getCodigo()} ";
            } else {
                $sql .= " AND VEN.VEN_CO_NUMERO = {$VendedorUsuario[0]->getCodigo()} ";
            }
            
            $sql .= ")";
        }

        $sql.=" ORDER BY DOC.DOC_DT_CADASTRO";

        //		echo "<pre>".$sql."</pre>"; //die();// teste geovanni

        if ($result = $this->conn->execSql($sql)) {
            return $result;
        }
        return false;
    }

    public function relatorioContadorABertas($dataInicial, $dataFinal, $protocolo = NULL, $area = NULL, GlbUsuario $ObjUsuario) {

        $sql = "SELECT                COUNT(*)    QTD_ABERTOS     
                                      FROM  DEV_OCORRENCIA DOC
                                      INNER JOIN CAL_TIPO_OCORRENCIA TOC ON TOC.TOC_CO_NUMERO = DOC.TOC_CO_NUMERO AND TOC.DEP_CO_NUMERO = 29 AND TOC.SLA_CO_NUMERO = 1
                                      LEFT  JOIN GLOBAL.GLB_CENTRO_DISTRIBUICAO CED ON CED.CED_CO_NUMERO = DOC.CED_CO_NUMERO
                                      LEFT JOIN GLOBAL.GLB_USUARIO USU ON DOC.DOC_CO_CADASTRANTE = USU.USU_CO_NUMERO 
                                      WHERE DOC.DOC_DT_CADASTRO BETWEEN TO_DATE ('{$dataInicial} 00:00:00', 'DD/MM/YYYY HH24:MI:SS') AND  TO_DATE ('{$dataFinal} 23:59:59', 'DD/MM/YYYY HH24:MI:SS')";

        if ($protocolo) {
            $sql .=" AND DOC.DOC_CO_NUMERO = {$protocolo}";
        }

        if ($area) {
            $sql .=" AND DOC.DOC_IN_CADASTRANTE in ({$area})";
        }
        
        if ($ObjUsuario->getUsuarioTipo() == "CLI") {

            $sql .=" AND DOC.CLI_CO_NUMERO = {$ObjUsuario->getObjCliente()->getClienteCodigo()}";
        } else if ($ObjUsuario->getUsuarioTipo() == "VEN") {
            
            $VendedorUsuario = $ObjUsuario->getObjVendedor();

            $sql .= " AND DOC.CLI_CO_NUMERO IN (
                        SELECT DISTINCT CLI.CLI_CO_NUMERO
                         FROM GLOBAL.GLB_CLIENTE_VENDEDOR CVEN 
                         INNER JOIN GLOBAL.GLB_VENDEDOR VEN ON VEN.VEN_CO_NUMERO = CVEN.VEN_CO_NUMERO
                         INNER JOIN GLOBAL.GLB_CLIENTE  CLI ON CLI.CLI_CO_NUMERO = CVEN.CLI_CO_NUMERO
                         WHERE 
                               CVEN.CLV_IN_STATUS IS NULL ";

            if (($VendedorUsuario[0]->getTipo() == "V") || ($VendedorUsuario[0]->getTipo() == "N")) {
                $sql .= " AND VEN.VEN_CO_SUPERVISOR = {$VendedorUsuario[0]->getCodigo()} ";
            } else {
                $sql .= " AND VEN.VEN_CO_NUMERO = {$VendedorUsuario[0]->getCodigo()} ";
            }
            
            $sql .= ")";
        }

        //		echo "<pre>".$sql."</pre>"; //die();// teste geovanni

        if ($retorno = $this->conn->execSql($sql)) {
            $dados = oci_fetch_object($retorno);
            $qtdOcorrencia = $dados->QTD_ABERTOS;
        } else {
            $qtdOcorrencia = 0;
        }

        $totalizador[] = $qtdOcorrencia;

        /* Ocorrencias aprovadas automaticamente */

        $sql = "SELECT                COUNT(*)    QTD_AUTO    
                                      FROM  DEV_OCORRENCIA DOC
                                      INNER JOIN CAL_TIPO_OCORRENCIA TOC ON TOC.TOC_CO_NUMERO = DOC.TOC_CO_NUMERO AND TOC.DEP_CO_NUMERO = 29 AND TOC.SLA_CO_NUMERO = 1
                                      LEFT  JOIN GLOBAL.GLB_CENTRO_DISTRIBUICAO CED ON CED.CED_CO_NUMERO = DOC.CED_CO_NUMERO
                                      LEFT JOIN GLOBAL.GLB_USUARIO USU ON DOC.DOC_CO_CADASTRANTE = USU.USU_CO_NUMERO 
                                      WHERE DOC.DOC_DT_CADASTRO BETWEEN TO_DATE ('{$dataInicial} 00:00:00', 'DD/MM/YYYY HH24:MI:SS') AND  TO_DATE ('{$dataFinal} 23:59:59', 'DD/MM/YYYY HH24:MI:SS')
                                      AND   DOC.DEV_IN_AUTOMATICO = 1
                                      
                                    ";

        if ($protocolo) {
            $sql .=" AND DOC.DOC_CO_NUMERO = {$protocolo}";
        }

        if ($area) {
            $sql .=" AND DOC.DOC_IN_CADASTRANTE in ({$area})";
        }
        
        if ($ObjUsuario->getUsuarioTipo() == "CLI") {

            $sql .=" AND DOC.CLI_CO_NUMERO = {$ObjUsuario->getObjCliente()->getClienteCodigo()}";
        } else if ($ObjUsuario->getUsuarioTipo() == "VEN") {
            
            $VendedorUsuario = $ObjUsuario->getObjVendedor();

            $sql .= " AND DOC.CLI_CO_NUMERO IN (
                        SELECT DISTINCT CLI.CLI_CO_NUMERO
                         FROM GLOBAL.GLB_CLIENTE_VENDEDOR CVEN 
                         INNER JOIN GLOBAL.GLB_VENDEDOR VEN ON VEN.VEN_CO_NUMERO = CVEN.VEN_CO_NUMERO
                         INNER JOIN GLOBAL.GLB_CLIENTE  CLI ON CLI.CLI_CO_NUMERO = CVEN.CLI_CO_NUMERO
                         WHERE 
                               CVEN.CLV_IN_STATUS IS NULL ";

            if (($VendedorUsuario[0]->getTipo() == "V") || ($VendedorUsuario[0]->getTipo() == "N")) {
                $sql .= " AND VEN.VEN_CO_SUPERVISOR = {$VendedorUsuario[0]->getCodigo()} ";
            } else {
                $sql .= " AND VEN.VEN_CO_NUMERO = {$VendedorUsuario[0]->getCodigo()} ";
            }
            
            $sql .= ")";
        }

        //		echo "<pre>".$sql."</pre>"; //die();// teste geovanni

        if ($retorno = $this->conn->execSql($sql)) {
            $dados = oci_fetch_object($retorno);
            $qtdAuto = $dados->QTD_AUTO;
        } else {
            $qtdAuto = 0;
        }

        $totalizador[] = $qtdAuto;

        /* Ocorrencias aprovadas automaticamente */

        $sql = "SELECT                COUNT(*)    QTD_AUTOVALOR    
                                      FROM  DEV_OCORRENCIA DOC
                                      INNER JOIN CAL_TIPO_OCORRENCIA TOC ON TOC.TOC_CO_NUMERO = DOC.TOC_CO_NUMERO AND TOC.DEP_CO_NUMERO = 29 AND TOC.SLA_CO_NUMERO = 1
                                      LEFT  JOIN GLOBAL.GLB_CENTRO_DISTRIBUICAO CED ON CED.CED_CO_NUMERO = DOC.CED_CO_NUMERO
                                      LEFT JOIN GLOBAL.GLB_USUARIO USU ON DOC.DOC_CO_CADASTRANTE = USU.USU_CO_NUMERO 
                                      WHERE DOC.DOC_DT_CADASTRO BETWEEN TO_DATE ('{$dataInicial} 00:00:00', 'DD/MM/YYYY HH24:MI:SS') AND  TO_DATE ('{$dataFinal} 23:59:59', 'DD/MM/YYYY HH24:MI:SS')
                                      AND   DOC.DEV_IN_AUTOMATICO = 2
                                      
                                    ";

        if ($protocolo) {
            $sql .=" AND DOC.DOC_CO_NUMERO = {$protocolo}";
        }

        if ($area) {
            $sql .=" AND DOC.DOC_IN_CADASTRANTE in ({$area})";
        }
        
        if ($ObjUsuario->getUsuarioTipo() == "CLI") {

            $sql .=" AND DOC.CLI_CO_NUMERO = {$ObjUsuario->getObjCliente()->getClienteCodigo()}";
        } else if ($ObjUsuario->getUsuarioTipo() == "VEN") {
            
            $VendedorUsuario = $ObjUsuario->getObjVendedor();

            $sql .= " AND DOC.CLI_CO_NUMERO IN (
                        SELECT DISTINCT CLI.CLI_CO_NUMERO
                         FROM GLOBAL.GLB_CLIENTE_VENDEDOR CVEN 
                         INNER JOIN GLOBAL.GLB_VENDEDOR VEN ON VEN.VEN_CO_NUMERO = CVEN.VEN_CO_NUMERO
                         INNER JOIN GLOBAL.GLB_CLIENTE  CLI ON CLI.CLI_CO_NUMERO = CVEN.CLI_CO_NUMERO
                         WHERE 
                               CVEN.CLV_IN_STATUS IS NULL ";

            if (($VendedorUsuario[0]->getTipo() == "V") || ($VendedorUsuario[0]->getTipo() == "N")) {
                $sql .= " AND VEN.VEN_CO_SUPERVISOR = {$VendedorUsuario[0]->getCodigo()} ";
            } else {
                $sql .= " AND VEN.VEN_CO_NUMERO = {$VendedorUsuario[0]->getCodigo()} ";
            }
            
            $sql .= ")";
        }
        

        //		echo "<pre>".$sql."</pre>"; //die();// teste geovanni

        if ($retorno = $this->conn->execSql($sql)) {
            $dados = oci_fetch_object($retorno);
            $qtdAutoValor = $dados->QTD_AUTOVALOR;
        } else {
            $qtdAutoValor = 0;
        }

        $totalizador[] = $qtdAutoValor;

        return $totalizador;
    }

    public function relatorioBaixaDevolucaoFiscal($data_inicio, $data_final, $protocolo = NULL, $cedis = NULL, $passo = NULL) {
        $sql = "SELECT * FROM ( 
					    SELECT 
						      'DEVOLUCAO' PASSO, 
						      USU.USU_NO_USERNAME usuario, 
						      to_char(dev.doc_dt_cadastro,'DD/MM/YYYY') data_cadastro, 
						      to_char(PAS.DOP_DT_SAIDA,'DD/MM/YYYY') data_passo,
						      dev.doc_nu_devserie ||' '|| dev.doc_nu_devnota nota_devolucao, 
						      nto.nto_nu_serie || ' '|| nto.nto_nu_nota nota_origem, 
						      PAS.dop_qt_volume volume,
						      cli.red_co_numero , 
						      cli.cli_co_numero,
                                                      dev.doc_co_numero as ocorrencia,
                                                      DEV.DOC_VR_VALOR,
                                                      CED.CED_NO_CENTRO,
                                                      PAS.DPA_CO_NUMERO,
                                                      PAS.DOP_IN_APROVADO,
                                                      ced.CED_CO_NUMERO
						    FROM DEV_OCORRENCIA DEV
                                                    INNER JOIN DEV_OCORRENCIA_PASSO PAS ON PAS.DOC_CO_NUMERO = DEV.DOC_CO_NUMERO AND PAS.DPA_CO_NUMERO = 4 AND PAS.DOP_IN_APROVADO = 1
                                                    INNER JOIN GLOBAL.GLB_USUARIO USU ON USU.USU_CO_NUMERO = PAS.USU_CO_NUMERO
						    INNER JOIN dev_notaorigem nto ON nto.doc_co_numero = dev.doc_co_numero 
						    INNER JOIN GLOBAL.glb_cliente cli ON cli.cli_co_numero = dev.cli_co_numero 
						    INNER JOIN GLOBAL.glb_rede_farmacia red ON red.red_co_numero = cli.red_co_numero
                                                    LEFT  JOIN GLOBAL.GLB_CENTRO_DISTRIBUICAO  CED ON CED.CED_CO_NUMERO = DEV.CED_CO_NUMERO
						    WHERE PAS.DOP_DT_SAIDA BETWEEN  to_date('$data_inicio 00:00:00', 'DD/MM/YYYY HH24:MI:SS') AND to_date('$data_final 23:59:59', 'DD/MM/YYYY HH24:MI:SS')
						    UNION ALL 
						    
						    SELECT 
						      'FISCAL' PASSO,
						      USU.USU_NO_USERNAME usuario, 
						      TO_CHAR(dev.doc_dt_cadastro,'DD/MM/YYYY') data_cadastro, 
						      TO_CHAR(PAS.DOP_DT_SAIDA,'DD/MM/YYYY') data_passo,
						      dev.doc_nu_devserie ||' '|| dev.doc_nu_devnota nota_devolucao, 
						      nto.nto_nu_serie || ' '|| nto.nto_nu_nota nota_origem, 
						       PAS.dop_qt_volume volume,
						      cli.red_co_numero , 
						      cli.cli_co_numero,
                                                      dev.doc_co_numero as ocorrencia,
                                                      DEV.DOC_VR_VALOR,
                                                      CED.CED_NO_CENTRO,
                                                      PAS.DPA_CO_NUMERO,
                                                      PAS.DOP_IN_APROVADO,
                                                      ced.CED_CO_NUMERO
						    FROM DEV_OCORRENCIA DEV
                                                    INNER JOIN DEV_OCORRENCIA_PASSO PAS ON PAS.DOC_CO_NUMERO = DEV.DOC_CO_NUMERO AND PAS.DPA_CO_NUMERO = 5 AND PAS.DOP_IN_APROVADO = 1
                                                    INNER JOIN GLOBAL.GLB_USUARIO USU ON USU.USU_CO_NUMERO = PAS.USU_CO_NUMERO
						    INNER JOIN dev_notaorigem nto ON nto.doc_co_numero = dev.doc_co_numero 
						    INNER JOIN GLOBAL.glb_cliente cli ON cli.cli_co_numero = dev.cli_co_numero 
						    INNER JOIN GLOBAL.glb_rede_farmacia red ON red.red_co_numero = cli.red_co_numero
                                                    LEFT  JOIN GLOBAL.GLB_CENTRO_DISTRIBUICAO  CED ON CED.CED_CO_NUMERO = DEV.CED_CO_NUMERO

						    WHERE PAS.DOP_DT_SAIDA BETWEEN  to_date('$data_inicio 00:00:00', 'DD/MM/YYYY HH24:MI:SS') AND to_date('$data_final 23:59:59', 'DD/MM/YYYY HH24:MI:SS')
					)
					WHERE 1=1 /*to_date(data_passo,'DD/MM/YYYY') BETWEEN to_date('$data_inicio 00:00:00', 'DD/MM/YYYY HH24:MI:SS') AND to_date('$data_final 23:59:59', 'DD/MM/YYYY HH24:MI:SS') */";

        if ($protocolo) {
            $sql .=" AND DOC_CO_NUMERO = {$protocolo}";
        }

        //        if ($area) {
        //            $sql .=" AND DEV.DOC_IN_CADASTRANTE in ({$area})";
        //        }
        //        
        //        
        if ($passo) {
            $sql .=" AND DPA_CO_NUMERO = {$passo}
                     AND DOP_IN_APROVADO = 1
            "
            ;
        }


        if (($cedis) && ($cedis != "TOD")) {
            $sql .= " AND CED_CO_NUMERO = {$cedis}";
        }


        $sql .=" ORDER BY data_passo,data_cadastro ";
        //        echo "<pre>".$sql."</pre>";
        $result = $this->conn->execSql($sql);
        return $result;
    }

    public function relatorioOcorrenciaReprovada($dataInicial, $dataFinal, $dataInicialRep = NULL, $dataFinalRep = NULL, $protocolo = NULL, $cliente = NULL, $rede = NULL, $passoOcorrencia = NULL, $motivoReprova = NULL, $rota = NULL, $cedis = NULL, GlbUsuario $ObjUsuario) {
        $status = 12; //codigo do status - 12	REPROVADO

        $sql = "
                    SELECT
                            OCO.DOC_CO_NUMERO OCORRENCIA,
                            TO_CHAR(OCO.DOC_DT_CADASTRO,'dd/mm/yyyy') DATA_CADASTRO,
                            OCO.ROT_CO_NUMERO AS ROTA, 
                            OCO.DOC_IN_TIPO COD_TIPO,
                            TID.TID_CO_DESCRICAO,
                            NTO.NTO_NU_SERIE || ' ' || NTO.NTO_NU_NOTA NOTA_ORIGEM,
                            OCO.DOC_NU_DEVSERIE || ' '|| OCO.DOC_NU_DEVNOTA NOTA_DEVOLUCAO ,
                            DECODE(NTV.INDICADOR, 'F', 'FARMA', 'H', 'HOSPITALAR', 'A', 'ALIMENTAR', 'O', 'DIST DIRETA', NTV.INDICADOR) RPR_IN_DIVISAO,
                            CED.CED_NO_CENTRO,
                            OCO.DOC_VR_VALOR VALOR,
                            OCO.DOC_QT_PRODUTO VOLUME,
                            OCO.DOC_IN_FINALIZADO,
                            CLI.CLI_CO_NUMERO CODIGO_CLIENTE, 
                            CLI.CLI_NO_RAZAO_SOCIAL CLIENTE,
                            DPA.DPA_NO_DESCRICAO NOME_PASSO,
                            MRE.MRE_NO_DESCRICAO,
                            TO_CHAR(LAP.LAP_DT_ACAO, 'DD/MM/YYYY') LAP_DT_ACAO
                    FROM DEV_OCORRENCIA OCO
                    INNER JOIN DEV_NOTAORIGEM                 NTO ON NTO.DOC_CO_NUMERO = OCO.DOC_CO_NUMERO
                    INNER JOIN USERVIMED.NOTAVENDA            NTV ON NTV.SERIE = NTO.NTO_NU_SERIE AND NTV.NUMNOTA = NTO.NTO_NU_NOTA AND NTV.CDCLIENTE = OCO.CLI_CO_NUMERO AND NTV.DTMOV = NTO.NTO_DT_NOTA
                    INNER JOIN GLOBAL.GLB_CLIENTE             CLI ON CLI.CLI_CO_NUMERO = OCO.CLI_CO_NUMERO
                    INNER JOIN DEV_PASSO                      DPA ON DPA.DPA_CO_NUMERO = OCO.DPA_CO_NUMERO
                    INNER JOIN DEV_TIPO_DEVOLUCAO             TID ON TID.TID_CO_NUMERO = OCO.DOC_IN_TIPO
                    LEFT  JOIN GLOBAL.GLB_CENTRO_DISTRIBUICAO CED ON CED.CED_CO_NUMERO = OCO.CED_CO_NUMERO
                    INNER JOIN DEV_LOG_ACAO_PASSO             LAP ON LAP.DOC_CO_NUMERO = OCO.DOC_CO_NUMERO AND LAP.DPA_CO_NUMERO = OCO.DPA_CO_NUMERO
                    INNER JOIN DEV_MOTIVO_REPROVA             MRE ON MRE.MRE_CO_NUMERO = LAP.MRE_CO_NUMERO
                    WHERE OCO.DST_CO_NUMERO = {$status}
                    AND   OCO.DOC_DT_CADASTRO BETWEEN TO_DATE ('{$dataInicial} 00:00:00', 'DD/MM/YYYY HH24:MI:SS') AND  TO_DATE ('{$dataFinal} 23:59:59', 'DD/MM/YYYY HH24:MI:SS')";

        if (($dataInicialRep != NULL) && ($dataFinalRep != NULL)) {
            $sql .=" AND  LAP.LAP_DT_ACAO BETWEEN TO_DATE ('{$dataInicialRep} 00:00:00', 'DD/MM/YYYY HH24:MI:SS') AND  TO_DATE ('{$dataFinalRep} 23:59:59', 'DD/MM/YYYY HH24:MI:SS')";
        }

        if ($protocolo) {
            $sql .=" AND OCO.DOC_CO_NUMERO = $protocolo";
        }

        if ($cliente) {
            $sql .=" AND cli.cli_co_numero = $cliente";
        }

        if ($rede) {
            $sql .=" AND cli.red_co_numero = $rede";
        }

        if (($passoOcorrencia) && ($passoOcorrencia != "TOD")) {
            $sql .=" AND OCO.DPA_CO_NUMERO = $passoOcorrencia";
        }

        if (($motivoReprova) && ($motivoReprova != "TOD")) {
            $sql .=" AND MRE.MRE_CO_NUMERO = $motivoReprova";
        }

        if (($cedis) && ($cedis != "TOD")) {
            $sql .= " AND OCO.CED_CO_NUMERO = {$cedis}";
        }

        if (($rota) && ($rota != "TOD")) {
            $sql .=" AND OCO.ROT_CO_NUMERO = $rota";
        }
        
        if ($ObjUsuario->getUsuarioTipo() == "CLI") {

            $sql .=" AND OCO.CLI_CO_NUMERO = {$ObjUsuario->getObjCliente()->getClienteCodigo()}";
        } else if ($ObjUsuario->getUsuarioTipo() == "VEN") {
            
            $VendedorUsuario = $ObjUsuario->getObjVendedor();

            $sql .= " AND OCO.CLI_CO_NUMERO IN (
                        SELECT DISTINCT CLI.CLI_CO_NUMERO
                         FROM GLOBAL.GLB_CLIENTE_VENDEDOR CVEN 
                         INNER JOIN GLOBAL.GLB_VENDEDOR VEN ON VEN.VEN_CO_NUMERO = CVEN.VEN_CO_NUMERO
                         INNER JOIN GLOBAL.GLB_CLIENTE  CLI ON CLI.CLI_CO_NUMERO = CVEN.CLI_CO_NUMERO
                         WHERE 
                               CVEN.CLV_IN_STATUS IS NULL ";

            if (($VendedorUsuario[0]->getTipo() == "V") || ($VendedorUsuario[0]->getTipo() == "N")) {
                $sql .= " AND VEN.VEN_CO_SUPERVISOR = {$VendedorUsuario[0]->getCodigo()} ";
            } else {
                $sql .= " AND VEN.VEN_CO_NUMERO = {$VendedorUsuario[0]->getCodigo()} ";
            }
            
            $sql .= ")";
        } else if ($ObjUsuario->getUsuarioTipo() == "TRA") {
            
            $sql .= " AND OCO.ROT_CO_NUMERO IN (
                        SELECT DISTINCT ROT_CO_NUMERO FROM USUARIO_ROTA WHERE USU_CO_NUMERO = {$ObjUsuario->getUsuarioCodigo()}
                        )";
            
        }


        $sql.=" ORDER BY OCO.DOC_DT_CADASTRO";

        //		echo "<pre>".$sql."</pre>"; //die();// teste geovanni

        if ($result = $this->conn->execSql($sql)) {
            return $result;
        }
        return false;
    }

    public function relatorioClienteEspecial($dataInicial, $dataFinal, $cliente = NULL, $rede = NULL) {

        $sql = "
                    SELECT TO_CHAR(ESP.ESP_DT_LIBERACAO,'DD/MM/YYYY') DT_LIBERACAO,
                           ESP.ESP_DT_LIBERACAO,
                           USU.USU_NO_USERNAME,
                           CLI.RED_CO_NUMERO,
                           CLI.CLI_CO_NUMERO,
                           CLI.CLI_NO_RAZAO_SOCIAL,
						   CLI.CLI_NU_CNPJ_CPF,
                           EQP.VEE_NU_EQUIPE,
                           EQP.VEE_NO_EQUIPE GERENTE_CONTAS,
                           EQP.VEE_NU_REGIONAL,
                           EQP.VEE_NO_REGIONAL GERENTE_REGIONAL,
                           EQP.VEE_NU_DIVISIONAL,
                           EQP.VEE_NO_DIVISIONAL GERENTE_DIVISIONAL,
						   ESP.ESP_NU_DIAS_LIBERACAONF
                    FROM   DEV_CLIENTE_ESPECIAL ESP
                    INNER  JOIN GLOBAL.GLB_CLIENTE CLI ON CLI.CLI_CO_NUMERO = ESP.ESP_CO_NUMERO
                    INNER  JOIN USERVIMED.VIEW_ESTRUTURA_EQUIPE EQP ON EQP.VEE_NU_SETOR = CLI.VEN_CO_NUMERO AND EQP.VEE_NU_MES = 1 AND EQP.VEE_NU_ANO = 2016
                    INNER  JOIN GLOBAL.GLB_USUARIO USU ON USU.USU_CO_NUMERO = ESP.USU_CO_NUMERO
                     ";

        if (($dataInicial != NULL) && ($dataFinal != NULL)) {
            $sql .=" AND  ESP.ESP_DT_LIBERACAO BETWEEN TO_DATE ('{$dataInicial} 00:00:00', 'DD/MM/YYYY HH24:MI:SS') AND  TO_DATE ('{$dataFinal} 23:59:59', 'DD/MM/YYYY HH24:MI:SS')";
        }

        if ($cliente) {
            $sql .=" AND cli.cli_co_numero = $cliente";
        }

        if ($rede) {
            $sql .=" AND cli.red_co_numero = $rede";
        }

        $sql.=" ORDER BY ESP.ESP_DT_LIBERACAO";

        //		echo "<pre>".$sql."</pre>"; //die();// teste geovanni

        if ($result = $this->conn->execSql($sql)) {
            return $result;
        }
        return false;
    }

    public function relatorioConferenciaColaborador($dataInicial = NULL, $dataFinal = NULL, $codOcorrencia = NULL, $conferente = NULL, $cedis = NULL) {

        $sql = "
                                SELECT OCO.DOC_CO_NUMERO
                                    ,OCO.DOC_DT_CADASTRO
                                    ,TO_CHAR(OCO.DOC_DT_CADASTRO ,'DD/MM/YYYY') DATA_EXIBIR
                                    ,TO_CHAR(OCO.DOC_DT_CONFERENCIA,'DD/MM/YYYY') DOC_DT_CONFERENCIA
                                    ,CON.COL_CO_CRACHA
                                    ,COL.COL_NO_COLABORADOR
                                    ,COUNT(NTO.DOC_CO_NUMERO) QTD_NOTAS
                                    ,CON.DOC_QT_VOLUME
                                    ,OCO.TOC_CO_NUMERO
                                    ,TOC.TOC_NO_DESCRICAO
                                    ,OCO.DOC_VR_VALOR
                                    ,CED.CED_NO_CENTRO
                                FROM   DEV_OCORRENCIA OCO
                                INNER JOIN CAL_TIPO_OCORRENCIA    TOC ON TOC.TOC_CO_NUMERO = OCO.TOC_CO_NUMERO
                                INNER JOIN DEV_NOTAORIGEM         NTO ON NTO.DOC_CO_NUMERO = OCO.DOC_CO_NUMERO
                                INNER JOIN DEV_OCORRENCIA_CONFERENTE CON ON CON.DOC_CO_NUMERO = OCO.DOC_CO_NUMERO
                                
                                LEFT  JOIN GLOBAL.GLB_COLABORADOR COL ON COL.EMP_CO_NUMERO = CON.EMP_CO_NUMERO 
                                                                    AND COL.ETB_CO_NUMERO = CON.ETB_CO_NUMERO 
                                                                    AND COL.COL_CO_CRACHA = CON.COL_CO_CRACHA
                                LEFT  JOIN GLOBAL.GLB_CENTRO_DISTRIBUICAO CED ON CED.CED_CO_NUMERO = OCO.CED_CO_NUMERO
                                WHERE OCO.DOC_DT_CONFERENCIA  IS NOT NULL
                                AND   TOC.TOC_IN_STATUS = 1
                     ";

        if (($dataInicial != NULL) && ($dataFinal != NULL)) {
            $sql .=" AND  OCO.DOC_DT_CONFERENCIA BETWEEN TO_DATE ('{$dataInicial} 00:00:00', 'DD/MM/YYYY HH24:MI:SS') AND  TO_DATE ('{$dataFinal} 23:59:59', 'DD/MM/YYYY HH24:MI:SS')";
        }

        if ($codOcorrencia) {
            $sql .=" AND OCO.DOC_CO_NUMERO = $codOcorrencia";
        }

        if ($conferente != 'TOD') {


            $conferente = explode("_", $conferente);
            $sql .= "
                                    AND CON.EMP_CO_NUMERO = {$conferente[0]}
                                    AND CON.ETB_CO_NUMERO = {$conferente[1]}
                                    AND CON.COL_CO_CRACHA = {$conferente[2]}

                                ";
        }

        if (($cedis) && ($cedis != "TOD")) {
            $sql .= " AND OCO.CED_CO_NUMERO = {$cedis}";
        }

        $sql.=" 
                               GROUP BY OCO.DOC_CO_NUMERO,
                                        OCO.DOC_DT_CADASTRO, 
                                        OCO.DOC_DT_CONFERENCIA, 
                                        CON.COL_CO_CRACHA, 
                                        COL.COL_NO_COLABORADOR, 
                                        CON.DOC_QT_VOLUME, 
                                        OCO.TOC_CO_NUMERO, 
                                        TOC.TOC_NO_DESCRICAO,
                                        OCO.DOC_VR_VALOR,
                                        CED.CED_NO_CENTRO
                                ORDER BY OCO.DOC_DT_CADASTRO,   
                                         COL.COL_NO_COLABORADOR
            ";

        //		echo "<pre>".$sql."</pre>"; //die();// teste geovanni

        if ($result = $this->conn->execSql($sql)) {
            return $result;
        }
        return false;
    }

    public function relatorioNotaLiberada($dtInicial, $dtFinal, $setor = null, $filtro_multa = null) {

        $sql = "    SELECT  LBN.RPR_CO_NUMERO,
                                REP.RPR_NO_LABEL,
                                LBN.CLI_CO_NUMERO,
                                LBN.NTO_NU_SERIE,
                                LBN.NTO_NU_NOTA,
                                TO_CHAR(LBN.NTO_DT_NOTA,'DD/MM/YYYY')  NTO_DT_NOTA,
                                LBN.LBN_TX_DESCRICAO,
                                LBN.LBN_IN_MULTA,
                                LBN.LBN_DT_LIBERACAO,
                                TO_CHAR(LBN.LBN_DT_LIBERACAO,'DD/MM/YYYY') DATA_LIBERACAO,
									U.USU_NO_USERNAME,
									TOC.TOC_NO_DESCRICAO								
                        FROM DEV_LIBERACAONOTA LBN
                        INNER JOIN USERVIMED.SVD_REPRESENTANTE REP ON REP.RPR_CO_NUMERO = LBN.RPR_CO_NUMERO 
						  INNER JOIN GLOBAL.GLB_USUARIO U ON U.USU_CO_NUMERO = LBN.USU_CO_NUMERO
						  LEFT JOIN CAL_TIPO_OCORRENCIA TOC ON TOC.TOC_CO_NUMERO = LBN.TOC_CO_NUMERO AND TOC.DEP_CO_NUMERO = 29              						
                        WHERE  LBN.NTO_NU_SERIE   IS NOT NULL
                        AND    LBN.NTO_NU_NOTA    IS NOT NULL
                        AND    LBN.LBN_IN_STATUS  IS NULL
                        ";


        if ($dtInicial && $dtFinal) {
            $sql.=" AND LBN.LBN_DT_LIBERACAO BETWEEN TO_DATE('{$dtInicial} 00:00:00', 'DD/MM/YYYY HH24:MI:SS') AND TO_DATE('{$dtFinal} 23:59:59', 'DD/MM/YYYY HH24:MI:SS') ";
        }
        if ($setor) {
            $sql .= " AND    LBN.RPR_CO_NUMERO   = {$setor} ";
        }
        if ($filtro_multa != 'TOD') {
            $sql .= " AND    LBN.LBN_IN_MULTA    = {$filtro_multa} ";
        }

        $sql .= "order by LBN.RPR_CO_NUMERO, LBN.LBN_DT_LIBERACAO";
        //          echo "<pre>".$sql."</pre>"; die();	

        if ($result = $this->conn->execSql($sql)) {
            return $result;
        }
        return false;
    }

    // adicionado 06/11/201/ - Luiz

    public function adicionaDadoLeadTime(){
        $sql = "select sum(data_retorno - data_inicio) from hist_data_ocorrencia_status";

        if ($result = $this->conn->execSql($sql)) {
            return $result;
        }
        return false;
    }

    public function relatorioColetaAprovacao($dtInicial, $dtFinal, $codOcorrencia = null, $cliente = null , $rot_co_numero = null, $procedimento = null, $tresp = null, $leadtime = null, $status = null, $ced_co_numero = null, $codDivisao = null, GlbUsuario $ObjUsuario  ) {

        $sql = "    SELECT 
                        CLI.CLI_NO_RAZAO_SOCIAL ,                       
                        DEV.CLI_CO_NUMERO ,                
                        DEV.DOC_CO_NUMERO ,
                        TO_CHAR(DEV.DOC_DT_CADASTRO,'dd/mm/yyyy') DOC_DT_CADASTRO ,
                        DEV.DOC_DT_CADASTRO TESTE,                        
                        DEV.DOC_NU_DEVSERIE ,                        
                        DEV.DOC_NU_DEVNOTA ,
                        TO_CHAR(DEV.DOC_DT_DEVNOTA,'dd/mm/yyyy') DOC_DT_DEVNOTA ,                        
                        DEV.DOC_VR_VALOR ,                        
                        DEV.DOC_QT_PRODUTO ,                        
                        DEV.DOC_IN_FINALIZADO ,                        
                        DEV.ROT_CO_NUMERO ,                        
                        DEV.DOC_IN_TRANSFERIDO ,                        
                        TO_CHAR(DEV.DOC_DT_TRANSFERENCIA,'dd/mm/yyyy') DOC_DT_TRANSFERENCIA ,                        
                        DEV.TOC_CO_NUMERO ,                      
                        DEV.DOC_TX_DESCRICAO ,
                        DEV.DOC_NO_EMAILCOLETA ,                        
                        TOC.TOC_NO_DESCRICAO ,                      
                        DEV.DOC_IN_TIPO,                        
                        CLI.CLI_ED_LOGRADOURO,                        
                        CLI.CLI_ED_NUMERO,                        
                        CLI.CLI_NO_BAIRRO,                        
                        CLI.CLI_ED_COMPLEMENTO,
                        LOC.LOC_NO_CIDADE,
                        LOC.EST_SG_ESTADO,                        
                        DEV.DOC_NO_CONTATO,                        
                        DEV.DOC_NO_DEPOCONTATO,                        
                        DEV.DOC_IN_CADASTRANTE,
                        USU.USU_NO_USERNAME   
                       ,CLI.CLI_NU_ROTA_TERCEIRIZADA
                       ,TRUNC(TO_DATE(TO_CHAR(SYSDATE, 'DD/MM/YYYY'),'DD/MM/YYYY') - TO_DATE(TO_CHAR(DOS.DOS_DT_ENTRADA, 'DD/MM/YYYY'),'DD/MM/YYYY'))  LEAD_TIME
                       ,DECODE(DEV.DOC_IN_COLETA_ENVIADA , 1 , 'ENTREGUE', 'PENDENTE' ) STATUS
                       ,TO_CHAR(DEV.DOC_DT_COLETA_ENVIADA, 'DD/MM/YYYY') DOC_DT_COLETA_ENVIADA
                       ,DOS.DST_CO_NUMERO
                       ,DECODE(DEV.DEV_CO_DIVISAO, 'F', 'FARMA', 'H', 'HOSPITALAR', 'A', 'ALIMENTAR', 'O', 'DIST DIRETA', DEV.DEV_CO_DIVISAO) DEV_CO_DIVISAO
                       ,CED.CED_NO_CENTRO
                       ,TO_CHAR(DOS.DOS_DT_ENTRADA, 'DD/MM/YYYY') DOS_DT_ENTRADA
                       ,(select sum(HST.data_retorno - HST.data_inicio)from hist_data_ocorrencia_status hst where DEV.DOC_CO_NUMERO = HST.DOC_CO_NUMERO) DATA_LEAD
                      FROM DEV_OCORRENCIA DEV
                    INNER JOIN GLOBAL.GLB_CLIENTE CLI  ON CLI.CLI_CO_NUMERO = DEV.CLI_CO_NUMERO
                    INNER JOIN GLOBAL.GLB_USUARIO USU  ON DEV.DOC_CO_CADASTRANTE = USU.USU_CO_NUMERO
                    LEFT  JOIN GLOBAL.GLB_CENTRO_DISTRIBUICAO CED ON CED.CED_CO_NUMERO = DEV.CED_CO_NUMERO
                    INNER JOIN DEV_OCORRENCIA_STATUS DOS  ON DEV.DOC_CO_NUMERO   = DOS.DOC_CO_NUMERO AND DOS.DST_CO_NUMERO = 5 /*aguardando coleta*/
                    INNER JOIN CAL_TIPO_OCORRENCIA TOC ON TOC.TOC_CO_NUMERO  = DEV.TOC_CO_NUMERO AND TOC.DEP_CO_NUMERO = 29 AND TOC.SLA_CO_NUMERO = 1
                    INNER JOIN GLOBAL.GLB_ROTA ROT     ON ROT.ROT_CO_NUMERO = DEV.ROT_CO_NUMERO                
                    INNER JOIN GLOBAL.GLB_LOCALIDADE LOC ON LOC.LOC_CO_NUMERO = CLI.LOC_CO_NUMERO
                    WHERE DEV.DOC_IN_FINALIZADO IS NULL
                    
                    
                        ";
                    
        if ($ObjUsuario->getUsuarioTipo() == "TRA") {

            $sql .=" AND DEV.ROT_CO_NUMERO IN ( SELECT ROT_CO_NUMERO FROM USUARIO_ROTA WHERE USU_CO_NUMERO = {$ObjUsuario->getUsuarioCodigo()} )";
        } 


        if ($dtInicial && $dtFinal) {
            
            //$sql.=" AND DEV.DOC_DT_CADASTRO  >=  TRUNC(TO_DATE('{$dtInicial}','DD/MM/YYYY')) AND DEV.DOC_DT_CADASTRO  < TRUNC(TO_DATE('{$dtFinal}','DD/MM/YYYY')+1) ";
			$sql.=" AND DOS.DOS_DT_ENTRADA  >=  TRUNC(TO_DATE('{$dtInicial}','DD/MM/YYYY')) AND DOS.DOS_DT_ENTRADA  < TRUNC(TO_DATE('{$dtFinal}','DD/MM/YYYY')+1) ";
        }
        
        if ($codOcorrencia) {
            $sql .=" AND DEV.DOC_CO_NUMERO = $codOcorrencia";
        }
        
        if ($cliente) {
            $sql .=" AND cli.cli_co_numero = $cliente";
        }
        
        if (($rot_co_numero) && ($rot_co_numero != "TOD")) {
            $sql .=" AND DEV.ROT_CO_NUMERO = '$rot_co_numero'";
        }
        
        if (($tresp) && ($tresp != "TOD")) {
            $sql .=" AND TOC.TOC_CO_NUMERO = $tresp";
        }

        if ($leadtime) {
            $sql .=" AND TRUNC(TO_DATE(TO_CHAR(SYSDATE, 'DD/MM/YYYY'),'DD/MM/YYYY') - TO_DATE(TO_CHAR(DOS.DOS_DT_ENTRADA, 'DD/MM/YYYY'),'DD/MM/YYYY')) = $leadtime";
        }
        
        if($status != 'TOD'){
            $sql .= " AND DEV.DOC_IN_COLETA_ENVIADA = {$status}";
        }
        
        if (($ced_co_numero) && ($ced_co_numero != "TOD")) {
            $sql .= " AND DEV.CED_CO_NUMERO = {$ced_co_numero}";
        }
        
        if ($codDivisao) {
            $sql .=" AND DEV.DEV_CO_DIVISAO in ({$codDivisao})";
        }


        $sql .= " ORDER BY LEAD_TIME DESC, CLI.CLI_NU_ROTA_TERCEIRIZADA";
        //          echo "<pre>".$sql."</pre>"; die();	

        if ($result = $this->conn->execSql($sql)) {
            return $result;
        }
        return false;
    }
    
    public function relatorioLiberacaoTransportador($dtInicial, $dtFinal, $ced_co_numero = null, GlbUsuario $ObjUsuario  ) {
        //echo 'date ini: '. $dtInicial . '  ...  date fim: '. $dtFinal;
        $sql = "    SELECT 
                            OCO.DOC_CO_NUMERO
                           ,NTO.NTO_NU_SERIE||' '|| NTO.NTO_NU_NOTA AS NOTA_ORIGEM
                           ,OCO.CLI_CO_NUMERO
                           ,DOP.DOP_QT_VOLUME AS DOC_QT_POLO_SAIDA_PRODUTO
                           ,OCO.DOC_IN_TIPO
                           ,TOC.TOC_NO_DESCRICAO
                           ,ROT.ROT_CO_NUMERO
                           ,OCO.DOC_NU_DEVSERIE ||' '||OCO.DOC_NU_DEVNOTA AS NOTA_DEVOLUCAO
                           ,TO_CHAR(DOP.DOP_DT_SAIDA, 'DD/MM/YYYY') AS DOC_DT_POLO_ENTRADA
                            ,OCO.CED_CO_NUMERO
                            ,CED.CED_NO_CENTRO
                             ,TID.TID_CO_DESCRICAO
                    FROM DEV_OCORRENCIA OCO 
                  INNER JOIN DEV_NOTAORIGEM NTO ON OCO.DOC_CO_NUMERO = NTO.DOC_CO_NUMERO 
                  INNER  JOIN DEV_OCORRENCIA_PASSO DOP ON DOP.DOC_CO_NUMERO = OCO.DOC_CO_NUMERO AND DOP.DPA_CO_NUMERO = 2 AND DOP.DOP_IN_APROVADO = 1 AND DOP.USU_CO_NUMERO = {$ObjUsuario->getUsuarioCodigo()}
                  INNER JOIN GLOBAL.GLB_ROTA ROT ON ROT.ROT_CO_NUMERO = OCO.ROT_CO_NUMERO 
                  INNER JOIN CAL_TIPO_OCORRENCIA TOC ON TOC.TOC_CO_NUMERO = OCO.TOC_CO_NUMERO AND TOC.DEP_CO_NUMERO = 29 AND TOC.SLA_CO_NUMERO = 1
                  LEFT JOIN GLOBAL.GLB_CENTRO_DISTRIBUICAO CED ON CED.CED_CO_NUMERO = OCO.CED_CO_NUMERO
                  INNER JOIN DEV_TIPO_DEVOLUCAO TID ON  OCO.DOC_IN_TIPO = TID.TID_CO_NUMERO
                  WHERE OCO.ROT_CO_NUMERO IN ( SELECT ROT_CO_NUMERO FROM USUARIO_ROTA WHERE USU_CO_NUMERO = {$ObjUsuario->getUsuarioCodigo()} )
                  AND   (OCO.DOC_IN_FINALIZADO IS NULL OR OCO.DOC_IN_FINALIZADO =0 )
                  AND   OCO.DST_CO_NUMERO IN (6,7)

                        ";

        if (($dtInicial != "")  && ($dtFinal != "")) {
            
            $sql.=" 
                    AND dop.DOP_DT_SAIDA BETWEEN TO_DATE ('{$dtInicial} 00:00:00', 'DD/MM/YYYY HH24:MI:SS') AND  TO_DATE ('{$dtFinal} 23:59:59', 'DD/MM/YYYY HH24:MI:SS')";
        }

        if (($ced_co_numero) && ($ced_co_numero != "TOD")) {
            $sql .= " AND oco.CED_CO_NUMERO = {$ced_co_numero}";
        }
        

        $sql .= " ORDER BY oco.DOC_CO_NUMERO";
        //          echo "<pre>".$sql."</pre>"; die();	

        if ($result = $this->conn->execSql($sql)) {
            return $result;
        }
        return false;
    }

	
	
    public function relatorioOcorrenciaLeadTime($dataInicial, $dataFinal, $protocolo = NULL, $cliente = NULL, $rede = NULL, $motivo = NULL, $rota = NULL, $cedis = NULL, $InDivisao=null, $passo=null, $dataInicialPasso=null, $dataFinalPasso=null, GlbUsuario $ObjUsuario) {
        $sql = "SELECT 	
						DPA.DPA_NO_DESCRICAO,
                        DOC.DST_CO_NUMERO,
                        DST.DST_NO_DESCRICAO,
                        DOC.DOC_CO_NUMERO,
                        TO_CHAR(DOC.DOC_DT_CADASTRO, 'DD/MM/YYYY') DATA_CADASTRO,
                        DOC.DOC_DT_CADASTRO,
                        DOC.DOC_CO_CADASTRANTE,
                        USU.USU_NO_USERNAME,
                        DOC.ROT_CO_NUMERO,
						CLI.CLI_NU_ROTA_TERCEIRIZADA,
                        DOC.DOC_IN_TIPO,
                        TID.TID_CO_DESCRICAO,
                        NTO.NTO_NU_SERIE,
                        NTO.NTO_NU_NOTA,
						TO_CHAR(NTO.NTO_DT_NOTA, 'DD/MM/YYYY') NTO_DT_NOTA,
						NTO.NTO_NU_ORIGEM_VENDA,
                        DECODE(NTO.NTO_CO_DIVISAO, 'F', 'FARMA', 'H', 'HOSPITALAR', 'A', 'ALIMENTAR', 'O', 'DIST DIRETA', NTO.NTO_CO_DIVISAO) DIVISAO,
                        DOC.CED_CO_NUMERO CD,
                        CED.CED_NO_CENTRO,
                        DOC.DOC_NU_DEVSERIE,
                        DOC.DOC_NU_DEVNOTA,
						TO_CHAR(DOC.DOC_DT_DEVNOTA, 'DD/MM/YYYY') DOC_DT_DEVNOTA,
                        DOC.DOC_VR_VALOR,
                        DOC.DOC_QT_PRODUTO,
						TO_CHAR(DOC.DOC_DT_DEVRECEBIMENTO, 'DD/MM/YYYY') DOC_DT_DEVRECEBIMENTO,
						TO_CHAR(DOC.DOC_DT_CONFERENCIA, 'DD/MM/YYYY') DOC_DT_CONFERENCIA,
                        CLI.CLI_CO_NUMERO,
                        CLI.CLI_NO_FANTASIA,
                        CLI.RED_CO_NUMERO,
						TOC.TOC_NO_DESCRICAO,
						DOC.USU_CO_NUMERO_GERENTE,
						NV.CDSETOR

                FROM DEV_OCORRENCIA DOC
                INNER JOIN DEV_NOTAORIGEM                 NTO ON NTO.DOC_CO_NUMERO = DOC.DOC_CO_NUMERO
				LEFT JOIN USERVIMED.NOTAVENDA             NV ON NV.SERIE = NTO.NTO_NU_SERIE AND NV.NUMNOTA = NTO.NTO_NU_NOTA AND NV.DTMOV = NTO.NTO_DT_NOTA
                INNER JOIN GLOBAL.GLB_CLIENTE             CLI ON CLI.CLI_CO_NUMERO = DOC.CLI_CO_NUMERO
                INNER  JOIN GLOBAL.GLB_CENTRO_DISTRIBUICAO CED ON CED.CED_CO_NUMERO = DOC.CED_CO_NUMERO
				INNER JOIN DEV_PASSO                      DPA ON DPA.DPA_CO_NUMERO = DOC.DPA_CO_NUMERO
                INNER JOIN DEV_STATUS                     DST ON DST.DST_CO_NUMERO = DOC.DST_CO_NUMERO
                INNER JOIN GLOBAL.GLB_USUARIO             USU ON USU.USU_CO_NUMERO = DOC.DOC_CO_CADASTRANTE
                INNER JOIN DEV_TIPO_DEVOLUCAO             TID ON TID.TID_CO_NUMERO = DOC.DOC_IN_TIPO
				INNER JOIN CAL_TIPO_OCORRENCIA            TOC ON TOC.TOC_CO_NUMERO = DOC.TOC_CO_NUMERO AND TOC.DEP_CO_NUMERO = 29 AND TOC.SLA_CO_NUMERO = 1
				";
		if ($passo != "TOD" and $dataInicialPasso != "" and $dataFinalPasso != ""){		
			$sql .= " INNER JOIN DEV_OCORRENCIA_PASSO            DOP ON DOP.DOC_CO_NUMERO = DOC.DOC_CO_NUMERO 
					AND DOP.DPA_CO_NUMERO = {$passo}
					and DOP.DOP_DT_SAIDA BETWEEN TO_DATE ('{$dataInicialPasso} 00:00:00', 'DD/MM/YYYY HH24:MI:SS') AND  TO_DATE ('{$dataFinalPasso} 23:59:59', 'DD/MM/YYYY HH24:MI:SS')";
		}
		
		if ($dataInicial != "" and $dataFinal != ""){				
			$sql .= " WHERE DOC.DOC_DT_CADASTRO BETWEEN TO_DATE ('{$dataInicial} 00:00:00', 'DD/MM/YYYY HH24:MI:SS') AND  TO_DATE ('{$dataFinal} 23:59:59', 'DD/MM/YYYY HH24:MI:SS')";
		}
		
        if (($motivo) && ($motivo != "TOD")) {
            $sql .=" AND DOC.TOC_CO_NUMERO = {$motivo}";
        }

        if ($protocolo) {
            $sql .=" AND DOC.DOC_CO_NUMERO = $protocolo";
        }

        if ($cliente) {
            $sql .=" AND cli.cli_co_numero = $cliente";
        }
        if ($rede) {
            $sql .=" AND cli.red_co_numero = $rede";
        }

        if (($rota) && ($rota != "TOD")) {
            $sql .=" AND DOC.ROT_CO_NUMERO = $rota";
        }

        if (($cedis) && ($cedis != "TOD")) {
            $sql .= " AND DOC.CED_CO_NUMERO = {$cedis}";
        }
		
        if (($InDivisao) && ($InDivisao != "")) {
			$divisao = str_replace("|","'",$InDivisao);
            $sql .= " AND NTO.NTO_CO_DIVISAO in( {$divisao} )";
        }		
		
        if (($passo) && ($passo != "TOD")) {
            $sql .= " AND DOC.DPA_CO_NUMERO = {$passo}";
        }		
        
        if ($ObjUsuario->getUsuarioTipo() == "CLI") {

            $sql .=" AND DOC.CLI_CO_NUMERO = {$ObjUsuario->getObjCliente()->getClienteCodigo()}";
        } else if ($ObjUsuario->getUsuarioTipo() == "VEN") {
            
            $VendedorUsuario = $ObjUsuario->getObjVendedor();

            $sql .= " AND DOC.CLI_CO_NUMERO IN (
                        SELECT DISTINCT CLI.CLI_CO_NUMERO
                         FROM GLOBAL.GLB_CLIENTE_VENDEDOR CVEN 
                         INNER JOIN GLOBAL.GLB_VENDEDOR VEN ON VEN.VEN_CO_NUMERO = CVEN.VEN_CO_NUMERO
                         INNER JOIN GLOBAL.GLB_CLIENTE  CLI ON CLI.CLI_CO_NUMERO = CVEN.CLI_CO_NUMERO
                         WHERE 
                               CVEN.CLV_IN_STATUS IS NULL ";

            if (($VendedorUsuario[0]->getTipo() == "V") || ($VendedorUsuario[0]->getTipo() == "N")) {
                $sql .= " AND VEN.VEN_CO_SUPERVISOR = {$VendedorUsuario[0]->getCodigo()} ";
            } else {
                $sql .= " AND VEN.VEN_CO_NUMERO = {$VendedorUsuario[0]->getCodigo()} ";
            }
            
            $sql .= ")";
        } else if ($ObjUsuario->getUsuarioTipo() == "TRA") {
            
            $sql .= " AND DOC.ROT_CO_NUMERO IN (
                        SELECT DISTINCT ROT_CO_NUMERO FROM USUARIO_ROTA WHERE USU_CO_NUMERO = {$ObjUsuario->getUsuarioCodigo()}
                        )";
            
        }

        $sql.=" ORDER BY DOC_DT_CADASTRO";

        //		echo "<pre>".$sql."</pre>"; //die();// teste geovanni

        if ($result = $this->conn->execSql($sql)) {
            return $result;
        }
        return false;
    }	
	
	
	public function relatorioOcorrenciaPassoLeadTime($protocolo = NULL, $passo=null, $dataInicialPasso=null, $dataFinalPasso=null) {
		$sql = "SELECT 
					DOP.DOC_CO_NUMERO,
					DOP.DPA_CO_NUMERO,
					DPA.DPA_NO_DESCRICAO,
					DOP.USU_CO_NUMERO,
					TO_CHAR(DOP.DOP_DT_ENTRADA, 'DD/MM/YYYY') DOP_DT_ENTRADA,
					TO_CHAR(DOP.DOP_DT_SAIDA, 'DD/MM/YYYY') DOP_DT_SAIDA,
					DOP.DOP_TX_OBSERVACAO
				FROM DEV_OCORRENCIA_PASSO DOP
				INNER JOIN DEV_PASSO DPA ON DPA.DPA_CO_NUMERO = DOP.DPA_CO_NUMERO
				WHERE DOP.DOC_CO_NUMERO = {$protocolo} 
				AND DOP.DPA_CO_NUMERO = {$passo}
				";
		
		if ($dataInicialPasso != "" and $dataFinalPasso != ""){		
			$sql .= " AND DOP.DOP_DT_SAIDA BETWEEN TO_DATE ('{$dataInicialPasso} 00:00:00', 'DD/MM/YYYY HH24:MI:SS') AND  TO_DATE ('{$dataFinalPasso} 23:59:59', 'DD/MM/YYYY HH24:MI:SS')";
		}		
		
        if ($result = $this->conn->execSql($sql)) {
            return $result;
        }
        return false;		
		
	}
	
	
	public function relatorioLeadTimeOcorrenciaStatus($protocolo=null, $status=null, $dataI=null, $dataF=null, $isRegUnico=false) {
		$sql = "select 
					DOC_CO_NUMERO, 
					DST_CO_NUMERO,
					TO_CHAR(DOS_DT_ENTRADA, 'DD/MM/YYYY') DOS_DT_ENTRADA
				from dev_ocorrencia_status where doc_co_numero = {$protocolo} 
				and DST_CO_NUMERO = {$status}";
		
		if ($dataI != ""){		
			$sql .= " AND DOS_DT_ENTRADA >= TO_DATE ('{$dataI} 00:00:00', 'DD/MM/YYYY HH24:MI:SS')";
		}		
		if ($dataF != ""){		
			$sql .= " AND DOS_DT_ENTRADA <= TO_DATE ('{$dataF} 23:59:59', 'DD/MM/YYYY HH24:MI:SS')";
		}				
		if ($isRegUnico){
			$sql .= " AND ROWNUM = 1";
		}
		
		$sql .= " ORDER BY DOS_DT_ENTRADA";
		
        if ($result = $this->conn->execSql($sql)) {
            return $result;
        }
        return false;		
		
	}
	
	
    public function relatorioPendenteResponsavel($dataInicial, $dataFinal, $cedis = NULL, $dataInicialPasso=null, $dataFinalPasso=null) {
        $sql = "SELECT 
					TO_CHAR(DOC.DOC_DT_CADASTRO, 'DD/MM/YYYY') DATA_CADASTRO,
					DOC.DOC_CO_NUMERO,
					TO_CHAR(DOP.DOP_DT_SAIDA, 'DD/MM/YYYY') DOP_DT_SAIDA,
					NTO.NTO_NU_SERIE,
					NTO.NTO_NU_NOTA,
					TO_CHAR(NTO.NTO_DT_NOTA, 'DD/MM/YYYY') NTO_DT_NOTA,
					TOC.TOC_NO_DESCRICAO,					
					CED.CED_NO_CENTRO
                FROM 
				DEV_OCORRENCIA DOC
                INNER JOIN DEV_NOTAORIGEM                 NTO 
					ON NTO.DOC_CO_NUMERO = DOC.DOC_CO_NUMERO
				INNER JOIN DEV_OCORRENCIA_PASSO 		  DOP 
					ON DOP.DOC_CO_NUMERO = DOC.DOC_CO_NUMERO AND DOP.DPA_CO_NUMERO = DOC.DPA_CO_NUMERO AND DOP.DPA_CO_NUMERO = 4 AND DOP.DOP_DT_SAIDA IS NOT NULL
				INNER JOIN CAL_TIPO_OCORRENCIA            TOC 
					ON TOC.TOC_CO_NUMERO = DOC.TOC_CO_NUMERO AND TOC.DEP_CO_NUMERO = 29 AND TOC.SLA_CO_NUMERO = 1
                LEFT  JOIN GLOBAL.GLB_CENTRO_DISTRIBUICAO CED 
					ON CED.CED_CO_NUMERO = DOC.CED_CO_NUMERO
                WHERE 1=1 ";

		if ($dataInicial != "" and $dataFinal != ""){
			$sql .= " and DOC.DOC_DT_CADASTRO BETWEEN TO_DATE ('{$dataInicial} 00:00:00', 'DD/MM/YYYY HH24:MI:SS') AND  TO_DATE ('{$dataFinal} 23:59:59', 'DD/MM/YYYY HH24:MI:SS')";
		}					
				
		if ($dataInicialPasso != "" and $dataFinalPasso != ""){		
			$sql .= " and DOP.DOP_DT_SAIDA BETWEEN TO_DATE ('{$dataInicialPasso} 00:00:00', 'DD/MM/YYYY HH24:MI:SS') AND  TO_DATE ('{$dataFinalPasso} 23:59:59', 'DD/MM/YYYY HH24:MI:SS')";
		}					
				
        if (($cedis) && ($cedis != "TOD")) {
            $sql .= " AND DOC.CED_CO_NUMERO = {$cedis}";
        }
        
        $sql.=" ORDER BY DOC_DT_CADASTRO";

        //		echo "<pre>".$sql."</pre>"; //die();// teste geovanni

        if ($result = $this->conn->execSql($sql)) {
            return $result;
        }
        return false;
    }	
			
    public function relatorioOcorrenciaCountSumRetornoPor($retornoPor = NULL, $dataInicial, $dataFinal, $cliente = NULL, $rede = NULL, $status = NULL, $rota = NULL, $cedis = NULL, $motivo = NULL, $tipo = NULL, $divisao = NULL, $representante = NULL, $gerente = NULL, GlbUsuario $ObjUsuario) {
        $sql = "SELECT RETORNO, NUM, VALOR
			FROM 
			(
				SELECT "; 
		if (strtoupper($retornoPor) == "STATUS"){
			$sql.=" DOC.DST_CO_NUMERO RETORNO, ";
		}else if (strtoupper($retornoPor) == "MOTIVO"){
			$sql.=" DOC.TOC_CO_NUMERO RETORNO, ";
		}
		   $sql.= " COUNT(*) AS NUM,
					SUM(DOC.DOC_VR_VALOR) as VALOR
                FROM DEV_OCORRENCIA DOC
                INNER JOIN DEV_NOTAORIGEM                 NTO ON NTO.DOC_CO_NUMERO = DOC.DOC_CO_NUMERO
                INNER JOIN GLOBAL.GLB_CLIENTE             CLI ON CLI.CLI_CO_NUMERO = DOC.CLI_CO_NUMERO
                INNER JOIN GLOBAL.GLB_USUARIO             USU ON USU.USU_CO_NUMERO = DOC.DOC_CO_CADASTRANTE
                WHERE DOC.DOC_DT_CADASTRO BETWEEN TO_DATE ('{$dataInicial} 00:00:00', 'DD/MM/YYYY HH24:MI:SS') AND  TO_DATE ('{$dataFinal} 23:59:59', 'DD/MM/YYYY HH24:MI:SS')";

        if (($status) && ($status != "TOD")) {
			$sql .=" AND DOC.DST_CO_NUMERO in({$status})";
        }
        if ($cliente) {
            $sql .=" AND cli.cli_co_numero = $cliente";
        }
        if ($rede) {
            $sql .=" AND cli.red_co_numero = $rede";
        }
        if (($rota) && ($rota != "TOD")) {
            $sql .=" AND DOC.ROT_CO_NUMERO = '$rota'";
        }
        if (($cedis) && ($cedis != "TOD")) {
            $sql .= " AND DOC.CED_CO_NUMERO = {$cedis}";
        }
        if ($motivo) {
            $sql .=" AND DOC.TOC_CO_NUMERO in({$motivo})";
        }
        if ($tipo) {
            $sql .=" AND DOC.DOC_IN_TIPO = $tipo";
        }
        if ($divisao) {
            $sql .=" AND DOC.DEV_CO_DIVISAO = '$divisao'";
        }		
        if ($gerente) {
            $sql .=" AND DOC.USU_CO_NUMERO_GERENTE = $gerente";
        }				
        if ($ObjUsuario->getUsuarioTipo() == "CLI") {
            $sql .=" AND DOC.CLI_CO_NUMERO = {$ObjUsuario->getObjCliente()->getClienteCodigo()}";
        } else if ($ObjUsuario->getUsuarioTipo() == "VEN") {
            $VendedorUsuario = $ObjUsuario->getObjVendedor();
            $sql .= " AND DOC.CLI_CO_NUMERO IN (
                        SELECT DISTINCT CLI.CLI_CO_NUMERO
                         FROM GLOBAL.GLB_CLIENTE_VENDEDOR CVEN 
                         INNER JOIN GLOBAL.GLB_VENDEDOR VEN ON VEN.VEN_CO_NUMERO = CVEN.VEN_CO_NUMERO
                         INNER JOIN GLOBAL.GLB_CLIENTE  CLI ON CLI.CLI_CO_NUMERO = CVEN.CLI_CO_NUMERO
                         WHERE 
                               CVEN.CLV_IN_STATUS IS NULL ";
            if (($VendedorUsuario[0]->getTipo() == "V") || ($VendedorUsuario[0]->getTipo() == "N")) {
                $sql .= " AND VEN.VEN_CO_SUPERVISOR = {$VendedorUsuario[0]->getCodigo()} ";
            } else {
                $sql .= " AND VEN.VEN_CO_NUMERO = {$VendedorUsuario[0]->getCodigo()} ";
            }
            $sql .= ")";
        } else if ($ObjUsuario->getUsuarioTipo() == "TRA") {
            $sql .= " AND DOC.ROT_CO_NUMERO IN (
                        SELECT DISTINCT ROT_CO_NUMERO FROM USUARIO_ROTA WHERE USU_CO_NUMERO = {$ObjUsuario->getUsuarioCodigo()}
                        )";
        }

		if (strtoupper($retornoPor) == "STATUS"){
			$sql.=" GROUP BY DOC.DST_CO_NUMERO 
			) ";
		}else if (strtoupper($retornoPor) == "MOTIVO"){
			$sql.=" GROUP BY DOC.TOC_CO_NUMERO 
			) ";
		}
        //echo "<pre>".$sql."</pre>"; //die();// teste geovanni
        
        if ($result = $this->conn->execSql($sql)) {
            return $result;
        }
        return false;
    }	
	
    public function relatorioOcorrenciaCountSum($retorno = NULL, $dataInicial, $dataFinal, $cliente = NULL, $rede = NULL, $status = NULL, $rota = NULL, $cedis = NULL, $motivo = NULL, $tipo = NULL, $divisao = NULL, $representante = NULL, $gerente = NULL, $coleta = NULL, GlbUsuario $ObjUsuario) {
        if (strtoupper($retorno) == "COUNT"){
			$sql = "SELECT COUNT(*) AS RETORNO ";
		}else{
			$sql = "SELECT SUM(DOC.DOC_VR_VALOR) as RETORNO ";
		}
		$sql .= " FROM DEV_OCORRENCIA DOC
                INNER JOIN DEV_NOTAORIGEM                 NTO ON NTO.DOC_CO_NUMERO = DOC.DOC_CO_NUMERO
                INNER JOIN GLOBAL.GLB_CLIENTE             CLI ON CLI.CLI_CO_NUMERO = DOC.CLI_CO_NUMERO
                INNER JOIN GLOBAL.GLB_USUARIO             USU ON USU.USU_CO_NUMERO = DOC.DOC_CO_CADASTRANTE";
		if ($coleta != ""){		
			if (strtoupper($coleta) == "FORA"){
				$sql .= " INNER JOIN DEV_OCORRENCIA_STATUS          DOS  ON DOC.DOC_CO_NUMERO   = DOS.DOC_CO_NUMERO AND DOS.DST_CO_NUMERO = DOC.DST_CO_NUMERO and DOS.DOS_DT_ENTRADA < (SELECT (SYSDATE  - PAR_NU_DIASPASSO) FROM DEV_PARAMETROS) ";
			}else if (strtoupper($coleta) == "DENTRO"){
				$sql .= " INNER JOIN DEV_OCORRENCIA_STATUS          DOS  ON DOC.DOC_CO_NUMERO   = DOS.DOC_CO_NUMERO AND DOS.DST_CO_NUMERO = DOC.DST_CO_NUMERO and DOS.DOS_DT_ENTRADA > (SELECT (SYSDATE  - PAR_NU_DIASPASSO) FROM DEV_PARAMETROS) ";
			}
		}
				
        $sql .= " WHERE DOC.DOC_DT_CADASTRO BETWEEN TO_DATE ('{$dataInicial} 00:00:00', 'DD/MM/YYYY HH24:MI:SS') AND  TO_DATE ('{$dataFinal} 23:59:59', 'DD/MM/YYYY HH24:MI:SS')";

        if (($status) && ($status != "TOD")) {
			$status = str_replace("|", ",", $status);
            $sql .=" AND DOC.DST_CO_NUMERO in({$status})";
        }
        if ($cliente) {
            $sql .=" AND cli.cli_co_numero = $cliente";
        }
        if ($rede) {
            $sql .=" AND cli.red_co_numero = $rede";
        }
        if (($rota) && ($rota != "TOD")) {
            $sql .=" AND DOC.ROT_CO_NUMERO = '$rota'";
        }
        if (($cedis) && ($cedis != "TOD")) {
            $sql .= " AND DOC.CED_CO_NUMERO = {$cedis}";
        }
        if ($motivo) {
            $sql .=" AND DOC.TOC_CO_NUMERO = $motivo";
        }
        if ($tipo) {
            $sql .=" AND DOC.DOC_IN_TIPO = $tipo";
        }
        if ($divisao) {
            $sql .=" AND DOC.DEV_CO_DIVISAO = '$divisao'";
        }		
        if ($gerente) {
            $sql .=" AND DOC.USU_CO_NUMERO_GERENTE = $gerente";
        }		
        if ($ObjUsuario->getUsuarioTipo() == "CLI") {
            $sql .=" AND DOC.CLI_CO_NUMERO = {$ObjUsuario->getObjCliente()->getClienteCodigo()}";
        } else if ($ObjUsuario->getUsuarioTipo() == "VEN") {
            $VendedorUsuario = $ObjUsuario->getObjVendedor();
            $sql .= " AND DOC.CLI_CO_NUMERO IN (
                        SELECT DISTINCT CLI.CLI_CO_NUMERO
                         FROM GLOBAL.GLB_CLIENTE_VENDEDOR CVEN 
                         INNER JOIN GLOBAL.GLB_VENDEDOR VEN ON VEN.VEN_CO_NUMERO = CVEN.VEN_CO_NUMERO
                         INNER JOIN GLOBAL.GLB_CLIENTE  CLI ON CLI.CLI_CO_NUMERO = CVEN.CLI_CO_NUMERO
                         WHERE 
                               CVEN.CLV_IN_STATUS IS NULL ";
            if (($VendedorUsuario[0]->getTipo() == "V") || ($VendedorUsuario[0]->getTipo() == "N")) {
                $sql .= " AND VEN.VEN_CO_SUPERVISOR = {$VendedorUsuario[0]->getCodigo()} ";
            } else {
                $sql .= " AND VEN.VEN_CO_NUMERO = {$VendedorUsuario[0]->getCodigo()} ";
            }
            $sql .= ")";
        } else if ($ObjUsuario->getUsuarioTipo() == "TRA") {
            $sql .= " AND DOC.ROT_CO_NUMERO IN (
                        SELECT DISTINCT ROT_CO_NUMERO FROM USUARIO_ROTA WHERE USU_CO_NUMERO = {$ObjUsuario->getUsuarioCodigo()}
                        )";
        }

        //		echo "<pre>".$sql."</pre>"; //die();// teste geovanni

        if ($result = $this->conn->execSql($sql)) {
			OCIFetchInto($result, $row, OCI_ASSOC);
            return $row['RETORNO'];
        }
        return false;
    }	
	
}
