<?php 
Class DevUsuarioDao
{
	private $conn;

	function __construct() {
		$this->conn = new DaoSistema();
		$this->conn->conectar();
	}
	
	function consultarPorTipo($tipo){
		try{
			$sql ="SELECT
					USU.USU_CO_NUMERO,
					USU.USU_NO_COMPLETO,
					USU.USU_NO_LOGIN,
					USU.USU_NO_EMAIL
				FROM USUARIO USU
				WHERE USU.USU_DT_EXCLUSAO IS NULL AND USU.TPU_CO_NUMERO = ".$tipo; 
			//echo "<pre>".$sql."</pre>" ; 
			
			if($result = $this->conn->execSql($sql)){
				return $result; 
			}
			return false; 
		}catch(Exception $e){
			print("[ Excepion Update Usuario ]".$e->getMessage());
		}			
	}
	
}