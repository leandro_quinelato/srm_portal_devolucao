<?php

//include_once("../OcorrenciaPasso.class.php");

/**
 * Description of OcorrenciaPassoDao
 *
 * @author geovanni.info
 */
class OcorrenciaPassoDao {

    private $conn;

    function __construct() {
        $this->conn = new DaoSistema();
        $this->conn->conectar();
    }

    public function inserirPasso(OcorrenciaPasso $OcorrenciaPasso) {

        $sql = "
                
            INSERT
            INTO DEV_OCORRENCIA_PASSO
              (
                DOC_CO_NUMERO,
                DPA_CO_NUMERO,
                DOP_DT_ENTRADA
              )
              VALUES
              (
                {$OcorrenciaPasso->getCodigoOcorrencia()},
                {$OcorrenciaPasso->getCodigoPasso()},
                SYSDATE
              )
                     
            ";

//                echo $sql;//die();
        if ($this->conn->execSql($sql)) {
            return true;
        }
        return false;
    }

    public function aprovarPasso(OcorrenciaPasso $OcorrenciaPasso) {

        $sql = "
                
                UPDATE DEV_OCORRENCIA_PASSO SET 
                 USU_CO_NUMERO     = {$OcorrenciaPasso->getUsuario()}
                ,DOP_DT_SAIDA      = SYSDATE
                ,DOP_IN_APROVADO   = {$OcorrenciaPasso->getIndicadorAprovado()}
                ,DOP_TX_OBSERVACAO = '{$OcorrenciaPasso->getObservacao()}'    ";

        if ($OcorrenciaPasso->getQtdVolume()) {
            $sql .= "
                    ,DOP_QT_VOLUME     = {$OcorrenciaPasso->getQtdVolume()} ";
        }


        $sql .= "
                WHERE DOC_CO_NUMERO   = {$OcorrenciaPasso->getCodigoOcorrencia()}
                AND DPA_CO_NUMERO     = {$OcorrenciaPasso->getCodigoPasso()}      
            ";


//                echo $sql;//die();
        if ($this->conn->execSql($sql)) {
            return true;
        }
        return false;
    }

    public function consultarOcorrenciaPasso(OcorrenciaPasso $OcorrenciaPasso) {

        $sql = "
                    SELECT DOP.DOC_CO_NUMERO,
                           DOP.DPA_CO_NUMERO,
                           DOP.USU_CO_NUMERO,
                           USU.USU_NO_USERNAME,
                           TO_CHAR(DOP.DOP_DT_ENTRADA, 'DD/MM/YYYY') DOP_DT_ENTRADA,
                           TO_CHAR(DOP.DOP_DT_SAIDA, 'DD/MM/YYYY') DOP_DT_SAIDA,
                           NVL(DOP.DOP_IN_APROVADO,9) DOP_IN_APROVADO,
                           DOP.DOP_TX_OBSERVACAO,
                           DOP.DOP_QT_VOLUME
                    FROM  DEV_OCORRENCIA_PASSO DOP
                    LEFT  JOIN GLOBAL.GLB_USUARIO USU ON USU.USU_CO_NUMERO = DOP.USU_CO_NUMERO
                    WHERE DOP.DOC_CO_NUMERO = {$OcorrenciaPasso->getCodigoOcorrencia()}		
		";

        if ($OcorrenciaPasso->getCodigoPasso()) {
            $sql .= "
                       AND   DOP.DPA_CO_NUMERO  = {$OcorrenciaPasso->getCodigoPasso()}
                    ";
        }    
        
//        echo $sql;
        $result = $this->conn->execSql($sql);
        while ($Rpasso = oci_fetch_object($result)) {
            $OcorrenciaPasso = new OcorrenciaPasso();
            $OcorrenciaPasso->setCodigoOcorrencia($Rpasso->DOC_CO_NUMERO);
            $OcorrenciaPasso->setCodigoPasso($Rpasso->DPA_CO_NUMERO);
            $OcorrenciaPasso->setUsuario($Rpasso->USU_CO_NUMERO);
            $OcorrenciaPasso->setUsuarioNome($Rpasso->USU_NO_USERNAME);
            $OcorrenciaPasso->setDataEntrada($Rpasso->DOP_DT_ENTRADA);
            $OcorrenciaPasso->setDataSaida($Rpasso->DOP_DT_SAIDA);
            $OcorrenciaPasso->setIndicadorAprovado($Rpasso->DOP_IN_APROVADO);
            $OcorrenciaPasso->setObservacao($Rpasso->DOP_TX_OBSERVACAO);
            $OcorrenciaPasso->setQtdVolume($Rpasso->DOP_QT_VOLUME);

            $ObjOcorrenciaPasso[] = $OcorrenciaPasso;
        }
        return $ObjOcorrenciaPasso;
    }
    
    
    public function reativarPasso(OcorrenciaPasso $OcorrenciaPasso) {
        
        $sql = "UPDATE DEV_OCORRENCIA_PASSO SET 
                         DOP_DT_SAIDA = NULL
                        ,DOP_IN_APROVADO = NULL
                        ,DOP_TX_OBSERVACAO = NULL
                WHERE DOC_CO_NUMERO   = {$OcorrenciaPasso->getCodigoOcorrencia()}
                AND DPA_CO_NUMERO     = {$OcorrenciaPasso->getCodigoPasso()}   
               ";
                
//                echo $sql;//die();
        if ($this->conn->execSql($sql)) {
            return true;
        }
        return false;
                
        
    }
    
    public function alterarVolume(OcorrenciaPasso $OcorrenciaPasso) {

        $sql = "
                
                UPDATE DEV_OCORRENCIA_PASSO SET 
  
                    DOP_QT_VOLUME     = {$OcorrenciaPasso->getQtdVolume()} 

                WHERE DOC_CO_NUMERO   = {$OcorrenciaPasso->getCodigoOcorrencia()}
                AND DPA_CO_NUMERO     = {$OcorrenciaPasso->getCodigoPasso()}      
            ";


//                echo $sql;//die();
        if ($this->conn->execSql($sql)) {
            return true;
        }
        return false;
    }
    

}
