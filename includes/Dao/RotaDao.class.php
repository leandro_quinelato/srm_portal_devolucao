<?php

//include_once("../Rota.class.php");


Class RotaDao {

    private $conn;

    function __construct() {
        $this->conn = new DaoSistema();
        $this->conn->conectar();
    }

    public function excluir(Rota $ObjRota) {
        $sql = "";
        if ($this->conn->execSql($sql)) {
            return true;
        } else {
            return false;
        }
    }

    /* Inculta de rotas  */

    public function consultar(Rota $ObjRota) {
        $sql = "SELECT * FROM global.glb_rota
				WHERE 1=1 
		";
        if ($ObjRota->getDescricaoRota()) {
            $sql .=" AND ROT_NO_ROTA like '{$ObjRota->getDescricaoRota()}%'";
        }
        if ($ObjRota->getCodigoRota()) {
            $sql .=" AND ROT_CO_NUMERO = {$ObjRota->getCodigoRota()}";
        }
        if ($result = $this->conn->execSql($sql)) {
            return $result;
        } else {
            return false;
        }
    }

    public function listar() {
        $sql = "SELECT * FROM global.glb_rota ORDER BY ROT_CO_NUMERO ";
        if ($result = $this->conn->execSql($sql)) {
            return $result;
        } else {
            return false;
        }
    }

    /* Inser��o de usuario rotas podendo tambem ser usuario do sistema w2  */

    public function inserirUsuRota($usu_co_numero, $rot_co_numero) {
        $sql = "INSERT INTO USUARIO_ROTA(
					USU_CO_NUMERO
					,ROT_CO_NUMERO					
					)
				VALUES(
					{$usu_co_numero}, 
					'{$rot_co_numero}'										
				)";
        //echo $sql; 		 			
        if ($this->conn->execSql($sql)) {
            return true;
        }
        return false;
    }

    /* Consultar usuario rota  */

    public function consultaUsuRota($usu_co_numero, $rot_co_numero = NULL) {
        $sql = " SELECT * FROM 
				 USUARIO_ROTA usr
				 INNER JOIN global.glb_rota rot				 
			    	 ON ROT.ROT_CO_NUMERO = USR.ROT_CO_NUMERO  
		        INNER JOIN global.GLB_USUARIO USU
		            ON USU.USU_CO_NUMERO = USR.USU_CO_NUMERO             
				WHERE 
					USU.USU_IN_STATUS IS NULL     
				";
        if (!is_null($usu_co_numero)) {
            $sql .= " AND usr.USU_CO_NUMERO = {$usu_co_numero}";
        }
        if ($rot_co_numero) {
            $sql .= " AND  rot.ROT_CO_NUMERO = '{$rot_co_numero}'";
        }

        $result = $this->conn->execSql($sql);
        return $result;
    }

    /* Listagem de usuario rota   */

    public function listaUsuRota(devUsuarios $ObjUsuario) {
        $sql = "select 
					usu.usu_co_numero
					,usu.usu_no_completo
					,usu.usu_no_login
					,rot.rot_no_descricao
					,rot.rot_co_numero
					,tpu.tpu_no_descricao
					from usuario usu
					inner join usuario_rota usr 
					on usu.usu_co_numero = usr.usu_co_numero
					inner join rota rot 
					on rot.rot_co_numero = usr.rot_co_numero
					inner join tipo_usuario tpu
					on  tpu.tpu_co_numero = usu.tpu_co_numero					
				where 1=1
				";
        if ($ObjUsuario->getNomeUsuario()) {
            $sql .= " AND usu.usu_no_completo like '{$ObjUsuario->getNomeUsuario()}%'";
        }
        $result = $this->conn->execSql($sql);
        return $result;
    }

    /* Altera��o de rota do usuario  */

    public function alterarUsuRota($usu_co_numero, $rot_co_numero) {
        $sql = " UPDATE USUARIO_ROTA SET 
					ROT_CO_NUMERO = '$rot_co_numero' 
				 WHERE 
				 	USU_CO_NUMERO = $usu_co_numero ";

        if ($result = $this->conn->execSql($sql)) {
            return true;
        } else {
            return false;
        }
    }

    /* Consultar usuario rota  */

    public function consultaUsuRotaDevolucao($rot_co_numero, $tpu_co_numero) {
        $sql = " SELECT * FROM 
				    USUARIO_ROTA usr
				 INNER JOIN ROTA rot
			        ON rot.ROT_CO_NUMERO = usr.ROT_CO_NUMERO  
         		 INNER JOIN usuario usu
           		    ON usu.usu_co_numero = usr.usu_co_numero
				 WHERE					
					AND usu.tpu_co_numero = $tpu_co_numero
					AND rot.ROT_CO_NUMERO = $rot_co_numero";

        if ($result = $this->conn->execSql($sql)) {
            return $result;
        }
        return false;
    }

    public function consultarUsuarioRotaIn($rot_co_numeros = NULL) {


        $sql = "SELECT * FROM global.glb_rota
				WHERE 1=1 
		";
        if ($rot_co_numeros) {
            $sql .=" AND ROT_CO_NUMERO NOT IN (";
            $i = 1;
            foreach ($rot_co_numeros as $rota) {
                if ($i != 1) {
                    $sql .=",";
                }
                $sql .= "'" . $rota . "'";
                $i++;
            }
            $sql .=")";
        }
//		echo $sql; 	
        if ($result = $this->conn->execSql($sql)) {
            return $result;
        }
        return false;
    }

    public function deletUsuarioRota(devUsuarios $ObjUsuario) {

        $rot[] = $ObjUsuario->getObjRota();

        $sql = " DELETE FROM USUARIO_ROTA 
					WHERE 
						USU_CO_NUMERO = {$ObjUsuario->getCodigoUsuario()} 
						AND ROT_CO_NUMERO = '{$rot[0]->getCodigoRota()}'";
        //echo $sql;
        if ($result = $this->conn->execSql($sql)) {
            return true;
        }
        return false;
    }

    public function OcorrenciaRota($doc_co_numero) {

        $sql = " SELECT * FROM USUARIO USU
			INNER JOIN USUARIO_ROTA USR
			ON USR.USU_CO_NUMERO = USU.USU_CO_NUMERO 
			WHERE 
			USR.ROT_CO_NUMERO IN (  SELECT ROT_CO_NUMERO 
				FROM DEV_OCORRENCIA WHERE DOC_CO_NUMERO = {$doc_co_numero} )";
        if ($result = $this->conn->execSql($sql)) {
            return $result;
        }
        return false;
    }

    public function consultarRotaVendedor(GlbVendedor $ObjVendedor) {

        $sql = " SELECT DISTINCT CLI.ROT_CO_NUMERO
                         FROM GLOBAL.GLB_CLIENTE_VENDEDOR CVEN 
                         INNER JOIN GLOBAL.GLB_VENDEDOR VEN ON VEN.VEN_CO_NUMERO = CVEN.VEN_CO_NUMERO
                         INNER JOIN GLOBAL.GLB_CLIENTE  CLI ON CLI.CLI_CO_NUMERO = CVEN.CLI_CO_NUMERO
                         WHERE 
                               CVEN.CLV_IN_STATUS IS NULL ";


        if ($ObjVendedor) {
            if (($ObjVendedor->getTipo() == "V") || ($ObjVendedor->getTipo() == "N")) {
                $sql .= " AND VEN.VEN_CO_SUPERVISOR = {$ObjVendedor->getCodigo()} ";
            }else{
                $sql .= " AND VEN.VEN_CO_NUMERO = {$ObjVendedor->getCodigo()} ";
            }
        }
        
                $sql .= " ORDER BY CLI.ROT_CO_NUMERO";
        
//        echo $sql;

        if ($result = $this->conn->execSql($sql)) {
            return $result;
        }
        return false;
    }

}
