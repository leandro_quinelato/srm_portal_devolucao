<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ClienteContatoDevolucao
 *
 * @author geovanni.info
 */
class ClienteContatoDevolucaoDao {
    //put your code here
    
    private $conn;

    function __construct() {

        $this->conn = new DaoSistema();
        $this->conn->conectar();
    }
    
    public function gerarCodigoContato() {



        try {
            $sql = "SELECT NVL( MAX( DCC_CO_NUMERO ), 0 ) + 1 AS DCC_CO_NUMERO
                    FROM DEV_CLIENTE_CONTATO
            ";

            $result = $this->conn->execSql($sql);
            OCIFetchInto($result, $row, OCI_ASSOC);

            return $row['DCC_CO_NUMERO'];
        } catch (Exception $e) {
            print( "[ ERRO NO METODO gerarCodigoOcorrencia ]: " . $e->getMessage());
            return null;
        }
    }
    
    
    public function consultaDadosCliente($cli_co_numero){
        
        $sql = "
                    SELECT CLI.CLI_NU_CNPJ_CPF
                          ,CLI.CLI_NO_RAZAO_SOCIAL
                          ,CLI.CLI_NO_FANTASIA
                          ,CLI.CLI_CO_NUMERO
                          ,CLI.CLI_NU_CEP
                          ,CLI.CLI_ED_LOGRADOURO
                          ,CLI.CLI_ED_NUMERO
                          ,CLI.CLI_ED_COMPLEMENTO
                          ,CLI.CLI_NO_BAIRRO
                          ,CLI.LOC_CO_NUMERO
                          ,LOC.LOC_NO_CIDADE
                          ,LOC.EST_SG_ESTADO

                    FROM GLOBAL.GLB_CLIENTE CLI
                    INNER JOIN GLOBAL.GLB_LOCALIDADE LOC ON CLI.LOC_CO_NUMERO = LOC.LOC_CO_NUMERO
                    WHERE CLI.CLI_CO_NUMERO = {$cli_co_numero}
        ";
                
                
        return $this->conn->execSql($sql);
        
        
    }
    
    public function consultarContatoCliente($cli_co_numero) {
        $sql = "
		    SELECT DCC_CO_NUMERO,
                           CLI_CO_NUMERO,
                           decode(DCC_IN_CONTATO, 'T', 'TELEFONE', 'E', 'E-MAIL', 'F', 'FAX' ,DCC_IN_CONTATO ) DCC_IN_CONTATO,
                           DCC_NO_DESCONTATO,
                           DCC_NO_CONTATO,
                           TO_CHAR(DCC_DT_CADASTRO,'DD/MM/YYYY') DCC_DT_CADASTRO,
                           DCC_DT_EXCLUSAO,
                           USU_CO_NUMERO
                    FROM DEV_CLIENTE_CONTATO
                    WHERE CLI_CO_NUMERO = {$cli_co_numero}
                    AND   DCC_DT_EXCLUSAO IS NULL    
		";

        $result = $this->conn->execSql($sql);
        while ($Rcliente = oci_fetch_object($result)) {
            $ClienteContatoDevolucao = new ClienteContatoDevolucao(); 
            $ClienteContatoDevolucao->setCodigoContato($Rcliente->DCC_CO_NUMERO);
            $ClienteContatoDevolucao->setCodigoCliente($Rcliente->CLI_CO_NUMERO);
            $ClienteContatoDevolucao->setTipoContato($Rcliente->DCC_IN_CONTATO);
            $ClienteContatoDevolucao->setDescContato($Rcliente->DCC_NO_DESCONTATO);
            $ClienteContatoDevolucao->setContato($Rcliente->DCC_NO_CONTATO);
            $ClienteContatoDevolucao->setDataCadastro($Rcliente->DCC_DT_CADASTRO);
            $ClienteContatoDevolucao->setDataExclusao($Rcliente->DCC_DT_EXCLUSAO);
            $ClienteContatoDevolucao->setUsuario($Rcliente->USU_CO_NUMERO);

            $ObjContato[] = $ClienteContatoDevolucao;
        }
        return $ObjContato;
    }
    
    public function inserirClienteContato(ClienteContatoDevolucao $ClienteContatoDevolucao) {
        $codigocontato = $this->gerarCodigoContato();
        $sql = "
                    INSERT
                    INTO DEV_CLIENTE_CONTATO
                      (
                        DCC_CO_NUMERO,
                        CLI_CO_NUMERO,
                        DCC_IN_CONTATO,
                        DCC_NO_DESCONTATO,
                        DCC_NO_CONTATO,
                        DCC_DT_CADASTRO,
                        USU_CO_NUMERO
                      )
                      VALUES
                      (
                        {$codigocontato},
                        '{$ClienteContatoDevolucao->getCodigoCliente()}',
                        '{$ClienteContatoDevolucao->getTipoContato()}',
                        '{$ClienteContatoDevolucao->getDescContato()}',
                        '{$ClienteContatoDevolucao->getContato()}',
                        SYSDATE,
                        {$ClienteContatoDevolucao->getUsuario()}
                      )   
            ";

//                echo $sql;//die();
        if ($this->conn->execSql($sql)) {
            return true;
        }
        return false;
    }
    
    public function excluirClienteContato(ClienteContatoDevolucao $ClienteContatoDevolucao) {
        $sql = "
                    UPDATE DEV_CLIENTE_CONTATO
                    SET DCC_DT_EXCLUSAO = SYSDATE
                    WHERE CLI_CO_NUMERO   = {$ClienteContatoDevolucao->getCodigoCliente()}
                    AND DCC_CO_NUMERO     = {$ClienteContatoDevolucao->getCodigoContato()}
            ";

//                echo $sql;//die();
        if ($this->conn->execSql($sql)) {
            return true;
        }
        return false;
    }
    
    
}
