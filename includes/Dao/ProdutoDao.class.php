<?php

//include_once("GlbCentroDistribuicao.class.php");
//include_once("../Ocorrencia.class.php");
//include_once("../DevNotaOrigem.class.php");
//include_once("../DevNotaOrigemItem.class.php");

class ProdutoDao {

    private $conn;

    function __construct() {

        $this->conn = new DaoSistema();
        $this->conn->conectar();
    }

    public function gerarCodigoOcorrencia($usu_co_numero) {
        /*
         * Exemplo montagem: 16015800067012
         * 
         *     16     015800         067             012
         * ano YY     Usu_glB      dia_do_ano        contagem
         *                       0 a 365/366         operador oco
         */

        $codigoOperador = str_pad($usu_co_numero, 6, '0', STR_PAD_LEFT);

        try {
            $codigoBase = date("y") . "$codigoOperador" . str_pad(date("z"), 3, 0, STR_PAD_LEFT);
            $sql = "SELECT NVL( MAX( doc_co_numero ), " . $codigoBase . "000 ) + 1 AS DOC_CO_NUMERO
                    FROM dev_ocorrencia
                    WHERE SUBSTR( doc_co_numero, 1, 11 ) = $codigoBase
            ";

            $result = $this->conn->execSql($sql);
            OCIFetchInto($result, $row, OCI_ASSOC);

            return $row['DOC_CO_NUMERO'];
        } catch (Exception $e) {
            print( "[ ERRO NO METODO gerarCodigoOcorrencia ]: " . $e->getMessage());
            return null;
        }
    }

    public function consultaCliente($cli_co_numero, $rpr_co_numero = NULL, $rpr_co_supervisor = null) {
        $sql = "SELECT CLI.CLI_CO_NUMERO,
                       CLI.CLI_NO_RAZAO_SOCIAL,
                       CLI.ROT_CO_NUMERO ,
                       REP.RPR_CO_NUMERO,
                       REP.RPR_CO_SUPERVISOR,
                       CLI.VEN_CO_NUMERO,
                       CLI.CLI_NU_ROTA_TERCEIRIZADA
                FROM GLOBAL.GLB_CLIENTE CLI
                INNER JOIN USERVIMED.SVD_REPRESENTANTE REP
                  ON REP.RPR_CO_NUMERO = CLI.VEN_CO_NUMERO
                WHERE CLI_CO_NUMERO = $cli_co_numero 
        ";
        if ($rpr_co_numero) {
            $sql .=" AND REP.RPR_CO_NUMERO = $rpr_co_numero ";
        }
        if ($rpr_co_supervisor) {
            $sql .=" AND REP.RPR_CO_SUPERVISOR = $rpr_co_supervisor ";
        }
  
        return $this->conn->execSql($sql);
    }
    
    public function consultaClienteTransp($cli_co_numero, $usu_co_numero = NULL) {
                
        $sql = "SELECT CLI.CLI_CO_NUMERO,
                       CLI.CLI_NO_RAZAO_SOCIAL,
                       CLI.ROT_CO_NUMERO ,
                       CLI.VEN_CO_NUMERO,
                       CLI.CLI_NU_ROTA_TERCEIRIZADA
                FROM GLOBAL.GLB_CLIENTE CLI
                WHERE CLI_CO_NUMERO = $cli_co_numero 
                AND   CLI.ROT_CO_NUMERO IN (
                        SELECT DISTINCT ROT_CO_NUMERO FROM USUARIO_ROTA WHERE USU_CO_NUMERO = {$usu_co_numero}
                        )
        ";
        return $this->conn->execSql($sql);
    }

    public function consultaProduto($co_produto, $usu_co_numero = NULL) {
                
        $sql = "SELECT  PRO_NO_DESCRICAO, 
                        PRO_QT_CAIXA_MASTER, 
                        PRO_NU_CODIGO_BARRAS, 
                        PRO_CO_NUMERO,
                        PRO_VR_PRECO_FABRICA
                FROM GLOBAL.GLB_PRODUTO
                WHERE PRO_NU_CODIGO_BARRAS = $co_produto OR PRO_CO_NUMERO = $co_produto                
        ";        
        return $this->conn->execSql($sql);
    }

    public function carregaNotaItem($cli_co_numero, $serie, $numNota, $dataNota) {

        $sql = "SELECT  PRO.PRO_CO_NUMERO
                       ,PRO.PRO_NO_DESCRICAO
                       ,PRO.PRO_IN_GELADEIRA
                       ,NVI.QTDE
                       ,NVI.VLUNITNF
                       ,( NVI.VLUNITNF * NVI.QTDE ) TOTAL_BRUTO
                       ,( NVI.VLDESCCIAL  +  NVI.VLDESCPROM )  VALOR_DESCONTO
                       ,( NVI.VLICMS + NVI.VLICMSST + NVI.VLPISCONFIS )  VALOR_IMPOSTO
                       ,( (( NVI.VLUNITNF * NVI.QTDE ) -  (NVI.VLDESCCIAL +  NVI.VLDESCPROM)) + ( NVI.VLICMS + NVI.VLICMSST + NVI.VLPISCONFIS )  ) TOTAL_LIQUIDO
                FROM  USERVIMED.NOTAVENDAI NVI
                INNER JOIN GLOBAL.GLB_PRODUTO PRO ON PRO.PRO_CO_NUMERO = NVI.CDPRODUTO
                WHERE NVI.SERIE   = {$serie}
                AND   NVI.NUMNOTA = {$numNota}
                AND   NVI.CDCLIENTE = {$cli_co_numero}
                AND   NVI.DTMOV   = TO_DATE('{$dataNota}', 'dd/mm/yyyy')
                ORDER BY PRO.PRO_NO_DESCRICAO 
        ";

        return $this->conn->execSql($sql);
    }
    
    public function carregaNotaItemSaldoDev($cli_co_numero, $serie, $numNota, $dataNota) {

        $sql = "SELECT   PRO.PRO_CO_NUMERO
                        ,PRO.PRO_NO_DESCRICAO
                        ,PRO.PRO_IN_GELADEIRA
                        ,NVI.QTDE
                        ,NVI.VLUNITNF
                        ,( NVI.VLUNITNF * NVI.QTDE ) TOTAL_BRUTO
                        ,( NVI.VLDESCCIAL  +  NVI.VLDESCPROM )  VALOR_DESCONTO
                        ,( NVI.VLICMS + NVI.VLICMSST + NVI.VLPISCONFIS )  VALOR_IMPOSTO
                        , ( (( NVI.VLUNITNF * NVI.QTDE ) -  (NVI.VLDESCCIAL +  NVI.VLDESCPROM)) + ( NVI.VLICMS + NVI.VLICMSST + NVI.VLPISCONFIS )  ) TOTAL_LIQUIDO
                        ,SUM(NTI.NTI_NU_DEVQTD) QTD_ITENS_DEVOLVIDOS
                        , NVI.QTDE - SUM(NVL(NTI.NTI_NU_DEVQTD,0)) SALDO_ATUAL_NOTA
                       
                FROM  USERVIMED.NOTAVENDAI NVI
                INNER JOIN GLOBAL.GLB_PRODUTO PRO ON PRO.PRO_CO_NUMERO = NVI.CDPRODUTO
                LEFT JOIN DEV_NOTAORIGEM_ITEM  NTI ON NVI.SERIE = NTI.NTI_NU_SERIE AND NTI.NTI_NU_NOTA = NVI.NUMNOTA AND NTI.CLI_CO_NUMERO = NVI.CDCLIENTE AND NTI.PRO_CO_NUMERO = NVI.CDPRODUTO 
                LEFT JOIN DEV_OCORRENCIA   DEV ON DEV.DOC_CO_NUMERO = NTI.DOC_CO_NUMERO and DEV.DST_CO_NUMERO NOT IN (12)
                WHERE NVI.SERIE   = {$serie}
                AND   NVI.NUMNOTA = {$numNota}
                AND   NVI.CDCLIENTE = {$cli_co_numero}
                AND   NVI.DTMOV   = TO_DATE('{$dataNota}', 'dd/mm/yyyy')
             --   AND   DEV.DST_CO_NUMERO NOT IN (12)
                GROUP BY  PRO.PRO_CO_NUMERO
                         ,PRO.PRO_NO_DESCRICAO
                         ,PRO.PRO_IN_GELADEIRA
                         ,NVI.QTDE
                         ,NVI.VLUNITNF
                         ,( NVI.VLUNITNF * NVI.QTDE )
                         ,( NVI.VLDESCCIAL  +  NVI.VLDESCPROM )
                         ,( NVI.VLICMS + NVI.VLICMSST + NVI.VLPISCONFIS )
                         ,( (( NVI.VLUNITNF * NVI.QTDE ) -  (NVI.VLDESCCIAL +  NVI.VLDESCPROM)) + ( NVI.VLICMS + NVI.VLICMSST + NVI.VLPISCONFIS )  )

                ORDER BY PRO.PRO_NO_DESCRICAO 
        ";

        return $this->conn->execSql($sql);
    }
    
    public function verificaItemGeladeiraNota($cli_co_numero, $serie, $numNota, $dataNota) {

        $sql = "SELECT  COUNT(*) QTD
                FROM  USERVIMED.NOTAVENDAI NVI
                INNER JOIN GLOBAL.GLB_PRODUTO PRO ON PRO.PRO_CO_NUMERO = NVI.CDPRODUTO
                WHERE NVI.SERIE   = {$serie}
                AND   NVI.NUMNOTA = {$numNota}
                AND   NVI.CDCLIENTE = {$cli_co_numero}
                AND   NVI.DTMOV   = TO_DATE('{$dataNota}', 'dd/mm/yyyy')
                AND   PRO.PRO_IN_GELADEIRA = 'S'
        ";

        $result = $this->conn->execSql($sql);
        OCIFetchInto($result, $row, OCI_ASSOC);

        if ($row['QTD'] > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
        
    }
	
    public function verificaItemGeladeiraOcorrencia(Ocorrencia $ObjOcorrencia) {

        $sql = "SELECT  COUNT(*) QTD
                FROM  USERVIMED.NOTAVENDAI NVI
                INNER JOIN GLOBAL.GLB_PRODUTO PRO ON PRO.PRO_CO_NUMERO = NVI.CDPRODUTO
                INNER JOIN DEV_NOTAORIGEM DNO ON DNO.NTO_NU_SERIE = NVI.SERIE 
                  AND DNO.NTO_NU_NOTA = NVI.NUMNOTA AND DNO.NTO_DT_NOTA = NVI.DTMOV AND DNO.CLI_CO_NUMERO = NVI.CDCLIENTE 
                WHERE PRO.PRO_IN_GELADEIRA = 'S'
                AND DNO.DOC_CO_NUMERO =". $ObjOcorrencia->getCodigoDevolucao();

        $result = $this->conn->execSql($sql);
        OCIFetchInto($result, $row, OCI_ASSOC);

        if ($row['QTD'] > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
        
    }	

    public function consultaNotaVenda($cli_co_numero, $serie, $numNota, $dataNota) {
        print_r ($cli_co_numero);
        $sql = "SELECT * 
                FROM USERVIMED.NOTAVENDA
                WHERE SERIE   = {$serie}
                AND   NUMNOTA = {$numNota}
                AND   CDCLIENTE = {$cli_co_numero}
                AND   DTMOV   = TO_DATE('{$dataNota}', 'dd/mm/yyyy')
        ";


        $result = $this->conn->execSql($sql);
        return oci_fetch_object($result);
    }

    function inserir(Ocorrencia $Ocorrencia) {

        $sql = "
            INSERT INTO dev_ocorrencia
            (
                doc_co_numero,
                doc_dt_cadastro,             
                rot_co_numero,
                toc_co_numero,
                doc_in_tipo,
                cli_co_numero,
                dep_co_numero,
                sla_co_numero,
                doc_no_emailcadastrante,
                doc_co_cadastrante,
                doc_no_depocontato,
                doc_in_cadastrante,
                DST_CO_NUMERO,
                DOC_TX_DESCRICAO,
                CED_CO_NUMERO,
                DEV_CO_DIVISAO,
                DOC_VR_VALOR
            )
            VALUES
            (
                {$Ocorrencia->getCodigoDevolucao()}
                ,SYSDATE 
                ,'{$Ocorrencia->getObjRota()->getCodigoRota()}'
                ,{$Ocorrencia->getTipoOcorrencia()->getCodTipoOcorrencia()}
                ,{$Ocorrencia->getTipoTipoDevolucao()}
                ,{$Ocorrencia->getGlbCliente()->getClienteCodigo()}
                ,{$Ocorrencia->getTipoOcorrencia()->getDepartamentoTipoOcorrencia()}
                ,1
                ,'{$Ocorrencia->getEmailCadDevolucao()}'  
                ,{$Ocorrencia->getCodigoCadastranteDevolucao()}     
                ,'{$Ocorrencia->getDeptContatoClienteDevolucao()}'   
                ,'{$Ocorrencia->getTipoCadastranteDevolucao()}'
                ,{$Ocorrencia->getStatusOcorrencia()}
                ,'{$Ocorrencia->getDescricaoDevolucao()}'
                ,{$Ocorrencia->getCedis()}
                ,'{$Ocorrencia->getDivisao()}'
                ,{$Ocorrencia->getValorDevolucao()}    
                
            )";
        //echo $sql;
        if ($this->conn->execSql($sql)) {
            return true;
        }
        return false;
        
    }

    public function excluirOcorrencia(Ocorrencia $Ocorrencia) {

        $sql = "
            DELETE
            FROM dev_ocorrencia
            WHERE doc_co_numero = {$Ocorrencia->getCodigoDevolucao()}
        ";
 
        if ($this->conn->execSql($sql)) {
            return true;
        }
        return false;       
    }

    function alterar(Ocorrencia $ObjOcorrencia) {
        try {
            $sql = "UPDATE dev_ocorrencia 
                           SET doc_co_numero ={$ObjOcorrencia->getCodigoDevolucao()}";

            if ($ObjOcorrencia->getGlbCliente()->getClienteCodigo()) {
                $sql .=", cli_co_numero = {$ObjOcorrencia->getGlbCliente()->getClienteCodigo()}";
            }

            if ($ObjOcorrencia->getSerieNfDevolucao() && $ObjOcorrencia->getNotaNfDevolucao() && $ObjOcorrencia->getDataNfDevolucao()) {
                $sql .=", doc_nu_devserie = '{$ObjOcorrencia->getSerieNfDevolucao()}'
                        , doc_nu_devnota = '{$ObjOcorrencia->getNotaNfDevolucao() }'
                        , doc_dt_devnota = TO_DATE('{$ObjOcorrencia->getDataNfDevolucao()}','DD/MM/YYYY')";
            }

            if ($ObjOcorrencia->getQntdVolumeDevolucao()) {
                $sql .=",doc_qt_produto = {$ObjOcorrencia->getQntdVolumeDevolucao()}";
            }

            if ($ObjOcorrencia->getValorDevolucao()) {
                $sql .=",doc_vr_valor = {$ObjOcorrencia->getValorDevolucao()}";
            }

            if ($ObjOcorrencia->getTipoTipoDevolucao()) {
                $sql .=",doc_in_tipo = {$ObjOcorrencia->getTipoTipoDevolucao()}";
            }

            if ($ObjOcorrencia->getObjRota()->getCodigoRota()) {
                $sql .=",rot_co_numero = '{$ObjOcorrencia->getObjRota()->getCodigoRota()}'";
            }

            if ($ObjOcorrencia->getDescricaoDevolucao()) {
                $sql .=", doc_tx_descricao ='{$ObjOcorrencia->getDescricaoDevolucao()}'";
            }

            if ($ObjOcorrencia->getFinalizadoDevolucao()) {
                $sql .= ",doc_in_finalizado={$ObjOcorrencia->getFinalizadoDevolucao()}";
            }

            if ($ObjOcorrencia->getTransferidoDevolucao()) {
                $sql .=",doc_in_transferido={$ObjOcorrencia->getTransferidoDevolucao()}";
                $sql .=",doc_dt_transferencia = SYSDATE ";
            }
            if ($ObjOcorrencia->getTipoOcorrencia()->getCodTipoOcorrencia()) {
                $sql .=",toc_co_numero = {$ObjOcorrencia->getTipoOcorrencia()->getCodTipoOcorrencia()}";
            }

            if ($ObjOcorrencia->getUsuarioGerDevolucao()) {
                $sql .= ",usu_co_numero_gerente={$ObjOcorrencia->getUsuarioGerDevolucao()}";
                $sql .= ",doc_dt_gerente= SYSDATE ";
                $sql .= ",doc_in_gerente_aprova = {$ObjOcorrencia->getAprovacaoGerDevolucao()}";
                $sql .= ",doc_no_emailcoleta = '{$ObjOcorrencia->getEmailColetaDevolucao()}'";
                if ($ObjOcorrencia->getUsuarioIntGerDevolucao()) {
                    $sql .=" ,doc_in_gerente_interno = {$ObjOcorrencia->getUsuarioIntGerDevolucao()}";
                }

                if ($ObjOcorrencia->getReentregaGerDevolucao()) {
                    $sql .= ",doc_in_gerente_reentrega = {$ObjOcorrencia->getReentregaGerDevolucao()}";
                }
                $sql .= ",doc_tx_gerente_descricao = '{$ObjOcorrencia->getDescricaoGerDevolucao()}'";
            }

            if ($ObjOcorrencia->getUsuarioIntDevolucao()) {
                $sql .= ",usu_co_numero_interno = {$ObjOcorrencia->getUsuarioIntDevolucao()}";
                $sql .= ",doc_dt_interno= SYSDATE";
                $sql .= ",doc_in_interno_aprova = {$ObjOcorrencia->getAprovacaoIntDevolucao()}";
                $sql .= ",doc_tx_interno_descricao = '{$ObjOcorrencia->getDescricaoIntDevolucao()}'";
                $sql .= ",doc_qt_interno_produto = '{$ObjOcorrencia->getQntdVolumeIntDevolucao()}'";


                if ($ObjOcorrencia->getDataConferencia()) {
                    $sql .=",DOC_DT_CONFERENCIA = TO_DATE('{$ObjOcorrencia->getDataConferencia()}','DD/MM/YYYY')";
                }

                if ($ObjOcorrencia->getEmpresa()) {
                    $sql .=",EMP_CO_NUMERO = {$ObjOcorrencia->getEmpresa()}";
                    $sql .=",ETB_CO_NUMERO = {$ObjOcorrencia->getEstabelecimento()}";
                    $sql .=",COL_CO_CRACHA = {$ObjOcorrencia->getCracha()}";
                }

                if ($ObjOcorrencia->getDataDevRecebimento()) {
                    $sql .=",DOC_DT_DEVRECEBIMENTO = TO_DATE('{$ObjOcorrencia->getDataDevRecebimento()}','DD/MM/YYYY')";
                }
            }
            
                if ($ObjOcorrencia->getDataDevRecebimento()) {
                    $sql .=",DOC_DT_DEVRECEBIMENTO = TO_DATE('{$ObjOcorrencia->getDataDevRecebimento()}','DD/MM/YYYY')";
                }
                if ($ObjOcorrencia->getDataConferencia()) {
                    $sql .=",DOC_DT_CONFERENCIA = TO_DATE('{$ObjOcorrencia->getDataConferencia()}','DD/MM/YYYY')";
                }

            if ($ObjOcorrencia->getUsuarioPolEntDevolucao()) {
                $sql .= ",usu_co_numero_polo_entrada = {$ObjOcorrencia->getUsuarioPolEntDevolucao()}";
                $sql .= ",doc_dt_polo_entrada = SYSDATE  ";
                $sql .= ",doc_in_polo_entrada_aprova = {$ObjOcorrencia->getAprovacaoPolEntDevolucao()}";
                $sql .= ",doc_tx_polo_entrada_descricao = '{$ObjOcorrencia->getDescricaoPolEntDevolucao()}'";
                $sql .= ",doc_qt_polo_entrada_produto = '{$ObjOcorrencia->getQntdVolumePolEntDevolucao()}'";

                if ($ObjOcorrencia->getUsuarioIntPolEntDevolucao()) {
                    $sql .=" ,doc_in_polo_entrada_interno = {$ObjOcorrencia->getUsuarioIntPolEntDevolucao()}";
                }
            }

            if ($ObjOcorrencia->getUsuarioFisDevolucao()) {
                $sql .= ",usu_co_numero_fiscal = {$ObjOcorrencia->getUsuarioFisDevolucao()}";
                $sql .= ",doc_dt_fiscal = SYSDATE";
                $sql .= ",doc_in_fiscal_aprova = {$ObjOcorrencia->getAprovacaoFisDevolucao()}";
                $sql .= ",doc_tx_fiscal_descricao = '{$ObjOcorrencia->getDescricaoFisDevolucao()}'";

                if ($ObjOcorrencia->getUsuarioIntFisDevolucao()) {
                    $sql .=",doc_in_fiscal_interno = {$ObjOcorrencia->getUsuarioIntFisDevolucao()}";
                }
            }


            // add - 05/09/2014
            if ($ObjOcorrencia->getDocInTransmitido()) {
                $sql .= ",doc_in_transmitido = '{$ObjOcorrencia->getDocInTransmitido()}'";
            }

            // add - 15/10/2014
            if ($ObjOcorrencia->getNumChamadoCsl()) {
                $sql .= ",doc_nu_chamadocsl = '{$ObjOcorrencia->getNumChamadoCsl()}'";
            }

            // add - 08/04/2016
            if ($ObjOcorrencia->getStatusOcorrencia()) {
                $sql .= ",DST_CO_NUMERO = {$ObjOcorrencia->getStatusOcorrencia()}";
            }

            if ($ObjOcorrencia->getPassoOcorrencia()) {
                $sql .= ",DPA_CO_NUMERO = {$ObjOcorrencia->getPassoOcorrencia()}";
            }

            if ($ObjOcorrencia->getIndicaAutomatico()) {
                $sql .= ",DEV_IN_AUTOMATICO = {$ObjOcorrencia->getIndicaAutomatico()}";
            }

            $sql .= " WHERE doc_co_numero ={$ObjOcorrencia->getCodigoDevolucao()}";

            if ($this->conn->execSql($sql)) {
                return true;
            }
            return false;
        } catch (Exception $e) {
            print( "[ ERRO NO METODO DEV_OCORRENCIA ]: " . $e->getMessage());
            return null;
        }
    }

    public function excluirNotaOrigem(Ocorrencia $ObjOcorrencia) {
        $ObjetoNota = $ObjOcorrencia->getDevNotaOrigem();
        foreach ($ObjetoNota as $Objeto) {

            $sql = "
                DELETE
                FROM DEV_NOTAORIGEM
                WHERE DOC_CO_NUMERO = {$ObjOcorrencia->getCodigoDevolucao()}
                AND   NTO_NU_SERIE = {$Objeto->getSerie()}
                AND   NTO_NU_NOTA  = {$Objeto->getNota()}
                AND   CLI_CO_NUMERO = {$Objeto->getCodCliente()}
            ";
    
            if (!$this->conn->execSql($sql)) {
                return false;
            } else {

                $sql = "
                    DELETE
                    FROM DEV_NOTAORIGEM_ITEM
                    WHERE DOC_CO_NUMERO = {$ObjOcorrencia->getCodigoDevolucao()}
                    AND   NTI_NU_SERIE  = {$Objeto->getSerie()}
                    AND   NTI_NU_NOTA   = {$Objeto->getNota()}
                    AND   CLI_CO_NUMERO = {$Objeto->getCodCliente()}
                ";

                if (!$this->conn->execSql($sql)) {
                    return false;
                }
            }
        }

        return true;
    }

    public function inserirNotaOrigem(Ocorrencia $ObjOcorrencia) {
        $ObjetoNota = $ObjOcorrencia->getDevNotaOrigem();
        foreach ($ObjetoNota as $Objeto) {
            /*
              $sql = "INSERT INTO DEV_NOTAORIGEM(
              DOC_CO_NUMERO
              ,NTO_NU_SERIE
              ,NTO_NU_NOTA
              ,NTO_DT_NOTA
              ,CHDOC
              ,CLI_CO_NUMERO
              ,NTO_VL_DEVOLUCAO
              ,NTO_NU_ORIGEM_VENDA
              ,NTO_CO_CDIST
              ,NTO_CO_DIVISAO
              )VALUES(
              {$ObjOcorrencia->getCodigoDevolucao()}
              ,{$Objeto->getSerie()}
              ,{$Objeto->getNota()}
              ,TO_DATE('{$Objeto->getData()}','DD/MM/YYYY')
              ,'NF'|| LPAD('{$Objeto->getSerie()}',3,' ')||LPAD('{$Objeto->getNota()}',8,' ')
              ,{$Objeto->getCodCliente()}
              ,{$Objeto->getValorTotDevolucao()}
              ,'{$Objeto->getOrigemVenda()}'
              ,{$Objeto->getCentroDist()}
              ,'{$Objeto->getDivisao()}'
              )
              "; */

            $sql = "
                MERGE INTO DEV_NOTAORIGEM NTO USING(
                        SELECT  
                             {$ObjOcorrencia->getCodigoDevolucao()}                                         W_DOC_CO_NUMERO
                            ,{$Objeto->getSerie()}                                                          W_NTO_NU_SERIE
                            ,{$Objeto->getNota()}                                                           W_NTO_NU_NOTA
                            ,TO_DATE('{$Objeto->getData()}','DD/MM/YYYY')                                   W_NTO_DT_NOTA
                            ,'NF'|| LPAD('{$Objeto->getSerie()}',3,' ')||LPAD('{$Objeto->getNota()}',8,' ') W_CHDOC
                            ,{$Objeto->getCodCliente()}                                                     W_CLI_CO_NUMERO
                            ,{$Objeto->getValorTotDevolucao()}                                              W_NTO_VL_DEVOLUCAO
                            ,'{$Objeto->getOrigemVenda()}'                                                  W_NTO_NU_ORIGEM_VENDA
                            ,{$Objeto->getCentroDist()}                                                     W_NTO_CO_CDIST
                            ,'{$Objeto->getDivisao()}'                                                      W_NTO_CO_DIVISAO
                        FROM DUAL
                ) ON (          NTO.DOC_CO_NUMERO = W_DOC_CO_NUMERO
                          AND   NTO.NTO_NU_SERIE  = W_NTO_NU_SERIE
                          AND   NTO.NTO_NU_NOTA   = W_NTO_NU_NOTA
                          AND   NTO.CLI_CO_NUMERO = W_CLI_CO_NUMERO
                          AND   NTO.NTO_DT_NOTA =   W_NTO_DT_NOTA

                    )
                WHEN MATCHED THEN
                        UPDATE SET
                          NTO_VL_DEVOLUCAO    = W_NTO_VL_DEVOLUCAO
                         ,NTO_NU_ORIGEM_VENDA = W_NTO_NU_ORIGEM_VENDA
                         ,NTO_CO_CDIST        = W_NTO_CO_CDIST
                         ,NTO_CO_DIVISAO      = W_NTO_CO_DIVISAO
                WHEN NOT MATCHED THEN
                        INSERT (
                        
                             NTO.DOC_CO_NUMERO
                            ,NTO.NTO_NU_SERIE
                            ,NTO.NTO_NU_NOTA
                            ,NTO.NTO_DT_NOTA
                            ,NTO.CHDOC
                            ,NTO.CLI_CO_NUMERO
                            ,NTO.NTO_VL_DEVOLUCAO
                            ,NTO.NTO_NU_ORIGEM_VENDA
                            ,NTO.NTO_CO_CDIST
                            ,NTO.NTO_CO_DIVISAO

                        )  VALUES (
                             W_DOC_CO_NUMERO
                            ,W_NTO_NU_SERIE
                            ,W_NTO_NU_NOTA
                            ,W_NTO_DT_NOTA
                            ,W_CHDOC
                            ,W_CLI_CO_NUMERO
                            ,W_NTO_VL_DEVOLUCAO
                            ,W_NTO_NU_ORIGEM_VENDA
                            ,W_NTO_CO_CDIST
                            ,W_NTO_CO_DIVISAO
                        ) 
                    ";

            //echo $sql;      
            if (!$this->conn->execSql($sql)) {
                return false;
            }
        }

        return true;
    }

    public function consultaNotasDevolucao(Ocorrencia $Ocorrencia) {
        $sql = "
                    SELECT DOC_CO_NUMERO,
                        NTO_NU_SERIE,
                        NTO_NU_NOTA,
                        TO_CHAR(NTO_DT_NOTA,'DD/MM/YYYY') NTO_DT_NOTA,
                        NTO_CO_RESPONSAVEL,
                        NTO_IN_ORIGEM_RESPONSAVEL,
                        OCO_CO_NUMERO,
                        NTO_TX_RESPONSAVEL,
                        TRP_CO_NUMERO,
                        CHDOC,
                        CLI_CO_NUMERO,
                        NTO_VL_DEVOLUCAO,
                        NTO_NU_ORIGEM_VENDA,
                        NTO_CO_CDIST
                    FROM DEV_NOTAORIGEM 
                    WHERE  DOC_CO_NUMERO = {$Ocorrencia->getCodigoDevolucao()}	
		";

        $result = $this->conn->execSql($sql);
        while ($notas = oci_fetch_object($result)) {
            $DevNotaOrigem = new DevNotaOrigem();
            $DevNotaOrigem->setCodOcorrencia($notas->DOC_CO_NUMERO);
            $DevNotaOrigem->setSerie($notas->NTO_NU_SERIE);
            $DevNotaOrigem->setNota($notas->NTO_NU_NOTA);
            $DevNotaOrigem->setData($notas->NTO_DT_NOTA);
            $DevNotaOrigem->setResponsavel($notas->NTO_CO_RESPONSAVEL);
            $DevNotaOrigem->setOrigem($notas->NTO_IN_ORIGEM_RESPONSAVEL);
            $DevNotaOrigem->setDescricaoResponsavel($notas->NTO_TX_RESPONSAVEL);
            $DevNotaOrigem->setMotivoResponsabilidade($notas->TRP_CO_NUMERO);
            $DevNotaOrigem->setCodCliente($notas->CLI_CO_NUMERO);
            $DevNotaOrigem->setValorTotDevolucao($notas->NTO_VL_DEVOLUCAO);
            $DevNotaOrigem->setOrigemVenda($notas->NTO_NU_ORIGEM_VENDA);
            $DevNotaOrigem->setCentroDist($notas->NTO_CO_CDIST);

            $ObjDevNotaOrigem[] = $DevNotaOrigem;
        }
        return $ObjDevNotaOrigem;
    }

    public function verificaNotacadastrada($codOcorrencia, $cli_co_numero = null, $serie = null, $numNota = null) {

        $sql = "
                    SELECT count(*) QTD 
                    FROM   DEV_NOTAORIGEM
                    WHERE  DOC_CO_NUMERO = {$codOcorrencia}";

        if ($serie) {
            $sql .= " 
                    AND    NTO_NU_SERIE  = {$serie}
                    AND    NTO_NU_NOTA   = {$numNota}
                    AND    CLI_CO_NUMERO = {$cli_co_numero}
		";
        }

        $result = $this->conn->execSql($sql);
        OCIFetchInto($result, $row, OCI_ASSOC);

        if ($row['QTD'] > 0) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function verificaOcocadastrada($codOcorrencia) {

        $sql = "
                    SELECT count(*) QTD 
                    FROM   DEV_OCORRENCIA
                    WHERE  DOC_CO_NUMERO = {$codOcorrencia}

		";

        $result = $this->conn->execSql($sql);
        OCIFetchInto($result, $row, OCI_ASSOC);

        if ($row['QTD'] > 0) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function filtroCentroDis() {

        $sql = "    SELECT * 
                        FROM GLOBAL.GLB_CENTRO_DISTRIBUICAO
                        ORDER BY CED_NO_CENTRO";


        $result = $this->conn->execSql($sql);
        while ($centro = oci_fetch_object($result)) {
            $GlbCentroDistribuicao = new GlbCentroDistribuicao();
            $GlbCentroDistribuicao->setCodigo($centro->CED_CO_NUMERO);
            $GlbCentroDistribuicao->setNome($centro->CED_NO_CENTRO);
            $GlbCentroDistribuicao->setDestinatario($centro->CED_NO_DESTINATARIO);
            $GlbCentroDistribuicao->setCnpj($centro->CED_NU_CNPJ);
            $GlbCentroDistribuicao->setEndereco($centro->CED_NO_ENDERECO);
            $GlbCentroDistribuicao->setEnderecoNumero($centro->CED_ED_NUMERO);
            $GlbCentroDistribuicao->setBairro($centro->CED_NO_BAIRRO);
            $GlbCentroDistribuicao->setLocalidade($centro->LOC_CO_NUMERO);
            $GlbCentroDistribuicao->setEstado($centro->EST_SG_ESTADO);
            $GlbCentroDistribuicao->setCep($centro->CED_NU_CEP);

            $ObjGlbCentroDistribuicao[] = $GlbCentroDistribuicao;
        }
        return $ObjGlbCentroDistribuicao;
    }	

	public function consultaOcorrenciaRomaneio(Ocorrencia $Ocorrencia, $dataInicial = null, $dataFinal = null, $InRota) {

		$sql = "SELECT
					OCO.DOC_CO_NUMERO OCORRENCIA,
					TO_CHAR(OCO.DOC_DT_CADASTRO,'dd/mm/yyyy') DATA_CADASTRO,
					TO_CHAR(DOP.DOP_DT_SAIDA,'dd/mm/yyyy') DATA_TRANSPORTADOR,
					OCO.ROT_CO_NUMERO AS ROTA, 
					OCO.DOC_IN_TIPO COD_TIPO,
					TID.TID_CO_DESCRICAO,
					OCO.DOC_NU_DEVSERIE || ' '|| OCO.DOC_NU_DEVNOTA NOTA_DEVOLUCAO ,
					CED.CED_NO_CENTRO,
					DOP.DOP_QT_VOLUME VOLUME,
					CLI.CLI_CO_NUMERO CODIGO_CLIENTE, 
					CLI.CLI_NO_RAZAO_SOCIAL CLIENTE,
					TOC.TOC_NO_DESCRICAO
				FROM DEV_OCORRENCIA OCO
				INNER JOIN GLOBAL.GLB_CLIENTE             CLI ON CLI.CLI_CO_NUMERO = OCO.CLI_CO_NUMERO				
				INNER JOIN DEV_OCORRENCIA_PASSO           DOP ON DOP.DOC_CO_NUMERO = OCO.DOC_CO_NUMERO AND DOP.DPA_CO_NUMERO = 2 AND DOP.DOP_IN_APROVADO = 1
				LEFT JOIN GLOBAL.GLB_USUARIO			  USU ON USU.USU_CO_NUMERO = DOP.USU_CO_NUMERO
				INNER JOIN DEV_TIPO_DEVOLUCAO             TID ON TID.TID_CO_NUMERO = OCO.DOC_IN_TIPO
				LEFT  JOIN GLOBAL.GLB_CENTRO_DISTRIBUICAO CED ON CED.CED_CO_NUMERO = OCO.CED_CO_NUMERO
				INNER JOIN CAL_TIPO_OCORRENCIA            TOC ON TOC.TOC_CO_NUMERO = OCO.TOC_CO_NUMERO AND TOC.DEP_CO_NUMERO = 29
				where USU.USU_IN_TIPO <> 'INT' AND OCO.DOC_CO_NUMERO NOT IN(SELECT DOC_CO_NUMERO FROM DEV_OCORRENCIA_PASSO WHERE DOC_CO_NUMERO = OCO.DOC_CO_NUMERO AND DPA_CO_NUMERO = 3 AND DOP_IN_APROVADO = 1)
				";

		if (($dataInicial != "") && ($dataFinal != "")) {
			$sql .= " 
					AND   DOP.DOP_DT_SAIDA BETWEEN TO_DATE ('{$dataInicial} 00:00:00', 'DD/MM/YYYY HH24:MI:SS') AND  TO_DATE ('{$dataFinal} 23:59:59', 'DD/MM/YYYY HH24:MI:SS')
					";
		}

		if (($Ocorrencia->getCedis()) && ($Ocorrencia->getCedis() != "TOD")) {
			$sql .= " AND OCO.CED_CO_NUMERO = {$Ocorrencia->getCedis()}";
		}
		
		if ($InRota != "") {
			$InRota = str_replace("|","'",$InRota);
			$sql .=" AND OCO.rot_co_numero in( {$InRota} ) ";
		}		

		$sql .= " ORDER BY OCO.DOC_CO_NUMERO ASC";

		if ($result = $this->conn->execSql($sql)) {
			return $result;
		}
		return false;
	}	
	
	public function consultaOcorrenciaRomaneioBaixa($codRomaneio) {
		$sql = "SELECT
					OCO.DOC_CO_NUMERO OCORRENCIA,
					TO_CHAR(OCO.DOC_DT_CADASTRO,'dd/mm/yyyy') DATA_CADASTRO,
					TO_CHAR(DOP.DOP_DT_SAIDA,'dd/mm/yyyy') DATA_TRANSPORTADOR,
					OCO.ROT_CO_NUMERO AS ROTA, 
					OCO.DOC_IN_TIPO COD_TIPO,
					TID.TID_CO_DESCRICAO,
					OCO.DOC_NU_DEVSERIE || ' '|| OCO.DOC_NU_DEVNOTA NOTA_DEVOLUCAO ,
					CED.CED_NO_CENTRO,
					DOP.DOP_QT_VOLUME VOLUME,
					CLI.CLI_CO_NUMERO CODIGO_CLIENTE, 
					CLI.CLI_NO_RAZAO_SOCIAL CLIENTE,
					TOC.TOC_NO_DESCRICAO,
					OCO.DPA_CO_NUMERO PASSO_ATUAL,
					(SELECT OP.DOP_QT_VOLUME FROM DEV_OCORRENCIA_PASSO OP WHERE OP.DOC_CO_NUMERO = OCO.DOC_CO_NUMERO AND OP.DPA_CO_NUMERO = 3) AS VOLUME_RECEBIMENTO,
					DOR.DOR_IN_BAIXA
				FROM DEV_OCORRENCIA OCO
				INNER JOIN DEV_OCORRENCIA_ROMANEIO		  DOR ON DOR.DOC_CO_NUMERO = OCO.DOC_CO_NUMERO
				INNER JOIN GLOBAL.GLB_CLIENTE             CLI ON CLI.CLI_CO_NUMERO = OCO.CLI_CO_NUMERO
				INNER JOIN DEV_OCORRENCIA_PASSO           DOP ON DOP.DOC_CO_NUMERO = OCO.DOC_CO_NUMERO AND DOP.DPA_CO_NUMERO = 2 AND DOP.DOP_IN_APROVADO = 1
				INNER JOIN DEV_TIPO_DEVOLUCAO             TID ON TID.TID_CO_NUMERO = OCO.DOC_IN_TIPO
				LEFT  JOIN GLOBAL.GLB_CENTRO_DISTRIBUICAO CED ON CED.CED_CO_NUMERO = OCO.CED_CO_NUMERO
				INNER JOIN CAL_TIPO_OCORRENCIA            TOC ON TOC.TOC_CO_NUMERO = OCO.TOC_CO_NUMERO AND TOC.DEP_CO_NUMERO = 29
				where DOR.DRO_CO_NUMERO = {$codRomaneio}
				";

		$sql .= " ORDER BY OCO.DOC_CO_NUMERO";


		if ($result = $this->conn->execSql($sql)) {
			return $result;
		}
		return false;
	}		

    public function consultaOcorrenciaPrincipal(Ocorrencia $Ocorrencia, $dataInicial = null, $dataFinal = null, $serie = null, $nota = null, $rede = null, $valorInicial = null, $valorFinal = null, GlbUsuario $ObjUsuario) {

        $sql = "    
                    SELECT DOC.DOC_CO_NUMERO,
                           DOC.DPA_CO_NUMERO,
                           DPA.DPA_NO_DESCRICAO,
                           DOC.DST_CO_NUMERO,
                           DST.DST_NO_DESCRICAO,
                           DOC.DOC_DT_CADASTRO,
                           TO_CHAR(DOC.DOC_DT_CADASTRO, 'DD/MM/YYYY') DATA_EXIBICAO,
                           DOC.DOC_NO_CONTATO,
                           DOC.DOC_CO_CADASTRANTE,
                           DOC.DOC_NO_DEPOCONTATO,
                           USU.USU_NO_USERNAME,
                           DOC.CED_CO_NUMERO,
                           CED.CED_NO_CENTRO,
                           DOC.TOC_CO_NUMERO,
                           TOC.TOC_NO_DESCRICAO,
                           DOC.DOC_IN_TIPO,
                           TID.TID_CO_DESCRICAO,
                           TID.TID_IN_DEVOLUCAO,
                           NVL(DOC.DOC_VR_VALOR, 0) DOC_VR_VALOR,
                           CLI.CLI_CO_NUMERO,
                           CLI.CLI_NO_RAZAO_SOCIAL,
                           CLI.CLI_NO_FANTASIA,
                           DOC.DOC_TX_DESCRICAO,
                           DOC.ROT_CO_NUMERO,
                           DOC.DOC_NO_EMAILCOLETA,
                           TO_CHAR(DOC.DOC_DT_CONFERENCIA, 'DD/MM/YYYY') DOC_DT_CONFERENCIA,
                           TO_CHAR(DOC.DOC_DT_DEVRECEBIMENTO, 'DD/MM/YYYY') DOC_DT_DEVRECEBIMENTO,
                           DOC.DOC_NU_DEVSERIE,
                           DOC.DOC_NU_DEVNOTA,
                           TO_CHAR(DOC.DOC_DT_DEVNOTA, 'DD/MM/YYYY') DOC_DT_DEVNOTA,
                           DOC.DOC_QT_PRODUTO,
                           DECODE(DOC.DEV_CO_DIVISAO, 'F', 'FARMA', 'H', 'HOSPITALAR', 'A', 'ALIMENTAR', 'O', 'DIST DIRETA', DOC.DEV_CO_DIVISAO) DEV_CO_DIVISAO
                    FROM DEV_OCORRENCIA                       DOC
                    LEFT  JOIN DEV_STATUS                     DST ON DST.DST_CO_NUMERO = DOC.DST_CO_NUMERO
                    LEFT  JOIN DEV_PASSO                      DPA ON DPA.DPA_CO_NUMERO = DOC.DPA_CO_NUMERO
                    LEFT  JOIN GLOBAL.GLB_CENTRO_DISTRIBUICAO CED ON CED.CED_CO_NUMERO = DOC.CED_CO_NUMERO
                    INNER JOIN CAL_TIPO_OCORRENCIA            TOC ON TOC.TOC_CO_NUMERO = DOC.TOC_CO_NUMERO AND TOC.DEP_CO_NUMERO = 29 AND TOC.SLA_CO_NUMERO = 1
                    INNER JOIN DEV_TIPO_DEVOLUCAO             TID ON TID.TID_CO_NUMERO = DOC.DOC_IN_TIPO
                    INNER JOIN GLOBAL.GLB_CLIENTE             CLI ON CLI.CLI_CO_NUMERO = DOC.CLI_CO_NUMERO
                    INNER JOIN GLOBAL.GLB_USUARIO             USU ON USU.USU_CO_NUMERO = DOC.DOC_CO_CADASTRANTE
                    WHERE 1=1
                    
                ";

        if ($Ocorrencia->getCodigoDevolucao()) {
            $sql .= " 
                    AND  DOC.DOC_CO_NUMERO = {$Ocorrencia->getCodigoDevolucao()} 
                    ";
        }

        if (($dataInicial != "") && ($dataFinal != "")) {
            $sql .= " 
                    AND   DOC.DOC_DT_CADASTRO BETWEEN TO_DATE ('{$dataInicial} 00:00:00', 'DD/MM/YYYY HH24:MI:SS') AND  TO_DATE ('{$dataFinal} 23:59:59', 'DD/MM/YYYY HH24:MI:SS')
                    ";
        }

        if ($Ocorrencia->getGlbCliente()->getClienteCodigo()) {
            $sql .=" AND DOC.cli_co_numero ={$Ocorrencia->getGlbCliente()->getClienteCodigo()}";
        }

        if ($Ocorrencia->getNotaNfDevolucao() && $Ocorrencia->getSerieNfDevolucao()) {
            $sql .= " AND DOC.DOC_NU_DEVSERIE = '{$Ocorrencia->getSerieNfDevolucao()}' 
                      AND DOC.DOC_NU_DEVNOTA  = '{$Ocorrencia->getNotaNfDevolucao()}'";
        }

        if (($serie != "") && ($nota != "")) {
            $sql .= " AND DOC.DOC_CO_NUMERO IN (SELECT NTO.DOC_CO_NUMERO FROM DEV_NOTAORIGEM NTO 
							WHERE   NTO.NTO_NU_SERIE = {$serie} 
							AND   NTO.NTO_NU_NOTA  = {$nota}
							)
                    ";
        }

        if ($rede) {
            $sql .=" AND CLI.RED_CO_NUMERO ={$rede}";
        }

        if (($Ocorrencia->getCedis()) && ($Ocorrencia->getCedis() != "TOD")) {
            $sql .= " AND DOC.CED_CO_NUMERO = {$Ocorrencia->getCedis()}";
        }

        if (($Ocorrencia->getObjRota()->getCodigoRota()) && ($Ocorrencia->getObjRota()->getCodigoRota() != "TOD")) {
            $sql .="AND DOC.rot_co_numero = '{$Ocorrencia->getObjRota()->getCodigoRota()}'";
        }

        if (($valorInicial != "") && ($valorFinal != "")) {

            $valorInicial = str_replace(',', '.', str_replace('.', '', $valorInicial));
            $valorFinal = str_replace(',', '.', str_replace('.', '', $valorFinal));
            $sql .= " 
                    AND   DOC.DOC_VR_VALOR BETWEEN {$valorInicial} AND {$valorFinal}
                    ";
        }

        if (($Ocorrencia->getStatusOcorrencia()) && ($Ocorrencia->getStatusOcorrencia()) != 'TOD') {
            $sql .="AND DOC.DST_CO_NUMERO = {$Ocorrencia->getStatusOcorrencia()}";
        }

        if (($Ocorrencia->getPassoOcorrencia()) && ($Ocorrencia->getPassoOcorrencia() != 'TOD')) {
            $sql .="AND DOC.DPA_CO_NUMERO = {$Ocorrencia->getPassoOcorrencia()}";
        }


        if (($Ocorrencia->getTipoOcorrencia()) && ($Ocorrencia->getTipoOcorrencia() != 'T')) {
            $sql .="AND DOC.DOC_IN_TIPO = {$Ocorrencia->getTipoOcorrencia()}";
        }
        
        if ($ObjUsuario->getUsuarioTipo() == "CLI") {

            $sql .=" AND DOC.CLI_CO_NUMERO = {$ObjUsuario->getObjCliente()->getClienteCodigo()}";
        } else if ($ObjUsuario->getUsuarioTipo() == "VEN") {
            
            $VendedorUsuario = $ObjUsuario->getObjVendedor();

            $sql .= " AND DOC.CLI_CO_NUMERO IN (
                        SELECT DISTINCT CLI.CLI_CO_NUMERO
                         FROM GLOBAL.GLB_CLIENTE_VENDEDOR CVEN 
                         INNER JOIN GLOBAL.GLB_VENDEDOR VEN ON VEN.VEN_CO_NUMERO = CVEN.VEN_CO_NUMERO
                         INNER JOIN GLOBAL.GLB_CLIENTE  CLI ON CLI.CLI_CO_NUMERO = CVEN.CLI_CO_NUMERO
                         WHERE 
                               CVEN.CLV_IN_STATUS IS NULL ";

            if (($VendedorUsuario[0]->getTipo() == "V") || ($VendedorUsuario[0]->getTipo() == "N")) {
                $sql .= " AND VEN.VEN_CO_SUPERVISOR = {$VendedorUsuario[0]->getCodigo()} ";
            } else {
                $sql .= " AND VEN.VEN_CO_NUMERO = {$VendedorUsuario[0]->getCodigo()} ";
            }
            
            $sql .= ")";
        } else if ($ObjUsuario->getUsuarioTipo() == "TRA") {
            
            $sql .= " AND DOC.ROT_CO_NUMERO IN (
                        SELECT DISTINCT ROT_CO_NUMERO FROM USUARIO_ROTA WHERE USU_CO_NUMERO = {$ObjUsuario->getUsuarioCodigo()}
                        )";
            
        }
        

        $sql .= "
                 ORDER BY DOC.DOC_DT_CADASTRO
                ";

        if ($result = $this->conn->execSql($sql)) {
            return $result;
        }
        return false;
    }
    	
	public function consultaOcorrenciaNF($codOcorrencia) {
		$sql = "SELECT
					NTO.NTO_NU_SERIE,
					NTO.NTO_NU_NOTA,
					TO_CHAR(NTO.NTO_DT_NOTA, 'DD/MM/YYYY') NTO_DT_NOTA
				FROM DEV_NOTAORIGEM NTO
				where NTO.DOC_CO_NUMERO = {$codOcorrencia}
				";

		if ($result = $this->conn->execSql($sql)) {
			return $result;
		}
		return false;
	}		
    
    public function consultaOcorrencia(Ocorrencia $Ocorrencia, $dataInicial = null, $dataFinal = null, $serie = null, $nota = null, $rede = null, $valorInicial = null, $valorFinal = null) {

        $sql = "    
                    SELECT DOC.DOC_CO_NUMERO,
                           DOC.DPA_CO_NUMERO,
                           DPA.DPA_NO_DESCRICAO,
                           DOC.DST_CO_NUMERO,
                           DST.DST_NO_DESCRICAO,
                           DOC.DOC_DT_CADASTRO,
                           TO_CHAR(DOC.DOC_DT_CADASTRO, 'DD/MM/YYYY') DATA_EXIBICAO,
                           DOC.DOC_NO_CONTATO,
                           DOC.DOC_CO_CADASTRANTE,
                           DOC.DOC_NO_DEPOCONTATO,
                           USU.USU_NO_USERNAME,
                           DOC.CED_CO_NUMERO,
                           CED.CED_NO_CENTRO,
                           DOC.TOC_CO_NUMERO,
                           TOC.TOC_NO_DESCRICAO,
                           DOC.DOC_IN_TIPO,
                           TID.TID_CO_DESCRICAO,
                           TID.TID_IN_DEVOLUCAO,
                           NVL(DOC.DOC_VR_VALOR, 0) DOC_VR_VALOR,
                           CLI.CLI_CO_NUMERO,
                           CLI.CLI_NO_RAZAO_SOCIAL,
                           CLI.CLI_NO_FANTASIA,
                           DOC.DOC_TX_DESCRICAO,
                           DOC.ROT_CO_NUMERO,
                           DOC.DOC_NO_EMAILCOLETA,
                           TO_CHAR(DOC.DOC_DT_CONFERENCIA, 'DD/MM/YYYY') DOC_DT_CONFERENCIA,
                           TO_CHAR(DOC.DOC_DT_DEVRECEBIMENTO, 'DD/MM/YYYY') DOC_DT_DEVRECEBIMENTO,
                           DOC.DOC_NU_DEVSERIE,
                           DOC.DOC_NU_DEVNOTA,
                           TO_CHAR(DOC.DOC_DT_DEVNOTA, 'DD/MM/YYYY') DOC_DT_DEVNOTA,
                           DOC.DOC_QT_PRODUTO,
                           DECODE(DOC.DEV_CO_DIVISAO, 'F', 'FARMA', 'H', 'HOSPITALAR', 'A', 'ALIMENTAR', 'O', 'DIST DIRETA', DOC.DEV_CO_DIVISAO) DEV_CO_DIVISAO,
                           NTO.NTO_NU_SERIE,
                           NTO.NTO_NU_NOTA
                    FROM DEV_OCORRENCIA                       DOC
                    INNER JOIN DEV_NOTAORIGEM                 NTO ON NTO.DOC_CO_NUMERO = DOC.DOC_CO_NUMERO
                    LEFT  JOIN DEV_STATUS                     DST ON DST.DST_CO_NUMERO = DOC.DST_CO_NUMERO
                    LEFT  JOIN DEV_PASSO                      DPA ON DPA.DPA_CO_NUMERO = DOC.DPA_CO_NUMERO
                    LEFT  JOIN GLOBAL.GLB_CENTRO_DISTRIBUICAO CED ON CED.CED_CO_NUMERO = DOC.CED_CO_NUMERO
                    INNER JOIN CAL_TIPO_OCORRENCIA            TOC ON TOC.TOC_CO_NUMERO = DOC.TOC_CO_NUMERO AND TOC.DEP_CO_NUMERO = 29 AND TOC.SLA_CO_NUMERO = 1
                    INNER JOIN DEV_TIPO_DEVOLUCAO             TID ON TID.TID_CO_NUMERO = DOC.DOC_IN_TIPO
                    INNER JOIN GLOBAL.GLB_CLIENTE             CLI ON CLI.CLI_CO_NUMERO = DOC.CLI_CO_NUMERO
                    INNER JOIN GLOBAL.GLB_USUARIO             USU ON USU.USU_CO_NUMERO = DOC.DOC_CO_CADASTRANTE
                    WHERE 1=1
                    
                ";

        if ($Ocorrencia->getCodigoDevolucao()) {
            $sql .= " 
                    AND  DOC.DOC_CO_NUMERO = {$Ocorrencia->getCodigoDevolucao()} 
                    ";
        }

        if (($dataInicial != "") && ($dataFinal != "")) {
            $sql .= " 
                    AND   DOC.DOC_DT_CADASTRO BETWEEN TO_DATE ('{$dataInicial} 00:00:00', 'DD/MM/YYYY HH24:MI:SS') AND  TO_DATE ('{$dataFinal} 23:59:59', 'DD/MM/YYYY HH24:MI:SS')
                    ";
        }

        if ($Ocorrencia->getGlbCliente()->getClienteCodigo()) {
            $sql .=" AND DOC.cli_co_numero ={$Ocorrencia->getGlbCliente()->getClienteCodigo()}";
        }

        if ($Ocorrencia->getNotaNfDevolucao() && $Ocorrencia->getSerieNfDevolucao()) {
            $sql .= " AND DOC.DOC_NU_DEVSERIE = '{$Ocorrencia->getSerieNfDevolucao()}' 
                      AND DOC.DOC_NU_DEVNOTA  = '{$Ocorrencia->getNotaNfDevolucao()}'";
        }

        if (($serie != "") && ($nota != "")) {
            $sql .= " 
                             AND   NTO.NTO_NU_SERIE = {$serie}
                             AND   NTO.NTO_NU_NOTA  = {$nota}
                    ";
        }

        if ($rede) {
            $sql .=" AND CLI.RED_CO_NUMERO ={$rede}";
        }

        if (($Ocorrencia->getCedis()) && ($Ocorrencia->getCedis() != "TOD")) {
            $sql .= " AND DOC.CED_CO_NUMERO = {$Ocorrencia->getCedis()}";
        }

        if (($Ocorrencia->getObjRota()->getCodigoRota()) && ($Ocorrencia->getObjRota()->getCodigoRota() != "TOD")) {
            $sql .=" AND DOC.rot_co_numero = '{$Ocorrencia->getObjRota()->getCodigoRota()}'";
        }

        if (($valorInicial != "") && ($valorFinal != "")) {

            $valorInicial = str_replace(',', '.', str_replace('.', '', $valorInicial));
            $valorFinal = str_replace(',', '.', str_replace('.', '', $valorFinal));
            $sql .= " 
                    AND   DOC.DOC_VR_VALOR BETWEEN {$valorInicial} AND {$valorFinal}
                    ";
        }

        if (($Ocorrencia->getStatusOcorrencia()) && ($Ocorrencia->getStatusOcorrencia()) != 'TOD') {
            $sql .=" AND DOC.DST_CO_NUMERO = {$Ocorrencia->getStatusOcorrencia()}";
        }

        if (($Ocorrencia->getPassoOcorrencia()) && ($Ocorrencia->getPassoOcorrencia() != 'TOD')) {
            $sql .=" AND DOC.DPA_CO_NUMERO = {$Ocorrencia->getPassoOcorrencia()}";
        }

        $sql .= "
                 ORDER BY DOC.DOC_DT_CADASTRO
                ";

        if ($result = $this->conn->execSql($sql)) {
            return $result;
        }
        return false;
    }

    public function detalhesNotaVenda($nto_nu_serie, $nto_nu_nota, $nto_dt_nota, $cli_co_numero) {

        $sql = "SELECT 
                  nto.chdoc,
                  nto.dtmov,
                  nto.CDFILIAL,
                  nto.CDEQUIPE,
                  nto.CDSETOR,
                  nto.CDOPERSERV,
                  nto.CDOPERGLOBAL,
                  nto.CDCLIENTE,
                  nto.tporigvenda,
                  ori.nome AS origem_venda,
                  cli.cli_no_razao_social AS razao_social,
                  equ.noequipe                
                FROM uservimed.notavenda nto
                INNER JOIN global.glb_cliente cli ON cli.cli_co_numero = nto.cdcliente
                INNER JOIN uservimed.tp_origemvenda ori ON trim(ori.tporigvenda) = trim(nto.tporigvenda)
                INNER JOIN uservimed.equipe equ ON equ.cdsetor = nto.cdsetor AND equ.cdequipe = nto.cdequipe AND equ.ano = TO_CHAR(dtmov,'YYYY') AND equ.mes = TO_CHAR(dtmov,'MM')               
                WHERE SERIE = $nto_nu_serie
                  AND NUMNOTA = $nto_nu_nota
                  AND dtmov = TO_DATE('$nto_dt_nota','dd/mm/yyyy')
                  AND nto.cdcliente = '$cli_co_numero'
                group by nto.chdoc,
                  nto.dtmov,
                  nto.CDFILIAL,
                  nto.CDEQUIPE,
                  nto.CDSETOR,
                  nto.CDOPERSERV,
                  nto.CDOPERGLOBAL,
                  nto.CDCLIENTE,
                  nto.tporigvenda,
                  ori.nome,
                  cli.cli_no_razao_social,
                  equ.noequipe  
        ";
        return $this->conn->execSql($sql);
    }

    function industria() {
        $sql = "SELECT * FROM global.GLB_FABRICANTE FAB
                    WHERE
                      ( SELECT COUNT(1) 
                                FROM global.GLB_PRODUTO PRO 
                                WHERE FAB.FAB_CO_NUMERO = PRO.FAB_CO_NUMERO )>0     
                      ORDER BY FAB_NO_RAZAO_SOCIAL";

        $result = $this->conn->execSql($sql);
        if ($result) {
            return $result;
        }
        return false;
    }

    public function consultaOperadorFaturamento($tpu_co_tipo_usuario, $ope_co_operador) {
        $sql = "SELECT *
                 FROM  uservimed.sys_operador
                WHERE 
                 tpu_co_tipo_usuario = $tpu_co_tipo_usuario
                 AND OPE_CO_OPERADOR = $ope_co_operador ";
        if ($tpu_co_tipo_usuario == 2) {
            $sql.=" AND etr_co_estrutura = 1 ";
        }

        if ($tpu_co_tipo_usuario == 3) {
            $sql.=" AND etr_co_estrutura in (5,6,3,15,61,62)";
        }

        return $this->conn->execSql($sql);
    }

    public function consultaNomeOperador($ven_co_numero) {

        $sql = "SELECT VEN_NO_PSEUDONIMO
                FROM GLOBAL.GLB_VENDEDOR
                WHERE VEN_CO_NUMERO = {$ven_co_numero}";

        $result = $this->conn->execSql($sql);
        OCIFetchInto($result, $row, OCI_ASSOC);
        return $row['VEN_NO_PSEUDONIMO'];
    }

    public function consultaOperadorHospitalar($cdopersev, $nto_dt_nota) {
        $sql = "SELECT usu.nome,
                    usu.cdusuario
                FROM uservimed.ceh_usuario_bases uba
                    INNER JOIN uservimed.usuario usu
                    ON usu.cdusuario    = uba.usu_co_numero
                WHERE rpr_co_numero = $cdopersev
                    AND to_date('$nto_dt_nota','DD/MM/YYYY') BETWEEN uba_dt_inicio AND uba_dt_fim                   
                    AND uba_in_base ='B'
                    AND usu.status is null 
        ";
        return $this->conn->execSql($sql);
    }

    public function atribuirResponsabilidade(Ocorrencia $ObjDevolucao) {

        $ObjetoNota = $ObjDevolucao->getDevNotaOrigem();

        foreach ($ObjetoNota as $Objeto) {
            $sql = "UPDATE dev_notaorigem set 
                    nto_co_responsavel ='{$Objeto->getResponsavel()}'
                    ,nto_in_origem_responsavel ='{$Objeto->getOrigem()}'
                    ,nto_tx_responsavel = '{$Objeto->getDescricaoResponsavel()}'
                    ,trp_co_numero = {$Objeto->getMotivoResponsabilidade()->getCodigo()}
                 WHERE DOC_CO_NUMERO = {$ObjDevolucao->getCodigoDevolucao()} 
                    AND nto_nu_serie = '{$Objeto->getSerie()}'
                    AND nto_nu_nota = '{$Objeto->getNota()}'
                ";
        }

        if ($this->conn->execSql($sql)) {
            return true;
        }
        return false;
    }

    public function diasUteisNota($dtEmissao) {
        $dtAtual = date('d/m/Y');
        $sql = "            
                    SELECT COUNT(*)  AS DIAS_UTEIS
                    FROM USERVIMED.CALENDARIO 
                    WHERE DATDIA BETWEEN TO_DATE('{$dtEmissao}', 'dd/mm/yyyy')+1 AND TO_DATE('{$dtAtual}', 'dd/mm/yyyy')
                    AND   IDEDIA = 'U'
        ";

        $result = $this->conn->execSql($sql);
        OCIFetchInto($result, $row, OCI_ASSOC);

        return $row['DIAS_UTEIS'];
    }

    function ContatosClienteDevolucao($cli_co_numero) {

        $sql = "
                SELECT * 
                FROM GLOBAL.VIEW_CLIENTE_CONTATO 
                WHERE CON_TX_EMAIL IS NOT NULL
                AND   CLI_CO_NUMERO = {$cli_co_numero}
                 ";

        $sql .= "ORDER BY CON_NO_CONTATO ASC";

        if ($result = $this->conn->execSql($sql)) {
            return $result;
        }
        return false;
    }

    function consultaOrdemColeta(Ocorrencia $ObjOcorrencia) {

        $departamento = 29;
        $sala = 1;
        
        $sql = "     
                SELECT   CED.CED_NO_DESTINATARIO
                        ,CED.CED_NO_ENDERECO
                        ,CED.CED_NO_BAIRRO
                        ,CED.CED_NU_CNPJ
                        ,CED.CED_ED_NUMERO
                        ,LOC_CED.LOC_NO_CIDADE CIDADE_CED
                        ,CED.CED_NO_CENTRO
                        ,CED.CED_NU_CEP
                        ,CED.EST_SG_ESTADO     ESTADO_CED
                        ,DEV.DOC_CO_NUMERO
                        ,TO_CHAR(DEV.DOC_DT_CADASTRO, 'DD/MM/YYYY') DOC_DT_CADASTRO
                        ,DEV.CLI_CO_NUMERO
                        ,CLI.CLI_NO_RAZAO_SOCIAL
                        ,DEV.ROT_CO_NUMERO
                        ,CLI.CLI_NU_ROTA_TERCEIRIZADA
                        ,DEV.DOC_NO_CONTATO
                        ,DEV.DOC_NO_DEPOCONTATO
                        ,CLI.CLI_ED_LOGRADOURO
                        ,CLI.CLI_ED_NUMERO
                        ,CLI.CLI_NO_BAIRRO
                        ,CLI.CLI_NU_CEP
                        ,CLI.LOC_CO_NUMERO
                        ,CLI.CLI_NU_CNPJ_CPF
                        ,LOC_CLI.LOC_NO_CIDADE CIDADE_CLIENTE
                        ,LOC_CLI.EST_SG_ESTADO ESTADO_CLIENTE
                        ,DEV.DOC_QT_PRODUTO
                        ,DEV.DOC_IN_TIPO
                        ,TID.TID_CO_DESCRICAO
                        ,TOC.TOC_NO_DESCRICAO
                        ,DEV.DOC_TX_DESCRICAO
                        ,DEV.DOC_NU_DEVSERIE
                        ,DEV.DOC_NU_DEVNOTA
                        ,TO_CHAR(DEV.DOC_DT_CADASTRO, 'DD/MM/YYYY') DOC_DT_CADASTRO
                        ,TO_CHAR(DEV.DOC_DT_DEVNOTA, 'DD/MM/YYYY') DOC_DT_DEVNOTA
                FROM  DEV_OCORRENCIA DEV
                INNER JOIN DEV_NOTAORIGEM NTO ON DEV.DOC_CO_NUMERO  = NTO.DOC_CO_NUMERO
                LEFT JOIN GLOBAL.GLB_CLIENTE CLI ON CLI.CLI_CO_NUMERO = DEV.CLI_CO_NUMERO
                INNER JOIN USERVIMED.NOTAVENDA NTV ON  NTV.SERIE = NTO.NTO_NU_SERIE AND NTV.NUMNOTA = NTO.NTO_NU_NOTA 
                                         AND NTV.CDCLIENTE = DEV.CLI_CO_NUMERO 
                                         AND NTV.DTMOV = NTO.NTO_DT_NOTA
                LEFT  JOIN GLOBAL.GLB_CENTRO_DISTRIBUICAO CED ON CED.CED_CO_NUMERO = NTV.CD
                INNER JOIN GLOBAL.GLB_LOCALIDADE LOC_CLI ON CLI.LOC_CO_NUMERO = LOC_CLI.LOC_CO_NUMERO
                LEFT JOIN GLOBAL.GLB_LOCALIDADE LOC_CED ON CED.LOC_CO_NUMERO = LOC_CED.LOC_CO_NUMERO
                INNER JOIN CAL_TIPO_OCORRENCIA TOC ON TOC.TOC_CO_NUMERO  = DEV.TOC_CO_NUMERO AND TOC.DEP_CO_NUMERO = {$departamento} AND TOC.SLA_CO_NUMERO = {$sala}
                INNER JOIN DEV_TIPO_DEVOLUCAO  TID ON TID.TID_CO_NUMERO = DEV.DOC_IN_TIPO
                WHERE DEV.DOC_CO_NUMERO = {$ObjOcorrencia->getCodigoDevolucao()}";



        if ($result = $this->conn->execSql($sql)) {
            return $result;
        }
        return false;
    }
    
    public function consultaNotaOrigem(Ocorrencia $ObjOcorrencia) {
        $ObjetoNota = $ObjOcorrencia->getDevNotaOrigem();
        $sql = "SELECT 
                  nto.nto_nu_serie ,
                  nto.nto_nu_nota ,
                  to_char(nto.nto_dt_nota,'DD/MM/YYYY') AS nto_dt_nota,
                  nto.nto_tx_responsavel,
                  nto.NTO_CO_RESPONSAVEL,
                  nto.nto_in_origem_responsavel,
                  nto.trp_co_numero, 
                  cli_co_numero 
            FROM dev_notaorigem nto
            WHERE  (1=1) 
            
        ";

        if ($ObjOcorrencia->getCodigoDevolucao()) {
            $sql .= " AND DOC_CO_NUMERO ={$ObjOcorrencia->getCodigoDevolucao()}";
        }
        if ($ObjetoNota) {
            foreach ($ObjetoNota as $Objeto) {
                $sql .="
                    AND NTO_NU_SERIE ='{$Objeto->getSerie()}' 
                    AND NTO_NU_NOTA ='{$Objeto->getNota()}'
                ";

                if ($Objeto->getResponsavel()) {
                    $sql .=" AND NTO_CO_RESPONSAVEL = {$Objeto->getResponsavel()}";
                }
                if ($Objeto->getOrigem()) {
                    $sql .=" AND NTO_IN_ORIGEM_RESPONSAVEL = '{$Objeto->getOrigem()}'";
                }
            }
        }
        $sql .= " ORDER BY NTO_DT_NOTA";
        //echo "<pre>consultaNotaOrigem: ".$sql."</pre>";   
        return $this->conn->execSql($sql);
    }
    
    function consultaDuplicidadeOcorrencia($cli_co_numero, $nto_nu_serie, $nto_nu_nota, $nto_dt_nota, $TipoDevolucao) {
        $sql = "
                    SELECT OCO.* 
                    FROM DEV_OCORRENCIA OCO
                    INNER JOIN DEV_NOTAORIGEM  NTO ON NTO.DOC_CO_NUMERO = OCO.DOC_CO_NUMERO
                    WHERE OCO.CLI_CO_NUMERO = {$cli_co_numero}
                    AND   NTO.NTO_NU_SERIE  = {$nto_nu_serie}
                    AND   NTO.NTO_NU_NOTA   = {$nto_nu_nota}
                    AND   NTO.NTO_DT_NOTA   = to_date('{$nto_dt_nota}','dd/mm/yyyy')
                    AND   OCO.DST_CO_NUMERO NOT IN (12)    
            ";
        if( $TipoDevolucao == "I"){
            
                $sql .="            
                               AND   OCO.DOC_IN_TIPO IN (3,4,5)

                              ";

        }else{       
                $sql .="            
                            AND   OCO.DOC_IN_TIPO IN (3,4)

                           ";
        }
       
        $result = $this->conn->execSql($sql);
        return $result;
    }
    
    function consultaDuplicidadeOcorrenciaParcial($cli_co_numero, $nto_nu_serie, $nto_nu_nota, $nto_dt_nota) {
        $sql = "
                    SELECT COUNT(*) QTDOCO
                    FROM DEV_OCORRENCIA OCO
                    INNER JOIN DEV_NOTAORIGEM  NTO ON NTO.DOC_CO_NUMERO = OCO.DOC_CO_NUMERO
                    WHERE OCO.CLI_CO_NUMERO = {$cli_co_numero}
                    AND   NTO.NTO_NU_SERIE  = {$nto_nu_serie}
                    AND   NTO.NTO_NU_NOTA   = {$nto_nu_nota}
                    AND   NTO.NTO_DT_NOTA   = to_date('{$nto_dt_nota}','dd/mm/yyyy')
                    AND   OCO.DOC_IN_TIPO IN (5)
                    AND   OCO.DST_CO_NUMERO NOT IN (12)
                   ";
                 
        $result = $this->conn->execSql($sql);
        $retorno = oci_fetch_object($result);
        
        if($retorno->QTDOCO > 0){
            return 1;
            
        }else{
            return 0;
        }
        
    }    
    
    public function consultarClienteVendedor(GlbVendedor $ObjVendedor) {

        $sql = " SELECT DISTINCT CLI.CLI_CO_NUMERO
                         FROM GLOBAL.GLB_CLIENTE_VENDEDOR CVEN 
                         INNER JOIN GLOBAL.GLB_VENDEDOR VEN ON VEN.VEN_CO_NUMERO = CVEN.VEN_CO_NUMERO
                         INNER JOIN GLOBAL.GLB_CLIENTE  CLI ON CLI.CLI_CO_NUMERO = CVEN.CLI_CO_NUMERO
                         WHERE 
                               CVEN.CLV_IN_STATUS IS NULL ";


        if ($ObjVendedor) {
            if (($ObjVendedor->getTipo() == "V") || ($ObjVendedor->getTipo() == "N")) {
                $sql .= " AND VEN.VEN_CO_SUPERVISOR = {$ObjVendedor->getCodigo()} ";
            }else{
                $sql .= " AND VEN.VEN_CO_NUMERO = {$ObjVendedor->getCodigo()} ";
            }
        }
        
        $sql .= " ORDER BY CLI.CLI_CO_NUMERO";

        if ($result = $this->conn->execSql($sql)) {
            return $result;
        }
        return false;
    }
    
    public function ordemColetaEntregue($doc_co_numero) {
        $sql = "update dev_ocorrencia set
                DOC_IN_COLETA_ENVIADA =1,
                DOC_DT_COLETA_ENVIADA = SYSDATE
                where doc_co_numero = $doc_co_numero";
        $this->conn->execSql($sql);
    }
    
    public function consultaDadosEspelhoNota($cli_co_numero, $serie, $numNota, $dataNota, $doc_co_numero) {

        $sql = "SELECT PRO.PRO_CO_NUMERO
                      ,NDI.NTI_NU_DEVQTD
                      ,PRO.PRO_NO_DESCRICAO
                      ,NDI.NTI_VL_PRECOUNIT
                      ,NDI.NTI_VL_TOTBRUTO
                      ,NDI.NTI_VL_TOTDESC
                      ,(NDI.NTI_VL_TOTLIQ / NDI.NTI_NU_DEVQTD) VALOR_UNITLIQ
                      ,NDI.NTI_VL_TOTLIQ
                      ,NVI.QTDE
                      ,(( NVL(NVI.BCVLICMSST,0) /NVI.QTDE) * NDI.NTI_NU_DEVQTD) BASE_CALCULOST
                      ,(( NVL(NVI.VLICMSST,0) / NVI.QTDE ) * NDI.NTI_NU_DEVQTD) VALOR_PROPST
                      ,(( NVL(NVI.BCVLICMS,0) / NVI.QTDE ) * NDI.NTI_NU_DEVQTD) BASE_CALCULOICMS
                      ,(( NVL(NVI.VLICMS,0)   / NVI.QTDE ) * NDI.NTI_NU_DEVQTD) VALOR_PROPICMS
                      
                      ,NVI.BCVLICMSST
                      ,NVI.VLICMSST
                      ,NVI.BCVLICMS
                      ,NVI.VLICMS
                      
                FROM  DEV_NOTAORIGEM_ITEM NDI
                INNER JOIN GLOBAL.GLB_PRODUTO   PRO ON PRO.PRO_CO_NUMERO = NDI.PRO_CO_NUMERO
                INNER JOIN USERVIMED.NOTAVENDAI NVI ON NVI.SERIE = NDI.NTI_NU_SERIE AND NDI.NTI_NU_NOTA = NVI.NUMNOTA AND NDI.CLI_CO_NUMERO = NVI.CDCLIENTE AND NDI.PRO_CO_NUMERO = NVI.CDPRODUTO 
                
                
                WHERE NDI.NTI_NU_SERIE = {$serie}
                AND   NDI.NTI_NU_NOTA  = {$numNota}
                AND   NDI.CLI_CO_NUMERO = {$cli_co_numero}
                AND   NDI.NTI_DT_NOTA   = TO_DATE('{$dataNota}', 'dd/mm/yyyy')
                AND   NDI.DOC_CO_NUMERO = {$doc_co_numero}
                
                ORDER BY PRO.PRO_NO_DESCRICAO 
        ";
        return $this->conn->execSql($sql);
    }
    
    public function consultaTotalEspelhoNota($doc_co_numero) {

        $sql = "SELECT 

                       SUM(NDI.NTI_VL_TOTBRUTO) TOTAL_BRUTO 
                      ,SUM(NDI.NTI_VL_TOTDESC)  TOTAL_DESCONTO
                      ,SUM(NDI.NTI_VL_TOTLIQ)   TOTAL_LIQUIDO
                      ,SUM((( NVL(NVI.BCVLICMSST,0) /NVI.QTDE) * NDI.NTI_NU_DEVQTD)) TOTAL_BASE_CALCULOST 
                      ,SUM((( NVL(NVI.VLICMSST,0) / NVI.QTDE ) * NDI.NTI_NU_DEVQTD)) TOTAL_VALOR_PROPST
                      ,SUM((( NVL(NVI.BCVLICMS,0) / NVI.QTDE ) * NDI.NTI_NU_DEVQTD)) TOTAL_BASE_CALCULOICMS
                      ,SUM((( NVL(NVI.VLICMS,0)   / NVI.QTDE ) * NDI.NTI_NU_DEVQTD)) TOTAL_VALOR_PROPICMS 

                FROM  DEV_NOTAORIGEM_ITEM NDI
                INNER JOIN USERVIMED.NOTAVENDAI NVI ON NVI.SERIE = NDI.NTI_NU_SERIE AND NDI.NTI_NU_NOTA = NVI.NUMNOTA AND NDI.CLI_CO_NUMERO = NVI.CDCLIENTE AND NDI.PRO_CO_NUMERO = NVI.CDPRODUTO 
                WHERE NDI.DOC_CO_NUMERO = {$doc_co_numero}
        ";
 
        return $this->conn->execSql($sql);
    } 

    public function consultaOcorrenciaPendentes($dataInicial = null, $dataFinal = null, $codUsuario, $codPasso) {
        $sql = "SELECT 
					DOC.DOC_CO_NUMERO,
					CLI.CLI_CO_NUMERO,
					CLI.CLI_NO_RAZAO_SOCIAL,
					CLI.CLI_NO_FANTASIA,
					DOC.ROT_CO_NUMERO,
					NVL(DOC.DOC_VR_VALOR, 0) DOC_VR_VALOR,
					TOC.TOC_NO_DESCRICAO,
					DECODE(DOC.DEV_CO_DIVISAO, 'F', 'FARMA', 'H', 'HOSPITALAR', 'A', 'ALIMENTAR', 'O', 'DIST DIRETA', DOC.DEV_CO_DIVISAO) DEV_CO_DIVISAO,
					CED.CED_NO_CENTRO
				FROM DEV_OCORRENCIA DOC
				INNER JOIN DEV_NOTAORIGEM NTO ON NTO.DOC_CO_NUMERO = DOC.DOC_CO_NUMERO
				LEFT  JOIN GLOBAL.GLB_CENTRO_DISTRIBUICAO CED ON CED.CED_CO_NUMERO = DOC.CED_CO_NUMERO
				INNER JOIN CAL_TIPO_OCORRENCIA TOC ON TOC.TOC_CO_NUMERO = DOC.TOC_CO_NUMERO AND TOC.DEP_CO_NUMERO = 29 AND TOC.SLA_CO_NUMERO = 1
				INNER JOIN GLOBAL.GLB_CLIENTE CLI ON CLI.CLI_CO_NUMERO = DOC.CLI_CO_NUMERO
				inner join DEV_OCORRENCIA_PASSO OP ON OP.DOC_CO_NUMERO = DOC.DOC_CO_NUMERO AND OP.DPA_CO_NUMERO = 1
				WHERE OP.USU_CO_NUMERO = {$codUsuario} AND DOC.DPA_CO_NUMERO = {$codPasso}
                ";

        if (($dataInicial != "") && ($dataFinal != "")) {
            $sql .= " 
                    AND   DOC.DOC_DT_CADASTRO BETWEEN TO_DATE ('{$dataInicial} 00:00:00', 'DD/MM/YYYY HH24:MI:SS') AND  TO_DATE ('{$dataFinal} 23:59:59', 'DD/MM/YYYY HH24:MI:SS')
                    ";
        }

        $sql .= " ORDER BY DOC.DOC_DT_CADASTRO";

        if ($result = $this->conn->execSql($sql)) {
            return $result;
        }
        return false;
    }
      

}