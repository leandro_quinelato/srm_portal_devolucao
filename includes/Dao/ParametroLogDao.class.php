<?php

//include_once("../DevParametroLog.class.php");

/**
 * Description of ParametroLogDao
 * 12/08/2015
 * @author geovanni.info
 */
class ParametroLogDao {
    private $conn;
	
    function __construct(){
        $this->conn = new DaoSistema();
        $this->conn->conectar();
    }
    
    public function gravar(DevParametroLog $devParametroLog) {
        
        $sql = "     
                
                    INSERT
                    INTO DEV_PARAMETROS_LOG
                    (
                        PLO_CO_USUARIO,
                        PLO_IN_USUARIO,
                        PLO_DT_ACAO
                    )
                    VALUES
                    (
                        {$devParametroLog->getUsuario()},
                        '{$devParametroLog->getTipoUsuario()}',
                        SYSDATE
                    )
               ";
                     
        
        
//        echo $sql;
        if ($this->conn->execSql($sql)) {
            return true;
            
        }
        return false;
    }
    
    public function consultar(){
        $sql = "
                SELECT * FROM (
                                SELECT PLO.PLO_CO_USUARIO,
                                       USU.OPR_NO_EXIBICAO,
                                       PLO.PLO_DT_ACAO,
                                       TO_CHAR(PLO.PLO_DT_ACAO , 'dd/mm/yyyy HH24:MI:SS' ) DATA_EXIBIR
                                FROM DEV_PARAMETROS_LOG PLO
                                INNER JOIN SYS_OPERADOR USU ON USU.OPR_CO_NUMERO = PLO.PLO_CO_USUARIO
                                ORDER BY PLO_DT_ACAO DESC
                ) WHERE ROWNUM = 1		
		";
        if($result = $this->conn->execSql( $sql ))
        {
            return $result; 
        }
            return false;  
	}
    
}
