<?php

/**
 * Classe Controller para tipo ocorrencia
 */
//echo $_SERVER["DOCUMENT_ROOT"] ;
//include_once( "../CalTipoOcorrencia.class.php" );

Class TipoOcorrenciaDAO {

    private $conn;

    function __construct() {
        $this->conn = new DaoSistema();
        $this->conn->conectar();
    }

//    public function listarTipoOcorrencia( CalDepartamento $Departamento )
//    {
//        $Sala = $Departamento->getSala();
//
//        try
//        {
//            $sql = "
//                SELECT *
//		FROM cal_tipo_ocorrencia
//		WHERE dep_co_numero = ". $Departamento->getCodDepartamento() ."
//                    AND sla_co_numero = ". $Sala->getCodSala() ."
//                    AND TOC_IN_STATUS = 1 
//		ORDER BY toc_no_descricao
//            ";
//            /*
//            $sql = "
//                SELECT *
//		FROM cal_tipo_ocorrencia
//		WHERE dep_co_numero = 29". "
//                    AND sla_co_numero = 1
//                    AND TOC_IN_STATUS = 1 
//		ORDER BY toc_no_descricao
//            ";
//             * 
//             */
//            $vConsulta = $this->conn->execSql( $sql );
////            echo $sql;
//            return $vConsulta;
//        }
//        catch( Exception $e )
//        {
//            print( "[ ERRO NO METODO listarTipoOcorrencia ]: ". $e->getMessage() );
//            return null;
//        }
//    }

    public function listarTipoOcorrencia(CalTipoOcorrencia $TipoOcorrencia) {

        try {
            $sql = "
                        SELECT *
                        FROM cal_tipo_ocorrencia
                        WHERE dep_co_numero = 29
                        AND   sla_co_numero = 1
            ";
            

            if  ( $TipoOcorrencia->getCodTipoOcorrencia() ){
                $sql .= "    AND   toc_co_numero = " . $TipoOcorrencia->getCodTipoOcorrencia();
            }
            
            $sql .= "
                    ORDER BY TOC_NO_DESCRICAO
            ";
            
            
//            echo "<pre>";
//            print_r($sql);
//            echo "</pre>";
//            die();

            $result = $this->conn->execSql($sql);
            while ($rsTipoOcorrencia = oci_fetch_object($result)) {
                $TipoOcorrencia = new CalTipoOcorrencia();
                $TipoOcorrencia->setCodTipoOcorrencia($rsTipoOcorrencia->TOC_CO_NUMERO);
                $TipoOcorrencia->setNomTipoOcorrencia($rsTipoOcorrencia->TOC_NO_DESCRICAO);
                $TipoOcorrencia->setNumTempoAtendimento($rsTipoOcorrencia->TOC_MI_ATENDIMENTO);
                $TipoOcorrencia->setNumTempoFechamento($rsTipoOcorrencia->TOC_MI_FECHAMENTO);
                $TipoOcorrencia->setIndAtivoTipoOcorrencia($rsTipoOcorrencia->TOC_IN_STATUS);
                $TipoOcorrencia->setRegra($rsTipoOcorrencia->TOC_IN_REGRA);

                $ObjTipoOcorrencias[] = $TipoOcorrencia;
            }
            return $ObjTipoOcorrencias;
        } catch (Exception $e) {
            print( "[ ERRO NO METODO consultarTipoOcorrencia ]: " . $e->getMessage());
            return null;
        }
    }
	
    public function listarTipoOcorrenciaPerfil(CalTipoOcorrencia $TipoOcorrencia, $perfilUsuario) {

        try {
            $sql = "
                        SELECT cto.* 
                        FROM cal_tipo_ocorrencia cto 
						INNER JOIN dev_tipo_ocorrencia_perfil dtop on dtop.toc_co_numero = cto.toc_co_numero 
						WHERE cto.dep_co_numero = 29 
                        AND   cto.sla_co_numero = 1 
						AND cto.toc_in_status = 1
                        AND cto.TOC_CO_NUMERO != 80
						AND dtop.per_co_numero = ".$perfilUsuario;
            

            if  ( $TipoOcorrencia->getCodTipoOcorrencia() ){
                $sql .= "    AND   cto.toc_co_numero = " . $TipoOcorrencia->getCodTipoOcorrencia();
            }
            
            $sql .= " ORDER BY cto.TOC_NO_DESCRICAO";
            
            
//            echo "<pre>";
//            print_r($sql);
//            echo "</pre>";
//            die();

            $result = $this->conn->execSql($sql);
            while ($rsTipoOcorrencia = oci_fetch_object($result)) {
                $TipoOcorrencia = new CalTipoOcorrencia();
                $TipoOcorrencia->setCodTipoOcorrencia($rsTipoOcorrencia->TOC_CO_NUMERO);
                $TipoOcorrencia->setNomTipoOcorrencia($rsTipoOcorrencia->TOC_NO_DESCRICAO);
                $TipoOcorrencia->setNumTempoAtendimento($rsTipoOcorrencia->TOC_MI_ATENDIMENTO);
                $TipoOcorrencia->setNumTempoFechamento($rsTipoOcorrencia->TOC_MI_FECHAMENTO);
                $TipoOcorrencia->setIndAtivoTipoOcorrencia($rsTipoOcorrencia->TOC_IN_STATUS);
                $TipoOcorrencia->setRegra($rsTipoOcorrencia->TOC_IN_REGRA);

                $ObjTipoOcorrencias[] = $TipoOcorrencia;
            }
            return $ObjTipoOcorrencias;
        } catch (Exception $e) {
            print( "[ ERRO NO METODO consultarTipoOcorrencia ]: " . $e->getMessage());
            return null;
        }
    }

    public function consultarTipoOcorrencia(CalTipoOcorrencia $TipoOcorrencia) {
        try {
            $sql = "
                SELECT *
                    FROM cal_tipo_ocorrencia
                    WHERE dep_co_numero = 29
                    AND sla_co_numero = 1
                    AND toc_co_numero = " . $TipoOcorrencia->getCodTipoOcorrencia();
//$sql;
            $vConsulta = $this->conn->execSql($sql);
            $limTipoOcorrencia = OCIFetchStatement($vConsulta, $rsTipoOcorrencia);

            $TipoOcorrencia->setNomTipoOcorrencia($rsTipoOcorrencia['TOC_NO_DESCRICAO'][0]);
            $TipoOcorrencia->setNumTempoAtendimento($rsTipoOcorrencia['TOC_MI_ATENDIMENTO'][0]);
            $TipoOcorrencia->setNumTempoFechamento($rsTipoOcorrencia['TOC_MI_FECHAMENTO'][0]);
            $TipoOcorrencia->setIndAtivoTipoOcorrencia($rsTipoOcorrencia['TOC_IN_STATUS'][0]);
            $TipoOcorrencia->setRegra($rsTipoOcorrencia['TOC_IN_REGRA'][0]);

            return $TipoOcorrencia;
            
        } catch (Exception $e) {
            print( "[ ERRO NO METODO consultarTipoOcorrencia ]: " . $e->getMessage());
            return null;
        }
    }

    public function incluirTipoOcorrencia(CalTipoOcorrencia $TipoOcorrencia) {
        $Departamento = 29;
        $Sala = 1;

        try {
            $sql = "
                INSERT INTO cal_tipo_ocorrencia (
                    dep_co_numero
                    , sla_co_numero
                    , toc_co_numero
                    , toc_no_descricao
                    , toc_in_status
                    , toc_in_regra
		) VALUES (
                    " . $Departamento . "
                    , " . $Sala . "
                    , ( SELECT NVL( MAX( toc_co_numero), 0 ) + 1
                        FROM cal_tipo_ocorrencia
                        WHERE dep_co_numero = " . $Departamento . "
                            AND sla_co_numero = " . $Sala . "
                    )
                    , UPPER( '" . $TipoOcorrencia->getNomTipoOcorrencia() . "' )
                    , {$TipoOcorrencia->getIndAtivoTipoOcorrencia()}
                    , '{$TipoOcorrencia->getRegra()}'
		)
            ";
                    
            echo '<pre>';
            print_r($sql);
            echo '</pre>';  
                    
            $vConsulta = $this->conn->execSql($sql);
        } catch (Exception $e) {
            print( "[ ERRO NO METODO incluir ]: " . $e->getMessage());
        }
    }

    public function alterarTipoOcorrencia(CalTipoOcorrencia $TipoOcorrencia) {
        $Departamento = 29;
        $Sala = 1;

        try {
            $sql = "
                UPDATE cal_tipo_ocorrencia SET
                      toc_no_descricao = UPPER( '" . $TipoOcorrencia->getNomTipoOcorrencia() . "' )
                    , toc_in_status = " . $TipoOcorrencia->getIndAtivoTipoOcorrencia() . "
                    , toc_in_regra  = '{$TipoOcorrencia->getRegra()}'  
		WHERE dep_co_numero = " . $Departamento . "
                    AND sla_co_numero = " . $Sala. "
                    AND toc_co_numero = " . $TipoOcorrencia->getCodTipoOcorrencia();
                                       
//            echo '<pre>';
//            print_r($sql);
//            echo '</pre>';
                        
            $vConsulta = $this->conn->execSql($sql);
        } catch (Exception $e) {
            print( "[ ERRO NO METODO alterarTipoOcorrencia ]: " . $e->getMessage());
        }
    }

    public function inativarTipoOcorrencia(CalTipoOcorrencia $TipoOcorrencia) {
//        $Departamento = $TipoOcorrencia->getDepartamentoTipoOcorrencia();
//        $Sala = $Departamento->getSala();

        try {
            $sql = "
                UPDATE cal_tipo_ocorrencia SET toc_in_status = 0
                WHERE dep_co_numero = 29
                    AND sla_co_numero = 1
                    AND toc_co_numero = " . $TipoOcorrencia->getCodTipoOcorrencia();
            $vConsulta = $this->conn->execSql($sql);
        } catch (Exception $e) {
            print( "[ ERRO NO METODO inativarTipoOcorrencia ]: " . $e->getMessage());
        }
    }

    public function verificarDescricaoTipoOcorrencia(CalTipoOcorrencia $TipoOcorrencia) {
//        $Departamento = $TipoOcorrencia->getDepartamentoTipoOcorrencia();
//        $Sala = $Departamento->getSala();

        try {
            $sql = "
                SELECT *
		FROM cal_tipo_ocorrencia 
		WHERE dep_co_numero = 29
                    AND sla_co_numero = 1
                    AND toc_no_descricao = UPPER( '" . $TipoOcorrencia->getNomTipoOcorrencia() . "' )
            ";
            if ($TipoOcorrencia->getCodTipoOcorrencia())
                $sql .= " AND toc_co_numero != " . $TipoOcorrencia->getCodTipoOcorrencia();

            $vConsulta = $this->conn->execSql($sql);
            $limTipoOcorrencia = OCIFetchStatement($vConsulta, $rsTipoOcorrencia);

            if ($limTipoOcorrencia > 0)
                return true;
            else
                return false;
        } catch (Exception $e) {
            print( "[ ERRO NO METODO verificarDescricaoTipoOcorrencia ]: " . $e->getMessage());
            return null;
        }
    }

//    public function pesquisarDescricaoTipoOcorrencia(CalTipoOcorrencia $TipoOcorrencia) {
//        $Departamento = $TipoOcorrencia->getDepartamentoTipoOcorrencia();
//        $Sala = $Departamento->getSala();
//
//        try {
//            $sql = "
//                SELECT *
//		FROM cal_tipo_ocorrencia
//		WHERE dep_co_numero = " . $Departamento->getCodDepartamento() . "
//                    AND sla_co_numero = " . $Sala->getCodSala() . "
//                    AND toc_no_descricao LIKE UPPER( '%" . $TipoOcorrencia->getNomTipoOcorrencia() . "%' )
//		
//            ";
//            //echo $TipoOcorrencia->getIndAtivoTipoOcorrencia() . "<BR>";
//            if ($TipoOcorrencia->getIndAtivoTipoOcorrencia() == "1") {
//                $sql .= " AND toc_in_status = 1";
//            }
//            $sql .= " ORDER BY toc_no_descricao";
//            //echo $sql;
//            $vConsulta = $this->conn->execSql($sql);
//
//            return $vConsulta;
//            /* $limTipoOcorrencia = OCIFetchStatement( $vConsulta, $rsTipoOcorrencia );
//
//              for ( $x = 0; $x < $limTipoOcorrencia; $x++ )
//              {
//              $TipoOcorrencia = new CalTipoOcorrencia();
//              $TipoOcorrencia->setCodTipoOcorrencia( $rsTipoOcorrencia['TOC_CO_NUMERO'][$x] );
//              $TipoOcorrencia->setNomTipoOcorrencia( $rsTipoOcorrencia['TOC_NO_DESCRICAO'][$x] );
//              $TipoOcorrencia->setNumTempoAtendimento( $rsTipoOcorrencia['TOC_MI_ATENDIMENTO'][$x] );
//              $TipoOcorrencia->setNumTempoFechamento( $rsTipoOcorrencia['TOC_MI_FECHAMENTO'][$x] );
//              $TipoOcorrencia->setIndAtivoTipoOcorrencia( $rsTipoOcorrencia['TOC_IN_STATUS'][$x] );
//              $TipoOcorrencia->setDepartamentoTipoOcorrencia( $Departamento );
//
//              $tipoOcorrencia[] = $TipoOcorrencia;
//              }
//              return $tipoOcorrencia;
//             */
//        } catch (Exception $e) {
//            print( "[ ERRO NO METODO pesquisarDescricaoTipoOcorrencia ]: " . $e->getMessage());
//            return null;
//        }
//    }

}

?>