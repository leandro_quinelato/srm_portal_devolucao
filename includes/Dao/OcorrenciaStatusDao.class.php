<?php

//include_once("../OcorrenciaStatus.class.php");

/**
 * Description of OcorrenciaStatusDao
 *
 * @author geovanni.info
 */
class OcorrenciaStatusDao {

    private $conn;

    function __construct() {
        $this->conn = new DaoSistema();
        $this->conn->conectar();
    }

    public function consultar(OcorrenciaStatus $OcorrenciaStatus) {

        $sql = "
                    SELECT DOS.DOC_CO_NUMERO,
                           DOS.DST_CO_NUMERO,
                           DOS.USU_CO_NUMERO,
                           USU.USU_NO_USERNAME,
                           TO_CHAR(DOS.DOS_DT_ENTRADA, 'DD/MM/YYYY HH24:MI:SS') DATA_ENTRADA,
                           DOS.DOS_DT_ENTRADA,
                           DST.DST_NU_SEQFLUXO,
                           DST.DST_NO_DESCRICAO,
                           TO_CHAR(DOS.DOS_DT_SAIDA, 'DD/MM/YYYY HH24:MI:SS') DOS_DT_SAIDA
                    FROM DEV_OCORRENCIA_STATUS DOS
                    INNER JOIN DEV_STATUS      DST    ON DOS.DST_CO_NUMERO = DST.DST_CO_NUMERO
                    LEFT  JOIN GLOBAL.GLB_USUARIO USU ON USU.USU_CO_NUMERO = DOS.USU_CO_NUMERO
                    WHERE DOS.DOC_CO_NUMERO = {$OcorrenciaStatus->getCodigoOcorrencia()}
                    ORDER BY DOS.DOS_DT_ENTRADA,  DST.DST_NU_SEQFLUXO  
		";

        $result = $this->conn->execSql($sql);
        while ($Rstatus = oci_fetch_object($result)) {
            $OcorrenciaStatus = new OcorrenciaStatus();
            $OcorrenciaStatus->setCodigoOcorrencia($Rstatus->DOC_CO_NUMERO);
            $OcorrenciaStatus->setCodStatus($Rstatus->DST_CO_NUMERO);
            $OcorrenciaStatus->setUsuario($Rstatus->USU_CO_NUMERO);
            $OcorrenciaStatus->setUsuarioNome($Rstatus->USU_NO_USERNAME);
            $OcorrenciaStatus->setDataEntrada($Rstatus->DATA_ENTRADA);
            $OcorrenciaStatus->setDataSaida($Rstatus->DOS_DT_SAIDA);
            $OcorrenciaStatus->setDescStatus($Rstatus->DST_NO_DESCRICAO);

            $ObjOcorrenciaStatus[] = $OcorrenciaStatus;
        }
        return $ObjOcorrenciaStatus;
    }

    public function consultarStatus($codOcorrencia) {

        $sql = "
                    SELECT DOS.DOC_CO_NUMERO,
                           DOS.DST_CO_NUMERO,
                           DOS.USU_CO_NUMERO,
                           USU.USU_NO_USERNAME,
                           TO_CHAR(DOS.DOS_DT_ENTRADA, 'DD/MM/YYYY HH24:MI:SS') DATA_ENTRADA,
                           DOS.DOS_DT_ENTRADA,
                           DST.DST_NU_SEQFLUXO,
                           DST.DST_NO_DESCRICAO,
                           TO_CHAR(DOS.DOS_DT_SAIDA, 'DD/MM/YYYY HH24:MI:SS') DOS_DT_SAIDA
                    FROM DEV_OCORRENCIA_STATUS DOS
                    INNER JOIN DEV_STATUS      DST    ON DOS.DST_CO_NUMERO = DST.DST_CO_NUMERO
                    LEFT  JOIN GLOBAL.GLB_USUARIO USU ON USU.USU_CO_NUMERO = DOS.USU_CO_NUMERO
                    WHERE DOS.DOC_CO_NUMERO = $codOcorrencia
                    ORDER BY DOS.DOS_DT_ENTRADA,  DST.DST_NU_SEQFLUXO  
		";

        $result = $this->conn->execSql($sql);
        while ($Rstatus = oci_fetch_object($result)) {
            if($Rstatus->DST_CO_NUMERO == 5){   
                return true;             
                break;                
            }            
        }
        return false;
    }

    public function registrarSatus(OcorrenciaStatus $OcorrenciaStatus) {

        $sql = "
                    INSERT
                    INTO DEV_OCORRENCIA_STATUS
                    (
                        DOC_CO_NUMERO,
                        DST_CO_NUMERO,
                        USU_CO_NUMERO,
                        DOS_DT_ENTRADA
                    )
                    VALUES
                    (
                        {$OcorrenciaStatus->getCodigoOcorrencia()},
                        {$OcorrenciaStatus->getCodStatus()},
                        {$OcorrenciaStatus->getUsuario()},
                        SYSDATE
                    )	
		";

        if ($this->conn->execSql($sql)) {
            return true;
        }
        return false;
    }

}
