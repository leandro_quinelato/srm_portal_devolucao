<?php


/**
 * Description of DevLogAcaoPasso
 * Chamado: 50020
 * Data: 15/07/2015
 * Obs.: Criado para registrar o log das ações realizadas nas ocorrencias durante os passos ( Reprovar , Reativar)
 * @author geovanni.info
 */
class LogAcaoPasso {
    
    private $doc_co_numero;
    private $lap_no_solicitante;
    private $lap_no_departamento;
    private $lap_co_usuario;
    private $lap_in_usuario;
    private $lap_in_acao;
    private $lap_dt_acao;
    private $lap_in_passo;
    private $lap_tx_descricao;
    private $mre_co_numero;
    private $dpa_co_numero;

    
    public function getCodOcorrencia() {
        return $this->doc_co_numero;
    }

    public function setCodOcorrencia($doc_co_numero) {
        $this->doc_co_numero = $doc_co_numero;
    }
    
    
    public function getNomeSolicitante() {
        return $this->lap_no_solicitante;
    }

    public function setNomeSolicitante($lap_no_solicitante) {
        $this->lap_no_solicitante = $lap_no_solicitante;
    }
    
    public function getNomeDepartamento() {
        return $this->lap_no_departamento;
    }

    public function setNomeDepartamento($lap_no_departamento) {
        $this->lap_no_departamento = $lap_no_departamento;
    }
    
    public function getUsuario() {
        return $this->lap_co_usuario;
    }

    public function setUsuario($lap_co_usuario) {
        $this->lap_co_usuario = $lap_co_usuario;
    }
    
    public function getTipoUsuario() {
        return $this->lap_in_usuario;
    }

    public function setTipoUsuario($lap_in_usuario) {
        $this->lap_in_usuario = $lap_in_usuario;
    }
    
    public function getAcao() {
        return $this->lap_in_acao;
    }

    public function setAcao($lap_in_acao) {
        $this->lap_in_acao = $lap_in_acao;
    }
    
    public function getDataAcao() {
        return $this->lap_dt_acao;
    }

    public function setDataAcao($lap_dt_acao) {
        $this->lap_dt_acao = $lap_dt_acao;
    }
    
    public function getPasso() {
        return $this->lap_in_passo;
    }

    public function setPasso($lap_in_passo) {
        $this->lap_in_passo = $lap_in_passo;
    }
    
    
    public function getAcaoDescricao() {
        return $this->lap_tx_descricao;
    }

    public function setAcaoDescricao($lap_tx_descricao) {
        $this->lap_tx_descricao = $lap_tx_descricao;
    }
    
    public function getCodReprova() {
        return $this->mre_co_numero;
    }

    public function setCodReprova($mre_co_numero) {
        $this->mre_co_numero = $mre_co_numero;
    }
    
    public function getPassoPortal() {
        return $this->dpa_co_numero;
    }

    public function setPassoPortal($dpa_co_numero) {
        $this->dpa_co_numero = $dpa_co_numero;
    }
    
    
}
