<?php

/**
 * Description of OcorrenciaPasso
 *
 * @author geovanni.info
 * 
 * TABELA: DEV_OCORRENCIA_PASSO
 * DOC_CO_NUMERO	NUMBER(30,0)
 * DPA_CO_NUMERO	NUMBER(3,0)
 * USU_CO_NUMERO	NUMBER(8,0)
 * DOP_DT_ENTRADA	DATE
 * DOP_DT_SAIDA	        DATE
 * DOP_IN_APROVADO	NUMBER
 * DOP_TX_OBSERVACAO	CLOB
 * DOP_QT_VOLUME	NUMBER(4,0)
 * 
 */
class OcorrenciaPasso {

	private $codigoOcorrencia;
	private $codigoPasso; 
        private $usuario;
        private $usuarioNome;
	private $dataEntrada;
	private $dataSaida;
	private $indicadorAprovado;
	private $observacao;
	private $qtdVolume;
        
        
	public function getCodigoOcorrencia()
	{
		return $this->codigoOcorrencia; 
	}
	public function setCodigoOcorrencia($codigoOcorrencia)
	{
		$this->codigoOcorrencia = $codigoOcorrencia; 
	}
        
        
	public function getCodigoPasso()
	{
		return $this->codigoPasso; 
	}
	public function setCodigoPasso($codigoPasso)
	{
		$this->codigoPasso = $codigoPasso; 
	}
        
        
	public function getUsuario()
	{
		return $this->usuario; 
	}
	public function setUsuario($usuario)
	{
		$this->usuario = $usuario; 
	}
        
	public function getUsuarioNome()
	{
		return $this->usuarioNome; 
	}
	public function setUsuarioNome($usuarioNome)
	{
		$this->usuarioNome = $usuarioNome; 
	}
        
	public function getDataEntrada()
	{
		return $this->dataEntrada; 
	}
	public function setDataEntrada($dataEntrada)
	{
		$this->dataEntrada = $dataEntrada; 
	}
        
	public function getDataSaida()
	{
		return $this->dataSaida; 
	}
	public function setDataSaida($dataSaida)
	{
		$this->dataSaida = $dataSaida; 
	}
        
	public function getIndicadorAprovado()
	{
		return $this->indicadorAprovado; 
	}
	public function setIndicadorAprovado($indicadorAprovado)
	{
		$this->indicadorAprovado = $indicadorAprovado; 
	}
        
	public function getObservacao()
	{
		return $this->observacao; 
	}
	public function setObservacao($observacao)
	{
		$this->observacao = $observacao; 
	}
        
	public function getQtdVolume()
	{
		return $this->qtdVolume; 
	}
	public function setQtdVolume($qtdVolume)
	{
		$this->qtdVolume = $qtdVolume; 
	}
        
        
    
}
