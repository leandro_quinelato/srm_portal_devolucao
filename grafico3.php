<?php
session_start();

# PHPlot Example: Two plots on one image
require_once 'includes/phplot/phplot.php';

$title = '.';

# Note: Graph is based on the real thing, but the data is invented.
# Data for plot #1: stackedbars:
$y_title1 = '';
/*
$data1 = array(
    array("Average\n U.S. Cities",  120200.10),
    array('2/09',  20300),
    array('3/09',  20400),
    array('4/09',  20500),
    array('5/09',  20400),
    array('6/09',  20500),
    array('7/09',  20400),
    array('8/09',  20300),
    array('9/09',  20200),
    array('10/09', 20100)
);
*/
$data1 = $_SESSION["data_grafico3_1"];

$legend1 = "Valor";//array('Valor');

# Data for plot #2: linepoints:
$y_title2 = '';
/*
$data2 = array(
    array("Average\n U.S. Cities",   500),
    array('2/09',  100),
    array('3/09',  150),
    array('4/09',  300),
    array('5/09',  400),
    array('6/09',  450),
    array('7/09',  470),
    array('8/09',  350),
    array('9/09',  250),
    array('10/09', 200)
);
*/
$data2 = $_SESSION["data_grafico3_2"];
$legend2 = array('Qtd.');

$plot = new PHPlot(1000, 500);
$plot->SetImageBorderType('plain'); // For presentation in the manual
$plot->SetPrintImage(False); // Defer output until the end
$plot->SetTitle($title);
$plot->SetPlotBgColor('gray');
//$plot->SetLightGridColor('black'); // So grid stands out from background

# Plot 1
$plot->SetDrawPlotAreaBackground(false);
$plot->SetPlotType('bars');
$plot->SetDataType('text-data');
$plot->SetDataValues($data1);
$plot->SetYTitle($y_title1);
# Set and position legend #1:
$plot->SetLegend($legend1);
$plot->SetLegendPixels(450, 1);
# Set margins to leave room for plot 2 Y title on the right.
$plot->SetMarginsPixels(80, 50);
# Specify Y range of these data sets:
//$plot->SetPlotAreaWorld(NULL, 0, NULL, 25000);
//$plot->SetYTickIncrement(10000);
$plot->SetXTickLabelPos('none');
$plot->SetXTickPos('none');
# Format Y tick labels as integers, with thousands separator:
$plot->SetYLabelType('data', 2, '$', '');
//$plot->SetXLabelAngle(90);
$plot->SetYDataLabelPos('plotin'); //label sobre a coluna/linha
$plot->DrawGraph();

# Plot 2
$plot->SetDrawPlotAreaBackground(False); // Cancel background
$plot->SetDrawYGrid(False); // Cancel grid, already drawn
$plot->SetPlotType('linepoints');
$plot->SetDataValues($data2);
# Set Y title for plot #2 and position it on the right side:
$plot->SetYTitle($y_title2, 'plotright');
# Set and position legend #2:
$plot->SetLegend($legend2);
$plot->SetLegendPixels(510, 1);
# Specify Y range of this data set:
$plot->SetPlotAreaWorld(NULL, 0, NULL, 1000);
$plot->SetYTickIncrement(50);
$plot->SetYTickPos('plotright');
$plot->SetYTickLabelPos('plotright');
$plot->SetDataColors('black');
# Format Y tick labels as integers with trailing percent sign:
$plot->SetYLabelType('data', 0);
$plot->SetYDataLabelPos('plotin');//label sobre a coluna/linha
$plot->DrawGraph();

# Now output the graph with both plots:
$plot->PrintImage();
?>