<?php
session_start();

# PHPlot Example - Horizontal Bars
require_once 'includes/phplot/phplot.php';

/*
$data = array(
  array('Phoenix AZ', 8.3),
  array('New York NY', 49.7),
  array('New Orleans LA', 64.2),
  array('Miami FL', 52.3),
  array('AGUARD. CONFERENCIA FISICA X NF', 13.2),
  array('Honolulu HI', 18.3),
  array('Helena MT', 11.3),
  array('Duluth MN', 31.0),
  array('Dodge City KS', 22.4),
);
*/

$data = $_SESSION["data_grafico2"];

$plot = new PHPlot(500, 250);
$plot->SetImageBorderType('plain'); // Improves presentation in the manual
$plot->SetTitle("");
//$plot->SetBackgroundColor('gray');
#  Set a tiled background image:
//$plot->SetPlotAreaBgImage('images/drop.png', 'centeredtile');
#  Force the X axis range to start at 0:
$plot->SetPlotAreaWorld(0);
#  No ticks along Y axis, just bar labels:
$plot->SetYTickPos('none');
#  No ticks along X axis:
$plot->SetXTickPos('none');
#  No X axis labels. The data values labels are sufficient.
$plot->SetXTickLabelPos('none');
#  Turn on the data value labels:
$plot->SetXDataLabelPos('plotin');
#  No grid lines are needed:
$plot->SetDrawXGrid(FALSE);
#  Set the bar fill color:
$plot->SetDataColors('blue');
#  Use less 3D shading on the bars:
$plot->SetXLabelType('data', 2, '$', '');
$plot->SetShading(2);
$plot->SetDataValues($data);
$plot->SetDataType('text-data-yx');
$plot->SetPlotType('bars');
$plot->DrawGraph();
?>