<?php

//extract($_REQUEST);

//include_once("../includes/Dao.class.php");
session_start();
if ($_SESSION['erro']) {
    $erro = $_SESSION['erro'];
    session_destroy();
} else {
    $erro = 0;
}

//Direciona para Home caso j� esteja logado
if ($_SESSION['ObjLogin']) {
    header("Location:/Home");
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>        
        <!-- meta section -->
        <title>Devolução</title>
        
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" >
        <meta http-equiv="X-UA-Compatible" content="IE=edge" >
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" >
        
        <link rel="icon" href="favicon.ico" type="image/x-icon" >
        <!-- /meta section -->        
        
        <!-- css styles -->
        <link rel="stylesheet" type="text/css" href="css/blue-white.css" id="dev-css">
        <!-- ./css styles -->                                    
                
        <!--[if lte IE 9]>
        <link rel="stylesheet" type="text/css" href="css/dev-other/dev-ie-fix.css">
        <![endif]-->
        
        <!-- javascripts -->
        <script type="text/javascript" src="js/plugins/modernizr/modernizr.js"></script>
        <script type="text/javascript" src="js/plugins/jquery/jquery.min.js"></script>       
        <script type="text/javascript" src="js/libPortal.js"></script>
        <!-- ./javascripts -->
        
        <style>.dev-page{visibility: hidden;}</style>
        <script>
            $(function(){
                carregarLogin(<?php echo $erro; ?>);
            });
        </script>   
    </head>
    <body>
                
        <!-- page wrapper -->
        <div class="dev-page dev-page-login dev-page-login-v2">
                      
            <div class="dev-page-login-block" id="area-login">
                <!-- area login -->
            </div>
              
        </div>
        <!-- ./page wrapper -->                
        
        <!-- javascript -->
        <script type="text/javascript" src="js/plugins/jquery/jquery.min.js"></script>       
        <script type="text/javascript" src="js/plugins/bootstrap/bootstrap.min.js"></script>        
        <!-- ./javascript -->
    </body>
</html>






