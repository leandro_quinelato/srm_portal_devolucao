<?php 
session_start();
ob_start();
include_once("include_novo/Tradutor.class.php");
include_once("GlbUsuario.class.php");
include_once("GlbContato.class.php");
include_once("include_novo/GlbTransportadora.class.php");
include_once("include_novo/GlbContatoTransportador.class.php");


if(!isset($_SESSION['ObjLogin'])){
    header("location:login.php");
}

$ObjUsuario = new GlbUsuario();
$ObjUsuario = unserialize($_SESSION['ObjLogin']);
$Tradutor = new Tradutor();

?>
<div class="dev-page-header">
                
    <div class="dph-logo">
        <a href="/Home"></a>
        <a class="dev-page-sidebar-collapse">
            <div class="dev-page-sidebar-collapse-icon">
                <span class="line-one"></span>
                <span class="line-two"></span>
                <span class="line-three">asdasdas</span>
            </div>
        </a>
    </div>

    <ul class="dph-buttons pull-right">                    
        <li>
            <a style="cursor:default;">
                <?php
                    if( $ObjUsuario->getUsuarioTipo() == 'INT' ){
                        echo $Tradutor->ucwordspt($ObjUsuario->getObjColaborador()->getPessoaNome());
                    }
                    if( $ObjUsuario->getUsuarioTipo() == 'CLI' ){
                        $contato = $ObjUsuario->getObjCliente()->getObjContato();
                        //print_r($contato);
                        if($contato[0] != null && $contato[0]->getPessoaNome() != null){
                            echo $Tradutor->ucwordspt($contato[0]->getPessoaNome()) ; 
                        }
                        
                    }
                    if( $ObjUsuario->getUsuarioTipo() == 'VEN' ){
                        $vendedor = $ObjUsuario->getObjVendedor();
                        if($vendedor[0] != null && $vendedor[0]->getPessoaNome() != null){
                            echo $Tradutor->ucwordspt($vendedor[0]->getPessoaNome()) ; 
                        }                        
                    }
                    if( $ObjUsuario->getUsuarioTipo() == 'TRA' ){
                        $contato = $ObjUsuario->getObjTransportadora()->getObjContato();
                        if($contato[0] != null && $contato[0]->getPessoaNome() != null)
                            echo $Tradutor->ucwordspt($contato[0]->getPessoaNome() . ' - ' . $ObjUsuario->getObjTransportadora()->getRazaoSocial()  ) ; 
                    }
                        
                ?>
            </a>
        </li> 
    </ul>   
    <ul class="dph-buttons pull-right" style="float:right;">                    
        <li class="dph-button-stuck">
            <a onclick="javascript: logout();" title="Sair" class="botaoSair" data-original-title="Logoff"><i class="fa fa-power-off"></i></a>
        </li>                    
    </ul>
</div>