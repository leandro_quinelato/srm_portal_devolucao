<!-- meta section -->

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" >
<meta http-equiv="X-UA-Compatible" content="IE=edge" >
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" >

<link rel="icon" href="http://<?php echo $_SERVER['HTTP_HOST']; ?>/favicon.ico" type="image/x-icon" >
<!-- ./meta section -->

<!-- css styles -->
<link rel="stylesheet" type="text/css" href="http://<?php echo $_SERVER['HTTP_HOST']; ?>/css/lightblue.css" id="dev-css">


<!-- ./css styles -->                                     

<!--[if lte IE 9]>
<link rel="stylesheet" type="text/css" href="css/dev-other/dev-ie-fix.css">
<![endif]-->

<!-- javascripts -->
<script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/plugins/modernizr/modernizr.js"></script>
<script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/libPortal.js"></script>
<script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/libCadastro.js"></script>
<script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/js/plugins/jquery/ajaxupload.js"></script>

<!-- ./javascripts -->

<style>
    .dev-page{visibility: hidden;}            
</style>