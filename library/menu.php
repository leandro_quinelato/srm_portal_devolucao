<?php
@session_start();
include_once("DaoGlobal.class.php");
include_once("SistemaDao.class.php");
include_once("GlbSistema.class.php");
include_once("GlbSistemaPerfil.class.php");
include_once("GlbUsuario.class.php");
include_once("GlbColaborador.class.php");
include_once("GlbPessoa.class.php");
include_once("GlbEndereco.class.php");
include_once("GlbTelefone.class.php");
include_once("GlbProgramaRotina.class.php");
include_once("GlbModuloPrograma.class.php");
include_once("GlbSistemaModulo.class.php");
include_once("PerfilUsuarioDao.class.php");
include_once("PerfilPermissaoDao.class.php");

$ObjUsuario = unserialize($_SESSION['ObjLogin']);
$ObjRotinas = $ObjUsuario->getObjSistemaPerfil()->getObjRotina();

?>
<div class="dev-page-sidebar">
                    
    <ul class="dev-page-navigation">
        <?php 
        $codigosPrograma = array();
        $codigosModulo = array();

        foreach ($ObjRotinas as $chaveModulo) {
            if (!in_array($chaveModulo->getObjPrograma()->getObjModulo()->getCodigoModulo(), $codigosModulo)) {
                ?>
            <li>
                <a class="active" href="#"><i class="fa <?php echo $chaveModulo->getObjPrograma()->getObjModulo()->getDescricaoModulo(); ?>"></i><span><?php echo utf8_encode($chaveModulo->getObjPrograma()->getObjModulo()->getNomeModulo()); ?></span></a>
                <ul>
                <?php
                foreach ($ObjRotinas as $chave) {
                    if( $chave->getObjPrograma()->getObjModulo()->getCodigoModulo() == $chaveModulo->getObjPrograma()->getObjModulo()->getCodigoModulo() ){
                        ?>
                        <li><a href="<?php echo $chave->getObjPrograma()->getUrlPrograma(); ?>"><?php echo utf8_encode($chave->getObjPrograma()->getNomePrograma()); ?></a></li>
                        <?php
                        $codigosPrograma[] = $chave->getObjPrograma()->getCodigoPrograma();
                    }
                }
                $codigosPrograma = NULL;
                ?>
                </ul>
            </li>
                <?php
            }
            $codigosModulo[] = $chaveModulo->getObjPrograma()->getObjModulo()->getCodigoModulo();

        }
        ?>
    </ul>
    
</div>