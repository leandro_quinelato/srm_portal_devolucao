<?php

//include_once("../OcorrenciaArquivos.class.php");

/**
 * Description of OcorrenciaArquivosDao
 *
 * @author geovanni.info
 */
class OcorrenciaArquivosDao {

    private $conn;

    function __construct() {

        $this->conn = new DaoSistema();
        $this->conn->conectar();
    }

    public function registrarArquivo(OcorrenciaArquivos $OcorrenciaArquivos) {

        $sql = "
                    INSERT
                    INTO DEV_OCORRENCIA_ARQUIVOS
                      (
                        DOC_CO_NUMERO,
                        DTA_CO_CODIGO,
        ";

        if ($OcorrenciaArquivos->getNomeArquivoCliente()) {

            $sql .="
                     DOA_NO_ARQCLIENTE,
                   ";
        }

        $sql .="
        
                        
                        DOA_NO_ARQSYSTEM,
                        
        ";

        if ($OcorrenciaArquivos->getCodigoUsuario()) {

            $sql .="
                     USU_CO_NUMERO,
                   ";
        }

        $sql .="
                        DOA_TX_OBSERVACAO,
                        DOA_DT_CRIACAO
                      )
                      VALUES
                      (
                        {$OcorrenciaArquivos->getCodOcorrencia()},
                        {$OcorrenciaArquivos->getCodigoTipoArquivo()},
                            
                ";

        if ($OcorrenciaArquivos->getNomeArquivoCliente()) {

            $sql .="
                     '{$OcorrenciaArquivos->getNomeArquivoCliente()}',
                   ";
        }

        $sql .="

                        
                        '{$OcorrenciaArquivos->getNomeArquivoSystem()}',
                            
                ";

        if ($OcorrenciaArquivos->getCodigoUsuario()) {

            $sql .="
                     {$OcorrenciaArquivos->getCodigoUsuario()},
                   ";
        }

        $sql .="

                        
                        '{$OcorrenciaArquivos->getObservacaoArquivo()}',
                        SYSDATE
                      )
		";
	print_r($sql);
        if ($this->conn->execSql($sql)) {
            return true;
        }
        return false;
    }

    public function consultaArquivos( OcorrenciaArquivos $OcorrenciaArquivos) {

        $sql = " SELECT DOA.DOC_CO_NUMERO,
                        DOA.DTA_CO_CODIGO,
                        DTA.DTA_NO_DESCRICAO,
                        DOA.DOA_TX_OBSERVACAO,
                        DECODE(DOA.DOA_NO_ARQCLIENTE , NULL , DOA.DOA_NO_ARQSYSTEM, DOA.DOA_NO_ARQCLIENTE ) NOMEARQUIVO,
                        DOA.DOA_NO_ARQSYSTEM,
                        DOA.USU_CO_NUMERO,
                        USU.USU_NO_USERNAME,
                        DTA.DTA_NO_PATH

                 FROM  DEV_OCORRENCIA_ARQUIVOS  DOA
                 INNER JOIN DEV_TIPOARQUIVO     DTA   ON DOA.DTA_CO_CODIGO = DTA.DTA_CO_CODIGO
                 LEFT  JOIN GLOBAL.GLB_USUARIO  USU   ON USU.USU_CO_NUMERO = DOA.USU_CO_NUMERO
                 WHERE DOA.DOC_CO_NUMERO = {$OcorrenciaArquivos->getCodOcorrencia()}
                     
        ";
        
        if ($OcorrenciaArquivos->getCodigoTipoArquivo()){
            $sql .= "AND DOA.DTA_CO_CODIGO = {$OcorrenciaArquivos->getCodigoTipoArquivo()} ";
        }
        
                 
        $sql .= "
                 ORDER BY DOA.DTA_CO_CODIGO
        ";
//        if ($rpr_co_numero) {
//            $sql .=" AND REP.RPR_CO_NUMERO = $rpr_co_numero ";
//        }
//        if ($rpr_co_supervisor) {
//            $sql .=" AND REP.RPR_CO_SUPERVISOR = $rpr_co_supervisor ";
//        }
//        echo $sql;  
        return $this->conn->execSql($sql);
    }
    
    
    public function verificaNotaDev( OcorrenciaArquivos $OcorrenciaArquivos) {

        $sql = " SELECT count(*) QTD
                 FROM DEV_OCORRENCIA_ARQUIVOS
                 WHERE DOC_CO_NUMERO = {$OcorrenciaArquivos->getCodOcorrencia()}
                 and   DTA_CO_CODIGO = 1
        ";
                 
//        //echo $sql;  
        $result =  $this->conn->execSql($sql);
        OCIFetchInto($result, $row, OCI_ASSOC);
        
        if($row['QTD'] > 0){
            return TRUE;
        }else{
            return FALSE;
        }

    }

    public function verificaTipoOcorrencia( OcorrenciaArquivos $OcorrenciaArquivos) {

        $sql = " SELECT TIPO.TOC_NO_DESCRICAO
                    FROM DEV_OCORRENCIA OCO, CAL_TIPO_OCORRENCIA TIPO
                        WHERE OCO.TOC_CO_NUMERO = TIPO.TOC_CO_NUMERO
                            AND OCO.DOC_CO_NUMERO = {$OcorrenciaArquivos->getCodOcorrencia()}
        ";
                 
//        //echo $sql;  
        $result =  $this->conn->execSql($sql);
        OCIFetchInto($result, $row, OCI_ASSOC);
        
        if($row['TOC_NO_DESCRICAO'] == 'SOBRA DE MERCADORIA FISICA'){
            return TRUE;
        }else{
            return FALSE;
        }

    }
    
    public function ExcluirArquivo(OcorrenciaArquivos $OcorrenciaArquivos) {

        $sql = "
                    DELETE
                    FROM DEV_OCORRENCIA_ARQUIVOS
                    WHERE DOC_CO_NUMERO = {$OcorrenciaArquivos->getCodOcorrencia()}
                    AND   DTA_CO_CODIGO = {$OcorrenciaArquivos->getCodigoTipoArquivo()}
                    AND   DOA_NO_ARQCLIENTE = '{$OcorrenciaArquivos->getNomeArquivoCliente()}'
		";
        //echo $sql;
        if ($this->conn->execSql($sql)) {
            return true;
        }
        return false;
    }
    
    
    
}


/*
 * 

;
 * 
 */
